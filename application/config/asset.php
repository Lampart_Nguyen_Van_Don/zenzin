<?php
$config['asset'] = array(
        'common'    => array(
                'css' => array(
                        '/public/admin/css/common.css',
                        '/public/admin/css/default.css',
                        '/public/admin/css/jquery-confirm.css',
                        '/public/admin/css/lib.css',
                        '/public/admin/iconfonts/genericons.css'
                ),
                'js' => array(
                        '/public/admin/js/jquery-1.11.2.min.js',
                        '/public/admin/js/jquery-confirm.js',
                        '/public/admin/js/jquery.cookie.js',
                        '/public/admin/js/smoothscroll.js',
                        '/public/admin/js/theme.js',
                        '/public/admin/js/bignumber.min.js',
                        '/public/admin/js/com.js',
                )
        ),
        'authenticate'    => array(
                'css' => array(
                        '/public/admin/css/login.css'
                ),
                'js' => array(

                )
        ),
        'account'    => array(
                'css' => array(
                    '/public/admin/css/jquery-ui.css',
                    '/public/admin/account/style.css',
                ),
                'js' => array(
                    '/public/admin/js/jquery-ui.min.js',
                )
        ),
        'authority'    => array(
                'css' => array(
                    '/public/admin/css/jquery-ui.css'
                ),
                'js' => array(
                    '/public/admin/js/jquery-ui.min.js'
                )
        ),
        'department'  => array(
                'css' => array(
                    '/public/admin/css/jquery-ui.css'
                ),
                'js' => array(
                    '/public/admin/js/jquery-ui.min.js'
                )
         ),
        'bank_account'  => array(
            'css' => array(
                '/public/admin/css/jquery-ui.css'
            ),
            'js' => array(
                '/public/admin/js/jquery-ui.min.js'
            )
        ),
        'business_unit'  => array(
            'css' => array(
                '/public/admin/css/jquery-ui.css'
            ),
            'js' => array(
                '/public/admin/js/jquery-ui.min.js'
            )
        ),
        'gui' => array(
                'css' => array(
                    '/public/admin/css/jquery-ui.css',
                ),
                'js' => array(
                    '/public/admin/js/jquery-ui.min.js',
                )
        ),
        'shipping' => array(
                'css' => array(
                    '/public/admin/css/jquery-ui.css',
                ),
                'js' => array(
                    '/public/admin/js/jquery-ui.min.js'
                )
        ),
        'holiday' => array(
                'css' => array(
                    '/public/admin/css/holiday.css',
                    '/public/admin/css/jquery-ui.css',
                ),
                'js' => array(
                    '/public/admin/js/jquery-ui.min.js',
                ),
        ),
        'client'  => array(
                'css' => array(
                    '/public/admin/css/jquery-ui.css',
//#7846:Start
//                    '/public/admin/css/ui-grid-unstable.css',
                    '/public/admin/css/ui-grid-unstable-ver3.0.7.css',
//#7846:End
                    '/public/admin/order/main.css',
                    '/public/admin/client/main.css'
                ),
                'js' => array(
                    '/public/admin/js/jquery-ui.min.js',
                	//#6985:Start
                    //'/public/admin/js/angular.min.js',
                	'/public/admin/js/angular.js',
                    //#6985:End
//#7846:Start
                   // '/public/admin/js/angular-touch.min.js',
                    //'/public/admin/js/angular-animate.js',
                    '/public/admin/js/angular-sanitize.min.js',
                   '/public/admin/js/ui-grid-unstable.min.js',
                    '/public/admin/js/ui-grid-unstable-ver3.0.7.js',
//#7846:End
                    '/public/admin/js/jquery.floatThead.min.js',
                    '/public/admin/js/jquery.number.min.js',
                    '/public/admin/client/function.js',
                )
         ),
        'subcontractor'  => array(
                'css' => array(
                        '/public/jqwidgets/styles/jqx.base.css',
                        '/public/jqwidgets/styles/jqx.office.css',
                        '/public/admin/css/jquery-ui.css'
                ),
                'js' => array(
                        '/public/jqwidgets/jqxcore.js',
                        '/public/jqwidgets/jqxdata.js',
                        '/public/jqwidgets/jqxcore.js',
                        '/public/jqwidgets/jqxbuttons.js',
                        '/public/admin/js/jquery-ui.min.js',
                        '/public/jqwidgets/jqxscrollbar.js',
                        '/public/jqwidgets/jqxlistbox.js'
                )
        ),
        'sample'  => array(
                'css' => array(
                        '/public/admin/css/jquery-ui.css'
                ),
                'js' => array(
                        '/public/admin/js/jquery-ui.min.js'
                )
        )
        ,
        'product'  => array(
                'css' => array(
                        '/public/admin/css/jquery-ui.css'
                ),
                'js' => array(
                        '/public/admin/js/jquery-ui.min.js'
                )
        ),
        'order_acceptance' => array(
                'css' => array(
                    '/public/admin/css/jquery-ui.css',
                    '/public/admin/css/order_acceptance.css',
//#7846:Start
//                    '/public/admin/css/ui-grid-unstable.css',
                    '/public/admin/sample/main.css',

//                    '/public/admin/css/jquery-ui.css',
                ),
                'js' => array(
//                    '/public/admin/js/jquery-ui.min.js',
//                    '/public/admin/js/jquery.form.js',
//                    '/public/admin/js/angular.js',
//                    '/public/admin/js/angular-touch.js',
//                    '/public/admin/js/angular-animate.js',
//                    '/public/admin/js/angular-sanitize.min.js',
//                    '/public/admin/js/ui-grid-unstable.js',
//                    '/public/admin/order_acceptance/app.js',
//                    '/public/admin/order/detail.js',
                    '/public/admin/js/jquery-ui.min.js',
//                    '/public/admin/js/jszip.js',
//                    '/public/admin/js/xlsx.js',
//                    '/public/admin/client/function.js'
//#7846:End
                )
        ),
        'order' => array(
                'css' => array(
                        '/public/admin/css/jquery-ui.css',
                ),
                'js' => array(
                        '/public/admin/js/jquery-ui.min.js',
                        '/public/admin/order/function.js',
                        '/public/admin/client/function.js',
                )
        ),
        'advance_payment' => array(
                'css' => array(
                        '/public/admin/css/jquery-ui.css',
                ),
                'js' => array(
                        '/public/admin/js/jquery-ui.min.js',
                )
        ),
        'finish_work_report' => array(
                'css' => array(
                        '/public/admin/css/jquery-ui.css',
                        '/public/admin/finish_work_report/main.css'
                ),
                'js' => array(
                        '/public/admin/js/jquery-ui.min.js',
                        '/public/admin/client/function.js'
                )
        ),
        'invoice' => array(
                'css' => array(
                        '/public/admin/css/jquery-ui.css',
//#7846:Start
//                        '/public/admin/css/ui-grid-unstable.css',
                        '/public/admin/css/ui-grid-unstable-ver3.0.7.css',
//#7846:End
                        '/public/admin/css/jquery-ui.css',
                        '/public/admin/css/invoice.css',
                        '/public/admin/css/order_acceptance.css',
                ),
                'js' => array(
                        '/public/admin/js/jquery-ui.min.js',
                        '/public/admin/js/angular.js',
//#7846:Start
//                        '/public/admin/js/angular-touch.js',
//                        '/public/admin/js/angular-animate.js',
                        '/public/admin/js/angular-sanitize.min.js',
//                        '/public/admin/js/ui-grid-unstable.js',
                        '/public/admin/js/ui-grid-unstable-ver3.0.7.js',
//#7846:End
                        '/public/admin/invoice/app.js',
                        '/public/admin/client/function.js',
                )
         ),
        'sales' => array(
                'css' => array(
                        '/public/admin/css/jquery-ui.css',
                        '/public/admin/css/handsontable.full.min.css',
                        '/public/admin/css/select2.css',
                        '/public/admin/css/ui-grid-unstable.css',
                        '/public/admin/sales/main.css',
                ),
                'js' => array(
                        '/public/admin/js/jquery-ui.min.js',
                        '/public/admin/js/select2.min.js',
                        '/public/admin/js/handsontable.full.min.js',
                        '/public/admin/js/select2-editor.js',
                        '/public/admin/js/angular.js',
                        '/public/admin/js/angular-touch.js',
                        '/public/admin/js/angular-animate.js',
                        '/public/admin/js/angular-sanitize.min.js',
                        '/public/admin/js/ui-grid-unstable.js',
                        '/public/admin/sales/app.js',
                        '/public/admin/sales/function.js',
                        '/public/admin/client/function.js'
                )
        ),

        'account_title'  => array(
                'css' => array(
                    '/public/admin/css/jquery-ui.css'
                ),
                'js' => array(
                    '/public/admin/js/jquery-ui.min.js'
                )
        ),
        /* start volyminhnhan@gmail.com modification*/
        'reconcile_amount' => array(
                'css' => array(
                    '/public/admin/css/jquery-ui.css',
                    '/public/admin/css/ui-grid-unstable.css',
                    '/public/admin/css/order_acceptance.css',
                    '/public/admin/css/handsontable.full.min.css',
                    '/public/admin/reconcile_amount/main.css',
                ),
                'js' => array(
                    '/public/admin/js/jquery-ui.min.js',
                    '/public/admin/js/jquery.form.js',
                    '/public/admin/js/jquery-fixed-thead.js',
                )
        ),
        'sales_target' => array(
                'css' => array(
                        '/public/admin/css/jquery-ui.css',
                        '/public/admin/sales_target/main.css',
                ),
                'js' => array(
                        '/public/admin/js/jquery-ui.min.js',
                )
        ),
		'closing_accountant'  => array(
				'css' => array(
						'/public/admin/css/jquery-ui.css'
				),
				'js' => array(
						'/public/admin/js/jquery-ui.min.js',
						'/public/admin/closing_accountant/app.js'
				)
		),
);
