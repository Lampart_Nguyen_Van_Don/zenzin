<?php
const STATUS_ENABLE = 0;
const STATUS_DISABLE = 1;
const IS_DISPLAY_MENU = 1;
const MAX_SEQUENCE_ALLOW = 65535; //500
const DAYS_CHANGE_PASSW = 30;
const AJAX_SUCCESS = 1;
const AJAX_FAIL = 0;

const GENDER_MALE = 1;
const GENDER_FEMALE = 2;

//Day of week
const DAY_OF_WEEK_MON = 1;
const DAY_OF_WEEK_TUE = 2;
const DAY_OF_WEEK_WED = 3;
const DAY_OF_WEEK_THU = 4;
const DAY_OF_WEEK_FRI = 5;
const DAY_OF_WEEK_SAT = 6;
const DAY_OF_WEEK_SUN = 7;

//Holiday
const HOLIDAY   = 8;
const GAP_YEARS = 10;

//Client
const CLIENT_STATUS_CREDIT_WAITING       = 1;   // 与信待ち
const CLIENT_STATUS_CREDIT_ALREADY       = 11;  // 与信済み（期限内）
const CLIENT_STATUS_CL_APPROVAL_PENDING  = 21;  // CL承認申請中
const CLIENT_STATUS_CL_APPROVED          = 22;  // CL承認済み
const CLIENT_STATUS_CREDIT_EVERYTIME     = 23;  // 都度与信
const CLIENT_STATUS_CREDIT_EXPIRED       = 12;  // 与信済み（期限切れ）
const CLIENT_STATUS_RE_CREDIT_WAITING    = 24;  // 再与信待ち
const CLIENT_STATUS_CL_APPROVAL_REJECTED = 29;  // CL承認却下
const CLIENT_STATUS_TRADING_NG           = 19;  // 取引ＮＧ

const CLIENT_CREDIT_LEVEL_A = 1;
const CLIENT_CREDIT_LEVEL_B = 2;
const CLIENT_CREDIT_LEVEL_C = 3;
const CLIENT_CREDIT_LEVEL_NG = 9;

const CLIENT_CORPORATE_STATUS_NONE = 1;

const CLIENT_PAYMENT_CURRENT_MONTH = 0;

//Sales slip
const SALES_STATUS_NO_INPUT = 1;                    // 売上明細未入力
const SALES_STATUS_UNCREATED = 2;                   // 売上伝票未作成
const SALES_STATUS_CREATED = 3;                     // 売上伝票作成中
const SALES_STATUS_APPROVAL_PENDING = 11;           // 売上伝票承認申請中
const SALES_STATUS_INVOICE_APPROVAL_PENDING = 21;   // 請求書発行承認申請中
const SALES_STATUS_INVOICE_APPROVED = 22;           // 請求書発行承認済み
const SALES_STATUS_INVOICE_ISSUED = 23;             // 請求書発行済み
const SALES_STATUS_INVOICED_DISCARDED = 29;         // 請求書発行破棄
const SALES_STATUS_INVOICED_MODIFY = 1;         // 請求書発行破棄

//Flags Invoice

const IS_AD_INVOICE_ISSUE =1;
const IS_NOT_AD_INVOICE_ISSUE = 0;
const IS_INVOICE_ISSUE =1;
const IS_NOT_INVOICE_ISSUE  =  0;
const IS_AD_RECEIVE_PAYMENT =  1;
const IS_NOT_AD_RECEIVE_PAYMENT =  0;
const IS_NOT_AD_SALES_SLIP_INPUT = 0;
const IS_NOT_SALES_SLIP_INPUT = 0;
const IS_ACCEPTANCE_INPUT = 1;
const IS_NOT_ACCEPTANCE_INPUT = 0;
const IS_NOT_ORDER_INPUT = 0;
const IS_ORDER_INPUT = 1;
const IS_INSERT_ORDERACCEPTANCE =  4;

//Authorities
const AUTH_RECEIVE_CRON_CHECK_CREDIT_MAIL = 108; //メール送信-再与信督促
const AUHT_DM_BUSINESS_CHIEF = 5;
const AUTH_DP_BUSINESS_CHIEF = 8;
const AUTH_CONTROL_BRAIN_CHIEF = 1;

//ORDER
const ORDER_STATUS_UNAPPROVED = 1;
const ORDER_STATUS_UNDETERMINED = 2;
const ORDER_STATUS_CONFIRMED = 3;
const ORDER_STATUS_REJECTED = 4;
const ORDER_STATUS_CANCELLED = 9;


//ORDER ACCEPTANCE
const IMPORT_ORDER_ACCEPTANCE_TEMPLATE_PATH = 'public/admin/order_acceptance/発注入力テンプレート.xlsx';
const ORDER_ACCEPTANCE_UNAPPROVED           = 1; //未承認
const ORDER_ACCEPTANCE_APPROVED             = 2; //検収承認済み
const ORDER_ACCEPTANCE_ACCOUNT_TIGHTEN      = 3; //経理締め済み

const ORDER_ACCEPTANCE_IS_REPORT_NOT_READY = 1;
const ORDER_ACCEPTANCE_IS_REPORT_ALREADY = 2;

//GUI PARTS ELEMENT
const SHIPMENTS_SHAPE_ID = 5;
//7989:Start
const COMPANY_TITLE_ID = 14; //法人格
//7989:End
const TAX_EXCLUSIVE = 1; // 外
const TAX_INCLUDED  = 2; // 内
const TAX_NON       = 3; // 非

const GUI_PART_TAX_DIVISION = 6; // 消費税区分

const GUI_PART_NEN_INPUT = 7; // 消費税区分
//TAX
const CONSUMPTION_TAX = 1; //消費税

// CLOSING ACCOUNTANT
const ACCOUNTANT_UNCLOSE_BUSINESS_DAYS = 7;

// BUGYO MANAGE NAME
const BUGYO_ORDINARY_DEPOSIT   = 'ordinary_deposit';
const BUGYO_ACCOUNT_RECEIVABLE = 'receivable';
const BUGYO_COMMISSION_PAID    = 'commission_payment';
const BUGYO_AD_RECEIVE         = 'ad_receive_payment';

const OVER_RECORD = 2000;

//#7315:Start
const MAX_SIGNED_INT_10 = 2147483647;
//#7315:End
//#9547:Start
const MAX_UNSIGNED_INT_10 = 4294967295;
//#9547:End

//7910:Start
const DM_FWR_TEMPLATE_CODE = 1; //FWR: Finish Work Report
const DP_FWR_TEMPLATE_CODE = 2; //FWR: Finish Work Report
//7910:End