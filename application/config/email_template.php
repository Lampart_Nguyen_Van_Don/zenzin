<?php defined('BASEPATH') OR exit('No direct script access allowed');
$config['mail_list'] = array(
        'cron_check_credit'     => 'cron_check_credit.php',
        'client_apply'          => 'client_apply.php',
        'client_reapply'        => 'client_reapply.php',
        'client_credit'         => 'client_credit.php',
        'order_approve_reject'  => 'order_approve_reject.php',
        'change_report'         => 'change_report.php',
        'change_report_apply'   => 'change_report_apply.php',
#7625:Start
        'change_report_cancel_request'   => 'change_report_cancel_request.php',
        'change_report_approve_cancel_request'   => 'change_report_approve_cancel_request.php',
#7625:End

#9787:Start
        'discard_invoice'   => 'discard_invoice.php',
#9787:End

);
