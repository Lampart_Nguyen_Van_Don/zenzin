<?php
/**
 * List primary keys are not changed
 *
 * email system
 * $mail['from']
 * $mail['system_to']
 * $mail['system_cc']
 * $mail['system_bcc']
 * $mail['system_subject']
 * $mail['system_body']
 *
 * customer
 * $mail['customer_cc']
 * $mail['customer_bcc']
 * $mail['customer_subject']
 * $mail['customer_body']
 */

/**
 * List variables
 */

$mail['customer_subject'] = '';

$replace_list = array(
    'account_id' => '{account_id}',
    'client_name' => '{client_name}',
    'j_code' => '{j_code}',
    'branch_cd' => '{branch_cd}',
    'bu_name' => '{bu_name}',
    'mail_content' => '{mail_content}'
);


$mail['system_subject'] = '【変レポ変更内容承認申請】{j_code}-{branch_cd} / {account_id} {bu_name}';

/**
 * content mail
 */
$mail['system_body'] = '
変更レポートが申請されました。
－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
Ｊ担当営業：{account_id}
申込社名　：{client_name}
受注番号　：{j_code}-{branch_cd}
－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
{mail_content}
';