<?php

$mail['customer_subject'] = '';

$replace_list = array(
    'account_id'	=> '{account_id}',
    'client_name'	=> '{client_name}',
    'j_code'		=> '{j_code}',
    'branch_cd'		=> '{branch_cd}',
    'mail_content' 	=> '{mail_content}',
    'bu_name'		=> '{bu_name}'
);

$mail['system_subject'] = '【変レポ変更内容承認報告】{j_code}-{branch_cd} / {account_id} {bu_name}';

$mail['system_body'] = '
変レポで申込キャンセルが申請されました。
----------------------------------------
Ｊ担当営業：{account_id}
申込社名　：{client_name}
受注番号　：{j_code}-{branch_cd}
----------------------------------------
{mail_content}
';