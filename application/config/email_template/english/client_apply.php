<?php
/**
 * List primary keys are not changed
 *
 * email system
 * $mail['from']
 * $mail['system_to']
 * $mail['system_cc']
 * $mail['system_bcc']
 * $mail['system_subject']
 * $mail['system_body']
 *
 * customer
 * $mail['customer_cc']
 * $mail['customer_bcc']
 * $mail['customer_subject']
 * $mail['customer_body']
 */

/**
 * List key info replace
 */
$replace_list = array(
        'name'           => '{name}',
        'name_kana'      => '{name_kana}',
        's_account_id' => '{s_account_id}',
        'payment_term'   => '{payment_term}',
        'zipcode'        => '{zipcode}',
        'address1'       => '{address1}',
        'address2'       => '{address2}',
        'phone_no'       => '{phone_no}',
        'business_category'   => '{business_category}',
        'remark'         => '{remark}',
);

$mail['customer_subject'] = '';
$mail['system_subject'] = '【新規与信確認】{name}';

/**
 * content message
 */
$mail['system_body'] =
'
{name} 様の与信確認をお願い致します。

------------------------------------------------------------

会社名        :  {name}
フリガナ        ：  {name_kana}
営業担当者   :  {s_account_id}
支払条件     ：  {payment_term}

郵便番号        ：  {zipcode}
住所        ：  {address1}
               {address2}
TEL        ：  {phone_no}
業種        ：  {business_category}
特記事項        ：  {remark}

------------------------------------------------------------
'
;