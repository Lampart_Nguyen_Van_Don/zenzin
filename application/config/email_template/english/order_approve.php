<?php
/**
 * List primary keys are not changed
 *
 * email system
 * $mail['from']
 * $mail['system_to']
 * $mail['system_cc']
 * $mail['system_bcc']
 * $mail['system_subject']
 * $mail['system_body']
 *
 * customer
 * $mail['customer_cc']
 * $mail['customer_bcc']
 * $mail['customer_subject']
 * $mail['customer_body']
 */

/**
 * List key info replace
 */
$replace_list = array(
        'j_account_id'   => '{j_account_id}',
        'client_name'    => '{client_name}',
        'j_code'         => '{j_code}',
        'client_id'      => '{client_id}',
        'branchs'        => '{branchs}',
        'cost_total_price'  => '{cost_total_price}',
        'gross_profit_amount' => '{gross_profit_amount}',
        'gross_profit_rate'   => '{gross_profit_rate}',
);

$mail['customer_subject'] = '';
$mail['system_subject'] = '【受注計上報告】{営業担当} / {営業担当の事業部}{営業担当の部署} / {社名} / {税別合計の合算}円}';

/**
 * content message
 */
$mail['system_body'] =
'
受注が計上されました。
－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
Ｊ担当営業：{j_account_id}
申込社名　：{client_name}
受注番号　：{j_code}
－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
枝 / 商品名 / 内容名 / 納品日 / 受注金額 / 粗利額 / 粗利率
{branchs}
－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
計上合計金額：{cost_total_price}円
粗利額合計額：{gross_profit_amount}円
粗利率　　　：{gross_profit_rate}%

おめでとうございます！
'
;