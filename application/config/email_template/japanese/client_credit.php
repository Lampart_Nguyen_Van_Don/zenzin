<?php
/**
 * List primary keys are not changed
 *
 * email system
 * $mail['from']
 * $mail['system_to']
 * $mail['system_cc']
 * $mail['system_bcc']
 * $mail['system_subject']
 * $mail['system_body']
 *
 * customer
 * $mail['customer_cc']
 * $mail['customer_bcc']
 * $mail['customer_subject']
 * $mail['customer_body']
 */

/**
 * List key info replace
 */
$replace_list = array(
        'subject'        => '{subject}',
        'name'           => '{name}',
        'account_1st'    => '{account_1st}',
        'account_2nd'    => '{account_2nd}',
        'client_id'      => '{client_id}',
        'is_recredit'    => '{is_recredit}',
        'payment_terms'  => '{payment_terms}',
        'zipcode'        => '{zipcode}',
        'address_1'      => '{address_1}',
        'address_2'      => '{address_2}',
        'phone_no'       => '{phone_no}',
        'business_category'   => '{business_category}',
        'representative_name' => '{representative_name}',
        'delivery_amount' => '{delivery_amount}',
        'credit_status'  => '{credit_status}',
        'credit_amount'  => '{credit_amount}',
        'comment'        => '{comment}',
        'expire'         => '{expire}',
        'business_name'  => '{business_name}'
);

$mail['customer_subject'] = '';
$mail['system_subject'] = '{subject}{client_id}/{name} ({business_name})';

/**
 * content message
 */
$mail['system_body'] =
'
----------------------------------------
取引先申請のお知らせ

下記企業の与信結果が出ました。
----------------------------------------
{account_1st}
{account_2nd}
申請番号	  ：{client_id}
申請区分	  ：{is_recredit}

取引先名称　：{name}
郵便番号	  ：{zipcode}
住所	  ：{address_1}
　　　　　	　  {address_2}
ＴＥＬ　　	  ：{phone_no}
代表者名	  ：{representative_name}

業種　　　　　　　　	：{business_category}
納品予定額（月額）　	：{delivery_amount}
支払サイト　　　　　	：{payment_terms}

■与信結果
与信レベル  ：{credit_status}
与信限度額  ：{credit_amount}
{expire}
経理担当承認コメント	：{comment}
'
;