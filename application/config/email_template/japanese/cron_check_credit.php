<?php
/**
 * List primary keys are not changed
 *
 * email system
 * $mail['from']
 * $mail['system_to']
 * $mail['system_cc']
 * $mail['system_bcc']
 * $mail['system_subject']
 * $mail['system_body']
 *
 * customer
 * $mail['customer_cc']
 * $mail['customer_bcc']
 * $mail['customer_subject']
 * $mail['customer_body']
 */

/**
 * List key info replace
 */
$replace_list = array(
        'system_subject' => '{system_subject}',
        's_code'         => '{s_code}',
        'name'           => '{name}',
        'name_kana'      => '{name_kana}',
        's_account_id_1st' => '{s_account_id_1st}',
        's_account_id_2nd' => '{s_account_id_2nd}',
        'credit_amount'  => '{credit_amount}',
        'payment_term'   => '{payment_term}',
        'last_delivery_month' => '{last_delivery_month}',
        'zipcode'        => '{zipcode}',
        'address1'       => '{address1}',
        'address2'       => '{address2}',
        'phone_no'       => '{phone_no}',
        'business_category'   => '{business_category}',
        'remark'         => '{remark}',
        'comment'        => '{comment}',
        'credit_approval_date' => '{credit_approval_date}'
);

$mail['customer_subject'] = '';
$mail['system_subject'] = '{system_subject}';

/**
 * content message
 */
$mail['system_body'] =
'
{name} 様の再与信を {credit_approval_date} までにお願いします。

----------------------------------------

Ｓコード	        :　{s_code}
会社名		:　{name}
フリガナ	        :　{name_kana}
営業担当者　	:　{s_account_id_1st}，{s_account_id_2nd}
与信額　　　	:　{credit_amount}
支払条件　　	:　{payment_term}
最終取引月　	:　{last_delivery_month}

郵便番号　　	:　{zipcode}
住所		:　{address1}
		　 {address2}
ＴＥＬ		:　{phone_no}
業種		:　{business_category}
特記事項　　	:　{remark}
コメント　　	        :　{comment}

----------------------------------------
'
;