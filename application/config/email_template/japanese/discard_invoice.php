<?php
$mail['customer_subject'] = '';

$replace_list = array(
        'client_name' => '{client_name}',
        'sales_slip_number' => '{sales_slip_number}',
);


$mail['system_subject'] = '【請求書発行却下】{client_name}_{sales_slip_number}';

/**
 * content mail
 */
$mail['system_body'] = '
請求書発行が却下されました。
内容を確認の上、再申請して下さい。

クライアント名：{client_name}
売伝番号　　　：{sales_slip_number}
';