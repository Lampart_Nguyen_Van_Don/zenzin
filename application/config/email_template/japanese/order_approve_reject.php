<?php
/**
 * List primary keys are not changed
 *
 * email system
 * $mail['from']
 * $mail['system_to']
 * $mail['system_cc']
 * $mail['system_bcc']
 * $mail['system_subject']
 * $mail['system_body']
 *
 * customer
 * $mail['customer_cc']
 * $mail['customer_bcc']
 * $mail['customer_subject']
 * $mail['customer_body']
 */

/**
 * List key info replace
 */
$replace_list = array(
        'charge_name'    => '{charge_name}',
        'business_unit_name' => '{business_unit_name}',
        'department_name' => '{department_name}',
        'j_account_id'   => '{j_account_id}',
        'client_name'    => '{client_name}',
        'j_code'         => '{j_code}',
        'order_detail_mail_content'   => '{order_detail_mail_content}',
        'total_cost_total_price'  => '{total_cost_total_price}',
        'total_gross_profit_amount' => '{total_gross_profit_amount}',
        'total_gross_profit_rate'   => '{total_gross_profit_rate}',
);

$mail['customer_subject'] = '';
$mail['system_subject'] = '【受注計上報告】{charge_name} / {business_unit_name} {department_name} / {client_name} / {total_cost_total_price}円';

/**
 * content message
 */
$mail['system_body'] =
'
受注が計上されました。
----------------------------------------
Ｊ担当営業：{j_account_id}
申込社名　：{client_name}
受注番号　：{j_code}
----------------------------------------
枝 / 商品名 / 内容名 / 納品日 / 受注金額 / 粗利額 / 粗利率
{order_detail_mail_content}
----------------------------------------
計上合計金額　：{total_cost_total_price}円
粗利額合計額　：{total_gross_profit_amount}円
粗利率　　　　　 ：{total_gross_profit_rate}%

おめでとうございます！
'
;