<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|    http://codeigniter.com/user_guide/general/hooks.html
|
*/
$hook['pre_controller'] = array(
        array(
                'class'    => 'pre_controller',
                'function' => 'load_lang',
                'filename' => 'pre_controller.php',
                'filepath' => 'hooks',
                'params'   => ''
        ),
        array(
                'class'    => 'pre_controller',
                'function' => 'do_authenticate',
                'filename' => 'pre_controller.php',
                'filepath' => 'hooks',
                'params'   => ''
        ),
        array(
                'class'    => 'pre_controller',
                'function' => 'do_debug',
                'filename' => 'pre_controller.php',
                'filepath' => 'hooks',
                'params'   => ''
        ),
        array(
                'class'    => 'pre_controller',
                'function' => 'load_menu',
                'filename' => 'pre_controller.php',
                'filepath' => 'hooks',
                'params'   => ''
        ),
        array(
                'class'    => 'pre_controller',
                'function' => 'generate_redirect_post_data',
                'filename' => 'pre_controller.php',
                'filepath' => 'hooks',
                'params'   => ''
        )
);