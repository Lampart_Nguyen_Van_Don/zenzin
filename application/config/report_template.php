<?php defined('BASEPATH') OR exit('No direct script access allowed');
$config['report_list'] = array(
        'finish_work_report'     => 'finish_work_report.php',
        'invoice'                => 'invoice.php',
        'advance_payment_report'     => 'advance_payment_report.php',
);
