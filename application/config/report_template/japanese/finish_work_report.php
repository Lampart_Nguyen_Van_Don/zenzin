<?php

/**
 * List key info replace
 */
$replace_list = array(
    'client_name' => '{client_name}',
    'report_division_name' => '{report_division_name}',
    'report_charge_name' => '{report_charge_name}',
    'report_fax_no' => '{report_fax_no}',
    'delivery_date' => '{delivery_date}',
    'report_contents' => '{report_contents}',
    'report_remarks' => '{report_remarks}'
);

$content = '
    <style type="text/css">

        table.customer-info {
            font-size: 14px;
            text-align: right;
            width: 250px;
            font-family: msgothici;
        }

        table.logo {
            width: 100px;
        }

        table.term {
            font-family: msgothici;
            font-size: 15px;
        }

        table.delivery_info {
            font-family: msgothici;
            font-size: 15px;
            width: 580px;
        }

        table.delivery_info td {
            border: 1px solid #000;
            text-align: center;
        }
    </style>

    <page>
        <br/><br/><br/>
        <img src="/public/admin/finish_work_report/img/header.png" border="0" height="49" width="761"/><br/>
        <img src="/public/admin/finish_work_report/img/lbl_customer_info.png" border="0" height="21" width="50"/><br/>
        <br/>
        <table>
            <tr>
                <td>
                    <table class="customer-info">
                        <tr>
                            <td style="width: 260px">{client_name}<br/></td>
                        </tr>
                        <tr>
                            <td>{report_division_name}<br/></td>
                        </tr>
                        <tr>
                            <td style="border-bottom: 0.1px solid #000;">{report_charge_name}&nbsp;&nbsp;&nbsp;社</td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="width: 110px"></td>
                            <td style="width: 150px">
                                <table>
                                    <tr>
                                        <td  style="border-bottom: 0.1px solid #000"><span style="font-size: 10px">FAX: </span>{report_fax_no}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                   </table>
                </td>
                <td>
                    <img src="/public/admin/finish_work_report/img/logo.png" border="0" height="152" width="300" />
                </td>
            </tr>
        </table>
        <br/><br/><br/>
        <table class="term">
            <tr><td>いつも大変お世話になっております。</td></tr>
            <tr><td>株式会社ゼンリンビズネクサス　カスタマーサービス課でございます。</td></tr>
        </table>

        <br/><br/>
        <table class="term">
            <tr><td>ご依頼を頂いておりました発送の作業を完了致しましたので、</td></tr>
            <tr><td>ご報告申し上げます。</td></tr>
        </table>

        <br/><br/>
        <table class="term">
            <tr><td>ご発注ありがとうございました。</td></tr>
            <tr><td>今後も何卒宜しくお願い申し上げます。</td></tr>
        </table>

        <br/><br/>
        <table>
            <tr>
                <td style="width: 70px"></td>
                <td>
                    <table cellpadding="4" class="delivery_info">
                        <tr>
                            <td style="width: 100px">完了日</td>
                            <td>{delivery_date}</td>
                        </tr>
                        <tr>
                            <td>内容</td>
                            <td>{report_contents}</td>
                        </tr>
                        <tr>
                            <td>備考</td>
                            <td>{report_remarks}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <br/><br/><br/>
        <img src="/public/admin/finish_work_report/img/note.png" border="0" height="162" width="732"/>

        <br/><br/><br/><br/><br/><br/>
        <img src="/public/admin/finish_work_report/img/footer.png" border="0" height="67" width="761"/><br/>
    </page>
    ';

?>