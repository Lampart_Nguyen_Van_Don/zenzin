<?php

/**
 * List key info replace
 */
$replace_list = array(
    'client_name' => '{client_name}',
    'report_division_name' => '{report_division_name}',
    'report_charge_name' => '{report_charge_name}',
    'report_fax_no' => '{report_fax_no}',
    'delivery_date' => '{delivery_date}',
    'report_contents' => '{report_contents}',
    'report_remarks' => '{report_remarks}'
);

$content = '
    <style type="text/css">
        * {
            font-family: cid0jp;
        }

        .center-text {
            text-align: center;
        }

        .header {
            font-size: 25px;
            color: #003366;
        }
        
        .top-table,
        .2nd-table {
            font-size: 9px;
        }

        .green-bg {
            background-color: #CCFFCC;
        }

        .grey-bg {
            background-color: #C0C0C0;
        }
        
        .white-bg {
            background-color: #FFF;
        }
        .s-code-table tr td {
            border: 1px dashed #C0C0C0;
        }
        .total-amount-table {
            font-size: 14px;
        }
        
        .bank-account-table {
            border: 1px solid red;
        }

        .bank-account-wrapper {
            border: 2px solid red;
            border-collapse: separate;
            border-spacing: 10px 50px;
        }

    </style>

    <page>
        <div class="header-wrapper">
            <div class="header center-text">請求書</div>
            <span class="left">〒</span>
            <br><br><br>
            <table class="top-table" border="0">
                <tr>
                    <td width="60%"></td>
                    <td width="40%">株式会社ゼンリンビズネクサス</td>
                </tr>
                <tr>
                    <td></td>
                    <td>〒101-0065</td>
                </tr>
                <tr>
                    <td>TEL FAX</td>
                    <td>東京都千代田区西神田1-1-1</td>
                </tr>
                <tr>
                    <td></td>
                    <td>オフィス21ビル2階</td>
                </tr>
                <tr>
                    <td></td>
                    <td>TEL　03-5577-1990</td>
                </tr>
                <tr>
                    <td>毎度格別のお引き立てを賜り、誠にありがとうございます。<br>下記の通り、ご請求申し上げます。
                    </td>
                    <td>担当者名</td>
                </tr>
            </table>
            <br>
            <table class="2nd-table" border="0">
                <tr>
                    <td width="60%"></td>
                    <td width="40%"></td>
                </tr>
                <tr>
                    <td>
                        <table class="s-code-table">
                            <tr>
                                <td class="green-bg" width="80px">&nbsp;お客様コード</td>
                                <td class="white-bg" width="60px"></td>
                            </tr>
                            <tr>
                                <td class="green-bg">&nbsp;請求書番号</td>
                                <td class="white-bg"></td>
                            </tr>
                        </table>
                    </td>
                    <td rowspan="3" class="bank-account-wrapper">
                        <table class="bank-account-table" border="0">
                            <tr>
                                <td>金融機関名</td>
                                <td>三菱東京ＵＦＪ銀行（0005）</td>
                            </tr>
                            <tr>
                                <td>支店名</td>
                                <td>新宿中央支店（469）</td>
                            </tr>
                            <tr>
                                <td>口座番号</td>
                                <td>普通 5791869</td>
                            </tr>
                            <tr>
                                <td>口座名義</td>
                                <td>カ）　ゼンリンビズネクサス</td>
                            </tr>
                            <tr>
                                <td>請求締め日</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>お支払い期限日</td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><table class="total-amount-table">
                            <tr style="line-height:25px;">
                                <td style="border: 1px solid #000" class="grey-bg" align="center" width="80px">ご請求金額</td>
                                <td style="border: 1px solid #000" align="right" width="155px">0&nbsp;&nbsp;円&nbsp;&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2" style="font-size:9px; border:none">※お振込手数料はお客様のご負担でお願い申し上げます。</td>
                            </tr>
                    </table></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            
        </div>
        
    </page>
    ';

?>