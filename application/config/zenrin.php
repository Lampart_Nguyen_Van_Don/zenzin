<?php
$config['zenrin'] = '';
$config['free_action'] = array(
        'authenticate/login',
        'error/access_deny',
        'error/show_404',
        'error/uri_error',
        'authenticate/logout',
        'authenticate/change_password',
        'home/index',
        'consumption_tax/get_tax_rate',
        'holiday/check_holiday',
        'client/ajax_check_payment',
        'order/count_order_unavailable',
        'order/pdf_export',
        'account/change_password',
        'holiday/ajax_check_holiday'
//#7769:Start
//        'sales/update_payment_billing_date'
//#7769:End
);

//These action could be remember when you do not login yet.
$config['history_action'] = array(
        'sales_target/setting',
        'sales_target/report',
        'client/show',
        'order/show',
        'order_acceptance/show',
        'order_acceptance/show/type/dp',
        'order_acceptance/import',
        'sales/show',
        'invoice/show',
        'finish_work_report/show',
        'advance_payment/show',
        'stock_check_list/show',
        'order/delivery_statement',
        'reconcile_amount/import_clearance',
        'reconcile_amount/import',
        'closing_accountant/show',
        'bank_account/show',
        'product/show',
        'gui/show',
        'shipping/show',
        'subcontractor/show',
        'account/show',
        'authority/show',
        'business_unit/show',
        'department/show',
        'holiday/show',
        'account_title/show'
);
