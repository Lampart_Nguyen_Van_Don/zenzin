<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class ZR_Controller extends MX_Controller {

    public function __construct(){
        parent::__construct();
        //#6985:Start
        $this->password_change_on_the_fly();
        //#6985:End
        $this->load_head();
        bcscale(6);
    }

    //#6985:Start
    public function password_change_on_the_fly() {

//     	$_ci =& get_instance();
    	$active_module = $this->router->fetch_module();
    	$module  = config_item('modules');
    	$current_module = $module[$active_module];

    	if ($current_module['module'] != "_admin") //just apply for '_admin' only, not for 'cron' (command line)
    		return;

    	$this->load->model("account_model");
    	$auth_data = $this->session->read("_admin_authenticate");
    	$user_info = $this->account_model->get_account_by_login_id($auth_data["username"]);

    	if (trim($auth_data["user_password"]) !== trim($user_info["password"])) {
    		$this->auth->logout();
    		if (!$this->input->is_ajax_request()) {
    			redirect('_admin/authenticate/login');
    		}
    		echo json_encode(array('code' => "force_to_logout",'message' => ''));
    		exit();
    	}

    	return;

    }
    //#6985:End

    #7067:Start
    public function standadize_jcode_range($jcode_branch_cd, $fill, $max_jcode_length = 8) {

    	$return_value = null;

    	if (strlen(trim($jcode_branch_cd) == 0))
    		return $return_value;

    	$limit_length = $max_jcode_length;
    	$pos = strpos($jcode_branch_cd, "-");

    	if ($pos) {
    		$pos_portion = explode("-", $jcode_branch_cd);
    		$jcode = $pos_portion[0];
    		$branch_cd = $pos_portion[1];
    		if (strlen($jcode) <= $limit_length) {
    			$filled_string = $jcode . str_repeat($fill, $limit_length - strlen($jcode)) . "-" .  $branch_cd;
    			$return_value = $filled_string;
    		}

    	} else {
    		$jcode = $jcode_branch_cd;
    		if (strlen($jcode) <= $limit_length) {
    			$filled_string = $jcode . str_repeat($fill, $limit_length - strlen($jcode)) . "-" .  str_repeat($fill, 2);
    			$return_value = $filled_string;
    		}
    	}

    	return $return_value;
    }
    #7067:End

    public function validate($rules) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    /**
     * Assign one or more variables to view.
     *
     * @param string $key
     * @param mixed  $value
     * @return void
     */
    public function assign($key, $value = '') {
        $this->app_smarty->assign($key,$value);
    }

    /**
     * Set template file
     * @param string $template
     */
    public function view($template) {
        $form_data = array();
        if(!$form_data = $this->input->post()) {
            $form_data = $this->get_var();
        }
        $form = array();
        $this->get_from_data($form_data, $form);
        $this->assign('form', $form);
        $this->assign('this', $this);
        $this->app_smarty->display($template);
    }

    public function get_from_data(&$vars, &$retval, $escape = true) {
        foreach (array_keys($vars) as $name) {
            if (is_array($vars[$name])) {
                $retval[$name] = array();
                $this->get_from_data($vars[$name], $retval[$name], $escape);
            } else {
                $retval[$name] = $escape ? htmlspecialchars($vars[$name], ENT_QUOTES) : $vars[$name];
            }
        }
    }
    /**
     * Return param from URL
     *
     * If $decode = true, automatically decode params which get from URL
     * (use for searching feature only)
     *
     * $param string $key
     * $param bool $decode
     * $return array|string
     */
    public function get_var($key = null, $decode = false) {

        $module =  $this->router->fetch_module();
        if (empty($this->var)) {
            $int_var = 3; // $this->segment_start;

            foreach ($this->config->item('modules') as $value) {
                if ($module === $value['module']) {
                    if (isset($value['segment_start'])) {
                        $int_var = $value['segment_start'];
                    }
                    break;
                }
            }
            $this->var = $this->uri->uri_to_assoc($int_var);
        }

        if ($key == null) {
            $var = $this->var;
        } else {
            $var = (isset($this->var[$key])) ? $this->var[$key] : false;
        }

        if ($decode == true) {
            if (is_array($var)) {
                foreach ($var as $key => &$value) {
                    $value = rawurldecode($value);
                }
            } else {
                $var = rawurldecode($var);
            }
        }
        return $var;
    }

    /**
     * Create url link
     *
     * @param unknown $uri
     * @param unknown $params
     * @return Ambigous <multitype:, string>|string
     *
     */
    public function create_url($uri = '', $params = array(), $extra = '') {
        if ($uri == '') {
            $uri = $this->config->item('back_link_default');
        }

        $uriArray = explode('/', trim($uri, '/'));

        $uri = implode('/', $uriArray) . '/';
        if (!empty($params)) {
            foreach ($params as $k => $v) {
                if (empty($v)) {
                    unset($params[$k]);
                }
            }
            $uri .= $this->uri->assoc_to_uri($params);
        }

        if (preg_match('#^https?://#i', $uri)) {
            return create_url_string(rtrim($uri, '/'));
        }
        return create_url_string(rtrim($uri, '/') . $extra);
    }

    public function ajax_json($data = array()) {
        header('Content-type: application/json');
        echo json_encode($data);
        exit;
    }

    public function load_head() {
        if ($this->input->is_ajax_request()) {
            return;
        }
        $this->load->config('asset');
        $all_asset = array();
        if ($asset = config_item('asset')) {
            if (isset($asset['common'])) {
                $all_asset = $asset['common'];
            }
            if (isset($asset[$this->controller])) {
                $all_asset['css'] = array_merge($all_asset['css'], $asset[$this->controller]['css']);
                $all_asset['js'] = array_merge($all_asset['js'], $asset[$this->controller]['js']);
            }
            if (isset($asset[$this->controller][$this->router->fetch_method()])) {
                $all_asset['css'] = array_merge($all_asset['css'], $asset[$this->controller][$this->router->fetch_method()]['css']);
                $all_asset['js'] = array_merge($all_asset['js'], $asset[$this->controller][$this->router->fetch_method()]['js']);
            }
        }
        $head = '';
        if (!empty($all_asset)) {
            foreach ($all_asset['js'] as $file) {
                $head .= '<script src="' . $file . '"></script>';
            }
            foreach ($all_asset['css'] as $file) {
                $head .= '<link href="' . $file . '" rel="stylesheet">';
            }
        }
        $this->app_smarty->assign('head', $head);
    }

    public function set_message($message, $type = 'error', $is_pupup = false) {
        $msg = '';
        switch($type) {
            case 'error':
                $msg = '<div class="error-message">' . $message . '</div>';
                break;
            case 'success':
                $msg = '<div class="success-message">' . $message . '</div>';
                break;
            case 'warning':
                $msg = '<div class="warning-message">' . $message . '</div>';
                break;
        }
        $this->session->set_flashdata('message', $msg);
    }

    public function check_message() {
        if ($message = $this->session->flashdata('message')) {
            $this->assign('message', $message);
        }
    }

    public function _make_option_data($data, $column = 'id,name') {
        $option_data = array("" => lang('common_lbl_all'));
        list($key, $val) = explode(',', $column);
        foreach ($data as $k => $item) {
            if (isset($item[$key]) && isset($item[$val])) {
                $option_data[$item[$key]] = $item[$val];
            }
        }
        return $option_data;
    }
//#7427:Start Move this function from client controller to this file
    /**
     * Send mail to user of authority name, see {@link Client_model::authority()}
     *
     * @param string $column_name - quick setting column name
     * @param string $template
     * @param array $params
     *
     * @author quang_duc
     */
    protected function send_mail_by_authority($column_name, $template, $params, $account_ids = array())
    {
        $this->load->model('authority_model');

        $authority_ids = $this->authority_model->get_authority_by_quick_setting($column_name);

        if (empty($authority_ids)) {
            return;
        }

        $accounts = $this->db->select('id, mail_address')
        ->where_in('authority_id', $authority_ids)
        ->where('disable', STATUS_ENABLE)
        ->where('resignation', STATUS_ENABLE)
        ->get('account')
        ->result_array();
        $sent_address = array();

        //        $accounts = array(array('mail_address' => 'quang_duc@lampart.com.vn'));     // Change to your email when testing

        foreach ($accounts as $account) {
            if (!isset($sent_address[$account['mail_address']])) {
                if (!empty($account_ids)) {
                    if (in_array($account['id'], $account_ids)) {
                        if (!$this->email_template->send_mail($template, array('system_to' => $account['mail_address']), $params)) {
                            log_message('error', 'Cannot send mail to ' . $account['mail_address'] . ', template "' . $template . '"');
                        } else {
                            $sent_address[$account['mail_address']] = true;
                        }
                    }
                } else {
                    if (!$this->email_template->send_mail($template, array('system_to' => $account['mail_address']), $params)) {
                        log_message('error', 'Cannot send mail to ' . $account['mail_address'] . ', template "' . $template . '"');
                    } else {
                        $sent_address[$account['mail_address']] = true;
                    }
                }
            }
        }
    }
#7427:End
}