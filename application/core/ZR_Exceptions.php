<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ZR_Exceptions extends CI_Exceptions {
    public function show_404()
    {
        header("Location: /_admin/error/show_404");
        exit;
    }
}