<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class ZR_Model extends CI_Model
{
    /**
     * Pagination links
     * @var array
     */
    static $_links = array();

    /**
     * Current page number to generate pagination.
     * Set to false to turn off pagination.
     * @var false|int
     */
    protected $_use_pager = false;


    /**
     * Return single record
     * @var boolean
     */
    protected $_set_single = false;

    /**
     * Total rows number to use with pagination
     * @var int
     */
    protected $_total_rows = 0;

    /**
     * Start and set pager and set current page number.
     *
     * If page number is null, this will automatic detect page number.
     *
     * @param array $pager
     * @param int $pageNumber
     * @return self
     */
    public function set_pager($pager = array(), $page_number = null) {

        if (!is_array($pager)) {
            show_error('First parameter must be an array');
        }
        $this->load->library('pagination');

        if (!empty($pager)) {
            $this->pagination->initialize($pager);
        }

        if ($page_number != null) {
            $this->pagination->cur_page = $page_number;
        }

        $this->_use_pager = true;

        return $this;
    }

    /**
     * Retrieve paging links.
     *
     * @return html string
     */
    public function paging_links($link_index = 0) {

        return self::$_links[$link_index]['links'];
    }

    /**
     * Retrieve total rows.
     *
     * @return html string
     */
    public function total_rows($link_index = 0) {

        return self::$_links[$link_index]['total_rows'];
    }

    public function get_pagination($link_index = null) {
        if ($link_index === null) {
            $link_index = count(self::$_links) - 1;
        }

        return self::$_links[$link_index];
    }

    /**
     * Generate paging links and assign to view as "pagination"
     */
    protected function _generate_paging_links() {

        if ($this->_use_pager === false) {
            show_error('You should configure pager first by ' . get_class($this) . '::set_pager().');
        }

        // reset pager
        $this->_use_pager = false;

        // set params for paging
        $this->load->helper('form');
        $pager = array(
                'total_rows' => $this->_total_rows,
        );

        $this->pagination->initialize($pager);

        $pagination = array(
                'links'       => $this->pagination->create_links(),
                'total_rows'  => $this->_total_rows,
                'total_pages' => ceil($this->_total_rows / $this->pagination->per_page),
                'is_next_link'=> $this->pagination->is_next_link()
        );

        global $CI;
        if (count(self::$_links) < 1) {
            $CI->assign('pagination', $pagination);
        } else {
            $CI->assign('pagination_' . count(self::$_links), $pagination);
        }

        self::$_links[] = $pagination;
    }

    /**
     * Get current time of database.
     *
     * @return string
     */
    public function _db_now($date_format = null) {

        static $database_now = '';

        if ($database_now == '') {
            $database_now = $this->db->query('SELECT NOW() AS now', false)->row()->now;
        }

        return ($date_format == null) ? $database_now : date($date_format, strtotime($database_now));
    }

    /**
     * Using this function to pager when we use pure sql query.
     *
     * @param string $sql
     * @param array $list
     */
    public function exec_query($sql, $list = array()) {
        if ($this->_use_pager == true) {

            $sql = preg_replace('/SELECT/', 'SELECT SQL_CALC_FOUND_ROWS', $sql, 1);
            $sql .= ' LIMIT ?, ?';

            $limit  = $this->pagination->per_page;
            $offset = ($this->pagination->use_page_numbers == true)
                          ? ($this->pagination->cur_page - 1) * $this->pagination->per_page
                          : $this->pagination->cur_page;

            $list[] = $offset;
            $list[] = $limit;
        }
        $query = $this->db->query($sql, $list);

        if ($this->_use_pager == true) {
            $this->db->save_queries = FALSE;
            $this->_total_rows =  $this->db->query('SELECT FOUND_ROWS() AS count', false)->row()->count;
            $this->db->save_queries = TRUE;
            $this->_generate_paging_links();
        }

        if ($query->num_rows() == 0) {
            $this->_set_single = false;
            return array();
        }

        if ($this->_set_single) {
            if ($query->num_rows() > 1) {
                show_error('YOUR SET RETURN SINGLE ROW BUT THERE ARE MULTIPLE ROW');
            }
            $this->_set_single = false;
            return current($query->result_array());
        }

        return $query->result_array();
    }

    /**
     * Basic query get all items from table
     * @param string $table
     * @param array $column
     * @return boolean
     */
    public function get_items($table, $order = '', $column = array()) {
        if (!$table) {
            return FALSE;
        }
        $query = ' SELECT ' . $this->_make_select($column)
               . ' FROM ' . $table
               . ' WHERE disable = ' . STATUS_ENABLE;
           if ($order) {
               $query .= ' ORDER BY ' . $order;
           }
        return  $this->exec_query($query);
    }

    /**
     * Get item by id
     * @param string $table
     * @param array $column
     * @return array
     */
    public function get_item_by_id($table, $id, $order = '', $column = array()) {
        if (!$table || !$id) {
            $this->app_logger->error_log('GET ITEM BY ID BUT ID IS NULL.');
            return FALSE;
        }
        $query = ' SELECT ' . $this->_make_select($column)
        . ' FROM ' . $table;
        if (is_array($id)) {
            $query .= ' WHERE id IN ?';
        } else {
            $query .= ' WHERE id = ?';
        }

        $query .= ' AND disable = ' . STATUS_ENABLE;

        if ($order) {
            $query .= ' ORDER BY ' . $order;
        }

        if ($result =  $this->exec_query($query, array($id))) {
            if (is_array($id)) {
                return $result;
            } else {
                return current($result);
            }

        }
        return FALSE;
    }

    /**
     * Make condition
     * @param unknown $column
     * @return string
     */
    public function _make_select($column = array()) {
        if (is_array($column) && !empty($column)) {
            return implode(', ', $column);
        } else {
            return ' * ';
        }
    }

    /**
     * Insert a item
     * @param string $table
     * @param array $data
     */
    public function insert($table, $data) {
        $sql = 'INSERT ' . $table . ' SET ';
        $list = array();
        foreach ($data as $col => $val) {
            $sql .= $col .' = ?,';
            $list[] = $val;
        }

        $sql .= ' create_datetime = "' . $this->_db_now() .'",'
               .' lastup_datetime = "' . $this->_db_now() .'",'
               .' disable = '. STATUS_ENABLE;
        return $this->db->query($sql,$list);
    }

    /**
     * Update a item
     * @param string $table
     * @param array $data
     */
    public function update($table, $data, $condition) {

        if (empty($condition)) {
            $this->app_logger->error_log('DO NOT ALLOW TO UPDATE WHEN CONDITION IS NULL');
            return FALSE;
        }

        $list = array();

        $sql = 'UPDATE ' . $table . ' SET ';
        foreach ($data as $col => $val) {
            $sql .= $col . ' = ?,';
            $list[] = $val;
        }
        $sql .= 'lastup_datetime = "' . $this->_db_now() .'"';

        $where = array();
        if (is_array($condition)) {
            foreach ($condition as $col => $val) {
                $where[] = $col . ' = ?';
                $list[] = $val;
            }
        } else {
            $where[] = 'id = ?';
            $list[] = $condition;
        }

        $sql .= ' WHERE ' . implode(' AND ', $where);

        return $this->db->query($sql, $list);
    }

    /**
     * Perform a full update, values that only exists in $data will be inserted,
     * values that only exists in database will be deleted
     *
     * @param string $table - table name
     * @param array $data - update params
     * @param array $where - condition of rows that will be updated
     * @return array
     *
     * @author quang_duc
     */
    public function update_full($table, $data, $where)
    {
        if (empty($where)) {
            $this->app_logger->error_log('DO NOT ALLOW TO UPDATE WHEN CONDITION IS NULL');
            return FALSE;
        }

        $where += array('disable' => STATUS_ENABLE);

        if (isset($data[0])) {
            $keys = array_keys($data[0]);
        } else {
            $keys = array_keys($data);
        }

        $keys[] = 'id';

        $old_data = $this->db->select($keys)->where($where)->get($table)->result_array();

        foreach ($old_data as $o_k => $o_v) {
            foreach ($data as $k => $v) {
                $diff = array_diff_assoc($o_v, $v);
                if (count($diff) == 1 && isset($diff['id'])) {
                    unset($old_data[$o_k]);
                    unset($data[$k]);
                }
            }
        }

        $this->db->trans_start();

        $delete_params = array(
                'disable' => STATUS_DISABLE,
                'lastup_account_id' => $this->auth->get_account_id(),
                'lastup_datetime' => $this->_db_now()
        );

        $delete_ids = array_map(function ($ar) { return $ar['id']; }, $old_data);
        if (!empty($delete_ids)) {
//             $this->delete($table, array('id' => $delete_ids));
            $this->db->where_in('id', $delete_ids)->update($table, $delete_params);
        }

        if (!empty($data)) {
            reset($where);
            $where_key = key($where);
            foreach ($data as &$item) {
                $item[$where_key] = $where[$where_key];
                $item['lastup_account_id'] = $this->auth->get_account_id();
                $item['lastup_datetime'] = $this->_db_now();
                $item['create_datetime'] = $this->_db_now();
            }

            $this->db->insert_batch($table, $data);
        }

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function delete($table, $condition = array()) {
        if (empty($condition)) {
            $this->app_logger->error_log('DO NOT ALLOW TO DELETE WHEN CONDITION IS NULL');
            return FALSE;
        }
        return $this->update($table, array('disable' => STATUS_DISABLE), $condition);
    }

    /**
     * Upating multi record
     * @param string $table
     * @param array $data
     * @param string $id
     */
    public function update_batch($table, $data, $id) {
        return $this->db->update_batch($table, $data, $id);
    }

    public function set_single() {
        $this->_set_single = true;
    }

    /**
     * Validation
     * @param array $rules
     * @author van_don
     */
     public function validate($rule_name, $params = array()) {

        if (!$rule_name) {
            return TRUE;
        }
        $validate_rule = $this->rules($rule_name, $params);
        $this->load->library('form_validation');
        $this->form_validation->set_rules($validate_rule);
        return  $this->form_validation->run();
    }

    /**
     * Get insert data
     */
    public function get_insert_id() {
        return $this->db->insert_id();
    }

    public function begin_transaction() {
        $this->db->trans_begin();
    }

    public function rollback_transaction() {
        $this->db->trans_rollback();
    }

    public function commit_transaction() {
        $this->db->trans_commit();
    }

    /**
     * Make where
     *
     * @param array $conditions
     * @return string
     * @author hoang_minh
     */
    public function _make_where($conditions = array()) {
        if (is_array($conditions) && !empty($conditions)) {
            return ' WHERE ' . implode(' AND ', $conditions);
        }

        return '';
    }

    /**
     * Make orders
     *
     * @param array $orders
     * @return string
     * @author hoang_minh
     */
    public function _make_order($orders = array()) {
        if (is_array($orders) && !empty($orders)) {
            return ' ORDER BY ' . implode(', ', $orders);
        }

        return '';
    }

    /**
     * Make limits
     *
     * @param array $limits
     * @return string
     * @author hoang_minh
     */
    public function _make_limit($limits = array()) {
        if (is_array($limits) && !empty($limits)) {
            return ' LIMIT ' . implode(', ', $limits);
        }

        return '';
    }

    /**
     * query get items by parameters from table
     *
     * @param string $table
     * @param array $
     * @return boolean
     * @author hoang_minh
     */
    public function get_items_by_parameters($table, $columns = array(),
            $conditions = array(), $orders = array(), $limits = array()) {
        if (!$table) {
            return FALSE;
        }

        $query = ' SELECT ' . $this->_make_select($columns)
        . ' FROM ' . $table
        . $this->_make_where($conditions)
        . $this->_make_order($orders)
        . $this->_make_limit($limits);

        return  $this->exec_query($query);
    }

    /**
     * delete mutiple items by ids
     * @param string $table
     * @param string ids
     */
    public function delete_multiple($table, $ids) {
        if (empty($ids)) {
            $this->app_logger->error_log('DO NOT ALLOW TO UPDATE WHEN CONDITION IS NULL');
            return FALSE;
        }

        $sql = 'UPDATE '
             . $table
             . ' SET disable = ' . STATUS_DISABLE
             . '     ,lastup_datetime = "' . $this->_db_now() .'"'
             . ' WHERE id IN (' . mysql_escape_string($ids) . ')';

       return $this->db->query($sql);
    }

    public function insert_batch($table, $data) {
        return $this->db->insert_batch($table, $data);
    }
    
    
//#7067 : Start

    /**
     * get max lenght of string jcode on order table
     * @author: nhu_tham
     * @return string $n_return : max lenght of jcode
     */
    
    public function get_lenght_max_jcode_order(){
    	 
    	$sql =" select  o.j_code
				FROM  `order`  o
				order BY  convert( o.j_code,unsigned) DESC
				LIMIT 1";
    	$jcodes =  $this->exec_query($sql);
    	 
    	if(count($jcodes) > 0 ){
    
    		$n_return = strlen($jcodes[0]['j_code']);
    
    	}else{
    		$n_return = 0;
    	}
    	 
    	return $n_return;
    	 
    }
    
    /**
     * get max lenght of string branch code  on order_detail table
     * @author: nhu_tham
     * @return string $n_return : max lenght of branch code
     */
    
    public function get_length_max_branch_code(){
    	 
    	$sql =" SELECT  od.branch_cd
				FROM   order_detail as od
				ORDER BY convert( od.branch_cd  ,unsigned) DESC
				LIMIT 1";
    	 
    	$brachcodes =  $this->exec_query($sql);
    
    	if(count($brachcodes) > 0 ){
    		 
    		$n_return = strlen($brachcodes[0]['branch_cd']);
    		 
    	}else{
    		$n_return = 0;
    	}
    
    	return $n_return;
    }

    //#7067 : Start
    
    /**
     * return string with zero by number (Ex: 2-> 00)
     * @author: nhu_tham
     * @param  int $n
     * @return string $str_return
     */
    
    function   get_number_zezo_by_number($n){
    	$str_return = "";
    	for($i = 1;$i <= $n ; $i++){
    		$str_return .= "0";
    	}
    	return $str_return;
    }
    
    //#7067: End
    
    /**
     * return sql_for_branch_code_jcode_code
     * @param array  $data['from_jcode_branch'],$data['to_jcode_branch']
     * @return string where_sql
     */
    function get_where_sql_by_branchcode_and_jcode($data,$name_field_jcode,$name_field_branch_code){
    	 

    	$isFromJcodeBranch = false;
    	$isToJcodeBranch = false;
    	 
    	$n_max_jocde = $this->get_lenght_max_jcode_order();
    	$n_max_brachCode = $this->get_length_max_branch_code();
//        $where_sql = "";
    	$where_sql = array();
    	 
    	if(!empty($data['from_jcode_code'])){
    
    		$isFromJcodeBranch = true;
    		$formjcode = trim($data['from_jcode_code']);
    		if(!empty($data['from_branch_code'])){
    			$formbranchcode =  trim($data['from_branch_code']);
    		}
    
    	}
    	else{
    		 
    
    		if(isset($data['from_jcode_branch']) && strlen(trim($data['from_jcode_branch'])) > 0){
    			$isFromJcodeBranch = true;
    			$formbrandcodes =  explode("-",$data['from_jcode_branch']);

    			if(count($formbrandcodes) >1){
    					$formjcode = trim($formbrandcodes[0]);
    					$formbranchcode = trim($formbrandcodes[1]);
    			}
    			else{
    				if(count($formbrandcodes) >0){
    					$formjcode = trim($formbrandcodes[0]);
    				}
    			}
    			
    		}
    		else{
    			$formjcode = "";
    			$formbranchcode = "";
    		}
    
    	}
    	
    	if(!empty($data['to_jcode_code'])){
    
    		$isToJcodeBranch = true;
    		$tojcode = trim($data['to_jcode_code']);
    
    		if(!empty($data['to_branch_code'])){
    			 
    			$tobranchcode = trim($data['to_branch_code']);
    		}
    
    	}
    	else{
    
    		if(!empty($data['to_jcode_branch'])){
    			 
    			$isToJcodeBranch = true;
    			$to_jcode_branchs =  explode("-",$data['to_jcode_branch']);
    			if(count($to_jcode_branchs) > 1){
    				
    					$tojcode = trim($to_jcode_branchs[0]);
    					$tobranchcode = trim($to_jcode_branchs[1]);
    			}
    			else{
    				if(count($to_jcode_branchs) > 0){
    					$tojcode = trim($to_jcode_branchs[0]);
    				}
    			}
    			
    		}
    		else{
    			$tojcode = "";
    			$tobranchcode = "";
    		}
    
    	}

    	if ( $isFromJcodeBranch &&  !$isToJcodeBranch ) {
    		
    		
    		if(strlen(trim($formjcode)) > 0 && empty($formbranchcode) ){
//                $where_sql .= $name_field_jcode.' LIKE  "%'.trim($formjcode).'%"';
    			    $where_sql[] = '(' . $name_field_jcode . ' LIKE  "' . trim($formjcode) . '%")';
    		}else{
    			
    			if(!empty($formjcode) && !empty($formbranchcode) ) {
//                    $where_sql .= $name_field_jcode.' LIKE  "%'.trim($formjcode).'%"';
//                    $where_sql .= " And ".$name_field_branch_code." LIKE '%".trim($formbranchcode)."%'";
    				$where_sql[] = '(' . $name_field_jcode . ' LIKE  "' . trim($formjcode) . '%"
    				    AND ' . $name_field_branch_code . ' LIKE "' . trim($formbranchcode) . '%")';
    			} else {
                    $where_sql[] = '1 = 0';
                }
    		}

    	}
    	elseif ( !$isFromJcodeBranch && $isToJcodeBranch ) {
    
    	}
    	else{

            if (!empty($formjcode)) {
                $where_sql[] = '(CHAR_LENGTH('.$name_field_jcode.') >= CHAR_LENGTH("'.$formjcode.'") AND IF(CONCAT('.$name_field_jcode.') <> "", CONCAT('.$name_field_jcode.'), "'.$this->get_number_zezo_by_number(strlen($formjcode)).'") >= "'.$formjcode.'")';
            }
            if (!empty($formjcode) && !empty($formbranchcode)) {
            	
            	$formjcodemin = $formjcode;
            	
            	for($j = strlen($formjcode) ;$j< 8; $j++){
            	
            		if($j == 7){
            			$formjcodemin .= '1';
            		}
            		else{
            			$formjcodemin .= '0';
            		}
            	}

                $where_sql[] = '(IF  ( IF (CONCAT('.$name_field_jcode.') <> "", CONCAT('.$name_field_jcode.'), "'.$this->get_number_zezo_by_number(strlen($formjcode)).'") = RPAD("'.$formjcode.'", CHAR_LENGTH('.$name_field_jcode.'), "0") or ('.$name_field_jcode.' ="'.$formjcodemin.'")
                     , IF (CONCAT('.$name_field_branch_code.') <> "", CONCAT('.$name_field_branch_code.'), "'.$this->get_number_zezo_by_number($n_max_brachCode).'") >= "'.str_pad($formbranchcode,$n_max_brachCode,0, STR_PAD_RIGHT).'"
                     , TRUE ) )';
                
            }

            if (!empty($tojcode)) {
                $where_sql[] = '(CHAR_LENGTH('.$name_field_jcode.') >= CHAR_LENGTH("'.$tojcode.'") AND IF(CONCAT('.$name_field_jcode.') <> "", CONCAT('.$name_field_jcode.'), "'.$this->get_number_zezo_by_number(strlen($tojcode)).'") <= RPAD("'.$tojcode.'", CHAR_LENGTH('.$name_field_jcode.'), "9"))';
            }
            if (!empty($tojcode) && !empty($tobranchcode)) {
                $where_sql[] = '(IF  ( IF (CONCAT('.$name_field_jcode.') <> "", CONCAT('.$name_field_jcode.'), "'.$this->get_number_zezo_by_number(strlen($tojcode)).'") = RPAD("'.$tojcode.'", CHAR_LENGTH('.$name_field_jcode.'), 9)
                     , IF (CONCAT('.$name_field_branch_code.') <> "", CONCAT('.$name_field_branch_code.'), "'.$this->get_number_zezo_by_number($n_max_brachCode).'") <= "'.str_pad($tobranchcode,$n_max_brachCode,9, STR_PAD_RIGHT).'"
                     , TRUE ) )';
            }
    		
/*    		if(!empty($formjcode) && empty($formbranchcode) ){
    			
    
    			$where_sql .= 'if (CONCAT('.$name_field_jcode.') <> "", CONCAT('.$name_field_jcode.'), "'.$this->get_number_zezo_by_number(strlen($formjcode)).'") >="'.str_pad($formjcode,strlen($formjcode),0, STR_PAD_RIGHT).'"';

    			
    		}else{
    			
    			if(!empty($formjcode) && !empty($formbranchcode) ){
    				
    				$where_sql .=  '   if (CONCAT('.$name_field_jcode.') <> "", CONCAT('.$name_field_jcode.'), "'.$this->get_number_zezo_by_number(strlen($formjcode)).'") >= "'.str_pad($formjcode,strlen($formjcode),0, STR_PAD_RIGHT).'"';
//     				$where_sql .= '   AND if (CONCAT('.$name_field_branch_code.') <> "", CONCAT('.$name_field_branch_code.'), "'.$this->get_number_zezo_by_number($n_max_brachCode).'") >="'.str_pad($formbranchcode,$n_max_brachCode,0, STR_PAD_RIGHT).'"';
    				$where_sql .=     ' AND  IF  ( IF (CONCAT('.$name_field_jcode.') <> "", CONCAT('.$name_field_jcode.'), "'.$this->get_number_zezo_by_number(strlen($formjcode)).'") >= "'.str_pad($formjcode,strlen($formjcode),0, STR_PAD_RIGHT).'"';
    				$where_sql .=     ', IF (CONCAT('.$name_field_branch_code.') <> "", CONCAT('.$name_field_branch_code.'), "'.$this->get_number_zezo_by_number($n_max_brachCode).'") >="'.str_pad($formbranchcode,$n_max_brachCode,0, STR_PAD_RIGHT).'",';
    				$where_sql .=     ' IF (CONCAT('.$name_field_branch_code.') <> "", CONCAT('.$name_field_branch_code.'), "00") >= 0 )';
    			}
    		}
    		 
    		 
    		if(!empty($tojcode) && empty($tobranchcode) ){
    			 
    			$n_tohjodebranch  = strlen($tojcode);
    
    			for($k = $n_tohjodebranch ; $k < $n_max_jocde ; $k++){
    				$tojcode.= "9";
    			}
    			$where_sql .= ' AND if (CONCAT('.$name_field_jcode.') <> "", CONCAT('.$name_field_jcode.'), "'.$this->get_number_zezo_by_number(strlen($tojcode)).'") <="'. str_pad($tojcode,strlen($tojcode),0,STR_PAD_RIGHT).'"';
    
    
    		}else{
    			 
    			if(!empty($tojcode) && !empty($tobranchcode) ){
    

    				$n_tohjodebranch  = strlen($tojcode);
    				
    				for($k = $n_tohjodebranch ; $k < $n_max_jocde ; $k++){
    					$tojcode.= "9";
    				}
    				
    				$where_sql .= 'AND if (CONCAT('.$name_field_jcode.') <> "", CONCAT('.$name_field_jcode.'), "'.$this->get_number_zezo_by_number(strlen($tojcode)).'") <= "'. str_pad( $tojcode , strlen($n_max_jocde) ,0, STR_PAD_RIGHT).'"';
//     				$where_sql .= 'AND if (CONCAT('.$name_field_branch_code.') <> "", CONCAT('.$name_field_branch_code.'), "'.$this->get_number_zezo_by_number($n_max_brachCode).'") <= "'.str_pad($tobranchcode, $n_max_brachCode ,"0", STR_PAD_RIGHT).'"';
    				$where_sql .=     ' AND  IF  ( IF (CONCAT('.$name_field_jcode.') <> "", CONCAT('.$name_field_jcode.'), "'.$this->get_number_zezo_by_number(strlen($tojcode)).'") >= "'.str_pad($tojcode,strlen($tojcode),0, STR_PAD_RIGHT).'"';
    				$where_sql .=     ', IF (CONCAT('.$name_field_branch_code.') <> "", CONCAT('.$name_field_branch_code.'), "'.$this->get_number_zezo_by_number($n_max_brachCode).'") <="'.str_pad($tobranchcode,$n_max_brachCode,0, STR_PAD_RIGHT).'",';
    				$where_sql .=     ' IF (CONCAT('.$name_field_branch_code.') <> "", CONCAT('.$name_field_branch_code.'), "00") >= 0 )';
    				 
    			}
    		}*/
    		 
    	}

//        return $where_sql;
        return implode(' AND ', $where_sql);
       
    }
//#7067: End
    
    
    
}

?>
