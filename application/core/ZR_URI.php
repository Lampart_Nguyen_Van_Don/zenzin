<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class ZR_URI extends CI_URI {
    public function filter_uri(&$str)
    {
        if ( ! empty($str) && ! empty($this->_permitted_uri_chars) && ! preg_match('/^['.$this->_permitted_uri_chars.']+$/i'.(UTF8_ENABLED ? 'u' : ''), $str))
        {
            header("Location: /_admin/error/uri_error");
            exit;
        }
    }
}