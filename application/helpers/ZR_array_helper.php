<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Patch for PHP 5 - 5.2
 */
if (!function_exists('array_replace'))
{
    function array_replace(array $array, array $array1)
    {
        foreach($array as $k=>$v)
        {
            if(array_key_exists($k, $array1))
            {
                $array[$k] = $array1[$k];
            }
        }
        return $array;
    }
}

/**
 * Replace function for ksort($array, SORT_NATURAL)
 * because SORT_NATURAL is only available from PHP 5.4
 *
 * @param array $array
 * @param string $order - SORT_ASC or SORT_DESC
 *
 * @author kiet_minh
 */
function knatsort(&$array, $order = SORT_ASC)
{
    if ($order != SORT_DESC) {
        uksort($array, function($a, $b) {
            return strnatcmp($a, $b);
        });
    } else {
        uksort($array, function($a, $b) {
            return strnatcmp($b, $a);
        });
    }
}

/**
 * Insert an new array into an array
 *
 * @param array $array - source array
 * @param int|string $key - key from source array which will insert new array
 * @param array $data - insert array
 * @return boolean
 */
function array_insert_element(array &$array, $key, array $data)
{
    // if the key doesn't exist
    if (($offset = array_search($key, array_keys($array))) === false) {
        return false;
    }

    $array = array_merge(array_slice($array, 0, $offset + 1), $data, array_slice($array, $offset + 1));
}

/**
 * Return a new array from $array[$key], optional group by $group
 *
 * Example: $array = array(
 *          0 => array(
 *              'id'   => 'a01',
 *              'name' => 'John',
 *              'class' => '2A'
 *          ),
 *          1 => array(
 *              'id'   => 'a02',
 *              'name' => 'Peter',
 *              'class' => '2A'
 *          ),
 *          2 => array(
 *              'id'   => 'b01',
 *              'name' => 'Mark',
 *              'class' => '2B'
 *          ),
 *          3 => array(
 *              'id'   => 'b02',
 *              'name' => 'Adam',
 *              'class' => '2B'
 *          ),
 * );
 *
 * get_element($array, 'name') = array('John', 'Peter', 'Mark', 'Adam')
 *
 * get_element($array, 'name', 'class') = array(
 *          '2A' => array('John', 'Peter'),
 *          '2B' => array('Mark', 'Adam')
 * )
 *
 * @param array|object|CI_DB_Result $array - the input array
 * @param string $key - the key to extract to new array
 * @param string $group - optional, group by key name
 * @return array|object
 *
 * @author kiet_minh
 */
function get_element($array, $key, $group = null)
{
    if ($array instanceof CI_DB_result) {
        $array = $array->result();
    }

    if (empty($array)) {
        return array();
    }

    $result = array();

    if (is_object($array[0])) {
        // If index is not exist, return false
        if (!property_exists($array[0], $key)) {
            return false;
        }

        foreach ($array as $element) {
            if ($group) {
                $result[$element->$group][] = $element->$key;
            } else {
                $result[] = $element->$key;
            }
        }
    } else {
        // If index is not exist, return false
        if (!isset($array[0][$key])) {
            return false;
        }

        foreach ($array as $element) {
            if ($group) {
                $result[$element[$group]][] = $element[$key];
            } else {
                $result[] = $element[$key];
            }
        }
    }

    return $result;
}

/**
 * Create a array($from => $to) from the input $array
 *
 * Example: $array = (refer to get_element() example)
 *
 * map_element($array, 'id', 'name') = array(
 *          'a01' => 'John',
 *          'a02' => 'Peter',
 *          'b01' => 'Mark',
 *          'b02' => 'Adam'
 * )
 *
 * map_element($array, 'id', 'name', 'class') = array(
 *          '2A' => array(
 *              'a01' => 'John',
 *              'a02' => 'Peter'
 *          ),
 *          '2B' => array(
 *              'b01' => 'Mark',
 *              'b02' => 'Adam'
 *          )
 * )
 *
 * @param array|object|CI_DB_Result $array
 * @param string $from
 * @param string $to
 * @param string $group
 * @return array|object
 *
 * @author kiet_minh
 */
function map_element($array, $from, $to, $group = null)
{
    if ($array instanceof CI_DB_result) {
        $array = $array->result();
    }

    if (empty($array)) {
        return array();
    }

    $result = array();

    if (is_object($array[0])) {
        // If indexes is not exist, return false
        if (!property_exists($array[0], $from) || !property_exists($array[0], $to)) {
            return false;
        }

        foreach ($array as $element) {
            if ($group) {
                $result[$element->$group][$element->$from] = $element->$to;
            } else {
                $result[$element->$from] = $element->$to;
            }
        }
    } else {
        // If indexes is not exist, return false
        if (!isset($array[0][$from]) || !isset($array[0][$to])) {
            return false;
        }

        foreach ($array as $element) {
            if ($group) {
                $result[$element[$group]][$element[$from]] = $element[$to];
            } else {
                $result[$element[$from]] = $element[$to];
            }
        }
    }

    return $result;
}

/**
 * Create a array($key => $array[element]) from the input $array
 *
 * Example: $array = (refer to get_element() example)
 *
 * index_element($array, 'id') = array(
 *          'a01' => array(
 *              'id'   => 'a01',
 *              'name' => 'John',
 *              'class' => '2A'
 *          ),
 *          'a02' => array(
 *              'id'   => 'a02',
 *              'name' => 'Peter',
 *              'class' => '2A'
 *          ),
 *          'b01' => array(
 *              'id'   => 'b01',
 *              'name' => 'Mark',
 *              'class' => '2B'
 *          ),
 *          'b02' => array(
 *              'id'   => 'b02',
 *              'name' => 'Adam',
 *              'class' => '2B'
 *          ),
 * )
 *
 * index_element($array, 'id', 'class') = array(
 *          '2A' => array(
 *              'a01' => array(
 *                  'id'   => 'a01',
 *                  'name' => 'John',
 *                  'class' => '2A'
 *              ),
 *              'a02' => array(
 *                  'id'   => 'a02',
 *                  'name' => 'Peter',
 *                  'class' => '2A'
 *              ),
 *          ),
 *          '2B' => array(
 *              'b01' => array(
 *                  'id'   => 'b01',
 *                  'name' => 'Mark',
 *                  'class' => '2B'
 *              ),
 *              'b02' => array(
 *                  'id'   => 'b02',
 *                  'name' => 'Adam',
 *                  'class' => '2B'
 *              ),
 *          )
 * )
 *
 * @param array|object|CI_DB_Result $array
 * @param string $key
 * @param string $group
 * @return array|object
 *
 * @author kiet_minh
 */
function index_element($array, $key, $group = null)
{
    if ($array instanceof CI_DB_result) {
        $array = $array->result();
    }

    if (empty($array)) {
        return array();
    }

    $result = array();

    if (is_object($array[0])) {
        // If index is not exist, return false
        if (!property_exists($array[0], $key)) {
            return false;
        }

        foreach ($array as $element) {
            if ($group) {
                $result[$element->$group][$element->$key] = $element;
            } else {
                $result[$element->$key] = $element;
            }
        }
    } else {
        // If index is not exist, return false
        if (!isset($array[0][$key])) {
            return false;
        }

        foreach ($array as $element) {
            if ($group) {
                $result[$element[$group]][$element[$key]] = $element;
            } else {
                $result[$element[$key]] = $element;
            }
        }
    }

    return $result;
}

/**
 * @param unknown $var
 * @return boolean
 *
 * @author quang_qui
 */
function is_assoc($var)
{
    return is_array($var) && array_diff_key($var,array_keys(array_keys($var)));
}

/**
 * Similar to index_element() but this function will wrap each index_element result element
 * in a numeric key array
 *
 * @param array $array
 * @param string $key_name
 * @return multitype:unknown
 *
 * @author quang_duc
 */
function array_assoc($array, $key_name)
{
    foreach ($array as $element) {
        $result[] = array($key_name => $element);
    }
    return $result;
}

/**
 * trim array recursive
 * @param  mixed
 * @return mixed
 * 
 * @author  cam_tien
 */
function array_map_trim($input) {
    if (!is_array($input)) return trim($input);
    return array_map('array_map_trim', $input);
}

//#8273:Start
/**
 * remove non printable character such as tabs, whitespaces, newlines
 * this function does not remove 2 byte whitespace characters
 * @param  array|string $input
 * @return array|string
 */
function remove_non_printable_character($input) {
    if (!is_array($input)) return preg_replace('/\s+/S', "", $input);
    return array_map('remove_non_printable_character', $input);
}
//#8273:End