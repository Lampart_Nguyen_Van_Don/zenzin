<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Parse a date string into 'Y-m-d' format
 *
 * Ex:  2015/5 -> 2015-05
 *      2/1    -> {current_year}-02-01
 *      2/1/1  -> 2002-01-01
 *      16/1/1 -> 2016-01-01
 *
 * @param string $raw_date
 * @return string|boolean - Return false if invalid date
 *
 * @author quang_duc
 */
function parse_date($raw_date, $month_day_is_year_month = false)
{
    $format = 'Y-m-d';
    $delimiter = '-';

    if (strpos($raw_date, '/') !== false) {
        $delimiter = '/';
    }

    $parts = explode($delimiter, $raw_date);

    try {
    	if (!is_numeric($parts[0]) || (isset($parts[1]) && !is_numeric($parts[1])) || (isset($parts[2]) && !is_numeric($parts[2]))) {

    		return false;
    	}

        // Case 1: 2 parts date 'YYYY/MM' or 'MM/DD'
        if (count($parts) == 2) {

            // Case 1.1: Year/Month format

            if (!$month_day_is_year_month) {
                // Check if 1st part is a full year
                if (strlen(ltrim($parts[0], ' 0')) == 4) {
                    $year = ltrim($parts[0], ' 0');
                    $month = $parts[1];

                    if (is_numeric($year) && is_numeric($month)) {
                        // Check if 2nd part is a month
                        if ($month >= 1 && $month <= 12) {
                            return $year . '-' . sprintf('%02d', $month);
                        }
                    }

                // Case 1.2: Month/Date format

                // Use PHP DateTime library to parse
                } else {
                    if (!checkdate($parts[0], $parts[1], date('Y'))) {

                        return false;
                    }

                    $date = new DateTime(date('Y') . $delimiter . $raw_date);
                    return $date->format($format);
                }
            } else {
                if (!checkdate($parts[1], '01', $parts[0])) {

                    return false;
                }

                return date('Y-m', strtotime($parts[0] . '-' . $parts[1] . '-01'));
            }
        // Case 2: 3 parts date Y/M/D format
        } elseif (count($parts) == 3) {
            $year = ltrim($parts[0], ' 0');
            if (strlen($year) < 4) {
                $year = '2' . sprintf('%03d', $parts[0]);
            }

            if ( ! checkdate($parts[1], $parts[2], $year)) {
               return false;
            }

            // Use PHP DateTime library to parse
            $date = new DateTime($year . '-' . $parts[1] . '-' . $parts[2]);
            return $date->format($format);
        }
    } catch (Exception $e) {
        return false;
    }

    // Not any of the above case? This is not a valid date..
    return false;
}