<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function form_error($field = '', $prefix = '', $suffix = '')
{
    if (!$prefix || !$suffix) {
        $prefix = '<span class="validation-error">';
        $suffix = '</span>';
    }
    if (FALSE === ($OBJ =& _get_validation_object()))
    {
        return '';
    }

    return $OBJ->error($field, $prefix, $suffix);
}

/**
 * Generate dropdown menu.
 *
 * @param unknown_type $name
 * @param unknown_type $options
 * @param unknown_type $selected
 * @param unknown_type $extra
 * @return string
 */
function form_dropdown($data = '', $options = array(), $selected = array(), $extra = '', $prompt = false) {

    $defaults = array('name' => (( ! is_array($data)) ? $data : ''));

    if (is_array($data)) {
        if (isset($data['items'])) {
            $options = $data['items'];
            unset($data['items']);
        }
        // Deprecated: use key "items" instead of "options"
        elseif (isset($data['options'])) {
            $options = $data['options'];
            unset($data['options']);
        }
        if (isset($data['value'])) {
            $selected = $data['value'];
            unset($data['value']);
        }
        if (isset($data['prompt'])) {
            $prompt = $data['prompt'];
            unset($data['prompt']);
        }
        if (isset($data['multiple']) && ($data['multiple'] == true) ) {
            $extra .= 'multiple="multiple"';
            unset($data['multiple']);
        }
    }

    if (!is_array($selected)) {
        $selected = array($selected);
    }

    if ($extra != '') $extra = ' '.$extra;

    $form = '<select '._parse_form_attributes($data, $defaults).$extra.">\n";

    if ($prompt !== false) {
        if ($prompt === true) {
            $prompt = lang('common_dropdown_please_select');
        }
        $form .= '<option value="">'.$prompt.'</option>'."\n";
    }

    foreach ((array)$options as $key => $val) {
        $key = (string) $key;
        if (is_array($val) && ! empty($val)) {
            $form .= '<optgroup label="'.$key.'">'."\n";

            foreach ($val as $optgroup_key => $optgroup_val) {
                $sel = (in_array($optgroup_key, $selected)) ? ' selected="selected"' : '';

                $form .= '<option value="'.$optgroup_key.'"'.$sel.'>'.htmlspecialchars($optgroup_val)."</option>\n";
            }

            $form .= '</optgroup>'."\n";
        } else {
            $sel = (in_array($key, $selected)) ? ' selected="selected"' : '';

            $form .= '<option value="'.$key.'"'.$sel.'>'.htmlspecialchars($val)."</option>\n";
        }
    }

    $form .= '</select>';

    return $form;
}

function form_label($text = '', $id = '', $attributes = array()) {

    if (is_array($text)) {
        $attributes = $text;
        $text = $attributes['text'];
        unset($attributes['text']);
        if (isset($attributes['id'])) {
            $id = $attributes['id'];
            unset($attributes['id']);
        }
    }

    $label = '<label';

    if ($id != '') {
        $label .= " for=\"$id\"";
    }

    if (is_array($attributes) AND count($attributes) > 0) {
        foreach ($attributes as $key => $val) {
            $label .= ' '.$key.'="'.$val.'"';
        }
    }

    $label .= ">$text</label>";

    return $label;
}

/**
 * Make hidden field in search form
 * @param string $is_serial
 * @return string
 */
function make_search_hidden($is_serial = TRUE) {
    $hidden_field = '';
    $hidden = array();
    $CI =& get_instance();


    if (!$is_serial && $redirect_form_post = $CI->input->post('redirect_form_post')) {
        $redirect_form_post = unserialize($redirect_form_post);
        return sprintf("<div  style=\"display:none\">%s</div>", form_hidden($redirect_form_post));
    }

    if ($is_serial) {
        $CI->load->config('pagination');
        if (($posts = $CI->input->post()) && (config_item('post_prefix') != '')) {
            $length = strlen(config_item('post_prefix'));
            foreach ($posts as $key => $value) {
                if (($key == config_item('query_string_segment')) || (substr($key, 0, $length) == config_item('post_prefix'))) {
                    $hidden[$key] = $value;
                }
            }
        }
        $CI->load->library('pagination');

        $cur_page = $CI->pagination->cur_page;
        if ($cur_page && ($cur_page > 1)) {
            $hidden['pg'] = $cur_page;
        }
        $serial['redirect_form_post'] = serialize($hidden);
        return sprintf("<div  style=\"display:none\">%s</div>", form_hidden($serial));
    }
    return '';
}

function anchor_button($url = '', $content = '', $extra = '', $span = '') {

    if (is_array($url)) {
        !isset($url['content']) OR ($content = $url['content']);
        !isset($url['extra']) OR ($extra = $url['extra']);
        !isset($url['span']) OR ($span = $url['span']);

        $url = (isset($url['url'])) ? $url['url'] : '';
    }

    $anchor = anchor($url, $content, $extra);
    if ($span != '') {
        $anchor = '<span ' . $span . '>' . $anchor . '</span>';
    }

    return $anchor;
}

function _parse_form_attributes($attributes, $default, $auto_generate_id = true) {

    if (is_array($attributes)) {
        if (isset($attributes['name'])
            && ($auto_generate_id !== false)
            && (false === strpos($attributes['name'], '['))
        ) {
            $default['id'] = $attributes['name'];
        }

        foreach ($default as $key => $val) {
            if (isset($attributes[$key])) {
                $default[$key] = $attributes[$key];
                unset($attributes[$key]);
            }
        }

        if (count($attributes) > 0) {
            $default = array_merge($default, $attributes);
        }
    }

    if ((isset($default['type'])) && ($default['type'] == 'file') && isset($default['value'])) {
        unset($default['value']);
    }


    $att = '';

    foreach ($default as $key => $val) {
        if ($key == 'value') {
            $val = form_prep($val, $default['name']);
        } elseif (is_bool($val)) {
            if ($val == true) {
                $att .= $key . ' ';
            }
            continue;
        }

        $att .= $key . '="' . $val . '" ';
    }

    return $att;
}

function form_checkboxes($data, $options = array(), $value = array()) {
    if (is_array($data)) {
        if (isset($data['items'])) {
            $options = $data['items'];
            unset($data['items']);
        }
        // Deprecated: use key "items" instead of "options"
        elseif (isset($data['options'])) {
            $options = $data['options'];
            unset($data['options']);
        }
        if (isset($data['value'])) {
            $value = $data['value'];
            unset($data['value']);
        }
        if (isset($data['group_number'])) {
            $group_number = $data['group_number'];
            unset($data['group_number']);
        }
        if (isset($data['prepend'])) {
            $prepend = $data['prepend'];
            unset($data['prepend']);
        }
        if (isset($data['append'])) {
            $append = $data['append'];
            unset($data['append']);
        }
        if (isset($data['label_class'])) {
            $label_class = $data['label_class'];
            unset($data['label_class']);
        }

        if (isset($data['max_length'])) {
            $max_lenght = $data['max_length'];
            unset($data['max_length']);
        }
    } else {
        $data = array('name' => $data);
    }

    if (!is_array($value)) {
        $value = (array) $value;
    }

    $out = '';
    if (isset($group_number)) {
        $out = '<div class="display_table w_full">';
    }

    $i = 1;
    if (isset($label_class)) {
        $label_class = ' class="' . $label_class . '"';
    } else {
        $label_class = '';
    }

    if (empty($max_lenght)) {
        $max_lenght = 0;
        foreach ($options as $option => $content) {
            if ($max_lenght < strlen($content)) {
                $max_lenght = strlen($content);
            }
        }
    }

    foreach ($options as $option => $content) {
        $checkbox = $data;
        $checkbox['value'] = $option;
        if (in_array($option, $value)) {
            $checkbox['checked'] = true;
        }
        if (isset($prepend)) {
            if (is_array($prepend)) {
                if (isset($prepend[$option])) {
                    $out .= $prepend[$option];
                }
            } else {
                $out .= $prepend;
            }
        }
        if (isset($group_number)) {
            $width = ($max_lenght * 7);
            if ($width > 250) {
                $width = 250;
            }
            $out .= '<div style="width:'.$width.'px; float: left"><label' . $label_class .'>' . form_checkbox($checkbox) . htmlspecialchars($content) . '</label></div>';
        }else {
            $out .= '<label' . $label_class .'>' . form_checkbox($checkbox) . htmlspecialchars($content) . '</label>';
        }
        if (isset($append)) {
            if (is_array($append)) {
                if (isset($append[$option])) {
                    $out .= $append[$option];
                }
            } else {
                $out .= $append;
            }
        }
        if (isset($group_number)) {
            if (($i++ % $group_number) === 0) {
                $out .= '</div><div class="display_table w_full">';
            }
        }
    }

    if (isset($group_number)){
        return $out .'</div>';
    }
    return $out;
}

function form_radios($data) {

    $data['type'] = 'radio';
    return form_checkboxes($data);
}

function form_checkboxes_groups($data) {
    $result = '';
    $max_lenght = 0;
    $items = $data['items'];
    unset($data['items']);

    if (isset($data['max_length'])) {
        $max_lenght = $data['max_length'];
        unset($data['max_length']);
    }

    //Find max length text in all two dimension array.
    if (empty($max_lenght)) {
        foreach ($items as $key => $value) {
            foreach ($value as $sub_value) {
                if ($max_lenght < strlen($sub_value)) {
                    $max_lenght = strlen($sub_value);
                }
            }
        }
    }

    //Render checkbox.
    foreach ($items as $key => $value) {
        $result .= "<div style='clear:both;'><div style='font-weight:bold; font-size: 14px;'>■{$key}</div>";
        $checkboxes = $data + array('items' => $value,'max_length' => $max_lenght);
        $result .= form_checkboxes($checkboxes) .'</div><br />';
    }

    return $result;
}

function form_checkbox($data = '', $value = '', $checked = FALSE, $extra = '') {
    $defaults = array('type' => 'checkbox', 'name' => (( ! is_array($data)) ? $data : ''), 'value' => $value);

    if (is_array($data) AND array_key_exists('checked', $data)) {
        $checked = $data['checked'];

        if ($checked == FALSE) {
            unset($data['checked']);
        } else {
            $data['checked'] = 'checked';
        }
    }

    if ($checked == TRUE) {
        $defaults['checked'] = 'checked';
    } else {
        unset($defaults['checked']);
    }

    return "<input "._parse_form_attributes($data, $defaults, false).$extra." />";
}

function form_elements($data) {
    if (empty($data)) {
        return;
    }

    // if there is no type specified, then return the value of element instead of the html input
    if (!isset($data['type']) || ($data['type'] == '')) {
        if (isset($data['value'])) {
            // case of element is dropdown (multiple select and single select), radio, checkbox
            if (isset($data['items'])) {
                $array = array();
                if (!is_array($data['value'])) {
                    $data['value'] = array($data['value']);
                }

                // check if value exists in $data['items']
                foreach ($data['value'] as $value) {
                    if (isset($data['items'][$value])) {
                        $array[] = htmlspecialchars($data['items'][$value]);
                    }
                }

                return implode('<br />', $array);
            }
            // case of element is kind of text input
            else {
                $out = htmlspecialchars($data['value']);
                // Set prepent for element
                if (isset($data['prepend'])) {
                    $out = $data['prepend'] . $out;
                }

                if (isset($data['append'])) {
                    $out .= $data['append'];
                }
                return $out;
            }
        } else {
            return '';
        }
    } else {
        $type = $data['type'];
        unset($data['type']);
    }
    (isset($data['name'])) OR ($data['name'] = '');
    (isset($data['value'])) OR ($data['value'] = '');

    switch ($type) {
        case 'radio':
            $data['type'] = 'radio';
        case 'checkbox':
            return form_checkboxes($data);
        case 'dropdown':
            return form_dropdown($data);
        case 'textarea':
            unset($data['type']);
            return form_textarea($data);
        case 'upload':
            $data['type'] = 'file';
        default:
            $data['type'] = $type;
            return form_input($data);
    }
}

/**
 *
 *
 * @param array $data
 * <ul>
 *     <li>type - type of button</li>
 *     <li>content - content to display</li>
 *     <li>name - name to $_POST</li>
 *     <li>value - value to $_POST</li>
 *     <li>{attributes}</li>
 * </ul>
 * @return string
 */
function form_buttons($data) {
    if (empty($data)) {
        return;
    }

    if (isset($data['type'])) {
        $type = $data['type'];
        unset($data['type']);
    } else {
        $type = 'button';
    }
    if (isset($data['uri'])) {
        $uri = $data['uri'];
        unset($data['uri']);
    }
    if (isset($data['span'])) {
        $span = $data['span'];
        unset($data['span']);
    }
    if (isset($data['content'])) {
        $content = $data['content'];
        unset($data['content']);
    }
    if (isset($data['value'])) {
        $value = $data['value'];
        unset($data['value']);
    }
    if (isset($data['name'])) {
        $name = $data['name'];
        unset($data['name']);
    }

    switch ($type) {
        case 'anchor_popup':
            (!isset($uri)) OR ($data['uri'] = $uri);
            (!isset($content)) OR ($data['title'] = $content);
            (!isset($span)) OR ($data['span'] = $span);
            return anchor_popup($data);
        case 'anchor':
            (!isset($uri)) OR ($data['uri'] = $uri);
            (!isset($content)) OR ($data['title'] = $content);
            (!isset($span)) OR ($data['span'] = $span);
            return anchor($data);
        case 'input_submit':
            (!isset($content)) OR ($data['value'] = $content);
            (!isset($name)) OR ($data['name'] = $content);
            return form_submit($data);
        default:
            $data['type'] = $type;
            (!isset($content)) OR ($data['content'] = $content);
            (!isset($name)) OR ($data['name'] = $name);
            (!isset($value)) OR ($data['value'] = $value);
            return form_button($data);
    }
}

/**
 * Create form action button
 *
 * @param array $action_button
 * @param string $back_link (create, use)
 */
function generate_action_button($action_button, $back_link = 'create') {
    $CI =& get_instance();
    $CI->load->config('pagination');

    $action = $action_button['uri'];

    $attributes = array(
            'method' => 'post'
    );

    $hidden = array();

    if (isset($action_button['hidden'])) {
        $hidden = $action_button['hidden'];
    }

    if ($back_link == 'create') {
        $hidden['back_link'] = create_current_url();
    }

    if (($posts = $CI->input->post()) && (config_item('post_prefix') != '')) {
        $length = strlen(config_item('post_prefix'));
        $length_sort = strlen(config_item('post_prefix_sort'));

        foreach ($posts as $key => $value) {
            if (($key == config_item('query_string_segment')) || (substr($key, 0, $length) == config_item('post_prefix'))) {
                $hidden[$key] = $value;
            }
        }
    }
    $form_open = form_open($action, $attributes, $hidden);

    $button = array(
            'type'    => 'submit',
            'name'    => isset($action_button['name']) ? $action_button['name'] : '',
            'value'   => isset($action_button['value']) ? $action_button['value'] : '',
            'content' => $action_button['content'],
            'class'   => $action_button['class'],
    );

    if (isset($action_button['title'])) {
        $button['title'] = $action_button['title'];
    }

    $form_button = form_button($button);

    $form_close = form_close();

    return $form_open . $form_button . $form_close;
}

function form_add_button($url = '') {
    $CI = & get_instance();
    if (!$url) {
        $url = '/' . $CI->module . '/' . $CI->controller . '/add';
    }

    $add_button = array(
            'uri' => $url,
            'class' => 'btn-small btn-green btn-add',
            'value' => 'Add',
            'content' => lang('common_lbl_add')
    );
    return generate_action_button($add_button);
}

function form_edit_button($url = '', $hidden = array()) {
    $CI = & get_instance();
    if (!$url) {
        $url = '/' . $CI->module . '/' . $CI->controller . '/edit';
    }
    $edit_button = array(
            'uri'   => $url,
            'hidden' => $hidden,
            'content' => lang('common_lbl_edit'),
            'class' => 'btn-small btn-green btn-edit'
    );
    return generate_action_button($edit_button);
}

function form_view_button($url = '', $hidden = array()) {
    $CI = & get_instance();
    if (!$url) {
        $url = '/' . $CI->module . '/' . $CI->controller . '/edit';
    }
    $view_button = array(
            'uri'   => $url,
            'hidden' => $hidden,
            'content' => lang('common_lbl_view'),
            'name' => 'detail',
            'class' => 'btn-small btn-green btn-view'
    );
    return generate_action_button($view_button);
}

function form_sequence($url = '') {
    $CI = & get_instance();
    if (!$url) {
        $url = '/' . $CI->module . '/' . $CI->controller . '/update_sequence';
    }

    $attributes = array(
            "id" => "frm_update_sequence",
            "name" => "frm_update_sequence",
            "method"  => "post"
    );
    $form_open = form_open($url, $attributes);

    $button = array(
            'type'    => 'submit',
            'content' => lang('common_lbl_update_sequence'),
            'class'   => 'btn-small btn-green btn-update-sequence'
    );
    $form_button = form_button($button);


    $CI->load->library('pagination');

    $cur_page = $CI->pagination->cur_page;
    $hidden = array();
    if ($cur_page && ($cur_page > 1)) {
        $hidden['pg'] = $cur_page;
    }

    $form_close = form_close();
    return $form_open . $form_button . form_hidden($hidden). $form_close;
}

function form_back_button($url = '') {
    $CI = & get_instance();
    if (!$url) {
        $url = '/' . $CI->module . '/' . $CI->controller . '/edit';
    }
    $back_button = array(
            'uri'   => $url,
            'content' => lang('common_lbl_comeback'),
            'class' => 'btn-small btn-gray btn-back'
    );
    return generate_action_button($back_button);
}

function form_save_button($url = '') {
    $CI = & get_instance();
    if (!$url) {
        $url = '/' . $CI->module . '/' . $CI->controller . '/edit';
    }
    $back_button = array(
            'uri'   => $url,
            'content' => lang('common_lbl_save'),
            'class' => 'btn-small btn-green btn-save'
    );
    return generate_action_button($back_button);
}

function form_delete_button($url = '', $hidden = array()) {
    $CI = & get_instance();
    if (!$url) {
        $url = '/' . $CI->module . '/' . $CI->controller . '/delete';
    }

    $delete_button = array(
            'uri'   => $url,
            'hidden' => $hidden,
            'content' => lang('common_lbl_delete'),
            'class' => 'btn-small btn-green btn-delete'
    );

    if (isset($hidden['back_link'])) {
        return generate_action_button($delete_button, $hidden['back_link']);
    }

    return generate_action_button($delete_button);
}

/**
 * Fetch data from POST
 *
 * Ex: $data = fetch_post(array(
 *      'name',
 *      'age',
 *      'gender' => '1'    <-- default gender = 1 if it's not exist in POST
 * ));
 *
 *
 * @param array|string $index
 * @param boolean $xss_clean
 * @return array
 *
 * @author kiet_minh
 */
function fetch_post($index, $xss_clean = null)
{
    $CI =& get_instance();

    if (!is_array($index)) {
        $index = array($index);
    }

    $post_indexes = array();
    foreach ($index as $key => $value) {
        if (is_numeric($key)) {
            $post_indexes[] = $value;
            unset($index[$key]);
        } else {
            $post_indexes[] = $key;
        }
    }

    $post = array_filter($CI->input->post($post_indexes, $xss_clean), function($value) {
        return ($value !== null);
    });

    foreach ($index as $key => $value) {
        if (!isset($post[$key]) || ($post[$key] === '')) {
            $post[$key] = $value;
        }
    }

    return $post;
}
