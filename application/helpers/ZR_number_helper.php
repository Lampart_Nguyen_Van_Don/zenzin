<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Round float number in some special case
 * @link http://floating-point-gui.de
 *
 * @param $number
 * @param int $precision
 * @return string
 */
//#8243:Start
//function strip($number, $precision = 12) {
function strip($number, $precision = 4) {
//#8243:End
//    return number_format((float) $number, $precision, '.', '');
    return $number;
}

function f_ceil($number) {
    return ceil(strip($number));
}

function f_floor($number) {
    return floor(strip($number));
}