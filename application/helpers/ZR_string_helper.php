<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * add quote for if parameter given is a string
 * 
 * @param string $str
 * @author cam_tien
 */
function add_quote($str) {
    return is_string($str) ? "'" . $str . "'" : $str;
}

/**
 * Convert filename to urlencode
 * 
 * @filename string Name of file
 * @author cong_tien
 */
if (!function_exists('filename_to_urlencode')) {
    function filename_to_urlencode($filename) {
        if (isset($_SERVER['HTTP_USER_AGENT']) && (
                strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE || 
                strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/') !== FALSE || 
                strpos($_SERVER['HTTP_USER_AGENT'], 'Edge/') !== FALSE)) {
            return urlencode($filename);
        }
        
        return $filename;
    }
}