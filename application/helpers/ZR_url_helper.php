<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function anchor($uri = '', $title = '', $attributes = '', $span = '') {
    if (is_array($uri)) {
        $attributes = $uri;
        $uri = null;
        if (isset($attributes['title'])) {
            $title = $attributes['title'];
            unset($attributes['title']);
        }
        if (isset($attributes['uri'])) {
            $uri = $attributes['uri'];
            unset($attributes['uri']);
        }
        if (isset($attributes['span'])) {
            $span = $attributes['span'];
            unset($attributes['span']);
        }
        (!empty($attributes)) OR ($attributes = '');
    }
    $title = (string) $title;

    if ($uri === null) {
        $site_url = 'javascript:void(0)';
    } else {
        //$site_url = (!preg_match('!^\w+://! i', $uri) && !preg_match('/javascript:/', $uri) && ($uri[0] != '#')) ? site_url($uri) : $uri;
        $site_url = (!preg_match('!^\w+://! i', $uri) && !preg_match('/javascript:/', $uri) && ($uri[0] != '#')) ? create_url_string($uri) : $uri;
    }

    if ($title == '') {
        $title = $site_url;
    }

    if ($attributes != '') {
        $attributes = _parse_attributes($attributes);
    }

    $anchor = '<a href="'.$site_url.'"'.$attributes.'>'.$title.'</a>';

    if ($span != '') {
        $anchor = '<span ' . _parse_attributes($span) . '>' . $anchor . '</span>';
    }

    return $anchor;
}

function anchor_popup($uri = '', $title = '', $attributes = FALSE, $span = '') {
    if (is_array($uri)) {
        $attributes = $uri;
        $uri = null;
        if (isset($attributes['title'])) {
            $title = $attributes['title'];
            unset($attributes['title']);
        }
        if (isset($attributes['uri'])) {
            $uri = $attributes['uri'];
            unset($attributes['uri']);
        }
        if (isset($attributes['span'])) {
            $span = $attributes['span'];
            unset($attributes['span']);
        }
        (!empty($attributes)) OR ($attributes = false);
    }
    $title = (string) $title;

    //$site_url = ( ! preg_match('!^\w+://! i', $uri)) ? site_url($uri) : $uri;
    $site_url = ( ! preg_match('!^\w+://! i', $uri)) ? create_url_string($uri) : $uri;

    if ($title == '') {
        $title = $site_url;
    }

    if ($attributes === FALSE) {
        return "<a href='javascript:void(0);' onclick=\"window.open('".$site_url."', '_blank');\">".$title."</a>";
    }

    if ( ! is_array($attributes)) {
        $attributes = array();
    }

    foreach (array('width' => '800', 'height' => '600', 'scrollbars' => 'yes', 'status' => 'yes', 'resizable' => 'yes', 'screenx' => '0', 'screeny' => '0', ) as $key => $val) {
        $atts[$key] = ( ! isset($attributes[$key])) ? $val : $attributes[$key];
        unset($attributes[$key]);
    }

    if ($attributes != '') {
        $attributes = _parse_attributes($attributes);
    }

    $anchor = "<a href='javascript:void(0);' onclick=\"window.open('".$site_url."', '_blank', '"._parse_attributes($atts, TRUE)."');\"$attributes>".$title."</a>";

    if ($span != '') {
        $anchor = '<span ' . _parse_attributes($span) . '>' . $anchor . '</span>';
    }

    return $anchor;
}

/**
 * Create a url
 *
 * @param string|array $uri
 * @return false | string
 */
function create_url_string($uri = '') {
    if (!$uri) {
        return '/';
    }

    global $CFG;
    if ($CFG->item('enable_query_strings') == FALSE) {
        if (is_array($uri))
        {
            foreach ($uri as $key => $value) {
                if (empty($value)) {
                    unset($uri[$key]);
                }
            }

            $uri = implode('/', $uri);
        } else {
            $uri = trim($uri);
        }

        if ($uri && strpos($uri, '/') !== 0) {
            $uri = '/' . $uri;
        }
    } else {
        if (is_array($uri))
        {
            $i = 0;
            $str = '';
            foreach ($uri as $key => $val)
            {
                if (!empty($val)) {
                    $prefix = ($i == 0) ? '' : '&';
                    $str .= $prefix.$key.'='.$val;
                    $i++;
                }
            }
            $uri = $str;
        }
    }

    return $uri;
}

/**
 * Create a current url
 */
function create_current_url()
{
    $CI =& get_instance();
    return create_url_string($CI->uri->uri_string());
}

/**
 *
 * @param string $uri
 * @param boolean $back_link
 */
function redirect_post($uri, $back_link = true) {
    $CI =& get_instance();
    $CI->load->config('pagination');
    $CI->load->library('session');

    $hidden = array();
    if (($posts = $CI->input->post()) && (config_item('post_prefix') != '')) {
        $length = strlen(config_item('post_prefix'));

        foreach ($posts as $key => $value) {
            if (($key == 'pg') || (substr($key, 0, $length) == config_item('post_prefix'))) {
                $hidden[$key] = $value;
            }
        }
    }

    $CI->session->set_flashdata('redirect_post_data', $hidden);
    redirect($uri);
}


/**
 * Create new directory
 *
 * @author minh_tha
 * @param $string
 * @return string
 *
 */
function htmlreplace_url($string) {
    return preg_replace('"\b(http://\S+)"', '<a href="$1">$1</a>', $string);
}

/**
 * Redirect to an uri and sending data along.
 *
 * The data will be sent through POST.
 *
 * @param string $uri
 * @param array $data
 */
function redirect_data($uri, $data) {

    if ( ! preg_match('#^https?://#i', $uri)) {
        $uri = site_url($uri);
    }

    $form_params = array(
            'action' => $uri,
            'method' => 'POST',
            'id'     => 'redirect',
            'hidden' => $data,
    );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ja" xml:lang="ja">
<head>
</head>
<body>
<?php echo form_open($form_params) . form_close() ?>
<script type="text/javascript">
document.getElementById("redirect").submit();
</script>
</body>
</html>
<?php
    exit;
}
