<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function page_estimator($total_row, $header_footer_total_row, $header_no_footer_total_row, $body_footer_total_row, $body_no_footer_total_row) {
    if ($total_row <= $header_footer_total_row) {
        return $page_estimator = array(
            'header' => 1,
            'body'   => 0,
            'footer' => 0,
            'total'  => 1,
        );
    }

    $header_row = $total_row > $header_footer_total_row ? $header_no_footer_total_row : $header_footer_total_row;
    $total_row_left = $total_row - $header_row;
 
    $body_page = 0;
    do {
        if ($total_row_left > $body_footer_total_row) {
            $body_page++;
            $total_row_left = $total_row_left - $body_no_footer_total_row;
        }

    } while ($total_row_left > $body_footer_total_row);

    $page_estimator = array(
        'header' => 1,
        'body'   => $body_page,
        'footer' => 1,
        'total'  => 1 + $body_page + 1,
    );

    return $page_estimator;
}