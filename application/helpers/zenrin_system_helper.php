<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function update_sequence($table, $data) {
    $ci = & get_instance();
    //Load authority model
    $ci->load->model('authority_model');
    $data = $data['post_sequence'];
    $ids = array();
    foreach ($data as $id => $val) {
        if (ctype_digit($val) && ($val <= MAX_SEQUENCE_ALLOW)) {
            $ids[] = $id;
        }
    }
    if (empty($ids)) {
        return true;
    }
    //Get data before update
    if (!$old_data = $ci->authority_model->get_item_by_id($table, $ids)) {
        return true;
    }

    $update_data = array();

    foreach ($old_data as $row => $val) {
        if ($data[$val['id']] != $val['sequence']) {
            $item = array();
            $item['id'] = $val['id'];
            $item['sequence'] = $data[$val['id']];
            $item['lastup_datetime'] = $ci->authority_model->_db_now();
            $item['lastup_account_id'] = ($ci->auth->get_account_id()) ? $ci->auth->get_account_id() : 0;
            $update_data[] = $item;
        }
    }

    return $ci->authority_model->update_batch($table, $update_data, 'id');
}

/**
 * Detective credit amount
 * @param int $client_id
 * @param int $order_id
 * @param array $new_order
 * @return array
 */
function get_payment_remain($client_id, $order_id = '', $new_order = array()) {

    /**
     * Step 1
     * Calculating payment remain on grid.
     */

    $over_credit['is_over_credit'] = false;
    $over_credit['over_credit'] = array();
    if (!$new_order) {
        return $over_credit;
    }

    $ci = & get_instance();
    $ci->load->model('client_model');
    $ci->load->model('order_model');

    if ($order_id) {
        $order_info = $ci->order_model->calculating_order_detail($order_id);
        $old_total_amount = $order_info['total_amount'];
        $new_total_amount = 0;
        foreach($new_order as $order_detail) {
            /*tmp#6913: Start 2015/09/29
             if ($order_detail['delivery_date'] && $order_detail['payment_date']) {
            tmp#6913: End*/
             if ($order_detail['delivery_date']) {
                 $new_total_amount += $order_detail['amount'];
             }
        }

        if ($old_total_amount >= $new_total_amount) {
            return $over_credit;
        }
    }

    $credit_amount = 0;
    $payment_amount = 0;
    if ($client_info = $ci->client_model->get_client_by_id($client_id)) {
        $credit_amount = $client_info['credit_amount'];
    }

    /**
     * Step 2
     * Get payment past of client.
     */
//#7703:Start move this source code down
//     if ($payment_past_data = $ci->client_model->get_payment_past_data($client_id)) {
//         $payment_amount = $payment_past_data['amount'];
//     }
//#7703:End
    if ($client_info['credit_level'] == CLIENT_CREDIT_LEVEL_C) {
        $c_level_over = array();
        foreach($new_order as $order_detail) {
            /*tmp#6913: Start 2015/09/29
            if ($order_detail['delivery_date'] && $order_detail['payment_date'] ) {
            tmp#6913: End*/
            if ($order_detail['delivery_date']) {
                $payment_amount += $order_detail['amount'];
            }
            if ($payment_amount > $credit_amount) {
                $over_credit['over_credit'][$order_detail['branch']]  = $order_detail;
                $over_credit['is_over_credit'] = true;
            }
        }
        return $over_credit;
    }

//#7703:Start
    /**
     * Step 2
     * Get payment past of client.
     */
    if ($payment_past_data = $ci->client_model->get_payment_past_data($client_id)) {
        $payment_amount = $payment_past_data['amount'];
    }
//#7703:End

    //Get nearest close date
    $close_date = $ci->client_model->get_nearest_closing_date();

    /**
     * Step 3
     * Get payment amount after closing date.
     */
    if ($payment_info = $ci->client_model->get_payment_info($client_id, $close_date)) {
        $payment_amount += $payment_info['total_amount'];
    }

//#7983:Start
    $new_order = calculating_payment_dates($new_order, $client_info);
//#7983:End

    /**
     * Step 4
     * Caculating total amount on every order detail
     */
    $sub_total_amount = array();
    $delivery_date_arr = array();
    $ci->load->model('holiday_model');

    foreach($new_order as $order_detail) {
        /*tmp#6913: Start 2015/09/29
        if ($order_detail['delivery_date'] && $order_detail['payment_date'] ) {
        tmp#6913: End*/
        if ($order_detail['delivery_date']) {

            $delivery_date = new DateTime($order_detail['delivery_date']);
//#7983:Start
            $payment_date  = new DateTime($order_detail['payment_date']);
//#7983:End

            if (!isset($sub_total_amount[$delivery_date->format('Ymd')])) {
                $sub_total_amount[$delivery_date->format('Ymd')] = 0;
            }
//#7983:Start --> Comment out source code and move to calculating_payment_dates() function
//             //Calculating payment_date
//             $cnt_month = $client_info['payment_month_cd'];
//             if ($delivery_date->format('d') > $client_info['closing_date']) {
//                 $cnt_month++;
//             }

//             $cnt_month += $delivery_date->format('m') + 1;

//             $year = $delivery_date->format('Y');
//             if ($cnt_month > 12) {
//                 $year++;
//                 $cnt_month = $cnt_month - 12;
//             }

//             $str = $year . '/' . $cnt_month . '/01';
//             $temp_payment_date = new DateTime($str);

//             //Minus 1 to get last day of month
//             $temp_payment_date->modify('-1 day');

//             $temp_date = $temp_payment_date->format('Y') . '-' . $temp_payment_date->format('m') . '-' . $client_info['closing_date'];

//             if ($temp_payment_date->format('d') < $client_info['closing_date']) {
//                 $temp_date = $temp_payment_date->format('Y') . '-' . $temp_payment_date->format('m') . '-' . $temp_payment_date->format('d');
//             }

//             $payment_date = new DateTime($temp_date);
//             do {
//                 if (!$holiday = $ci->holiday_model->check_holiday($payment_date->format('Y-m-d'))) {
//                     break;
//                 }
//                 $payment_date->modify('-1 days');
//             } while (true);
//#7983:End
            //End check holiday
            $sub_total_amount[$delivery_date->format('Ymd')] += $order_detail['amount'];

            if (!isset($delivery_date_arr[$order_detail['delivery_date']])) {
 //#7983:Start
                //Caculating credit on grid
                $sub_total_amount[$delivery_date->format('Ymd')] += caculating_total($new_order, $order_detail['delivery_date'], $order_detail['payment_date']);
//#7983:End
                //Caculating total amount in case is_invoice_issue = 0 and is_ad_receive_payment = 0
                if ($order_detail_info = $ci->order_model->caculating_credit_on_order_detail($client_id, $order_detail['delivery_date'], $payment_date->format('Y-m-d') ,$order_id)) {
                    $sub_total_amount[$delivery_date->format('Ymd')] += $order_detail_info['total_amount'];
                }

                //Caculating total amount in case is_invoice_issue = 1 and is_ad_receive_payment in [0,1]
                if ($sales_slip_info = $ci->order_model->caculating_credit_on_sales_slip($client_id, $order_detail['delivery_date'], $payment_date->format('Y-m-d'), $order_id)) {
                    $sub_total_amount[$delivery_date->format('Ymd')] += $sales_slip_info['total_amount'];
                }
            }

            if (($credit_amount - $payment_amount - $sub_total_amount[$delivery_date->format('Ymd')]) < 0) {

                $over_credit['over_credit'][$order_detail['branch']] = $order_detail;
                $over_credit['is_over_credit'] = true;
            }
            $delivery_date_arr[$order_detail['delivery_date']] = $order_detail['delivery_date'];
        }
    }

    return $over_credit;
}

//#7983:Start
/**
 * Caculating credit amount on grid
 * The current item that we are checking must be
 * had delivery_date <= all payment_date in grid and not is self and delivery_date
 * must be >= payment_date
 * @param array $new_order
 * @param date $delivery_date
 * @return Integer
 */
function caculating_total($new_order, $o_delivery_date, $o_payment_date) {
    $total = 0;
    foreach($new_order as $order_detail) {

        $current_payment_date  = new DateTime($o_payment_date);
        $current_delivery_date = new DateTime($o_delivery_date);

        $payment_date  = new DateTime($order_detail['payment_date']);
        $delivery_date = new DateTime($order_detail['delivery_date']);

        if (($delivery_date->format('Ymd') <= $current_payment_date->format('Ymd'))
                && ($o_delivery_date != $order_detail['delivery_date'])
                    && ($payment_date->format('Ymd') >  $current_delivery_date->format('Ymd'))) {
            $total += $order_detail['amount'];
        }
    }

    return $total;
}

function calculating_payment_dates($new_order, $client_info) {
    if (empty($new_order)) {
        return array();
    }

    $ci = & get_instance();
    $ci->load->model('holiday_model');
    //Calculating payment date
    foreach ($new_order as &$order_detail) {
        if ($order_detail['delivery_date']) {

            $delivery_date = new DateTime($order_detail['delivery_date']);
            //Calculating payment_date
            $cnt_month = $client_info['payment_month_cd'];
            if ($delivery_date->format('d') > $client_info['closing_date']) {
                $cnt_month++;
            }

            $cnt_month += $delivery_date->format('m') + 1;

            $year = $delivery_date->format('Y');
            if ($cnt_month > 12) {
                $year++;
                $cnt_month = $cnt_month - 12;
            }

            $str = $year . '/' . $cnt_month . '/01';
            $temp_payment_date = new DateTime($str);

            //Minus 1 to get last day of month
            $temp_payment_date->modify('-1 day');

            $temp_date = $temp_payment_date->format('Y') . '-' . $temp_payment_date->format('m') . '-' . $client_info['payment_day_cd'];

            if ($client_info['payment_day_cd'] > $temp_payment_date->format('d')) {
                $temp_date = $temp_payment_date->format('Y') . '-' . $temp_payment_date->format('m') . '-' . $temp_payment_date->format('d');
            }

            $payment_date = new DateTime($temp_date);
            do {
                if (!$holiday = $ci->holiday_model->check_holiday($payment_date->format('Y-m-d'))) {
                    break;
                }
                $payment_date->modify('-1 days');
            } while (true);

            $order_detail['payment_date'] = $payment_date->format('Y-m-d');
        }
    }
    return $new_order;
}
//#7983:End


function sales_slip_export_pdf($sales_slip_id) {
    $ci = & get_instance();
    $ci->load->model('sales_slip_model');

    if (!$sales_slip = $ci->sales_slip_model->get_sales_slip_export_pdf($sales_slip_id)) {
        return array();
    }

    if ($sales_slip[0]['is_ad_receive_payment'] == 0) {
        return $sales_slip;
    }

    if (($sales_slip[0]['is_ad_receive_payment_sales'] == 1)
            || (($sales_slip[0]['is_ad_receive_payment_sales'] == 0) && ($sales_slip[0]['ad_receive_sales_slip_detail_id'] == 0))) {
        return $sales_slip;
    }

    //Get fix ids
    $sales_slip_detail_ids = array();
    foreach ($sales_slip as $key => $val) {
        $sales_slip_detail_ids[$val['ad_receive_sales_slip_detail_id']] = $val['ad_receive_sales_slip_detail_id'];
    }

    $sales_slip_detail = array();
    if ($sales_slip_received = $ci->sales_slip_model->get_sales_slip_export_pdf(0,$sales_slip_detail_ids)) {
        foreach ($sales_slip_received as $key => $val) {
            $is_first = true;
            foreach ($sales_slip as $key2 => $val2) {
                if ($val['ad_receive_sales_slip_detail_id'] == $val2['ad_receive_sales_slip_detail_id']) {
                    if ($is_first) {
                        $val['is_show_minus'] = true;
                        $sales_slip_detail[] = $val;
                    }
                    if ($val2['amount'] != 0) {
                        $sales_slip_detail[] = $val2;
                    }
                    unset($sales_slip[$key2]);
                    $is_first = false;
                }
            }
            if ($is_first) {
                $sales_slip_detail[] = $val;
            }
        }

        //Processing case part fix remain money that sales_slip.ad_receive_sales_slip_detail_id = 0
        if ($sales_slip) {
            $sales_slip_detail = array_merge($sales_slip_detail, $sales_slip);
        }

    } else {
        $sales_slip_detail = $sales_slip;
    }

//#8149:Start
    usort($sales_slip_detail, function ($a, $b) {
        return $a['sequence'] - $b['sequence'];
    });
//#8149:End

    return $sales_slip_detail;
}
