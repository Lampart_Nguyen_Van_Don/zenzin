<?php
class pre_controller {
    private $_ci = null;

    public function __construct() {
        $this->_ci = & get_instance();
        $this->_ci->module = $this->_ci->router->fetch_module();
        $this->_ci->controller = $this->_ci->router->class;
        $this->_ci->method    = $this->_ci->router->method;
    }
    public function load_lang() {

        $lang = $this->_ci->config->item('language');
        $path_common_lang = APPPATH . 'language/' . $lang . '/common_lang.php';
        if (file_exists($path_common_lang)) {
            $this->_ci->lang->load('common', $path_common_lang);
        }

        $path_sub_lang = '';
        $path_sub_lang = APPPATH . 'modules/' . $this->_ci->module .'/language/' . $lang . '/' . $this->_ci->controller . '_lang.php';
        if (file_exists($path_sub_lang)) {
            $this->_ci->lang->load($this->_ci->controller, $lang);
        }

    }

    public function do_authenticate() {
        $this->_ci->load->library('auth');
        $this->_ci->load->model('action_model');
        $module  = $this->_ci->config->item('modules');
        $current_module = $module[$this->_ci->module];

        switch ($current_module['module']) {
            case '_admin':
                if (!$this->_ci->auth->authenticate()) {
                    if ($this->_ci->controller != 'authenticate' && $this->_ci->method != 'login') {
                        $history_action = $this->_ci->config->item('history_action');
                        if (($this->_ci->method !='show_404') && in_array($this->_ci->controller . '/' . $this->_ci->method, $history_action)) {
                            $this->_ci->load->library('session');
                            $missing_url = '/' . $current_module['module'] . '/' . $this->_ci->controller . '/' . $this->_ci->method;
                            $this->_ci->session->set_userdata('missing_url', $missing_url);
                        }
                        redirect('/' . $current_module['module'] . '/authenticate/login');
                    } elseif ($this->_ci->input->is_ajax_request()) {
                        $this->_ci->app_smarty->display('error/ajax_not_logged_in.tpl');
                        exit;
                    }
                }

                if ($this->_ci->auth->is_update_password() && ($this->_ci->method != 'change_password') && ($this->_ci->method != 'logout')) {
                    redirect('/' . $current_module['module'] . '/authenticate/change_password');
                }

                if (($this->_ci->controller != 'authenticate') && ($this->_ci->method != 'login') && (!$this->_ci->auth->is_update_password())) {
                    //Get current action
//                     $current_action = $this->_ci->controller . '/' . $this->_ci->method;
                    $current_action = str_replace('_admin/', '', ($this->_ci->router->uri->uri_string != '') ? $this->_ci->router->uri->uri_string : 'home/index');
                    $free_action = $this->_ci->config->item('free_action');
                    $auth_data = $this->_ci->auth->get_userdata();

                    if (!in_array($current_action, $free_action)) {
                        if ((!$current_group = $this->_ci->action_model->get_action_by_name($current_action)) || ($auth_data['authority_id'] != $current_group['authority_id'])) {
                            if ($this->_ci->input->is_ajax_request()) {
                                $this->_ci->app_smarty->display('error/ajax_access_deny.tpl');
                                exit;
                            }
                            redirect('/' . $current_module['module'] . '/error/access_deny');
                        }
                    }

                    if (!isset($current_group)) {
                        $current_group = $this->_ci->action_model->get_action_by_name($current_module['default_controller']);
                    }

                    $breadcrumb = '';
                    if (!empty($current_group)) {
                        if($breadcrumb_data = $this->_ci->action_model->get_breadcrumb($current_group)) {
                            foreach ($breadcrumb_data as $key => $bc) {
                                $breadcrumb .= '<a href="/_admin/' . $bc['method_name'] . '" class="btn-small btn-green">' . $bc['name'] . '</a>';
                            }
                        }
                    }

                    //Set title
                    $this->_ci->app_smarty->assign('breadcrumb', $breadcrumb);
                    $this->_ci->app_smarty->assign('title', isset($current_group['name']) ? $current_group['name'] : '');
                    $this->_ci->app_smarty->assign('account_name', $auth_data['name']);

                    //Set current acction group is selected
                    $this->_ci->app_smarty->assign('current_group', $current_group);

                    //Set current module
                    $this->_ci->app_smarty->assign('module', $current_module['module']);
                }
                break;

            case 'cron':
                if ( ! $this->_ci->input->is_cli_request()) {
                    exit();
                }
                break;
        }
    }

    public function load_menu() {

        $this->_ci->load->library('auth');
        $module  = $this->_ci->config->item('modules');
        $current_module = $module[$this->_ci->module];
        $action = array();
        switch ($this->_ci->module) {
            case $module['_admin']['module']:
                if (!$this->_ci->auth->authenticate()) {
                    return;
                }
                $this->_ci->load->model('action_model');

                //Get menu group
                $menu_group = $this->_ci->action_model->get_menu_group();

                //Get action group
                $action_group = $this->_ci->action_model->get_action_group();
                $actions = $this->_ci->action_model->get_actions();

                //arrange action menu
                $menu_action_group = array();
                $action_group_name = array();
                foreach ($action_group as $key1 => $group) {
                    $action_group_name[$group['id']] = $group['name'];
                    $sequence_id = 1;
                    foreach ($actions as $key2 => $action) {
                        if ($action['action_group_id'] == $group['id']) {
                            $menu_action_group[$group['menu_group_id']][$group['id']][$sequence_id] = $action;
                            $sequence_id++;
                        }
                    }
                }

                $this->_ci->app_smarty->assign('menu_group', $menu_group);
                $this->_ci->app_smarty->assign('menu_action_group', $menu_action_group);
                $this->_ci->app_smarty->assign('action_group_name', $action_group_name);
                break;
        }
    }

    public function generate_redirect_post_data() {
        $this->_ci->load->library('session');

        if ($posts = $this->_ci->session->flashdata('redirect_post_data')) {
            $_POST += $posts;
        }
    }

    public function do_debug() {
        if (ENVIRONMENT != 'development' || $this->_ci->input->is_cli_request()) {
            return;
        }

        $ips = $this->_ci->config->item('display_profiler_ip');
        $target_ip = $_SERVER['REMOTE_ADDR'];
        $chk_prv_ip = explode('.', $target_ip);

        if (!in_array($target_ip, $ips) && ($chk_prv_ip[0] != '172') && ($chk_prv_ip[1] != '16')) {
            return;
        }

        // set profiler
        if ($this->_ci->config->item('enable_profiler') === true) {
            if (!$this->_ci->input->is_ajax_request()) {
                $this->_ci->output->enable_profiler(TRUE);
            }
            $this->_ci->load->library('profiler');
        }
    }
}