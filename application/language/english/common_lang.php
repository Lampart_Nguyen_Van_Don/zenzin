<?php
$lang["common_btn_add"] = "Add";
$lang["txt_ok"] = "OK";
$lang["txt_cancel"] = "Cancel";
$lang["txt_no_data"] = "No data";
$lang["common_lbl_sequence"] = "Sequence";
$lang["common_lbl_update_sequence"] = "Update sequence";
$lang["common_lbl_add"] = "Add";
$lang["common_lbl_edit"] = "Edit";
$lang["common_lbl_date_modified"] = "Date modified";
$lang["common_lbl_modified_by"] = "Modified by";
$lang["common_lbl_action"] = "Action";
$lang["common_lbl_logout"] = "ログアウト";
$lang["common_lbl_all"] = "All";

$lang["common_lbl_no_data"] = "No data";
$lang["common_lbl_comeback"] = "Back";
$lang["common_lbl_delete"] = "Delete";

$lang["common_lbl_search"] = "Search";
$lang["common_lbl_view"] = "View";
$lang["common_lbl_save"] = "Save";
$lang["common_lbl_close"] = "Close";
$lang["common_lbl_all"]  = "All";
$lang["common_lbl_show"]  = "Show";

$lang["common_lbl_month"] = "month";
$lang["common_lbl_months"] = "months";
$lang["common_lbl_date"]  = "date";
$lang["common_lbl_yen"]   = "yen";

//Error message
$lang["erro_login_is_incorrect"] = 'User name or password is incorrect.';
// Successful message
$lang["common_insert_successfully"] = 'Inserted successfully';
$lang["common_edit_successfully"] = 'Editted successfully';
$lang["common_copy_successfully"] = 'Copied successfully';
// Confirm delete
$lang['message_sure_to_delete']   = 'Are you sure to delete?';

$lang["common_could_not_add"] = 'Could not add. Please try again later!';
$lang["common_could_not_edit"] = 'Could not edit. Please try again later!';
$lang["common_could_not_delete"] = 'Could not delete. Please try again later!';
$lang["common_record_not_exist"] = 'This record has not been existed!';

$lang["common_please_select"] = '--Please select--';
$lang["common_select"] = '--Select--';
$lang["common_msg_access_deny"]  = "Access deny";
$lang['common_msg_not_logged_in'] = 'You are not logged in.';
$lang['common_msg_no_data'] = 'No data.';

$lang['common_msg_2byte_required'] = '%field% must be full width character.';
$lang['common_msg_invalid_date'] = '%field% is not a valid date';
$lang['common_msg_invalid_date_range'] = 'Invalid date range';
$lang['common_msg_digit_not_enough'] = 'Number of digit is not enough';
$lang['common_msg_enter_7_digit_'] = 'Enter 7 digit';
$lang['common_msg_enter_hyphen_and_num'] = '%field% please enter hyphen and number';

//#7069:Start
$lang['common_lbl_change_password'] = 'Change password';
$lang['common_lbl_cancel'] = 'Cancel';
//#7069:End

$lang['common_msg_over_record'] = '取得データが2000件を超えています。条件を変えて検索し直してください。';