<?php
/**
 * %character% : character set which is allowed
 */
$lang['allow_character'] = 'The %field% field may contain only %character%.';

/**
 * %extenstion% : file extention set which is allowed
 */
$lang['allow_file_extention'] = 'The file name in %field% field must contain only %extenstion% extensions.';

/**
 * Original.
 */
$lang['alpha'] = 'The %field% field may only contain alphabetical characters.';

/**
 * Original.
 */
$lang['alpha_dash'] = 'The %field% field may only contain alpha-numeric characters, underscores, and dashes.';

/**
 * Original.
 */
$lang['alpha_numeric'] = 'The %field% field may only contain alpha-numeric characters.';

/**
 * %min%          : minimum value.
 * %max%          : maximum value.
 * %min_operator% : operator which compare to minimum value, using [number_operator].
 * %max_operator% : operator which compare to minimum value, using [number_operator].
 */
$lang['between'] = 'The %field% must between %min% and %max%.';

/**
 * No param.
 */
$lang['captcha'] = 'The captcha code is wrong.';

/**
 * %other_field% : all other fields to compare to, display as listing forrmat.
 * %operator%    : operator which compare to datetime of other fields, using [datetime_operator].
 * %datetime%    : biggest time of other fields if operator is greater than, smallest time if less than or exactly time if equal to.
 */
$lang['datetime_compare_multiple_fields'] = 'The [%field%] field must be %operator% [%other_field%] field (%datetime%).';

/**
 * %pattern% : date, time or date and time format to check.
 */
$lang['datetime_pattern']  = 'The %field% field is invalid date or time format.';

/**
 * Original.
 */
$lang['decimal'] = 'The %field% field must contain a decimal number.';

/**
 * Original.
 */
$lang['exact_length'] = 'The %field% field must be exactly %param% characters in length.';

/**
 * Original.
 */
$lang['integer'] = 'The %field% field must contain an integer.';

/**
 * %value% : value of the current field
 */
$lang['is_exist'] = 'The %field% field not exist in database';

$lang['is_exist_one_in_multiple_field'] = 'The %field% field not exist in database';

/**
 * %fields% : all field names including current field
 */
$lang['is_exist_multiple_fields'] = '%fields% must exist.';

/**
 * Original.
 */
$lang['is_natural'] = 'The %field% field must contain only positive numbers.';

/**
 * Original.
 */
$lang['is_natural_no_zero'] = 'The %field% field must contain a number greater than zero.';

/**
 * Original.
 */
$lang['is_numeric'] = 'The %field% field must contain only numeric characters.';

/**
 * No param.
 */
$lang['in_simple_options'] = 'The %field% field option is invalid.';

/**
 * %value% : value of the current field
 */
$lang['is_unique'] = 'The %field% field must contain a unique value.';

/**
 * %fields% : all field names including current field
 */
$lang['is_unique_multiple_fields'] = '%fields% must contain unique values.';

/**
 * Original.
 */
$lang['isset'] = 'The %field% field must have a value.';

/**
 * Original.
 */
$lang['greater_than'] = 'The %field% field must contain a number greater than %param%.';

/**
 * %length%   : length to compare to.
 * %operator% : operator of comparision of length, using [length_operator].
 */
$lang['length_compare'] = 'The %field% field must be %operator% %length% characters in length.';

/**
 * Original.
 */
$lang['less_than'] = 'The %field% field must contain a number less than %param%.';

/**
 * %other_field% : field to compare to.
 */
$lang['matches'] = 'The %field% field does not match the %other_field% field.';
$lang['not_matches'] = 'The value match with the %other_field% field.';
/**
 * Original.
 */
$lang['max_length'] = 'The %field% field can not exceed %param% characters in length.';

/**
 * Original.
 */
$lang['min_length'] = 'The %field% field must be at least %param% characters in length.';

/**
 * %number%   : number to compare to.
 * %operator% : operator of comparision of number, using [number_operator].
 */
$lang['number_compare'] = 'The %field% field must contain a number %operator% %number%.';

/**
 * %other_field% : all other fields to compare to, display as listing forrmat.
 * %operator%    : operator which compare to value of other fields, using [number_operator].
 * %number%      : biggest value of other fields if operator is greater than, smallest value if less than or exactly value if equal to.
 */
$lang['number_compare_multiple_fields'] = 'The %field% field must be %operator% %other_field% field (%number%).';

/**
 * Original.
 */
$lang['numeric'] = 'The %field% field must contain only numbers.';

/**
 * %datetime% : date, time or date and time to compare to
 * %operator% : operator of comparision of date and time, using [datetime_operator].
 */
$lang['period_compare']   = 'The %field% field must be %operator% %datetime%.';
$lang['datetime_compare'] = 'The %field% field must be %operator% %datetime%.';

/**
 * %param% : phone format
 */
$lang['phone'] = 'The %field% \'s input content is defected.'.((ENVIRONMENT != 'production') ? ' Input format must be %param%':'');

/**
 * No param.
 */
$lang['phone_number'] = 'The %field% field only number and -';

/**
 * Original.
 */
$lang['regex_match'] = 'The %field% field is not in the correct format.';

/**
 * Original.
 */
$lang['required'] = 'The %field% field is required.';

/**
 * Original.
 */
$lang['valid_email'] = 'The %field% field must contain a valid email address.';
$lang['valid_password'] = 'The %field% field must contain least one uper, lower and numeric.';
/**
 * Original.
 */
$lang['valid_emails'] = 'The %field% field must contain all valid email addresses.';

/**
 * Original.
 */
$lang['valid_ip'] = 'The %field% field must contain a valid IP.';

/**
 * No param.
 */
$lang['valid_model'] = 'This address is out of service.';

/**
 * Original.
 */
$lang['valid_url'] = 'The %field% field must contain a valid URL.';


/**
 * Binding value, necessary to bind a multi-language value.
 */
$lang['number_operator'] = array(
        '<'  => 'less than',
        '<=' => 'less than or equal to',
        '='  => 'equal to',
        '>=' => 'greater than or equal to',
        '>'  => 'greater than'
);
$lang['length_operator'] = array(
        '<'  => 'at least',
        '<=' => 'shorter than or equal to',
        '='  => 'equal to',
        '>=' => 'longer than or equal to',
        '>'  => 'longer than'
);
$lang['datetime_operator'] = array(
        '<'  => 'before',
        '<=' => 'equal to or before',
        '='  => 'equal to',
        '>=' => 'equal to or after',
        '>'  => 'after'
);
$lang['listing_seperator'] = array(
        'normal' => ', ',
        'last'   => ' and '
);
$lang['character'] = array(
        'alpha'             => 'alphabet',
        'upercase_alpha'   => 'uppercase alphabet',
        'numeric'           => 'numeric',
        'katakana'          => 'Katakana',
        'hiragana'          => 'Hiragana',
        'kanji'             => 'Kanji',
        'furigana'          => 'Furigana',
        'j-alphanumeric'    => 'Japanese alphanumeric',
        'whitespace'        => 'whitespace',
        'special_character' => 'character [%character%]',

);

/* Deprecated */
$lang['after_period']              = 'The %field% field must be after %param%.';
$lang['before_period']             = 'The %field% field must be before %param%.';
$lang['date_pattern']              = 'The %field% field is invalid date format.';
$lang['hiragana']                  = 'The %field% field may only contain Hiragana characters.';
$lang['furigana']                  = 'The %field% field may only contain 1-byte Katakana characters.';
$lang['japanese']                  = 'The %field% field may only contain Japanese characters.';
$lang['alpha_extra']               = 'The %field% field may contain invalid character.';
$lang['alpha_numeric_extra']       = 'The %field% field may contain invalid character.';
$lang['in_options']                = 'The %field% field option is invalid.';
$lang['matches_date_compare']      = 'The %field% field must %operator% the %macth_field% field.';
$lang['matches_less_than']         = 'The %field% field does not greater than the %macth_field% field.';
$lang['matches_date_less_than']    = 'The %field% field does not greater than the %macth_field% field.';
$lang['matches_date_greater_than'] = 'The %field% field does not less than the %macth_field% field.';
$lang['translate_form_validate_implode'] = ', ';
$lang['katakana']                  = 'The %field% field must be katakana.';
$lang['filter_comma_chars']        = 'The %field% field may contain invalid character.';

$lang['check_address'] = 'The address is required.';
$lang['first_letter_is_a_char']           = 'The first letter must be a char.';
/**
 * check_whether_intersect_ranges
 */
$lang['check_whether_intersect_ranges'] = 'Range from %min% to %max% is exist.';

$lang['check_prefecture_name'] = 'State and zip code do not match';
$lang['compare_date'] = 'The %field% field must be %operator% %datetime%.';

$lang['only_2byte'] = '%field% only 2-byte character allow';
$lang['validate_zipcode_department'] = 'Zipcode must be numeric only';
/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */

$lang['valid_date'] = 'The %field% field is not a valid date';

$lang['in_list'] = '';

$lang['valid_full_date'] = 'The %field% field is not a valid date';
