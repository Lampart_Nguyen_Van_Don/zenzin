<?php
$lang['error_login_failed']                       = 'Login failed! Please try again';
/* History */
$lang['history_detail_update']                    = "%field% has been change from %old_value% to %new_value% ";
$lang['error_password_incorrect']                 = 'The password you entered is incorrect.';
$lang['error_user_incorrect']                     = 'The username you entered is incorrect.';
$lang['error_username_or_password_incorrect']     = 'The username or password is incorrect.';
$lang['error_fail_connect_to_business_site']      = 'Connect to business site has been failed';
$lang['error_update_has_been_failed']             = 'Update has been failed';
$lang['error_username_or_password_wrong']         = 'The username or password is incorrect.';