<?php
$lang["common_btn_add"] = "追加";
$lang["txt_ok"] = "OK";
$lang["txt_cancel"] = "Cancel";
$lang["txt_no_data"] = "No data";
$lang["common_lbl_sequence"] = "ソート順";
$lang["common_lbl_update_sequence"] = "ソート順更新";
$lang["common_lbl_add"] = "追加";
$lang["common_lbl_edit"] = "編集";
$lang["common_lbl_date_modified"] = "Date modified";
$lang["common_lbl_modified_by"] = "Modified by";
$lang["common_lbl_action"] = "処理";
$lang["common_lbl_logout"] = "ログアウト";
$lang["common_lbl_all"] = "＝＝すべて＝＝";

$lang["common_lbl_no_data"] = "データがありません。";
$lang["common_lbl_comeback"] = "一覧へ戻る";
$lang["common_lbl_delete"] = "削除";
$lang["common_lbl_undo"] = "復活";

$lang["common_lbl_search"] = "検索";
$lang["common_lbl_view"] = "参照";
$lang["common_lbl_save"] = "保存";
$lang["common_lbl_close"] = "閉じる";
$lang["common_lbl_show"] = "表示";

$lang["common_lbl_month"] = "月";
$lang["common_lbl_months"] = "ヶ月";
$lang["common_lbl_date"]  = "日";
$lang["common_lbl_yen"]   = "円";

//Error message
$lang["erro_login_is_incorrect"] = 'ログインID またはパスワードが不正です。';
// Successful message
$lang["common_insert_successfully"] = '登録が完了しました。';
$lang["common_edit_successfully"] = '更新が完了しました。';
$lang["common_copy_successfully"] = '登録が完了しました。';
$lang["common_bulk_edit_successfully"] = '選択した売上伝票の承認申請が完了しました。';
$lang["common_edit_sales_successfully"] = '売上伝票の承認申請を行いました。';
$lang["common_bulk_successfully_edit"] = '申請しました。';
$lang["common_successfully_edit"] = '保存しました。';
$lang["common_reject_successfully"] = '却下しました。';
// Confirm delete
$lang['message_sure_to_delete']   = '削除します。よろしいですか？';

$lang["common_could_not_add"] = '只今、追加できません。再度行ってください。';
$lang["common_could_not_edit"] = '只今、更新できません。再度行ってください。';
$lang["common_could_not_delete"] = '只今、削除できません。再度行ってください。';
$lang["common_record_not_exist"] = '該当データが存在していません。';

$lang["common_please_select"] = '--選択してください--';
$lang["common_select"] = '--選択--';
$lang["common_msg_access_deny"]  = "アクセスできません。";
$lang['common_msg_not_logged_in'] = '現在、ログアウトされています。';
$lang['common_msg_no_data'] = 'データがありません。';

$lang['common_msg_2byte_required'] = '%field%は全角で入力してください。';
$lang['common_msg_invalid_date'] = '%field%は無効な日付です';
$lang['common_msg_invalid_date_range'] = '検索範囲が正しくありません。';
$lang['common_msg_search_range_invalid'] = '検索範囲%field%が無効です。';
$lang['common_msg_digit_not_enough'] = '入力桁数が足りません。';
$lang['common_msg_enter_7_digit_'] = '7桁の数値で入力してください。';
$lang['common_msg_enter_hyphen_and_num'] = '%field%はハイフンと数値で入力してください。';

$lang['common_lbl_open_search'] = '検索条件を開く';
$lang['common_lbl_hide_search'] = '検索条件を隠す';

$lang['error_incorrect_search_range'] = '検索範囲が正しくありません。';
$lang['error_common_number_invalid'] = '正の整数で入力して下さい。';
$lang['lbl_address'] = '住所';
// Use $lang['title_client_detail'] for client, finish_work_report, order
$lang['title_client_detail'] = 'クライアント詳細';

// #6901 -Add -S
$lang['error-change-report-order-detail'] = '変レポ中なので入力できません';
//#6901 -Add -E

//#7069:Start
$lang['common_lbl_change_password'] = 'パスワード変更';
$lang['common_lbl_cancel'] = 'キャンセル';
//#7069:End

$lang['common_msg_over_record'] = '取得データが2000件を超えています。条件を変えて検索し直してください。';

//#9787:Start
$lang['system_error'] = 'システムエラー：管理者へお問い合わせください';
//#9787:End