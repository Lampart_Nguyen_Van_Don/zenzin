<?php
/**
 * %character% : character set which is allowed
 */
$lang['allow_character'] = '%field%は %character% しか入力できません。';

/**
 * %extenstion% : file extention set which is allowed
 */
$lang['allow_file_extention'] = '%field%にはファイル名は %extenstion% の拡張子しか処理できません。';

/**
 * Original.
 */
$lang['alpha'] = '%field%には、半角アルファベット以外は入力できません。';

/**
 * Original.
 */
$lang['alpha_dash'] = '%field%には、半角英数字、アンダースコア(_)、ハイフン(-)以外は入力できません。';

/**
 * Original.
 */
$lang['alpha_numeric'] = '%field%には、半角英数字以外は入力できません。';

/**
 * %min%          : minimum value.
 * %max%          : maximum value.
 * %min_operator% : operator which compare to minimum value, using [number_operator].
 * %max_operator% : operator which compare to minimum value, using [number_operator].
 */
//$lang['between'] = '%field%は %min% から %max% まで入力する必要があります。';
$lang['between'] = '%field% は %min% から %max% の範囲で入力して下さい。';
/**
 * No param.
 */
$lang['captcha'] = 'キャプチャコードが間違っています。';

/**
 * %other_field% : all other fields to compare to, display as listing forrmat.
 * %operator%    : operator which compare to datetime of other fields, using [datetime_operator].
 * %datetime%    : biggest time of other fields if operator is greater than, smallest time if less than or exactly time if equal to.
 */
$lang['datetime_compare_multiple_fields'] = '%field%は %other_field%(%datetime%)%operator%入力する必要があります。';

/**
 * %pattern% : date, time or date and time format to check.
 */
$lang['datetime_pattern']  = '%field%の日付が不正です';

/**
 * Original.
 */
$lang['decimal'] = '%field%は10進数しか入力できません。';

/**
 * Original.
 */
$lang['exact_length'] = '%field%は %param% 文字でなければなりません。';

/**
 * Original.
 */
$lang['integer'] = '%field%には、整数以外は入力できません。';

/**
 * %value% : value of the current field
 */
$lang['is_exist'] = '%field%はデータベースに存在していません。';

$lang['is_exist_one_in_multiple_field'] = '%field%はデータベースに存在していません。';

/**
 * %fields% : all field names including current field
 */
$lang['is_exist_multiple_fields'] = '%fields%は存在しなくてはいけません。';

/**
 * Original.
 */
$lang['is_natural'] = '%field%には、正の整数以外は入力できません。';

/**
 * Original.
 */
$lang['is_natural_no_zero'] = '%field%には、0より大きい整数以外は入力できません。';

/**
 * Original.
 */
$lang['is_numeric'] = '%field%には、数値以外は入力できません。';

/**
 * No param.
 */
$lang['in_simple_options'] = '%field%の選択肢は無効です。';

/**
 * %value% : value of the current field
 */
$lang['is_unique'] = '%field%は既に使用されています';

/**
 * %fields% : all field names including current field
 */
$lang['is_unique_multiple_fields'] = '%fields%は一意の値を入力する必要があります。';

/**
 * Original.
 */
$lang['isset'] = '%field%は空欄にできません。';

/**
 * Original.
 */
$lang['greater_than'] = '%field%は %param%より大きな数しか入力できません。';

/**
 * %length%   : length to compare to.
 * %operator% : operator of comparision of length, using [length_operator].
 */
$lang['length_compare'] = '%field%は %length%桁%operator%文字を入力する必要があります。';

/**
 * Original.
 */
$lang['less_than'] = '%field%は %param%より小さい数しか入力できません。';

/**
 * %other_field% : field to compare to.
 */
$lang['matches'] = '%field%が  %other_field%と一致しません。';
$lang['not_matches'] = 'The value match with the %other_field% field.';
/**
 * Original.
 */
$lang['max_length'] = '%field%は %param%文字を超えてはいけません。';

/**
 * Original.
 */
$lang['min_length'] = '%field%は最低 %param%文字以上でなければなりません。';

/**
 * %number%   : number to compare to.
 * %operator% : operator of comparision of number, using [number_operator].
 */
$lang['number_compare'] = '%field%は %number% %operator%数字を入力する必要があります。';

/**
 * %other_field% : all other fields to compare to, display as listing forrmat.
 * %operator%    : operator which compare to value of other fields, using [number_operator].
 * %number%      : biggest value of other fields if operator is greater than, smallest value if less than or exactly value if equal to.
 */
$lang['number_compare_multiple_fields'] = '%field%は %other_field%(%number%) %operator%数を入力する必要があります。';

/**
 * Original.
 */
$lang['numeric'] = '%field%には、数字以外は入力できません。';

/**
 * %datetime% : date, time or date and time to compare to
 * %operator% : operator of comparision of date and time, using [datetime_operator].
 */
$lang['period_compare']   = '%field%は %datetime% %operator%。';
$lang['datetime_compare'] = '%field%は %datetime% %operator%。';

/**
 * %param% : phone format
 */
$lang['phone'] = '%field%は入力内容に不備があります。'.((ENVIRONMENT != 'production') ? ' %param% の入力形式':'');

/**
 * No param.
 */
$lang['phone_number'] = '%field%は数字とハイフン(-)しか入力できません。';

/**
 * Original.
 */
$lang['regex_match'] = '%field%は、正しい形式ではありません。';

/**
 * Original.
 */
$lang['required'] = '%field%を入力して下さい。';

/**
 * Original.
 */
$lang['valid_email'] = 'メールアドレスの形式になっていません。<p>半角英数字であること、ドメインが正しい事をご確認ください。</p>';
$lang['valid_password'] = '大小英字・数字を使用してください。';
/**
 * Original.
 */
$lang['valid_emails'] = '%field%には正しいEmailアドレスを入力する必要があります。';

/**
 * Original.
 */
$lang['valid_ip'] = '%field%には正しいIPアドレスを入力する必要があります。';

/**
 * No param.
 */
$lang['valid_model'] = 'サービス提供外地域です';

/**
 * Original.
 */
$lang['valid_url'] = '%field%には正しいURLを入力する必要があります。';


/**
 * Binding value, necessary to bind a multi-language value.
 */
$lang['number_operator'] = array(
        '<'  => 'より小さい',
        '<=' => 'よりも前の日付を指定して下さい',
        '='  => 'と等しい',
        '>=' => '以上で',
        '>'  => 'より大きい'
);
$lang['length_operator'] = array(
        '<'  => 'より小さい',
        '<=' => 'よりも前の日付を指定して下さい',
        '='  => 'と等しい',
        '>=' => '以上で',
        '>'  => 'より大きい'
);
$lang['datetime_operator'] = array(
        '<'  => '以前に',
        '<=' => 'よりも前の日付を指定して下さい',
        '='  => 'と等しい',
        '>=' => '以上で',
        '>'  => '以降に'
);
$lang['listing_seperator'] = array(
        'normal' => '、',
        'last'   => '、'
);
$lang['character'] = array(
        'alpha'             => 'アルファベット',
        'upercase_alpha'   => 'アルファベット',
        'numeric'           => '数字',
        'katakana'          => 'カタカナ',
        'hiragana'          => 'ひらがな',
        'kanji'             => '漢字',
        'furigana'          => 'プリガナ',
        'j-alphanumeric'    => '日本の英数字',
        'whitespace'        => '空白',
        'special_character' => '文字「%character%」',

);

/* Deprecated */
$lang['after_period']                    = '%field%は %param% 以降に入力する必要があります。';
$lang['before_period']                   = '%field%は %param% 以前に入力する必要があります。';
$lang['date_pattern']                    = '%field%は日付形式が無効です。';
$lang['hiragana']                        = '%field%には、ひらがな以外は入力できません。';
$lang['furigana']                        = '%field%には、半角カタカナ以外は入力できません。';
$lang['japanese']                        = '%field%には、全角文字以外は入力できません。';
$lang['alpha_extra']                     = '%field%に無効な文字が含まれています。';
$lang['alpha_numeric_extra']             = '%field%に無効な文字が含まれています。';
$lang['in_options']                      = '%field% の選択肢は無効です。';
$lang['matches_date_compare']            = '%field% %operator% %macth_field%に入力する必要があります。';
$lang['matches_less_than']               = '%field%は %macth_field%より大きくてはいけません。';
$lang['matches_date_less_than']          = '%field%は %macth_field%より大きくてはいけません。';
$lang['matches_date_greater_than']       = '%field%は %macth_field%より小さくてはいけません。';
$lang['translate_form_validate_implode'] = ' - ';
$lang['katakana']                        = '%field%にカタカナ字で入力してください。';
$lang['filter_comma_chars']              = '%field%に無効な文字が含まれています。';

$lang['check_address'] = '%field%は必須項目です。';
$lang['first_letter_is_a_char'] = '先頭文字は英字で登録が必要です。';
/**
 * check_whether_intersect_ranges
 */
$lang['check_whether_intersect_ranges'] = '%min%～%max%の範囲は既に存在しています。';

$lang['check_prefecture_name'] = '郵便番号と都道府県が一致しません';
$lang['compare_date'] = '%field%は %datetime% %operator%。';

$lang['only_2byte'] = '%field%は全角で入力してください。';
$lang['validate_zipcode_department'] = '郵便番号は数値のみで入力してください。';
/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */

$lang['valid_date'] = 'The %field% field is not a valid date';

$lang['in_list'] = '';

$lang['valid_full_date'] = '%field%は日付フォーマットが無効です。';
