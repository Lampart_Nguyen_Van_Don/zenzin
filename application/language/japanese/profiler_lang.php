<?php

$lang['profiler_database']         = 'データベース';
$lang['profiler_controller_info']  = 'クラス/メソッド';
$lang['profiler_benchmarks']       = 'ベンチマーク';
$lang['profiler_queries']          = 'クエリ';
$lang['profiler_get_data']         = 'GETデータ';
$lang['profiler_post_data']        = 'POSTデータ';
$lang['profiler_uri_string']       = 'URI文字列';
$lang['profiler_memory_usage']     = 'メモリ使用量';
$lang['profiler_config']           = '設定変数';
$lang['profiler_session_data']     = 'SESSION データ';
$lang['profiler_headers']          = 'HTTP ヘッダ';
$lang['profiler_no_db']            = 'データベースドライバは現在読み込まれていません';
$lang['profiler_no_queries']       = '実行されたクエリはありません';
$lang['profiler_no_post']          = 'POSTデータはありません';
$lang['profiler_no_get']           = 'GETデータはありません';
$lang['profiler_no_uri']           = 'URIデータはありません';
$lang['profiler_no_memory']        = 'メモリ使用量を取得できません';
$lang['profiler_no_profiles']      = 'プロファイルデータがありません - すべてのプロファイラセクションは動作しません。';
$lang['profiler_section_hide']     = '隠す';
$lang['profiler_section_show']     = '表示';

// user defined
$lang['profiler_print_data']    = 'データプリント';
$lang['first_page_link']        = '&lsaquo; 最初の';
$lang['last_page_link']         = '最後 &rsaquo;';

/* End of file profiler_lang.php */
/* Location: ./system/language/english/profiler_lang.php */