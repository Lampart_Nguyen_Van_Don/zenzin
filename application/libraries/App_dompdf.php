<?php
/*
 Example:
    $this->_ci->load->library('app_dompdf');
    $dompdf = new DOMPDF();
    $dompdf->set_base_path($_SERVER['DOCUMENT_ROOT']);
    $dompdf->load_html($content); // <- HTML string goes here
    $dompdf->render();
    $dompdf->stream('work_report.pdf');

* Note: Any images or css/js file path must use relative path, ex: public/img/...
  DO NOT USE ABSOLUTE PATH like /public/img/... or it won't work

* Note 2: HTML template
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        @page {
            font-family: meriyo;
        }
    </style>
    </head>
    <body>
    </body>
</html>
 */

define('DOMPDF_ENABLE_AUTOLOAD', false);
require 'application/vendor/dompdf/dompdf/dompdf_config.inc.php';

/**
 * Class App_DOMPDF
 */
class App_DOMPDF extends DOMPDF {

}