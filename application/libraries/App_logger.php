<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @author van_don
 * @since 2015-01-22
 * @version 1.0
 */
class App_logger {

    public $log_path;
    public $_date_fmt    = 'Y-m-d H:i:s';
    private $_ci = '';

    public function __construct() {
        $this->log_path = APPPATH .'logs/';
        $this->_ci = & get_instance();
    }

    /**
     * Common function write log
     * It detect autometically the module of website
     * If there is no module assigned.
     * @param string $msg
     * @param string $module
     * @param string $log_type
     * @return boolean
     */
    public function write_log($msg, $module = '', $log_type = 'ERROR'){

        if (!$module) {
            $module = $this->_ci->module;
        }

        $controller = $this->_ci->controller;
        $method     = $this->_ci->method;
        if ($module == '_admin') {
            $login_id = $this->_ci->auth->get_username();
        } else {
            $login_id = 'cron';
        }

        if($module != ''){
            $filepath = $this->log_path . $module . '/' . $module . '-' . date('Ymd') . EXT;
        }else{
            $filepath = $this->log_path . 'log-' . date('Ymd') . EXT;
        }

        $message  = "";

        if(! file_exists($filepath)){
            $message .= "<"."?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?".">\n\n";
        }

        if(! $fp = @fopen($filepath, FOPEN_WRITE_CREATE)){
            return FALSE;
        }

        $message .= date($this->_date_fmt) . ' --> ['. $log_type . ']: ' . $msg . "\n";

        $message .= ($login_id == '') ? "*****Info: \n" : "*****Info: \nUsername: " . $login_id;
        $message .= " Action: /" . $module . "/" .$controller . "/" . $method . "\n\n";
        flock($fp, LOCK_EX);
        fwrite($fp, $message);
        flock($fp, LOCK_UN);
        fclose($fp);

        @chmod($filepath, FILE_WRITE_MODE);
        return TRUE;
    }

    /**
     * Write error log use only
     * @param unknown $message
     * @param string $module
     */
    public function error_log($msg, $is_send_mail = FALSE, $module = '') {
        $this->write_log($msg, $module, 'ERROR');
        if ($is_send_mail === TRUE) {
           $this->_error_mail($msg);
        }
    }

    /**
     * Write info log use only
     * @param unknown $message
     * @param string $module
     */
    public function info_log($msg, $module = '') {
        $this->write_log($msg, $module, 'INFO');
    }

    /**
     * Write warning log use only
     * @param unknown $message
     * @param string $module
     */
    public function warning_log($msg, $module = '') {
        $this->write_log($msg, $module, 'WARN');
    }
    /**
     * Send mail when the system error
     * @param unknown $error_message
     */
    private function _error_mail($error_message) {

        $this->_ci->load->library('email');
        $system_mail_address = config_item('from_mail_address');
        $monitoring_email = config_item('monitoring_mail_address');

        foreach ($monitoring_email as $mail) {

            $this->_ci->email->from($system_mail_address['email'], $system_mail_address['name']);
            $this->_ci->email->to($mail);
            $this->_ci->email->subject('Error system');
            $this->_ci->email->message($error_message);

            $this->_ci->email->send();
        }
    }

	#7175:Start
    public function cron_log($message, $file_suffix) {

    	$this->write_cron_log($message, $file_suffix);

    }

    public function write_cron_log($message, $file_suffix, $module = 'cron') {

    	$message .= "\n";

    	$filepath = $this->log_path . $module . '/' . date('Y_m_d') . "_" . $file_suffix . '.log';
    	if (!$fp = @fopen($filepath, FOPEN_WRITE_CREATE))
    		return false;

    	flock($fp, LOCK_EX);
    	fwrite($fp, $message);
    	flock($fp, LOCK_UN);
    	fclose($fp);
    	@chmod($filepath, FILE_WRITE_MODE);

    	return true;
    }

    #7175:End
}