<?php
/**
 * @author van_don
 * @since 2015-01-22
 * @version 1.0
 */
require_once(APPPATH.'libraries/smarty/Smarty.class.php');
class App_smarty extends Smarty {
    private $_ci = null;
    public function __construct() {
        parent::__construct();
        $this->_ci = & get_instance();
        $module = $this->_ci->router->module;
        $templates = APPPATH . 'modules/' . $module . '/views';
        $templates_c = APPPATH.'templates_c/' . $module;
        
        $this->setTemplateDir($templates);
        $this->setCompileDir($templates_c);
        $this->setConfigDir(APPPATH.'libraries/smarty/configs');
        $this->setCacheDir(APPPATH.'cache');
        
        $this->assign( 'APPPATH', APPPATH );
        $this->assign( 'BASEPATH', BASEPATH );
        $this->force_compile = 1;
        $this->caching = true;
        $this->cache_lifetime = 120;
    }   
}