<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Auth
{
    /**
     * @var ZR_Controller
     */
    protected $_ci;

    protected $_messages = array();

    protected $_namespace = '_authenticate';

    protected $_table = '';
    protected $_usernamecol = '';
    protected $_passwordcol = '';
    protected $_isuseadmincol = '';
    protected $_isuseacquisitioncol = '';
    protected $_db_fields = '';
    protected $_db_where = '';
    protected $_check_component = '';

    protected $_crypt_type = '';
    protected $_use_encrypt_key = false;
    protected $_use_encrypt_sql = true;

    protected $auth_data = array();

    protected $is_logged = false;
    protected $message = '';
    protected $is_update_password = false;
    protected $_business_unit;

    /**
     * Constructor - Sets Preferences
     *
     * The constructor can be passed an array of config values
     */
    public function __construct($config = array()) {
        $this->_ci =& get_instance();

        if (!empty($config)) {
            $this->initialize($config);
        }

        if (($auth_data = $this->_ci->session->read($this->_namespace)) AND isset($auth_data['logged_in'])) {
            $this->auth_data = $auth_data;
            $this->is_logged = true;
            $this->is_update_password = $auth_data['is_update_password'];

            $this->_ci->session->update($this->_namespace);
        }
        log_message('debug', 'Auth class Initialized');
    }

    protected function _save() {
        $this->_ci->session->write($this->_namespace, $this->auth_data);
    }

    public function authenticate() {
        return $this->is_logged;
    }

    public function is_update_password() {
        return $this->is_update_password;
    }
    public function set_logged_status ($status = false) {
        $this->is_logged = $status;
    }
    public function get_account_id() {
        return (isset($this->auth_data['account_id'])) ? $this->auth_data['account_id'] : 0;
    }

    public function get_username() {
        return $this->auth_data['username'];
    }

    public function set_userdata ($data) {
        $this->auth_data['userdata'] = $data;
        $this->_save();
    }

    public function get_userdata($field = null) {
        if ($this->is_logged !== true) {
            return false;
        }
        if ($field == null) {
            return $this->auth_data['userdata'];
        }

        return (isset($this->auth_data['userdata'][$field])) ? $this->auth_data['userdata'][$field] : false;
    }

    public function refresh_userdata() {
        $this->_ci->db->from($this->_table)->where(array('id' => $this->get_account_id()));
        if ($this->_db_where) {
            $this->_ci->db->where($this->_db_where);
        }
        $user = $this->_ci->db->get()->row();
        if (empty($user)) {
            $this->logout();
        }

        $user_data = array();
        foreach ($this->_db_fields as $field) {
            $user_data[$field] = $user->$field;
        }

        $this->auth_data['userdata'] = $user_data;

        $this->_save();
    }

    /**
     * Initialize preferences
     *
     * @access    public
     * @param    array
     * @return    void
     */
    public function initialize($config = array()) {
        foreach ($config as $key => $val) {
            if (isset($this->{'_'.$key})) $this->{'_'.$key} = $val;
        }
    }

     /**
      * Process login
      *
      * @param string $login_id
      * @param string $password
      * @return boolean
      */
    public function login($login_id, $password, $active = false) {
        $this->logout();
        if ($active == true) {
            $where_array = array(
                                   $this->_usernamecol => $login_id,
                                   $this->_passwordcol => md5($password),
                                   'active'            => 1
            );
        } else {
            $where_array = array(
                    $this->_usernamecol => $login_id,
                    $this->_passwordcol => md5($password)
            );
        }

        $this->_ci->load->model('account_model');
        $this->_ci->db->from($this->_table)->where($where_array);
        if ($this->_db_where) {
            $this->_ci->db->where($this->_db_where);
        }
        $user = $this->_ci->db->get()->row();

        // check login_id and password is correct.
        if (empty($user)) {
            $this->message = $this->_messages['default'];
            return false;
        }

        $user_data = array();
        foreach ($this->_db_fields as $field) {
            $user_data[$field] = $user->$field;
        }
        $this->is_update_password = false;
        if(!$update_password = $this->_ci->account_model->check_update_password($user->id)) {
            $this->is_update_password = true;
        }
        $encrypted_password = md5($password);
        $this->auth_data = array(
                'account_id'     => $user->id,
                'username'       => $login_id,
                'is_update_password' => $this->is_update_password,
                'mail_address'   => isset($user->mail_address) ? $user->mail_address : '',
                'logged_in'      => true,
                'login_time'     => $this->_ci->account_model->_db_now(),
                'userdata'         => $user_data
        		,'user_password'  => $encrypted_password
        );

        $this->_save();
        $this->is_logged = true;

        return true;
    }

    public function logout() {
        $this->_ci->session->destroy($this->_namespace);
        return true;
    }

    public function get_message() {
        return $this->message;
    }

    /**
     * Create logs file for login: admin,_staff,mypage
     * @access    public
     */

    public function create_log_login($message){

        $_log_path = APPPATH .'logs/login_log-' . date('Y-m-d') . '.log';
        if(!file_exists($_log_path)){
            $message .= "";
        }
        $fp = @fopen($_log_path, FOPEN_WRITE_CREATE);
        if($fp !== FALSE){

            flock($fp, LOCK_EX);
            fwrite($fp, $message);
            flock($fp, LOCK_UN);
            fclose($fp);

            @chmod($_log_path, FILE_WRITE_MODE);
        }

    }

    public function get_auth_data () {
        return $this->auth_data;
    }

    public function reset_auth_data ($auth_data) {
        $this->auth_data = $auth_data;
        $this->is_logged = true;
        $this->_save();
    }

    /**
     * Check current login account has authority to access an action
     *
     * @param string $name - action METHOD name or quick_setting COLUMN name
     * @param string $name_type - 'method' or 'column', default is 'method'
     * @return boolean
     *
     * @author quang_duc
     */
    public function has_permission($name, $name_type = 'method') {

        $this->_ci->load->model('authority_model');

        return $this->_ci->authority_model->check_permission($this->auth_data['userdata']['authority_id'], $name, $name_type);
    }

    /**
     * get current account business unit
     *
     * @return array
     * @author  cam_tien
     */
    public function get_business_unit() {
        $this->_ci->load->model('account_model');
        if (!$this->_business_unit) {
            $this->_business_unit = $this->_ci->account_model->get_account_business_unit($this->get_account_id());
        }

        return $this->_business_unit;
    }
}

?>
