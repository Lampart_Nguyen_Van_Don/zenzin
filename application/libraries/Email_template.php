<?php
class Email_template {

    private $_mail_list = array();
    private $_ci;
    private $_data = array();

    /**
     * Constructor
     */
    public function __construct($config = array()) {
        $this->_ci =& get_instance();

        if ( ! empty($config)) {
            $this->initialize($config);
        }

        $this->_ci->load->library('email');
        log_message('debug', 'Mail template class Initialized');
    }

    /**
     * Insert content into the template mail
     *
     * Arguments description
     *   $template = 'template_name' template_name initialized in folder config/email_template.php
     *
     * $data_mail = array(
     *   'from'    => 'info@mobtest.mobi',
     *   'to'      => 'example1@mobtest.mobi',
     *   'cc'      => 'example2@mobtest.mobi',
     *   'bcc'     => 'example3@mobtest.mobi',
     * );
     *
     * $replace_params: identical with file template mail in config/email_template/template.php
     *
     * @access public
     * @param  string $template
     * @param  array $data_mail
     * @param  array $replace_params [opt]
     * @return boolean
     */

    public function send_mail($template, $data_mail = array(), $replace_params = array()) {
        if (!key_exists($template, $this->_mail_list)) {
            return false;
        }

        if (!isset($this->_data[$template])) {
            if (!$template_path = config_item('email_template_path')) {
                return false;
            }
            $template_path = $template_path.config_item('language').'/'.$this->_mail_list[$template];
            if (file_exists($template_path)) {
                include $template_path;
                $this->_data[$template] = array(
                        'mail'         => $mail,
                        'replace_list' => $replace_list
                );
            } else {
                $this->_data[$template] = false;
            }
        }

        if ($this->_data[$template] == false) {
            return false;
        }

        $mail         = $this->_data[$template]['mail'];
        $replace_list = $this->_data[$template]['replace_list'];

        $replaces = array();
        foreach ($replace_list as $key => $replace) {
            if (isset($replace_params[$key])) {
                $replaces['{'.$key.'}'] = $replace_params[$key];
            } else {
                $replaces['{'.$key.'}'] = '';
            }
        }

        $is_success = true;
        if (($mail['customer_subject'] != '') && ($mail['customer_body'] != '') && (!isset($data_mail['disable_customer_mail']) || ($data_mail['disable_customer_mail'] !== true))) {

            if (isset($data_mail['to']) && ($data_mail['to'] != '')) {
                $to      = $data_mail['to'];
                $subject = str_replace (array_keys($replaces), array_values($replaces), $mail['customer_subject']);
                $body    = $this->replace_mail_body(str_replace (array_keys($replaces), array_values($replaces), $mail['customer_body']));

                if (!isset($data_mail['from']) && isset($mail['customer_from'])) {
                    $data_mail['from'] = $mail['customer_from'];
                }

                if (!$this->_send_mail($to, $subject, $body, $data_mail)) {
                    $is_success = false;
                    log_message('error',  'Fail to send customer email.');
                }
            } else {
                $is_success = false;
            }
        }

        if (($mail['system_subject'] != '') && ($mail['system_body'] != '') && (!isset($data_mail['disable_system_mail']) || ($data_mail['disable_system_mail'] !== true))) {
            if (isset($data_mail['system_to'])) {
                $to = $data_mail['system_to'];
            } elseif (isset($mail['system_to'])) {
                $to = $mail['system_to'];
            } elseif ($config_mail = config_item('customer_mail_address')) {
                $to = $config_mail;
            } else {
                log_message('error', 'Config "customer_mail_address" is not specified.');
            }
//             $to = ((isset($mail['system_to'])) && ($mail['system_to'] != '')) ? $mail['system_to'] : config_item('customer_mail_address');


            if ($to) {
                $subject = str_replace (array_keys($replaces), array_values($replaces), $mail['system_subject']);
                $body    = $this->replace_mail_body(str_replace (array_keys($replaces), array_values($replaces), $mail['system_body']));

                $system_mail_extra = array();
                (!isset($mail['system_from'])) OR ($system_mail_extra['from'] = $mail['system_from']);
                (!isset($mail['system_cc'])) OR ($system_mail_extra['cc'] = $mail['system_cc']);
                (!isset($mail['system_bcc'])) OR ($system_mail_extra['bcc'] = $mail['system_bcc']);

                if (!$this->_send_mail($to, $subject, $body, $system_mail_extra)) {
                    $is_success = false;
                    log_message('error',  'Fail to send system email.');
                }
            } else {
                $is_success = false;
            }
        }

        return $is_success;
    }

    /**
     * Error email for monitoring
     *
     * @param string $subject
     * @param string $body
     * @author Quoc Huy
     */
    public function error_email($subject, $body) {

        if (!$recipients_address = config_item('monitoring_mail_address')) {
            log_message('error', 'Config "monitoring_mail_address" is not specified.');
            return false;
        }

        $mail_extra = array();

        $to = array_shift($recipients_address);
        if (count($recipients_address) > 0) {
            $mail_extra['cc'] = $recipients_address;
        }

        $this->_ci->email->subject($subject);
        $this->_ci->email->message($body);

        if (!$this->_send_mail($to, $subject, $body, $mail_extra)) {
            log_message('error',  'Fail to send error email.');
            return false;
        }

        return true;
    }

    /**
     * @access  protected
     * @param   String
     * @return  boolean
     */
    protected function _send_mail($to, $subject, $body, $extra = null) {

        if (!is_array($to)) {
            $this->_ci->email->to($to);
        } elseif (isset($to['email'])) {
            $this->_ci->email->to($to['email']);
        } else {
            log_message('error', "No set valid email or name to is empty.");
        }

        $this->_ci->email->subject($subject);
        $this->_ci->email->message($body);

        if (isset($extra['from']) && ($extra['from'] != null)) {
            $from = $extra['from'];
        } else {
            if (!$from = config_item('from_mail_address')) {
                log_message('error', 'Config "from_mail_address" is not specified.');
                return false;
            }
        }

        if (!is_array($from)) {
            $this->_ci->email->from($from);
        }
        elseif (isset($from['email']) && isset($from['name']) && $this->_ci->email->valid_email($from['email'])) {
            $this->_ci->email->from($from['email'], $from['name']);
        }
        else {
            log_message('error', "No set valid email or name from is empty.");
            return false;
        }

        if (isset($extra['cc']) && ($extra['cc'] != null)) {
            $this->_ci->email->cc($extra['cc']);
        }
        if (isset($extra['bcc']) && ($extra['bcc'] != null)) {
            $this->_ci->email->bcc($extra['bcc']);
        }

        return $this->_ci->email->send();
    }

    /**
     * Initialize preferences
     *
     * @access  public
     * @param   array
     * @return  void
     */
    public function initialize($config = array()) {
        // process each key
        $this->_mail_list = $config['mail_list'];
    }

    public function replace_mail_body($body)
    {
        $body = explode("\n", $body);
        $replay_array = array();
        foreach ($body as $key => $value){
            if ($value != null) {
                if (str_replace(" ", "", $value) != null) {
                    $replay_array[] = $value;
                }
            }else{
                $replay_array[] = $value;
            }
        }
        return rtrim(str_replace("&np;", "\n", implode("\n",$replay_array)));
    }

    /**
     * send email with attachment
     *
     * @param array $params
     * @return boolean
     * @author Quoc Huy
     */
    function send_attachment_mail($params) {

        $this->_ci->email->clear(TRUE);

        if (!$from_address = config_item('from_mail_address')) {
            log_message('error', 'Config "from_mail_address" is not specified.');
            return false;
        }

        if (!$customer_address = config_item('customer_mail_address')) {
            log_message('error', 'Config "customer_mail_address" is not specified.');
            return false;
        }

        if(isset($params['from_name'])){
            $from_name = $params['from_name'];
        }else{
            $from_name = $from_address['name'];
        }

        if(isset($params['from_mail'])){
            $from_mail = $params['from_mail'];
        }else{
            $from_mail = $from_address['email'];
        }

        if(isset($params['to_mail'])){
            $to_mail = $params['to_mail'];
        }else{
            $to_mail = $customer_address;
        }

        $subject    = $params['subject'];
        $message    = $params['message'];

        // setting param for email
        $this->_ci->email->from($from_mail, $from_name);
        $this->_ci->email->to($to_mail);
        $this->_ci->email->subject($subject);
        $this->_ci->email->message($message);

        // attach file
        if(isset($params['attach'])){
            while(list($key,$val) = each($params['attach'])){
                $this->_ci->email->attach($val);
            }
        }

        return $this->_ci->email->send();
    }
}
