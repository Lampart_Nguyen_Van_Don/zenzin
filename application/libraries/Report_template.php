<?php
class Report_template {

    private $_report_list = array();
    private $_ci;
    private $_data = array();

    /**
     * Constructor
     */
    public function __construct($config = array()) {
        $this->_ci =& get_instance();

        if ( ! empty($config)) {
            $this->initialize($config);
        }

        $this->_ci->load->library('tcpdf/tcpdf');
        log_message('debug', 'Report template class Initialized');
    }

    /**
     * Export report
     *
     * Arguments description
     *   $template = 'template_name' template_name initialized in folder config/report_template.php
     *
     * $replace_params: identical with file template mail in config/report_template/xxx.php
     *
     * @access public
     * @param  array $replace_params
     * @return boolean
     */

    public function export($template, $report_data = array()) {
        if (!key_exists($template, $this->_report_list)) {
            return false;
        }

        if (!isset($this->_data[$template])) {
            if (!$template_path = config_item('report_template_path')) {
                return false;
            }
            $template_path = $template_path.config_item('language').'/'.$this->_report_list[$template];
            if (file_exists($template_path)) {
                include $template_path;
                $this->_data[$template] = array(
                    'replace_list' => $replace_list,
                    'content' => $content
                );
            } else {
                $this->_data[$template] = false;
            }
        }

        if ($this->_data[$template] == false) {
            return false;
        }

        if ( ! empty($content)) {

            // create new PDF document
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->setPrintFooter(false);
            $pdf->setCellPaddings(0, 0, 0, 0);
            $pdf->SetAutoPageBreak(TRUE, 0);
            $pdf->SetDocInfoUnicode(TRUE, 0);
            $pdf->SetDisplayMode('fullpage');

            foreach ($report_data as $item) {
                //get template
                $content = $this->_data[$template]['content'];

                $replaces = array();
                foreach ($replace_list as $key => $replace) {
                    if (isset($item[$key])) {
                        $replaces['{'.$key.'}'] = htmlspecialchars($item[$key]);
                    } else {
                        $replaces['{'.$key.'}'] = '';
                    }
                }

                $content = $this->replace_body(str_replace (array_keys($replaces), array_values($replaces), $content));

                $pdf->AddPage();
                $pdf->writeHTML($content, true, 0, true, 0);
                unset($content);
            }

            $pdf->Output();

            exit();
        }
    }

    /**
     * Initialize preferences
     *
     * @access  public
     * @param   array
     * @return  void
     */
    public function initialize($config = array()) {
        // process each key
        $this->_report_list = $config['report_list'];
    }

    public function replace_body($body)
    {
        $body = explode("\n", $body);
        $replay_array = array();
        foreach ($body as $key => $value){
            if ($value != null) {
                if (str_replace(" ", "", $value) != null) {
                    $replay_array[] = $value;
                }
            }else{
                $replay_array[] = $value;
            }
        }
        return rtrim(str_replace("&np;", "\n", implode("\n",$replay_array)));
    }
}
