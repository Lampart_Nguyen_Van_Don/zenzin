<?php (defined('BASEPATH')) OR exit('No direct script access allowed');


class ZR_Session extends CI_Session {

    protected $_ci = null;
    public function __construct() {
        parent::__construct();
        $this->_ci = &get_instance();
        $this->_ci->load->library('encrypt');
        $this->_set_default_value();
    }

    private function _set_default_value() {
        $this->encryption_key          = $this->_ci->config->item('encryption_key');
        $this->sess_expiration         = $this->_ci->config->item('sess_expiration');
        $this->sess_encrypt_cookie     = $this->_ci->config->item('sess_encrypt_cookie');
        $this->sess_expire_on_close    = $this->_ci->config->item('sess_expire_on_close');

        $this->cookie_prefix           = $this->_ci->config->item('cookie_prefix');
        $this->cookie_domain           = $this->_ci->config->item('cookie_domain');
        $this->cookie_path             = $this->_ci->config->item('cookie_path');
        $this->cookie_secure           = $this->_ci->config->item('cookie_secure');
        $this->cookie_httponly         = $this->_ci->config->item('cookie_httponly');
        $this->sess_use_database       = $this->_ci->config->item('sess_use_database');
    }
    /**
     * Fetch the current session data if it exists
     *
     * @access    public
     * @return    bool
     */
    public function read($cookie_name) {

        static $cookie_array = array();

        if (isset($cookie_array[$cookie_name])) {
            return $cookie_array[$cookie_name];
        }

        // No cookie?  Goodbye cruel world!...
        if (false === ($session = $this->_ci->input->cookie($cookie_name))) {
            $cookie_array[$cookie_name] = false;
            return false;
        }

        // Decrypt the cookie data
        if ($this->sess_encrypt_cookie == true) {
            $session = $this->_ci->encrypt->decode($session);
        } else {
            // encryption was not used, so we need to check the md5 hash
            $hash     = substr($session, strlen($session)-32); // get last 32 chars
            $session = substr($session, 0, strlen($session)-32);

            // Does the md5 hash match?  This is to prevent manipulation of session data in userspace
            if ($hash !==  md5($session.$this->encryption_key)) {
                $this->destroy($cookie_name);
                return false;
            }
        }

        // Unserialize the session array
        $session = unserialize($session);
        $cookie_array[$cookie_name] = $session;
        return $session;
    }

    /**
     * Write the session data directly to the input cookie name
     *
     * @access    public
     * @return    void
     */
    public function write($cookie_name, $cookie_data, $expire = null) {
        $cookie_data['last_activity'] = time();

        // Serialize the userdata for the cookie
        $cookie_data = serialize($cookie_data);
        //         $cookie_data['last_activity'] = $this->_get_time();

        if ($this->sess_encrypt_cookie == true) {
            $cookie_data = $this->_ci->encrypt->encode($cookie_data);
        } else {
            // if encryption is not used, we provide an md5 hash to prevent userside tampering
            $cookie_data = $cookie_data.md5($cookie_data.$this->encryption_key);
        }

        $expire = ($this->sess_expire_on_close === true) ? 0 : $this->sess_expiration + time();

        // Set the cookie
        setcookie(
                $cookie_name,
                $cookie_data,
                $expire,
                $this->cookie_path,
                $this->cookie_domain,
                $this->cookie_secure
        );
    }

    /**
     * Destroy the current session
     *
     * @access    public
     * @return    void
     */
    public function destroy($cookie_name) {
        // Kill the cookie
        setcookie(
                $cookie_name,
                '',
                (time() - 31500000),
                $this->cookie_path,
                $this->cookie_domain,
                0
        );
    }

    /**
     * Update an existing session
     *
     * @access    public
     * @return    void
     */
    public function update($cookie_name) {
        $session = $this->read($cookie_name);

        // We only update the session every five minutes by default
        if (($session['last_activity'] + $this->sess_time_to_update) >= time()) {
            return;
        }

        // writing cookie always get the current time as last activity,
        // so have to do nothing but write itself data into cookie
        $this->write($cookie_name, $session);
    }

    /**
     * Fetch a specific item from the session array
     *
     * Override the original function to add setting multiple level for session.
     * Each dot "." represent a level.
     *
     * Exemple:
     *   userdata('auth.username') will get $session['auth']['username']
     *
     * @access    public
     * @param    string
     * @return    string
     */
    function userdata($item = '') {
        if (!$item) {
            return array();
        }
        $field_str = '["' . str_replace('.', '"]["', $item) . '"]';

        eval("\$item = (!isset(\$this->userdata{$field_str})) ? false : \$this->userdata{$field_str};");
        return $item;
    }

    /**
    * Add or change data in the "userdata" array.
    *
    * Override the original function to add setting multiple level for session.
    * Each dot "." represent a level.
    *
    * Exemple:
    *   set_userdata('auth.username') will set $session['auth']['username']
    *
    * @access    public
    * @param    mixed
    * @param    string
    * @return    void
    */
    function set_userdata($newdata = array(), $newval = '') {

        if (is_string($newdata)) {
            $newdata = array($newdata => $newval);
        }

        if (count($newdata) > 0) {
            foreach ($newdata as $key => $val) {
                // replace for DOT "." convenion
                $field_str = '["' . str_replace('.', '"]["', $key) . '"]';

                eval("\$this->userdata{$field_str} = \$val;");
            }
        }

        $this->sess_write();
    }

    /**
     * Delete a session variable from the "userdata" array
     *
     * Override the original function to add setting multiple level for session.
     * Each dot "." represent a level.
     *
     * Exemple:
     *   unset_userdata('auth.username') will unset $session['auth']['username']
     *
     * @access    array
     * @return    void
     */
    function unset_userdata($newdata = array()) {

        if (is_string($newdata)) {
            $newdata = array($newdata => '');
        }

        if (count($newdata) > 0) {
            foreach ($newdata as $key => $val) {
                // replace for DOT "." convenion
                $field_str = '["' . str_replace('.', '"]["', $key) . '"]';

                eval("if (isset(\$this->userdata{$field_str})) unset(\$this->userdata{$field_str});");
            }
        }

        $this->sess_write();
    }

    /**
     * Write the session data
     *
     * @access    public
     * @return    void
     */
    function sess_write()
    {
        // Are we saving custom data to the DB?  If not, all we do is update the cookie
        if ($this->sess_use_database === FALSE)
        {
            $this->_set_cookie();
            return;
        }

        // set the custom userdata, the session data we will set in a second
        $custom_userdata = $this->userdata;
        $cookie_userdata = array();

        // Before continuing, we need to determine if there is any custom data to deal with.
        // Let's determine this by removing the default indexes to see if there's anything left in the array
        // and set the session data while we're at it
        foreach (array('session_id','ip_address','user_agent','last_activity') as $val)
        {
            unset($custom_userdata[$val]);
            $cookie_userdata[$val] = $this->userdata[$val];
        }

        // Did we find any custom data?  If not, we turn the empty array into a string
        // since there's no reason to serialize and store an empty array in the DB
        if (count($custom_userdata) === 0)
        {
        $custom_userdata = '';
        }
        else
        {
            // Serialize the custom data array so we can store it
                $custom_userdata = $this->_serialize($custom_userdata);
            }

            // Run the update query
            $this->CI->db->where('session_id', $this->userdata['session_id']);
                    $this->CI->db->update($this->sess_table_name, array('last_activity' => $this->userdata['last_activity'], 'user_data' => $custom_userdata));

                    // Write the cookie.  Notice that we manually pass the cookie data array to the
                    // _set_cookie() function. Normally that function will store $this->userdata, but
                    // in this case that array contains custom data, which we do not want in the cookie.
                    $this->_set_cookie($cookie_userdata);
        }
        /**
         * Serialize an array
         *
         * This function first converts any slashes found in the array to a temporary
         * marker, so when it gets unserialized the slashes will be preserved
         *
         * @access    private
         * @param    array
         * @return    string
         */
        function _serialize($data)
        {
            if (is_array($data))
            {
                foreach ($data as $key => $val)
                {
                    if (is_string($val))
                    {
                        $data[$key] = str_replace('\\', '{{slash}}', $val);
                    }
                }
            }
            else
            {
                if (is_string($data))
                {
                    $data = str_replace('\\', '{{slash}}', $data);
                }
            }

            return serialize($data);
        }

        /**
         * Write the session cookie
         *
         * @access    public
         * @return    void
         */
        function _set_cookie($cookie_data = NULL)
        {
            if (is_null($cookie_data))
            {
                $cookie_data = $this->userdata;
            }

            // Serialize the userdata for the cookie
            $cookie_data = $this->_serialize($cookie_data);

            if ($this->sess_encrypt_cookie == TRUE)
            {
                $cookie_data = $this->_ci->encrypt->encode($cookie_data);
            }
            else
            {
                // if encryption is not used, we provide an md5 hash to prevent userside tampering
                $cookie_data = $cookie_data.md5($cookie_data.$this->encryption_key);
            }

            $expire = ($this->sess_expire_on_close === TRUE) ? 0 : $this->sess_expiration + time();

            // Set the cookie
            setcookie(
                    $this->sess_cookie_name,
                    $cookie_data,
                    $expire,
                    $this->cookie_path,
                    $this->cookie_domain,
                    $this->cookie_secure
            );
        }
}
