<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ZR_Pagination extends CI_Pagination
{
    var $_ci = null;

    var $page_uri_array = FALSE;
    var $segment_start  = 3;
    var $use_per_page_segment = FALSE;
    var $per_page_segment = 'per_page';
    var $cur_page = null;
    var $method = 'get';
    var $is_next_link = FALSE;
    var $use_page_numbers = TRUE;


    var $get_template = array(
            'anchor_class'    => '',
            'full_tag_open'    => '',
            'full_tag_close'  => '',
            'first_tag_open'  => '',
            'first_tag_close' => '&nbsp;',
            'last_tag_open'   => '&nbsp;',
            'last_tag_close'  => '',
            'cur_tag_open'    => '&nbsp;<strong>',
            'cur_tag_close'   => '</strong>',
            'next_tag_open'   => '&nbsp;',
            'next_tag_close'  => '&nbsp;',
            'prev_tag_open'   => '&nbsp;',
            'prev_tag_close'  => '',
            'num_tag_open'    => '&nbsp;',
            'num_tag_close'   => '',
    );

    var $post_prefix = '';
    var $post_template = array(
            'form_class'      => '',
            'button_class'    => '',
            'full_tag_open'   => '',
            'full_tag_close'  => '',
            'first_tag_open'  => '',
            'first_tag_close' => '&nbsp;',
            'last_tag_open'   => '&nbsp;',
            'last_tag_close'  => '',
            'cur_tag_open'    => '&nbsp;<strong>',
            'cur_tag_close'   => '</strong>',
            'next_tag_open'   => '&nbsp;',
            'next_tag_close'  => '&nbsp;',
            'prev_tag_open'   => '&nbsp;',
            'prev_tag_close'  => '',
            'num_tag_open'    => '&nbsp;',
            'num_tag_close'   => '',
            'first_button_class' => '',
            'prev_button_class'  => '',
            'last_button_class'  => '',
            'next_button_class'  => ''
    );

    var $ajax_class = '';
    var $ajax_template = array(
            'anchor_class'    => '',
            'full_tag_open'    => '',
            'full_tag_close'  => '',
            'first_tag_open'  => '',
            'first_tag_close' => '&nbsp;',
            'last_tag_open'   => '&nbsp;',
            'last_tag_close'  => '',
            'cur_tag_open'    => '&nbsp;<strong>',
            'cur_tag_close'   => '</strong>',
            'next_tag_open'   => '&nbsp;',
            'next_tag_close'  => '&nbsp;',
            'prev_tag_open'   => '&nbsp;',
            'prev_tag_close'  => '',
            'num_tag_open'    => '&nbsp;',
            'num_tag_close'   => '',
    );

    /**
     * Constructor
     *
     * @access    public
     * @param    array    initialization parameters
     */
    public function __construct($params = array()) {
        $this->_ci =& get_instance();

        if (count($params) > 0) {
            $this->initialize($params);
        }

        log_message('debug', "Pagination Class Initialized");
    }

    /**
     * Initialize Preferences
     *
     * @access    public
     * @param    array    initialization parameters
     * @return    void
     */
    public function initialize($params = array()){
        $this->_ci =& get_instance();

        if (count($params) > 0) {
            foreach ($params as $key => $val) {
                if (isset($this->$key)) {
                    if (is_array($this->$key) && is_array($val)) {
                        foreach ($val as $k => $v) {
                            if (isset($this->{$key}[$k])) {
                                $this->{$key}[$k] = $v;
                            }
                        }
                    } else {
                        $this->$key = $val;
                    }
                }
            }
        }
        
        if (isset($params['use_per_page_segment'])) {
            $this->_process_per_page();
        }
        
        if ($this->cur_page === null) {
            $this->_process_current_page();
        }
    }

    /**
     * Process current page and and number of items
     *
     * @access private
     */
    private function _process_current_page() {

        // Set the base page index for starting page number
        if ($this->use_page_numbers) {
            $base_page = 1;
        } else {
            $base_page = 0;
        }

        switch ($this->method) {
            case 'ajax':
                // ajax always begin with the first page
                $this->cur_page = 0;
                break;
            case 'post':
                if ($this->_ci->input->post($this->query_string_segment) != $base_page) {
                    $this->cur_page = $this->_ci->input->post($this->query_string_segment);
                }
                break;
            case 'get':
            default:
                if ($this->_ci->config->item('enable_query_strings') === TRUE OR $this->page_query_string === TRUE) {
                    if ($this->_ci->input->get($this->query_string_segment) != $base_page) {
                        $this->cur_page = $this->_ci->input->get($this->query_string_segment);
                    }
                } else {
                    if ($this->page_uri_array) {
                        // Get uri array
                        $vars = $this->_ci->uri->uri_to_assoc($this->segment_start);

                        $this->cur_page = isset($vars[$this->query_string_segment]) ? $vars[$this->query_string_segment] : 0;
                    } elseif ($this->_ci->uri->segment($this->uri_segment) != $base_page) {
                        $this->cur_page = $this->_ci->uri->segment($this->uri_segment);
                    }
                }
        }

        // Prep the current page - no funny business!
        $this->cur_page = (int) $this->cur_page;

        // Set current page to 1 if using page numbers instead of offset
        if ($this->use_page_numbers AND $this->cur_page == 0) {
            $this->cur_page = $base_page;
        }
    }

    private function _process_per_page() {

        if ($this->page_uri_array) {
            // Get uri array
            $vars = $this->_ci->uri->uri_to_assoc($this->segment_start);
        }

        // get items number vie uri
        if ($this->use_per_page_segment == TRUE) {
            if (($this->_ci->config->item('enable_query_strings') === TRUE OR $this->page_query_string === TRUE)) {
                if ($per_page = $this->_ci->input->get($this->per_page_segment)) {
                    $this->per_page = $per_page;
                }
            }
            else {
                if (!$this->page_uri_array) {
                    if ($per_page = $this->_ci->uri->segment($this->per_page_segment)) {
                        $this->per_page = $per_page;
                    }
                } else {
                    // Get uri array
                    if (isset($vars[$this->per_page_segment])) {
                        $this->per_page = $vars[$this->per_page_segment];
                    }
                }
            }
        }
    }

    /**
     * Create pagination HTML via AJAX.
     *
     * @param int $num_pages
     * @param int $base_page
     * @return string
     */
    protected function _create_links_via_ajax($num_pages, $base_page) {
        // Is the page number beyond the result range?
        // If so we show the last page
        if ($this->use_page_numbers) {
            if ($this->cur_page > $num_pages) {
                $this->cur_page = $num_pages;
            }
        } else {
            if ($this->cur_page > $this->total_rows) {
                $this->cur_page = ($num_pages - 1) * $this->per_page;
            }
        }
        $this->num_links = (int)$this->num_links;

        if ($this->num_links < 1) {
            show_error('Your number of links must be a positive number.');
        }

        // Calculate the start and end numbers. These determine
        // which number to start and end the digit links with
        $start  = (($this->cur_page - $this->num_links) > 0) ? $this->cur_page - ($this->num_links - 1) : 1;
        $end   = (($this->cur_page + $this->num_links) < $num_pages) ? $this->cur_page + $this->num_links : $num_pages;

        // And here we go...
        $template = $this->get_template;
        $output = '';

        $anchor_class = ($template['anchor_class'] != '') ? ' class="' . $template['anchor_class'] . '"' : '';
        $link_anchor = '<a href="%s"' . $anchor_class . '>%s</a>';

        // Render the "First" link
        if  ($this->first_link !== FALSE && $this->cur_page != 1) {
            $output .= $template['first_tag_open'] . sprintf($link_anchor, '#1', $this->first_link) . $template['first_tag_close'];
        }

        // Render the "previous" link
        if  (($this->prev_link !== FALSE && $this->cur_page != 1)) {
            $prev_page = ($this->cur_page != 1) ? ($this->cur_page - 1) : 1;
            $output .= $template['prev_tag_open'] . sprintf($link_anchor, '#' . $prev_page, $this->prev_link) . $template['prev_tag_close'];
        }

        // Render the pages
        if ($this->display_pages !== FALSE) {
            // Write the digit links
            for ($loop = $start; $loop <= $end; $loop++) {
                if ($this->cur_page == $loop) {
                    $output .= $template['cur_tag_open'] . $loop . $template['cur_tag_close']; // Current page
                } else {
                    $output .= $template['num_tag_open'] . sprintf($link_anchor, '#' . $loop, $loop) . $template['num_tag_close'];
                }
            }
        }

        // Render the "next" link
        if ($this->next_link !== FALSE && $this->cur_page < $num_pages) {
            $next_page = ($this->cur_page == $num_pages) ? $num_pages : ($this->cur_page + 1);
            $output .= $template['next_tag_open'] . sprintf($link_anchor, '#' . $next_page,$this->next_link) . $template['next_tag_close'];
        }

        // Render the "last" link
        if ($this->last_link !== FALSE && $this->cur_page < $num_pages) {
            $output .= $template['last_tag_open'] . sprintf($link_anchor, '#' . $num_pages, $this->last_link) . $template['last_tag_close'];
        }

        // Add the wrapper HTML if exists
        if ($this->ajax_class != '') {
            if (false !== ($class_pos = strpos($template['full_tag_open'], 'class='))) {
                $template['full_tag_open'] = substr_replace($template['full_tag_open'], 'class="' . $this->ajax_class . ' ', $class_pos, 7);
            }
        }

        $output = $template['full_tag_open'] . $output . $template['full_tag_close'] . "\n";

        //Render js for this pager.
        $output .= '<script type="text/javascript">' . "\n";
        $output .= "$(document).ready(function() { \n";
        $output .= " $('." . $this->ajax_class . " a').on('click', function(event) { \n";
        $output .= "event.preventDefault();\n";
        $output .= "action = $(this).parent().attr('class');\n";
        $output .= "container = $(this).parent().parent();\n";
        $output .= "var params = {\n";
        $output .= "    pg : $(this).attr('href').replace('#', '')\n";
        $output .= "};\n";
        $output .= "if (action == 'prev' || action == 'next') {\n";
        $output .= "    action = $(this).parent().parent().attr('class');\n";
        $output .= "    container = $(this).parent().parent().parent();\n";
        $output .= "}\n";
        $output .= "action = action.replace(/pager|\s/gi, '');\n";
        $output .= " $.post(MOD_URL + 'top/ajax_get_' + action, params, function(rs) { \n";
        $output .= "if (rs.status == 1 && rs.data) { \n";
        $output .= "    table = rs.data.table;\n";
        $output .= "    links = rs.data.pagination.links;\n";
        $output .= "    total_page = rs.data.pagination.total_rows;\n";
        $output .= "    container.siblings('table[class=\"show\"]').remove();\n";
        $output .= "    container.parent().prepend(table);\n";
        $output .= "    container.empty().append(links);\n";
        $output .= "    container.append('<div class=\"total_page\">" . lang('common_label_total_item') . ":' + total_page + '</div>');\n";
        $output .= "}\n";
        $output .= "}, 'json');\n";
        $output .= "});\n";
        $output .= "})\n";
        $output .= '</script>';

        return $output;
    }

    /**
     * Create pagination HTML via POST.
     *
     * @param int $num_pages
     * @param int $base_page
     * @return string
     */
    protected function _create_links_via_post($num_pages, $base_page) {

        // Is the page number beyond the result range?
        // If so we show the last page
        if ($this->use_page_numbers) {
            if ($this->cur_page > $num_pages) {
                $this->cur_page = $num_pages;
            }
        } else {
            if ($this->cur_page > $this->total_rows) {
                $this->cur_page = ($num_pages - 1) * $this->per_page;
            }
        }

        $this->num_links = (int)$this->num_links;

        if ($this->num_links < 1) {
            show_error('Your number of links must be a positive number.');
        }

        $uri_page_number = $this->cur_page;

        if (!$this->use_page_numbers) {
            $this->cur_page = floor(($this->cur_page/$this->per_page) + 1);
        }

        // Calculate the start and end numbers. These determine
        // which number to start and end the digit links with
        $start = (($this->cur_page - $this->num_links) > 0) ? $this->cur_page - ($this->num_links - 1) : 1;
        $end   = (($this->cur_page + $this->num_links) < $num_pages) ? $this->cur_page + $this->num_links : $num_pages;

        $this->base_url = rtrim($this->base_url, '/') .'/';

        // And here we go...
        $template = $this->post_template;
        $output = '';

        $button_class = ($template['button_class'] != '') ? ' class="' . $template['button_class'] . '"' : '';
        $link_button = '<button type="submit" name="' . $this->query_string_segment . '" value="%s"' . $button_class . '>%s</button>';

        // Render the "First" link
        if  (($this->first_link !== FALSE) AND ($this->cur_page > ($this->num_links + 1))) {
            $first_button_class = ($template['first_button_class'] != '') ? ' class="' . $template['first_button_class'] . '"' : '';
            $first_link_button = '<button type="submit" name="' . $this->query_string_segment . '" value="%s"' . $first_button_class . '>%s</button>';
            $output .= $template['first_tag_open'] . sprintf($first_link_button, 1, $this->first_link) . $template['first_tag_close'];
        }

        // Render the "previous" link
        if  (($this->prev_link !== FALSE) AND ($this->cur_page != 1)) {
            if ($this->use_page_numbers) {
                $i = $uri_page_number - 1;
            } else {
                $i = $uri_page_number - $this->per_page;
            }
            $prev_button_class = ($template['prev_button_class'] != '') ? ' class="' . $template['prev_button_class'] . '"' : '';
            $prev_link_button = '<button type="submit" name="' . $this->query_string_segment . '" value="%s"' . $prev_button_class . '>%s</button>';
            $output .= $template['prev_tag_open'] . sprintf($prev_link_button, $i, $this->prev_link) . $template['prev_tag_close'];
        } 

        // Render the pages
        if ($this->display_pages !== FALSE) {
            // Write the digit links
            for ($loop = $start -1; $loop <= $end; $loop++) {
                if ($this->use_page_numbers) {
                    $i = $loop;
                } else {
                    $i = ($loop * $this->per_page) - $this->per_page;
                }

                if ($i >= $base_page) {
                    if ($this->cur_page == $loop) {
                        $output .= $template['cur_tag_open'] . $loop . $template['cur_tag_close']; // Current page
                    } else {
                        $output .= $template['num_tag_open'] . sprintf($link_button, $i, $loop) . $template['num_tag_close'];
                    }
                }
            }
        }

        // Render the "next" link
        if (($this->next_link !== FALSE) AND ($this->cur_page < $num_pages)) {
            $this->is_next_link = TRUE;
            if ($this->use_page_numbers) {
                $i = $this->cur_page + 1;
            } else {
                $i = ($this->cur_page * $this->per_page);
            }
            $next_button_class = ($template['next_button_class'] != '') ? ' class="' . $template['next_button_class'] . '"' : '';
            $next_link_button = '<button type="submit" name="' . $this->query_string_segment . '" value="%s"' . $next_button_class . '>%s</button>';
            $output .= $template['next_tag_open'] . sprintf($next_link_button, $i, $this->next_link) . $template['next_tag_close'];
        }

        // Render the "Last" link
        if (($this->last_link !== FALSE) AND (($this->cur_page + $this->num_links) < $num_pages)) {
            if ($this->use_page_numbers) {
                $i = $num_pages;
            } else {
                $i = (($num_pages * $this->per_page) - $this->per_page);
            }
            $last_button_class = ($template['last_button_class'] != '') ? ' class="' . $template['last_button_class'] . '"' : '';
            $last_link_button = '<button type="submit" name="' . $this->query_string_segment . '" value="%s"' . $last_button_class . '>%s</button>';
            $output .= $template['last_tag_open'] . sprintf($last_link_button, $i, $this->last_link) . $template['last_tag_close'];
        }

        // Kill double slashes.  Note: Sometimes we can end up with a double slash
        // in the penultimate link so we'll kill all double slashes.
        $output = preg_replace("#([^:])//+#", "\\1/", $output);

        // Insert POST as input hidden
        $hidden = '';
        if ($post = $this->_ci->input->post()) {
            if (isset($post[$this->query_string_segment])) {
                unset($post[$this->query_string_segment]);
            }
        }
        if (!empty($post)) {
            $this->_ci->load->helper('form');
            if(isset($post['sequence'])){
                unset($post['sequence']);
            }
            $hidden = '<div style="display:none">' . form_hidden($post) . '</div>';
        }

        // Add the wrapper HTML if exists
        $form_class = ($template['form_class'] != '') ? ' class="' . $template['form_class'] . '"' : '';
        $output = '<form method="POST" action="' . $this->base_url . '"' . $form_class . '>' . $hidden . $template['full_tag_open'] . $output . $template['full_tag_close'] . '</form>';

        return $output;
    }

    /**
     * Create pagination HTML via GET.
     *
     * @param int $num_pages
     * @param int $base_page
     * @return string
     */
    protected function _create_links_via_get($num_pages, $base_page) {

        if ($this->page_uri_array) {
            // Get uri array
            $vars = $this->_ci->uri->uri_to_assoc($this->segment_start);
        }

        // Add per page segment string
        if ($this->use_per_page_segment == TRUE) {
            if (($this->_ci->config->item('enable_query_strings') === TRUE) OR ($this->page_query_string === TRUE)) {
                if (!$this->_ci->input->get($this->per_page_segment)) {
                    $this->base_url .= '&amp;' . $this->per_page_segment . '=' . $this->per_page;
                }
            }
            elseif ($this->page_uri_array) {
                if (!isset($vars[$this->per_page_segment])) {
                    $vars[$this->per_page_segment] = $this->per_page;
                }
            }
        }

        // Is the page number beyond the result range?
        // If so we show the last page
        if ($this->use_page_numbers) {
            if ($this->cur_page > $num_pages) {
                $this->cur_page = $num_pages;
            }
        } else {
            if ($this->cur_page > $this->total_rows) {
                $this->cur_page = ($num_pages - 1) * $this->per_page;
            }
        }

        $this->num_links = (int)$this->num_links;

        if ($this->num_links < 1) {
            show_error('Your number of links must be a positive number.');
        }

        $uri_page_number = $this->cur_page;

        if (!$this->use_page_numbers) {
            $this->cur_page = floor(($this->cur_page/$this->per_page) + 1);
        }

        // Calculate the start and end numbers. These determine
        // which number to start and end the digit links with
        $start = (($this->cur_page - $this->num_links) > 0) ? $this->cur_page - ($this->num_links - 1) : 1;
        $end   = (($this->cur_page + $this->num_links) < $num_pages) ? $this->cur_page + $this->num_links : $num_pages;

        // Is pagination being used over GET or POST?  If get, add a per_page query
        // string. If post, add a trailing slash to the base URL if needed
        if ($this->_ci->config->item('enable_query_strings') === TRUE OR $this->page_query_string === TRUE) {
            $this->base_url = rtrim($this->base_url).'&amp;'.$this->query_string_segment.'=';
        } else {
            if ($this->page_uri_array == TRUE) {
                if ($segment_start = $this->_ci->uri->segment($this->segment_start)) {
                    $this->base_url = rtrim(substr($this->base_url, 0, strpos($this->base_url, '/' . $segment_start . '/')), '/') . '/';
                }
            }
            $this->base_url = rtrim($this->base_url, '/') .'/';
        }

        // And here we go...
        $template = $this->get_template;
        $output = '';

        $anchor_class = ($template['anchor_class'] != '') ? ' class="' . $template['anchor_class'] . '"' : '';
        $link_anchor = '<a href="%s"' . $anchor_class . '>%s</a>';

        // Render the "First" link
        if  ($this->first_link !== FALSE AND $this->cur_page > ($this->num_links + 1)) {
            if (!$this->first_url == '') {
                $first_url = $this->first_url;
            } else {
                $first_url = $this->base_url;
                if ($this->page_uri_array) {
                    $vars[$this->query_string_segment] = $base_page;
                    $first_url .= $this->_ci->uri->assoc_to_uri($vars);
                }
            }

            $output .= $template['first_tag_open'] . sprintf($link_anchor, $first_url, $this->first_link) . $template['first_tag_close'];
        }

        // Render the "previous" link
        if  (($this->prev_link !== FALSE) AND ($this->cur_page != 1)) {
            if ($this->use_page_numbers) {
                $i = $uri_page_number - 1;
            } else {
                $i = $uri_page_number - $this->per_page;
            }

            if ($i == 0 && $this->first_url != '') {
                $output .= $template['prev_tag_open'] . sprintf($link_anchor, $this->first_url, $this->prev_link) . $template['prev_tag_close'];
            } else {
                if (!$this->page_uri_array) {
                    $i = ($i == 0) ? '' : $this->prefix.$i.$this->suffix;
                } else {
                    $vars[$this->query_string_segment] = ($i == '') ? $base_page : $i;
                    $i = $this->_ci->uri->assoc_to_uri($vars);
                }

                $output .= $template['prev_tag_open'] . sprintf($link_anchor, $this->base_url . $i, $this->prev_link) . $template['prev_tag_close'];
            }
        }

        // Render the pages
        if ($this->display_pages !== FALSE) {
            // Write the digit links
            for ($loop = $start -1; $loop <= $end; $loop++) {
                if ($this->use_page_numbers) {
                    $i = $loop;
                } else {
                    $i = ($loop * $this->per_page) - $this->per_page;
                }

                if ($i >= $base_page) {
                    if ($this->cur_page == $loop) {
                        $output .= $template['cur_tag_open'] . $loop . $template['cur_tag_close']; // Current page
                    } else {
                        $n = ($i == $base_page) ? '' : $i;

                        if ($n == '' && $this->first_url != '') {
                            $output .= $template['num_tag_open'] . sprintf($link_anchor, $this->first_url, $loop) . $template['num_tag_close'];
                        } else {
                            if (!$this->page_uri_array) {
                                $n = ($n == '') ? '' : $this->prefix.$n.$this->suffix;
                            } else {
                                $vars[$this->query_string_segment] = ($n == '') ? $base_page : $n;
                                $n = $this->_ci->uri->assoc_to_uri($vars);
                            }

                            $output .= $template['num_tag_open'] . sprintf($link_anchor, $this->base_url . $n, $loop) . $template['num_tag_close'];
                        }
                    }
                }
            }
        }

        // Render the "next" link
        if ($this->next_link !== FALSE AND $this->cur_page < $num_pages) {
            if ($this->use_page_numbers) {
                $i = $this->cur_page + 1;
            } else {
                $i = ($this->cur_page * $this->per_page);
            }

            if (!$this->page_uri_array) {
                $i = $this->prefix.$i.$this->suffix;
            } else {
                $vars[$this->query_string_segment] = $i;
                $i = $this->_ci->uri->assoc_to_uri($vars);
            }

            $output .= $template['next_tag_open'] . sprintf($link_anchor, $this->base_url . $i, $this->next_link) . $template['next_tag_close'];
        }

        // Render the "last" link
        if ($this->last_link !== FALSE AND ($this->cur_page + $this->num_links) < $num_pages) {
            if ($this->use_page_numbers) {
                $i = $num_pages;
            } else {
                $i = (($num_pages * $this->per_page) - $this->per_page);
            }

            if (!$this->page_uri_array) {
                $i = $this->prefix.$i.$this->suffix;
            } else {
                $vars[$this->query_string_segment] = $i;
                $i = $this->_ci->uri->assoc_to_uri($vars);
            }

            $output .= $template['last_tag_open'] . sprintf($link_anchor, $this->base_url . $i, $this->last_link) . $template['last_tag_close'];
        }

        // Kill double slashes.  Note: Sometimes we can end up with a double slash
        // in the penultimate link so we'll kill all double slashes.
        $output = preg_replace("#([^:])//+#", "\\1/", $output);

        // Add the wrapper HTML if exists
        $output = $template['full_tag_open'] . $output . $template['full_tag_close'];

        return $output;
    }

    /**
     * Generate the pagination links
     *
     * @access    public
     * @return    string
     */
    public function create_links() {

        // If our item count or per-page total is zero there is no need to continue.
        if ($this->total_rows == 0 OR $this->per_page == 0) {
            return '';
        }
        
        // Calculate the total number of pages
        $num_pages = ceil($this->total_rows / $this->per_page);

        // Is there only one page? Hm... nothing more to do here then.
        if ($num_pages == 1) {
            return '';
        }

        // Set the base page index for starting page number
        if ($this->use_page_numbers) {
            $base_page = 1;
        } else {
            $base_page = 0;
        }
        
        // If no base url, auto-load current url
        if ($this->base_url == '') {
            $this->_ci->load->helper('url');
            
            $this->base_url = create_current_url();
        }
        
        switch ($this->method) {
            case 'ajax':
                return $this->_create_links_via_ajax($num_pages, $base_page);
            case 'post':
                return $this->_create_links_via_post($num_pages, $base_page);
            case 'get':
            default:
                return $this->_create_links_via_get($num_pages, $base_page);
        }
    }
    
    public function is_next_link() {
        return $this->is_next_link;
    }
}

?>
