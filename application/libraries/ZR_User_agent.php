<?php
class ZR_User_agent  extends CI_User_agent {

    /**
     * Check if request is from iPhone device.
     * @return boolean
     */
    public function is_iphone() {
        return ($this->mobile() == $this->mobiles['iphone']);
    }

    /**
     * Check if request is from iPad device.
     * @return boolean
     */
    public function is_ipad() {
        return ($this->mobile() == $this->mobiles['ipad']);
    }

    /**
     * Check if request is from iPod device.
     * @return boolean
     */
    public function is_ipod() {
        return ($this->mobile() == $this->mobiles['ipod']);
    }

    /**
     * Check if request is from Android OS device.
     * @return boolean
     */
    public function is_android() {
        if (false !== (strpos(strtolower($this->agent), 'android'))) {
            return true;
        }
    }

    /**
     * Check if request is from Android Tablet device.
     * @return boolean
     */
    public function is_android_tablet() {
        if (!$this->is_android()) {
            return false;
        }

        if (false !== (strpos(strtolower($this->agent), 'mobile'))) {
            return false;
        }

        return true;
    }
}