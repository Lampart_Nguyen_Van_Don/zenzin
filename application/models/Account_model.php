<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account_model extends ZR_Model {

//#7069:Start
    const LIMIT_RECENT_LOG_PASSWORD = 2;
//#7069:Start

    /**
     * get all account
     */
    public function get_accounts() {
        return $this->get_items('account');
    }

    /**
     * Get accounts that is still working
     *
     * @param string $select - select column
     * @return array
     *
     * @author quang_duc
     */
    public function get_working_accounts($select = '') {

        return $data_return = $this->db->select($select)
                        ->where('resignation', STATUS_ENABLE)
                        ->where('disable', STATUS_ENABLE)
                        ->order_by('id')
                        ->get('account')
                        ->result_array();

    }

    /**
     * Get account and its department, business unit information
     * By default, this function will get id and name of account, department, business_unit
     *
     * @param int|array $id - leave empty to get all accounts
     * @param string $select - Additional fields
     * @param boolean $working_only - Get only account.resignation = 0
     * @return array
     *
     * @author quang_duc
     */
    public function get_account_department_business_unit($id = '', $select = '', $working_only = TRUE) {


        $this->db->select('account.id, account.name, department.id AS department_id, department.name AS department_name, business_unit.id AS business_unit_id, business_unit.name AS business_unit_name')
                 ->join('department', 'account.department_id = department.id')
                 ->join('business_unit', 'department.business_unit_id = business_unit.id')
                 ->where('account.disable', STATUS_ENABLE);
        $this->db->order_by('account.id asc');

        if (is_array($id)) {
            $this->db->where_in('account.id', $id);
        } elseif (is_numeric($id)) {
            $this->db->where('account.id', $id);
        }

        if ($select) {
            $this->db->select($select);
        }

        if ($working_only) {
            $this->db->where('account.resignation', STATUS_ENABLE);
        }
        return $this->db->get('account')->result_array();

    }

    /**
     * Get account by id
     * @param int $id
     * @return array
     */
    public function get_account_by_id($id) {
        return $this->get_item_by_id('account', $id);
    }

    /**
     * Insert a account
     * @param array $data
     * @return int
     */
    public function insert_account($data) {
        return $this->insert('account', $data);
    }

    /**
     * Insert reset password to history
     * @param array $data
     */
    public function insert_account_password_log($data) {
        return $this->insert('account_password_log', $data);
    }
    /**
     * Get password log by account id
     * @param int $account_id
     * @return array
     */
    public function get_password_log_by_account_id($account_id) {
        $sql = " SELECT *"
              ." FROM account_password_log"
              ." WHERE account_id = ?"
              ." AND disable = " . STATUS_ENABLE
//#7069:Start
              //." ORDER BY create_datetime DESC"
              //." LIMIT 2";
              ." ORDER BY id DESC"
              ." LIMIT " . self::LIMIT_RECENT_LOG_PASSWORD;
//#7069:End

        return $this->exec_query($sql, array($account_id));
    }

    public function update_account($data, $id) {
        return $this->update('account', $data, $id);
    }
    /**
     * Get password log by account id
     * @param int $account_id
     * @return array
     */
    public function check_update_password($account_id) {
        $sql = " SELECT *"
               ." FROM account_password_log"
               ." WHERE account_id = ?"
               ."    AND create_datetime >= DATE_SUB(NOW(), INTERVAL 3 MONTH)"
               ." AND disable = " . STATUS_ENABLE;
        return $this->exec_query($sql, array($account_id));
    }

    public function check_account_update_password($account_id) {
        $sql = " SELECT *"
               ." FROM account"
               ." WHERE id = ?"
               ."    AND create_datetime >= DATE_SUB(NOW(), INTERVAL 3 MONTH)"
               ." AND disable = " . STATUS_ENABLE;
        return $this->exec_query($sql, array($account_id));
    }
    /**
     * Validation area
     */
    public function rules($rule_name) {

        //Validate login form
        $validate_rule['login'] = array(
                array(
                        'field' => 'login_id',
                        'rules' => 'trim|required',
                        'label' => lang('lbl_id')
                    ),
                array(
                        'field' => 'password',
                        'label' => lang('lbl_password'),
                        'rules' => 'trim|required|md5',
               ),
        );

        //Validate changing password feature

        $validate_rule['change_password'] = array(
                array(
                        'field' => 'new_password',
                        'label' => lang('lbl_new_password'),
                        'rules' => 'trim|required|min_length[8]|valid_password'
                ),
                array(
                        'field' => 'confirm_password',
                        'label' => lang('lbl_confirm_password'),
                        'rules' => 'trim|required|matches[new_password]'
                ),
        );

        $validate_rule['edit'] = array(
                array(
                    'field'  => 'name',
                    'label'  => lang('lbl_name'),
                    'rules'  => 'trim|required|max_length[128]',
                    'errors' => array(
                            'required' => lang('error_name_required'),
                    )
                ),
                array(
                        'field' => 'password',
                        'label'  => 'password',
                        'rules'  => 'trim|min_length[8]|valid_password',
                        'errors' => array(
                                'min_length'     => lang('error_password_min_length'),
                                'valid_password' => lang('error_password_alphabet_number'),
                        )
                ),
                array(
                        'field'  => 'confirm_password',
                        'label'  => 'confirm_password',
                        'rules'  => 'trim|matches[password]',
                        'errors' => array(
                                'matches' => lang('error_password_not_match'),
                        )
                ),
                array(
                    'field'  => 'resignation',
                    'label'  => 'Resignation',
                    'rules'  => 'in_list[0,1]',
                    'errors' => array(
                            'in_list' => lang('lbl_invalid_input'),
                    )
                )
        );

        $validate_rule['email'] = array(
                array(
                    'field'  => 'mail_address',
                    'label'  => 'Mail address',
                    'rules'  => 'trim|required|valid_email|max_length[128]',
                    'errors' => array(
                            'required'    => lang('error_email_required'),
                            'valid_email' => lang('error_email_wrong'),
                            // 'is_unique'   => lang('error_email_exist'),
                    )
                )
        );

        $validate_rule['add'] = array(
                array(
                    'field'  => 'login_id',
                    'label'  => 'Login Id',
                    'rules'  => 'trim|required|is_unique[account.login_id]',
                    'errors' => array(
                            'required'  => lang('error_login_id_required'),
                            'is_unique' => lang('error_login_id_exist'),
                    )
                ),
                array(
                    'field'  => 'name',
                    'label'  => 'Name',
                    'rules'  => 'trim|required',
                    'errors' => array(
                            'required' => lang('error_name_required'),
                    )
                ),
                array(
                        'field' => 'password',
                        'label'  => 'password',
                        'rules'  => 'trim|min_length[8]|valid_password|required',
                        'errors' => array(
                                'min_length'     => lang('error_password_min_length'),
                                'valid_password' => lang('error_password_alphabet_number'),
                                'required'       => lang('error_password_required'),
                        )
                ),
                array(
                        'field'  => 'confirm_password',
                        'label'  => 'confirm_password',
                        'rules'  => 'trim|matches[password]|required',
                        'errors' => array(
                                'matches'  => lang('error_password_not_match'),
                                'required' => lang('error_password_required'),
                        )
                ),
                array(
                    'field'  => 'mail_address',
                    'label'  => 'Mail address',
                    'rules'  => 'trim|required|valid_email',
                    'errors' => array(
                            'required'    => lang('error_email_required'),
                            'valid_email' => lang('error_email_wrong'),
                            // 'is_unique'   => lang('error_email_exist'),
                    )
                ),
                array(
                    'field'  => 'resignation',
                    'label'  => 'Resignation',
                    'rules'  => 'in_list[0,1]',
                    'errors' => array(
                            'in_list' => lang('lbl_invalid_input'),
                    )
                ),
                array(
                    'field'  => 'department_id',
                    'label'  => 'Department id',
                    'rules'  => 'required',
                    'errors' => array(
                            'required' => lang('error_please_select_department'),
                    )
                ),
                array(
                    'field'  => 'authority_id',
                    'label'  => 'Authority id',
                    'rules'  => 'required',
                    'errors' => array(
                            'required' => lang('error_please_select_authority'),
                    )
                ),
        );



        if (isset($validate_rule[$rule_name])) {
            return $validate_rule[$rule_name];
        }
        return array();
    }

    /**
     * End validation
     */

    /**
     * Get accounts data by parameters
     *
     * @param array $params
     * @return mixed bool|array
     * @author hoang_minh
     */
    public function get_accounts_by_parameters($params = array(), $is_filter = true, $is_unique = false) {

        if (empty($params)) {
            return $this->get_items('account');
        }

        //init params
        $columns = (isset($params['columns'])) ? $params['columns'] : array();
        $filter_conditions = array();
        $orders = array();
        $limits = (isset($params['limits'])) ? $params['limits'] : array();


        //processing conditions
        if (isset($params['conditions']) && is_array($params['conditions'])) {
            foreach ($params['conditions'] as $key => $val) {
                switch ($key) {
                    case 'id':
                         if ($is_unique) {
                             $filter_conditions[] = 'account.' . $key . " != '" . mysql_escape_string($val) . "'";
                         } else {
                             $filter_conditions[] = 'account.' . $key . " = '" . mysql_escape_string($val) . "'";
                         }
                        break;

                    case 'ids':
                        $filter_conditions[] = "account.id IN (" . mysql_escape_string($val) . ")";
                        break;

                    case 'name':
                    case 'login_id':
                    case 'mail_address':
                        if ($is_filter) {
                            $filter_conditions[] = 'account.' . $key . " LIKE '%" . mysql_escape_string($val) . "%'";
                        } else {
                            $filter_conditions[] = 'account.' . $key . " = '" . mysql_escape_string($val) . "'";
                        }
                        break;

                    case 'department_name':
                        $filter_conditions[] = "department.name LIKE '%" . mysql_escape_string($val) . "%'";
                        break;

                    case 'authority_id':
                        $filter_conditions[] = 'account.' . $key . " = '" . mysql_escape_string($val) . "'";
                        break;

                    case 'authority_ids':
                        $filter_conditions[] = "account.authority_id IN (" . mysql_escape_string($val) . ")";
                        break;

                    case 'authority_name':
                        $filter_conditions[] = "authority.name LIKE '%" . mysql_escape_string($val) . "%'";
                        break;

                    case 'lastup_datetime':
                        $filter_conditions[] = 'account.' . $key . " >= '" . date('Y-m-d', strtotime($val)) . "'";
                        break;
                }
            }
        }

        $filter_conditions[] = 'account.disable = ' . STATUS_ENABLE;
#8533:Start
        $filter_conditions[] = 'account.resignation = ' . STATUS_ENABLE;
#8533:End
        $filter_conditions[] = 'authority.disable = ' . STATUS_ENABLE;
        $filter_conditions[] = 'department.disable = ' . STATUS_ENABLE;


        //processing ordering
        if (isset($params['orders']) && is_array($params['orders'])) {
            foreach ($params['orders'] as $key => $val) {
                $orders[] = $key . ' ' . mysql_escape_string($val);
            }
        }

        //table
        $table = 'account JOIN authority ON account.authority_id = authority.id '
               . 'JOIN department ON account.department_id = department.id';

        return $this->get_items_by_parameters($table, $columns, $filter_conditions,
                $orders, $limits);
    }

    /**
     * Delete mutiple rows.
     *
     * @param string $ids
     * @return bool
     * @author hoang_minh
     */
    public function delete_accounts($ids) {
        return $this->delete_multiple('account', $ids);
    }

    public function search_by_name_and_department($data) {

        $query =    "SELECT a.id
                        , a.name
                        , d.name as department_name
                        , bu.name as business_name
                    FROM account a
                    INNER JOIN department d on d.id = a.department_id
                    INNER JOIN business_unit bu on bu.id = d.business_unit_id
                    WHERE a.disable = 0";

        $conditions = array();
        if (isset($data['name'])) {
            $query .= " AND a.name LIKE ?";
            $conditions[] = '%'.$data['name'].'%';
        }

        if (isset($data['business_unit_id']) && $data['business_unit_id'] != -1) {
            $query .= " AND bu.id = ?";
            $conditions[] = $data['business_unit_id'];
        }

        if (isset($data['department_id']) && $data['department_id'] != -1) {
            $query .= " AND a.department_id = ?";
            $conditions[] = $data['department_id'];
        }
        $query .= " ORDER BY bu.sequence, bu.id ASC, d.id ASC, a.id ASC";
        return $this->exec_query($query, $conditions);
    }

    public function get_detail($id) {
        $query =   "SELECT a.*
                        , bu.name as business_name
                        , d.name as department_name
                        , au.name as authority_name
                    FROM account a
                    INNER JOIN department d ON d.id = a.department_id
                    INNER JOIN business_unit bu on bu.id  = d.business_unit_id
                    INNER JOIN authority au ON au.id = a.authority_id
                    WHERE a.disable = 0 AND a.id = ?";
        $this->set_single();
        return $this->exec_query($query, array($id));
    }

    public function export_csv($data) {
        $query = "SELECT login_id
                    , a.name
                    , mail_address
                    , CONCAT(bu.name, d.name) as department_name
                    , au.name as authority_name
                    , resignation
                  FROM account a
                  INNER JOIN department d ON d.id = a.department_id
                  INNER JOIN business_unit bu ON bu.id = d.business_unit_id
                  INNER JOIN authority au ON au.id = a.authority_id
                  WHERE a.disable = 0";

        $conditions = array();
        if (isset($data['name'])) {
            $query .= " AND a.name LIKE ?";
            $conditions[] = '%'.$data['name'].'%';
        }

        if (isset($data['department_id']) && $data['department_id'] != -1) {
            $query .= " AND a.department_id = ?";
            $conditions[] = $data['department_id'];
        }
        $query .= " ORDER BY a.id DESC";
        return $this->exec_query($query, $conditions);
    }

    /**
     * delete an account
     * @param  int $id account_id
     * @return mixed     if account is in use then return -1 to display the reason , else true | false
     */
    public function delete_account($id) {
        if (!$this->can_delete($id)) return -1;

        /* $query = "SELECT id FROM client c WHERE c.s_account_id_1st = ? OR c.s_account_id_2nd = ?
                  UNION
                  SELECT id FROM `order` o WHERE o.j_account_id = ?";
        if (!$this->exec_query($query, array($id, $id, $id))) {
            $this->delete('account', array('id' => $id));
            return $this->db->affected_rows();
        }
        return false; */

        $this->delete('account', array('id' => $id));
        return $this->db->affected_rows();
    }

    /**
     * check an account can delete or not?
     * if account is in use in 'order' or 'client' then return false
     * @param  int $id account_id
     * @return boolean
     */
    private function can_delete($id) {
        /* $query = "SELECT EXISTS (
                    SELECT id
                    FROM `order` o
                    WHERE   o.j_account_id = ?
                            OR o.order_approval_account_id = ?
                            OR o.change_report_approval_account_id = ?
                    UNION ALL
                    SELECT id
                    FROM client c
                    WHERE   c.s_account_id_1st = ?
                            OR c.s_account_id_2nd = ?
                            OR c.credit_account_id = ?
                            OR c.approval_account_id = ?
                ) as exist";
        $this->set_single();
        $result = $this->exec_query($query, array($id, $id, $id, $id, $id, $id, $id));
        if ($result['exist']) return false;
        return true; */

        $sql1 = "SELECT o.id
                FROM `order` o JOIN order_detail od
                ON o.id = od.order_id
                WHERE (o.j_account_id = ? OR o.order_approval_account_id = ?
                    OR od.change_report_applicant_account_id = ?
                    OR od.change_report_approval_account_id = ?)
                    AND o.disable = 0 AND od.disable = 0
                    ";
        $result1 = $this->exec_query($sql1, array($id, $id, $id, $id));
        $sql2 = "SELECT id
                FROM `client`
                WHERE (s_account_id_1st = ? OR s_account_id_2nd = ?
                      OR approval_account_id = ? OR applicant_account_id = ?)
                      AND disable = 0";
        $result2 = $this->exec_query($sql2, array($id, $id, $id, $id));
        if(!empty($result1) || !empty($result2)) {
            return false;
        }
        return true;
    }

    /**
     * Get an account business unit info
     * @param  int $id account id
     * @return array()
     * @author Tien
     */
    public function get_account_option_order_acception($id){
        //echo 'sddddddddd'.$id;
        $this->db->select('bu.*')
                    ->from('account a')
                    ->join('department d', 'd.id = a.department_id')
                    ->join('business_unit bu', 'bu.id = d.business_unit_id')
                    // ->join('account ac', 'ac.department_id = d.id','right')
                    ->where('a.id', $id)
                    ->where('bu.disable', 0);
        $data_return  = $this->db->get()->result_array();
        echo '<pre>';
        print_r($data_return);
        return $data_return;


    }

    public function get_account_business_unit($id) {
        $this->db->select('bu.*')
                ->from('account a')
                ->join('department d', 'd.id = a.department_id')
                ->join('business_unit bu', 'bu.id = d.business_unit_id')
                ->where('a.id', $id)
                ->where('bu.disable', 0)
                ->order_by('bu.sequence, bu.id');
        return $this->db->get()->result_array();
    }

    /**
     * list all account of a business unit
     * @param  int $business_unit_id
     * @return array()
     * @author Tien
     */
    public function get_all_account_by_business_unit($business_unit_id) {
        $this->db->select('a.*')
                ->from('account a')
                ->join('department d', 'd.id = a.department_id')
                ->join('business_unit bu', 'bu.id = d.business_unit_id')
                ->where('bu.id', $business_unit_id)
                ->where('a.disable', 0);
        $this->db->order_by('a.id');

        return $this->db->get()->result_array();
    }


    public function get_all_account_by_business_unit_resignation($business_unit_id) {
                $this->db->select('a.*')
                        ->from('account a')
                        ->join('department d', 'd.id = a.department_id')
                        ->join('business_unit bu', 'bu.id = d.business_unit_id')
                        ->where('bu.id', $business_unit_id)
                        ->where('resignation', STATUS_ENABLE);
                $this->db->order_by('a.id');
                $this->db->where('a.disable', 0);
                return $this->db->get()->result_array();
    }
    /**
     * get account by login id
     * @param  string $login_id
     * @return array
     */
    public function get_account_by_login_id($login_id) {
        return $this->db->select('*')
                        ->from('account')
                        ->where('login_id', $login_id)
                        ->get()
                        ->row_array();
    }

     /**
     * list all working account of a business unit
     * @param  int $business_unit_id, array $date
     * @return array()
     * @author Luan
     */
    public function get_sale_target_account_by_business_unit($business_unit_id , $date) {
        $this->db->select('a.name as account_name, a.id as account_id, st.target_amount, d.id as department_id, d.name as department_name,bu.name as business_name')
                ->from('account a')
                ->join('department d', 'd.id = a.department_id')
                ->join('business_unit bu', 'bu.id = d.business_unit_id')
                ->join('sales_target st', 'st.account_id = a.id')
                ->where('bu.id', $business_unit_id)
                ->where('YEAR(`st`.`closing_date`)', $date[0])
                ->where('MONTH(`st`.`closing_date`)', $date[1])
                ->where('st.target_amount <>', 0)
                ->where('st.disable', 0)
                ->where('a.disable', 0)
                ->where('a.resignation', 0);
        return $this->db->get()->result_array();
    }

    public function get_sale_target_account_by_department($department_ids, $date) {
        if(!is_array($department_ids)){
            $department_ids = array($department_ids);
        }
        $this->db->select('a.name as account_name, a.id as account_id, st.target_amount, d.id as department_id, d.name as department_name')
                ->from('account a')
                ->join('department d', 'd.id = a.department_id')
                ->join('sales_target st', 'st.account_id = a.id')
                ->where('YEAR(`st`.`closing_date`)', $date[0])
                ->where('MONTH(`st`.`closing_date`)', $date[1])
                ->where('st.target_amount <>', 0)
                ->where('st.disable', 0)
                ->where('d.id IN ('.implode(',',$department_ids).')')
                ->where('a.disable', 0)
                ->where('a.resignation', 0);

        return $this->db->get()->result_array();
    }

}

