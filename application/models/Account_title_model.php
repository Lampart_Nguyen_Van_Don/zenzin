<?php
/**
 * @created date: 31/July/2015
 * @author van_phong
 *
 */
class Account_title_model extends ZR_Model {
    /**
     *
     * @return multitype:
     */
    public function get_account_title() {
        $sql = "SELECT id, name, calculate_code, support_code, remarks, bugyo_dp_code
                FROM bugyo_cd_manage
                WHERE disable = 0
                ORDER BY id ASC";
        return $this->exec_query ( $sql );
    }

    /**
     *
     * @param unknown $id
     * @return multitype:
     */
    public function get_account_title_by_id($id) {
        $sql = "SELECT id, name, calculate_code, support_code, remarks, bugyo_dp_code
                FROM bugyo_cd_manage
                WHERE disable = 0 AND id = ?";
        $this->set_single();
        return $this->exec_query ( $sql, array($id) );
    }

    /**
     * Update a record
     * @param unknown $table
     * @param unknown $data
     * @param unknown $condition
     */
    public function update_account_title($table, $data, $condition) {
        return $this->update ( $table, $data, $condition );
    }

    /**
     * Rules validation
     * @param unknown $rule_name
     * @return Ambigous <multitype:string multitype:string  >|multitype:
     */
    public function rules($rule_name) {
        // Validate login form
        $validate_rule ['edit_account_title'] = array (
                array(
                        'field' => 'name',
                        'label' => 'name',
                        'rules' => 'max_length[64]',
                        'errors' => array (
                                'max_length'=> '勘定科目名は全角64文字までです。'
                        )
                ),
                array (
                        'field' => 'calculate_code',
                        'label' => 'calculate_code',
                        'rules' => 'trim|required|max_length[4]|check_contain_only_number',
                        'errors' => array (
                                'required' => '勘定科目コードを入力してください。',
                                'check_contain_only_number' => '勘定科目コードには数字以外は入力できません。',
                                'max_length'=> '勘定科目コードは半角数字4桁までです。'
                        )
                ),
                array (
                        'field' => 'support_code',
                        // 'rules' => 'trim|required|max_length[4]|check_contain_only_number',
                        'rules' => 'trim|max_length[4]|check_contain_only_number',
                        'errors' => array (
                                // 'required' => "補助科目コードを入力してください。",
                                'check_contain_only_number' => '補助科目コードには数字以外は入力できません。',
                                'max_length'=> '補助科目コードは半角数字4桁までです。'
                        )
                ),
                array (
                        'field' => 'remarks',
                        'rules' => 'required|max_length[40]',
                        'errors' => array (
                                'required' => '摘要文字列を入力してください。',
                                'max_length'=> ' 摘要文字列は全角40文字までです。'
                        )
                ),
                array (
                        'field' => 'bugyo_dp_code',
                        'rules' => 'required|check_contain_only_number|max_length[4]',
                        'errors' => array (
                                'required' => '部門コードを入力してください。',
                                'check_contain_only_number' => '部門コードには数字以外は入力できません。',
                                'max_length' => "部門コードは半角数字4桁です。"
                        )
                )
        );
        if (isset ( $validate_rule [$rule_name] )) {
            return $validate_rule [$rule_name];
        }
        return array ();
    }
}
