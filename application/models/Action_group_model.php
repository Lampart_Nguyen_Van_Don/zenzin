<?php
class Action_group_model extends ZR_Model {
    public function get_all_name_and_id() {
        $query = "SELECT id, name FROM action_group WHERE disable = 0 ORDER BY sequence";
        return $this->exec_query($query);
    }

}