<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Action_model extends ZR_Model {

    /**
     *
     * @param array
     * @return array
     */
    public function get_actions($filter = array()) {
        $sql = 'SELECT action.*'
               .' FROM action'
               .' INNER JOIN quick_setting'
               .'    ON quick_setting.id = `action`.quick_setting_id'
               .' INNER JOIN action_authority'
               .'    ON action_authority.quick_setting_id = `quick_setting`.id'
               .' INNER JOIN authority'
               .'    ON action_authority.authority_id = authority.id'
               .' INNER JOIN account'
               .'    ON account.authority_id = authority.id'
               .' WHERE account.id = ? AND is_main_action = ' . IS_DISPLAY_MENU
               .'    AND action_authority.disable = ' . STATUS_ENABLE
               .'    AND action.disable = ' . STATUS_ENABLE
               .'    AND authority.disable = ' . STATUS_ENABLE
               .'    AND account.disable = ' . STATUS_ENABLE;
        return $this->exec_query($sql, array($this->auth->get_account_id()));
    }

    /**
     *
     * @param array
     * @return array
     */
    public function get_breadcrumb($filter = array()) {
        $list = array();
        $sql = 'SELECT DISTINCT action.*'
                .' FROM action'
                .' INNER JOIN quick_setting'
                .'    ON quick_setting.id = `action`.quick_setting_id'
                .' INNER JOIN action_authority'
                .'    ON action_authority.quick_setting_id = `quick_setting`.id'
                .' INNER JOIN authority'
                .'    ON action_authority.authority_id = authority.id'
                .' INNER JOIN account'
                .'    ON account.authority_id = authority.id'
                .' WHERE account.id = ? AND is_display_menu = ' . IS_DISPLAY_MENU
                .'    AND action_authority.disable = ' . STATUS_ENABLE
                .'    AND action.disable = ' . STATUS_ENABLE
                .'    AND authority.disable = ' . STATUS_ENABLE
                .'    AND account.disable = ' . STATUS_ENABLE;
        $list[] = $this->auth->get_account_id();

        if (isset($filter['id'])) {
            $sql .= ' AND action.id NOT IN(?)';
            $list[] = $filter['id'];
        }

        if (isset($filter['id'])) {
            $sql .= ' AND action.action_group_id = ?';
            $list[] = $filter['action_group_id'];
        }

        return $this->exec_query($sql, $list);
    }

    public function get_action_by_name($name) {
        $sql = 'SELECT action.*, authority.id AS authority_id, action_group.menu_group_id'
               .' FROM action'
               .' INNER JOIN action_group'
               .'    ON action.action_group_id = action_group.id'
               .' INNER JOIN quick_setting'
               .'    ON `action`.quick_setting_id = quick_setting.id'
               .' INNER JOIN action_authority'
               .'    ON quick_setting.id = `action_authority`.quick_setting_id'
               .' INNER JOIN authority'
               .'    ON action_authority.authority_id = authority.id'
               .' INNER JOIN account'
               .'     ON account.authority_id = authority.id'
               .' WHERE action.method_name = ? AND account.id = ?'
               .'    AND action_group.disable = ' . STATUS_ENABLE
               .'    AND action_authority.disable = ' . STATUS_ENABLE
               .'    AND action.disable = ' . STATUS_ENABLE
               .'    AND authority.disable = ' . STATUS_ENABLE
               .'    AND account.disable = ' . STATUS_ENABLE
               .'    AND quick_setting.disable = ' . STATUS_ENABLE;
        if($actions = $this->exec_query($sql, array($name, $this->auth->get_account_id()))) {
            return $actions[0];
        }
        return array();
    }

    public function get_action_group() {
        return $this->get_items('action_group', 'sequence');
    }

    /**
     *
     * @return array
     */
    public function get_menu_group() {
        return $this->get_items('menu_group', 'sequence ASC');
    }
}