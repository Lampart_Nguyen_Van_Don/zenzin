<?php
/**
 *
 * @author van_phong
 *
 */
class Advance_payment_model extends ZR_Model {
    /**
     *
     * @param unknown $date_from
     * @param unknown $date_to
     * @param unknown $code
     * @param unknown $name
     * @param unknown $abbr_name
     * @return multitype:
     */
    public function search_order_acceptance($date_from, $date_to, $code, $name, $abbr_name) {
//#6806: Start 2015/09/30
        $sql = "
            SELECT *
            FROM
            (
                SELECT
                                    OA.shipping_type_cd,
                                    OA.delivery_date,
                                    OA.close_date,
                                    OA.adjusted_amount,
                                    OA.is_extra_item,
                                    SUB.code,
                                    SUB.name,
                                    SUB.abbr_name,
                                    OA.j_code,
                                    OA.branch_cd,
                                    OA.unit_price,
                                    OA.quantity,
                                    SUB.id,
                                    OA.id AS OAid,
                                    OA.client_name,
                                    OA.order_acceptance_status,
                                    OA.shipping_charge_tax,
                                    OA.source_subcontractor_id, OA.`disable`,
                                    SHT.is_advance_payment, OA.tax_free
                FROM order_acceptance OA
                LEFT JOIN subcontractor SUB ON OA.source_subcontractor_id = SUB.id
                LEFT JOIN shipping_type SHT ON OA.shipping_type_cd = SHT.code AND SHT.`disable` = 0
                WHERE SUB.`disable` = 0
            ) t1
            WHERE t1.disable = 0
                AND t1.OAid NOT IN
                (
                SELECT id
                FROM order_acceptance
                WHERE order_acceptance_status = 2
                AND ((shipping_charge_tax = 0 OR shipping_charge_tax = '') AND (tax_free = 0 OR tax_free = ''))
                AND disable = 0
                )
                AND t1.OAid NOT IN
                (
                    SELECT oa_mas.id
                    FROM order_acceptance oa_mas
                    WHERE oa_mas.adjusted_amount = 0
                    AND oa_mas.close_date!='0000-00-00'
                    AND oa_mas.disable = 0
                    AND oa_mas.source_subcontractor_id = t1.source_subcontractor_id
                    AND (
                        SELECT COUNT(oa_sub.id)
                        FROM order_acceptance oa_sub
                            LEFT JOIN subcontractor SUB ON oa_sub.source_subcontractor_id = SUB.id
                            LEFT JOIN shipping_type SHT ON oa_sub.shipping_type_cd = SHT.code AND SHT.`disable` = 0
                        WHERE oa_sub.source_subcontractor_id = t1.source_subcontractor_id
                        AND oa_sub.disable = 0
                        AND DATE_FORMAT(oa_sub.delivery_date,'%Y/%m') = DATE_FORMAT(t1.delivery_date,'%Y/%m')
                        AND oa_sub.id NOT IN
                                (
                                SELECT id
                                FROM order_acceptance
                                WHERE order_acceptance_status = 2
                                AND ((shipping_charge_tax = 0 OR shipping_charge_tax = '') AND (tax_free = 0 OR tax_free = ''))
                                AND disable = 0
                                )
                             AND ( oa_sub.is_extra_item = 1 OR SHT.is_advance_payment = 1)
                    ) = 1
                )
                AND (t1.is_extra_item = 1 OR t1.is_advance_payment = 1)
        ";
//#6806: End 2015/10/02
        /*
         * Line 57 to line 82: If this month has only one record (close accountant record),
         * and that record has adjusted_amount = 0
         * => No show that record
         */
        // $sql.=" AND OA.source_subcontractor_id = 15";
        // AND (OA.source_subcontractor_id = 15 OR OA.source_subcontractor_id = 14)
        $string_where = "";
        $array_params = array();
        if(!empty($date_from)) {
            $string_where.=" AND t1.delivery_date >= ? ";
            // $string_where.=" AND DATE_FORMAT(OA.delivery_date,'%Y/%m') >= ?";
            $array_params[] = $date_from;
        }
        if(!empty($date_to)) {
            $string_where.= " AND t1.delivery_date <= ? ";
            // $string_where.=" AND DATE_FORMAT(OA.delivery_date,'%Y/%m') <= ?";
            $array_params[] = $date_to;
        }
        if(!empty($code)) {
            $string_where.= " AND t1.code LIKE ? ";
            $array_params[] = $code."%";
        }
        if(!empty($name)) {
            $string_where.= " AND t1.name LIKE ? ";
            $array_params[] = "%".$name."%";
        }
        if(!empty($abbr_name)) {
            $string_where.= " AND t1.abbr_name LIKE ? ";
            $array_params[] = "%".$abbr_name."%";
        }
        // Join two strings

        if(!empty($string_where)) {
            $sql.=$string_where;
        }
        // $sql.=" ORDER BY OA.delivery_date, OA.j_code, OA.branch_cd, OA.id";
        return $this->exec_query($sql, $array_params);
    }

    /**
     * To calculate the first day of the month amount of each subcontractor
     * @param unknown $date_to
     * @return Ambigous <multitype:, mixed>
     */
    public function get_all_order_acceptance_limit_by_time_to($date_to, $code, $name, $abbr_name) {
//#7116: Start 9/Oct/2015
        $sql = "
                SELECT *
            FROM
            (
                SELECT
                                    OA.shipping_type_cd,
                                    OA.delivery_date,
                                    OA.close_date,
                                    OA.adjusted_amount,
                                    OA.is_extra_item,
                                    SUB.code,
                                    SUB.name,
                                    SUB.abbr_name,
                                    OA.j_code,
                                    OA.branch_cd,
                                    OA.unit_price,
                                    OA.quantity,
                                    SUB.id,
                                    OA.id AS OAid,
                                    OA.client_name,
                                    OA.order_acceptance_status,
                                    OA.shipping_charge_tax,
                                    OA.source_subcontractor_id, OA.`disable`,
                                    SHT.is_advance_payment, OA.tax_free
                FROM order_acceptance OA
                LEFT JOIN subcontractor SUB ON OA.source_subcontractor_id = SUB.id
                LEFT JOIN shipping_type SHT ON OA.shipping_type_cd = SHT.code AND SHT.`disable` = 0
                WHERE SUB.`disable` = 0
            ) t1
            WHERE t1.disable = 0 AND t1.OAid NOT IN
                (
                SELECT id
                FROM order_acceptance
                WHERE order_acceptance_status = 2
                AND ((shipping_charge_tax = 0 OR shipping_charge_tax = '') AND (tax_free = 0 OR tax_free = ''))
                AND disable = 0
                ) AND (t1.is_extra_item = 1 OR t1.is_advance_payment = 1)
        ";
//#7116: End 9/Oct/2015
        $string_where = "";
        $array_params = array();
        if(!empty($date_to)) {
            $string_where.= " AND t1.delivery_date <= ?";
            // $string_where.=" AND DATE_FORMAT(OA.delivery_date,'%Y/%m') <= ?";
            $array_params[] = $date_to;
        }
        if(!empty($code)) {
            $string_where.= " AND t1.code LIKE ?";
            $array_params[] = $code."%";
        }
        if(!empty($name)) {
            $string_where.= " AND t1.name LIKE ?";
            $array_params[] = "%".$name."%";
        }
        if(!empty($abbr_name)) {
            $string_where.= " AND t1.abbr_name LIKE ?";
            $array_params[] = "%".$abbr_name."%";
        }
        // Join two strings
        if(!empty($string_where)) {
            $sql.=$string_where;
        }
        $sql.=" ORDER BY t1.delivery_date, t1.j_code, t1.branch_cd, t1.id";
        return $this->exec_query($sql, $array_params);
    }
    /**
     * save_grid to DB
     * @author Phong
     * @created date 17/6/2015
     */
    public function save_grid($grid, $type) {
        if($type=="INSERT") {
            $query = "INSERT INTO order_acceptance
                    (delivery_date, j_code, branch_cd, client_name, unit_price, quantity,
                    adjusted_amount, source_subcontractor_id, lastup_account_id, report_remarks, shipping_type_cd,
                    order_acceptance_status, arrange_rep, create_datetime, lastup_datetime, disable, is_extra_item)
                    VALUES ";
            $list_params = array();
            foreach($grid as $row) {
                    $query.=" (?,?,?,?,?,?,?,?,?,'',?,
                            1,0,NOW(), NOW(), 0, 1),";
                    $list_params[] = $row['delivery_date'];
                    $list_params[] = isset($row['j_code'])? $row['j_code']: "";
                    $list_params[] = isset($row['branch_cd'])? $row['branch_cd']:"";
                    $list_params[] = isset($row['client_name'])? $row['client_name']:"";
                    $list_params[] = isset($row['unit_price'])? $row['unit_price']: 0;
                    $list_params[] = isset($row['quantity'])? $row['quantity']: 0;
                    $list_params[] = isset($row['adjusted_amount'])? $row['adjusted_amount']: 0;
                    $list_params[] = $row['source_subcontractor_id'];//$id_subcontractor;
                    $list_params[] = $this->auth->get_account_id ();
                    $list_params[] = isset($row['shipping_type_cd'])? $row['shipping_type_cd']: "";
            }
            // Remove "," at the last of statement
            $query = substr($query, 0, -1);
            return $this->db->query($query, $list_params);
        } else if($type == "UPDATE") {
            $query = "INSERT INTO order_acceptance
                    (id, delivery_date, client_name,
                    adjusted_amount, lastup_account_id, report_remarks, unit_price, quantity,
                    order_acceptance_status, arrange_rep, lastup_datetime, disable, is_extra_item) VALUES ";
            $list_params = array();
            foreach($grid as $row) {
                $query.=" (?
                        ,?
                        ,?
                        ,?
                        ,?
                        ,''
                        ,?
                        ,?
                        ,1,0, NOW(), 0, 1),";
                $list_params[] = $row['id'];
                $list_params[] = $row['delivery_date'];
                // $list_params[] = isset($row['j_code'])? $row['j_code']: "";
                // $list_params[] = isset($row['branch_cd'])? $row['branch_cd']:"";
                $list_params[] = isset($row['client_name'])? $row['client_name']:"";
                // $list_params[] = (isset($row['unit_price']) && $row['unit_price']!='') ? $row['unit_price']: 0;
                // $list_params[] = (isset($row['quantity']) && $row['quantity']!='')? $row['quantity']: 0;
                $list_params[] = (isset($row['adjusted_amount']) && $row['adjusted_amount']!='') ? $row['adjusted_amount']: 0;
                // $list_params[] = $row['source_subcontractor_id'];
                $list_params[] = $this->auth->get_account_id ();
                $list_params[] = isset($row['unit_price'])? $row['unit_price']: 0;
                $list_params[] = isset($row['quantity'])? $row['quantity']: 0;

            }
            $query = substr($query, 0, -1);
            $query.=" ON DUPLICATE KEY UPDATE
                    delivery_date=VALUES(delivery_date),
                    client_name=VALUES(client_name),
                    adjusted_amount=VALUES(adjusted_amount),
                    lastup_account_id=VALUES(lastup_account_id),
                    unit_price=VALUES(unit_price),
                    quantity=VALUES(quantity)
                    ";
            // print_r($query);
            return $this->db->query($query, $list_params);

        } else if($type == "DELETE") {
            $list_params = array();
            $query = "UPDATE order_acceptance SET disable = 1, lastup_datetime = ?, lastup_account_id = ? WHERE id IN ( ";
            $list_params[] = $this->_db_now();
            $list_params[] = $this->auth->get_account_id();
            foreach($grid as $row) {
                $query.= "?,";
                $list_params[] = $row['id'];
            }
            $query = substr($query, 0, -1);
            $query.= ")";

            return $this->db->query($query, $list_params);
        }
    }

    /**
     * get_accounting_confirm
     * @author: Phong
     * @created date: 18/June/2015
     */
    public function get_accounting_confirm() {
        $query = "SELECT id, close_date AS settled_date FROM closing_accountant WHERE disable = 0";
        return $this->exec_query($query);
    }

    /**
     * Check last month has closed accounting or not. If not, show last month as default when load advance payment
     * @param unknown $last_month
     * @return boolean
     */
    public function check_last_month_closed_accounting($last_month) {
        $query = "SELECT id
                    FROM closing_accountant
                    WHERE disable = 0 AND DATE_FORMAT(close_date,'%Y-%m') = ?";
        $this->set_single();
        $result = $this->exec_query($query, $last_month);
        if(empty($result)) {
            return false;
        }
        return true;
    }

    /**
     * Get nearest closed accounting for checking if has been closed, no add more row in grid
     * @return string
     */
    public function get_nearest_closed_accounting() {
        $query = "SELECT id, DATE_FORMAT(close_date,'%Y') AS year_, DATE_FORMAT(close_date,'%m') as month_
                    FROM closing_accountant
                    WHERE disable = 0
                    ORDER BY close_date DESC LIMIT 1
                ";
        $this->set_single();
        $result = $this->exec_query($query);
        if(!empty($result)) {
            $last_day_of_month = cal_days_in_month ( CAL_GREGORIAN, $result['month_'], $result['year_'] );
            $date = $result['year_']."-".$result['month_']."-".$last_day_of_month;
            return $date;
        } else {
            return "";
        }
    }

    /**
     * get_min_max_delivery_date
     * @return string
     */
    public function get_min_max_delivery_date() {
        $query = "SELECT YEAR(MIN(delivery_date)) AS YEAR_MIN_date, MONTH(MIN(delivery_date)) AS MONTH_MIN_date,
                    YEAR(MAX(delivery_date)) AS YEAR_MAX_date, MONTH(MAX(delivery_date)) AS MONTH_MAX_date
                    FROM order_acceptance
                    WHERE disable = 0 AND source_subcontractor_id != 0 ORDER BY delivery_date
                ";
        $this->set_single();
        $r = $this->exec_query($query);
        // print_r($r);
        $r['MONTH_MIN_date'] = ($r['MONTH_MIN_date'] < 10 && $r['MONTH_MIN_date'] > 0) ? "0".$r['MONTH_MIN_date'] : $r['MONTH_MIN_date'];
        $r['MONTH_MAX_date'] = ($r['MONTH_MAX_date'] < 10 && $r['MONTH_MAX_date'] > 0) ? "0".$r['MONTH_MAX_date'] : $r['MONTH_MAX_date'];
        // print_r($r);
        $date_from = $r['YEAR_MIN_date']."/".$r['MONTH_MIN_date'];
        $date_to = $r['YEAR_MAX_date']."/".$r['MONTH_MAX_date'];

        // int cal_days_in_month ( int $calendar , int $month , int $year )
        // $last_day_of_month = cal_days_in_month ( CAL_GREGORIAN, $parts [1], ($parts [0] + 2000) );

        $time_frame = $r['YEAR_MIN_date']."/".$r['MONTH_MIN_date']."~".$r['YEAR_MAX_date']."/".$r['MONTH_MAX_date'];
        if($time_frame!="/~/") {
            $arr_result = array("date_from"=>$date_from, "date_to"=>$date_to, "time_frame"=>$time_frame);
            return $arr_result;
        }
        return array("date_from"=>"", "date_to"=>"", "time_frame"=>"");
    }
}