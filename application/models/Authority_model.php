<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @author Phong van_don
 *
 */
class Authority_model extends ZR_Model {

    public $authority = null;

    /**
     * get all authority
     */
    public function get_authorities() {
        $query = "SELECT id, name, sequence FROM authority WHERE disable = 0 ORDER BY sequence ASC, id ASC";
        return $this->exec_query($query);
    }

    /**
     * Get authority by id
     * @param int $id
     * @return array
     * @author Phong
     */
    public function get_authority_by_id($id) {
        return $this->get_item_by_id('authority', $id);
    }

    /**
     * get_authority_and_functions_by_id
     * @param unknown $id
     * @author Phong
     */
    public function get_authority_and_functions_by_id($id) {
        $query = "      SELECT id, action_group_id, name,
                        CASE
                        WHEN id IN
                        (
                            SELECT  ac_au.action_id AS id
                            FROM
                             (authority au    INNER JOIN action_authority ac_au ON au.id = ac_au.authority_id)
                            WHERE au.id = ? AND au.disable = 0 AND ac_au.disable = 0
                        )
                       THEN 1
                       ELSE 0
                       END AS 'in_or_not'
                       FROM action
                       WHERE disable = 0 ORDER BY id
                ";
        return $this->exec_query($query, array($id));
    }

    /**
     * get_authority_name_by_id
     * @param
     * @author Phong
     */
    public function get_authority_name_by_id($id) {
        $query = "SELECT id, name FROM authority WHERE disable = 0 AND id = ?";
        $this->set_single ();
        return $this->exec_query($query, array($id));
    }

    /**
     * get_function_name_and_id
     * @param unknown $id
     * @return multitype:
     */
    public function get_function_name_and_id($id) {
        $query = "SELECT id, name FROM action WHERE disable = 0 AND id = ?";
        $this->set_single ();
        return $this->exec_query($query, array($id));
    }

    /**
     * delete_functions_of_authority_by_id
     * @param int $id
     * @return boolean
     * @author Phong
     */
    public function delete_functions_of_authority_by_id($author_id, $authority_id) {
        $query = " UPDATE action_authority"
                ."     SET lastup_account_id = ?,"
                ."     lastup_datetime = now(), disable = 1"
                ." WHERE authority_id = ?";
        return $this->db->query($query, array($author_id, $authority_id));
    }

    /**
     * Remove authority by authority_id and quick_setting_id
     * @param int $author_id
     * @param int $authority_id
     * @param array $quick_setting_id
     */
    public function remove_authority($author_id, $authority_id, $quick_setting_id) {
        $query = " UPDATE action_authority"
                ."     SET lastup_account_id = ?,"
                ."     lastup_datetime = now(), disable = 1"
                ." WHERE";
                $list = array();
                $list[] = $author_id;
                if (is_array($authority_id)) {
                    $query .= " authority_id IN(" .implode(',', $authority_id) . ")";
                } else {
                    $query .= " authority_id = ?";
                    $list[] = $authority_id;
                }

                if (is_array($quick_setting_id)) {
                    $query .= " AND quick_setting_id IN(" . implode(',', $quick_setting_id) . ")";
                } else {
                    $query .= " AND quick_setting_id = ?";
                    $list[] = $quick_setting_id;
                }

        return $this->db->query($query, $list);
    }

    /**
     * delete_authorities_of_function_by_id
     * @param unknown $author_id
     * @param unknown $function_id
     * @author Phong
     */
    public function delete_authorities_of_function_by_id($author_id, $function_id) {
        $query = "UPDATE action_authority SET lastup_account_id = ?, lastup_datetime = now(), disable = 1
                  WHERE quick_setting_id = ?";
        return $this->db->query($query, array($author_id, $function_id));
    }

    /**
     * insert_functions_of_authority_by_id
     * @param unknown $id_au
     * @param unknown $id_fu
     * @author Phong
     */
    public function insert_functions_of_authority_by_id($id_authority, $list_values = array()) {
        if(!empty($list_values)) {
            $query = "INSERT INTO action_authority(authority_id, quick_setting_id, create_datetime, lastup_datetime, disable) VALUES ";
            $list_params = array();
            foreach($list_values as $value) {
                $query.=" (?,?,NOW(), NOW(), 0),";
                $list_params[] = $id_authority;
                $list_params[] = $value;
            }
            $query = substr($query, 0, -1);
            return $this->db->query($query, $list_params);
        }
        return null;
    }

    /**
     * insert_authorities_of_function_by_id
     * @param unknown $function_id
     * @param unknown $list_authorities
     * @return NULL
     * @author Phong
     */
    public function insert_authorities_of_function_by_id($function_id, $list_authorities = array()) {
        if(!empty($list_authorities)) {
            $query = "INSERT INTO action_authority(authority_id, quick_setting_id, create_datetime, lastup_datetime, disable) VALUES ";
            $list_params = array();
            foreach($list_authorities as $value) {
                $query.=" (?,?,NOW(), NOW(), 0),";
                $list_params[] = $value;
                $list_params[] = $function_id;
            }
            $query = substr($query, 0, -1);

            return $this->db->query($query, $list_params);
        }
        return null;
    }

    /**
     * Insert a authority
     * @param array $data
     * @return int
     * @author Phong
     */
    public function insert_authority($data) {
        return $this->insert('acount', $data);
    }

    /**
     * check_authority_exist
     * @param unknown $id
     * @return Ambigous <multitype:, mixed>
     * @author Phong
     */
    public function check_authority_exist($id) {
        $query = "SELECT id FROM authority WHERE id = ?";
        // $this->set_single();
        return $this->exec_query($query, array($id));
    }

    /**
     * check_accounts_constrain_authority
     * @param unknown $id
     * @return boolean
     * @author Phong
     */
    public function check_accounts_constrain_authority($id) {
        $query = "SELECT id FROM account WHERE authority_id = ? AND disable = 0";
        $list = array_filter($this->exec_query($query, array($id)));
        return empty($list)? true : false;
    }

    /**
     * get_all_actions and what authority can interact to it by group action id
     * @return Ambigous <multitype:, mixed>
     * @author Phong
     */
    public function get_all_actions_by_group_action_id($id) {
        // $query = "SELECT id, name, is_display_menu FROM action WHERE disable = 0";
        $query = "SELECT gr.id AS group_action_id, gr.name AS group_action_name, t2.*
                FROM action_group gr INNER JOIN
                    (SELECT ac.id, ac.name, ac.action_group_id, ac.is_display_menu, ac_au_re.authority
                    FROM action ac LEFT JOIN
                        (SELECT action_id,GROUP_CONCAT(DISTINCT authority_id
                        ORDER BY  authority_id ASC SEPARATOR ',') AS 'authority'
                        FROM action_authority
                        GROUP BY action_id
                        ) ac_au_re ON ac.id = ac_au_re.action_id
                    WHERE ac.disable = 0
                    ) t2 ON gr.id = t2.action_group_id
                WHERE gr.id = ?
                ";
        return $this->exec_query($query, array($id));
    }

    /**
     * get_first_id_of_group_action
     * @author Phong
     * @return multitype:
     */
    public function get_first_id_of_group_action() {
        $query = "SELECT id FROM action_group
                  WHERE disable = 0
                  ORDER BY id ASC
                  LIMIT 1;";
        $this->set_single();
        return $this->exec_query($query);
    }
    /**
     * get_all_group_actions
     * @return Ambigous <multitype:, mixed>
     * @author Phong
     */
    public function get_all_action_groups() {
        $query = "SELECT id, name FROM action_group WHERE disable = 0 ORDER BY sequence";
        return $this->exec_query($query);
    }

    /**
     * get_authorities_of_action
     * @author Phong
     */
    public function get_authorities_of_action($id) {
        $query = "SELECT id, name,
                CASE
                WHEN id IN (
                SELECT ac_au.authority_id AS id
                FROM action_authority ac_au
                LEFT JOIN authority au ON ac_au.authority_id = au.id
                WHERE ac_au.action_id = ? AND ac_au.disable = 0
                )
                THEN '1'
                ELSE '0'
                END AS 'IN_OR_NOT'
                FROM authority
                WHERE disable = 0
                ";
        return $this->exec_query($query, array($id));
    }

    /**
     * Get all action authority with condition is authority id
     * @param int $id
     * @return array
     * @author van_don
     */
    public function get_action_authorities_by_authority_id($authority_id) {
        $query = " SELECT *"
                ." FROM action_authority"
                ." WHERE disable = 0"
                ."     AND authority_id = ?";
        return $this->exec_query($query, array($authority_id));
    }

    /**
     * Get all authority group action_goup id
     * @param int $id
     * @return array
     * @author van_don
     */
    public function get_authority_group_by_action_group_id($action_group_id) {
        $query = " SELECT quick_setting.*, action_authority.authority_id"
                ." FROM action_authority"
                ."    INNER JOIN quick_setting"
                ."    ON (action_authority.quick_setting_id = quick_setting.id)"
                ." WHERE action_authority.disable = 0"
                ."    AND quick_setting.disable = 0"
                ."    AND quick_setting.action_group_id = ?";
        return $this->exec_query($query, array($action_group_id));
    }

    /**
     * Get all authority by params
     * @param int $id
     * @return array
     * @author van_don
     */
    public function get_authority_by_params($params = array()) {
        $query = " SELECT quick_setting.*, action_authority.authority_id"
                ." FROM action_authority"
                ."    INNER JOIN quick_setting"
                ."    ON (action_authority.quick_setting_id = quick_setting.id)"
                ." WHERE action_authority.disable = 0"
                ."    AND quick_setting.disable = 0";

        $list = array();
        if (isset($params['action_group_id'])) {
            $query .="    AND quick_setting.action_group_id = ?";
            $list[] = $params['action_group_id'];
        }

        return $this->exec_query($query, $list);
    }

    /**
     * Get quick setting by action_group_id
     * @param int $id
     * @return array
     * @author van_don
     */
    public function get_quick_setting_by_action_group_id($action_group_id) {
        $query = " SELECT *"
                ." FROM quick_setting"
                ." WHERE quick_setting.disable = 0"
                ."    AND quick_setting.action_group_id = ?";
        return $this->exec_query($query, array($action_group_id));
    }

    /**
     * Get quick setting by params
     * @param int $id
     * @return array
     * @author van_don
     */
    public function get_quick_setting_by_params($params = array()) {
        $query = " SELECT *"
                ." FROM quick_setting"
                ." WHERE quick_setting.disable = 0";

        $list = array();
        if (isset($params['action_group_id']) && ($params['action_group_id'] != '')) {
            $query .="    AND quick_setting.action_group_id = ?";
            $list[] = $params['action_group_id'];
        }

        return $this->exec_query($query, $list);
    }

    /**
     * Get action authority by quick setting id
     * @param int $id
     * @return array
     * @author van_don
     */
    public function get_action_authority_by_quick_setting_id($quick_setting_id) {

        $query = " SELECT *"
                ." FROM action_authority"
                ." WHERE action_authority.disable = 0";
        $list = array();
        if (is_array($quick_setting_id)) {
            $query .= " AND action_authority.quick_setting_id IN (" . implode(',', $quick_setting_id) . ")";
        } else {
            $query .= " AND action_authority.quick_setting_id = ?";
            $list[] = $quick_setting_id;
        }

        return $this->exec_query($query, $list);
    }

    /**
     * Get list of authority id that have permission to action method name or quick setting column name
     *
     * @param string $name search by action.method_name or quick_setting.column_name
     * @param string $name_type 'method' or 'column', default is 'column'
     * @return array|bool
     * @author quang_duc
     */
    public function get_authority_by_quick_setting($name, $name_type = 'column') {

        $this->db->select('authority_id')
                 ->join('quick_setting', 'action_authority.quick_setting_id = quick_setting.id AND quick_setting.disable = ' . STATUS_ENABLE)
                 ->where('action_authority.disable', STATUS_ENABLE);

        if ($name_type == 'method') {
            $this->db->join('action', 'quick_setting.id = action.quick_setting_id AND action.disable = ' . STATUS_ENABLE, 'left')
                     ->where('action.method_name', $name);
        } elseif ($name_type == 'column') {
            $this->db->where('quick_setting.column_name', $name);
        } else {
            return false;
        }

        $result = $this->db->get('action_authority')
                           ->result_array();

        // Remove duplicates
        $authority_list = array();
        foreach ($result as $row) {
            $authority_list[$row['authority_id']] = true;
        }

        return array_keys($authority_list);
    }

    /**
     * Get all method name and column name of an authority
     *
     * @param int $authority_id
     * @return array(
     *      'column_name' => array(),
     *      'method_name' => array()
     * )
     *
     * @author quang_duc
     */
    public function get_permission($authority_id) {

        $result = $this->db->select('action.method_name, quick_setting.column_name')
                           ->join('quick_setting', 'action_authority.quick_setting_id = quick_setting.id AND quick_setting.disable = ' . STATUS_ENABLE)
                           ->join('action', 'quick_setting.id = action.quick_setting_id AND action.disable = ' . STATUS_ENABLE, 'left')
                           ->where('action_authority.authority_id', $authority_id)
                           ->where('action_authority.disable', STATUS_ENABLE)
                           ->get('action_authority')
                           ->result_array();

        $authority = array(
            'column_name' => array(),
            'method_name' => array(),
        );

        foreach ($result as $row) {
            if (trim($row['column_name'])) {
                $authority['column_name'][] = $row['column_name'];
            }
            if (trim($row['method_name'])) {
                $authority['method_name'][] = $row['method_name'];
            }
        }

        // Remove duplicate values
        $authority['column_name'] = array_keys(array_flip($authority['column_name']));
        $authority['method_name'] = array_keys(array_flip($authority['method_name']));

        return $authority;
    }

    /**
     * Check if an authority id has permission to access or do something
     *
     * @param int $authority_id
     * @param string $name search by action.method_name or quick_setting.column_name
     * @param string $name_type 'method' or 'column', default is 'method'
     * @return bool
     *
     * @author quang_duc
     */
    public function check_permission($authority_id, $name, $name_type = 'method') {

        if ($this->authority === null) {
            $this->authority = $this->get_permission($authority_id);
        }

        if (isset($this->authority[$name_type . '_name'])) {
            return in_array($name, $this->authority[$name_type . '_name']);
        } else {
            log_message('error', __FILE__ . '('.__LINE__.') ' . __FUNCTION__ . '(): Wrong $name_type, must be "method" or "column".');
        }

        return false;
    }

    /**
     * Put validation rules here
     * @author Phong
     */
    public function rules($rule_name, $params = '') {
        // Validate login form
        $validate_rule ['edit_authority'] = array (
                array (
                        'field' => 'authority_name',
                        'label' => lang('lbl_authority_name_table'),
                        'rules' => 'required'. (($params) ? '|is_unique[authority.name]' : ''),
                        'errors' => array (
                                'required' => lang('error_null_authority_name'),
                                'is_unique' => lang('error_authority_exists')
                        )

                )
        );
        if (isset ( $validate_rule [$rule_name] )) {
            return $validate_rule [$rule_name];
        }
        return array ();
    }

    /**
     * End validation rules
     */
}