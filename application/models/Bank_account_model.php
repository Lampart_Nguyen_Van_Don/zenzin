<?php
if (! defined ( 'BASEPATH' ))
    exit ( 'No direct script access allowed' );
class Bank_account_model extends ZR_Model {

    public function get_bank_account() {
        $sql = "SELECT id, financial_institution_name, branch_name, account_number, account_holder
                FROM zbn_account
                WHERE disable = 0";
        $this->set_single();
        return $this->exec_query($sql);
    }

    public function get_bank_account_by_id($id) {
        $sql = "SELECT id, financial_institution_name, branch_name, account_number, account_holder, bank_accountcol
                FROM zbn_account
                WHERE id = ? AND disable = 0";
        $this->set_single();
        return $this->exec_query($sql, $sql, array($id));
    }


    public function update_bank_account($table, $data, $condition) {
        return $this->update($table, $data, $condition);
    }

    public function rules($rule_name) {
        // Validation bank account
        $validate_rule ['edit_bank_account'] = array(
            array(
                'field' => 'bank_name',
                'label' => lang('lbl_bank_name'),
                'rules' => 'trim|required|max_length[128]',
                'errors' => array(
                    'required' => lang('bank_name_required')
                )
            ),
            array(
                'field' => 'branch_name',
                'label' => lang('lbl_branch_name'),
                'rules' => 'trim|required|max_length[128]',
                'errors' => array(
                    'required' => lang('branch_name_required')
                )
            ),
            array(
                'field' => 'account_number',
                'label' => lang('lbl_account_number'),
                'rules' => 'trim|required|max_length[128]',
                'errors' => array(
                    'required' => lang('account_number_required')
                )
            ),
            array(
                'field' => 'account_holder',
                'label' => lang('lbl_account_holder'),
                'rules' => 'trim|required|max_length[128]',
                'errors' => array(
                    'required' => lang('account_holder_required')
                )
            ),
        );

        if (isset($validate_rule[$rule_name])) {
            return $validate_rule[$rule_name];
        }
        return array();
    }

}
