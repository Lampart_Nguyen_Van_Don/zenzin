<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Business_unit_model extends ZR_Model {

    /**
     * Get id, name to show in department management
     * @return array with id, name column
     */
    public function get_business_unit_id_name() {
        $query = " SELECT id, name"
                ." FROM business_unit"
                ." WHERE disable = 0"
                //." ORDER BY name ASC";
        		."  ORDER BY sequence ASC, id ASC ";
        return $this->exec_query ( $query );
    }

    /**
     * Get id, name to show in department management
     * @return array with id, name column
     */
    public function get_product_business_unit_id_name() {
        $query = " SELECT id, name"
            ." FROM business_unit"
            ." WHERE is_product_division = 1"
            ." AND disable = 0"
            //." ORDER BY name ASC";
        	."  ORDER BY sequence ASC, id ASC ";
        return $this->exec_query ( $query );
    }

    /**
     * Get all column with data in business unit by id
     * @return array with all column data
     */
    public function get_business_unit_by_id($id) {
        $sql = " SELECT id,
                        name as name,
                        j_code,
                        bugyo_department_code,
                        bugyo_account_code,
                        bugyo_abstract_string,
                        product_manage_code, template,
                        sequence, is_product_division,
                        totalization_target_months,
                        finish_work_report_template"

              ." FROM business_unit"
              ." WHERE id = ?"
              ." AND disable = " . STATUS_ENABLE;

        $result =  $this->exec_query($sql, array($id));

        if(!empty($result)) {
            return $result[0];
        }
        return false;         
    }

    /**
     * Delete one row in table business unit by id
     * params: $id
     * @return true if success
     * @return false if fail
     */
    public function delete_business_unit_by_id($id) {
        return $this->delete('business_unit', array('id' => $id));
    }

    /**
     * Get id, name column data in table business unit
     * @return array
     */
    public function list_business_unit() {
        $sql = "SELECT id, name
                FROM business_unit
                WHERE disable = 0
                ORDER BY sequence ASC, id ASC";

        return $this->exec_query($sql);
    }

    /**
     * Get all data from table business unit with status not disable
     * @return array
     */
    public function get_all() {
        return $this->get_items('business_unit', 'sequence, id');
    }

    /**
     * list all business unit that have account
     * @return array
     * @author  cam_tien
     */
    public function get_all_business_unit_have_account() {
        return $this->db->select('
                          DISTINCT(bu.id)
                        , bu.name
                            ')
                        ->from('business_unit bu')
                        ->join('department d', 'd.business_unit_id = bu.id')
                        ->join('account a', 'a.department_id = d.id')
                        ->where('bu.disable', 0)
                        ->where('d.disable', 0)
                        ->where('a.disable', 0)
                        ->order_by('sequence, id')
                        ->get()
                        ->result_array();
    }

    /**
     * Set rules for add form. Used when validate form add
     * @param unknown $rule_name
     * @return $validate_rule[$rule_name] if isset $rule_name
     * @return array empty if not isset $rule_name
     */
    public function rules($rule_name, $is_val = true) {
        $validate_rule['add_business_unit'] = array(
            array(
                'field' => 'txt_bu_name',
                'label' => lang('lbl_business_unit_name'),
                'rules' => 'trim|required|max_length[128]' . (($is_val) ? '|is_unique[business_unit.name]' : ''),
                'errors' => array(
                    'required' => lang('txt_bu_name_required'),
                    'is_unique' => lang('txt_bu_name_unique')
                )
            ),
            array(
                'field' => 'txt_is_product_div',
                'label' => lang('lbl_is_product_div'),
                'rules' => 'in_list[0,1]',
                'errors' => array(
                    'in_list' => lang('lbl_invalid_input')
                )
            ),
            array(
                'field' => 'txt_target_months',
                'label' => lang('lbl_target_months'),
                'rules' => 'trim|required|numeric|integer|max_length[2]|in_list[1,01,2,02,3,03,4,04,5,05,6,06,7,07,8,08,9,09,10,11,12]',
                'errors' => array(
                    'in_list' => lang('lbl_invalid_month'),
                    'required' => lang('txt_target_months_required')
                )
            ),
            array(
                'field' => 'txt_j_code',
                'label' => lang('lbl_j_code'),
                'rules' => 'trim|required|max_length[1]|alpha_numeric',
                'errors' => array(
                    'required' => lang('txt_j_code_required')
                )

            ),
            array(
                'field' => 'txt_bugyo_department_code',
                'label' => lang('lbl_bugyo_code'),
                'rules' => 'trim|required|numeric|is_natural|exact_length[2]',
                'errors' => array(
                    'required' => lang('txt_bugyo_department_code_required')
                )

            ),
            array(
                'field' => 'txt_bugyo_account_code',
                'label' => lang('lbl_bugyo_account_code'),
                'rules' => 'trim|required|max_length[4]|numeric|is_natural',
                'errors' => array(
                    'required' => lang('txt_bugyo_account_code_required')
                )
            ),
            array(
                'field' => 'txt_bugyo_abstract_string',
                'label' => lang('lbl_bugyo_abstract_string'),
                'rules' => 'trim|required|max_length[128]',
                'errors' => array(
                    'required' => lang('txt_bugyo_abstract_string_required')
                )
            ),
            array(
                'field' => 'txt_product_mn_code',
                'label' => lang('lbl_good_mn_code'),
                'rules' => 'trim|required|max_length[2]|numeric|is_natural|exact_length[2]',
                'errors' => array(
                    'required' => lang('txt_product_mn_code_required'),
                    'exact_length' => lang('txt_product_mn_code_length')
                )

            ),
            array(
                'field' => 'rtb_template',
                'label' => lang('lbl_template'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('rtb_template_required')
                )
            ),
            array(
                'field' => 'work_finish_report_template',
                'label' => lang('lbl_work_finish_report_template'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('work_finish_report_template')
                )
            ),
        );

        $validate_rule['business_unit_one_rule'] = array(
            array(
                'field' => 'txt_bu_name',
                'label' => lang('lbl_business_unit_name'),
                'rules' => 'trim|required|max_length[128]' . (($is_val) ? '|is_unique[business_unit.name]' : ''),
                'errors' => array(
                    'required' => lang('txt_bu_name_required'),
                    'is_unique' => lang('txt_bu_name_unique')
                )

            )
        );

        if (isset($validate_rule[$rule_name])) {
            return $validate_rule[$rule_name];
        }
        return array();
    }

    /**
     * Check is product_divison
     * @param int $business_id
     * @return boolean
     */
    public function is_product_division($business_id) {
        $sql = " SELECT *"
              ." FROM business_unit"
              ." WHERE id = ?"
              ." AND is_product_division = 1"
              ." AND disable = " . STATUS_ENABLE;
        $result =  $this->exec_query($sql, array($business_id));
        if(!empty($result)) {
            return true;
        }

        return false;
    }

//#9695:Start
    /**
     * get list of business unit use in order acceptance search filter
     * @param  int|string $template_code
     * @return array
     */
    public function get_business_unit_for_order_acceptance($template_code) {
        return $this->db->select('id, name')
                        ->from('business_unit')
                        ->where('is_product_division', 1)
                        ->where('disable', 0)
                        ->where('finish_work_report_template', $template_code)
                        ->order_by('id ASC')
                        ->get()
                        ->result_array();
    }

    /**
     * get list of business unit that the same template code
     * @param  int|string $template_code
     * @param  string $column
     * @return array
     */
    public function get_all_business_unit_by_template_code($template_code, $column = 'id') {
        return $this->db->select($column)
                ->from('business_unit')
                ->where('disable', 0)
                ->where('finish_work_report_template', $template_code)
                ->order_by('id ASC')
                ->get()
                ->result_array();
    }
//#9695:End
}
