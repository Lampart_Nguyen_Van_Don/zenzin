<?php
class Client_model extends ZR_Model {
    public $s_code = '';
    public $status = '0';
    public $corporate_status_before = '0';
    public $corporate_status_after = '0';
    public $name = '';
    public $name_kana = '';
    public $zipcode = '';
    public $address_1st = '';
    public $address_2nd = '';
    public $representative_name = '';
    public $mail_address = '';
    public $phone_no = '';
    public $fax_no = '';
    public $business_category = '';
    public $division_name = '';
    public $charge_name = '';
    public $s_account_id_1st = '0';
    public $s_account_id_2nd = '0';
    public $closing_date = '0';
    public $payment_month_cd = '';
    public $payment_day_cd = '0';
    public $delivery_month_cd = '';
    public $delivery_amount = '';
    public $gross_profit_amount = '';
    public $gross_profit_rate = '';
    public $total_delivery_amount = '';
    public $total_gross_profit_amount = '';
    public $total_gross_profit_rate = '';
    public $remark = '';
    public $credit_level = '0';
    public $is_ad_receive_payment_request = '0';
    public $credit_amount = '0';
    public $comment = '';
    public $credit_account_id = '0';
    public $credit_date = '0000-00-00';
    public $is_ad_receive_payment = '0';
    public $approval_account_id = '0';
    public $issue_s_code_date = '0000-00-00';
    public $lastup_account_id = '0';
    public $create_datetime = '0000-00-00 00:00:00';
    public $lastup_datetime = '0000-00-00 00:00:00';
    public $disable = 0;
    public $error = '';
    protected $status_options = null;
    protected $credit_level_options = null;
    protected $payment_closing_date_options = null;
    protected $payment_month_options = null;
    protected $payment_date_options = null;
    protected $company_title_options = null;
    protected $business_category_options = null;
    protected $auth_data = null;

    const LIMIT = 50;
    
    /**
     * Get client status options by status code
     *
     * @param string $code
     *            - If null, return all status options
     * @return array|boolean - Return false if code is not exists
     *
     * @author quang_duc
     */
    public function status_options($code = null) {
        return $this->config_options ( 'client_status', $this->status_options, $code );
    }

    /**
     * Get credit level options by code
     *
     * @param string $code
     *            - If null, return all status options
     * @return array|boolean - Return false if code is not exists
     *
     * @author quang_duc
     */
    public function credit_level_options($code = null) {
        return $this->config_options ( 'credit_level', $this->credit_level_options, $code );
    }

    /**
     * Get payment closing date options by code
     *
     * @param string $code
     *            - If null, return all status options
     * @return array|boolean - Return false if code is not exists
     *
     * @author quang_duc
     */
    public function payment_closing_date_options($code = null) {
        return $this->config_options ( 'payment_closing_date', $this->payment_closing_date_options, $code );
    }

    /**
     * Get payment month options by code
     *
     * @param string $code
     *            - If null, return all status options
     * @return array|boolean - Return false if code is not exists
     *
     * @author quang_duc
     */
    public function payment_month_options($code = null) {
        return $this->config_options ( 'payment_month', $this->payment_month_options, $code );
    }

    /**
     * Get payment date options by code
     *
     * @param string $code
     *            - If null, return all status options
     * @return array|boolean - Return false if code is not exists
     *
     * @author quang_duc
     */
    public function payment_date_options($code = null) {
        return $this->config_options ( 'payment_date', $this->payment_date_options, $code );
    }

    /**
     * Get company title options by code
     *
     * @param string $code
     *            - If null, return all status options
     * @return array|boolean - Return false if code is not exists
     *
     * @author quang_duc
     */
    public function company_title_options($code = null) {
        return $this->config_options ( 'company_title', $this->company_title_options, $code );
    }

    /**
     * Get business category options by code
     *
     * @param string $code
     *            - If null, return all status options
     * @return array|boolean - Return false if code is not exists
     *
     * @author quang_duc
     */
    public function business_category_options($code = null) {
        return $this->config_options ( 'business_category', $this->business_category_options, $code );
    }

    /**
     * Get config options from gui_parts_element table by code
     *
     * @param string $config_name
     *            - column_name of gui_parts table
     * @param string $cache_var
     *            - variable to save the query result for future access
     * @param string $code
     *            - If null, return all status options
     * @return array|boolean - Return false if code is not exists
     *
     * @author quang_duc
     */
    protected function config_options($config_name, &$cache_var, $code) {
        if ($cache_var === null) {
            $this->load->model ( 'gui_parts_element_model' );
            $code_data = $this->gui_parts_element_model->get_by_gui_parts_column_name ( $config_name, 'code, name' );
            $cache_var = map_element ( $code_data, 'code', 'name' );
        }

        if ($code === null) {
            return $cache_var;
        }

        return isset ( $cache_var [$code] ) ? $cache_var [$code] : false;
    }

    /**
     * Merge company title to name
     *
     * @param string $name
     * @param int $title_before
     *            - corporate_status_before
     * @param int $title_after
     *            - corporate_status_after
     * @return string
     *
     * @author quang_duc
     */
    public function name_with_title($name, $title_before = 0, $title_after = 0) {
        if ($title_before > CLIENT_CORPORATE_STATUS_NONE)
            $name = $this->company_title_options ( $title_before ) . $name;
        elseif ($title_after > CLIENT_CORPORATE_STATUS_NONE)
            $name = $name . $this->company_title_options ( $title_after );

        return $name;
    }

    /**
     * Get client data
     *
     * @param bool $return_query
     * @return array
     * @author quang_duc
     */
    public function get_data($return_query = FALSE, $limit = null) {
            $this->load->model('gui_parts_model');
            
            if (!empty($limit)) {
                $this->db->limit($limit, 0);
            }
            
            $query = $this->db->select('client.*, acc_1.name AS acc_1_name')//, gui_parts_element.name AS gui_part_element_name, ')
                    ->from('client')
                    ->where('client.disable', STATUS_ENABLE)
                    //->join('gui_parts_element', 'gui_parts_element.code = `client`.`status` AND gui_parts_element.`disable` = 0', 'INNER', false)
                    //->join('gui_parts', 'gui_parts.id = gui_parts_element.gui_parts_id AND gui_parts.column_name = "' . gui_parts_model::COLUMN_NAME_CLIENT_STATUS . '" AND gui_parts.disable = 0', 'INNER')
                    ->join('account acc_1', 'acc_1.id = `client`.s_account_id_1st AND acc_1.`disable` = ' . STATUS_ENABLE, 'INNER', false)
                    ->get();

            return $return_query ? $query : $query->result_array();
    }

    /**
     * Order client data by s_code ASC, if s_code is blank, order by id
     * (This is a method chaining function and must be used with get_data() function)
     *
     * Ex: $client = $this->client_model->filter()->order_by_s_code()->get_data()
     *
     * @return Client_model
     *
     * @author quang_duc
     */
    public function order_by_s_code() {
        $this->db->order_by ( 'IF(s_code = "" OR s_code is null, 1, 0)', '', false )->order_by ( 's_code' )->order_by ( 'id' );
        return $this;
    }

    /**
     * Filter client data
     * (This is a method chaining function and must be used with get_data() function)
     *
     * Ex: $client = $this->client_model->filter()->order_by_s_code()->get_data()
     *
     * @param array $params
     *            - Filter conditions, array keys must be prefix with 'search_' or it will be ignored
     * @return Client_model
     *
     * @author quang_duc
     */
    public function filter($params) {
        foreach ( $params as $field => $value ) {
//#6984:Start
            if (($value != '') && (strpos ( $field, 'search_' ) === 0)) {
//#6984:End              
                $field = substr ( $field, 7 );
                switch ($field) {
                    case 'status' :
                        $this->db->where_in ( $field, $value );
                        break;
                    case 's_code' :
                        $this->db->like ( $field, $value, 'after' );
                        break;
                    case 'name' :
                    case 'name_kana' :
                    case 'division_name' :
                    case 'phone_no' :
                        $this->db->like ( 'client.' . $field, $value );
                        break;
                    case 'zipcode' :
                        $value = str_replace ( '-', '', $value );
                        $this->db->where ( 'client.zipcode', $value );
                        break;
                    case 'business' :
//#6994:Start
                        if ($value > 0) {
                            if (!$params ['search_account']) {
                                $this->db->join('account', '(s_account_id_1st = account.id OR s_account_id_2nd = account.id) AND account.disable = 0', '', false)->join('department', 'account.department_id = department.id AND department.disable = 0')->where('business_unit_id', $value)->distinct();
                            }
                        }
                        break;
                    case 'account' :
                        if ($value > 0) {
                            $this->db->where('(s_account_id_1st = "' . $value . '" OR s_account_id_2nd = "' . $value . '")');
                        }
//#6994:End
                        break;
                    case 'id' :
                    case 'closing_date' :
                    case 'payment_month_cd' :
                    case 'payment_day_cd' :
                    case 'is_ad_receive_payment_request' :
                        $this->db->where ( 'client.' . $field, $value );
                        break;
                    case 'issue_s_code_date_from' :
                        if (! $params ['search_issue_s_code_date_to']) {
                            if ($value = parse_date ( $value )) {
                                if (substr_count ( $value, '-' ) == 2) {
                                    $this->db->where ( 'issue_s_code_date >=', $value );
                                } else {
                                    $date = new DateTime ( $value . '-01' );
                                    $this->db->where ( 'issue_s_code_date BETWEEN "' . $date->format ( 'Y-m-d' ) . '" AND "' . $date->format ( 'Y-m-t' ) . '"' );
                                }
                            } else {
                                $this->db->where ( '1 = 0' );
                            }
                        }
                        break;
                    case 'issue_s_code_date_to' :
                        if ($params ['search_issue_s_code_date_from']) {
                            $date_from = parse_date ( $params ['search_issue_s_code_date_from'] );
                            $date_to = parse_date ( $params ['search_issue_s_code_date_to'] );
                            if (substr_count ( $date_from, '-' ) == 2 && substr_count ( $date_to, '-' ) == 2) {
                                $this->db->where ( 'issue_s_code_date BETWEEN "' . $date_from . '" AND "' . $date_to . '"' );
                            } else {
                                $this->db->where ( '1 = 0' );
                            }
                        } else {
                            if ($value = parse_date ( $value )) {
                                if (substr_count ( $value, '-' ) == 2) {
                                    $this->db->where ( 'issue_s_code_date <=', $value );
                                } else {
                                    $this->db->where ( '1 = 0' );
                                }
                            } else {
                                $this->db->where ( '1 = 0' );
                            }
                        }
                }
            }
        }

        return $this;
    }

    /**
     * Get client_bank_account_info by client id
     *
     * @param array|int $client_id
     *            - one or many client id
     * @param string|array $select
     *            - select column, select '*' if ommited
     *
     * @return array|boolean
     *
     * @author quang_duc
     */
    public function get_bank_account_info($client_id = array()) {
        if (! is_array ( $client_id )) {
            if (is_numeric ( $client_id )) {
                $client_id = array (
                        $client_id
                );
            } else {
                log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): ID is null or not a number.' );
                return false;
            }
        }

        if (! empty ( $client_id )) {
            $this->db->where_in ( 'client_id', $client_id );
        }

        $query = $this->db->select ( 'client_id' )->select ( 'GROUP_CONCAT(bank_account SEPARATOR "\n") AS bank_account' )->where ( 'disable', STATUS_ENABLE )->group_by ( 'client_id' )->get ( 'client_bank_account_info' );
        return $query->result_array ();
    }

    /**
     * Get client_bank_account_info by client id
     *
     * @param array|int $client_id
     *            - one or many client id
     * @param string|array $select
     *            - select column, select '*' if ommited
     *
     * @return array|boolean
     *
     * @author quang_duc
     */
    public function get_bank_account_info_($client_id = array()) {
        if (! is_array ( $client_id )) {
            if (is_numeric ( $client_id )) {
                $client_id = array (
                        $client_id
                );
            } else {
                log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): ID is null or not a number.' );
                return false;
            }
        }

        if (! empty ( $client_id )) {
            $this->db->where_in ( 'client_id', $client_id );
        }

        $query = $this->db->select ( 'client_id' )->select ( 'bank_account' )->where ( 'disable', STATUS_ENABLE )->get ( 'client_bank_account_info' );
        return $query->result_array ();
    }

    /**
     * Check if client has orders associated with it
     *
     * @param int $client_id
     * @return boolean
     *
     * @author quang_duc
     */
    public function has_order($client_id) {
        if (! $client_id || ! is_numeric ( $client_id )) {
            log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): ID is null or not a number.' );
            return false;
        }

        $query = $this->db->select ()->where ( 'client_id', $client_id )->where ( 'disable', STATUS_ENABLE )->get ( 'order' );

        return $query->num_rows () > 0 ? true : false;
    }

    /**
     * Insert new client
     *
     * @param
     *            $params
     * @return mixed
     *
     * @author quang_duc
     */
    public function apply($params) {
        $params = $this->_calculate_profit ( $params );
        $params ['applicant_account_id'] = $this->auth->get_account_id ();
        return $this->client_model->insert ( 'client', $params );
    }

    /**
     * Update client with new information
     *
     * @param
     *            $id
     * @param
     *            $params
     * @return mixed
     *
     * @author quang_duc
     */
    public function reapply($id, $params) {
        if (! $client = $this->_get_client ( $id )) {
            log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): get client id=' . $id . ' failed.' );
            return false;
        }

        if ($client ['status'] != CLIENT_STATUS_CREDIT_WAITING) {
            log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): wrong client status.' );
            return false;
        }

        $params = $this->_calculate_profit ( $params );
        return $this->client_model->update ( 'client', $params, $id );
    }

    /**
     * Approve client application
     *
     * @param int $id
     * @param array $params
     * @return boolean
     *
     * @author quang_duc
     */
    public function approve($id, $params = array()) {
        if (! $client = $this->_get_client ( $id )) {
            log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): get client id=' . $id . ' failed.' );
            return false;
        }

        if ($client ['status'] != CLIENT_STATUS_CL_APPROVAL_PENDING) {
            log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): wrong client status.' );

            if ($client ['status'] == CLIENT_STATUS_CL_APPROVED) {
                $this->error = lang ( 'msg_already_approve' );
            } elseif ($client ['status'] == CLIENT_STATUS_CL_APPROVAL_REJECTED) {
                $this->error = lang ( 'msg_already_reject' );
            }

            return false;
        }

        // Get name kana of client, convert to romaji, cut first 3 chars, to uppercase
        $this->load->helper ( 'text' );

        $name_kana = $client ["name_kana"];
        $name_kana = mb_ereg_replace ( "\s+", "", $name_kana );

        $romaji_type = "";
        $romaji = new JpnForPhp\Transliterator\Romaji ( "wapuro" );
        $name_romaji = $romaji->transliterate ( $name_kana );

        if (mb_strlen ( $name_kana ) <= 3) {

            $name_kana_length = mb_strlen ( $name_kana );
            $name_romaji_length = strlen ( $name_romaji );

            if ($name_kana_length == 1) {
                if ($name_romaji_length == 1)
                    $romaji_type = "wapuro";
                else
                    $romaji_type = "";
            } elseif ($name_kana_length == 2) {
                if ($name_romaji_length <= 2)
                    $romaji_type = "wapuro";
                else
                    $romaji_type = "";
            } elseif ($name_kana_length == 3) {
                if ($name_romaji_length <= 3)
                    $romaji_type = "wapuro";
                else
                    $romaji_type = "";
            }
        }

        $romaji = $name_romaji = null;
        $romaji = new JpnForPhp\Transliterator\Romaji ( $romaji_type );
        $name_romaji = $romaji->transliterate ( $name_kana );

        $s_prefix = strtoupper ( convert_accented_characters ( mb_substr ( $name_romaji, 0, 3 ) ) );
        $s_prefix_length = strlen ( $s_prefix );

        // Get client that has s_code starting with the first 3 chars above
        if ($s_prefix_length > 2) {
            $last_s_code_client = $this->db->select ( 's_code' )->like ( 's_code', $s_prefix, 'after' )->where ( 'client.disable', STATUS_ENABLE )->order_by ( 's_code', 'DESC' )->limit ( 1 )->get ( 'client' )->result_array ();
        } elseif ($s_prefix_length == 2) {
            $last_s_code_client = $this->db->select ( 's_code' )->where ( 'client.disable', STATUS_ENABLE )->where ( 'client.s_code REGEXP "^' . $s_prefix . '[0-9]{4}$"', NULL, FALSE )->order_by ( 's_code', 'DESC' )->limit ( 1 )->get ( 'client' )->result_array ();
        } elseif ($s_prefix_length == 1) {
            $last_s_code_client = $this->db->select ( 's_code' )->where ( 'client.disable', STATUS_ENABLE )->where ( 'client.s_code REGEXP "^' . $s_prefix . '[0-9]{5}$"', NULL, FALSE )->order_by ( 's_code', 'DESC' )->limit ( 1 )->get ( 'client' )->result_array ();
        }

        // If that client exists, + 1 to the suffix number, else, start with 001
        if (! empty ( $last_s_code_client )) {
            $last_s_code_client = $last_s_code_client [0];

            if ($s_prefix_length == 1)
                $s_suffix = sprintf ( '%05d', substr ( $last_s_code_client ['s_code'], 1 ) + 1 );
            elseif ($s_prefix_length == 2)
                $s_suffix = sprintf ( '%04d', substr ( $last_s_code_client ['s_code'], 2 ) + 1 );
            else
                $s_suffix = sprintf ( '%03d', substr ( $last_s_code_client ['s_code'], 3 ) + 1 );
        } else {
            if ($s_prefix_length == 1)
                $s_suffix = '00001';
            elseif ($s_prefix_length == 2)
                $s_suffix = '0001';
            else
                $s_suffix = '001';
        }

        // s_code is the combine of 3 romaji chars and an auto increment number
        $params ['s_code'] = $s_prefix . $s_suffix;
        $params ['issue_s_code_date'] = date ( 'Y-m-d' );

        $params ['credit_level'] = $client ['credit_level'];
        if (! $params = $this->_process_credit_approval_data ( $params )) {
            return false;
        }

        $params ['lastup_account_id'] = $this->auth->get_account_id ();

        $this->db->trans_start ();

        if ($client ['credit_level'] != CLIENT_CREDIT_LEVEL_NG) {
            $approval_log = array (
                    'client_id' => $id,
                    'credit_approval_id' => $this->auth->get_account_id (),
                    'credit_approval_date' => date ( 'Y-m-d' ),
                    'lastup_account_id' => $this->auth->get_account_id ()
            );

            $this->insert ( 'client_credit_approval_log', $approval_log );
        }

        $this->update ( 'client', $params, $id );

        $this->db->trans_complete ();

        return $this->db->trans_status ();
    }

    /**
     * Reject application
     *
     * @param int $id
     * @param array $params
     * @return boolean
     *
     * @author quang_duc
     */
    public function reject($id, $params = array()) {
        if (! $client = $this->_get_client ( $id )) {
            log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): get client id=' . $id . ' failed.' );
            return false;
        }

        if ($client ['status'] != CLIENT_STATUS_CL_APPROVAL_PENDING) {
            log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): wrong client status.' );

            if ($client ['status'] == CLIENT_STATUS_CL_APPROVED) {
                $this->error = lang ( 'msg_already_approve' );
            } elseif ($client ['status'] == CLIENT_STATUS_CL_APPROVAL_REJECTED) {
                $this->error = lang ( 'msg_already_reject' );
            }

            return false;
        }

        $params ['status'] = CLIENT_STATUS_CL_APPROVAL_REJECTED;
        $params ['lastup_account_id'] = $this->auth->get_account_id ();

        return $this->update ( 'client', $params, $id );
    }

    /**
     * Edit application
     *
     * @param int $id
     * @param array $data
     * @return bool
     *
     * @author quang_duc
     */
    public function edit($id, $data) {
        if (! $client = $this->_get_client ( $id )) {
            log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): get client id=' . $id . ' failed.' );
            return false;
        }

        if (in_array ( $client ['status'], array (
                CLIENT_STATUS_CREDIT_WAITING,
                CLIENT_STATUS_CREDIT_EXPIRED,
                CLIENT_STATUS_CL_APPROVAL_REJECTED,
                CLIENT_STATUS_TRADING_NG
        ) )) {
            log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): wrong client status.' );
            return false;
        }

        if (! $this->auth->has_permission ( 'client/input_credit' )) {
            unset ( $data ['corporate_status_before'] );
            unset ( $data ['corporate_status_after'] );
            unset ( $data ['name'] );
            unset ( $data ['name_kana'] );
            unset ( $data ['closing_date'] );
            unset ( $data ['payment_month_cd'] );
            unset ( $data ['payment_day_cd'] );
            unset ( $data ['update_credit'] );
            unset ( $data ['credit_level'] );
            unset ( $data ['credit_amount'] );
            unset ( $data ['comment'] );
            unset ( $data ['bank_account'] );
        }

        if (! $this->auth->has_permission ( 'change_sales_staff', 'column' )) {
            unset ( $data ['s_account_id_1st'] );
            unset ( $data ['s_account_id_2nd'] );
        }

        if (isset ( $data ['update_credit'] ) && $data ['update_credit']) {
            if (! $data = $this->_process_recredit_data ( $data, $client )) {
                return false;
            }
//          if ($data ['credit_level'] != CLIENT_CREDIT_LEVEL_NG) {
            if ($data ['credit_level'] != CLIENT_CREDIT_LEVEL_NG && isset($data['status'])) {
                $approval_log = array (
                        'client_id' => $id,
                        'credit_approval_id' => $this->auth->get_account_id (),
                        'credit_approval_date' => date ( 'Y-m-d' ),
                        'lastup_account_id' => $this->auth->get_account_id ()
                );

                $this->insert ( 'client_credit_approval_log', $approval_log );
            }
            unset ( $data ['update_credit'] );
        } else {
            unset ( $data ['update_credit'] );
            unset ( $data ['credit_level'] );
            unset ( $data ['credit_amount'] );
            unset ( $data ['comment'] );
        }

        $this->db->trans_start ();

        $bank_account = array ();
        if (isset ( $data ['bank_account'] )) {
            foreach ( $data ['bank_account'] as $account_name ) {
                if (trim ( $account_name )) {
                    $bank_account [] = array (
                            'bank_account' => trim ( $account_name )
                    );
                }
            }
            unset ( $data ['bank_account'] );
            $this->update_full ( 'client_bank_account_info', $bank_account, array (
                    'client_id' => $id
            ) );
        }

        $this->update ( 'client', $data, $id );

        $this->db->trans_complete ();

        return $this->db->trans_status ();
    }

    /**
     * Process credit result and update client data
     *
     * @param int $id
     * @param array $data
     * @return mixed
     *
     * @author quang_duc
     */
    public function input_credit($id, $data) {
        if (! $client = $this->_get_client ( $id )) {
            log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): get client id=' . $id . ' failed.' );
            return false;
        }

        if ($client ['status'] != CLIENT_STATUS_CREDIT_WAITING && $client ['status'] != CLIENT_STATUS_CREDIT_EVERYTIME) {
            log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): wrong client status.' );

            if ($client ['status'] == CLIENT_STATUS_CREDIT_ALREADY) {
                $this->error = lang ( 'msg_already_input_credit' );
            }

            return false;
        }

        if (!$data = $this->_process_credit_input_data ( $data )) {
            return false;
        }

        $data ['lastup_account_id'] = $this->auth->get_account_id ();

        return $this->update ( 'client', $data, $id );
    }

    /**
     * Client model validation rules
     *
     * @param string $rule_name
     * @return array
     *
     * @author quang_duc
     */
    public function rules($rule_name) {
        $rules = array (
                'apply' => array (
                        array (
                                'field' => 'name',
                                'label' => lang ( 'lbl_name' ),
                                'rules' => 'trim|required|only_2byte'
                        ),
                        array (
                                'field' => 'name_kana',
                                'label' => lang ( 'lbl_name_kana' ),
                                'rules' => 'trim|required|katakana'
                        ),
                        array (
                                'field' => 'zipcode',
                                'label' => lang ( 'lbl_zipcode' ),
                                'rules' => 'trim|required|validate_zipcode_department|min_length[7]|max_length[7]',
                                'errors' => array (
                                        'min_length' => lang ( 'msg_digit_not_enough' ),
                                        'max_length' => lang ( 'msg_enter_7_digit_' )
                                )
                        ),
                        array (
                                'field' => 'address_1st',
                                'label' => lang ( 'lbl_address' ) . '１',
                                'rules' => 'trim|required|only_2byte'
                        ),
                        array (
                                'field' => 'address_2nd',
                                'label' => lang ( 'lbl_address' ) . '２',
                                'rules' => 'trim|only_2byte'
                        ),
                        array (
                                'field' => 'representative_name',
                                'label' => lang ( 'lbl_representative_name' ),
                                'rules' => 'trim|required|only_2byte'
                        ),
                        array (
                                'field' => 'phone_no',
                                'label' => lang ( 'lbl_tel' ),
                                'rules' => 'trim|required|phone[{2}-{4}-{4}]',
                                'errors' => array (
                                        'phone' => str_replace ( '%field%', lang ( 'lbl_phone' ), lang ( 'msg_enter_hyphen_and_num' ) )
                                )
                        ),
                        array (
                                'field' => 'fax_no',
                                'label' => lang ( 'lbl_fax' ),
                                'rules' => 'trim|phone[{2}-{4}-{4}]',
                                'errors' => array (
                                        'phone' => str_replace ( '%field%', lang ( 'lbl_fax' ), lang ( 'msg_enter_hyphen_and_num' ) )
                                )
                        ),
                        array (
                                'field' => 'business_category',
                                'label' => lang ( 'lbl_business_category' ),
                                'rules' => 'required'
                        ),
                        array (
                                'field' => 'division_name',
                                'label' => lang ( 'lbl_division' ),
                                'rules' => 'trim|only_2byte'
                        ),
                        array (
                                'field' => 'charge_name',
                                'label' => lang ( 'lbl_charge_name' ),
                                'rules' => 'trim|required|only_2byte'
                        ),
                        array (
                                'field' => 'mail_address',
                                'label' => lang ( 'lbl_mail_address' ),
                                'rules' => 'trim|valid_email'
                        ),
                        array (
                                'field' => 's_account_id_1st',
                                'label' => lang ( 'lbl_sales_staff' ) . '１',
                                'rules' => 'required'
                        ),
                        array (
                                'field' => 'delivery_month_cd',
                                'label' => lang ( 'lbl_delivery_month' ),
                                'rules' => 'trim|required|numeric|greater_than[0]|less_than[13]',
                                'errors' => array (
                                        'greater_than' => lang ( 'msg_within_12_months' ),
                                        'less_than' => lang ( 'msg_within_12_months' )
                                )
                        ),
                        array (
                                'field' => 'delivery_amount',
                                'label' => lang ( 'lbl_delivery_amount' ),
                                'rules' => 'trim|required|numeric|greater_than[0]'
                        ),
                        array (
                                'field' => 'gross_profit_amount',
                                'label' => lang ( 'lbl_profit_amount' ),
                                'rules' => 'trim|required|numeric|greater_than[0]'
                        ),
                        array (
                                'field' => 'total_delivery_amount',
                                'label' => lang ( 'lbl_delivery_amount' ),
                                'rules' => 'trim|required|numeric|greater_than[0]'
                        ),
                        array (
                                'field' => 'total_gross_profit_amount',
                                'label' => lang ( 'lbl_profit_amount' ),
                                'rules' => 'trim|required|numeric|greater_than[0]'
                        ),
                        array (
                                'field' => 'remark',
                                'label' => lang ( 'lbl_remark' ),
                                'rules' => 'trim|required'
                        )
                ),
                'credit' => array (
                        array (
                                'field' => 'credit_level',
                                'label' => lang ( 'lbl_credit_level' ),
                                'rules' => 'required'
                        ),
                        array (
                                'field' => 'credit_amount',
                                'label' => lang ( 'lbl_credit_amount' ),
                                'rules' => 'required|is_natural',
                                'errors' => array (
                                        'is_natural' => lang ( 'msg_credit_amount_number' )
                                )
                        )
                ),
                // array(
                // 'field' => 'comment',
                // 'label' => lang('lbl_comment'),
                // 'rules' => 'required'
                // )
                'edit' => array (
                        array (
                                'field' => 'zipcode',
                                'label' => lang ( 'lbl_zipcode' ),
                                'rules' => 'trim|required|validate_zipcode_department|min_length[7]|max_length[7]',
                                'errors' => array (
                                        'min_length' => lang ( 'msg_digit_not_enough' ),
                                        'max_length' => lang ( 'msg_enter_7_digit_' )
                                )
                        ),
                        array (
                                'field' => 'address_1st',
                                'label' => lang ( 'lbl_address' ) . '１',
                                'rules' => 'trim|required|only_2byte'
                        ),
                        array (
                                'field' => 'address_2nd',
                                'label' => lang ( 'lbl_address' ) . '２',
                                'rules' => 'trim|only_2byte'
                        ),
                        array (
                                'field' => 'representative_name',
                                'label' => lang ( 'lbl_representative_name' ),
                                'rules' => 'trim|required'
                        ),
                        array (
                                'field' => 'phone_no',
                                'label' => lang ( 'lbl_tel' ),
                                'rules' => 'trim|required|phone[{2}-{4}-{4}]',
                                'errors' => array (
                                        'phone' => str_replace ( '%field%', lang ( 'lbl_phone' ), lang ( 'msg_enter_hyphen_and_num' ) )
                                )
                        ),
                        array (
                                'field' => 'fax_no',
                                'label' => lang ( 'lbl_fax' ),
                                'rules' => 'trim|phone[{2}-{4}-{4}]',
                                'errors' => array (
                                        'phone' => str_replace ( '%field%', lang ( 'lbl_fax' ), lang ( 'msg_enter_hyphen_and_num' ) )
                                )
                        ),
                        array (
                                'field' => 'business_category',
                                'label' => lang ( 'lbl_business_category' ),
                                'rules' => 'trim|required'
                        ),
                        array (
                                'field' => 'division_name',
                                'label' => lang ( 'lbl_division' ),
                                'rules' => 'trim|only_2byte'
                        ),
                        array (
                                'field' => 'charge_name',
                                'label' => lang ( 'lbl_charge_name' ),
                                'rules' => 'trim|required|only_2byte'
                        ),
                        array (
                                'field' => 'mail_address',
                                'label' => lang ( 'lbl_mail_address' ),
                                'rules' => 'trim|valid_email'
                        )
                )
        );

        if ($rule_name == 'apply') {
            if (! $this->input->post ( 'is_ad_receive_payment_request' )) {
                $rules ['apply'] = array_merge ( $rules ['apply'], array (
                        array (
                                'field' => 'closing_date',
                                'label' => lang ( 'lbl_closing_date' ),
                                'rules' => 'required'
                        ),
                        array (
                                'field' => 'payment_month_cd',
                                'label' => lang ( 'lbl_payment_month' ),
                                'rules' => 'required'
                        ),
                        array (
                                'field' => 'payment_day_cd',
                                'label' => lang ( 'lbl_payment_date' ),
                                'rules' => 'required'
                        )
                ) );
            }
        }

        if ($rule_name == 'edit') {
            if ($this->auth->has_permission ( 'client/input_credit' )) {
                $rules ['edit'] = array_merge ( $rules ['edit'], array (
                        array (
                                'field' => 'name',
                                'label' => lang ( 'lbl_name' ),
                                'rules' => 'trim|required|only_2byte'
                        ),
                        array (
                                'field' => 'name_kana',
                                'label' => lang ( 'lbl_name_kana' ),
                                'rules' => 'trim|required|katakana'
                        )
                ) );
                if (! $this->input->post ( 'is_ad_receive_payment_request' )) {
                    $rules ['edit'] = array_merge ( $rules ['edit'], array (
                            array (
                                    'field' => 'closing_date',
                                    'label' => lang ( 'lbl_closing_date' ),
                                    'rules' => 'required'
                            ),
                            array (
                                    'field' => 'payment_month_cd',
                                    'label' => lang ( 'lbl_payment_month' ),
                                    'rules' => 'required'
                            ),
                            array (
                                    'field' => 'payment_day_cd',
                                    'label' => lang ( 'lbl_payment_date' ),
                                    'rules' => 'required'
                            )
                    ) );
                }
                if ($this->input->post ( 'update_credit' )) {
                    $rules ['edit'] = array_merge ( $rules ['edit'], array (
                            array (
                                    'field' => 'credit_level',
                                    'label' => lang ( 'lbl_credit_level' ),
                                    'rules' => 'required'
                            ),
                            array (
                                    'field' => 'credit_amount',
                                    'label' => lang ( 'lbl_credit_amount' ),
                                    'rules' => 'required|is_natural',
                                    'errors' => array (
                                            'is_natural' => lang ( 'msg_credit_amount_number' )
                                    )
                            ),
//#7043:Start
//                            array (
//                                    'field' => 'comment',
//                                    'label' => lang ( 'lbl_comment' ),
//                                    'rules' => 'required'
//                            )
//#7043:End
                    ) );
                }
            }
            if ($this->auth->has_permission ( 'change_sales_staff', 'column' )) {
                $rules ['edit'] = array_merge ( $rules ['edit'], array (
                        array (
                                'field' => 's_account_id_1st',
                                'label' => lang ( 'lbl_sales_staff' ) . '１',
                                'rules' => 'required'
                        )
                ) );
            }
        }

        return isset ( $rules [$rule_name] ) ? $rules [$rule_name] : array ();
    }

    /**
     * Validation rule to check payment date must be larger than or equal closing date
     * when payment month is current month
     *
     * @param
     *            $closing_date
     * @param
     *            $payment_month
     * @param
     *            $payment_date
     * @return bool
     *
     * @author quang_duc
     */
    public function check_payment_date($closing_date, $payment_month, $payment_date) {
        if ($payment_month == CLIENT_PAYMENT_CURRENT_MONTH) {
            if ($payment_date < $closing_date) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get client to check credit
     *
     * @param
     *            null
     * @return mixed null|array
     * @author hoang_minh
     * @since 2015-05-28
     */
    public function get_client_credit_groub_b() {
        $sql = "SELECT " . "`client`.id, "
             . "`client`.s_code, "
             . "`client`.name, "
             . "`client`.name_kana, "
             . "`client`.s_account_id_1st, "
             . "`client`.s_account_id_2nd, "
             . "`client`.is_ad_receive_payment, "
#7427:Start
             . "`client`.applicant_account_id,"
#7427:End
             . "`client`.credit_amount, "
             . "`client`.closing_date, "
             . "`client`.payment_month_cd, "
             . "`client`.payment_day_cd, "
             . "`client`.zipcode, "
             . "`client`.address_1st, "
             . "`client`.address_2nd, "
             . "`client`.phone_no, "
             . "`client`.business_category, "
             . "`client`.remark, "
             . "`client`.comment, "
             . "(SELECT " . "DATE_ADD(MAX(credit_approval_date), INTERVAL 1 YEAR) "
             . "FROM client_credit_approval_log "
             . "WHERE client_credit_approval_log.client_id = `client`.id "
             . "AND client_credit_approval_log.disable = '" . STATUS_ENABLE . "' " . ") AS credit_approval_date, "
             . "(SELECT " . "MAX(delivery_date) "
             . "FROM order_detail "
             . "JOIN `order` ON order_detail.order_id = `order`.id "
             . "WHERE `order`.client_id = `client`.id "
             . "AND order_detail.disable = " . STATUS_ENABLE . " "
             . "AND `order`.disable = " . STATUS_ENABLE . ") AS max_delivery_date, "
             . "(SELECT " . "account.name "
             . "FROM account "
             . "WHERE account.id = `client`.s_account_id_1st "
             . "AND account.disable = " . STATUS_ENABLE . ") AS account_name_1, "
             . "(SELECT "
             . "account.name "
             . "FROM account "
             . "WHERE account.id = `client`.s_account_id_2nd "
             . "AND account.disable = " . STATUS_ENABLE . ") AS account_name_2, "
             . "(SELECT " . "gui_parts_element.name "
             . "FROM gui_parts_element "
             . "JOIN gui_parts ON gui_parts_element.gui_parts_id = gui_parts.id "
             . "WHERE gui_parts.column_name = 'business_category' "
             . "AND gui_parts_element.code = `client`.business_category "
             . "AND gui_parts.disable = " . STATUS_ENABLE . " "
             . "AND gui_parts_element.disable = " . STATUS_ENABLE . ") AS business_category_name "
             . "FROM `client` " . "WHERE (SELECT " . "MAX(credit_approval_date) " . "FROM client_credit_approval_log " . "WHERE client_credit_approval_log.client_id = `client`.id " . "AND client_credit_approval_log.disable = '" . STATUS_ENABLE . "' " . ") = DATE_ADD(DATE_SUB(CURDATE(), INTERVAL 1 YEAR), INTERVAL 7 DAY) " . "AND credit_level = '" . CLIENT_CREDIT_LEVEL_B . "' " . "AND status = '" . CLIENT_STATUS_CL_APPROVED . "' " . "AND disable = '" . STATUS_ENABLE . "'";

        return $this->exec_query ( $sql );
    }
    /**
     * Get client to check credit expired
     *
     * @param
     *            null
     * @return mixed null|array
     * @author hoang_minh
     * @since 2015-05-29
     */
    public function get_client_regist_expired() {
        $sql = "SELECT " . "id, " . "name " . "FROM client " . "WHERE credit_date < DATE_SUB(CURDATE(), INTERVAL 3 MONTH) " . "AND status = '" . CLIENT_STATUS_CREDIT_ALREADY . "' " . "AND disable = '" . STATUS_ENABLE . "'";

        return $this->exec_query ( $sql );
    }

    /**
     * Update client
     *
     * @param array $params
     * @return bool
     * @author hoang_minh
     * @since 2015-05-29
     */
    public function update_client($update_data = array(), $update_conditions = array()) {
        if (! is_array ( $update_data ) || empty ( $update_data )) {
            log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): $update_data is not a array or empty.' );
            return false;
        }

        $set = array ();
        $where = array ();

        // process update data
        foreach ( $update_data as $key => $val ) {
            $set [] = $key . "='" . $val . "'";
        }

        // process update conditions
        foreach ( $update_conditions as $key => $val ) {
            switch ($key) {
                case 'ids' :
                    $where [] = "id IN(" . $val . ")";
                    break;

                default :
                    break;
            }
        }

        $sql = 'UPDATE `client` ' . 'SET ' . implode ( ', ', $set ) . ' ' . 'WHERE ' . implode ( ' AND ', $where );

        return $this->db->query ( $sql );
    }

    /**
     * Calculate profit rate = profit amount / delivery amount * 100
     * The result is round up to 2 decimal number
     *
     * @param
     *            $data
     * @return mixed
     *
     * @author quang_duc
     */
    protected function _calculate_profit($data) {
        $result = $data;

        if (isset ( $result ['delivery_amount'] ) && isset ( $result ['gross_profit_amount'] )) {
            if ($result ['delivery_amount'] && $result ['gross_profit_amount']) {
//                $result ['gross_profit_rate'] = f_floor ( $result ['gross_profit_amount'] / $result ['delivery_amount'] * 100 * 100 ) / 100;
                $result ['gross_profit_rate'] = bcdiv(bcmul($result['gross_profit_amount'], 100), $result['delivery_amount'], 2);
            }
        }

        if (isset ( $result ['total_delivery_amount'] ) && isset ( $result ['total_gross_profit_amount'] )) {
            if ($result ['total_delivery_amount'] && $result ['total_gross_profit_amount']) {
//                $result ['total_gross_profit_rate'] = f_floor ( $result ['total_gross_profit_amount'] / $result ['total_delivery_amount'] * 100 * 100 ) / 100;
                $result ['total_gross_profit_rate'] = bcdiv(bcmul($result['total_gross_profit_amount'], 100), $result['total_delivery_amount'], 2);
            }
        }

        return $result;
    }

    /**
     * Set client status when it is approved
     *
     * @param
     *            $data
     * @return bool
     *
     * @author quang_duc
     */
    protected function _process_credit_approval_data($data) {
        $result = $data;
        switch ($data ['credit_level']) {
            case CLIENT_CREDIT_LEVEL_A :
            case CLIENT_CREDIT_LEVEL_B :
                $result ['status'] = CLIENT_STATUS_CL_APPROVED;
                break;
            case CLIENT_CREDIT_LEVEL_C :
                $result ['status'] = CLIENT_STATUS_CREDIT_EVERYTIME;
                break;
            case CLIENT_CREDIT_LEVEL_NG :
                $result ['status'] = CLIENT_STATUS_TRADING_NG;
                break;
            default :
                log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): Client\'s credit level must be either A, B or C.' );
                return false;
        }

        $result ['approval_account_id'] = $this->auth->get_account_id ();

        return $result;
    }
    protected function _process_recredit_data($data, $client) {
        $result = $data;
        switch ($data ['credit_level']) {
            case CLIENT_CREDIT_LEVEL_A :
            case CLIENT_CREDIT_LEVEL_B :
                $result ['status'] = CLIENT_STATUS_CL_APPROVED;
                break;
            case CLIENT_CREDIT_LEVEL_C :
                $result ['status'] = CLIENT_STATUS_CREDIT_EVERYTIME;
                break;
            case CLIENT_CREDIT_LEVEL_NG :
                $result ['status'] = CLIENT_STATUS_TRADING_NG;
                break;
            default :
                log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): Client\'s credit level must be either A, B or C.' );
                return false;
        }

//#8137:Start
        if ($client['status'] == CLIENT_STATUS_CREDIT_ALREADY || $client['status'] == CLIENT_STATUS_CL_APPROVAL_PENDING) {
            if ($result['credit_level'] != CLIENT_CREDIT_LEVEL_NG) {
                unset($result['status']);
            }
        }
//#8137:End

        $result ['credit_account_id'] = $this->auth->get_account_id ();
        $result ['credit_date'] = date ( 'Y-m-d' );

        return $result;
    }

    /**
     * Process credit data when input credit result
     *
     * @param
     *            $data
     * @return array|bool
     *
     * @author quang_duc
     */
    protected function _process_credit_input_data($data) {
        $result = $data;

        switch ($result ['credit_level']) {
            case CLIENT_CREDIT_LEVEL_A :
            case CLIENT_CREDIT_LEVEL_B :
                $result ['status'] = CLIENT_STATUS_CREDIT_ALREADY;
                if ($result ['is_ad_receive_payment'] || $result ['credit_amount'] == 0) {
                    $result ['is_ad_receive_payment'] = 1;
                    $result ['credit_amount'] = 0;
                }
                break;
            case CLIENT_CREDIT_LEVEL_C :
                if ($data['is_ad_receive_payment_request'] == 1) {
                    log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): Client\'s credit level C is not available for adv payment client' );
                    return false;
                }
                $result ['status'] = CLIENT_STATUS_CREDIT_ALREADY;
                $result ['is_ad_receive_payment'] = 0;
                break;
            case CLIENT_CREDIT_LEVEL_NG :
                $result ['status'] = CLIENT_STATUS_TRADING_NG;
                $result ['is_ad_receive_payment'] = 1;
                $result ['credit_amount'] = 0;
                break;
            default :
                log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): Client\'s credit level must be either A, B or C.' );
                return false;
        }

        if ($data['is_ad_receive_payment_request'] == 1) {
            $result['is_ad_receive_payment'] = 1;
            $result['credit_amount'] = 0;
        }

        $result ['credit_account_id'] = $this->auth->get_account_id ();
        $result ['credit_date'] = date ( 'Y-m-d' );

        return $result;
    }

    /**
     * Get client by id
     *
     * @param int $id
     * @return array|bool
     *
     * @author quang_duc
     */
    protected function _get_client($id) {
        if (! $id || ! is_numeric ( $id )) {
            log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): ID is null or not a number.' );
            return false;
        }

        $client = $this->filter ( array (
                'search_id' => $id
        ) )->get_data ();
        if (empty ( $client )) {
            log_message ( 'error', __FILE__ . '(line ' . __LINE__ . '): client ID is not exist in DB.' );
            return false;
        } else {
            $client = $client [0];
        }

        return $client;
    }

   /**
     * Get payment past of client
     *
     * @param int $client_id
     * @return array
     * @author van_don
     */
    public function get_payment_past_data($client_id) {
        $query = " SELECT * " . " FROM payment_past_data" . " WHERE client_id = ? " . " AND disable = 0" . " AND is_checked = 0" . " AND is_ad_receive_payment = 0";
        $this->set_single ();
        return $this->exec_query ( $query, array (
                $client_id
        ) );
    }

    /**
     * Get nearest closing date
     *
     * @return array
     * @author van_don
     */
    public function get_nearest_closing_date() {
        $query = " SELECT close_date " . " FROM closing_accountant" . " WHERE `disable` = 0" . " ORDER BY close_date DESC LIMIT 1";
        $this->set_single ();
        return $this->exec_query ( $query );
    }

    /**
     *
     * @author van_don
     */
    public function get_payment_info($client_id, $closing_date) {
        $list = array ();
        $query = " SELECT SUM(sales_slip.total_amount) AS total_amount"
               . " FROM sales_slip"
               . " WHERE sales_slip.`disable` = 0"
               . "    AND DATE_FORMAT(sales_slip.payment_date,'%Y-%m') <= DATE_FORMAT(?,'%Y-%m')"
               . "    AND sales_slip.is_checked = 0"
               . "    AND sales_slip.sales_slip_status = " . SALES_STATUS_INVOICE_ISSUED
               . "    AND sales_slip.client_id = ?";
        $this->set_single ();
        return $this->exec_query ($query, array ($closing_date,$client_id));
    }

    /**
     *
     * @param
     * $client_id
     * @param
     * $closing_date
     * @return array
     * @author van_don
     */
    public function get_payment_future($client_id, $closing_date, $order_id) {
        $query = " SELECT `order`.id, "
                . " order_detail.delivery_date,"
                . " order_detail.payment_date,"
                . " order_detail.amount"
                . " FROM order_detail"
                . " INNER JOIN `order`"
                . "    ON (`order`.id = order_detail.order_id AND `order`.`disable` = 0)"
                . " WHERE MONTH(order_detail.payment_date) > MONTH(?) AND order_detail.`disable` = 0"
                . " AND order.client_id = ?";
        $list = array ();
        $list [] = $closing_date;
        $list [] = $client_id;
        if ($order_id) {
            $query .= " AND order.id <> ?";
            $list [] = $order_id;
        }

        $query .= " AND order_detail.is_invoice_issue = 0"
                . " AND `order`.is_ad_receive_payment = 0"
                . " ORDER BY `order`.id ASC, order_detail.delivery_date DESC";
        return $this->exec_query ( $query, $list );
    }

    /**
     * Get client by id
     *
     * @param int $client_id
     */
    public function get_client_by_id($client_id) {
        return $this->db->select ( '*' )->from ( 'client' )->where ( 'id', $client_id )->where ( 'disable', 0 )->get ()->row_array ();
    }

    /**
     * Get clients
     *
     * @param int $offset
     * @param array $search_data
     * @param array $additional_table
     * @return array
     * @author cong_tien
     */
    public function get_clients($offset = 0, $search_data = array(), $additional_table = array()) {
        $this->load->model('gui_parts_model');
        
        if (!is_null($offset)) {
            $limit = self::LIMIT;
            $offset = $limit * $offset;
            $this->db->limit(self::LIMIT, $offset);
        }

        foreach ($search_data as $field => $value) {
//#6994:Start
            if (($value != '') && (strpos($field, 'search_') === 0)) {
//#6994:End
                $field = substr($field, 7);
                switch ($field) {
                    case 'status':
                        $this->db->where_in('client.status', $value);
                        break;
                    case 's_code':
                        $this->db->like('client.s_code', $value, 'after');
                        break;
                    case 'name':
                    case 'name_kana':
                    case 'division_name':
                    case 'phone_no':
                        $this->db->like('client.' . $field, $value);
                        break;
                    case 'zipcode':
                        $value = str_replace('-', '', $value);
                        $this->db->where('client.zipcode', $value);
                        break;
                    case 'business':
//#6994:Start
                        if ($value > 0) {
                            $this->db->join('account acc_2', 'client.s_account_id_2nd = acc_2.id AND acc_2.disable = ' . STATUS_ENABLE, 'left')
                                ->join('department dpm_2', 'acc_2.department_id = dpm_2.id AND dpm_2.disable = ' . STATUS_ENABLE, 'left')
                                ->where('(dpm_1.business_unit_id = ' . $value . ' OR dpm_2.business_unit_id = ' . $value . ')')
                                ->distinct();
                        }
                        break;
                    case 'account':
                        if ($value > 0) {
                            $this->db->where('(client.s_account_id_1st = "' . $value . '" OR client.s_account_id_2nd = "' . $value . '")');
                        }
//#6994:End
                        break;
                    case 'closing_date':
                    case 'payment_month_cd':
                    case 'payment_day_cd':
                    case 'is_ad_receive_payment_request':
                        $this->db->where('client.' . $field, $value);
                        break;
                    case 'issue_s_code_date_from':
                        if (!$search_data['search_issue_s_code_date_to']) {
                            if ($value = parse_date($value)) {
                                if (substr_count($value, '-') == 2) {
                                    $this->db->where('client.issue_s_code_date >=', $value);
                                } else {
                                    $date = new DateTime($value . '-01');
                                    $this->db->where('client.issue_s_code_date BETWEEN "' . $date->format('Y-m-d') . '" AND "' . $date->format('Y-m-t') . '"');
                                }
                            } else {
                                $this->db->where('1 = 0');
                            }
                        }
                        break;
                    case 'issue_s_code_date_to':
                        if ($search_data['search_issue_s_code_date_from']) {
                            $date_from = parse_date($search_data['search_issue_s_code_date_from']);
                            $date_to = parse_date($search_data['search_issue_s_code_date_to']);
                            
                            if (substr_count($date_from, '-') == 2 && substr_count($date_to, '-') == 2) {
                                $this->db->where('client.issue_s_code_date BETWEEN "' . $date_from . '" AND "' . $date_to . '"');
                            } else {
                                $this->db->where('1 = 0');
                            }
                        } else {
                            if ($value = parse_date($value)) {
                                if (substr_count($value, '-') == 2) {
                                    $this->db->where('client.issue_s_code_date <=', $value);
                                } else {
                                    $this->db->where('1 = 0');
                                }
                            } else {
                                $this->db->where('1 = 0');
                            }
                        }
                        break;
                }
            }
        }

//#6986:Start
        if (!empty($additional_table)) {
            foreach ($additional_table as $table) {
                switch ($table) {
                    case 'client_credit_approval_log':
                        $this->db->select('
                            (SELECT MIN(credit_approval_date)
                            FROM client_credit_approval_log
                            WHERE client_id = client.id
                            AND client_credit_approval_log.disable = 0) AS credit_approval_log_date
                        ', false);
                        break;
                }
            }
        }
//#6986:End
        
        $query = $this->db->select('
                    business_unit.id AS business_unit_id,
                    `acc_1`.`name` AS `acc_1_name`, `client`.*', false)
                ->from('client')
                ->join('account acc_1', 'acc_1.id = `client`.s_account_id_1st AND acc_1.`disable` = ' . STATUS_ENABLE, 'INNER', false)
                ->join('department dpm_1', 'dpm_1.id = acc_1.department_id AND dpm_1.disable = ' . STATUS_ENABLE)
                ->join('business_unit', 'business_unit.id = dpm_1.business_unit_id AND business_unit.disable = ' . STATUS_ENABLE)
                //->join('gui_parts_element', 'gui_parts_element.code = IF(`client`.corporate_status_before <> 0, `client`.corporate_status_before, `client`.corporate_status_after) AND gui_parts_element.`disable` = ' . STATUS_ENABLE, 'INNER', false)
                //->join('gui_parts', 'gui_parts.id = gui_parts_element.gui_parts_id AND gui_parts.column_name = "' . gui_parts_model::COLUMN_NAME_COMPANY_TITLE . '" AND gui_parts.disable = ' . STATUS_ENABLE, 'INNER')
                ->where('client.disable', STATUS_ENABLE)
                ->order_by('IF(client.s_code = "" OR client.s_code is null, 1, 0)', '', false)->order_by('client.s_code')->order_by('client.id')
                ->get();

        return $query->result_array();
    }
}
