<?php
/**
 *
 * @author van_phong
 *
 */
class Closing_accountant_model extends ZR_Model {
    /**
     * @author Phong
     * @created date: 26/June/2015
     * @return multitype:
     */
    public function get_closing_accountant() {
        $query = "SELECT id, close_date FROM closing_accountant WHERE disable = 0";
        return $this->exec_query($query);
    }

    public function get_closing_month_recent() {

        $query = "SELECT * FROM closing_accountant WHERE disable = 0 Order by close_date desc LIMIT 0,1";
        return $this->exec_query($query);

    }

    /**
     * @author Phong
     * @created date: 26/June/2015
     * @param unknown $time
     * @return boolean true on closed
     *
     * @edit Cam_Tien: add: defined delimiter of the given time, so that use delimiter as a patern
     */
    public function check_has_closed_or_not($time) {
//#9695:Start
//        $list_closing = $this->get_closing_accountant();
        $delimiter = strpos($time, '-') ? '-' : '/';
//        $patern = 'Y' . $delimiter . 'm';
        if (count(explode($delimiter, $time)) == 2) {
            $time = $time . $delimiter . '01';
        }
        $time = strtotime("$time first day of this month");
//        foreach($list_closing as $c) {
//            if(date($patern, strtotime($c['close_date'])) == $time){
//                return true;
//            }
//        }
        $max_closing_date = $this->get_closest_closing_date();
        $max_closing_date = $max_closing_date[0]['closest_closing_date'];
        $max_closing_date = strtotime("$max_closing_date first day of this month");

        if ($time < $max_closing_date) return true;

//#9695:End
        return false;
    }

    /**
     * @author Phong
     * @created date: 26/June/2015
     * @param unknown $time_year_month
     * @return boolean
     */
    public function check_orders_before_close_accountant($time_year_month) {
        $sql = "SELECT id
                FROM `order_detail`
                WHERE DATE_FORMAT(delivery_date,'%Y/%m') = ? AND (order_status = 1 OR order_status = 2 )
                AND disable = 0
               ";
        $result = $this->exec_query($sql, array($time_year_month));
        if(count($result) > 0) {
            return false;
        }
        return true;

    }

    /**
     * @author Phong
     * @created date: 26/June/2015
     * @param unknown $time_year_month
     * @return multitype:
     */
    public function delete_closed_accountant($time_year_month) {
        $sql = "UPDATE closing_accountant SET disable = 1 WHERE DATE_FORMAT(close_date,'%Y/%m') = ? ";
        return $this->db->query($sql, array($time_year_month));
    }

    /**
     * @author Phong
     * @created date: 26/June/2015
     * @param unknown $begin_date
     * @param unknown $end_date
     * @return multitype:
     */
    public function get_holiday_date($begin_date, $end_date) {
        $sql = "SELECT holiday FROM holiday WHERE disable = 0 AND holiday >= ? AND holiday <= ?";
        return $this->exec_query($sql, array($begin_date, $end_date));
    }

    /**
     * Get closest closing date
     *
     * @param null
     * @return String
     * @author hoang_minh
     * @since 2015/07/20
     */
    public function get_closest_closing_date() {
        $sql = "SELECT "
             .    "MAX(close_date) AS closest_closing_date "
             . "FROM closing_accountant WHERE disable = 0";
        return $this->exec_query($sql);
    }

    /**
     * Get closing accountants
     * @param string $type get one or all record
     * @param array $options
     * @return array
     * @author cong_tien
     */
    public function get_closing_accountants($type, $options = array()) {
        if (!empty($options)) {
            if (!empty($options['fields'])) {
                $this->db->select($options['fields']);
            }

            if (!empty($options['conditions'])) {
                foreach ($options['conditions'] as $field => $condition) {
                    $this->db->where("{$field}", "{$condition}");
                }
            }

            if (!empty($options['order'])) {
                $this->db->order_by($options['order']);
            }
        }

        $this->db->where("disable", 0);

        $query = $this->db->get("closing_accountant");

        if ($type == 'first') {
            return $query->row_array();
        } elseif ($type == 'all') {
            return $query->result_array();
        }
    }

    /**
     * Do close accountant
     *
     * @return array
     *
     * @author van_thuat 2015-08-12 #5978
     */
    public function close_closing_accountant() {
        $models = array('order_model', 'order_acceptance_model');
        $this->load->model($models);

        // Init date variable
        $prev_month = new DateTime('-1 months');

        // Check accountant 2 months ago isn't closed
        if (!($closed_accountant = $this->closing_accountant_model->get_closed_accountant('-2 months'))
            || ($closed_accountant['disable'] == STATUS_DISABLE)) {
            $prev_month->modify('-1 months');
        } else if (($closed_accountant = $this->closing_accountant_model->get_closed_accountant())
            && ($closed_accountant['disable'] == STATUS_ENABLE)) {
            // Check if accountant 1 months ago is closed => Alert previous month has been closed
            return array(
                    'status'  => 0,
                    'message' => "{$prev_month->format('Y年m月')}の経理締めが既に完了しました。"
            );
        }

        // Check if orders still remain processing => Not allow close accountant
        if ($undefined_orders = $this->order_model->get_undefined_orders_in_month($prev_month->format('Y-m-d'))) {
            // $message = "{$prev_month->format('Y年m月')}の経理締めは以下のｊコードが未定のため、完了できません。";
            $message = "以下のＪコードが未確定のため締めを行う事ができません。";
            $message .= "<br/>";
            $message .= "<div class='error-message'>";
            foreach ($undefined_orders as $undefined_order) {
                $message .= "<span class='row-error-icon'>{$undefined_order['j_code']}-{$undefined_order['branch_cd']}</span><br/>";
            }
            $message .= "</div>";

            return array(
                    'status'  => 0,
                    'message' => $message
            );
        }

        // Get extra order_acceptances start month
        $extra_order_acceptances = $this->order_acceptance_model->calculate_extra_order_acceptances_start_month($prev_month->format('Y-m-d'));

        try {
            $this->db->trans_begin();

            if ($closed_accountant) {
                $closing_accountant_data = array(
                        'close_date'        => "{$prev_month->format('Y-m')}-{$prev_month->format('t')}",
                        'lastup_account_id' => $this->auth->get_account_id(),
                        'disable'           => STATUS_ENABLE
                );
                if (!$this->update('closing_accountant', $closing_accountant_data, array('id' => $closed_accountant['id']))) {
                    throw new Exception(__LINE__ . "UPDATE closing_accountant FAIL");
                }
            } else {
                    $closing_accountant_data = array(
                            'close_date'        => "{$prev_month->format('Y-m')}-{$prev_month->format('t')}",
                            'lastup_account_id' => $this->auth->get_account_id(),
                    );
                    if (!$this->insert('closing_accountant', $closing_accountant_data)) {
                            throw new Exception(__LINE__ . "INSERT closing_accountant FAIL");
                    }
                }

                if (!empty($extra_order_acceptances['update'])) {
                    if (!$this->update_batch('order_acceptance', $extra_order_acceptances['update'], 'id')) {
                        throw new Exception(__LINE__ . "UPDATE order_acceptance FAIL");
                    }
                }

                if (!empty($extra_order_acceptances['insert'])) {
                    if (!$this->insert_batch('order_acceptance', $extra_order_acceptances['insert'])) {
                        throw new Exception(__LINE__ . "INSERT order_acceptance FAIL");
                    }
            }

            $this->db->trans_commit();

            return array(
                    'status'  => 1,
                    // 'message' => "{$prev_month->format('Y年m月')}の経理締めが成功しました。"
                    'message' => "{$prev_month->format('Y年m月')}の経理締めを行いました。"
            );
        } catch (Exception $e) {
            $this->db->trans_rollback();

            return array(
                'status'  => 0,
                'message' => "{$prev_month->format('Y年m月')}の経理締めが失敗しました。"
            );
        }
    }

    /**
     * Do unclose accountant
     *
     * @return array
     *
     * @author van_thuat 2015-08-12 #5978
     */
    public function unclose_closing_accountant() {
        // Init date variable
        $prev_month = new DateTime('-1 months');
        $cur_month  = new DateTime();

        // Check accountant is closed
        if (!($closed_accountant = $this->closing_accountant_model->get_closed_accountant())
            || ($closed_accountant['disable'] == STATUS_DISABLE)) {
            return array(
                    'status'  => 0,
                    'message' => "{$prev_month->format('Y年m月')}の経理締めが完了していないため、開けません。"
            );
        }

        try {
            $this->db->trans_begin();

            $delete_data = array(
                    'lastup_account_id' => $this->auth->get_account_id(),
                    'lastup_datetime'   => $this->_db_now(),
                    'disable'           => STATUS_DISABLE
            );

            $closing_accountant_condition = array(
                    'closing_accountant.disable' => STATUS_ENABLE,
                    "DATE_FORMAT(closing_accountant.close_date, '%Y/%m') = " => $prev_month->format('Y/m')
            );
            if (!$this->db->delete('closing_accountant', $closing_accountant_condition)) {
                throw new Exception(__LINE__ . "DELETE closing_accountant FAIL");
            }

            $order_acceptance_condition = array(
                    'order_acceptance.disable'       => STATUS_ENABLE,
                    'order_acceptance.is_extra_item' => 1,
                    'order_acceptance.delivery_date' => "{$cur_month->format('Y-m')}-01",
                    'order_acceptance.close_date'    => "{$prev_month->format('Y-m')}-{$prev_month->format('t')}"
            );
            if (!$this->update('order_acceptance', $delete_data, $order_acceptance_condition)) {
                throw new Exception(__LINE__ . "DELETE order_acceptance FAIL");
            }

            $this->db->trans_commit();

            return array(
                    'status'  => 1,
                    'message' => "{$prev_month->format('Y年m月')}分の経理締めを解除しました。"
            );
        } catch (Exception $e) {
            $this->db->trans_rollback();

            return array(
                    'status'  => 0,
                    'message' => "{$prev_month->format('Y年m月')}の経理開きが失敗しました。"
            );
        }
    }

    /**
     * Get Closing accountant by $accountant_month
     *
     * @param string $accountant_month
     * @return array
     *
     * @author van_thuat 2015-08-12 #5978
     */
    public function get_closed_accountant($accountant_month = '-1 months') {
        try {
            $accountant_month_obj = new DateTime($accountant_month);
        } catch (Exception $e) {
            $accountant_month_obj = new DateTime('-1 months');
        }

        $sql = "SELECT closing_accountant.id, closing_accountant.close_date, closing_accountant.disable"
        . " FROM closing_accountant"
        . " WHERE DATE_FORMAT(closing_accountant.close_date, '%Y/%m') = '{$accountant_month_obj->format('Y/m')}'"
        . " ORDER BY closing_accountant.disable ASC, closing_accountant.close_date DESC"
        . " LIMIT 1"
        ;

        $query = $this->db->query($sql);
        return $query->row_array();
    }

    /**
     * get the min closed data
     * @return array
     * @author cam_tien 2015-09-25
     */
    public function get_min_closed_accountant() {
        return $this->db->select('pe.entry_date')
                        ->from('payment_entry pe')
                        ->where('pe.disable', 0)
                        ->order_by('pe.entry_date', 'ASC')
                        ->limit(1)
                        ->get()->row_array();
    }
}