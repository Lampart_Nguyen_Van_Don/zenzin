<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Consumption_tax_model extends ZR_Model {

    public $list_order_status = array();
    public $list_business_unit;
    public $list_account_follow_bussiness_unit;

    public function __construct()
    {
        parent::__construct();
    }

    public function gex_tax_rate($tax_type) {
        $tax_rates = $this->db->select('rate, adaptation_date')
                ->from('consumption_tax ct')
                ->join('consumption_tax_adaptation cta', 'ct.consumption_tax_adaptation_id = cta.id')
                ->where('ct.disable', 0)
                ->where('cta.disable', 0)
                ->where('ct.consumption_tax_type_id', $tax_type)
                ->get()->result();

        return $tax_rates;
    }

    public function get_rate() {
        $query = "SELECT rate
                  FROM consumption_tax
                  WHERE
                    consumption_tax_adaptation_id = (SELECT id
                                                     FROM consumption_tax_adaptation
                                                     WHERE adaptation_date <= CURRENT_DATE ()
                                                     ORDER BY adaptation_date
                                                     LIMIT 1)";

        return $this->db->query($query)->row_array();

    }

    /**
     * Get tax rate by conditions
     *
     * @param array $params
     * @author hoang_minh
     * @since 2015-07-15
     * @return mixed null | array
     */
    public function get_tax_rate_by_conditions($params = array()) {

        if (empty($params)) {
            return null;
        }

        $table = '`consumption_tax` '
               . 'JOIN `consumption_tax_adaptation` ON `consumption_tax`.consumption_tax_adaptation_id = `consumption_tax_adaptation`.id '
               . 'JOIN consumption_tax_type ON `consumption_tax`.consumption_tax_type_id = consumption_tax_type.id ';

        $columns = (isset($params['columns'])) ? $params['columns'] : array();
        $limits = (isset($params['limits'])) ? $params['limits'] : array();
        $conditions = array(
                '`consumption_tax`.disable = ' . STATUS_ENABLE,
                '`consumption_tax_adaptation`.disable = ' . STATUS_ENABLE,
                '`consumption_tax_type`.disable = ' . STATUS_ENABLE
        );

        if (isset($params['conditions'])) {
            foreach ($params['conditions'] as $key => $val) {
                switch ($key) {

                    case 'consumption_tax_type_id':
                        $conditions[] = '`consumption_tax_type`.id = ' . $val;
                        break;

                    default:
                        break;
                }
            }
        }

        $orders = array();
        if (isset($params['orders'])) {
            foreach ($params['orders'] as $key => $val) {
                $orders[] = $key . ' ' . $val;
            }
        }

        return $this->get_items_by_parameters($table, $columns, $conditions, $orders, $limits);

    }
    
    /**
     * Get consumption tax
     * @param string $type get one or all record
     * @param array $options
     * @return array
     * @author cong_tien
     */
    public function get_consumption_taxs($type, $options = array()) {
        if (!empty($options)) {
            if (!empty($options['fields'])) {
                $this->db->select($options['fields']);
            }

            if (!empty($options['conditions'])) {
                foreach ($options['conditions'] as $field => $condition) {
                    $this->db->where("{$field}", "{$condition}");
                }
            }

            if (!empty($options['order'])) {
                $this->db->order_by($options['order']);
            }
            
            if (!empty($options['joins'])) {
                foreach ($options['joins'] as $table => $condition) {
                    $this->db->join($table, $condition);
                }
            }
        }

        $this->db->where("consumption_tax.disable", 0);
        
        $query = $this->db->get("consumption_tax");

        if ($type == 'first') {
            return $query->row_array();
        } elseif ($type == 'all') {
            return $query->result_array();
        }
    }
    
    public function get_consumption_taxs_delivery() {
        $this->db->select('consumption_tax_adaptation.id AS consumption_tax_adaptation_id,
                        consumption_tax.id AS consumption_tax_id, consumption_tax_adaptation.adaptation_date,
                        consumption_tax_adaptation.remark, consumption_tax.rate');
        $this->db->join('consumption_tax_adaptation', 'consumption_tax_adaptation.id = consumption_tax.consumption_tax_adaptation_id AND consumption_tax_adaptation.disable = 0');
        $this->db->order_by('consumption_tax_adaptation.adaptation_date DESC');
        $this->db->where("consumption_tax.disable", 0);
        $query = $this->db->get("consumption_tax");
        return $query->result_array();
    }
    public function get_tax_rate_by_date($datetime) {
        $tax_rates = $this->db->select('ct.rate as ct_rate')
                ->from('consumption_tax ct')
                ->join('consumption_tax_adaptation cta', 'ct.consumption_tax_adaptation_id = cta.id')
                ->where('ct.disable', 0)
                ->where('cta.disable', 0)
                ->where('cta.adaptation_date <=', $datetime)
                ->order_by('cta.adaptation_date DESC')
                ->get()->row();

        return $tax_rates?$tax_rates->ct_rate:0;
    }
}