<?php
if (! defined ( 'BASEPATH' ))
    exit ( 'No direct script access allowed' );
class Department_model extends ZR_Model {

    /**
     * Insert a department
     * @param unknown $data
     */
    public function insert_department($data) {
        return $this->insert ( 'department', $data );
    }

    /**
     * Get all department, associate with name and id of business unit it belongs to
     * @return Ambigous <multitype:, mixed>
     * @author Phong
     */
    public function get_all_departments($conditions = array()) {
        // Get department.name, department.id, business_unit.name, business_unit.id
        $query = "SELECT business_unit.id AS business_unit_id, business_unit.name AS business_unit_name,
                   department.id AS department_id, department.name AS department_name
                  FROM business_unit INNER JOIN department ON department.business_unit_id = business_unit.id
                  WHERE department.disable = 0
                ";
        // FROM department LEFT JOIN business_unit  ON department.business_unit_id = business_unit.id
        $list_params = array();
        if(!empty($conditions)) {
            foreach($conditions AS $key=>$value) {
                if($key=='search_business_unit') {
                    if(!empty($value)) {
                        $query.=" AND department.business_unit_id = ?";
                        $list_params [] = $value;
                    }
                }
                if($key == 'search_name') {
                    if(!empty($value)) {
                        $query.=" AND department.name LIKE ?";
                        $list_params [] = "%".$value."%";
                    }
                }
            }
        }
        $query.=" ORDER BY business_unit.sequence, business_unit.id, department.id ASC";
        return $this->exec_query ( $query, $list_params );
    }

    /**
     * Get a department for using in view detail and edit screen
     * @author Phong
     * @param unknown $id
     * @return Ambigous <multitype:, mixed>|boolean
     */
    public function get_department_by_id($id) {
        $query = "SELECT business_unit.id AS business_unit_id, business_unit.name AS business_unit_name, department.id, department.name, department.zipcode,
                         department.address_1, department.address_2, department.phone_no, department.fax_no
                  FROM department LEFT JOIN business_unit ON department.business_unit_id = business_unit.id
                  WHERE department.id = ?";
        // return $this->exec_query ( $query, array($id) );
        $this->set_single ();
        return $this->exec_query ( $query, array (
                $id
        ) );
    }

    /**
     * Update department
     * @param unknown $table
     * @param unknown $data
     * @param unknown $condition
     * @return boolean
     * @author Phong
     */
    public function update_department($table, $data, $condition) {
        return $this->update ( $table, $data, $condition );
    }

    /**
     *
     * @param unknown $name
     * @param unknown $business_unit
     */
    public function check_duplicated_item($name, $business_unit) {
        $query = "SELECT d.name AS department_name, b.name AS business_unit_name
                FROM department d INNER JOIN business_unit b ON d.business_unit_id = b.id
                WHERE d.disable = 0 AND d.business_unit_id = ? AND d.name = ? LIMIT 1
                ";
        $this->set_single();
        return $this->exec_query($query, array($business_unit, $name));
    }

    /**
     * check_duplicated_item when edit
     * @param unknown $name
     * @param unknown $business_unit
     * @param unknown $deparment_id
     * @return Ambigous <multitype:, mixed>
     */
    public function check_duplicated_item_edit($name, $business_unit, $deparment_id) {
        $query = "SELECT d.name AS department_name, b.name AS business_unit_name
                FROM department d INNER JOIN business_unit b ON d.business_unit_id = b.id
                WHERE d.disable = 0 AND d.business_unit_id = ? AND d.name = ? AND d.id != ? LIMIT 1
                ";
        $this->set_single();
        return $this->exec_query($query, array($business_unit, $name, $deparment_id ));
    }

    /**
     * get_departments_by_business_unit_id
     * @param unknown $business_unit_id
     * @return Ambigous <multitype:, mixed>
     * @author Phong
     */
    public function get_departments_by_business_unit_id($business_unit_id) {
        $query =   "SELECT id, name
                    FROM department
                    WHERE disable = 0 AND business_unit_id = ?";
        return $this->exec_query($query, array($business_unit_id));
    }

    /**
     * check_department_exist
     * @param unknown $if
     * @return Ambigous <multitype:, mixed>
     * @author Phong
     */
    public function check_department_exist($id) {
        $query = "SELECT id FROM department WHERE id = ? AND disable = 0";
        $this->set_single();
        return $this->exec_query($query, array($id));
    }

    /**
     * check_business_unit_id_exist
     * @Use in business_unit controller
     * @param unknown $id
     * @return Ambigous <multitype:, mixed>
     */
    public function check_business_unit_id_exist($id) {
        $query = "SELECT id FROM department WHERE business_unit_id = ? AND disable = 0";
        return $this->exec_query($query, array($id));
    }

    /**
     *
     * @param unknown $department_id
     */
    public function check_account_constraint($department_id) {
        $query = "SELECT id FROM account WHERE disable = 0 AND department_id = ? LIMIT 1";
        $this->set_single();
        return $this->exec_query($query, array($department_id));
    }

    /**
     * get_department_by_account_id
     * @param unknown $account_id
     */
    public function get_department_by_account_id($account_id) {
        return $this->db->select('
                 dm.business_unit_id
                ,dm.name
                ,dm.zipcode
                ,dm.address_1
                ,dm.address_2
                ,dm.phone_no
                ,dm.fax_no
                ,ac.name
                ')
        ->from('department dm')
        ->join('account ac', 'ac.department_id = dm.id')
        ->where('ac.id', $account_id)
        ->where('dm.disable', 0)
        ->where('ac.disable', 0)
        ->get()
        ->row_array();
    }

    /**
     * Created rules for validation
     * @param unknown $rule_name
     * @return Ambigous <multitype:string multitype:string  >|multitype:
     * @author Phong
     */
    public function rules($rule_name) {
        // Validate login form
        $validate_rule ['edit_department'] = array (

                array (
                        'field' => 'name',
                        'label' => 'name',
                        'rules' => 'required|max_length[128]',
                        'errors' => array (
                                'required' => lang('error_null_department_name'),
                                'max_length' => lang('alert_name_128_chars_max')
                        )
                ),
                array (
                        'field' => 'zipcode',
                        'label' => 'zipcode',
                        'rules' => 'trim|required|validate_zipcode_department|min_length[7]|max_length[12]',
                        'errors' => array (
                                'required' => lang('error_null_zipcode'),
                                'validate_zipcode_department' => lang('error_zipcode_not_contains_hyphen'),
                                'min_length'=> lang('alert_zipcode_7_chars'),
                                'max_length' => lang('alert_zipcode_12_chars_max')
                        )
                ),

                array (
                        'field' => 'address_1',
                        'rules' => 'required|max_length[255]',
                        'errors' => array (
                                'required' => lang('error_null_address1'),
                                'max_length' => lang('alert_address1_max')
                        )
                ),
                array (
                        'field' => 'address_2',
                        'rules' => 'max_length[255]',
                        'errors' => array (
                                'max_length' => lang('alert_address2_max')
                        )
                ),
                array (
                        'field' => 'phone_no',
                        'rules' => 'required|phone|max_length[16]',
                        'errors' => array (
                                'required' => lang('error_null_tel'),
                                'phone' => lang('error_TEL_contains_hyphen_and_no'),
                                'max_length' => lang('alert_phone_no_max')
                        )
                ),
                array (
                        'field' => 'fax_no',
                        'rules' => 'required|phone|max_length[16]',
                        'errors' => array (
                                'required' => lang('error_null_fax'),
                                'phone' => lang('error_FAX_contains_hyphen_and_no'),
                                'max_length' => lang('alert_fax_no_max')
                        )
                )
                ,
                array (
                        'field' => 'business_unit_id',
                        'rules' => 'required',
                        'errors' => array (
                                'required' => lang('error_null_business_unit_id')
                        )
                )
        );
        if (isset ( $validate_rule [$rule_name] )) {
            return $validate_rule [$rule_name];
        }
        return array ();
    }

     /**
     * get_business_unit_of_current_logged_user
     * @param unknown $id_logged_user
     * @author
     */
    public function get_business_unit_of_current_logged_user($id_logged_user) {

        $query = "SELECT id, name, product_manage_code, is_product_division
                  FROM business_unit
                  WHERE id IN
                      (SELECT business_unit_id
                       FROM department WHERE id IN
                            (SELECT department_id AS id
                             FROM account
                             WHERE id = ?
                            )
                      )";
        $this->set_single();
        return $this->exec_query($query, array($id_logged_user));
    }
}
