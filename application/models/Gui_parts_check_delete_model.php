<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Gui parts check delete model
 *
 * @author hoang_minh
 * @since 2015-06-30
 */
class Gui_parts_check_delete_model extends ZR_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Get gui parts checked delete by gui part id
     *
     * @author hoang_minh
     * @since 2015-06-30
     */
    public function get_by_gui_part_id($gui_part_id) {

        if (empty($gui_part_id)) {
            return false;
        }

        $query = "SELECT * "
               . "FROM gui_parts_check_delete "
               . "WHERE gui_parts_id = ? "
               .     "AND gui_parts_check_delete.disable = '" . STATUS_ENABLE . "'";

        return $this->exec_query($query, array($gui_part_id));
    }

    public function get_item_by_parameters($table, $columns, $conditions) {
        if ( ! $table || ! is_array($conditions) || empty($conditions)) {
            return false;
        }

        $where = array();
        foreach ($conditions as $key => $val) {
            $where[] = $key . "='" . $val . "'";
        }

        return $this->get_items_by_parameters($table, $columns, $where);
    }

}

?>