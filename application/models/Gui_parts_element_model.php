<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gui_parts_element_model extends ZR_Model {

    public function get_all_by_gui_parts_id($gui_parts_id) {


      /*  $query = "SELECT *
                  FROM gui_parts_element
                  WHERE disable = 0 AND gui_parts_id = ? ORDER BY  sequence  ASC, id ASC";
                  */
    	$query = "SELECT  gpe.*,
	    			        (SELECT  gp.name
	    			         FROM    gui_parts_element  as gp
	                         WHERE   gp.gui_parts_id = ".GUI_PART_NEN_INPUT."
	    			                 AND  gpe.is_no_add = gp.code) as new_input
				 FROM      gui_parts_element gpe
				 WHERE     gpe.disable = 0
	            	 AND gpe.gui_parts_id = ?
				 ORDER BY  gpe.sequence  ASC, gpe.id ASC ";
        return $this->exec_query($query, array($gui_parts_id));
    }

    /**
     * Get gui_parts_element by gui_parts column name
     *
     * @param string $column_name
     * @param string|array $select - select column, select '*' if ommited
     *
     * @return array
     *
     * @author quang_duc
     */
    public function get_by_gui_parts_column_name($column_name, $select = '*') {
        if ($select != '*') {
            if (!is_array($select)) {
                $select = explode(',', $select);
            }
            foreach ($select as &$column) {
                $column = 'gpe.' . trim($column);
            }
        }

        $query = $this->db->select($select)
                          ->join('gui_parts gp', 'gp.id = gpe.gui_parts_id')
                          ->where('gp.column_name', $column_name)
                          ->where('gpe.is_no_add', STATUS_ENABLE)
                          ->where('gpe.disable', STATUS_ENABLE)
                          ->order_by('sequence')
                          ->get('gui_parts_element gpe');

        return $query->result_array();
    }

    public function get_by_id($id) {
        $query = "SELECT gpe.*, gp.name as gui_parts_name, gp.id as gui_parts_id
                  FROM gui_parts_element gpe
                  INNER JOIN gui_parts gp ON gp.id = gpe.gui_parts_id
                  WHERE gpe.disable = 0 AND gpe.id = ?";
        $this->set_single();
        return $this->exec_query($query, array($id));
    }

    public function update_gui_parts_element($id, $data) {
        return $this->update('gui_parts_element', $data, array('id' => $id));
    }

    public function add_new_gui_parts_element($data) {
        $max_code = $this->get_max_code($data['gui_parts_id']);
        $data['code'] = $data['sequence'] = $max_code['max_code'] ? $max_code['max_code'] + 1 : 1;
        $data['lastup_account_id'] = $this->auth->get_account_id();
        return $this->insert('gui_parts_element', $data);
    }

    private function get_max_code($gui_parts_id) {
        $query = "SELECT MAX(code) as max_code
                  FROM gui_parts_element
                  WHERE gui_parts_id = ?";
        $this->set_single();
        return $this->exec_query($query, array($gui_parts_id));
    }

    public function delete_gui_part_element($id) {
        return $this->delete('gui_parts_element', array('id' => $id));
    }

    public function is_shipments_shape_code($code) {
        $this->db->select('id')
                ->from('gui_parts_element')
                ->where('gui_parts_id', SHIPMENTS_SHAPE_ID)
                ->where('code', $code);
        return $this->db->get()->num_rows() ? true : false;
    }

    public function get_shipments_shape_info_by_code($code) {
    	return $this->db->select('*')
    					->from('gui_parts_element')
    					->where('gui_parts_id', SHIPMENTS_SHAPE_ID)
    					->where('code', $code)
    					->get()->result_array();
    }

    public function get_all_shipment_shape_elements() {

        return $this->db->select('id, code, name')
                        ->from('gui_parts_element')
                        ->where('gui_parts_id', SHIPMENTS_SHAPE_ID)
                        ->where('is_no_add', STATUS_ENABLE)
                        ->get()->result_array();

    }

    public function rules($rule_name) {
        $validate_rule['edit'] = array (
                array (
                    'field'  => 'name',
                    'label'  => 'name',
                    'rules'  => 'required|trim|max_length[128]',
                    'errors' => array(
                                    'required' => lang('error_name_required'),
                                )
                ),
                array(
                    'field'  => 'is_no_add',
                    'label'  => 'is_no_add',
                    'rules'  => 'in_list[0,1]',
                    'errors' => array(
                                    'in_list' => lang('lbl_invalid_input'),
                                )

                ),
        );

        $posts = $this->input->post();

        if (isset($posts['sequence'])) {
            $validate_rule['edit'][] = array(
                'field' => 'sequence',
                'label' => lang('lbl_sequence'),
                'rules' => 'trim|required|integer|max_length[5]|is_natural|between[{0, '.MAX_SEQUENCE_ALLOW.'}]',
                'errors' => array(
                    'required'      => lang('lbl_invalid_input'),
                    'max_length'    => lang('error_sequence_max_length'),
                    'integer'       => lang('error_sequence_numeric'),
                    'is_natural'    => lang('error_common_number_invalid')
                )
            );
        }

        $validate_rule['add'] = array(
                                    array (
                                        'field'  => 'name',
                                        'label'  => lang('lbl_name'),
                                        'rules'  => 'trim|required|max_length[128]',
                                        'errors' => array(
                                                        'required' => lang('error_name_required'),
                                                    )
                                    ),
                                );

        if (isset($validate_rule[$rule_name])) {
            return $validate_rule[$rule_name];
        }
        return array ();
    }

//7989:Start
    public function get_business_status_info_by_code($code) {
    	return $this->db->select('*')
		    	->from('gui_parts_element')
		    	->where('gui_parts_id', COMPANY_TITLE_ID)
		    	->where('code', $code)
		    	->get()->result_array();
    }
//7989:End

}
