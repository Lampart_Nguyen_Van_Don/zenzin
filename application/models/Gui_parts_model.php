<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gui_parts_model extends ZR_Model {

    /**
     * Define column name is client_status
     */
    const COLUMN_NAME_CLIENT_STATUS = 'client_status';
    const COLUMN_NAME_COMPANY_TITLE = 'company_title';
    
    public function get_all() {
        return $this->get_items('gui_parts');
    }

    public function get_by_id($id) {
        return $this->get_item_by_id('gui_parts', $id);
    }

}