<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Holiday_model extends ZR_Model {

    /**
     * get all holiday in specify year
     * @param  int $year
     * @return array
     */
    public function get_all_by_year($year) {
        $query = "SELECT holiday
                  FROM holiday
                  WHERE YEAR(holiday) = ? and disable = 0";

        return $this->exec_query($query, array($year));
    }

    /**
     * get item by id
     * @param  int $id
     * @return array
     */
    public function get_by_id($id) {
        return $this->get_item_by_id('holiday', $id);
    }

    /**
     * @param  int $id
     * @return array
     * @author cong_minh 2015/6/1
     */
    public function check_holiday($date) {
        return $this->db->get_where('holiday', array('holiday' => $date, 'disable' => 0))->num_rows();
    }

    /**
     * get all years have been set holiday
     * @return array
     */
    public function get_established_years() {
//#7561:Start
//        $query = "SELECT DISTINCT YEAR(holiday) as year
//                  FROM holiday
//                  WHERE disable = 0
//                  ORDER BY year";

        $query = "SELECT DISTINCT YEAR(holiday) as year
                  FROM holiday
                  WHERE disable = 0 AND YEAR(holiday) != YEAR(CURDATE())
                  ORDER BY year";
//#7561:End
        return $this->exec_query($query);
    }

    /**
     * insert holiday
     * @param  int $year
     * @param  array $weekdays determine the holiday want to set is monday, tuesday,....
     * @return boolean
     */
    public function insert_holiday($year, $weekdays) {
        $today = date('Y-m-d');
        $datas = array();

        foreach ($weekdays as $weekday) {
            switch ($weekday) {
                case DAY_OF_WEEK_MON:
                case DAY_OF_WEEK_TUE:
                case DAY_OF_WEEK_WED:
                case DAY_OF_WEEK_THU:
                case DAY_OF_WEEK_FRI:
                case DAY_OF_WEEK_SAT:
                case DAY_OF_WEEK_SUN:
                    $datas = array_merge($datas, $this->get_date_for_specific_day_between_date($year .'-01-01', $year . '-12-31', $weekday));

                    break;

                case HOLIDAY:
                    $this->load->library('ical');
                    $holidays = array_keys($this->ical->apple_holiday_calendar(array('from_year'=> $year, 'to_year'=> $year)));
                    $datas = array_merge($datas, $holidays);
                    break;
                default:
                    break;
            }
        }

        foreach ($datas as $key => $data) {
            if (strtotime($data) < strtotime($today)) unset($datas[$key]);
        }

        $datas = array_unique($datas);

        $query_data = array();
        foreach ($datas as $data) {
            $query_data[] = "(?,'" . $this->auth->get_account_id() . "','" . $this->_db_now() ."', '" . $this->_db_now() . "')";
        }

        $this->db->trans_begin();
        try {
            if (!$this->disable_holidays_in_year($year)) throw new Exception("delete error");

            if (!empty($query_data)) {
                $query = "INSERT INTO holiday(holiday, lastup_account_id, create_datetime, lastup_datetime) VALUES" . implode(',', $query_data);

                if (!$this->db->query($query, $datas)) throw new Exception("insert error");
            }


        } catch (Exception $e) {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return true;
    }

    /**
     * get date of day
     * @param  string $start_date
     * @param  string $end_date
     * @param  int $weekday    1 for monday,...7 for sunday and 8 for holiday
     * @return array
     */
    private function get_date_for_specific_day_between_date($start_date, $end_date, $weekday)
    {
        $start_date = strtotime($start_date);
        $end_date   = strtotime($end_date);
        $date_arr   = array();

        do {
            if (date("N", $start_date) != $weekday) {
                $start_date += (24 * 3600);
            }
        } while(date("N", $start_date) != $weekday);

        while($start_date <= $end_date) {
            $date_arr[] = date('Y-m-d', $start_date);
            $start_date += (7 * 24 * 3600);
        }

        return($date_arr);
    }

    /**
     * disable of holiday date after the current date
     * @param  int $year
     * @return boolean
     */
    public function disable_holidays_in_year($year) {
        $query = "UPDATE holiday
                  SET disable = 1,
                      lastup_datetime = ?
                  WHERE holiday >= CURRENT_DATE() AND YEAR(holiday) = ?";
        return $this->db->query($query, array($this->_db_now(), $year));
    }

    /**
     * Get holidays data by parameters
     *
     * @param array $params
     * @return mixed bool|array
     * @author hoang_minh
     */
    public function get_holidays_by_parameters($params = array()) {

        if (empty($params)) {
            return $this->get_items('holiday');
        }

        //init params
        $columns = (isset($params['columns'])) ? $params['columns'] : array();
        $limits = (isset($params['limits'])) ? $params['limits'] : array();
        $filter_conditions = array();
        $orders = array();

        //processing conditions
        if (isset($params['conditions']) && is_array($params['conditions'])) {
            foreach ($params['conditions'] as $key => $val) {
                switch ($key) {

                    case 'holiday':
                        $filter_conditions[] = $key . " = '" . $val . "'";
                        break;

                    default:
                        break;
                }
            }
        }
        $filter_conditions[] = 'holiday.disable = ' . STATUS_ENABLE;

        //processing ordering
        if (isset($params['orders']) && is_array($params['orders'])) {
            foreach ($params['orders'] as $key => $val) {
                $orders[] = $key . ' ' . mysql_escape_string($val);
            }
        }

        return $this->get_items_by_parameters('holiday', $columns, $filter_conditions,
                $orders, $limits);
    }

    /**
     * Delete holiday
     *
     * @param array $data
     * @return int
     */
    public function delete_holiday($data) {
        return $this->delete('holiday', array('id' => $data['id']));
    }

    /**
     * Insert holidays
     *
     * @param array $data
     * @return int
     */
    public function insert_batch_holidays($datas, $year) {
        $tmp = array();
        $db_holiday_list = $this->get_all_by_year($year);
        foreach ($db_holiday_list as $holiday) {
            $tmp[] = date('Y-m-d', strtotime($holiday['holiday']));
        }

        $insert_datas = array();
        foreach ($datas as $data) {
            if (in_array($data, $tmp)) continue;
            $insert_datas[] = array(
                'holiday'         => $data,
                'create_datetime' => date('Y-m-d H:i:s'),
                'lastup_datetime' => date('Y-m-d H:i:s'),
            );
        }
        
        return $this->insert_batch('holiday', $insert_datas);
    }
    
    /**
     * Count working days between $date_from and $date_to
     * 
     * @param string $date_from
     * @param string $date_to
     * @return number
     * 
     * @author van_thuat 2015-08-15 #5978
     */
    public function count_working_days($date_from = 'first day of this month', $date_to = '') {
    	try {
    		$date_from_obj = new DateTime($date_from);
    	} catch (Exception $e) {
    		$date_from_obj = new DateTime('first day of this month');
    	}
    	
    	try {
    		$date_to_obj = new DateTime($date_to);
    	} catch (Exception $e) {
    		$date_to_obj = new DateTime();
    	}
    	
    	$sql = "SELECT DATEDIFF('{$date_to_obj->format('Y-m-d')}', '{$date_from_obj->format('Y-m-d')}') + 1 - COUNT(holiday.id) AS working_days"
    	. " FROM holiday"
    	. " WHERE holiday.disable = " . STATUS_ENABLE
    	. " 	AND DATE_FORMAT(holiday.holiday, '%Y%m%d') BETWEEN {$date_from_obj->format('Ymd')} AND {$date_to_obj->format('Ymd')}"
    	;
    	
    	$query = $this->db->query($sql);
    	
    	if ($res = $query->row_array()) {
    		return $res['working_days'];
    	}
    	
    	return 0;
    }
}