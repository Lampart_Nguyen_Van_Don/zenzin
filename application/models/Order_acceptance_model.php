<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order_acceptance_model extends ZR_Model {

    protected $_tax = null;
    protected $_subcontractors = null;
    protected $_shipping_types = null;
    protected $_list_closing_date = array();
    protected $_accounts = null;
//#7158:Start
    protected $_max_closing_date = null;
//#7158:End

    const IS_SETTLEMENT = 1;
    const UN_SETTLEMENT = 0;

    public function get_order_detail($j_code, $branch_cd) {
        $j_code    = trim($j_code);
        $branch_cd = trim($branch_cd);
        $branch_cd = sprintf('%02d', $branch_cd);

        // #6801-s #6901-modifly-s
        $this->db->select('
                      o.id
                    , o.j_account_id
                    , o.client_id
                    , c.name as client_name
                    , od.delivery_date
                    , od.is_order_input
                    , od.is_acceptance_input
                    , a.login_id
                    , a.name
                    , od.is_change_report_apply
                    ')
                ->from('order o')
                ->join('order_detail od', 'od.order_id = o.id')
                ->join('client c', 'c.id = o.client_id')
                ->join('account a', 'a.id = o.j_account_id')
                ->where('o.j_code', $j_code)
                ->where('od.branch_cd', $branch_cd)
                ->group_start()
                    ->or_where('od.order_status', 2)
                    ->or_where('od.order_status', 3)
                ->group_end()
                ->where('o.disable', 0)
                ->where('od.disable', 0);

//#9695:Start
        $this->db->select('bu.finish_work_report_template')
                ->join('department d', 'd.id = a.department_id')
                ->join('business_unit bu', 'bu.id = d.business_unit_id')
                ->where('d.disable', 0)
                ->where('bu.disable', 0);
//#9695:End
        return $this->db->get()->row();
        // #6801-s  #6901-modifly-e
    }

// #7090:start
/**
 * Check value date  by format of month and year
 * @param  string $date
 * @throws Exception
 * @return boolean
 * @author nhu_tham
 * @since  2015.10.30
 */
    public function check_year_month($date)
    {
        $checkval = str_replace(array("/","-"), array("",""), $date);

        try {

            if (!is_numeric($checkval)) {
                throw new Exception();
            }

            $list_date =  preg_split( '/[-\.\/ ]/', $date );
            $n_list_date = count($list_date);


            if($n_list_date != 2){
                throw new Exception();
            }

            $yy = strlen($list_date[0]);
            $mm = strlen($list_date[1]);

            if(($yy != 4) || ($mm > 2)){
                throw new Exception();
            }

            $day = '01';

            if (!checkdate($list_date[1], $day, $list_date[0])) {
                throw new Exception();
            }

            return true;

        } catch (Exception $ex) {
            return false;
        }
    }
// #7090:End

    public function import_excel($sheet_datas) {
        $errors = array();
        $data_import_have_jcode_branch = $data_import_without_jcode_branch = array();

        // use this code if using PHPExcel library as excel reader or import csv
        // PROGRESS STATUS
//#6555:Start
        // $unique_id  = $this->input->post('unique_id');
        // $file_name  = FCPATH . 'public/admin/order_acceptance/import/' . $unique_id.'.json';
        // $total_data = count($sheet_datas);
        // $percent    = 100 / $total_data;
//#6555:End

        foreach ($sheet_datas as $row => $datas) {
            // use this code if using PHPExcel library as excel reader or import csv
//#6555:Start
            // $fp = fopen($file_name, 'w');
            // fwrite($fp, json_encode(array('total' => $total_data, 'status' => ceil(($row + 1) * $percent))));
            // fclose($fp);
//#6555:End

            $jcode       = $datas[0];
            $branch_code = $datas[1];

// #7090:Start
            $summary_month = $datas[23];

            $summary_month = str_replace("-", "/", $summary_month);


// #7090:End

            if ($jcode != '' && $branch_code != '') {

                $common_data    = $this->import_common_data($row + 2, $errors, $datas);
                $different_data = $this->import_have_jcode_branch($row + 2, $errors, $datas);

                $merge_data     = array_replace_recursive($common_data, $different_data);

                if (isset($merge_data['errors'])) {
                    ksort($merge_data['errors']);
//#7125:Start
                } else {
                    $merge_data['amount'] = $this->calculate_total_amount($merge_data['tax_free'], $merge_data['shipping_charge_tax'], $merge_data['stamp_tax'], $merge_data['etc_tax'], $merge_data['shipping_charge'], $merge_data['etc'], $merge_data['summary_month']);
                }
//#7125:End

                $data_import_have_jcode_branch[$row] = $merge_data;
            } else {

                $common_data    = $this->import_common_data($row + 2, $errors, $datas);
                $different_data = $this->import_do_not_jcode_branch($row + 2, $errors, $datas);

                $merge_data     = array_replace_recursive($common_data, $different_data);

                if (isset($merge_data['errors'])) {
                    ksort($merge_data['errors']);
//#7125:Start
                } else {
                    $merge_data['amount'] = $this->calculate_total_amount($merge_data['tax_free'], $merge_data['shipping_charge_tax'], $merge_data['stamp_tax'], $merge_data['etc_tax'], $merge_data['shipping_charge'], $merge_data['etc'], $merge_data['summary_month']);
                }
//#7125:End

                $data_import_without_jcode_branch[$row] = $merge_data;
            }
        }

        $data = $data_import_have_jcode_branch + $data_import_without_jcode_branch;
        ksort($data);
        return $data;
    }

    /* @function get data have value zero (one array)
     * @author: Tham
     * @param: $order_acceptance_before,$order_acceptance array
     * @return $data
     */
    public function _returnvalueZero($order_acceptance_before,$order_acceptance){

        foreach ($order_acceptance_before as $key => $value ){
            if($value == 0 || $value == "0"){
                $order_acceptance[$key] = $value;
            }
        }
        return $order_acceptance;
    }

//#7090:Start
    public  function  _get_last_child(){
        $this->db->from("order_acceptance");
        $this->db->order_by('id',"DESC");
        $this->db->limit(1);
        $return_data = $this->db->get()->result_array();
        return  $return_data[0]['id'];

    }
//#7090:End


    /*
    * end  get data have value zero
    */
    public function import_data($order_acceptances) {

        $this->load->model('order_model');
        $this->load->model('order_detail_model');
        $this->load->model('order_acceptance_model');

//#7090:Start
        $listid_import = array();
//#7090:End


        try {

            $this->db->trans_begin();

            $order_inputs =array();
            foreach ($order_acceptances as $order_acceptance) {


                unset($order_acceptance['source_subcontractor_code']);
                unset($order_acceptance['work_subcontractor_code']);
                unset($order_acceptance['shipping_subcontractor_code']);
                unset($order_acceptance['login_id']);
                unset($order_acceptance['arrange_rep_login_id']);
                unset($order_acceptance['arrange_rep_name']);
                unset($order_acceptance['abbr_name']);
                unset($order_acceptance['work_subcontractor_name']);
                unset($order_acceptance['shipping_subcontractor_name']);
                unset($order_acceptance['shipping_type_name']);
                unset($order_acceptance['shipments_shape_name']);
                unset($order_acceptance['login_name']);
//#7289 :Start

                if(isset($order_acceptance['unit_price']) && empty($order_acceptance['unit_price'])){
                    $order_acceptance['unit_price'] = 0;
                }
                if(isset($order_acceptance['quantity']) && empty($order_acceptance['quantity'])){
                    $order_acceptance['quantity'] = 0;
                }
                if(isset($order_acceptance['fee']) && empty($order_acceptance['fee'])){
                    $order_acceptance['fee'] = 0;
                }

                if(isset($order_acceptance['weight']) && empty($order_acceptance['weight'])){
                    $order_acceptance['weight'] = 0;
                }


                $order_acceptance['create_datetime'] = date('Y-m-d H:i:s');
                $order_acceptance['lastup_datetime'] = date('Y-m-d H:i:s');
                $order_acceptance['lastup_account_id'] = $this->auth->get_account_id();

//#7289 :End

                $order_acceptance_before =  $order_acceptance;
                $order_acceptance = array_filter($order_acceptance);
                $order_acceptance = Order_acceptance_model::_returnvalueZero($order_acceptance_before,$order_acceptance);

                // if value is empty -> set value to default into DB
                $array_unset_value = array(
                    'arrange_rep',
                    'shipments_shape_cd'
                );

                foreach ($array_unset_value as $value) {
                    if (empty($order_acceptance[$value])) {
                        unset($order_acceptance[$value]);
                    }
                }

                if (empty($order_acceptance['report_remarks'])) {
                    $order_acceptance['report_remarks'] = "";
                }

                $order_acceptance['order_acceptance_status'] = ORDER_ACCEPTANCE_UNAPPROVED;

                if (!$this->db->insert('order_acceptance', $order_acceptance)) {
                    throw new Exception("Insert failed");
                }
//#7090:Start
                else{
                    $id_sle = $this->_get_last_child();
                    $listid_import[] =  $id_sle;

                }
//#7090:End
                if (isset($order_acceptance['j_code']) && isset($order_acceptance['branch_cd'])) {

                    /*
                     $this->db->where('order_id', $order_acceptance['order_id'])
                     ->where('branch_cd', $order_acceptance['branch_cd'])
                     ->update('order_detail', array('is_order_input' => 1));
                     */

                    $order_inputs[] = array(
                            'j_code' =>$order_acceptance['j_code'],
                            'branch_cd' =>$order_acceptance['branch_cd'],
                    );
                    $jcode = $order_acceptance['j_code'];
                    $branch_cd =  $order_acceptance['branch_cd'];
                    $listoderdetail = $this->order_model->get_order_detai_by_jCode_and_BrandCode($jcode,$branch_cd);
                    $listoderdetail = array_filter($listoderdetail);
                    $listdata = array();
                    if(count($listoderdetail) > 0 ){

                        // set all j_code and brand code all order acceptance with status is 'report already
                        $listoccapprovel = $this->order_acceptance_model->get_all_order_acceptance_brandcode_jcode($jcode,$branch_cd,ORDER_ACCEPTANCE_IS_REPORT_ALREADY);

                        // set all j_code and brand code all order acceptance with status is 'report not already
                        $listocc_notapprovel = $this->order_acceptance_model->get_all_order_acceptance_brandcode_jcode($jcode,$branch_cd,ORDER_ACCEPTANCE_IS_REPORT_NOT_READY);

                        $nlistoccapprovel =$listoccapprovel[0]['n_id_oc'];
                        $nlist_occ_notapprovel = $listocc_notapprovel[0]['n_id_oc'];
                        $data['id'] = $listoderdetail[0]->id ;

                        if( ($nlistoccapprovel >= 0 ) && ($nlist_occ_notapprovel == 0)){
                            $data['is_acceptance_input'] = IS_ACCEPTANCE_INPUT;
                        }
                        else{
                            $data['is_acceptance_input'] = IS_NOT_ACCEPTANCE_INPUT;
                        }
                        $data['is_order_input'] = IS_ORDER_INPUT;
                        $listdata[] = $data;

                        if(count($order_inputs) >0 ){
                            $this->order_detail_model->update_batch('order_detail', $listdata,'id');
                        }
                    }
                }

            }
            $this->order_detail_model->update_status_by_flag($order_inputs);
            $this->db->trans_commit();

        } catch(Exception $e) {
            $this->db->trans_rollback();
//#7090:Start
            $data['listid_import'] = $listid_import;
            $data['status'] = true;
            return $data;
//#7090:End

        }

//#7090:Start
        $data['listid_import'] = $listid_import;
        $data['status'] = true;
        return $data;
//#7090:End


    }

    private function _set_import_error(&$data, $message, $column) {
        // if (!isset($data['error'])) $data['error'] = $message;
        $data['errors'][$column] = $message;
    }

    private function import_common_data($row, &$errors, $datas) {

        $is_calculate_total_amount = true;
        $data_to_be_imported = array();

        foreach ($datas as $column => $data) {
#7100:Start
            $data = trim($data);
#7100:End
            switch ($column) {
                case 5:
                    if (!empty($data)) {
                        $arrange_rep = $this->_check_account_login_id($data);

                        if (!$arrange_rep) {
                            $this->_set_import_error($data_to_be_imported, lang('error_arrange_rep_is_not_exist'), $column);
                        } elseif ($arrange_rep['resignation']) {
                            $this->_set_import_error($data_to_be_imported, str_replace('%field%', lang('lbl_arrange_rep'), lang('error_account_resigned')), $column);
                        }
                    }

                    $data_to_be_imported['arrange_rep_login_id'] = $data;
                    $data_to_be_imported['arrange_rep'] = (!empty($data) && $arrange_rep && !$arrange_rep['resignation']) ? $arrange_rep['id'] : '';
                    $data_to_be_imported['arrange_rep_name'] = (!empty($data) && $arrange_rep && !$arrange_rep['resignation']) ? $arrange_rep['name'] : '';
                    break;

                case 8:
                case 10:
                case 12:
                    if (!$this->_subcontractors) {
//#9695:Start
//                        $all_subcontractors = $this->subcontractor_model->get_subcontractors();
                        $all_subcontractors = $this->subcontractor_model->get_subcontractors_with_business();
//#9695:End
                        foreach ($all_subcontractors as $subcontractor) {
                            $this->_subcontractors['all'][] = $subcontractor['code'];
                            if ($subcontractor['is_no_add'] == 0) {
                                if ($subcontractor['direct_consignment'] == 1) $this->_subcontractors['direct'][] = $subcontractor;
                                $this->_subcontractors['indirect'][] = $subcontractor;
                            }
                        }
                        // echo '<pre>';print_r($this->_subcontractors['direct']);die;
                        // $this->_subcontractors['relation'] = $this->subcontractor_model->get_id_code_of_all_subcontractor_and_relation();
                        // foreach ($this->_subcontractors['relation'] as &$subcontractor) {
                        //     $tmp = array(
                        //         'child_id'        => $subcontractor['parent_id'],
                        //         'child_code'      => $subcontractor['parent_code'],
                        //         'child_name'      => $subcontractor['parent_name'],
                        //         'child_abbr_name' => $subcontractor['parent_abbr_name'],
                        //     );

                        //     array_unshift($subcontractor['children'], $tmp);
                        // }
                    }
                    $direct_subcontractors = array();
//#9695:Start
                    $is_dm_direct_subcontractor = false;
//#9695:End
                    foreach ($this->_subcontractors['direct'] as $subcontractor) {
                        $direct_subcontractors[$subcontractor['id']] = $subcontractor['code'];
                        if ($subcontractor['code'] == $data) {
                            $abbr_name = $subcontractor['abbr_name'];
//#9695:Start
                            if ($subcontractor['finish_work_report_template'] == DM_FWR_TEMPLATE_CODE) $is_dm_direct_subcontractor = true;
//#9695:End
                        }
                    }

                    if ($column == 8) {
                        if (empty($data)) {
                            $this->_set_import_error($data_to_be_imported, lang('error_empty_source_subcontractor'), $column);
                            $data_to_be_imported['source_subcontractor_code'] = $data;
                        } elseif (!in_array($data, $this->_subcontractors['all'])) {
                            $this->_set_import_error($data_to_be_imported, str_replace('%field%', lang('lbl_source_subcontractor_code'), lang('error_is_not_exist')), $column);
                            $data_to_be_imported['source_subcontractor_code'] = $data;
                        } elseif (!in_array($data, $direct_subcontractors)) {
                            $this->_set_import_error($data_to_be_imported, lang('error_source_subcontractor_can_not_use'), $column);
                            $data_to_be_imported['source_subcontractor_code'] = $data;
//#9695:Start
//                        }
                        } elseif (!$is_dm_direct_subcontractor) {
                            $this->_set_import_error($data_to_be_imported, str_replace('%field%', lang('lbl_source_subcontractor_code'), lang('error_is_not_exist')), $column);
                            $data_to_be_imported['source_subcontractor_code'] = $data;
//                        else {
                        } else {
//#9695:End
                            $data_to_be_imported['source_subcontractor_id']   = array_search($data, $direct_subcontractors);
                            $data_to_be_imported['source_subcontractor_code'] = $data;
                            $data_to_be_imported['abbr_name'] = $abbr_name;
                        }

                    }
                    if ($column == 10 && !empty($data) && !empty($datas[8])) {

                        if (!in_array($data, $this->_subcontractors['all'])) {
                            $this->_set_import_error($data_to_be_imported, str_replace('%field%', lang('lbl_work_subcontractor_code'), lang('error_is_not_exist')), $column);
                            $data_to_be_imported['work_subcontractor_code'] = $data;
                            break;
                        }

                        $flag = false;
                        foreach ($this->_subcontractors['indirect'] as $subcontractor) {
                            if ($subcontractor['code'] == $data) {
                                $flag = true;
                                $data_to_be_imported['work_subcontractor_id']   = $subcontractor['id'];
                                $data_to_be_imported['work_subcontractor_code'] = $subcontractor['code'];
                                $data_to_be_imported['work_subcontractor_name'] = $subcontractor['abbr_name'];
//#9695:Start
                                if ($subcontractor['finish_work_report_template'] != DM_FWR_TEMPLATE_CODE) {
                                    $this->_set_import_error($data_to_be_imported, str_replace('%field%', lang('lbl_work_subcontractor_code'), lang('error_is_not_exist')), $column);        
                                }
//#9695:End
                                break;
                            }
                        }

                        if (!$flag) {
                            $this->_set_import_error($data_to_be_imported, lang('error_work_subcontractor_can_not_use'), $column);
                            $data_to_be_imported['work_subcontractor_code'] = $data;
                        }
                    }

                    if ($column == 12 && !empty($data) && !empty($datas[8])) {

                        if (!in_array($data, $this->_subcontractors['all'])) {
                            $this->_set_import_error($data_to_be_imported, str_replace('%field%', lang('lbl_shipping_subcontractor_code'), lang('error_is_not_exist')), $column);
                            $data_to_be_imported['shipping_subcontractor_code'] = $data;
                            break;
                        }

                        $flag = false;
                        foreach ($this->_subcontractors['indirect'] as $subcontractor) {
                            if ($subcontractor['code'] == $data) {
                                $flag = true;
                                $data_to_be_imported['shipping_subcontractor_id']   = $subcontractor['id'];
                                $data_to_be_imported['shipping_subcontractor_code'] = $subcontractor['code'];
                                $data_to_be_imported['shipping_subcontractor_name'] = $subcontractor['abbr_name'];
//#9695:Start
                                if ($subcontractor['finish_work_report_template'] != DM_FWR_TEMPLATE_CODE) {
                                    $this->_set_import_error($data_to_be_imported, str_replace('%field%', lang('lbl_shipping_subcontractor_code'), lang('error_is_not_exist')), $column);        
                                }
//#9695:End
                                break;
                            }
                        }

                        if (!$flag) {
                            $this->_set_import_error($data_to_be_imported, lang('error_shipping_subcontractor_can_not_use'), $column);
                            $data_to_be_imported['shipping_subcontractor_code'] = $data;
                        }
                    }

                    break;

                case 14:
                    if (empty($data)) {
                        $this->_set_import_error($data_to_be_imported, lang('error_empty_shipping_type'), $column);
                    } else {
                        if (!$this->_shipping_types) {
                            $shipping_types = $this->shipping_type_model->list_all();
                            foreach ($shipping_types as $shipping_type) {
                                $this->_shipping_types['all'][] = $shipping_type['code'];
                                if (!$shipping_type['is_no_add']) {
                                    $this->_shipping_types['accept_code'][] = $shipping_type['code'];
                                    $this->_shipping_types['code_name'][$shipping_type['name']] = $shipping_type['code'];
//#6940:Start
                                    $this->_shipping_types['is_require_delivery_date'][$shipping_type['code']] = $shipping_type['is_advance_payment'];
//#6940:End
                                }

                            }
                        }
                        if (!in_array($data, $this->_shipping_types['all'])) {
                            $this->_set_import_error($data_to_be_imported, str_replace('%field%', lang('lbl_shipping_type_code'), lang('error_is_not_exist')), $column);

                        } elseif (!in_array($data, $this->_shipping_types['accept_code'])) {
                            $this->_set_import_error($data_to_be_imported, lang('error_shipping_type_can_not_use'), $column);
                        }
                    }

                    $data_to_be_imported['shipping_type_name'] = !empty($data) ? array_search($data, $this->_shipping_types['code_name']) : '';
                    $data_to_be_imported['shipping_type_cd']   = $data;
                    break;

                case 15:
                    $data_to_be_imported['set_name'] = $data;
                    break;

                case 16:
                    // echo $data;die;
                    if (!empty($data) && !is_numeric($data)) {
                        $this->_set_import_error($data_to_be_imported, lang('error_unit_price_must_be_number'), $column);
                    }
                    if (!empty($data) && $pos = strrpos($data, '.')) {
                        if (strlen(substr($data, 0, $pos)) > 9) {
                            $this->_set_import_error($data_to_be_imported, lang('error_unit_price_max_length'), $column);
                        }
                    } else if (!empty($data) && !strrpos($data, '.')) {
                        if (strlen($data) > 9) {
                            $this->_set_import_error($data_to_be_imported, lang('error_unit_price_max_length'), $column);
                        }
                    }

                    $data_to_be_imported['unit_price'] = $data;
                    $delimiter_pos = strrpos($data, '.');
                    if ($delimiter_pos)
                        $data_to_be_imported['unit_price'] = (float)(substr($data, 0, $delimiter_pos + 3));
                    break;

                case 17:
                    if (!empty($data) && !is_numeric($data)) {
                        $this->_set_import_error($data_to_be_imported, lang('error_quantity_must_be_number'), $column);
                    }

                    if (!empty($data) && $data < 0) {
                        $this->_set_import_error($data_to_be_imported, lang('error_quantity_can_not_be_negative'), $column);
                    }

                    if (!empty($data) && strpos($data, '.')) {
                        $this->_set_import_error($data_to_be_imported, lang('error_quantity_must_be_integer'), $column);
                    }
//#8258:Start
//                     if (!empty($data) && strlen($data) > 6) {
                    if (!empty($data) && strlen($data) > 7) {
//#8258:End
                        $this->_set_import_error($data_to_be_imported, lang('error_quantity_max_length'), $column);
                    }

                    $data_to_be_imported['quantity'] = $data;
                    break;

                case 18:
                    $delimiter = strpos($data, '-') ? '-' : '\/';
                    $patern = '/^[0-9]{4}'.$delimiter.'(0[1-9]|1[0-2])'.$delimiter.'(0[1-9]|[1-2][0-9]|3[0-1])$/';
                    $regex = preg_match($patern, $data);

                    $delimiter = strpos($data, '-') ? '-' : '/';
                    $date = explode($delimiter, $data);

//#6940:Start
//                    if (!empty($data) && !$regex) {
                    if (empty($data) && !empty($datas[14])) {
                        if (isset($this->_shipping_types['is_require_delivery_date'][$datas[14]]) && $this->_shipping_types['is_require_delivery_date'][$datas[14]] == 1) {
                            $this->_set_import_error($data_to_be_imported, lang('error_delivery_date_is_required'), $column);
                        }
                    } elseif (!empty($data) && !$regex) {
                        $this->_set_import_error($data_to_be_imported, lang('error_delivery_date_invalid'), $column);
                    } elseif (!empty($data) && (count($date) != 3 || !checkdate($date[1], $date[2], $date[0]))) {
                        $this->_set_import_error($data_to_be_imported, lang('error_delivery_date_invalid'), $column);
                    }
//#6940:End

                    $data_to_be_imported['delivery_date'] = implode('-', $date);
                    break;

                case 19:
                    if (!empty($data) && !is_numeric($data)) {
                        $this->_set_import_error($data_to_be_imported, lang('error_fee_must_be_number'), $column);
                    }

                    if (!empty($data) && $data < 0) {
                        $this->_set_import_error($data_to_be_imported, lang('error_fee_can_not_be_negative'), $column);
                    }

                    if (!empty($data) && strpos($data, '.')) {
                        $this->_set_import_error($data_to_be_imported, lang('error_fee_must_be_integer'), $column);
                    } else if (!empty($data) && !strpos($data, '.')) {
                        if (strlen($data) > 9) {
                            $this->_set_import_error($data_to_be_imported, lang('error_fee_max_length'), $column);
                        }
                    }

                    $data_to_be_imported['fee'] = $data;
                    break;

                case 20:
                    if (!empty($data) && !is_numeric($data)) {
                        $this->_set_import_error($data_to_be_imported, lang('error_weight_must_be_number'), $column);
                    }

                    if (!empty($data) && $data < 0) {
                        $this->_set_import_error($data_to_be_imported, lang('error_weight_can_not_be_negative'), $column);
                    }
//#7090:Start
                    if (!empty($data) && $pos = strpos($data, '.')) {
                        // $this->_set_import_error($data_to_be_imported, lang('error_weight_must_be_integer'), $column);
                        if (strlen(substr($data, 0, $pos)) > 4) {
                            $this->_set_import_error($data_to_be_imported, lang('error_weight_max_length'), $column);
                        }
                    }
//#7090:End
                    $data_to_be_imported['weight'] = ($pos = strpos($data, '.')) ? substr($data, 0, $pos + 2) : $data;
                    break;

                case 22:
                    if (!empty($data) && !$this->_check_is_shipment_shape_code($data)) {
                        $this->_set_import_error($data_to_be_imported, lang('error_shipments_shape_id_is_not_exist'), $column);
                    }
                    foreach ($this->_get_shipment_shape_code() as $shipment_shape) {
                        if ($shipment_shape['code'] == $data) {
                            $data_to_be_imported['shipments_shape_name'] = $shipment_shape['name'];
                        }
                    }
                    $data_to_be_imported['shipments_shape_cd'] = $data;
                    break;

                case 24:
                    if (!empty($data) && !is_numeric($data)) {
                        $this->_set_import_error($data_to_be_imported, lang('error_tax_free_must_be_number'), $column);
                        $is_calculate_total_amount = false;
                    }

                    if (!empty($data) && strpos($data, '.')) {
                        $this->_set_import_error($data_to_be_imported, lang('error_tax_free_must_be_integer'), $column);
                    } else if (!empty($data) && !strpos($data, '.')) {
                        if (strlen($data) > 9) {
                            $this->_set_import_error($data_to_be_imported, lang('error_tax_free_max_length'), $column);
                        }
                    }

                    $data_to_be_imported['tax_free'] = $data;
                    break;

                case 25:
                    if (!empty($data) && !is_numeric($data)) {
                        $this->_set_import_error($data_to_be_imported, lang('error_shipping_charge_tax_must_be_number'), $column);
                        $is_calculate_total_amount = false;
                    }

                    if (!empty($data) && strpos($data, '.')) {
                        $this->_set_import_error($data_to_be_imported, lang('error_shipping_charge_tax_must_be_integer'), $column);
                    } else if (!empty($data) && !strpos($data, '.')) {
                        if (strlen($data) > 9) {
                            $this->_set_import_error($data_to_be_imported, lang('error_shipping_charge_tax_max_length'), $column);
                        }
                    }

                    $data_to_be_imported['shipping_charge_tax'] = $data;
                    break;

                case 26:
                    if (!empty($data) && !is_numeric($data)) {
                        $this->_set_import_error($data_to_be_imported, lang('error_shipping_stamp_tax_must_be_number'), $column);
                        $is_calculate_total_amount = false;
                    }

                    if (!empty($data) && strpos($data, '.')) {
                        $this->_set_import_error($data_to_be_imported, lang('error_stamp_tax_must_be_integer'), $column);
                    } else if (!empty($data) && !strpos($data, '.')) {
                        if (strlen($data) > 9) {
                            $this->_set_import_error($data_to_be_imported, lang('error_stamp_tax_max_length'), $column);
                        }
                    }

                    $data_to_be_imported['stamp_tax'] = $data;
                    break;

                case 27:
                    if (!empty($data) && !is_numeric($data)) {
                        $this->_set_import_error($data_to_be_imported, lang('error_etc_tax_must_be_number'), $column);
                        $is_calculate_total_amount = false;
                    }

                    if (!empty($data) && strpos($data, '.')) {
                        $this->_set_import_error($data_to_be_imported, lang('error_etc_tax_must_be_integer'), $column);
                    } else if (!empty($data) && !strpos($data, '.')) {
                        if (strlen($data) > 9) {
                            $this->_set_import_error($data_to_be_imported, lang('error_etc_tax_max_length'), $column);
                        }
                    }

                    $data_to_be_imported['etc_tax'] = $data;
                    break;

                case 28:

// #7090:Start
//#7774:Start
//                 	$checkval =  true;
// 					if(!empty($datas[23])){

// 						if(empty($datas[1]) && empty($datas[2])){
// 						    $checkval = Order_acceptance_model::check_year_month($datas[23]);
// 						}
// 					}

//                     if($checkval === false ){
//                     		$this->_set_import_error($data_to_be_imported,lang("lbl_error_format_summary_month"), $column);
//                     }
//                     else{
//#7774:End

                        $valsm = true;
                        if (!empty($data) && !is_numeric($data)) {
                            $this->_set_import_error($data_to_be_imported, lang('error_shipping_charge_must_be_number'), $column);
                            $is_calculate_total_amount = false;
                            $valsm = false;
                        }
                        if (!empty($data) && strpos($data, '.')) {
                            $this->_set_import_error($data_to_be_imported, lang('error_shipping_charge_must_be_integer'), $column);
                            $valsm = false;
                        } else if (!empty($data) && !strpos($data, '.')) {
                            if (strlen($data) > 9) {
                                $this->_set_import_error($data_to_be_imported, lang('error_shipping_charge_max_length'), $column);
                                $valsm = false;
                            }
                        }
//#7774:Start
//                     }
//#7774:End
// #7090:End

                    $data_to_be_imported['shipping_charge'] = $data;
                    break;

                case 29:
                    if (!empty($data) && !is_numeric($data)) {
                        $this->_set_import_error($data_to_be_imported, lang('error_etc_must_be_number'), $column);
                        $is_calculate_total_amount = false;
                    } else if (!empty($data) && !strpos($data, '.')) {
                        if (strlen($data) > 9) {
                            $this->_set_import_error($data_to_be_imported, lang('error_etc_max_length'), $column);
                        }
                    }

                    if (!empty($data) && strpos($data, '.')) {
                        $this->_set_import_error($data_to_be_imported, lang('error_etc_must_be_integer'), $column);
                    }

                    $data_to_be_imported['etc'] = $data;
                    break;

                default:
                    break;
            }
        }
//#7125:Start
//        if ($is_calculate_total_amount) {
//            $data_to_be_imported['amount'] = $this->calculate_total_amount($datas[24], $datas[25], $datas[26], $datas[27], $datas[28], $datas[29]);
//        }
//#7125:End

        $data_to_be_imported['lastup_account_id'] = $this->auth->get_account_id();
        $data_to_be_imported['create_datetime']   = date('Y-m-d H:i:s');
        return $data_to_be_imported;

    }
    private function import_have_jcode_branch($row, &$errors, $datas) {
        $this->load->model('closing_accountant_model');

        if ($closing_month_recent = $this->closing_accountant_model->get_closing_month_recent()) {
            $closing_month = date('Y-m-01', strtotime($closing_month_recent[0]['close_date']));
        } else {
            $closing_month = date('Y-m-01');
        }

        $data_to_be_imported = array();

        $order = $this->get_order_detail($datas[0], $datas[1]);



        if ($order) {
//#9695:Start
            if ($order->finish_work_report_template != DM_FWR_TEMPLATE_CODE) {
                $delivery_date = null;
                $this->_set_import_error($data_to_be_imported, lang('error_jcode_branch_is_not_exist'), 0);
            } else {
//#9695:End
                // #6901 -Add -S
                $is_change_report = $order->is_change_report_apply;

                if($is_change_report){

                    $this->_set_import_error($data_to_be_imported, lang('error-change-report-order-detail'), 0);

                }
                // #6901 -Add -E
                $delivery_date = substr($order->delivery_date, 0, 7);
                $data_to_be_imported['order_id'] = $order->id;
//#7090:Start
//             if ($order->is_acceptance_input || (strtotime($delivery_date . '-01') <=  strtotime($closing_month))) {
                if (strtotime($delivery_date . '-01') <=  strtotime($closing_month)) {
//#7090:End
                    $this->_set_import_error($data_to_be_imported, lang('error_finalization'), 0);
                }
            }
        } else {
            $delivery_date = null;
            $this->_set_import_error($data_to_be_imported, lang('error_jcode_branch_is_not_exist'), 0);
        }

        foreach ($datas as $column => $data) {
#7100:Start
            $data = trim($data);
#7100:End
            switch ($column) {
                case 0:
                    $data_to_be_imported['j_code'] = $data;
                    break;
                case 1:
                    if (strlen($data) > 2) {
                        $this->_set_import_error($data_to_be_imported, lang('error_branch_only_two_characters'), $column);
                    } else {
                        $data_to_be_imported['branch_cd'] = preg_replace('/^[0-9]$/', '0$0', $data);
                    }
                    break;

                case 3:
//#9695:Start
                    if ($order && $order->finish_work_report_template != DM_FWR_TEMPLATE_CODE) {
                        $this->_set_import_error($data_to_be_imported, lang('error_account_business'), $column);
                    }
//#9695:End
                    $data_to_be_imported['account_id'] = $order ? $order->j_account_id : '';
                    $data_to_be_imported['login_id']   = $order ? $order->login_id : '';
                    $data_to_be_imported['login_name']   = $order ? $order->name : '';
                    break;

                case 6:
                    $data_to_be_imported['client_name'] = $order ? $order->client_name : '';
                    break;

                case 23:
                    $data_to_be_imported['summary_month'] = $delivery_date;
                    break;

                default:
                    break;
            }
        }

        return $data_to_be_imported;
    }

    private function import_do_not_jcode_branch($row, &$errors, $datas) {
        $data_to_be_imported = array();

        foreach ($datas as $column => $data) {
#7100:Start
            $data = trim($data);
#7100:End
            switch ($column) {
                case 3:
                    if (empty($data)) {
                        $this->_set_import_error($data_to_be_imported, lang('error_empty_account_id'), $column);
                    } else {
                        $account = $this->_check_account_login_id($data);
                        if (!$account) {
                            $this->_set_import_error($data_to_be_imported, lang('error_account_id_is_not_exist'), $column);
                        } elseif ($account['resignation']) {
                            $this->_set_import_error($data_to_be_imported, str_replace('%field%', lang('lbl_account_id'), lang('error_account_resigned')), $column);
                        }
                    }

                    $data_to_be_imported['login_id']   = $data;
                    $data_to_be_imported['account_id'] = (!empty($data) && $account && !$account['resignation']) ? $account['id'] : null;
                    $data_to_be_imported['login_name'] = (!empty($data) && $account && !$account['resignation']) ? $account['name'] : null;

                    break;

                case 6:
                    if (empty($data)) {
                        $this->_set_import_error($data_to_be_imported, lang('error_empty_client_name'), $column);
                    }
                    $data_to_be_imported['client_name'] = $data;
                    break;

                case 23:
//#7774:Start
                    $checkval =  true;
                    if(!empty($datas[23])){

                        if(empty($datas[1]) && empty($datas[2])){
                            $checkval = Order_acceptance_model::check_year_month($datas[23]);
                        }
                    }

                    if($checkval === false ){
                                $this->_set_import_error($data_to_be_imported,lang("lbl_error_format_summary_month"), $column);
                    }
                    else{
//#7774:End

                            if (empty($data)) {
//#7774:Start
        // #7090:Start
//		    $this->_set_import_error($data_to_be_imported, lang('error_summary_month_has_not_close'), $column);
                                $this->_set_import_error($data_to_be_imported,lang('error_delivery_date_required'), $column);
        // #7090:End
//#7774:End
                            }

        //#7091:Start
                            // $delimiter = strpos($data, '/') ? '/' : '-';
                            // $split = explode($delimiter, $data);

                            if (!empty($data)) {
                                // if (count($split) != 3) {
                                //     $this->_set_import_error($data_to_be_imported, lang('error_summary_month_has_not_close'), $column);
                                // } else {
                                //     if (!checkdate(intval($split[1]), intval($split[2]), intval($split[0]))) {
                                if (!parse_date($data)) {
        // #7090:Start
                                        $this->_set_import_error($data_to_be_imported,lang('error_summary_month_has_not_close'), $column);
        // #7090:End
                                } else {
                                    $data = substr(parse_date($data), 0, 7);
                                    // $data = $split[0] . '-' . $split[1];
                                    if ($this->closing_accountant_model->check_has_closed_or_not($data)) {

        // #7090:Start
                                        $this->_set_import_error($data_to_be_imported,  lang('error_summary_month_has_not_close'), $column);
        // #7090:End
                                    }
                                }
                            }
//#7774:Start
                    }
//#7774:End


                    // $data_to_be_imported['summary_month'] = substr(implode('-', $split), 0, 7);
                    $data_to_be_imported['summary_month'] = $data;



//#7091:End
                    break;

                default:
                    break;
            }
        }

        return $data_to_be_imported;
    }

    /**
     * concat j_code and branch_cd
     * @return array
     */
    public function get_all_jcode_branch() {
        $this->db->select('CONCAT(oc.j_code, oc.branch_cd) as jcode_branch', false)
                ->from('order_acceptance oc')
                ->join('order o', 'o.id = oc.order_id', 'left')
                ->where('oc.disable', 0)
                ->order_by('jcode_branch', 'ASC');

        $results = $this->db->get()->result_array();
        foreach ($results as $result) {
            $datas[] = $result['jcode_branch'];
        }
        return $datas;
    }
    public function get_all_order_acceptance_brandcode_jcode($jcode,$brandcode,$status,$isWhereStatus = true) {

        if($isWhereStatus)
        {
                $this->db->select('count(id) as n_id_oc ,oc.order_acceptance_status', false)
                ->from('order_acceptance oc')
                ->where('oc.j_code',$jcode)
//#10077:start
//                ->where('oc.branch_cd',$brandcode)
                ->where('oc.branch_cd = '.$brandcode)
//#10077:start
                ->where('oc.order_acceptance_status',$status)
                ->where("oc.is_extra_item",0)
                ->where('oc.disable', 0);

                 $data_return  = $this->db->get()->result_array();
                 //echo $this->db->last_query();
                return $data_return;
        }
        else{
            $this->db->select('oc.id,oc.j_code,oc.branch_cd', false)
            ->from('order_acceptance oc')
            ->where('oc.j_code',$jcode)
            ->where('oc.branch_cd',$brandcode)
            ->where("oc.is_extra_item",0)
            ->where('oc.disable', 0);

            $data_return  = $this->db->get()->result_array();
            //echo $this->db->last_query();
            return $data_return;
        }

    }
    /**
     * get a list of j_code and brand_cd base on the matrix rate
     *
     * @example jcode             branch
     *             11                 01
     *             11                 02
     *             12                 03
     *             12                 04
     *             15                 05
     * if from is 1 and to is 02 then return
     *             11                 01
     *             11                 02
     * else from is 11 and to is 03 then return
     *             11                 01
     *             11                 02
     *             12                 03
     * @param  string $from_jcode_branch
     * @param  string $to_jcode_branch
     * @return array
     */
    public function create_jcode_branch_search_conditions($from_jcode_branch, $to_jcode_branch) {
        $from_jcode_branch = trim($from_jcode_branch) != "" ? $from_jcode_branch : null;
        $to_jcode_branch   = trim($to_jcode_branch) != "" ? $to_jcode_branch : null;

        $all_jcode_branch = $this->get_all_jcode_branch();

        $flag = false;
        $start = $end = null;
        foreach ($all_jcode_branch as $key => $jcode_branch) {
            $jcode_branch = strval($jcode_branch);

            if (!is_null($from_jcode_branch)) {
                if (!$flag && strpos($jcode_branch, strval($from_jcode_branch)) === 0) {
                    $flag = true;
                    $start = $key;
                }
            }

            if (!is_null($to_jcode_branch)) {
                if (strpos($jcode_branch, strval($to_jcode_branch)) === 0) {
                    $end = $key;
                }
            }
        }

        if (!is_null($start) || !is_null($end)) {
            $start = $start ? $start : 0;
            $end = $end ? $end : (count($all_jcode_branch) - 1);
            $conditions = array();
            while ($start <= $end) {
                $conditions[] = $all_jcode_branch[$start];
                $start++;
            }

            return $conditions;
        }

        return false;
    }
     /**
     *Get right date  when iput date
     * @param  datetime $date / string $date
     * @param  bool isNotDay : return date without day
     * @param  bool $onlyYear : return only year
     * @param  bool $onlyMonth : return only month
     * @param  bool $onlyDay : return only day
     * @return datetime $dateturn
     * @todo if account id is select all
     */
     protected   function getRightFomatDate($date,$isNotDay = false ,$onlyYear = false ,$onlyMonth = false, $onlyDay = false,$isToDate = false){

         $str_search    = array("/", "-", ".");
         $str_replace   = array("-", "-", "-");
         $date  = str_replace ($str_search,$str_replace,$date);

         $listdate = explode("-",$date);
         $n_to_day_delvie = count($listdate);
         $year = "";$moth = "";$day="";

         if($n_to_day_delvie == 2){
             $year = $listdate[0];
             $moth = $listdate[1];
             if (strlen($year) == 2){ $year = "20".$year;}

             $moth = $listdate[1];
             if(strlen($moth) == 1){
                 $moth =  "0".$moth;
             }
         }
         elseif($n_to_day_delvie == 3){
             $year =  $listdate[0];
             $moth =  $listdate[1];
             $day =   $listdate[2];

             if (strlen($year) == 2){ $year = "20".$year;}

             $moth = $listdate[1];
             if(strlen($moth) == 1){
                 $moth =  "0".$moth;
             }
             if(strlen($day) == 1){
                 $day =  "0".$day;
             }
         }
         else{

             $year = $listdate[0];
             if (strlen($year) == 2){ $year = "20".$year;}
         }



         if($onlyYear){
             return $year;
         }
         elseif ($onlyMonth){
             return $moth;
         }
         elseif ($onlyDay){
             return $day;
         }
         else{

                 if(!$isNotDay){
                     if(empty($day)){
                         if($isToDate){
                             $day = cal_days_in_month(CAL_GREGORIAN,$moth, $year);
                         }
                         else{
                             $day = "01";
                         }
                     }
                     return $year."-".$moth."-".$day;
                 }
                 else{
                     return $year."-".$moth;
                 }

         }


     }

    /**
     * search order acceptance
     *
     * @param  array $data
     * @return array
     * @todo if account id is select all
     */
    public function search($data) {

        $postFormOrder = 0 ;

        if(isset($data['postFormOrder'])){
            $postFormOrder = $data['postFormOrder'];
        }

        $this->db->select('order_detail.is_acceptance_input AS flag_is_accceptance_input,
                        , oc.*
                        , o.client_id
                        , CONCAT(oc.j_code, oc.branch_cd) as jcode_branch'.
//Fixbug taskcheckbox:Start
                        ', oc.amount as amount_frist_bd'
//Fixbug taskcheckbox:End
                        , false)
                ->from('order_acceptance oc')
                ->join('order o', 'o.id = oc.order_id AND o.disable = ' . STATUS_ENABLE, 'left')
                ->join('order_detail', 'order_detail.order_id = o.id AND order_detail.branch_cd = oc.branch_cd AND order_detail.disable = ' . STATUS_ENABLE, 'left')
//#9748:Start
                ->join('account a', 'a.id = oc.account_id AND a.disable = ' . STATUS_ENABLE)
                ->join('department d', 'd.id = a.department_id AND d.disable = ' . STATUS_ENABLE)
                ->join('business_unit bu', 'bu.id = d.business_unit_id AND bu.disable = ' . STATUS_ENABLE)
                ->where('bu.finish_work_report_template', DM_FWR_TEMPLATE_CODE)
//#9748:End
                ->where('oc.disable', STATUS_ENABLE)
                ->where('oc.is_extra_item', STATUS_ENABLE)
                ->order_by('oc.j_code', 'ASC')
                ->order_by('oc.branch_cd', 'ASC')
                ->order_by('oc.set_name', 'ASC')
                ->order_by('oc.delivery_date', 'ASC')
                ->order_by('oc.shipping_type_cd', 'ASC');

        if (isset($data['account_id']) && $data['account_id'] != -1) {
            $this->db->where('oc.account_id', $data['account_id']);
        } elseif (isset($data['business_unit_id']) && $data['business_unit_id'] != -1) {
//#9748:Start
//            $this->db->join('account a', 'a.id = oc.account_id AND a.disable = ' . STATUS_ENABLE)
//                    ->join('department d', 'd.id = a.department_id AND d.disable = ' . STATUS_ENABLE)
//                    ->where('d.business_unit_id', $data['business_unit_id']);
            $this->db->where('bu.id', $data['business_unit_id']);
//#9748:End
        }

        $losingDatedefault = $this->_get_closing_date();

        //tmp#6870: Start
        // filter by Status And Closing accountance (承認状態)
        if (!empty($data['order_acceptance_status'])) {
            if (($key = array_search(ORDER_ACCEPTANCE_ACCOUNT_TIGHTEN, $data['order_acceptance_status'])) !== false) {
                unset($data['order_acceptance_status'][$key]);
                //tmp#6870: Start
                $closing_date_after = date('Y-m', strtotime("+1 month", strtotime($losingDatedefault . '-01')));

                $this->db->where('oc.summary_month <', $closing_date_after);
                //tmp#6870: End
            } else {
                //tmp#6870: Start
                if (!isset($data['is_from_order'])) {
                    $this->db->where('oc.summary_month >', $losingDatedefault);
                }
                //tmp#6870: Start
            }

            if (!empty($data['order_acceptance_status'])) {
                $this->db->where_in('oc.order_acceptance_status', $data['order_acceptance_status']);
            }
        } else {
            // #6741-S Modify
            $this->db->where_in('oc.order_acceptance_status', array(ORDER_ACCEPTANCE_UNAPPROVED, ORDER_ACCEPTANCE_APPROVED));
            // #6741-E Modify

            /*tmp#6870: Start
            $this->db->where('oc.summary_month >', $losingDatedefault);
            tmp#6870: End*/
        }

        if (isset($data['arrange_rep']) && $data['arrange_rep'] != -1) {
            $this->db->where('oc.arrange_rep', $data['arrange_rep']);
        }
        if (isset($data['client_name']) && $data['client_name']) {
            $this->db->like('oc.client_name', $data['client_name']);
        }
        if (isset($data['source_subcontractor_name']) && $data['source_subcontractor_name']) {
            $this->db->join('subcontractor sos', 'sos.id = oc.source_subcontractor_id')
                    ->like('sos.name', $data['source_subcontractor_name']);
        }
        if (isset($data['work_subcontractor_name']) && $data['work_subcontractor_name']) {
            $this->db->join('subcontractor wos', 'wos.id = oc.work_subcontractor_id')
                    ->like('wos.name', $data['work_subcontractor_name']);
        }
        if (isset($data['shipping_subcontractor_name']) && $data['shipping_subcontractor_name']) {
            $this->db->join('subcontractor shs', 'shs.id = oc.shipping_subcontractor_id')
                    ->like('shs.name', $data['shipping_subcontractor_name']);
        }
        if (isset($data['shipping_type_cd']) && $data['shipping_type_cd'] != -1) {
            $this->db->where('oc.shipping_type_cd', $data['shipping_type_cd']);
        }

        if (!empty($data['from_summary_month']) && empty($data['to_summary_month'])) {
            $to_date = $data['from_summary_month'];
            $year = $this->getRightFomatDate($to_date, false, true);
            $moth = $this->getRightFomatDate($to_date, false, false, true);
            if ($year != "") {
//#7156:Start
//                $this->db->where(" YEAR(DATE_FORMAT(STR_TO_DATE(oc.summary_month,'%Y-%m-%d'), '%Y-%m-%d')) = ", $year, FALSE);
                $this->db->where(" YEAR(DATE_FORMAT(STR_TO_DATE(oc.summary_month,'%Y-%m-%d'), '%Y-%m-%d')) >= ", $year, FALSE);
//#7156:End
            }
            if ($moth != ""):
//#7156:Start
//                $this->db->where(" MONTH(DATE_FORMAT(STR_TO_DATE(oc.summary_month,'%Y-%m-%d'), '%Y-%m-%d')) = ", $moth, FALSE);
                $this->db->where(" MONTH(DATE_FORMAT(STR_TO_DATE(oc.summary_month,'%Y-%m-%d'), '%Y-%m-%d')) >= ", $moth, FALSE);
//#7156:End
            endif;
        } elseif (empty($data['from_summary_month']) && !empty($data['to_summary_month'])) {
            $to_date = $data['to_summary_month'];
            $to_date = $this->getRightFomatDate($to_date, true);
            $this->db->where(" DATE_FORMAT(STR_TO_DATE(oc.summary_month,'%Y-%m-%d'), '%Y-%m') <= ", $to_date);
        } else {
            if (!empty($data['from_summary_month']) && !empty($data['to_summary_month'])) {


                $form_summarymonth = $data['from_summary_month'];

                $to_summarymonth = $data['to_summary_month'];

                $dayss = $this->getRightFomatDate($form_summarymonth, true);

                $dayss_to = $this->getRightFomatDate($to_summarymonth, true);

                $this->db->where(" DATE_FORMAT(STR_TO_DATE(oc.summary_month,'%Y-%m-%d'), '%Y-%m') >= ", $dayss);
                $this->db->where(" DATE_FORMAT(STR_TO_DATE(oc.summary_month,'%Y-%m-%d'), '%Y-%m') <= ", $dayss_to);
            }
        }

        if (!empty($data['from_delivery_date']) && empty($data['to_delivery_date'])) {
            $form_day_delvie = $data['from_delivery_date'];
            $year = $this->getRightFomatDate($form_day_delvie, false, true);
            $moth = $this->getRightFomatDate($form_day_delvie, false, false, true);
            $day = $this->getRightFomatDate($form_day_delvie, false, false, false, true);
            if ($year != ""):
//#7156:Start
//                $this->db->where('  YEAR(oc.delivery_date) ', $year);
                $this->db->where('  YEAR(oc.delivery_date) >=', $year);
//#7156:End
            endif;

            if ($moth != ""):
//#7156:Start
//                $this->db->where('  MONTH(oc.delivery_date) ', $moth);
                $this->db->where('  MONTH(oc.delivery_date) >=', $moth);
//#7156:End
            endif;

            if ($day != ""):
//#7156:Start
//                $this->db->where('  DAY(oc.delivery_date) ', $day);
                $this->db->where('  DAY(oc.delivery_date) >=', $day);
//#7156:End
            endif;

        } elseif (!empty($data['to_delivery_date']) && !empty($data['from_delivery_date'])) {

            $from_delivery_date =  $data['from_delivery_date'];
            $to_delivery_date   =  $data['to_delivery_date'];
            $formdatedv = $this -> getRightFomatDate($from_delivery_date);
            $todatedv   = $this->getRightFomatDate($to_delivery_date,false,false,false,false,true);
            $this->db->where(" oc.delivery_date >= ", $formdatedv);
            $this->db->where(" oc.delivery_date <= ", $todatedv);

            //             list($from_year, $from_month, $from_day) = array_pad(explode('-', $from_delivery_date), 3, null);
            //             list($to_year, $to_month, $to_day)       = array_pad(explode('-', $to_delivery_date), 3, null);

            //             if ($from_year) {
            //                 $this->db->where('YEAR(oc.delivery_date) >= ', $from_year);
            //             }
            //             if ($from_month) {
            //                 $this->db->where('MONTH(oc.delivery_date) >= ', $from_month);
            //             }
            //             if ($from_day) {
            //                 $this->db->where('DAY(oc.delivery_date) >= ', $from_day);
            //             }

            //             if ($to_year) {
            //                 $this->db->where('YEAR(oc.delivery_date) <= ', $to_year);
            //             }
            //             if ($to_month) {
            //                 $this->db->where('MONTH(oc.delivery_date) <= ', $to_month);
            //             }
            //             if ($to_day) {
            //                 $this->db->where('DAY(oc.delivery_date) <= ', $to_day);
            //             }

        }

//#7067:Start
//         if ((isset($data['from_jcode_branch']) ) && (trim($data['to_jcode_branch']) == "")&& (trim($data['from_jcode_branch']) != "")) {

//            $formbrandcode =  $data['from_jcode_branch'];

//            $n_form_brandcode =  strlen($formbrandcode);

//            if($n_form_brandcode <= 8){
//                      $this->db->where('oc.j_code LIKE "'.trim($formbrandcode.'%"'));
//                //     $this->db->where('if (CONCAT(oc.j_code) <> "", CONCAT(oc.j_code, oc.branch_cd), "000000000") >=', str_pad($data['from_jcode_branch'], 9, "0", STR_PAD_RIGHT));
//            }
//            else{

//                   $varjcode  = substr($formbrandcode,0,8);
//                   $varbranchcode = str_replace($varjcode,"",$formbrandcode);
//                   $this->db->where('oc.j_code LIKE "'.trim($varjcode.'%"'));
//                   $this->db->where('oc.branch_cd LIKE"'.trim($varbranchcode.'%"'));
//            }
//            // $this->db->where('if (CONCAT(oc.j_code, oc.branch_cd) <> "", CONCAT(oc.j_code, oc.branch_cd), "000000000") >=', str_pad($data['from_jcode_branch'], 9, "0", STR_PAD_RIGHT));


//         }
//         elseif (isset($data['to_jcode_branch']) && (trim($data['from_jcode_branch'])=="")&& (trim($data['to_jcode_branch']) != "")) {

//             // $to_jcode_branch =  $data['to_jcode_branch'];
//             // $n_tohjodebranch =  strlen($to_jcode_branch);
//             // if($n_tohjodebranch <= 8){

//             //      $this->db->where('if (CONCAT(oc.j_code) <> "", CONCAT(oc.j_code), "000000000") <=', str_pad($to_jcode_branch,9,"0", STR_PAD_RIGHT));
//             // }
//             // else{

//             //     $varjcode  = substr($to_jcode_branch,0,8);
//             //     $varbranchcode = str_replace($varjcode,"",$to_jcode_branch);

//             //     $this->db->where('if (CONCAT(oc.j_code) <> "", CONCAT(oc.j_code), "00000000") <=', str_pad($varjcode,8,"0", STR_PAD_RIGHT));
//             //     $this->db->where('if (CONCAT(oc.branch_cd) <> "", CONCAT(oc.branch_cd), "00") <=', str_pad($varbranchcode,2,"0", STR_PAD_RIGHT));

//             // }

//         }
//         else{
//             if(isset($data['from_jcode_branch'])){
//               $formbrandcode =  $data['from_jcode_branch'];
//             }
//             else{
//                 $formbrandcode = "";
//             }
//             $n_form_brandcode =  strlen($formbrandcode);


//             if(isset($data['to_jcode_branch'])){
//                 $to_jcode_branch =  $data['to_jcode_branch'];
//             }
//             else{
//                 $to_jcode_branch ="";
//             }

//             $n_tohjodebranch =  strlen($to_jcode_branch);

//             if($n_form_brandcode > 0 ){
//                     if($n_form_brandcode <= 8 ){
//                           //$this->db->where('CONVERT(FLOAT,oc.j_code) >=',(int)$formbrandcode);
//                         $this->db->where('if (CONCAT(oc.j_code) <> "", CONCAT(oc.j_code), "00000000") >=', str_pad($formbrandcode,8,"0", STR_PAD_RIGHT));
//                     }
//                     else{
//                         $varjcode  = substr($formbrandcode,0,8);
//                         $varbranchcode = str_replace($varjcode,"",$formbrandcode);

//                         ///echo 'varcode'.$varjcode.',varbranchcode'.$varbranchcode;

//                         $this->db->where('if (CONCAT(oc.j_code) <> "", CONCAT(oc.j_code), "00000000") >=', str_pad($varjcode,8,"0", STR_PAD_RIGHT));
//                         $this->db->where('if (CONCAT(oc.branch_cd) <> "", CONCAT(oc.branch_cd), "00") >=', str_pad($varbranchcode,2,"0", STR_PAD_RIGHT));
//                     }

//            }
//            if($n_tohjodebranch){
//                    if($n_tohjodebranch <= 8){

//                         for($k = $n_tohjodebranch ; $k <= 8; $k++){
//                              $to_jcode_branch.= "9";
//                         }
//                     $this->db->where('if (CONCAT(oc.j_code) <> "", CONCAT(oc.j_code), "00000000") <=', str_pad($to_jcode_branch,8,"0", STR_PAD_RIGHT));
//                        //$this->db->where('CONVERT(FLOAT,oc.j_code) <=',(int)$to_jcode_branch);
//                 }
//                 else{


//                     $varjcode  = substr($to_jcode_branch,0,8);
//                     $varbranchcode = str_replace($varjcode,"",$to_jcode_branch);
//                     //echo 'varcode'.$varjcode.',varbranchcode'.$varbranchcode;

//                     $this->db->where('if (CONCAT(oc.j_code) <> "", CONCAT(oc.j_code), "00000000") <=', str_pad($varjcode,8,"0", STR_PAD_RIGHT));
//                     $this->db->where('if (CONCAT(oc.branch_cd) <> "", CONCAT(oc.branch_cd), "00") <=', str_pad($varbranchcode,2,"0", STR_PAD_RIGHT));

//                 }
//            }

//         }



            $wheresql_jocde_branch = $this->get_where_sql_by_branchcode_and_jcode($data,'oc.j_code','oc.branch_cd');

            if(!empty($wheresql_jocde_branch)){
                $this->db->where($wheresql_jocde_branch);
            }

//#7067:End
// 		$data_re = $this->db->get()->result_array();
// 		echo $this->db->last_query();
// 		exit();

//#7090:Start
        if(isset($data['list_id_oc_imput']['listid_import'])){

                if(($data['list_id_oc_imput']['listid_import'])){
                    $this->db->where_in('oc.id', $data['list_id_oc_imput']['listid_import']);
                }

        }
//#7090:End
        return $this->db->get()->result_array();
    }


    private function _set_data($order_acceptance) {

        foreach ($order_acceptance as $k => $v) {

            if (empty($v)) $order_acceptance[$k] = 0;
            if (empty($v) && $k == 'j_code') $order_acceptance[$k] = '';
            if (empty($v) && $k == 'branch_cd') $order_acceptance[$k] = '';
            if (empty($v) && $k == 'client_name') $order_acceptance[$k] = '';
            if (empty($v) && $k == 'set_name') $order_acceptance[$k] = '';
            if (empty($v) && $k == 'summary_month') $order_acceptance[$k] = '';
            if (empty($v) && $k == 'summary_month') $order_acceptance[$k] = '';
            if (empty($v) && $k == 'delivery_date_for_save') {
                $order_acceptance[$k] = '0000-00-00';
            }
            if (empty($v) && $k == 'summary_month') $order_acceptance[$k] = '';

            if ( (empty($v) && $k == 'tax_free')
                || (empty($v) && $k == 'shipping_charge_tax')
                || (empty($v) && $k == 'stamp_tax')
                || (empty($v) && $k == 'etc_tax')
                || (empty($v) && $k == 'shipping_charge')
                || (empty($v) && $k == 'etc')) {

                if (trim($v) == "") {
                    $order_acceptance[$k] = "";
                } else {
                    $order_acceptance[$k] = 0;
                }
            }
        }

        $data = array (
            'account_id'                => $order_acceptance['account_id'],
            'arrange_rep'               => $order_acceptance['arrange_rep'],
            'client_name'               => $order_acceptance['client_name'],
            'source_subcontractor_id'   => $order_acceptance['source_subcontractor_id'],
            'work_subcontractor_id'     => $order_acceptance['work_subcontractor_id'],
            'shipping_subcontractor_id' => $order_acceptance['shipping_subcontractor_id'],
            'shipping_type_cd'          => $order_acceptance['shipping_type_cd'],
            'set_name'                  => $order_acceptance['set_name'],
            'unit_price'                => $order_acceptance['unit_price'],
            'quantity'                  => $order_acceptance['quantity'],
            'fee'                       => $order_acceptance['fee'],
            'weight'                    => $order_acceptance['weight'],
            'shipments_shape_cd'        => $order_acceptance['shipments_shape_cd'],
            'summary_month'             => date('Y-m', strtotime(str_replace('/', '-', $order_acceptance['summary_month'].'-'.'01'))),
            'tax_free'                  => $order_acceptance['tax_free'],
            'shipping_charge_tax'       => $order_acceptance['shipping_charge_tax'],
            'stamp_tax'                 => $order_acceptance['stamp_tax'],
            'etc_tax'                   => $order_acceptance['etc_tax'],
            'shipping_charge'           => $order_acceptance['shipping_charge'],
            'etc'                       => $order_acceptance['etc'],
            'lastup_account_id'         => $this->auth->get_account_id(),
        );


        // #6801-S
        if (isset($order_acceptance['delivery_date_for_save'])) {
            $data['delivery_date'] = $order_acceptance['delivery_date_for_save'];
        }

//#6961:Start
//         else {
//             $data['delivery_date'] = "0000-00-00";
//         }
//#6961:End
        // #6801-E

//#7125:Start
//        $data['amount'] = $this->calculate_total_amount($order_acceptance['tax_free'], $order_acceptance['shipping_charge_tax'], $order_acceptance['stamp_tax'], $order_acceptance['etc_tax'], $order_acceptance['shipping_charge'], $order_acceptance['etc']);
        $data['amount'] = $this->calculate_total_amount($order_acceptance['tax_free'], $order_acceptance['shipping_charge_tax'], $order_acceptance['stamp_tax'], $order_acceptance['etc_tax'], $order_acceptance['shipping_charge'], $order_acceptance['etc'], $data['summary_month']);
//#7125:End


//#6946:Start

        if(!isset($order_acceptance['id'])){
             $data['order_acceptance_status'] = ORDER_ACCEPTANCE_UNAPPROVED;
        }
        else{
            $data['order_acceptance_status'] =$order_acceptance['order_acceptance_status'];
        }

//#6946:End
       // if (isset($order_acceptance['is_approved']) && $order_acceptance['is_approved'] && $order_acceptance['is_change_acceptance_status'] && $data['amount']) {
        if (isset($order_acceptance['is_approved']) && $order_acceptance['is_approved'] && $order_acceptance['is_change_acceptance_status']){

//#7278: Start
            $data['order_acceptance_status']     = ORDER_ACCEPTANCE_APPROVED;
            $data['acceptance_approval_date']    = date('Y-m-d');
            $data['acceptance_approval_account_id'] = $this->auth->get_account_id();
//#7278: End
        }

        $data['j_code']    = $order_acceptance['j_code'];
        $data['branch_cd'] = sprintf('%02d', $order_acceptance['branch_cd']);
        $data['order_id']  = $order_acceptance['order_id'];

//#8088:Start

        if(isset($order_acceptance['returnStatusReturn']) && ($order_acceptance['returnStatusReturn'] == 1)){
            $data['order_acceptance_status'] = ORDER_ACCEPTANCE_UNAPPROVED;
        }

//#8088:End

        if (isset($order_acceptance['is_difference_common']) && $order_acceptance['is_difference_common']) {
            $data['j_code']   = $data['branch_cd'] = '';
            $data['order_id'] = 0;
        }

        $data['lastup_account_id'] = $this->auth->get_account_id();
        $data['lastup_datetime'] = date('Y-m-d H:i:s');
        if (isset($order_acceptance['is_new']) && $order_acceptance['is_new']) {

            $data['create_datetime'] = date('Y-m-d H:i:s');
            return $data;

        }
        if(isset($order_acceptance['returnStatusReturn']) && ($order_acceptance['returnStatusReturn'] == 1)){
            $data['order_acceptance_status'] = ORDER_ACCEPTANCE_UNAPPROVED;
        }


        $data['id'] = $order_acceptance['id'];
        return $data;

    }

    public function update_order_acceptance2($order_acceptances,$disapprovel = false) {
        $delete_datas = array();
        $update_datas = array();
        $order_detail_datas = array();

        foreach ($order_acceptances as $order_acceptance) {
            if (empty($order_acceptance)) continue;
                if($disapprovel){
                    $order_acceptance['order_acceptance_status'] =   ORDER_ACCEPTANCE_UNAPPROVED;
                }
                else{
                        $order_acceptance['order_acceptance_status'] =   ORDER_ACCEPTANCE_APPROVED;
                }

                $seted_data = $order_acceptance;
                if (($seted_data['order_acceptance_status']  ==  ORDER_ACCEPTANCE_APPROVED) && ($seted_data['order_id'])) {

                    $order_detail_datas[] = array(
                            'order_id'            => $seted_data['order_id'],
                            'branch_cd'           => $seted_data['branch_cd'],
                    );
                }
                $update_datas[] = $seted_data;
        }
        $this->db->trans_begin();
        try {
            if (!empty($update_datas)) {

                if (!$this->db->update_batch('order_acceptance', $update_datas, 'id')) {
                     throw new Exception("update error");
                }
            }
        } catch(Exception $e) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_commit();
        return true;
    }

    public function update_order_acceptance($order_acceptances) {

        $delete_datas = array();
        $update_datas = array();
        $order_detail_datas = array();
//#7968:Start
        $revert_is_order_input = array();
//#7968:End
        foreach ($order_acceptances as $order_acceptance) {
            if (empty($order_acceptance)) continue;

            if (isset($order_acceptance['is_deleting']) && $order_acceptance['is_deleting']) {
                $delete_datas[] = array(
                    'id'              => $order_acceptance['id'],
                    'disable'         => 1,
                    'lastup_datetime' => date('Y-m-d H:i:s'),
                    'lastup_account_id' => $this->auth->get_account_id()
                );

                //Get data for reverting is_order_input incase deleting a order accceptance
                if ($order_acceptance['jcode_branch'] && ($revert_data = $this->_check_order_acceptance_to_revert_is_order_input($order_acceptance['jcode_branch']))) {
                    $revert_is_order_input[$order_acceptance['jcode_branch']] = $order_acceptance['jcode_branch'];
                }

            } else {

                $seted_data = $this->_set_data($order_acceptance);
                if (($seted_data['order_acceptance_status'] == ORDER_ACCEPTANCE_APPROVED) && ($seted_data['order_id'])) {

                    $order_detail_datas[] = array(
                        'order_id'            => $seted_data['order_id'],
                        'branch_cd'           => $seted_data['branch_cd'],
                    );
                }
                $update_datas[] = $this->_set_data($order_acceptance);
//#7968:Start
                //Get data for reverting is_order_input incase updating a order accceptance
                if ($order_acceptance['jcode_branch']) {
                    $revert_j_code      = substr($order_acceptance['jcode_branch'], 0, -2);
                    $revert_branch_cd   = substr($order_acceptance['jcode_branch'], -2);
                    if(isset($order_acceptance['j_code_has_oldVal'])){
                        $revert_j_code = $order_acceptance['j_code_has_oldVal'];
                    }
                    if(isset($order_acceptance['branch_cd_has_oldVal'])){
                        $revert_branch_cd = $order_acceptance['branch_cd_has_oldVal'];
                    }
                    $order_acceptance['jcode_branch'] =  $revert_j_code.$revert_branch_cd;


                    if ($seted_data['branch_cd'] == '') {
                        if ($revert_data = $this->_check_order_acceptance_to_revert_is_order_input($order_acceptance['jcode_branch'])) {
                            $revert_is_order_input[$order_acceptance['jcode_branch']]  = $order_acceptance['jcode_branch'];
                        }
                    } elseif (($seted_data['j_code'] != $revert_j_code) || ($revert_branch_cd != $seted_data['branch_cd'])) {
                        if ($revert_data = $this->_check_order_acceptance_to_revert_is_order_input($order_acceptance['jcode_branch'])) {
                            $revert_is_order_input[$order_acceptance['jcode_branch']]  = $order_acceptance['jcode_branch'];
                        }
                    }

                    if (( $revert_j_code) || ($revert_branch_cd )) {
                        if ($revert_data = $this->_check_order_acceptance_to_revert_is_order_input($order_acceptance['jcode_branch'])) {
                            $revert_is_order_input[$order_acceptance['jcode_branch']]  = $order_acceptance['jcode_branch'];
                        }
                    }


                }
//#7968:End
            }
        }

        try {

            $this->db->trans_begin();
            if (!empty($update_datas)) {
                if (!$this->db->update_batch('order_acceptance', $update_datas, 'id')) {
                    throw new Exception("update error");
                }
            }

            if (!empty($delete_datas)) {
                if (!$this->db->update_batch('order_acceptance', $delete_datas, 'id')) {
                    throw new Exception("update error");
                }
            }

            if (!empty($order_detail_datas)) {
                if (!$this->_update_is_acceptance_input($order_detail_datas)) {
                    throw new Exception("update error");
                }
            }
//#7968:Start
            if (!empty($revert_is_order_input)) {
                foreach ($revert_is_order_input as $key => $jcode_branch) {
                    if ($this->_check_order_acceptance_to_revert_is_order_input($key)) {
                        continue;
                    }
                    if (!$this->revert_is_order_input($jcode_branch)) {
                        throw new Exception("update error");
                    }
                }
            }
//#7968:Start
// print_r($this->db->last_query());
            $this->db->trans_commit();

        } catch(Exception $e) {
            $this->db->trans_rollback();
            return false;
        }

        return true;
    }

//#7968:Start
/**
 * Check order acceptance to update flag is_order_input
 * @param string $jcode_branch
 * @return array
 * @author van_don
 */
    public function _check_order_acceptance_to_revert_is_order_input($jcode_branch) {
        $revert_data = array();
        $revert_data = $this->get_order_acceptance_by_jcode_and_branch_cd($jcode_branch);
        return $revert_data;
    }
//#7968:End

    public function insert_order_acceptance($order_acceptances) {

        $this->db->trans_begin();

        try {
            foreach ($order_acceptances as $k => $order_acceptance) {

                if (empty($order_acceptance)) continue;

                $data = $this->_set_data($order_acceptance);

                if (empty($data['report_remarks'])) {
                    $data['report_remarks'] = "";
                }
                if (!$this->db->insert('order_acceptance', $data))
                    throw new Exception("Insert fail");

                if ($data['order_acceptance_status'] == ORDER_ACCEPTANCE_APPROVED && $data['order_id']) {
                    if ($this->db->update('order_detail', array('is_acceptance_input' => 1, 'lastup_datetime' => date('Y-m-d H:i:s')), 'order_id = '. $data['order_id'] . ' and branch_cd = ' . $data['branch_cd'])) {
                        throw new Exception("update fail");
                    }
                }

                $insert_data_id[$k] = $this->db->insert_id();
            }

            $this->db->trans_commit();

        } catch(Exception $e) {
            $this->db->trans_rollback();
            return array (
                'status' => false,
                'data_id' => array(),
            );
        }

        return array (
            'status' => true,
            'data_id' => $insert_data_id,
        );
    }

    public function cancel_order_acceptance($order_acceptances) {
        $datas = array();
        $order_detail_datas = array();

        foreach ($order_acceptances as $order) {
            if (isset($order['id'])) {
                $datas[] = array(
                    'id'                          => $order['id'],
                    'order_acceptance_status'     => ORDER_ACCEPTANCE_UNAPPROVED,
                    'acceptance_approval_date'    => '0000-00-00',
//#7278: Start
//                    'acceptance_approval_account' => '0',
                    'acceptance_approval_account_id' => '0',
//#7278: End
                    'lastup_datetime'             => date('Y-m-d H:i:s'),
                );
                if (isset($order['order_id']) && $order['order_id']) {
                    $order_detail_datas[] = array (
                        'order_id' => $order['order_id'],
                        'branch_cd' => $order['branch_cd'],
                    );
                }

            }
        }
        try {
            if (!empty($datas)) {
                if (!$this->db->update_batch('order_acceptance', $datas, 'id')) {
                    throw new Exception("cancel error");
                }
            }

            if (!empty($order_detail_datas)) {
                if (!$this->_update_is_acceptance_input($order_detail_datas, false)) {
                    throw new Exception("cancel error");
                }
            }

        } catch(Exception $e) {
            return false;
        }
        return true;
    }

    public function delete_order_acceptance($order_acceptance) {
        $update_data = array(
            'disable'         => 1,
            'lastup_datetime' => date('Y-m-d H:i:s'),
            'lastup_account_id' => $this->auth->get_account_id()
        );
        return $this->db->update('order_acceptance', $update_data, 'id = ' . $order_acceptance['id']);
    }

    private function calculate_total_amount($tax_free, $shipping_charge_tax, $stamp_tax, $etc_tax, $shipping_charge, $etc, $summary_month) {

        $isReturnNull = true;

        if((trim($tax_free) <> "") || (trim($shipping_charge_tax) <> "" ) || (trim($stamp_tax) <> "" ) || (trim($etc_tax)  <> "") || (trim($shipping_charge)  <> "") || (trim($etc) <> "")){
           $isReturnNull = false;
        }

        if($isReturnNull === true){  return "";}

        if(!$tax_free && !$shipping_charge_tax && !$stamp_tax && !$etc_tax && !$shipping_charge && !$etc) {
            return 0;
        }

//#7125:Start
//        $tax = $this->get_tax_rate();
        $tax = $this->get_tax_rate($summary_month);
//#7125:End

//        $shipping_charge_tax = f_ceil($shipping_charge_tax / (1 + ($tax['rate']/100)));
//        $stamp_tax           = f_ceil($stamp_tax / (1 + ($tax['rate']/100)));
//        $etc_tax             = f_ceil($etc_tax / (1 + ($tax['rate']/100)));
        $shipping_charge_tax = f_ceil(bcdiv($shipping_charge_tax, bcadd(1, bcdiv($tax['rate'], 100))));
        $stamp_tax           = f_ceil(bcdiv($stamp_tax, bcadd(1, bcdiv($tax['rate'], 100))));
        $etc_tax             = f_ceil(bcdiv($etc_tax, bcadd(1, bcdiv($tax['rate'], 100))));

        return $shipping_charge_tax + $stamp_tax + $etc_tax + $tax_free + $shipping_charge + $etc;
    }

//#7125:Start
//    private function get_tax_rate() {
//        if (!$this->_tax) {
//            $this->load->model('consumption_tax_model');
//            $this->_tax = $this->consumption_tax_model->get_rate();
//        }
//        return $this->_tax;
//    }
    private function get_tax_rate($summary_month) {
        if (!$this->_tax) {
            $this->load->model('consumption_tax_model');
            $this->_tax = $this->consumption_tax_model->get_consumption_taxs('all', array(
                'fields' => 'rate, adaptation_date',
                'joins'  => array(
                    'consumption_tax_adaptation' => 'consumption_tax_adaptation.id = consumption_tax.consumption_tax_adaptation_id AND consumption_tax_adaptation.disable = 0'
                ),
                'order' => 'adaptation_date DESC'
            ));
        }

        $summary_month = str_replace('/', '-', $summary_month);
        if (!preg_match('/^([0-9]{4}-(0[1-9]|1[012]))$/', $summary_month)) {
            return false;
        }

        $i = 0;
        while ($i < count($this->_tax) && $summary_month < $this->_tax[$i]['adaptation_date']) {
            $i++;
        }

        if ($i < count(($this->_tax))) {
            return $this->_tax[$i];
        } else {
            return false;
        }
    }
//#7125:End

    private function _update_is_acceptance_input($order_detail_datas, $is_approved = true) {
        $conditions = array();
        foreach ($order_detail_datas as $order_detail_data) {
            $conditions[] = '( order_id = ' . $order_detail_data['order_id'] . ' AND branch_cd = ' . $order_detail_data['branch_cd'] . ')';
        }
        $conditions = implode(' OR ', $conditions);

        $is_acceptance_input = $is_approved ? 1 : 0;
        return $result = $this->db->where($conditions)
                                ->update('order_detail', array('lastup_datetime' => date('Y-m-d H:i:s'),'is_acceptance_input' => $is_acceptance_input));
    }


    public function rules($rule_name, $conditions = array()) {
        $rules = array();
        foreach ($conditions as $k => $v) {
//#6940:Start
//            if ($v) $rules[] = $k;
            if ($v && $k != 'is_required_delivery_date') {
                $rules[] = $k;
            }

        }

        $is_required_delivery_date = '';
        if (isset($conditions['is_required_delivery_date']) && $conditions['is_required_delivery_date']) {
            $is_required_delivery_date = 'required|';
        }
//#6940:End
        $rules = implode('|', $rules);
        $rules = !empty($rules) ? $rules . '|' : '';

        $validate_rule['update'] = array (
                array(
                    'field'  => 'j_code',
                    'label'  => 'j_code',
                    'rules'  => $rules . 'trim|max_length[10]',
                    'errors' => array(
                                    'required' => lang('error_j_code_is_required'),
                                    'max_length' => lang('error_j_code_max_length'),
                                )
                ),
                array(
                    'field'  => 'branch_cd',
                    'label'  => 'branch_cd',
                    'rules'  => $rules . 'trim|max_length[2]',
                    'errors' => array(
                                    'required' => lang('error_branch_cd_is_required'),
                                    'max_length' => lang('error_branch_cd_max_length'),
                                )
                ),
                array(
                    'field'  => 'account_id',
                    'label'  => 'account_id',
                    'rules'  => 'required|trim',
                    'errors' => array(
                                    'required' => lang('error_account_id_is_required'),
                                )
                ),
                array(
                    'field'  => 'arrange_rep',
                    'label'  => 'arrange_rep',
                    'rules'  => 'trim',
                    'errors' => array(
                                    'required' => lang('error_arrange_rep_is_required'),
                                )
                ),
                array(
                    'field'  => 'client_name',
                    'label'  => 'client_name',
                    'rules'  => 'required|trim',
                    'errors' => array(
                                    'required' => lang('error_client_name_is_required'),
                                )
                ),
                array(
                    'field'  => 'source_subcontractor_id',
                    'label'  => 'source_subcontractor_id',
                    'rules'  => 'required|trim',
                    'errors' => array(
                                    'required' => lang('error_source_subcontractor_id_is_required'),
                                )
                ),
                array(
                    'field'  => 'work_subcontractor_id',
                    'label'  => 'work_subcontractor_id',
                    'rules'  => 'trim',
                    'errors' => array(
                                    'required' => lang('error_work_subcontractor_name_is_required'),
                                )
                ),
                array(
                    'field'  => 'shipping_subcontractor_id',
                    'label'  => 'shipping_subcontractor_id',
                    'rules'  => 'trim',
                    'errors' => array(
                                    'required' => lang('error_shipping_subcontractor_name_is_required'),
                                )
                ),
                array(
                    'field'  => 'set_name',
                    'label'  => 'set_name',
                    'rules'  => 'trim',
                    'errors' => array(
                                    'required' => lang('error_set_name_is_required'),
                                )
                ),
                array(
                    'field'  => 'unit_price',
                    'label'  => 'unit_price',
                    'rules'  => 'trim|numeric|max_length[9]',
                    'errors' => array(
                                    'required'     => lang('error_unit_price_is_required'),
                                    'numeric'      => lang('error_unit_price_must_be_number'),
                                    'max_length'    => lang('error_unit_price_max_length')
                                    // 'greater_than_equal_to' => lang('error_unit_price_can_not_be_negative'),
                                )
                ),
                array(
                    'field'  => 'quantity',
                    'label'  => 'quantity',
//#8258:Start
                    'rules'  => 'trim|numeric|greater_than_equal_to[0]|max_length[7]',
//#8258:End
                        'errors' => array(
                                    // 'required'     => lang('error_quantity_is_required'),
                                    'numeric'      => lang('error_quantity_must_be_number'),
                                    'greater_than_equal_to' => lang('error_quantity_can_not_be_negative'),
                                    'max_length' => lang('error_quantity_max_length')
                                )
                ),
                array(
                    'field'  => 'delivery_date_for_save',
                    'label'  => 'delivery_date_for_save',
//#6940:Start
//                    'rules'  => 'trim|valid_full_date',
                    'rules'  => $is_required_delivery_date . 'trim|valid_full_date',
//#6940:End
                    'errors' => array(
                                    'required' => lang('error_delivery_date_is_required'),
                                    'valid_full_date' => lang('error_delivery_date_invalid'),
                                )
                ),
                array(
                    'field'  => 'fee',
                    'label'  => 'fee',
                    'rules'  => 'trim|numeric|greater_than_equal_to[0]|max_length[9]',
                    'errors' => array(
                                    'required'     => lang('error_fee_is_required'),
                                    'numeric'      => lang('error_fee_must_be_number'),
                                    'greater_than_equal_to' => lang('error_fee_can_not_be_negative'),
                                    'max_length'    => lang('error_fee_max_length')
                                )
                ),
                array(
                    'field'  => 'weight',
                    'label'  => 'weight',
                    'rules'  => 'trim|numeric|greater_than_equal_to[0]|max_length[4]',
                    'errors' => array(
                                    'required'     => lang('error_weight_is_required'),
                                    'numeric'      => lang('error_weight_must_be_number'),
                                    'greater_than_equal_to' => lang('error_weight_can_not_be_negative'),
//#7090:Start
                                    'max_length' => lang('error_weight_max_length')
//#7090:End
                                )
                ),
                array(
                        'field'  => 'summary_month',
                        'label'  => 'summary_month',
                        'rules'  => 'trim|required',
                        'errors' => array(
                                'required' => lang('error_summary_month_insert_is_required'),
                        )
                ),
                array(
                    'field'  => 'shipments_shape_cd',
                    'label'  => 'shipments_shape_cd',
                    'rules'  => 'trim',
                    'errors' => array(
                                    'required' => lang('error_shipments_shape_cd_is_required'),
                                )
                ),
                array(
                    'field'  => 'tax_free',
                    'label'  => 'tax_free',
                    'rules'  => 'trim|numeric|max_length[9]',
                    'errors' => array(
                                    'numeric' => lang('error_tax_free_must_be_number'),
                                    'max_length' => lang('error_tax_free_max_length')
                                )
                ),
                array(
                    'field'  => 'shipping_charge_tax',
                    'label'  => 'shipping_charge_tax',
                    'rules'  => 'trim|numeric|max_length[9]',
                    'errors' => array(
                                    'numeric' => lang('error_shipping_charge_tax_must_be_number'),
                                    'max_length' => lang('error_shipping_charge_tax_max_length')
                                )
                ),
                array(
                    'field'  => 'stamp_tax',
                    'label'  => 'stamp_tax',
                    'rules'  => 'trim|numeric|max_length[9]',
                    'errors' => array(
                                    'numeric' => lang('error_shipping_stamp_tax_must_be_number'),
                                    'max_length' => lang('error_stamp_tax_max_length')
                                )
                ),
                array(
                    'field'  => 'etc_tax',
                    'label'  => 'etc_tax',
                    'rules'  => 'trim|numeric|max_length[9]',
                    'errors' => array(
                                    'numeric' => lang('error_etc_tax_must_be_number'),
                                    'max_length' => lang('error_etc_tax_max_length')
                                )
                ),
                array(
                    'field'  => 'shipping_charge',
                    'label'  => 'shipping_charge',
                    'rules'  => 'trim|numeric|max_length[9]',
                    'errors' => array(
                                    'numeric' => lang('error_shipping_charge_must_be_number'),
                                    'max_length' => lang('error_shipping_charge_max_length')
                                )
                ),
                array(
                    'field'  => 'etc',
                    'label'  => 'etc',
                    'rules'  => 'trim|numeric|max_length[9]',
                    'errors' => array(
                                    'numeric' => lang('error_etc_must_be_number'),
                                    'max_length' => lang('error_etc_max_length')
                                )
                ),
                array(
                    'field'  => 'amount',
                    'label'  => 'amount',
                    'rules'  => 'trim|numeric|max_length[9]',
                    'errors' => array(
                                    'numeric' => lang('error_etc_must_be_number'),
                                    'max_length' => lang('error_total_amount_max_length')
                                )
                ),

        );



        if (isset($validate_rule[$rule_name])) {
            return $validate_rule[$rule_name];
        }
        return array ();
    }

    /**
     * validate all input data when user insert/update on grid
     * @param  array $order_acceptance
     * @return array (empty if there is no error)
     */
    public function run_validate($order_acceptance,$template_finishish_report  = DM_FWR_TEMPLATE_CODE) {

//#8436:START
        if(isset($order_acceptance['delivery_date_for_save'])){

                $order_acceptance['delivery_date'] = $order_acceptance['delivery_date_for_save'];
        }
        else{
                $order_acceptance['delivery_date'] = $order_acceptance['delivery_date'];

        }
//#8436:END

        $this->load->model('closing_accountant_model');
        $this->load->library('form_validation');

        $is_required = false;
        if (isset($order_acceptance['is_difference_common']) && !$order_acceptance['is_difference_common']) {
            $is_required = true;
        } elseif (!isset($order_acceptance['is_difference_common'])) {
            $is_required = true;
        }
//#6940:Start

        if (!$this->_shipping_types) {
            $this->load->model('shipping_type_model');
            $this->_shipping_types = $this->shipping_type_model->list_all();
        }

        $is_required_delivery_date = false;
        foreach ($this->_shipping_types as $shipping_type) {
            if ($shipping_type['code'] == $order_acceptance['shipping_type_cd'] && $shipping_type['is_advance_payment'] == 1) {
                $is_required_delivery_date = true;
            }
        }

//        $this->form_validation->set_rules($this->rules('update', array('required' => $is_required)));

        if($template_finishish_report == DP_FWR_TEMPLATE_CODE){

            $condition_rules = array(
                    'required' => $is_required,
                    'is_required_delivery_date' => true,
            );
            $this->form_validation->set_rules($this->set_rules_mgt_DP('update', $condition_rules));
            //#6940:End
            $this->form_validation->set_data($order_acceptance);
            $error = array();
            if ($this->form_validation->run() === FALSE) {
                $error = array(
                        'j_code'                    => form_error('j_code'),
                        'branch_cd'                 => form_error('branch_cd'),
                        'account_id'                => form_error('account_id'),
                        'arrange_rep'               => form_error('arrange_rep'),
                        'client_name'               => form_error('client_name'),
                        'source_subcontractor_id'   => form_error('source_subcontractor_id'),
                        'work_subcontractor_id'     => form_error('work_subcontractor_id'),
                        'shipping_subcontractor_id' => form_error('shipping_subcontractor_id'),
                        'shipping_type_cd'          => form_error('shipping_type_cd'),
                        'amount'                    => form_error('amount'),
                        'delivery_date'             => form_error('delivery_date_for_save'),
                        'summary_month'             => form_error('summary_month')
                );
            }

        }
        else{
            $condition_rules = array(
                    'required' => $is_required,
                    'is_required_delivery_date' => $is_required_delivery_date,
            );
            $this->form_validation->set_rules($this->rules('update', $condition_rules));
            //#6940:End
            $this->form_validation->set_data($order_acceptance);

            $error = array();

            if ($this->form_validation->run() === FALSE) {
                $error = array(
                        'j_code'                    => form_error('j_code'),
                        'branch_cd'                 => form_error('branch_cd'),
                        'account_id'                => form_error('account_id'),
                        'arrange_rep'               => form_error('arrange_rep'),
                        'client_name'               => form_error('client_name'),
                        'source_subcontractor_id'   => form_error('source_subcontractor_id'),
                        'work_subcontractor_id'     => form_error('work_subcontractor_id'),
                        'shipping_subcontractor_id' => form_error('shipping_subcontractor_id'),
                        'shipping_type_cd'          => form_error('shipping_type_cd'),
                        'summary_month'             => form_error('summary_month'),
                        'set_name'                  => form_error('set_name'),
                        'unit_price'                => form_error('unit_price'),
                        'quantity'                  => form_error('quantity'),
                        'delivery_date'             => form_error('delivery_date_for_save'),
                        'fee'                       => form_error('fee'),
                        'weight'                    => form_error('weight'),
                        'shipments_shape_cd'        => form_error('shipments_shape_cd'),
                        'tax_free'                  => form_error('tax_free'),
                        'shipping_charge_tax'       => form_error('shipping_charge_tax'),
                        'stamp_tax'                 => form_error('stamp_tax'),
                        'etc_tax'                   => form_error('etc_tax'),
                        'shipping_charge'           => form_error('shipping_charge'),
                        'etc'                       => form_error('etc'),
                        'amount'                    => form_error('amount')
                );
            }

        }


        if ($is_required && isset($order_acceptance['j_code']) && isset($order_acceptance['branch_cd'])) {
            $j_code = trim($order_acceptance['j_code']);
            $branch_cd = trim($order_acceptance['branch_cd']);

            // #6901-Modify-S

            $order_details = $this->order_acceptance_model->get_order_detail($order_acceptance['j_code'], $order_acceptance['branch_cd']);

            if (!empty($j_code) && !empty($branch_cd) && !$order_details) {

                    $message = array('<span class="validation-error">' . lang('error_jcode_branch_is_not_exist') . '<span>');
                    $error = array_merge($error, $message);
            }
            else{

//#7126:Start
                if(!empty($j_code) && !empty($branch_cd) && $order_details){
//#9695:Start
//#9748:Start
//                    $account_business_unit = $this->account_model->get_account_business_unit($this->auth->get_account_id());
//                    if ($order_details->finish_work_report_template != $account_business_unit[0]['finish_work_report_template']) {
                    if ($order_details->finish_work_report_template != $template_finishish_report) {
//#9748:End
                        $message = array('<span class="validation-error">' . lang('error_account_business') . '<span>');
                        $error = array_merge($error, $message);
                    } else {
//#9695:End
                        $is_change_report = $order_details->is_change_report_apply;
                        if($is_change_report){
                            $message = array('<span class="validation-error">' . lang('error-change-report-order-detail') . '<span>');
                            $error = array_merge($error, $message);
                        }
//#9695:Start
                    }
//#9695:End
               }

//#7126:End
            }

            // #6901-Modify-E

        }
//#7158:Start
//        if (!empty($order_acceptance['summary_month'])) {
        if ($order_acceptance['summary_month'] != '') {
            $summary_month = substr($order_acceptance['summary_month'], 0, 7);
            $delimiter     = strpos($summary_month, '/') ? '/' : '-';
            $summary_month = $summary_month . $delimiter . '01';
//           $summary_month = date('Y-m', strtotime($summary_month));
//#7183:Start
            $summary_month = parse_date($summary_month);
//#7183:End

            if (!$this->_max_closing_date) {
                $this->_max_closing_date = $this->closing_accountant_model->get_closest_closing_date();
                $this->_max_closing_date = $this->_max_closing_date[0]['closest_closing_date'];
            }

            $tmp_max_closing_date = $this->_max_closing_date;
            $tmp_max_closing_date = date('Y-m-d', strtotime("$tmp_max_closing_date first day of this month"));
//            if ($this->closing_accountant_model->check_has_closed_or_not($summary_month)) {
            if (strtotime($summary_month) <= strtotime($tmp_max_closing_date)) {
                $message = array('<span class="validation-error">' . lang('error_summary_month_has_not_close') . '<span>');
                $error = array_merge($error, $message);
            }
//#7158:End
        }

        $this->form_validation->reset_validation();
        return $error;
    }

    /**
     * check all input data from the search form
     * @param  array $form_data
     * @return array - empty if all data is valid
     */
    public function run_search_condition_validate($form_data) {

        $error_messages = array();
        if (!empty($form_data['from_delivery_date'])) {
            if (!parse_date($form_data['from_delivery_date'], true)) {
                $error_messages['delivery_date'] = lang('error_delivery_date_form_is_invalue');
            }
            //$form_data['from_delivery_date'] = parse_date($form_data['from_delivery_date']);
        }
        if (!empty($form_data['to_delivery_date'])) {
            if (!parse_date($form_data['to_delivery_date'], true)) {
                $error_messages['delivery_date'] =lang('error_delivery_date_invalid');
            }
            //$form_data['to_delivery_date'] = parse_date($form_data['to_delivery_date']);
        }
       // echo print_r($form_data['from_delivery_date']);
        if (trim($form_data['to_delivery_date']) != '' && trim($form_data['from_delivery_date']) == '') {
            $error_messages['delivery_date'] =  lang('error_delivery_date_form_is_required');
        }
        if(empty($form_data['from_delivery_date']) && !empty($form_data['to_delivery_date'])){

            $error_messages['delivery_date'] = lang('error_delivery_date_form_is_required');
        }
        else{
            if(!empty($form_data['from_delivery_date']) && !empty($form_data['to_delivery_date'])){
                        $form_data['from_delivery_date'] = parse_date($form_data['from_delivery_date'],true);
                        $form_data['to_delivery_date'] = parse_date($form_data['to_delivery_date'],true);
                        if (count(explode('-', $form_data['from_delivery_date'])) == 2) {
                            $date_form_delivery =  $form_data['from_delivery_date'].'-01';
                        }
                        else{
                            $date_form_delivery =  $form_data['from_delivery_date'];
                        }


                        if (count(explode('-', $form_data['to_delivery_date'])) == 2) {
                            $listdateto = str_replace("-", "/", $form_data['to_delivery_date']);
                            $listdatetoarra = explode("/",$listdateto);
                            $daydef = cal_days_in_month(CAL_GREGORIAN, $listdatetoarra[1], $listdatetoarra[0]);
                            $date_to_delivery  = $form_data['to_delivery_date'] . '-'.$daydef;
                        }
                        else{
                            $date_to_delivery  = $form_data['to_delivery_date'];
                        }
                        if (strtotime($date_to_delivery) < strtotime($date_form_delivery)) {
                            $error_messages['delivery_date'] = lang('error_delivery_date_search_area');
                        }
            }
        }


       // $error_messages['delivery_date'] ="testaaaa";

        if (!empty($form_data['from_summary_month'])) {
            if (!parse_date($form_data['from_summary_month'],true)) {
             //#6849 - Modify -S

                $error_messages['summary_month'] = lang('error_summary_month_has_not_close');

              //#6849 - Modify -S
            }

        }

        if (!empty($form_data['to_summary_month'])) {
            if (!parse_date($form_data['to_summary_month'],true)) {
                $error_messages['summary_month'] = lang('error_summary_month_has_not_close');
            }

        }

        if(empty($form_data['from_summary_month']) && !empty($form_data['to_summary_month'])){
            $error_messages['summary_month'] =     lang('error_summary_month_form_is_required');
        }

        $form_data['from_summary_month'] = parse_date($form_data['from_summary_month'], true);
        $form_data['to_summary_month'] = parse_date($form_data['to_summary_month'], true);

//#7156:Start
        if (count(explode('-', $form_data['from_summary_month'])) > 2) {
            $error_messages['summary_month'] = lang('error_summary_month_has_not_close');
        }

        if (count(explode('-', $form_data['to_summary_month'])) > 2) {
            $error_messages['summary_month'] = lang('error_summary_month_has_not_close');
        }
//#7156:End

        if(!empty($form_data['from_summary_month']) && !empty($form_data['to_summary_month'])){
            if (trim($form_data['to_summary_month']) != '' && trim($form_data['from_summary_month']) == '') {
                $error_messages['summary_month'] = lang('error_summary_month_search_area');
            }

            if (count(explode('-', $form_data['from_summary_month'])) == 2) {
                $date_form_delivery =  $form_data['from_summary_month'].'-01';
            }
            else{
                $date_form_delivery =  $form_data['from_summary_month'];
            }


            if (count(explode('-', $form_data['to_summary_month'])) == 2) {
                $listdateto = str_replace("-", "/", $form_data['to_summary_month']);
                $listdatetoarra = explode("/",$listdateto);
                $daydef = cal_days_in_month(CAL_GREGORIAN, $listdatetoarra[1], $listdatetoarra[0]);
                $date_to_delivery  = $form_data['to_summary_month'] . '-'.$daydef;
            }
            else{
                $date_to_delivery  = $form_data['to_summary_month'];
            }
            if (strtotime($date_to_delivery) < strtotime($date_form_delivery)) {
                $error_messages['summary_month'] = lang('error_delivery_date_search_area');
            }


//#7067:Start
//            if (trim($form_data['to_jcode_branch']) != '' && trim($form_data['from_jcode_branch']) == '') {
//                $error_messages['jcode_branch'] = lang('error_j_code_branch_cd_search_area');
//             }
//#7067:End


        }
//#7067:Start
            if (trim($form_data['to_jcode_branch']) != '' && trim($form_data['from_jcode_branch']) == '') {
                $error_messages['jcode_branch'] = lang('error_j_code_branch_cd_search_area');
            }
            else{
                if ((strpos(trim($form_data['from_jcode_branch']), '-') !== FALSE) && (strlen(trim($form_data['from_jcode_branch'])) - strpos(trim($form_data['from_jcode_branch']), '-') > 3)) {
                    $error_messages['jcode_branch'] = str_replace('%field%', 'From', lang('common_msg_search_range_invalid'));
                }
                            if (trim($form_data['to_jcode_branch']) != '' && trim($form_data['from_jcode_branch']) != '') {

                                $standadize_jcode_range = $this->standadize_jcode_range($form_data['to_jcode_branch'], 9);
                                if (!is_null($standadize_jcode_range))
                                                        $to_jcode_branch = $standadize_jcode_range;
                                else  					$to_jcode_branch = $form_data['to_jcode_branch'];

                                $standadize_jcode_range = $this->standadize_jcode_range($form_data['from_jcode_branch'], 0);
                                if (!is_null($standadize_jcode_range))
                                        $form_jcode_branch = $standadize_jcode_range;
                                else    $form_jcode_branch = $form_data['from_jcode_branch'];

                                if (strcmp(trim($form_jcode_branch), trim($to_jcode_branch)) > 0) {

                                    $error_messages['jcode_branch'] = lang("error_j_code_branch_cd_2");
                                }

                            }
            }

//#7067:End
        return $error_messages;
    }

//#7067:Start
    public function standadize_jcode_range($jcode_branch_cd, $fill) {

        $return_value = null;
        $limit_length = 8;
        $pos = strpos($jcode_branch_cd, "-");

        if ($pos) {
            $pos_portion = explode("-", $jcode_branch_cd);
            $jcode = $pos_portion[0];
            $branch_cd = $pos_portion[1];
            if (strlen($jcode) <= $limit_length) {
                $filled_string = $jcode . str_repeat($fill, $limit_length - strlen($jcode)) . "-" .  $branch_cd;
                $return_value = $filled_string;
            }

        } else {
            $jcode = $jcode_branch_cd;
            if (strlen($jcode) <= $limit_length) {
                $filled_string = $jcode . str_repeat($fill, $limit_length - strlen($jcode)) . "-" .  str_repeat($fill, 2);
                $return_value = $filled_string;
            }
        }

        return $return_value;

    }
//#7067:End

    /**
     * Get all subcontractors.
     *
     * @param null
     * @return mixed null|array
     * @author hoang_minh
     * @since 2015-06-02
     */
    public function get_subcontractors() {
        //get columns
        $columns = array(
                'id',
                'j_code',
                'branch_cd',
                'client_name',
                'report_is_complete',
                'report_division_name',
                'report_charge_name',
                'report_fax_no',
                'delivery_date',
                'report_contents',
                'report_remarks',
        );

        return $this->get_items('order_acceptance', null, $columns);
    }

    /**
     * Get order acceptance by conditions
     *
     * @param array $params
     * @return mixed bool|array
     * @author hoang_minh
     * @since 2015-06-02
     */
    public function get_order_acceptance_by_conditions($params = array()) {
        if (empty($params)) {
            return $this->get_subcontractors();
        }

        //init variables
        $conditions = array();
        $orders = array();
        $table = '';
        $sub_condition = '';
        $bu_name = "";
        if (isset($params['conditions']['bu_name'])) {
        	$bu_name = $params['conditions']['bu_name'];
        }
//7185:Start
        $require_bu_id = false;
        $require_bu_id_condition = "";
//7185:End
        //make search conditions
        if ($bu_name != "DP")
        	$client_name_source = "order.client_name";
        else
        	$client_name_source = "order_acceptance.client_name";

        if (isset($params['conditions'])) {
            foreach ($params['conditions'] as $key => $val) {
                switch ($key) {
                    case 'id':
                        $conditions[] = "order_acceptance.id = '" . mysql_escape_string($val) . "'";
                        break;

                    case 'ids':
                        $conditions[] = "order_acceptance.id IN(" . mysql_escape_string($val) . ")";
                        break;

                    case 'j_code_branch_code_from':
                        $conditions[] = "CONCAT(order_acceptance.j_code, branch_cd) >= '" . str_pad($val, 9, "0", STR_PAD_RIGHT) . "' AND order_acceptance.j_code REGEXP '^[0-9]+$'";
                        break;

                    case 'j_code_branch_code_to':
                        $conditions[] = "CONCAT(order_acceptance.j_code, branch_cd) <= '" .str_pad($val, 9, "9", STR_PAD_RIGHT) . "'";
                        break;

                    case 'j_code_branch_cd':
                        $data['from_jcode_branch'] = $val['from'];
                        $data['to_jcode_branch'] = $val['to'];
//#7067:Start

                        $data2['from_jcode_branch'] = $data['from_jcode_branch'] ;

                        $data2['to_jcode_branch'] = $data['to_jcode_branch'];

                        $wheresql_jocde_branch = $this->get_where_sql_by_branchcode_and_jcode($data2,'order_acceptance.j_code','order_acceptance.branch_cd');
                        if(!empty($wheresql_jocde_branch)){
                             $conditions[] = $wheresql_jocde_branch;
                        }
// //                         if ((isset($data['from_jcode_branch']) ) && (trim($data['to_jcode_branch']) == "") && (trim($data['from_jcode_branch']) != "")) {

// //                             $formbrandcode = $data['from_jcode_branch'];

// //                             $n_form_brandcode = strlen($formbrandcode);

// //                             if ($n_form_brandcode <= 8) {
// //                                 $conditions[] = "order_acceptance.j_code LIKE '" . trim($formbrandcode . "%'");
// //                             } else {
// //                                 $varjcode = substr($formbrandcode, 0, 8);
// //                                 $varbranchcode = str_replace($varjcode, "", $formbrandcode);

// //                                 $conditions[] = "order_acceptance.j_code LIKE '" . trim($varjcode . "%'");
// //                                 $conditions[] = "order_acceptance.branch_cd LIKE '" . trim($varbranchcode . "%'");
// //                             }
// //                         } elseif (isset($data['to_jcode_branch']) && (trim($data['from_jcode_branch']) == "") && (trim($data['to_jcode_branch']) != "")) {
// //                             # set error
// //                         } else {
// //                             if (isset($data['from_jcode_branch'])) {
// //                                 $formbrandcode = $data['from_jcode_branch'];
// //                             } else {
// //                                 $formbrandcode = "";
// //                             }
// //                             $n_form_brandcode = strlen($formbrandcode);


// //                             if (isset($data['to_jcode_branch'])) {
// //                                 $to_jcode_branch = $data['to_jcode_branch'];
// //                             } else {
// //                                 $to_jcode_branch = "";
// //                             }

// //                             $n_tohjodebranch = strlen($to_jcode_branch);

// //                             if ($n_form_brandcode > 0) {
// //                                 if ($n_form_brandcode <= 8) {
// //                                     $conditions[] = 'if (CONCAT(order_acceptance.j_code) <> "", CONCAT(order_acceptance.j_code), "00000000") >= "' . str_pad($formbrandcode, 8, "0", STR_PAD_RIGHT) . '"';
// //                                 } else {
// //                                     $varjcode = substr($formbrandcode, 0, 8);
// //                                     $varbranchcode = str_replace($varjcode, "", $formbrandcode);

// //                                     $conditions[] = 'if (CONCAT(order_acceptance.j_code) <> "", CONCAT(order_acceptance.j_code), "00000000") >= ' . str_pad($varjcode, 8, "0", STR_PAD_RIGHT);
// //                                     $conditions[] = 'if (CONCAT(order_acceptance.branch_cd) <> "", CONCAT(order_acceptance.branch_cd), "00") >= ' . str_pad($varbranchcode, 2, "0", STR_PAD_RIGHT);
// //                                 }
// //                             }

// //                             if ($n_tohjodebranch) {
// //                                 if ($n_tohjodebranch <= 8) {

// //                                     for ($k = $n_tohjodebranch; $k <= 8; $k++) {
// //                                         $to_jcode_branch.= "9";
// //                                     }

// //                                     $conditions[] = 'if (CONCAT(order_acceptance.j_code) <> "", CONCAT(order_acceptance.j_code), "00000000") <= "' . str_pad($to_jcode_branch, 8, "0", STR_PAD_RIGHT) .'"';
// //                                 } else {
// //                                     $varjcode = substr($to_jcode_branch, 0, 8);
// //                                     $varbranchcode = str_replace($varjcode, "", $to_jcode_branch);

// //                                     $conditions[] = 'if (CONCAT(order_acceptance.j_code) <> "", CONCAT(order_acceptance.j_code), "00000000") <="' . str_pad($varjcode, 8, "0", STR_PAD_RIGHT) .'"';
// //                                     $conditions[] = 'if (CONCAT(order_acceptance.branch_cd) <> "", CONCAT(order_acceptance.branch_cd), "00") <= "' . str_pad($varbranchcode, 2, "0", STR_PAD_RIGHT) .'"';
// //                                 }
// //                             }
//                         }
//#7067:End
                        break;

                    case 's_code':
                        $conditions[] = "s_code LIKE '" . mysql_escape_string($val) . "%'";
                        break;

                    case 'client_name':
                    	if ($bu_name != "DP")
                    		$conditions[] = "order.client_name  LIKE '%" . mysql_escape_string($val) . "%'";
                    	 else
                    		$conditions[] = "order_acceptance.client_name  LIKE '%" . mysql_escape_string($val) . "%'";
                        break;

                    case 'report_is_complete':
                        if ($val != 0) {
                            $conditions[] = $key . " ='" . mysql_escape_string($val) . "'";
                        }
                        break;

                    case 'arrange_rep':
                        if ($val != 0) {
                            $conditions[] = $key . " ='" . mysql_escape_string($val) . "'";
                        }
                        break;

                    case 'is_reporting':
                        $sub_condition = 'AND shipping_type.is_reporting = 1 ';
                        break;

                    case 'source_subcontractor_id':
                    case 'work_subcontractor_id':
                    case 'shipping_subcontractor_id':
                        $conditions[] = $key . " ='" . mysql_escape_string($val) . "'";
                        break;

                    default:
                        break;
                }
            }

            if (!empty($params['conditions']['delivery_date_from'])) {
                if (!empty($params['conditions']['delivery_date_to'])) {
                    $conditions[] = "delivery_date >= '" . $params['conditions']['delivery_date_from'] . "'";
                    $conditions[] = "delivery_date <= '" . $params['conditions']['delivery_date_to'] . "'";
                } else {
                    $conditions[] = "delivery_date = '" . $params['conditions']['delivery_date_from'] . "'";
                }
            }
//7185:Start
            if (isset($params['conditions']['require_bu_id']) && $params['conditions']['require_bu_id'] == true) {
                $require_bu_id = true;
            }
//7185:End
        }
//9876:Start
        if ($bu_name == "DP") {
        	$conditions[] = 'order_acceptance.order_acceptance_status = ' . ORDER_ACCEPTANCE_APPROVED;
        } else { //DM or FROM Sub Contractor
        	$conditions[] = 'EXISTS (SELECT * '
            	          .         'FROM shipping_type '
                	      .         'WHERE shipping_type.code = order_acceptance.shipping_type_cd '
                    	  .          $sub_condition
                      	  .         'AND shipping_type.disable =' . STATUS_ENABLE . ')';
        }
//9876:End
        $conditions[] = 'order_acceptance.disable = ' . STATUS_ENABLE;
        $conditions[] = '`order`.disable = ' . STATUS_ENABLE;
        $conditions[] = '`client`.disable = ' . STATUS_ENABLE;
        $conditions[] = 'account.disable = ' . STATUS_ENABLE;
//7185:Start
        if ($require_bu_id == true) {
            $this->load->model('order_model');
            $this->load->library('auth');
            $logged_id = $this->auth->get_account_id();
            $logged_user_bu = $this->order_model->get_business_unit_of_logged_user($logged_id);
            $conditions[] = "department.business_unit_id = '" . mysql_escape_string($logged_user_bu['id']) . "' ";
            $require_bu_id_condition = "  JOIN `department` ON account.department_id = department.id ";
        }
//7185:End
        //make order conditions
        if (isset($params['orders'])) {
            foreach ($params['orders'] as $key => $val) {
                $orders[] = $key . " " . $val;
            }
        }

        //get columns
        $columns = array(
                'order_acceptance.id AS id',
                'order_acceptance.j_code AS j_code',
                'client_id',
                'branch_cd',
                $client_name_source,
                'report_is_complete',
                'order_acceptance.order_id AS root_order_id',
#7301:Start
                /*
                '`order`.division_name AS report_division_name',
                '`order`.charge_name AS report_charge_name',
                '`order`.fax_no AS report_fax_no',
                */
                'IF(TRIM(`order_acceptance`.report_division_name) = "", `order`.division_name, `order_acceptance`.report_division_name) AS report_division_name',
                'IF(TRIM(`order_acceptance`.report_charge_name) = "", `order`.charge_name, `order_acceptance`.report_charge_name) AS report_charge_name',
                'IF(TRIM(`order_acceptance`.report_fax_no) = "", `order`.fax_no, `order_acceptance`.report_fax_no) AS report_fax_no',
#7301:End
                'delivery_date',
#7301:Start
                //'`order_acceptance`.set_name AS report_contents',
                'IF(TRIM(`order_acceptance`.report_contents) = "",`order_acceptance`.set_name, `order_acceptance`.report_contents) AS report_contents',
#7301:End
                'report_remarks',
                'client.s_code AS s_code',
                'order_acceptance.order_id'
//7910:Start
                ,'order_acceptance.quantity'
//7910:End
//7989:Start
                ,'IF(`client`.corporate_status_before <> 0, "B", IF(`client`.corporate_status_after <> 0, "A", "N")) AS merchandise_name_pos'
                ,'`client`.business_category'
                ,'`client`.corporate_status_before'
                ,'`client`.corporate_status_after'
//7989:End
        );

        $table = ' order_acceptance JOIN `order` ON order_acceptance.order_id = `order`.id '
               . 'JOIN `client` ON `order`.client_id = `client`.id '
               . 'JOIN `account` ON `order_acceptance`.account_id = account.id';
//7185:Start
        $table .= $require_bu_id_condition;
//7185:End

        $orders = array(
                'delivery_date ASC',
                'order_acceptance.j_code ASC',
                'branch_cd ASC',
        );

       return $this->get_items_by_parameters($table, $columns, $conditions, $orders);

    }

    /**
     * Update order acceptance
     *
     * @param array $params
     * @return bool
     * @author hoang_minh
     * @since 2015-06-04
     */
    public function update_order_acceptance_by_params($params = array()) {
        if ( ! is_array($params) || empty($params)) {
            return false;
        }

        return $this->update('order_acceptance', $params['update_data'], $params['update_conditions']);
    }

    /**
     * Get orders for Delivery Statement
     * @param array $conditions
     * @return array
     * @author cong_tien
     */
    public function get_order_for_download($conditions = array()) {
        $this->db->select(
//#7631:Start
//                            '`order`.order_regist_date,'
                            '`order`.order_approval_date,' .
//#7631:End
                            'business_unit.name AS business_unit_name,
                            account.name AS account_name,
                            `order`.j_code, order_detail.branch_cd,
                            order_acceptance.id AS order_acceptance_id, `order`.id AS order_id,
                            department.name AS department_name, '
//#7117:Start
                            /*
                            SUM(order_acceptance.`tax_free`) AS tax_free,
                            SUM(order_acceptance.shipping_charge_tax) AS shipping_charge_tax,
                            SUM(order_acceptance.stamp_tax) AS stamp_tax,
                            SUM(order_acceptance.etc_tax) AS etc_tax,
                            SUM(order_acceptance.shipping_charge) AS shipping_charge,
                            SUM(order_acceptance.etc) AS etc, SUM(order_acceptance.amount) AS amount_sum,
                            */
                            ."
                            IF(COUNT(NULLIF(order_acceptance.`tax_free`, '')) > 0, SUM(order_acceptance.`tax_free`), NULL) AS tax_free,
                            IF(COUNT(NULLIF(order_acceptance.`shipping_charge_tax`, '')) > 0, SUM(order_acceptance.`shipping_charge_tax`), NULL) AS shipping_charge_tax,
                            IF(COUNT(NULLIF(order_acceptance.`stamp_tax`, '')) > 0, SUM(order_acceptance.`stamp_tax`), NULL) AS stamp_tax,
                            IF(COUNT(NULLIF(order_acceptance.`etc_tax`, '')) > 0, SUM(order_acceptance.`etc_tax`), NULL) AS etc_tax,
                            IF(COUNT(NULLIF(order_acceptance.`shipping_charge`, '')) > 0, SUM(order_acceptance.`shipping_charge`), NULL) AS shipping_charge,
                            IF(COUNT(NULLIF(order_acceptance.`etc`, '')) > 0, SUM(order_acceptance.`etc`), NULL) AS etc,
                            IF(COUNT(NULLIF(order_acceptance.`amount`, '')) > 0, SUM(order_acceptance.`amount`), NULL) AS amount_sum, "
//#7117:End
                            .'
                            `order`.division_name, `order`.charge_name,
                            `client`.id AS client_id, `client`.s_code, `client`.name AS client_name,
                            order_detail.id AS order_detail_id, order_detail.`contents`, order_detail.delivery_date, order_detail.billing_date, order_detail.payment_date, order_detail.quantity, order_detail.tax_type AS order_detail_tax_type,
                            order_detail.unit_price, order_detail.cost_unit_price, order_detail.cost_total_price, order_detail.gross_profit_amount, order_detail.gross_profit_rate, order_detail.amount,
                            product.name AS product_name, product.id AS product_id,
                            (
                                SELECT SUM(CASE
                                    WHEN sales_slip_detail.tax_type = ' . TAX_INCLUDED . ' THEN CEIL(
                                        sales_slip_detail.amount / (1 + ((
                                            SELECT consumption_tax.rate
                                            FROM consumption_tax_adaptation'
//#7117:Start
                                            // JOIN consumption_tax ON (consumption_tax.consumption_tax_adaptation_id AND consumption_tax_adaptation.id AND consumption_tax.`disable` = 0)
                                            . ' JOIN consumption_tax ON (consumption_tax.consumption_tax_adaptation_id = consumption_tax_adaptation.id AND consumption_tax.`disable` = 0)'
//#7117:End
                                            . ' WHERE sales_slip_detail.delivery_date >= consumption_tax_adaptation.adaptation_date AND consumption_tax_adaptation.`disable` = 0
                                            ORDER BY consumption_tax_adaptation.adaptation_date DESC
                                            LIMIT 1
                                        ) / 100))
                                    )
                                    ELSE sales_slip_detail.amount
                                    END
                                )
                                FROM sales_slip_detail'
//#7117:Start
                                // JOIN sales_slip ON (sales_slip.id = sales_slip_detail.sales_slip_id AND sales_slip.`disable` = 0 AND sales_slip.sales_slip_status IN (21, 22, 23))
                                . ' JOIN sales_slip ON (sales_slip.id = sales_slip_detail.sales_slip_id AND sales_slip.`disable` = 0 AND sales_slip.sales_slip_status IN (' . SALES_STATUS_INVOICE_APPROVAL_PENDING . ', ' . SALES_STATUS_INVOICE_APPROVED . ', ' . SALES_STATUS_INVOICE_ISSUED . '))'
//#7117:End
                                . ' WHERE sales_slip_detail.j_code = `order`.j_code
                                    AND sales_slip_detail.branch_cd = order_detail.branch_cd
                                    AND sales_slip_detail.is_ad_receive_payment = 1
                                    AND sales_slip_detail.ad_receive_sales_slip_detail_id <> 0
                                    AND sales_slip_detail.`disable` = 0
                            ) AS slip_amount_total_excluding_tax,
                            (
                                SELECT SUM(CASE
                                    WHEN sales_slip_detail.tax_type = ' . TAX_INCLUDED . ' THEN CEIL(
                                        sales_slip_detail.amount / (1 + ((
                                            SELECT consumption_tax.rate
                                            FROM consumption_tax_adaptation
                                            JOIN consumption_tax ON (consumption_tax.consumption_tax_adaptation_id = consumption_tax_adaptation.id AND consumption_tax.`disable` = 0) '
//#7117:Start
                                            // WHERE order_detail.delivery_date >= consumption_tax_adaptation.adaptation_date AND consumption_tax_adaptation.`disable` = 0
                                            . ' WHERE sales_slip_detail.delivery_date >= consumption_tax_adaptation.adaptation_date AND consumption_tax_adaptation.`disable` = 0'
//#7117:End
                                            . ' ORDER BY consumption_tax_adaptation.adaptation_date DESC
                                            LIMIT 1
                                        ) / 100))
                                    )
                                    ELSE sales_slip_detail.amount
                                    END
                                )
                                FROM sales_slip_detail'
//#7117:Start
                                // JOIN sales_slip ON (sales_slip.id = sales_slip_detail.sales_slip_id = sales_slip.`disable` = 0 AND sales_slip.sales_slip_status IN (21, 22, 23))
//#8334:Start
//                              . ' JOIN sales_slip ON (sales_slip.id = sales_slip_detail.sales_slip_id = sales_slip.`disable` = 0 AND sales_slip.sales_slip_status IN (' . SALES_STATUS_INVOICE_APPROVAL_PENDING . ', ' . SALES_STATUS_INVOICE_APPROVED . ', ' . SALES_STATUS_INVOICE_ISSUED . '))'
                                . ' JOIN sales_slip ON (sales_slip.id = sales_slip_detail.sales_slip_id AND sales_slip.`disable` = 0 AND sales_slip.sales_slip_status IN (' . SALES_STATUS_INVOICE_APPROVAL_PENDING . ', ' . SALES_STATUS_INVOICE_APPROVED . ', ' . SALES_STATUS_INVOICE_ISSUED . '))'
//#8334:End
//#7117:End
                                . 'WHERE sales_slip_detail.j_code = `order`.j_code
                                    AND sales_slip_detail.branch_cd = order_detail.branch_cd
                                    AND sales_slip_detail.is_ad_receive_payment = 0
                                    AND sales_slip_detail.`disable` = 0
                            ) AS slip_amount_total_confirmed_invoice,
                            (
                                SELECT consumption_tax.rate
                                FROM consumption_tax_adaptation
                                JOIN consumption_tax ON (consumption_tax.consumption_tax_adaptation_id = consumption_tax_adaptation.id AND consumption_tax.`disable` = 0)
                                WHERE order_detail.delivery_date >= consumption_tax_adaptation.adaptation_date AND consumption_tax_adaptation.`disable` = 0
                                ORDER BY consumption_tax_adaptation.adaptation_date DESC
                                LIMIT 1
                            ) AS consumption_tax_rate', false)
                ->from('order')
                ->join('order_detail', '(order_detail.order_id = `order`.id AND order_detail.`disable` = 0 AND order_detail.order_status IN (' . ORDER_STATUS_UNDETERMINED . ', ' . ORDER_STATUS_CONFIRMED . '))', '', false)
                ->join('product', '(product.id = order_detail.product_id AND product.`disable` = 0)', '', false)
                ->join('`client`', '(`client`.id = `order`.client_id AND `client`.`disable` = 0)', '', false)
                ->join('order_acceptance', '(order_acceptance.j_code = `order`.j_code AND order_acceptance.branch_cd = order_detail.branch_cd AND order_acceptance.`disable` = 0)', 'left', false)
                ->join('account', '(account.id = `order`.j_account_id AND account.`disable` = 0)', '', false)
                ->join('department', '(department.id = account.department_id AND department.`disable` = 0)', '', false)
                ->join('business_unit', '(business_unit.id = department.business_unit_id AND business_unit.`disable` = 0)', '', false)
                ->where('order.disable', '0')
                ->group_by('order.j_code, order_detail.branch_cd')
//#7631:Start
//                ->order_by('order.order_regist_date, business_unit.name, account.name, order.j_code, order_detail.branch_cd');
                ->order_by('order.order_approval_date, business_unit.name, account.name, order.j_code, order_detail.branch_cd');
//#7631:End

        if (!empty($conditions['from'])) {
            $this->db->where('DATE_FORMAT(order_detail.delivery_date, "%Y-%m") >', $conditions['from']);
        }

        if (!empty($conditions['to'])) {
            $this->db->where('DATE_FORMAT(order_detail.delivery_date, "%Y-%m") <', $conditions['to']);
        }

        return $this->db->get()->result_array();
    }

    /**
     * Get remain order_acceptance
     *
     * @param array $params
     * @return mixed bool | array
     * @author hoang_minh
     * @since 2015/08/06
     */
    public function get_remain_order_acceptance($params) {
        if (empty($params) || ! isset($params['delivery_date_from']) || empty($params['delivery_date_from'])
                || ! isset($params['delivery_date_to']) || empty($params['delivery_date_to'])) {
                    return false;
                }

                $sql = "SELECT "
                     .   "client_name, "
                     .   "quantity, "
                     .   "delivery_date, "
                     .   "j_code, "
                     .   "branch_cd, "
                     .   "source_subcontractor_id, "
                     .   "work_subcontractor_id, "
                     .   "shipping_subcontractor_id, "
                     .   "(SELECT "
                     .     "subcontractor.name "
                     .     "FROM subcontractor "
                     .     "WHERE subcontractor.id = order_acceptance.source_subcontractor_id AND subcontractor.disable = " . STATUS_ENABLE
                     .   ") AS source_subcontractor_name, "
                     .   "(SELECT "
                     .     "subcontractor.abbr_name "
                     .     "FROM subcontractor "
                     .     "WHERE subcontractor.id = order_acceptance.work_subcontractor_id AND subcontractor.disable = " . STATUS_ENABLE
                     .   ") AS work_subcontractor_name, "
                     .   "(SELECT "
                     .     "subcontractor.abbr_name "
                     .     "FROM subcontractor "
                     .     "WHERE subcontractor.id = order_acceptance.shipping_subcontractor_id AND subcontractor.disable = " . STATUS_ENABLE
                     .   ") AS shipping_subcontractor_name, "
                     .   "(SELECT "
                     .     "account.name "
                     .     "FROM account "
                     .     "WHERE account.id = order_acceptance.account_id AND account.disable = " . STATUS_ENABLE
                     .   ") AS j_account_name, "
                     .   "(SELECT "
                     .     "account.name "
                     .     "FROM account "
                     .     "WHERE account.id = order_acceptance.arrange_rep AND account.disable = " . STATUS_ENABLE
                     .   ") AS arrange_rep_account_name, "
                     .   "shipping_type.name AS shipping_type_name "
                     . "FROM order_acceptance "
                     . "JOIN shipping_type ON order_acceptance.shipping_type_cd = shipping_type.code "
                     . "WHERE shipping_type.is_remaining_confirm = 1 AND shipping_type.disable = " . STATUS_ENABLE . " "
                     . "AND order_acceptance.delivery_date >= '" . $params['delivery_date_from'] . "' "
                     . "AND order_acceptance.delivery_date <= '" . $params['delivery_date_to'] . "' "
                     . "ORDER BY work_subcontractor_name ASC, order_acceptance.delivery_date ASC, j_code ASC, branch_cd ASC ";

                return  $this->exec_query($sql);
    }

    /**
     * Get Order Acceptances
     * @param string $type get one or all record
     * @param array $options
     * @return array
     * @author cong_tien
     */
    public function get_order_acceptances($type, $options = array()) {
        if (!empty($options)) {
            if (!empty($options['fields'])) {
                $this->db->select($options['fields']);
            }

            if (!empty($options['conditions'])) {
                foreach ($options['conditions'] as $field => $condition) {
                    $this->db->where("{$field}", "{$condition}");
                }
            }

            if (!empty($options['order'])) {
                $this->db->order_by($options['order']);
            }

            if (!empty($options['joins'])) {
                foreach ($options['joins'] as $table => $condition) {
                    $this->db->join($table, $condition);
                }
            }
        }

        $this->db->where("order_acceptance.disable", 0);

        $query = $this->db->get("order_acceptance");

        if ($type == 'first') {
            return $query->row_array();
        } elseif ($type == 'all') {
            return $query->result_array();
        }
    }

        /**
     * Calculate extra order_acceptances start month
     *
     * @param string $acceptance_month
     * @return array
     *
     * @author van_thuat 2015-08-12 #5978
     */
    public function calculate_extra_order_acceptances_start_month($acceptance_month = '') {
        try {
            $acceptance_month_obj = new DateTime($acceptance_month);
        } catch (Exception $e) {
            $acceptance_month_obj = new DateTime();
        }

        // Set value for next month of $acceptance_month
        $next_month = new DateTime($acceptance_month_obj->format('Y-m-d'));
        $next_month->modify("+1 months");

        // Calculate adjusted_amount for each source_subcontractor
        // Phong commented on 28/September/2015 - Redmine ID: 6727
        /* $sql = "SELECT order_acceptance.source_subcontractor_id"
        . ", SUM(order_acceptance.adjusted_amount) - SUM(FLOOR(IF(order_acceptance.order_acceptance_status = 2, order_acceptance.shipping_charge_tax, order_acceptance.unit_price * order_acceptance.quantity))) AS adjusted_amount"
        . " FROM order_acceptance"
        . " WHERE order_acceptance.disable = " . STATUS_ENABLE
        . " 	AND DATE_FORMAT(order_acceptance.delivery_date, '%Y/%m') = '{$acceptance_month_obj->format('Y/m')}'"
        . " GROUP BY order_acceptance.source_subcontractor_id"
        ; */
        // End Phong commented on 28/September/2015

        // Phong added on 28/September/2015 - Redmine ID: 6727
        // If August has no data of subcontractor A => No record of A in September is created
//#7116: Start 9/Oct/2015
        /*
         * Phong commentted 18 Dec 2015 - Redmine ID: 8538
         * Line 3079 old: SUM(FLOOR(IF(t1.order_acceptance_status = 2, (t1.shipping_charge_tax + t1.tax_free), t1.unit_price * t1.quantity)))
         * Line 3079 new: SUM(FLOOR(IF((t1.order_acceptance_status = 1) AND t1.shipping_charge_tax=0 AND t1.tax_free=0, t1.unit_price * t1.quantity, (t1.shipping_charge_tax + t1.tax_free) )))
         * Description: If order_acceptance_status = 1 AND shipping_charge_tax = 0 AND tax_free = 0 => Total amount = price*quantity
         * Else: Total amount = shipping_charge_tax + tax_free
         */
        $sql = "SELECT t1.source_subcontractor_id"
        . ", SUM(t1.adjusted_amount) - "
        . " SUM(FLOOR(IF((t1.order_acceptance_status = 1) AND t1.shipping_charge_tax=0 AND t1.tax_free=0, t1.unit_price * t1.quantity, (t1.shipping_charge_tax + t1.tax_free) )))"
        . "AS adjusted_amount"
        . " FROM
                (SELECT
                                    OA.shipping_type_cd,
                                    OA.delivery_date,
                                    OA.close_date,
                                    OA.adjusted_amount,
                                    OA.is_extra_item,
                                    SUB.code,
                                    SUB.name,
                                    SUB.abbr_name,
                                    OA.j_code,
                                    OA.branch_cd,
                                    OA.unit_price,
                                    OA.quantity,
                                    SUB.id,
                                    OA.id AS OAid,
                                    OA.client_name,
                                    OA.order_acceptance_status,
                                    OA.shipping_charge_tax,
                                    OA.source_subcontractor_id, OA.`disable`,
                                    SHT.is_advance_payment, OA.tax_free
                FROM order_acceptance OA
                LEFT JOIN subcontractor SUB ON OA.source_subcontractor_id = SUB.id
                LEFT JOIN shipping_type SHT ON OA.shipping_type_cd = SHT.code AND SHT.`disable` = 0
                WHERE SUB.`disable` = 0
            ) t1
        "
        . " WHERE (t1.is_extra_item = 1 OR t1.is_advance_payment = 1) AND t1.disable = " . STATUS_ENABLE
        . " 	AND DATE_FORMAT(t1.delivery_date, '%Y/%m') = '{$acceptance_month_obj->format('Y/m')}'"
        . "
            AND t1.OAid NOT IN
                (
                SELECT id
                FROM order_acceptance
                WHERE order_acceptance_status = 2
                AND ((shipping_charge_tax = 0 OR shipping_charge_tax = '') AND (tax_free = 0 OR tax_free = ''))
                AND disable = 0
                )
            GROUP BY t1.source_subcontractor_id ORDER BY t1.OAid"
        ;
        // End Phong added on 28/September/2015
//#7116: End 9/Oct/2015
        $query = $this->db->query($sql);
        if (!($extra_order_acceptances = $query->result_array())) return array();

        // Get extra_order_acceptances is disable
        $sql = "SELECT order_acceptance.id, order_acceptance.source_subcontractor_id"
        . " FROM order_acceptance"
        . " WHERE order_acceptance.disable = " . STATUS_DISABLE
        . " 	AND order_acceptance.is_extra_item = 1"
        . " 	AND order_acceptance.delivery_date = '{$next_month->format('Y-m')}-01'"
           . " 	AND order_acceptance.close_date = '{$acceptance_month_obj->format('Y-m')}-{$acceptance_month_obj->format('t')}'"
        ;

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $extra_order_acceptances_disable[$row['source_subcontractor_id']] = $row;
            }
        } else {
            $extra_order_acceptances_disable = array();
        }

        foreach ($extra_order_acceptances as $extra_order_acceptance) {
            if (isset($extra_order_acceptances_disable[$extra_order_acceptance['source_subcontractor_id']])) {
                $res['update'][] = array(
                        'id'                => $extra_order_acceptances_disable[$extra_order_acceptance['source_subcontractor_id']]['id'],
                        'adjusted_amount'   => $extra_order_acceptance['adjusted_amount'],
                        'lastup_account_id' => $this->auth->get_account_id(),
                        'lastup_datetime'   => $this->_db_now(),
                        'disable'           => STATUS_ENABLE
                );
            } else {
                $res['insert'][] = array(
                        'client_name'             => "■繰越",
                        'source_subcontractor_id' => $extra_order_acceptance['source_subcontractor_id'],
                        'delivery_date'           => "{$next_month->format('Y-m')}-01",
                        'is_extra_item'           => 1,
                        'adjusted_amount'         => $extra_order_acceptance['adjusted_amount'],
                        'close_date'              => "{$acceptance_month_obj->format('Y-m')}-{$acceptance_month_obj->format('t')}",
                        'lastup_account_id'       => $this->auth->get_account_id(),
                        'create_datetime'         => $this->_db_now(),
                        'lastup_datetime'         => $this->_db_now(),
                        'disable'                 => STATUS_ENABLE,
                        'report_remarks'          => ''
                );
            }
        }

        return $res;
    }

    protected function _get_closing_date() {
        if (!empty($this->_list_closing_date)) return $this->_list_closing_date;

        $this->load->model('closing_accountant_model');

        $closing_date = $this->closing_accountant_model->get_closing_accountant();
        $n_closingdate = count($closing_date);
        if($n_closingdate > 0){
            $index = $n_closingdate -1;
            $this->_list_closing_date = date('Y-m', strtotime($closing_date[$index]['close_date']));
        }

//         foreach ($closing_date as $date) {
//            // $this->_list_closing_date[] = date('Y-m', strtotime($date['close_date']));
//         }
        if(empty($this->_list_closing_date)){
            return date("Y-m");
        }
        else{

        return $this->_list_closing_date;
        }
    }

    protected function _get_shipment_shape_code() {
        static $array = null;

        if ($array !== null) return $array;

        $this->load->model('gui_parts_element_model');

        $array = $this->gui_parts_element_model->get_all_shipment_shape_elements();

        return $array;
    }

    protected function _check_is_shipment_shape_code($code) {
        $shipment_shapes = $this->_get_shipment_shape_code();

        foreach ($shipment_shapes as $shipment) {
            if ($shipment['code'] == $code) return true;
        }

        return false;
    }

    /**
     * check import arrang_rep|account_id
     * @param  string $login_id
     * @return mixed
     */
    protected function _check_account_login_id($login_id) {
        if (!$this->_accounts) {
            $this->_accounts = $this->account_model->get_accounts();
        }

        foreach ($this->_accounts as $account) {
            if ($account['login_id'] == $login_id) {
                if ($account['resignation']) return array('resignation' => 1, 'id' => $account['id'], 'name' => '');
                return array('resignation' => 0, 'id' => $account['id'], 'name' => $account['name']);
            }
        }
        return 0;

    }

//#7117:Start
    /**
     * Get Order Acceptances for Delivery Statement
     *
     * @param array $conditions
     * @return array
     * @author cong_tien
     */
    public function get_order_acceptances_download($conditions = array()) {
        $this->db->select('order_acceptance.id AS order_acceptance_id
                        , business_unit.name AS business_unit_name
                        , department.name AS department_name
                        , account.name AS account_name
                        , order_acceptance.client_name
                        , order_acceptance.tax_free
                        , order_acceptance.shipping_charge_tax
                        , order_acceptance.stamp_tax
                        , order_acceptance.etc_tax
                        , order_acceptance.shipping_charge
                        , order_acceptance.etc
                        , order_acceptance.amount')
                ->from('order_acceptance')
                ->join('account', '(account.id = order_acceptance.account_id AND account.`disable` = ' . STATUS_ENABLE . ')', '', false)
                ->join('department', '(department.id = account.department_id AND department.`disable` = ' . STATUS_ENABLE . ')', '', false)
                ->join('business_unit', '(business_unit.id = department.business_unit_id AND business_unit.`disable` = 0)', '', false)
                ->where('order_acceptance.`disable`', STATUS_ENABLE, false)
                ->where('order_acceptance.is_extra_item', STATUS_ENABLE)
                ->where('order_acceptance.j_code', '')
                ->where('order_acceptance.branch_cd', '')
                ->order_by('business_unit.name, account.name, order_acceptance.j_code, order_acceptance.branch_cd');

        // search delivery From
        if (!empty($conditions['from'])) {
            $this->db->where('order_acceptance.summary_month >', $conditions['from']);
        }

        // search delivery To
        if (!empty($conditions['to'])) {
            $this->db->where('order_acceptance.summary_month <', $conditions['to']);
        }

        return $this->db->get()->result_array();
    }
//#7117:End

//#7968:Start
    /**
     * Get all order acceptance by j_code and branch code
     * @return array
     * @author van_don
     * @since 2015/11/23
     */
    public function get_order_acceptance_by_jcode_and_branch_cd($j_code_branch_cd) {
        $this->db->select('oc.id, oc.j_code, oc.branch_cd', false)
        ->from('order_acceptance oc')
        ->join('order o', 'o.id = oc.order_id')
        ->where('CONCAT(oc.j_code, oc.branch_cd) = ', $j_code_branch_cd)
        ->where('oc.disable', 0);
        return $this->db->get()->result_array();
    }

    /**
     * Revert is_order_input status and order_status = 2 if old order status = 3
     * @return boolean
     * @author van_don
     * @since 2015/11/23
     */
    public function revert_is_order_input($jcode_branch) {

        $revert_j_code      = substr($jcode_branch, 0, -2);
        $revert_branch_cd   = substr($jcode_branch, -2);

        if (!$revert_j_code || !$revert_branch_cd) {
            $this->app_logger->error_log('WRONG UPDATE CONDITION.');
            return false;
        }

        //Get order_detail by j_code and branch code
        $this->load->model('order_model');
        if (!$order_detail = $this->order_model->get_order_detai_by_jCode_and_BrandCode($revert_j_code, $revert_branch_cd)) {
            $this->app_logger->error_log('ORDER DETAIL NOT EXISTS');
            return false;
        }

        $update_data = array(
                'is_order_input' => IS_NOT_ORDER_INPUT
        );

        if ($order_detail[0]->is_acceptance_input == IS_ACCEPTANCE_INPUT) {
            $update_data['is_acceptance_input'] = IS_NOT_ACCEPTANCE_INPUT;
        }

        if ($order_detail[0]->order_status == ORDER_STATUS_CONFIRMED) {
            $update_data['order_status'] = ORDER_STATUS_UNDETERMINED;
        }

        if (!$this->update('order_detail', $update_data, $order_detail[0]->id)) {
            $this->app_logger->error_log('REVERT is_order_input FAIL');
            return false;
        }
        return true;
    }
//#7968:End

//#9748: Start
    public function search_mgmt_dp($data) {
        $postFormOrder = 0 ;

        if(isset($data['postFormOrder'])){
            $postFormOrder = $data['postFormOrder'];
        }

        $this->db->select('order_detail.is_acceptance_input AS flag_is_accceptance_input,p.name as pro_name
                        , order_detail.contents as content_pro
                        , order_detail.unit_price as unit_price_in_order
                        , order_detail.quantity  as quanity_in_order
                        , order_detail.amount as total_in_order
                        , order_detail.cost_unit_price as sales_in_order
                        , order_detail.cost_total_price as total_cost_in_order_excluding_tax'.
//#9960:Start
                        ',order_detail.tax_type as od_tax_type
                        ,order_detail.amount as od_amount
                        ,order_detail.delivery_date as od_delivery_date'.
//#9960:Start
                        ',c.name as client_name_order
                        , oc.*
                        , o.client_id'.
//Fixbug taskcheckbox:Start
                        ', oc.amount as amount_frist_bd'.
//Fixbug taskcheckbox:End
                        ', CONCAT(oc.j_code, oc.branch_cd) as jcode_branch
                        ', false)
                                ->from('order_acceptance oc')
                                ->join('order o', 'o.id = oc.order_id AND o.disable = ' . STATUS_ENABLE, 'left')
                                ->join('order_detail', 'order_detail.order_id = o.id AND order_detail.branch_cd = oc.branch_cd AND order_detail.disable = ' . STATUS_ENABLE, 'left')
                                ->join('product p',  'order_detail.product_id = p.id AND p.disable = ' . STATUS_ENABLE, 'left')
                                ->join('client c',  'o.client_id = c.id AND c.disable = ' . STATUS_ENABLE, 'left')
//#9748:Start
                                ->join('account a', 'a.id = oc.account_id AND a.disable = ' . STATUS_ENABLE)
                                ->join('department d', 'd.id = a.department_id AND d.disable = ' . STATUS_ENABLE)
                                ->join('business_unit bu', 'bu.id = d.business_unit_id AND bu.disable = ' . STATUS_ENABLE)
                                ->where('bu.finish_work_report_template', DP_FWR_TEMPLATE_CODE)
//#9748:End
                                ->where('oc.disable', STATUS_ENABLE)
                                ->where('oc.is_extra_item', STATUS_ENABLE)
                                ->order_by('oc.j_code', 'ASC')
                                ->order_by('oc.branch_cd', 'ASC')
                                ->order_by('order_detail.contents', 'ASC')
                                ->order_by('oc.delivery_date', 'ASC');

        if (isset($data['account_id']) && $data['account_id'] != -1) {
            $this->db->where('oc.account_id', $data['account_id']);
        } elseif (isset($data['business_unit_id']) && $data['business_unit_id'] != -1) {
//#9748:Start
//            $this->db->join('account a', 'a.id = oc.account_id AND a.disable = ' . STATUS_ENABLE)
//            ->join('department d', 'd.id = a.department_id AND d.disable = ' . STATUS_ENABLE)
//            ->where('d.business_unit_id', $data['business_unit_id']);
            $this->db->where('bu.id', $data['business_unit_id']);
//#9748:End
        }

        $losingDatedefault = $this->_get_closing_date();
        // filter by Status And Closing accountance (承認状態)
        if (!empty($data['order_acceptance_status'])) {
            if (($key = array_search(ORDER_ACCEPTANCE_ACCOUNT_TIGHTEN, $data['order_acceptance_status'])) !== false) {

                unset($data['order_acceptance_status'][$key]);
                $closing_date_after = date('Y-m', strtotime("+1 month", strtotime($losingDatedefault . '-01')));
                $this->db->where('oc.summary_month <', $closing_date_after);

            } else {

                if (!isset($data['is_from_order'])) {
                    $this->db->where('oc.summary_month >', $losingDatedefault);
                }

            }

            if (!empty($data['order_acceptance_status'])) {
                $this->db->where_in('oc.order_acceptance_status', $data['order_acceptance_status']);
            }
        } else {

            $this->db->where_in('oc.order_acceptance_status', array(ORDER_ACCEPTANCE_UNAPPROVED, ORDER_ACCEPTANCE_APPROVED));
        }

        if (isset($data['client_name']) && $data['client_name']) {
            $this->db->like('c.name', $data['client_name']);
        }
        if (isset($data['source_subcontractor_name']) && $data['source_subcontractor_name']) {
            $this->db->join('subcontractor sos', 'sos.id = oc.source_subcontractor_id')
            ->like('sos.name', $data['source_subcontractor_name']);
        }

        if (!empty($data['from_summary_month']) && empty($data['to_summary_month'])) {
            $to_date = $data['from_summary_month'];
            $year = $this->getRightFomatDate($to_date, false, true);
            $moth = $this->getRightFomatDate($to_date, false, false, true);
            if ($year != "") {

                $this->db->where(" YEAR(DATE_FORMAT(STR_TO_DATE(oc.summary_month,'%Y-%m-%d'), '%Y-%m-%d')) >= ", $year, FALSE);
            }
            if ($moth != ""):

            $this->db->where(" MONTH(DATE_FORMAT(STR_TO_DATE(oc.summary_month,'%Y-%m-%d'), '%Y-%m-%d')) >= ", $moth, FALSE);

            endif;
        } elseif (empty($data['from_summary_month']) && !empty($data['to_summary_month'])) {
                $to_date = $data['to_summary_month'];
                        $to_date = $this->getRightFomatDate($to_date, true);
                                $this->db->where(" DATE_FORMAT(STR_TO_DATE(oc.summary_month,'%Y-%m-%d'), '%Y-%m') <= ", $to_date);
        } else {
            if (!empty($data['from_summary_month']) && !empty($data['to_summary_month'])) {


                    $form_summarymonth = $data['from_summary_month'];

                            $to_summarymonth = $data['to_summary_month'];

                        $dayss = $this->getRightFomatDate($form_summarymonth, true);

                                $dayss_to = $this->getRightFomatDate($to_summarymonth, true);

                                $this->db->where(" DATE_FORMAT(STR_TO_DATE(oc.summary_month,'%Y-%m-%d'), '%Y-%m') >= ", $dayss);
                $this->db->where(" DATE_FORMAT(STR_TO_DATE(oc.summary_month,'%Y-%m-%d'), '%Y-%m') <= ", $dayss_to);
            }
        }

        if (!empty($data['from_delivery_date']) && empty($data['to_delivery_date'])) {
                                $form_day_delvie = $data['from_delivery_date'];
                                        $year = $this->getRightFomatDate($form_day_delvie, false, true);
                                        $moth = $this->getRightFomatDate($form_day_delvie, false, false, true);
                                        $day = $this->getRightFomatDate($form_day_delvie, false, false, false, true);
                                        if ($year != ""):

                                        $this->db->where('  YEAR(oc.delivery_date) >=', $year);

                                        endif;

                                        if ($moth != ""):

                                               $this->db->where('  MONTH(oc.delivery_date) >=', $moth);

                                        endif;

                                        if ($day != ""):

                                              $this->db->where('  DAY(oc.delivery_date) >=', $day);
                                        endif;

            } elseif (!empty($data['to_delivery_date']) && !empty($data['from_delivery_date'])) {

            $from_delivery_date =  $data['from_delivery_date'];
            $to_delivery_date   =  $data['to_delivery_date'];
            $formdatedv = $this -> getRightFomatDate($from_delivery_date);
            $todatedv   = $this->getRightFomatDate($to_delivery_date,false,false,false,false,true);
            $this->db->where(" oc.delivery_date >= ", $formdatedv);
            $this->db->where(" oc.delivery_date <= ", $todatedv);


        }

        $wheresql_jocde_branch = $this->get_where_sql_by_branchcode_and_jcode($data,'oc.j_code','oc.branch_cd');

        if(!empty($wheresql_jocde_branch)){
        $this->db->where($wheresql_jocde_branch);
        }

            if(isset($data['list_id_oc_imput']['listid_import'])){

            if(($data['list_id_oc_imput']['listid_import'])){
            $this->db->where_in('oc.id', $data['list_id_oc_imput']['listid_import']);
        }

        }

        return $this->db->get()->result_array();

    }

    public function get_order_detail_mgnt_DP($j_code, $branch_cd) {
        $j_code    = trim($j_code);
        $branch_cd = trim($branch_cd);
        $branch_cd = sprintf('%02d', $branch_cd);

        // #6801-s #6901-modifly-s
        $this->db->select('
                      o.id
                    , o.j_account_id
                    , o.client_id
                    , c.name as client_name
                    , od.delivery_date
                    , od.is_order_input
                    , od.is_acceptance_input
                    , a.login_id
                    , a.name
                    , p.name as pro_name
                    , od.contents as content_pro
                    , od.unit_price as unit_price_in_order
                    , od.quantity  as quanity_in_order
                    , od.amount as total_in_order
                    , od.cost_unit_price as sales_in_order
                    , od.cost_total_price as total_cost_in_order_excluding_tax'.
//#9960:Start
                    ', od.tax_type as od_tax_type
                    , od.amount as od_amount
                    , od.delivery_date as od_delivery_date'.
//#9960:End
                    ', od.is_change_report_apply
                    ')
                        ->from('order o')
                        ->join('order_detail od', 'od.order_id = o.id')
                        ->join('client c', 'c.id = o.client_id')
                        ->join('account a', 'a.id = o.j_account_id')
                        ->join('product p',  'od.product_id = p.id AND p.disable = ' . STATUS_ENABLE)
                        ->where('o.j_code', $j_code)
                        ->where('od.branch_cd', $branch_cd)
                        ->group_start()
                        ->or_where('od.order_status', 2)
                        ->or_where('od.order_status', 3)
                        ->group_end()
                        ->where('o.disable', 0)
                        ->where('od.disable', 0);
        $this->db->select('bu.finish_work_report_template')
                 ->join('department d', 'd.id = a.department_id')
                 ->join('business_unit bu', 'bu.id = d.business_unit_id')
                 ->where('d.disable', 0)
        ->where('bu.disable', 0);
        return $this->db->get()->row();
        // #6801-s  #6901-modifly-e
    }


    public function insert_order_acceptance_mgnt_DP($order_acceptances) {

        $this->db->trans_begin();

        try {
            foreach ($order_acceptances as $k => $order_acceptance) {

                if (empty($order_acceptance)) continue;

                $data = $this->_set_data_mgnt_DP($order_acceptance);
//                 print_r($data);
                if (empty($data['report_remarks'])) {
                    $data['report_remarks'] = "";
                }
                if (!$this->db->insert('order_acceptance', $data))
                    throw new Exception("Insert fail");

                if ($data['order_acceptance_status'] == ORDER_ACCEPTANCE_APPROVED && $data['order_id']) {
                    if ($this->db->update('order_detail',
                            array('is_acceptance_input' => 1,
                                     'lastup_datetime' => date('Y-m-d H:i:s')), 'order_id = '. $data['order_id'] . ' and branch_cd = ' . $data['branch_cd'])) {
                        throw new Exception("update fail");
                    }
                }

                $insert_data_id[$k] = $this->db->insert_id();
            }

            $this->db->trans_commit();

        } catch(Exception $e) {
            $this->db->trans_rollback();
            return array (
                    'status' => false,
                    'data_id' => array(),
            );
        }

        return array (
                'status' => true,
                'data_id' => $insert_data_id,
        );
    }

    public function update_order_acceptance_mgnt_DP($order_acceptances){

        $delete_datas = array();
        $update_datas = array();
        $order_detail_datas = array();
        //#7968:Start
        $revert_is_order_input = array();
        //#7968:End
        foreach ($order_acceptances as $order_acceptance) {
            if (empty($order_acceptance)) continue;

            if (isset($order_acceptance['is_deleting']) && $order_acceptance['is_deleting']) {
                $delete_datas[] = array(
                        'id'              => $order_acceptance['id'],
                        'disable'         => 1,
                        'lastup_datetime' => date('Y-m-d H:i:s'),
                        'lastup_account_id' => $this->auth->get_account_id()
                );

                //Get data for reverting is_order_input incase deleting a order accceptance
                if ($order_acceptance['jcode_branch'] && ($revert_data = $this->_check_order_acceptance_to_revert_is_order_input($order_acceptance['jcode_branch']))) {
                    $revert_is_order_input[$order_acceptance['jcode_branch']] = $order_acceptance['jcode_branch'];
                }

            } else {

                $seted_data = $this->_set_data_mgnt_DP($order_acceptance);
                if (($seted_data['order_acceptance_status'] == ORDER_ACCEPTANCE_APPROVED) && ($seted_data['order_id'])) {

                    $order_detail_datas[] = array(
                            'order_id'            => $seted_data['order_id'],
                            'branch_cd'           => $seted_data['branch_cd'],
                    );
                }


                $update_datas[] = $this->_set_data_mgnt_DP($order_acceptance);
                //#7968:Start
                //Get data for reverting is_order_input incase updating a order accceptance
                if ($order_acceptance['jcode_branch']) {
                    $revert_j_code      = substr($order_acceptance['jcode_branch'], 0, -2);
                    $revert_branch_cd   = substr($order_acceptance['jcode_branch'], -2);
                    if(isset($order_acceptance['j_code_has_oldVal'])){
                        $revert_j_code = $order_acceptance['j_code_has_oldVal'];
                    }
                    if(isset($order_acceptance['branch_cd_has_oldVal'])){
                        $revert_branch_cd = $order_acceptance['branch_cd_has_oldVal'];
                    }
                    $order_acceptance['jcode_branch'] =  $revert_j_code.$revert_branch_cd;


                    if ($seted_data['branch_cd'] == '') {
                        if ($revert_data = $this->_check_order_acceptance_to_revert_is_order_input($order_acceptance['jcode_branch'])) {
                            $revert_is_order_input[$order_acceptance['jcode_branch']]  = $order_acceptance['jcode_branch'];
                        }
                    } elseif (($seted_data['j_code'] != $revert_j_code) || ($revert_branch_cd != $seted_data['branch_cd'])) {
                        if ($revert_data = $this->_check_order_acceptance_to_revert_is_order_input($order_acceptance['jcode_branch'])) {
                            $revert_is_order_input[$order_acceptance['jcode_branch']]  = $order_acceptance['jcode_branch'];
                        }
                    }

                    if (( $revert_j_code) || ($revert_branch_cd )) {
                        if ($revert_data = $this->_check_order_acceptance_to_revert_is_order_input($order_acceptance['jcode_branch'])) {
                            $revert_is_order_input[$order_acceptance['jcode_branch']]  = $order_acceptance['jcode_branch'];
                        }
                    }


                }
                //#7968:End
            }
        }

        try {

            $this->db->trans_begin();
            if (!empty($update_datas)) {
                if (!$this->db->update_batch('order_acceptance', $update_datas, 'id')) {
                    throw new Exception("update error");
                }
            }

            if (!empty($delete_datas)) {
                if (!$this->db->update_batch('order_acceptance', $delete_datas, 'id')) {
                    throw new Exception("update error");
                }
            }

            if (!empty($order_detail_datas)) {
                if (!$this->_update_is_acceptance_input($order_detail_datas)) {
                    throw new Exception("update error");
                }
            }
            //#7968:Start
            if (!empty($revert_is_order_input)) {
                foreach ($revert_is_order_input as $key => $jcode_branch) {
                    if ($this->_check_order_acceptance_to_revert_is_order_input($key)) {
                        continue;
                    }
                    if (!$this->revert_is_order_input($jcode_branch)) {
                        throw new Exception("update error");
                    }
                }
            }
            //#7968:Start
            // print_r($this->db->last_query());
            $this->db->trans_commit();

        } catch(Exception $e) {
            $this->db->trans_rollback();
            return false;
        }

        return true;
    }

    private function _set_data_mgnt_DP($order_acceptance) {

        foreach ($order_acceptance as $k => $v) {

            if (empty($v)) $order_acceptance[$k] = 0;
            if (empty($v) && $k == 'j_code') $order_acceptance[$k] = '';
            if (empty($v) && $k == 'branch_cd') $order_acceptance[$k] = '';
            if (empty($v) && $k == 'client_name') $order_acceptance[$k] = '';
            if (empty($v) && $k == 'set_name') $order_acceptance[$k] = '';
            if (empty($v) && $k == 'summary_month') $order_acceptance[$k] = '';
            if (empty($v) && $k == 'summary_month') $order_acceptance[$k] = '';
            if (empty($v) && $k == 'delivery_date_for_save') {
                $order_acceptance[$k] = '0000-00-00';
            }
            if (empty($v) && $k == 'summary_month') $order_acceptance[$k] = '';

            if ( (empty($v) && $k == 'tax_free')
                    || (empty($v) && $k == 'shipping_charge_tax')
                    || (empty($v) && $k == 'stamp_tax')
                    || (empty($v) && $k == 'etc_tax')
                    || (empty($v) && $k == 'shipping_charge')
                    || (empty($v) && $k == 'etc')) {

                        if (trim($v) == "") {
                            $order_acceptance[$k] = "";
                        } else {
                            $order_acceptance[$k] = 0;
                        }
                    }
        }

        $data = array (
                'account_id'                => $order_acceptance['account_id'],
                'client_name'               => $order_acceptance['client_name'],
                'source_subcontractor_id'   => $order_acceptance['source_subcontractor_id'],
                'quantity'                  => $order_acceptance['quanity_in_order'],
                'unit_price'                => $order_acceptance['unit_price_in_order'],
                'shipping_type_cd'          => $order_acceptance['shipping_type_cd'],
                'summary_month'             => date('Y-m', strtotime(str_replace('/', '-', $order_acceptance['summary_month'].'-'.'01'))),
                'lastup_account_id'         => $this->auth->get_account_id(),
        );
        if (isset($order_acceptance['delivery_date_for_save'])) {
            $data['delivery_date'] = $order_acceptance['delivery_date_for_save'];
        }

           $data['amount'] = $order_acceptance['amount'];

        if(!isset($order_acceptance['id'])){
            $data['order_acceptance_status'] = ORDER_ACCEPTANCE_UNAPPROVED;
        }
        else{
            $data['order_acceptance_status'] =$order_acceptance['order_acceptance_status'];
        }

        //#6946:End
        // if (isset($order_acceptance['is_approved']) && $order_acceptance['is_approved'] && $order_acceptance['is_change_acceptance_status'] && $data['amount']) {
        if (isset($order_acceptance['is_approved']) && $order_acceptance['is_approved'] && $order_acceptance['is_change_acceptance_status']){

            //#7278: Start
            $data['order_acceptance_status']     = ORDER_ACCEPTANCE_APPROVED;
            $data['acceptance_approval_date']    = date('Y-m-d');
            $data['acceptance_approval_account_id'] = $this->auth->get_account_id();
            //#7278: End
        }

        $data['j_code']    = $order_acceptance['j_code'];
        $data['branch_cd'] = sprintf('%02d', $order_acceptance['branch_cd']);
        $data['order_id']  = $order_acceptance['order_id'];

        //#8088:Start

        if(isset($order_acceptance['returnStatusReturn']) && ($order_acceptance['returnStatusReturn'] == 1)){
            $data['order_acceptance_status'] = ORDER_ACCEPTANCE_UNAPPROVED;
        }

        //#8088:End

        if (isset($order_acceptance['is_difference_common']) && $order_acceptance['is_difference_common']) {
            $data['j_code']   = $data['branch_cd'] = '';
            $data['order_id'] = 0;
        }

        $data['lastup_account_id'] = $this->auth->get_account_id();
        $data['lastup_datetime'] = date('Y-m-d H:i:s');
        if (isset($order_acceptance['is_new']) && $order_acceptance['is_new']) {

            $data['create_datetime'] = date('Y-m-d H:i:s');
            return $data;

        }
        if(isset($order_acceptance['returnStatusReturn']) && ($order_acceptance['returnStatusReturn'] == 1)){
            $data['order_acceptance_status'] = ORDER_ACCEPTANCE_UNAPPROVED;
        }


        $data['id'] = $order_acceptance['id'];
        return $data;

        }
//9748:End

//9748:Start
        public function set_rules_mgt_DP($rule_name, $conditions = array()){


            $rules = array();
            foreach ($conditions as $k => $v) {

                if ($v && $k != 'is_required_delivery_date') {
                    $rules[] = $k;
                }
            }

            $rules = implode('|', $rules);
            $rules = !empty($rules) ? $rules . '|' : '';

            $validate_rule['update'] = array (
                    array(
                        'field'  => 'j_code',
                        'label'  => 'j_code',
                        'rules'  => $rules . 'trim|max_length[10]',
                        'errors' => array(
                                        'required' => lang('error_j_code_is_required'),
                                        'max_length' => lang('error_j_code_max_length'),
                                    )
                    ),
                    array(
                        'field'  => 'branch_cd',
                        'label'  => 'branch_cd',
                        'rules'  => $rules . 'trim|max_length[2]',
                        'errors' => array(
                                        'required' => lang('error_branch_cd_is_required'),
                                        'max_length' => lang('error_branch_cd_max_length'),
                                    )
                    ),
                    array(
                        'field'  => 'account_id',
                        'label'  => 'account_id',
                        'rules'  => 'required|trim',
                        'errors' => array(
                                        'required' => lang('error_account_id_is_required'),
                                    )
                    ),
                    array(
                        'field'  => 'arrange_rep',
                        'label'  => 'arrange_rep',
                        'rules'  => 'trim',
                        'errors' => array(
                                        'required' => lang('error_arrange_rep_is_required'),
                                    )
                    ),
                    array(
                        'field'  => 'client_name',
                        'label'  => 'client_name',
                        'rules'  => 'required|trim',
                        'errors' => array(
                                        'required' => lang('error_client_name_is_required'),
                                    )
                    ),
                    array(
                        'field'  => 'source_subcontractor_id',
                        'label'  => 'source_subcontractor_id',
                        'rules'  => 'required|trim',
                        'errors' => array(
                                        'required' => lang('error_source_subcontractor_id_is_required'),
                                    )
                    ),
                    array(
                        'field'  => 'work_subcontractor_id',
                        'label'  => 'work_subcontractor_id',
                        'rules'  => 'trim',
                        'errors' => array(
                                        'required' => lang('error_work_subcontractor_name_is_required'),
                                    )
                    ),
                    array(
                        'field'  => 'shipping_subcontractor_id',
                        'label'  => 'shipping_subcontractor_id',
                        'rules'  => 'trim',
                        'errors' => array(
                                        'required' => lang('error_shipping_subcontractor_name_is_required'),
                                    )
                    ),
                    array(
                            'field'  => 'summary_month',
                            'label'  => 'summary_month',
                            'rules'  => 'trim|required',
                            'errors' => array(
                                    'required' => lang('error_summary_month_insert_is_required'),
                            )
                    ),
                    array(
                        'field'  => 'shipments_shape_cd',
                        'label'  => 'shipments_shape_cd',
                        'rules'  => 'trim',
                        'errors' => array(
                                        'required' => lang('error_shipments_shape_cd_is_required'),
                                    )
                    ),
                    array(
                        'field'  => 'amount',
                        'label'  => 'amount',
                        'rules'  => 'required|trim|numeric|max_length[9]',
                        'errors' => array(
                                        'required' =>lang('error_fixed_cost_excluding_tax'),
                                        'numeric' => lang('error_fixed_cost_excluding_tax_must_be_number'),
                                        'max_length' => lang('error_max_length_amount')
                                    )
                    ),
                    array(
                            'field'  => 'delivery_date_for_save',
                            'label'  => 'delivery_date_for_save',
                            'rules'  => 'required|trim|valid_full_date',
                            'errors' => array(
                                    'required' => lang('error_delivery_finish_date_required'),
                                    'valid_full_date' => lang('error_delivery_finish_date_invalid'),
                            )
                    ),
            );



            if (isset($validate_rule[$rule_name])) {
                return $validate_rule[$rule_name];
            }
        return array ();
        }
//#9748: End

}
