<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_detail_model extends ZR_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_rule($rule_name) {
        // Validate login form
        $validate_rule['insert'] = array(
                array(
                        'field' => 'order_id',
                        'rules' => 'required|integer'
                ),
                array(
                        'field' => 'branch_cd',
                        'label' => lang('lbl_branch_cd'),
                        'rules' => 'required'
                ),
                array(
                        'field' => 'product_id',
                        'label' => lang('lbl_product_name'),
                        'rules' => 'required',
                ),
                array(
                        'field' => 'contents',
                        'label' => lang('lbl_content'),
                        'rules' => 'required',
                ),
                array(
                        'field' => 'delivery_date',
                        'label' => lang('lbl_delivery_date'),
                        'rules' => 'required',
                ),
                array(
                        'field' => 'billing_date',
                        'label' => lang('lbl_billing_date'),
                        'rules' => 'required',
                ),
                array(
                        'field' => 'payment_date',
                        'label' => lang('lbl_payment_date'),
                        'rules' => 'required',
                ),
                array(
                        'field' => 'quantity',
                        'label' => lang('lbl_quantity'),
//7389:Start
                        //'rules' => 'required|integer|check_equal_zero|max_length[6]',
                		'rules' => 'required|integer|max_length[7]',
//7389:End
                        'errors' => array (
                                'check_equal_zero' => "数量を入力して下さい。",
                        		//#6837: Start 2015/09/22 | #6811 2015/09/24
                                'max_length' => '数量は８桁以上の入力ができません。管理部に問い合わせてください。'
                        		////#6837: End
                        )
                ),
                array(
                        'field' => 'tax_type',
                        'label' => lang('lbl_tax_type'),
                        'rules' => 'required|integer',
                ),
                array(
                        'field' => 'unit_price',
                        'label' => lang('lbl_unit_price'),
                        'rules' => 'required|numeric|check_equal_zero|less_than[999999999.99]',
                        'errors' => array (
                                'check_equal_zero' => "単価を入力して下さい。",
                        		//#6837: Start 2015/09/22 | #6811 2015/09/24
                        		'less_than' => '単価は１０桁以上の入力ができません。管理部に問い合わせてください。'
                        		//#6837: End
                        )
                ),
                array(
                        'field' => 'amount',
                        'label' => lang('lbl_amount'),
                        'rules' => 'required|integer|max_length[9]',
                        'errors' => array(
                                // #6811 2015/09/24
                                'max_length' => '合計は１０桁以上の入力ができません。管理部に問い合わせてください。'
                                // #6811 End
                        )
                ),
                array(
                    'field' => 'cost_unit_price',
                    'label' => lang('lbl_order_detail_cost_unit_price'),
                    'rules' => 'required|numeric|less_than[999999999.99]',
                    'errors' => array(
                        // #6837 2015/09/26
                        'less_than' => '原価税別単価は１０桁以上の入力ができません。管理部に問い合わせてください。'
                        // #6837 End
                    )
                ),
                array(
                        'field' => 'cost_total_price',
                        'label' => lang('lbl_order_detail_cost_total_price'),
                        'rules' => 'required|numeric|max_length[9]',
                        'errors' => array(
                                // #6811 2015/09/24
                                'max_length' => '原価税別合計は１０桁以上の入力ができません。管理部に問い合わせてください。'
                                // #6811 End
                        )
                ),
                /* array(
                        'field' => 'gross_profit_amount',
                        'label' => lang('lbl_gross_profit_amount'),
                        'rules' => 'required|integer',
                ),
                array(
                        'field' => 'gross_profit_rate',
                        'label' => lang('lbl_order_detail_gross_profit_rate'),
                        'rules' => 'required',
                ), */
        );
        if (isset ( $validate_rule [$rule_name] )) {
            return $validate_rule [$rule_name];
        }
        return array ();
    }

    public function validate_save($branch, $is_ad_receive_payment, $is_change_report = false) {
        $this->form_validation->reset_validation();
        if (!isset($branch->order_id)) $branch->order_id = null;
        if (!isset($branch->branch)) $branch->branch = null;
        if (!isset($branch->product_name)) $branch->product_name = null;
        if (!isset($branch->contents)) $branch->contents = null;
        if (!isset($branch->delivery_date)) $branch->delivery_date = null;
        if (!isset($branch->billing_date)) $branch->billing_date = null;
        if (!isset($branch->payment_date)) $branch->payment_date = null;
        if (!isset($branch->quantity)) $branch->quantity = null;
        if (!isset($branch->tax_type)) $branch->tax_type = null;
        if (!isset($branch->unit_price)) $branch->unit_price = null;
        if (!isset($branch->amount_value)) $branch->amount_value = null;
        if (!isset($branch->cost_unit_price)) $branch->cost_unit_price = null;
        if (!isset($branch->cost_total_price)) $branch->cost_total_price = null;
        if (!isset($branch->gross_profit_amount_value)) $branch->gross_profit_amount_value = null;
        if (!isset($branch->gross_profit_rate_value)) $branch->gross_profit_rate_value = null;
        if (!isset($branch->disable)) $branch->disable = 0;
        if (!isset($branch->shift_color_cd)) $branch->shift_color_cd = '';
        if (!isset($branch->is_cancel_request)) $branch->is_cancel_request = 0;

        $date = $this->_db_now();
        $data = array(
                'order_id' => $branch->order_id,
                'branch_cd' => $branch->branch,
                'product_id' => $branch->product_name,
                'contents' => $branch->contents,
                'delivery_date' => $branch->delivery_date,
                'billing_date' => $branch->billing_date,
                'payment_date' => $branch->payment_date,
                'quantity' => $branch->quantity,
                'tax_type' => $branch->tax_type,
                'unit_price' => $branch->unit_price,
                'amount' => $branch->amount_value,
                'cost_unit_price' => $branch->cost_unit_price,
                'cost_total_price' => $branch->cost_total_price,
                'gross_profit_amount' => $branch->gross_profit_amount_value,
                'gross_profit_rate' => $branch->gross_profit_rate_value,
                'lastup_account_id' => $this->auth->get_account_id(),
                'create_datetime' => $date,
                'lastup_datetime' => $date,
                'disable' => $branch->disable,
                'shift_color_cd' => $branch->shift_color_cd,
                'is_cancel_request' => $branch->is_cancel_request,
        );
        $result = new stdclass();
        $result->errors = '';

        if (!$is_change_report) {
            $result->data = $data;
            return $result;
        }

        $this->form_validation->set_rules($this->get_rule('insert'));
        //#6837: Start 2015/09/22
        $val_data = $data;
        // if ($pos = strrpos($val_data['unit_price'], '.')) {
        //     $val_data['unit_price'] = substr($val_data['unit_price'],0,$pos);
        // }
        // if ($pos = strrpos($val_data['cost_unit_price'], '.')) {
        //     $val_data['cost_unit_price'] = substr($val_data['cost_unit_price'],0,$pos);
        // }

        $this->form_validation->set_data($val_data);
        //#6837: End
        $this->form_validation->set_error_delimiters('<p class="error-item">', '</p>');

        if ($this->form_validation->run() === FALSE) {
            $result->errors = validation_errors();
        }
        else {
            $result->data = $data;
        }

        $this->load->model('holiday_model');
        $this->load->model('closing_accountant_model');
        $closest_closing_accountant = $this->closing_accountant_model->get_closest_closing_date();

        if ($branch->is_ad_receive_payment = 1) {
            if(!empty($branch->delivery_date)) {
                if (!parse_date($branch->delivery_date))
                {
                    $result->errors = $result->errors . "<p class='error-item'>納品日が不正です。</p>";
                } else {
                    if ($closest_closing_accountant) {
                            if (date('Y', strtotime($branch->delivery_date)) < date('Y', strtotime($closest_closing_accountant[0]['closest_closing_date']))) {
                                $result->errors = $result->errors . "<p class='error-item'>納品日は経理締め済みです。</p>";
                            } elseif (date('Y', strtotime($branch->delivery_date)) == date('Y', strtotime($closest_closing_accountant[0]['closest_closing_date']))
                                  && date('m', strtotime($branch->delivery_date)) <= date('m', strtotime($closest_closing_accountant[0]['closest_closing_date']))) {

                                $result->errors = $result->errors . "<p class='error-item'>納品日は経理締め済みです。</p>";
                            }
                    }
                }
            }

            if (!empty($branch->billing_date) && !parse_date($branch->billing_date))
            {
                $result->errors = $result->errors . "<p class='error-item'>請求日が不正です。</p>";
            }
            if (!empty($branch->payment_date) && !parse_date($branch->payment_date))
            {
                $result->errors = $result->errors . "<p class='error-item'>支払日が無効です。</p>";
            }
            $billing_date = date($branch->billing_date);
            $payment_date = date($branch->payment_date);
            $delivery_date = date($branch->delivery_date);
            if ($is_ad_receive_payment) {
                if (!(strtotime($billing_date) < strtotime($payment_date) && strtotime($payment_date) < strtotime($delivery_date))) {
                    $result->errors = $result->errors . "<p class='error-item'>請求日＜支払日＜納品日で設定してください。</p>";
                }
            }
            //if ($this->holiday_model->check_holiday($branch->billing_date)) {
            //    $result->errors = $result->errors . "<p>請求日は休日です。</p>";
            //}
            if ($this->holiday_model->check_holiday($branch->payment_date)) {
                $result->errors = $result->errors . "<p class='error-item'>支払日は休日です。</p>";
            }
        } else {
            if(!empty($branch->delivery_date)) {
                if ($closest_closing_accountant) {
                   if (date('Y', strtotime($branch->delivery_date)) < date('Y', strtotime($closest_closing_accountant[0]['closest_closing_date']))) {
                        $result->errors = $result->errors . "<p class='error-item'>納品日は経理締め済みです。</p>";
                   } elseif (date('Y', strtotime($branch->delivery_date)) == date('Y', strtotime($closest_closing_accountant[0]['closest_closing_date']))
                         && date('m', strtotime($branch->delivery_date)) <= date('m', strtotime($closest_closing_accountant[0]['closest_closing_date']))) {

                            $result->errors = $result->errors . "<p class='error-item'>納品日は経理締め済みです。</p>";
                   }
                }
            }
        }
//#6900:Start
        $this->load->model('sales_slip_model');
//#9700:Start
        if ($this->sales_slip_model->have_sales_slip_detail($branch->order_id, $branch->branch, $branch->is_cancel_request)) {
            // check flat is_ad_invoice_issue = 0 && is_sales_slip_input = 1 =>message error: 入力済みの前金売上伝票が残っています。全て削除してからキャンセルして下さい。
            // check flat is_ad_invoice_issue = 1 mean processinng => message error: 発行済みの請求書を破棄し、売上伝票の明細を全て削除して下さい。
            if ($order_detail_row = $this->order_detail_model->get_order_detail_by_order_id_branch_cd($branch->order_id, $branch->branch)) {
                if ($order_detail_row->is_ad_invoice_issue == 0 && $order_detail_row->is_ad_sales_slip_input == 1) {
                    $result->errors .= "<p class='error-item'>入力済みの前金売上伝票が残っています。全て削除してからキャンセルして下さい。</p>";
                }
                else if ($order_detail_row->is_ad_invoice_issue == 1) {
                    $result->errors .= "<p class='error-item'>発行済みの請求書を破棄し、売上伝票の明細を全て削除して下さい。</p>";
                } else {
                    $result->errors .= "<p class='error-item'>入力済みの売上明細があります。全て削除してから変レポして下さい。</p>";
                }                
            }
        }
//#9700:End
//#6900:End
        return $result;
    }

    /**
     * Update order details
     *
     * @param array $params
     * @return bool
     * @author hoang_minh
     * @since 2015-07-17
     */
    public function update_order_detail($params = array()) {
        if ( ! is_array($params) || empty($params)) {
            return false;
        }

        return $this->update('`order_detail`', $params['update_data'], $params['update_conditions']);
    }

    /**
     * Update multiple order details
     *
     * @param array $params
     * @return bool
     * @author hoang_minh
     * @since 2015-08-17
     */
    public function update_multiple_order_details($params = array()) {
        if ( ! is_array($params) || empty($params)
             || ! isset($params['update_data']) || empty($params['update_data'])
             || ! isset($params['update_conditions']) || empty($params['update_conditions'])) {

            return false;
        }

        $update_data = array();
        foreach ($params['update_data'] as $key => $val) {
            $update_data[] = $key . "='" . mysql_escape_string($val) . "'";
        }
        $update_data = implode(' , ', $update_data);

        $update_conditions = array();
        $update_condition = '';
        foreach ($params['update_conditions'] as $key => $val) {
            switch ($key) {
                case 'order_id':
                    $update_conditions[] = "order_id IN ('" . $val . "')";
                    break;

                default:
                    $update_conditions[] = $key . "='" . mysql_escape_string($val) . "'";
                    break;
            }
        }

        $update_condition = implode(' AND ', $update_conditions);

        $sql = "UPDATE "
             .   "order_detail "
             . "SET " . $update_data . " "
             . "WHERE " . $update_condition . " ";

        return $this->db->query($sql);
    }

    /**
     * Update order detail status upon flag changed
     *
     * @param array $order_input - Accept 2 type of format:
     * 1/     array('j_code' => $j_code, 'branch_cd' => $branch_cd)
     * 2/     array(
     *            array('j_code' => $j_code1, 'branch_cd' => $branch_cd1),
     *            array('j_code' => $j_code2, 'branch_cd' => $branch_cd2),
     *            array('j_code' => $j_code3, 'branch_cd' => $branch_cd3)
     *        )
     * @return bool|int - Number of record updated or FALSE when fail
     * @author quang_duc
     */
    public function update_status_by_flag($order_input)
    {
        if (!is_array($order_input)) {
            return false;
        }
        if (isset($order_input['j_code']) && isset($order_input['branch_cd'])) {
            $order_input = array(
                array(
                    'j_code'    => $order_input['j_code'],
                    'branch_cd' => $order_input['branch_cd']
                )
            );
        }

        $update_order = array();
        foreach ($order_input as $input) {
            if (!isset($input['j_code']) || !isset($input['branch_cd'])) {
                log_message('error', __FILE__ . '(line ' . __LINE__ . '): $order_input format is invalid');
                return false;
            }

            $order_detail = $this->db->select('od.id AS od_id, j_code, branch_cd, o.is_ad_receive_payment, od.order_status, is_ad_sales_slip_input, is_ad_invoice_issue, is_order_input, is_acceptance_input, is_sales_slip_input, is_invoice_issue')
                ->from('order o')
                ->join('order_detail od', 'o.id = od.order_id AND od.disable = 0')
                ->where('o.j_code', $input['j_code'])
                ->where('od.branch_cd', $input['branch_cd'])
                ->where('o.disable = 0')
                ->get()->result_array();

            if (count($order_detail) == 0) {
                log_message('error', __FILE__ . '(line ' . __LINE__ . '): j_code + branch_cd ' . $input['j_code'] . $input['branch_cd'] . ' not found.');
                return false;
            }

            $adv_payment_check = true;
            if ($order_detail[0]['is_ad_receive_payment'] == 1) {
                if ($order_detail[0]['is_ad_sales_slip_input'] == 0 || $order_detail[0]['is_ad_invoice_issue'] == 0) {
                    $adv_payment_check = false;
                }
            }

            if ($adv_payment_check && $order_detail[0]['is_order_input'] == 1 && $order_detail[0]['is_acceptance_input'] == 1 && $order_detail[0]['is_sales_slip_input'] == 1 && $order_detail[0]['is_invoice_issue'] == 1) {
                if ($order_detail[0]['order_status'] == ORDER_STATUS_UNDETERMINED) {
                    $update_order[] = array(
                        'id' => $order_detail[0]['od_id'],
                        'order_status' => ORDER_STATUS_CONFIRMED,
                        'lastup_account_id' => $this->auth->get_account_id(),
                        'lastup_datetime' => $this->_db_now()
                    );
                }
            } else {
                if ($order_detail[0]['order_status'] == ORDER_STATUS_CONFIRMED) {
                    $update_order[] = array(
                        'id' => $order_detail[0]['od_id'],
                        'order_status' => ORDER_STATUS_UNDETERMINED,
                        'lastup_account_id' => $this->auth->get_account_id(),
                        'lastup_datetime' => $this->_db_now()
                    );
                }
            }
        }

        if (!empty($update_order)) {
            return $this->db->update_batch('order_detail', $update_order, 'id');
        }

        return true;
    }
//7910:Start
    public function get_order_detail_info_by_orderid_branchcd($orderid, $branchcd) {
    	$query = " SELECT * "
    			." FROM order_detail "
    			." WHERE order_id = '$orderid' AND branch_cd = '$branchcd' ";
    	return $this->exec_query ( $query );
    }
//7910:End
//#9700:Start
    public function get_order_detail_by_order_id_branch_cd($order_id, $branch_cd)
    {
        $order_detail_row = $this->db->select()
            ->from('order_detail')
            ->where('order_id', $order_id)
            ->where('branch_cd', $branch_cd)
            ->where('disable', STATUS_ENABLE)
            ->get()->row();
        return $order_detail_row;
    }
//#9700:End

}