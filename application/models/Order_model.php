<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends ZR_Model {

    public $list_order_status;
    public $list_account_follow_bussiness_unit;
    public $my_account;

    const UNAPPROVED = 1;
    const UNDETERMINED = 2;
    const CONFIRMED = 3;
    const REJECTED = 4;
    const CANCELLED = 9;

    const GUI_PART_ORDER_STATUS = 9;
    const GUI_PART_CLOSING_DATE = 2;
    const GUI_PART_PAYMENT_MONTH = 3;
    const GUI_PART_PAYMENT_DATE = 4;

    public function __construct() {
        parent::__construct();
    }

    public function get_rule($rule_name) {
        $validate_rule['insert'] = array(
                array(
                        'field' => 'client_id',
                        'rules' => 'required|integer',
                        'errors' => array(
                                'required' => 'クライアントを選択してください'
                        )
                ),
                array(
                        'field' => 'zipcode',
                        'label' => lang('lbl_zipcode'),
                        'rules' => 'trim|required|validate_zipcode_department|min_length[7]|max_length[7]',
                                'errors' => array(
//#8537:Start
                                    // 'min_length' => lang('msg_digit_not_enough'),
                                    // 'max_length' => lang('msg_enter_7_digit_')
                                    'min_length' => str_replace('%field%', lang('lbl_zipcode'), lang('msg_digit_not_enough')),
                                    'max_length' => str_replace('%field%', lang('lbl_zipcode'), lang('msg_enter_7_digit_')),
//#8537:End
                                )
                ),
                array(
                        'field' => 'address_1st',
                        'label' => lang('lbl_address') . "1",
//#8537:Start
//                        'rules' => 'required',
                        'rules' => 'required|only_2byte',
                ),
                array(
                        'field' => 'address_2nd',
                        'label' => lang('lbl_address') . '2',
                        'rules' => 'only_2byte'
                ),
                array(
                        'field' => 'division_name',
                        'label' => lang('lbl_devision_name'),
                        'rules' => 'only_2byte'
                ),
//#8537:End
                array(
                        'field' => 'phone_no',
                        'label' => lang('lbl_phone'),
                        'rules' => 'trim|required|phone[{2}-{4}-{4}]',
                        'errors' => array(
                            'phone' => str_replace('%field%', lang('lbl_phone'), lang('msg_enter_hyphen_and_num'))
                        )
                ),
                array(
                        'field' => 'fax_no',
                        'label' => lang('lbl_fax'),
                        'rules' => 'trim|phone[{2}-{4}-{4}]',
                        'errors' => array(
                                'phone' => str_replace('%field%', lang('lbl_fax'), lang('msg_enter_hyphen_and_num'))
                        )
                ),
                array(
                        'field' => 'charge_name',
                        'label' => lang('lbl_charge_name'),
//#8537:Start
//                        'rules' => 'required',
                        'rules' => 'required|only_2byte',
//#8537:End
                ),
                array(
                        'field' => 'is_ad_receive_payment',
                        'label' => lang('lbl_is_ad_receive_payment'),
                        'rules' => 'required|in_list[0,1]',
                ),
                array(
                        'field' => 'j_account_id',
                        'label' => lang('lbl_account_id'),
                        'rules' => 'required|integer',
                ),
                array(
                        'field' => 'remark',
                        'label' => lang('lbl_remark'),
                        'rules' => 'required',
                )/* ,
                array(
                        'field' => 'total_amount',
                        'label' => lang('lbl_amount'),
                        'rules' => 'required|numeric',
                ),
                array(
                        'field' => 'total_amount_tax',
                        'label' => lang('lbl_tax_total'),
                        'rules' => 'required|numeric',
                ), */
        );
        $validate_rule['update'] = array(
                array(
                        'field' => 'id',
                        'rules' => 'required|integer'
                ),
                array(
                        'field' => 'zipcode',
                        'label' => lang('lbl_zipcode'),
                        'rules' => 'trim|required|validate_zipcode_department|min_length[7]|max_length[7]',
                                'errors' => array(
//#8537:Start
                                    // 'min_length' => lang('msg_digit_not_enough'),
                                    // 'max_length' => lang('msg_enter_7_digit_')
                                    'min_length' => str_replace('%field%', lang('lbl_zipcode'), lang('msg_digit_not_enough')),
                                    'max_length' => str_replace('%field%', lang('lbl_zipcode'), lang('msg_enter_7_digit_')),
//#8537:End
                                )
                ),
                array(
                        'field' => 'address_1st',
                        'label' => lang('lbl_address') . "1",
//#8537:Start
//                        'rules' => 'required',
                        'rules' => 'required|only_2byte',
                ),
                array(
                        'field' => 'address_2nd',
                        'label' => lang('lbl_address') . '2',
                        'rules' => 'only_2byte'
                ),
                array(
                        'field' => 'division_name',
                        'label' => lang('lbl_devision_name'),
                        'rules' => 'only_2byte'
                ),
//#8537:End
                array(
                        'field' => 'phone_no',
                        'label' => lang('lbl_phone'),
                        'rules' => 'trim|required|phone[{2}-{4}-{4}]',
                        'errors' => array(
                            'phone' => str_replace('%field%', lang('lbl_phone'), lang('msg_enter_hyphen_and_num'))
                        )
                ),
                array(
                        'field' => 'fax_no',
                        'label' => lang('lbl_fax'),
                        'rules' => 'trim|phone[{2}-{4}-{4}]',
                        'errors' => array(
                            'phone' => str_replace('%field%', lang('lbl_fax'), lang('msg_enter_hyphen_and_num'))
                        )
                ),
                array(
                        'field' => 'charge_name',
                        'label' => lang('lbl_charge_name'),
//#8537:Start
//                        'rules' => 'required',
                        'rules' => 'required|only_2byte',
//#8537:End
                ),
                array(
                        'field' => 'is_ad_receive_payment',
                        'label' => lang('lbl_is_ad_receive_payment'),
                        'rules' => 'required|in_list[0,1]',
                ),
                array(
                        'field' => 'j_account_id',
                        'label' => lang('lbl_account_id'),
                        'rules' => 'required|integer',
                ),
                array(
                        'field' => 'remark',
                        'label' => lang('lbl_remark'),
                        'rules' => 'required',
                ),
                /*array(
                        'field' => 'comment',
                        'label' => lang('lbl_comment_change_report'),
                        'rules' => 'required',
                ),
                array(
                        'field' => 'total_amount',
                        'label' => lang('lbl_amount'),
                        'rules' => 'required|numeric',
                ),
                array(
                        'field' => 'total_amount_tax',
                        'label' => lang('lbl_tax_total'),
                        'rules' => 'required|numeric',
                ),*/
        );
        if (isset ( $validate_rule [$rule_name] )) {
            return $validate_rule [$rule_name];
        }
        return array ();
    }

    public function get_products_by_bu_id($bu_id) {
        $query = $this->db->select('id, name')
            ->from('product')
            ->where('business_unit_id', $bu_id)
            ->where('is_no_add', 0)
            ->where('disable', 0)
            ->get();
        return $query->result();
    }

    public function get_j_account_by_bu_id($bu_id) {
        $query = $this->db->select('a.id, a.name, bu.template, bu.j_code')
                ->from('business_unit bu')
                ->join('department d', 'd.business_unit_id = bu.id')
                ->join('account a', 'a.department_id = d.id')
                ->where('bu.id', $bu_id)
                ->where('a.resignation', 0)
                ->where('a.disable', 0)
                ->get();
        return $query->result();
    }

    public function set_account_follow_bussiness_unit() {
        $query = $this->db->select('a.id, a.name, bu.id as bu_id')
                ->from('business_unit bu')
                ->join('department d', 'd.business_unit_id = bu.id')
                ->join('account a', 'a.department_id = d.id')
                ->where(array('bu.disable' => 0, 'a.disable' => 0, 'a.resignation' => 0))
//#7525:Start
                ->order_by('a.id')
//#7525:End
                ->get();
        $this->list_account_follow_bussiness_unit = json_encode($query->result());

        $this->load->library('auth');
        $auth_data = $this->auth->get_auth_data();
        $query = $this->db->select('a.id, a.name, bu.j_code, bu.id AS bu_id, bu.is_product_division,bu.finish_work_report_template')
                ->from('business_unit bu')
                ->join('department d', 'd.business_unit_id = bu.id')
                ->join('account a', 'a.department_id = d.id')
                ->where('a.id', $auth_data['account_id'])
                ->get();
        $result = $query->result();
        $this->my_account = json_encode($result[0]);
    }

    public function set_order_status() {
        $query = $this->db->select('gpe.code, gpe.name')
                ->from('gui_parts_element gpe')
                ->join('gui_parts gp', 'gp.id = gpe.gui_parts_id')
                ->where('gpe.disable', '0')
                ->where('gp.disable', '0')
                ->where('gp.column_name', 'order_status')
                ->get();

        $this->list_order_status = $query->result();
    }

    public function search_order($logged_user_bu, $order_status = '') {

        $this->db->select('o.id order_id, o.j_code o_j_code, od.branch_cd od_branch_cd, o.j_account_id as o_account_id, c.id as c_id, c.s_code c_s_code, c.name as c_name,
                        o.is_ad_receive_payment o_is_ad_receive_payment, od.is_order_input o_is_order_input, od.is_acceptance_input o_is_acceptance_input, od.is_sales_slip_input o_is_sales_slip_input,
                        od.is_invoice_issue o_is_invoice_issue, od.order_status od_order_status, p.name p_name, od.contents od_contents, od.delivery_date od_delivery_date,
                        od.billing_date od_billing_date, od.payment_date od_payment_date, od.quantity od_quantity, od.tax_type od_tax_type, od.unit_price od_unit_price,
                        od.amount od_amount, od.cost_unit_price od_cost_unit_price, od.cost_total_price od_cost_total_price, od.gross_profit_amount od_gross_profit_amount,
                        od.gross_profit_rate od_gross_profit_rate, od.is_change_report_apply od_is_change_report_apply, a.name a_name, od.is_ad_sales_slip_input o_is_ad_sales_slip_input, od.is_ad_invoice_issue o_is_ad_invoice_issue, bu.id bu_id')
                ->from('order o')
                ->join('order_detail od', 'od.order_id = o.id')
                ->join('client c', 'c.id = o.client_id')
                ->join('product p', 'p.id = od.product_id')
                ->join('account a', 'o.j_account_id = a.id')
                ->join('department d', 'a.department_id = d.id')
                ->join('business_unit bu', 'd.business_unit_id = bu.id')
                // ->where('od.order_status', 2)
                ->where('o.disable', '0')
                ->where('od.disable', '0')
                ->where('c.disable', '0')
                ->order_by('o.j_code, od.branch_cd');
//#9748:Start
        $this->db->select('bu.finish_work_report_template');
//#9748:End
        if ($logged_user_bu) {
            $this->db->where('a.id', $logged_user_bu);
        }
        if ($order_status) {
            $this->db->where('od.order_status', $order_status);
        }
        if (!$input = $this->input->raw_input_stream) {
            return $this->db->get()->result();
        }

        $input = json_decode($input);
        $order = $input->order;

        if (isset($order->order_status)) {
            $this->db->where_in('od.order_status', $order->order_status);
        }
        if (isset($order->is_ad_sales_slip_input)) {
            $this->db->where_in('od.is_ad_sales_slip_input', $order->is_ad_sales_slip_input);
        }
        if (isset($order->is_ad_invoice_issue)) {
            $this->db->where_in('od.is_ad_invoice_issue', $order->is_ad_invoice_issue);
        }
        if (isset($order->is_order_input)) {
            $this->db->where_in('od.is_order_input', $order->is_order_input);
        }
        if (isset($order->is_acceptance_input)) {
            $this->db->where_in('od.is_acceptance_input', $order->is_acceptance_input);
        }
        if (isset($order->is_sales_slip_input)) {
            $this->db->where_in('od.is_sales_slip_input', $order->is_sales_slip_input);
        }
        // echo $this->db->get_compiled_select();exit;
        if (isset($order->is_invoice_issue)) {
            $this->db->where_in('od.is_invoice_issue', $order->is_invoice_issue);
        }
        if (isset($order->is_ad_receive_payment)) {
            $this->db->where('o.is_ad_receive_payment', $order->is_ad_receive_payment);
        }
        if (isset($order->is_change_report_apply)) {
            $this->db->where('od.is_change_report_apply', $order->is_change_report_apply);
        }
        if (isset($order->j_account) && $order->j_account != '0') {
            $this->db->where('o.j_account_id', $order->j_account);
        } elseif (isset($order->j_business_unit) && $order->j_business_unit != '0') {
            $this->db->join('department jd', 'a.department_id = jd.id')
                ->where('jd.business_unit_id', $order->j_business_unit);
        }
        if (isset($order->client_name)) {
            $this->db->like('c.name', $order->client_name);
        }
        $from = (isset($order->j_code_branch_cd_from))? ($order->j_code_branch_cd_from) : "";
        $to = (isset($order->j_code_branch_cd_to))? ($order->j_code_branch_cd_to) : "";

        $this->jcode_condition($from, $to);

        if (isset($order->delivery_date_from) && $order->delivery_date_from) {
            $date_have_only_2_part = false;
            $date_from = '';
            $order->delivery_date_from = parse_date($order->delivery_date_from);

            if (count(explode('-', $order->delivery_date_from)) == 2) {
                $date_from = $order->delivery_date_from;
                $order->delivery_date_from = $order->delivery_date_from . '-01';
                $date_have_only_2_part = true;
            }

            if (isset($order->delivery_date_to) && $order->delivery_date_to) {
                $order->delivery_date_to = parse_date($order->delivery_date_to);

                if (count(explode('-', $order->delivery_date_to)) == 2) {
                    $order->delivery_date_to = $order->delivery_date_to . '-31';
                }

                $this->db->where('od.delivery_date>=', $order->delivery_date_from);
                $this->db->where('od.delivery_date<=', $order->delivery_date_to);
            } else {
                if ($date_have_only_2_part) {
                    $this->db->where('od.delivery_date>=', $order->delivery_date_from);
                    $this->db->where('od.delivery_date<=', $date_from . '-31');
                } else {
                    $this->db->where('od.delivery_date=', $order->delivery_date_from);
                }
            }
        }
        if (isset($order->order_regist_date_from) && $order->order_regist_date_from) {
            $date_have_only_2_part = false;
            $date_from = '';

            $order->order_regist_date_from = parse_date($order->order_regist_date_from);

            if (count(explode('-', $order->order_regist_date_from)) == 2) {
                $date_from = $order->order_regist_date_from;
                $order->order_regist_date_from = $order->order_regist_date_from . '-01';
                $date_have_only_2_part = true;
            }

            if (isset($order->order_regist_date_to) && $order->order_regist_date_to) {
                $order->order_regist_date_to = parse_date($order->order_regist_date_to);

                if (count(explode('-', $order->order_regist_date_to)) == 2) {
                    $order->order_regist_date_to = $order->order_regist_date_to . '-31';
                }

                $this->db->where('o.order_regist_date >=', $order->order_regist_date_from);
                $this->db->where('o.order_regist_date <=', $order->order_regist_date_to);
            } else {
                if ($date_have_only_2_part) {
                    $this->db->where('o.order_regist_date >=', $order->order_regist_date_from);
                    $this->db->where('o.order_regist_date <=', $date_from . '-31');
                } else {
                    $this->db->where('o.order_regist_date =', $order->order_regist_date_from);
                }
            }
        }

        if (isset($order->approval_date_from) && $order->approval_date_from) {
            $date_have_only_2_part = false;
            $date_from = '';

            $order->approval_date_from = parse_date($order->approval_date_from);

            if (count(explode('-', $order->approval_date_from)) == 2) {
                $date_from = $order->approval_date_from;
                $order->approval_date_from = $order->approval_date_from . '-01';
                $date_have_only_2_part = true;
            }

            if (isset($order->approval_date_to) && $order->approval_date_to) {
                $order->approval_date_to = parse_date($order->approval_date_to);

                if (count(explode('-', $order->approval_date_to)) == 2) {
                    $order->approval_date_to = $order->approval_date_to . '-31';
                }

                $this->db->where('o.order_approval_date >=', $order->approval_date_from);
                $this->db->where('o.order_approval_date <=', $order->approval_date_to);
            } else {
                if ($date_have_only_2_part) {
                    $this->db->where('o.order_approval_date >=', $order->approval_date_from);
                    $this->db->where('o.order_approval_date <=', $date_from . '-31');
                } else {
                    $this->db->where('o.order_approval_date =', $order->approval_date_from);
                }
            }
        }

        if (isset($order->payment_date_from) && $order->payment_date_from) {
            $date_have_only_2_part = false;
            $date_from = '';

            $order->payment_date_from = parse_date($order->payment_date_from);

            if (count(explode('-', $order->payment_date_from)) == 2) {
                $date_from = $order->payment_date_from;
                $order->payment_date_from = $order->payment_date_from . '-01';
                $date_have_only_2_part = true;
            }

            if (isset($order->payment_date_to) && $order->payment_date_to) {
                $order->payment_date_to = parse_date($order->payment_date_to);

                if (count(explode('-', $order->payment_date_to)) == 2) {
                    $order->payment_date_to = $order->payment_date_to . '-31';
                }

                $this->db->where('od.payment_date >=', $order->payment_date_from);
                $this->db->where('od.payment_date <=', $order->payment_date_to);
            } else {
                if ($date_have_only_2_part) {
                    $this->db->where('od.payment_date >=', $order->payment_date_from);
                    $this->db->where('od.payment_date <=', $date_from . '-31');
                } else {
                    $this->db->where('od.payment_date =', $order->payment_date_from);
                }
            }
        }

        if (isset($order->s_code)) {
            $this->db->like('c.s_code', $order->s_code, 'after');
        }
        if (isset($order->client_name_furigana)) {
            $this->db->like('c.name_kana', $order->client_name_furigana);
        }
        if (isset($order->client_department_name)) {
            $this->db->like('c.division_name', $order->client_department_name);
        }
        if (isset($order->s_account) && $order->s_account != '0') {
            $this->db->group_start()
                        ->where('c.s_account_id_1st', $order->s_account)
                        ->or_where('c.s_account_id_2nd', $order->s_account)
                    ->group_end();
        } elseif (isset($order->s_business_unit) && $order->s_business_unit != '0') {
            $this->db->join('account ac', 'c.s_account_id_1st = ac.id OR c.s_account_id_2nd = ac.id')
                ->join('department dc', 'ac.department_id = dc.id')
                ->where('dc.business_unit_id', $order->s_business_unit)
                ->distinct('od.id');
        }

        return $this->db->get()->result();
    }

    public function jcode_condition($from, $to) {


//#7067:Start
     	$data['from_jcode_branch'] = $from;
     	$data['to_jcode_branch'] = $to;

    	$wheresql_jocde_branch = $this->get_where_sql_by_branchcode_and_jcode($data,'o.j_code','od.branch_cd');
    	if(!empty($wheresql_jocde_branch)){
    		$this->db->where($wheresql_jocde_branch);
    	}

//         if (strlen($from) > 0 && strlen($to) == 0) {

//             $n_form_brandcode =  strlen($from);

//             if ($n_form_brandcode <= 8) {
//                 $this->db->where('o.j_code LIKE "' . trim($from.'%"'));
//             } else {
//                 $varjcode  = substr($from, 0, 8);
//                 $varbranchcode = str_replace($varjcode, "", $from);
//                 $this->db->where('o.j_code LIKE "' . trim($varjcode.'%"'));
//                 $this->db->where('od.branch_cd LIKE"' . trim($varbranchcode . '%"'));
//             }
//         } elseif (strlen($from) == 0 && strlen($to) > 0) {

//         } else {
//             $n_form_brandcode = (strlen($from) > 0)? (strlen($from)) : 0;
//             $n_tohjodebranch =  (strlen($to) > 0)? (strlen($to)) : 0;

//             if ($n_form_brandcode > 0 ) {
//                 if ($n_form_brandcode <= 8 ) {
//                     $this->db->where('if (CONCAT(o.j_code) <> "", CONCAT(o.j_code), "00000000") >=', str_pad($from, 8, "0", STR_PAD_RIGHT));
//                 }
//                 else {
//                     $varjcode  = substr($from, 0, 8);
//                     $varbranchcode = str_replace($varjcode, "", $from);
//                     $this->db->where('if (CONCAT(o.j_code) <> "", CONCAT(o.j_code), "00000000") >=', str_pad($varjcode, 8, "0", STR_PAD_RIGHT));
//                     $this->db->where('if (CONCAT(od.branch_cd) <> "", CONCAT(od.branch_cd), "00") >=', str_pad($varbranchcode, 2, "0", STR_PAD_RIGHT));
//                 }
//             }

//             if ($n_tohjodebranch) {
//                 if ($n_tohjodebranch <= 8) {

//                     for ($k = $n_tohjodebranch ; $k <= 8; $k++){
//                         $to .= "9";
//                     }
//                     $this->db->where('if (CONCAT(o.j_code) <> "", CONCAT(o.j_code), "00000000") <=', str_pad($to, 8, "0", STR_PAD_RIGHT));

//                 } else {

//                     $varjcode  = substr($to, 0, 8);
//                     $varbranchcode = str_replace($varjcode, "", $to);
//                     $this->db->where('if (CONCAT(o.j_code) <> "", CONCAT(o.j_code), "00000000") <=', str_pad($varjcode,8,"0", STR_PAD_RIGHT));
//                     $this->db->where('if (CONCAT(od.branch_cd) <> "", CONCAT(od.branch_cd), "00") <=', str_pad($varbranchcode, 2, "0", STR_PAD_RIGHT));

//                 }
//             }

//         }

//#7067:End

    }

    public function export_order() {
        /*
 o.id order_id,
                o.j_code o_j_code,
                od.branch_cd od_branch_cd,
                o.j_account_id as o_account_id,
                c.s_code c_s_code,
                c.name as c_name,
                o.is_ad_receive_payment o_is_ad_receive_payment,
                od.is_order_input o_is_order_input,
                od.is_acceptance_input o_is_acceptance_input,
                od.is_sales_slip_input o_is_sales_slip_input,
                od.is_invoice_issue o_is_invoice_issue,
                o.order_status o_order_status,
                p.name p_name,
                od.contents od_contents,
                od.delivery_date od_delivery_date,
                od.billing_date od_billing_date,
                od.payment_date od_payment_date,
                od.quantity od_quantity,
                od.tax_type od_tax_type,
                od.unit_price od_unit_price,
                od.amount od_amount,
                od.cost_unit_price od_cost_unit_price,
                od.cost_total_price od_cost_total_price,
                od.gross_profit_amount od_gross_profit_amount,
                od.gross_profit_rate od_gross_profit_rate,
                o.is_change_report_apply o_is_change_report_apply'

 * */

        $this->db->select('
                o.j_code,

                od.branch_cd,

                act.name,

                c.s_code,

                c.name as c_name,

                o.division_name,

                o.charge_name,'
//#8285:Start
        		.'o.is_ad_receive_payment,'
//#8285:End
               .'od.is_ad_sales_slip_input,

                od.is_ad_invoice_issue,

                od.is_order_input,

                od.is_acceptance_input,

                od.is_sales_slip_input,

                od.is_invoice_issue,

                od.order_status,

                p.name as p_name,

                od.contents,' .
//#7631:Start
               'o.order_approval_date,
                od.change_report_approval_date,' .

//#7631:End
               'od.delivery_date,

                od.billing_date,

                od.payment_date,

                od.quantity,

                od.tax_type,

                od.unit_price,

                od.amount,

                od.cost_unit_price,

                od.cost_total_price,

                od.gross_profit_amount,

                od.gross_profit_rate,

                od.is_change_report_apply

                '
        )
                            ->from('order o')
                            ->join('order_detail od', 'od.order_id = o.id')
                            ->join('client c', 'c.id = o.client_id')
                            ->join('product p', 'p.id = od.product_id')
                            ->join('account act', 'o.j_account_id = act.id', "left")
                            ->where('o.disable', '0')
                            ->where('od.disable', '0')
                            ->where('c.disable', '0')
                            ->order_by('o.j_code, od.branch_cd');

        if ($this->input->post('order_status')) {
            $order_status = array();
            foreach ($this->input->post('order_status') as $key => $value) {
                $order_status[] = $key;
            }
            $this->db->where_in('od.order_status', $order_status);
        }
        if ($this->input->post('is_ad_sales_slip_input')) {
            $this->db->where_in('od.is_ad_sales_slip_input', $this->input->post('is_ad_sales_slip_input'));
        }
        if ($this->input->post('is_ad_invoice_issue')) {
            $this->db->where_in('od.is_ad_invoice_issue', $this->input->post('is_ad_invoice_issue'));
        }
        if ($this->input->post('is_order_input')) {
            $this->db->where_in('od.is_order_input', $this->input->post('is_order_input'));
        }
        if ($this->input->post('is_acceptance_input')) {
            $this->db->where_in('od.is_acceptance_input', $this->input->post('is_acceptance_input'));
        }
        if ($this->input->post('is_sales_slip_input')) {
            $this->db->where_in('od.is_sales_slip_input', $this->input->post('is_sales_slip_input'));
        }
        if ($this->input->post('is_invoice_issue')) {
            $this->db->where_in('od.is_invoice_issue', $this->input->post('is_invoice_issue'));
        }
        if ($this->input->post('is_ad_receive_payment')) {
            $this->db->where('o.is_ad_receive_payment', $this->input->post('is_ad_receive_payment'));
        }
        if ($this->input->post('is_change_report_apply')) {
            $this->db->where('od.is_change_report_apply', $this->input->post('is_change_report_apply'));
        }
        if ($this->input->post('j_account') && $this->input->post('j_account') != '0') {
            $this->db->where('o.j_account_id', $this->input->post('j_account'));
        }
        if ($this->input->post('j_business_unit') && $this->input->post('j_business_unit') != 0) {
            $this->db->join('department jd', 'act.department_id = jd.id')
                ->where('jd.business_unit_id', $this->input->post('j_business_unit'));
        }

        if ($this->input->post('client_name')) {
            $this->db->like('c.name', $this->input->post('client_name'));
        }

        /*
        if ($this->input->post('j_code_branch_cd_from')) {
            $this->db->where('CONCAT(o.j_code, od.branch_cd) >', $this->input->post('j_code_branch_cd_from'));
        }
        if ($this->input->post('j_code_branch_cd_to')) {
            $this->db->where('CONCAT(o.j_code, od.branch_cd) <', $this->input->post('j_code_branch_cd_to'));
        }
        */
        if ($this->input->post('j_code_branch_cd_from'))
            $from = $this->input->post('j_code_branch_cd_from');
        else
            $from = "";

        if ($this->input->post('j_code_branch_cd_to'))
            $to = $this->input->post('j_code_branch_cd_to');
        else
            $to = "";

        $this->jcode_condition($from, $to);

        // Check delivery date
        if ($this->input->post('delivery_date_from')) {

            $date_have_only_2_part = false;
            $date_from = '';
            $delivery_date_from = '';
            $delivery_date_to = '';

            $delivery_date_from = parse_date($this->input->post('delivery_date_from'));
            // 2015/05 -> 2015/05/01
            if (count(explode('-', $delivery_date_from)) == 2) {
                $date_from = $delivery_date_from; // 2015/05
                $delivery_date_from = $delivery_date_from . '-01';
                $date_have_only_2_part = true;
            }
            // have input delivery_date_from && delivery_date_to, delivery_date_to = 2015/05 -> 2015/05/31
            if ($this->input->post('delivery_date_to')) {

                $delivery_date_to = parse_date($this->input->post('delivery_date_to'));
                if (count(explode('-', $delivery_date_to)) == 2) {
                    // $delivery_date_to =  $delivery_date_to . '-31';
                    $parts = $this->separate_string ( $delivery_date_to );
                    // int cal_days_in_month ( int $calendar , int $month , int $year )
                    $last_day_of_month = cal_days_in_month ( CAL_GREGORIAN, $parts [1], $parts [0] );
                    $delivery_date_to = $parts [0] . "-" . $parts [1] . "-" . $last_day_of_month;
                }
                // delivery_date_from (01) <= delivery_date <= delivery_date_to (31)
                $this->db->where('od.delivery_date >=', $delivery_date_from);
                $this->db->where('od.delivery_date <=', $delivery_date_to);
            } else { // not input delivery date to

                if ($date_have_only_2_part) { // 2015/05
                    $this->db->where('od.delivery_date >=', $delivery_date_from);
                    $parts = $this->separate_string ( $date_from );
                    $last_day_of_month = cal_days_in_month ( CAL_GREGORIAN, $parts [1], $parts [0] );
                    $date_from = $parts [0] . "-" . $parts [1] . "-" . $last_day_of_month;
                    $this->db->where('od.delivery_date <=', $date_from);

                } else {
                    $this->db->where('od.delivery_date =', $delivery_date_from);
                }
            }
        }

        //  Check regist date
        if ($this->input->post('order_regist_date_from')) {

            $date_have_only_2_part = false;
            $date_from = '';
            $order_regist_date_from = '';
            $order_regist_date_to = '';

            $order_regist_date_from = parse_date($this->input->post('order_regist_date_from'));
            // 2015/05 -> 2015/05/01
            if (count(explode('-', $order_regist_date_from)) == 2) {
                $date_from = $order_regist_date_from; // 2015/05
                $order_regist_date_from = $order_regist_date_from . '-01';
                $date_have_only_2_part = true;
            }
            // have input order_regist_date_from && order_regist_date_to, order_regist_date_to = 2015/05 -> 2015/05/31
            if ($this->input->post('order_regist_date_to')) {

                $order_regist_date_to = parse_date($this->input->post('order_regist_date_to'));
                if (count(explode('-', $order_regist_date_to)) == 2) {
                    // $order_regist_date_to =  $order_regist_date_to . '-31';
                    $parts = $this->separate_string ( $order_regist_date_to );
                    $last_day_of_month = cal_days_in_month ( CAL_GREGORIAN, $parts [1], $parts [0] );
                    $order_regist_date_to = $parts [0] . "-" . $parts [1] . "-" . $last_day_of_month;
                }
                // order_regist_date_from (01) <= order_regist_date <= order_regist_date_to (31)
                $this->db->where('o.order_regist_date >=', $order_regist_date_from);
                $this->db->where('o.order_regist_date <=', $order_regist_date_to);
            } else { // not input delivery date to

                if ($date_have_only_2_part) { // 2015/05
                    $this->db->where('o.order_regist_date >=', $order_regist_date_from);
                    $parts = $this->separate_string ( $date_from );
                    $last_day_of_month = cal_days_in_month ( CAL_GREGORIAN, $parts [1], $parts [0] );
                    $date_from = $parts [0] . "-" . $parts [1] . "-" . $last_day_of_month;
                    $this->db->where('o.order_regist_date <=', $date_from);
                } else {
                    $this->db->where('o.order_regist_date =', $order_regist_date_from);
                }
            }
        }

        // Check approve date
        if ($this->input->post('approval_date_from')) {

            $date_have_only_2_part = false;
            $date_from = '';
            $approval_date_from = '';
            $approval_date_to = '';

            $approval_date_from = parse_date($this->input->post('approval_date_from'));
            // 2015/05 -> 2015/05/01
            if (count(explode('-', $approval_date_from)) == 2) {
                $date_from = $approval_date_from; // 2015/05
                $approval_date_from = $approval_date_from . '-01';
                $date_have_only_2_part = true;
            }
            // have input approval_date_from && approval_date_to, approval_date_to = 2015/05 -> 2015/05/31
            if ($this->input->post('approval_date_to')) {

                $approval_date_to = parse_date($this->input->post('approval_date_to'));
                if (count(explode('-', $approval_date_to)) == 2) {
                    // $approval_date_to =  $approval_date_to . '-31';
                    $parts = $this->separate_string ( $approval_date_to );
                    $last_day_of_month = cal_days_in_month ( CAL_GREGORIAN, $parts [1], $parts [0] );
                    $approval_date_to = $parts [0] . "-" . $parts [1] . "-" . $last_day_of_month;
                }
                // approval_date_from (01) <= order_approval_date <= approval_date_to (31)
                $this->db->where('o.order_approval_date >=', $approval_date_from);
                $this->db->where('o.order_approval_date <=', $approval_date_to);
            } else { // not input delivery date to

                if ($date_have_only_2_part) { // 2015/05
                    $this->db->where('o.order_approval_date >=', $approval_date_from);
                    $parts = $this->separate_string ( $date_from );
                    $last_day_of_month = cal_days_in_month ( CAL_GREGORIAN, $parts [1], $parts [0] );
                    $date_from = $parts [0] . "-" . $parts [1] . "-" . $last_day_of_month;
                    $this->db->where('o.order_approval_date <=', $date_from);
                } else {
                    $this->db->where('o.order_approval_date =', $approval_date_from);
                }
            }
        }

        // Check payment date
        if ($this->input->post('payment_date_from')) {

            $date_have_only_2_part = false;
            $date_from = '';
            $payment_date_from = '';
            $approval_date_to = '';

            $payment_date_from = parse_date($this->input->post('payment_date_from'));
            // 2015/05 -> 2015/05/01
            if (count(explode('-', $payment_date_from)) == 2) {
                $date_from = $payment_date_from; // 2015/05
                $payment_date_from = $payment_date_from . '-01';
                $date_have_only_2_part = true;
            }
            // have input payment_date_from && approval_date_to, approval_date_to = 2015/05 -> 2015/05/31
            if ($this->input->post('payment_date_to')) {

                $payment_date_to = parse_date($this->input->post('payment_date_to'));
                if (count(explode('-', $payment_date_to)) == 2) {
                    // $payment_date_to =  $payment_date_to . '-31';
                    $parts = $this->separate_string ( $payment_date_to );
                    $last_day_of_month = cal_days_in_month ( CAL_GREGORIAN, $parts [1], $parts [0] );
                    $payment_date_to = $parts [0] . "-" . $parts [1] . "-" . $last_day_of_month;
                }
                // payment_date_from (01) <= payment_date <= payment_date_to (31)
                $this->db->where('od.payment_date >=', $payment_date_from);
                $this->db->where('od.payment_date <=', $payment_date_to);
            } else { // not input delivery date to

                if ($date_have_only_2_part) { // 2015/05
                    $this->db->where('od.payment_date >=', $payment_date_from);
                    $parts = $this->separate_string ( $date_from );
                    $last_day_of_month = cal_days_in_month ( CAL_GREGORIAN, $parts [1], $parts [0] );
                    $date_from = $parts [0] . "-" . $parts [1] . "-" . $last_day_of_month;
                    $this->db->where('od.payment_date <=', $date_from);
                } else {
                    $this->db->where('od.payment_date =', $payment_date_from);
                }
            }
        }

        if ($this->input->post('s_code')) {
            $this->db->like('c.s_code', $this->input->post('s_code'), 'after');
        }
        if ($this->input->post('client_name_furigana')) {
            $this->db->like('c.name_kana', $this->input->post('client_name_furigana'));
        }
        if ($this->input->post('client_department_name')) {
            $this->db->like('c.division_name', $this->input->post('client_department_name'));
        }
        if ($this->input->post('s_account') && $this->input->post('s_account') != '0') {
            $this->db->group_start()
                ->where('c.s_account_id_1st', $this->input->post('s_account'))
                ->or_where('c.s_account_id_2nd', $this->input->post('s_account'))
            ->group_end();
        } elseif ($this->input->post('s_business_unit') && $this->input->post('s_business_unit') != '0') {
            $this->db->join('account ac', 'c.s_account_id_1st = ac.id OR c.s_account_id_2nd = ac.id')
                ->join('department d', 'ac.department_id = d.id')
                ->where('d.business_unit_id', $this->input->post('s_business_unit'))
                ->distinct('od.id');
        }

        $list = $this->db->get()->result_array();
        // echo "<pre>";
        // print_r($list);
        // Add more data before export file csv
        $list_order_status = $this->get_all_order_status();
        $list_tax_division = $this->get_all_tax_division();
        // $tax_rate = $this->get_tax_rates();
        // $list_sales_slip_detail_amount = $this->get_sales_slip_detail($list);
        // $list_order_acceptance_amount = $this->get_order_acceptance_amount($list);

        foreach ($list as &$fields) {
            $approved = false;
            if($fields['order_status']==3) {
                $approved = true;
            }

            if($fields['is_change_report_apply']==1) {
                $fields['order_status'] = "変レポ申請中";
            } else {
                foreach($list_order_status as $lo) {
                    if($fields['order_status']==$lo['code']) {
                        $fields['order_status'] = $lo['name'];
                    }
                }
            }
            unset($fields['is_change_report_apply']);

            foreach($list_tax_division as $lt) {
                if($fields['tax_type']==$lt['code']) {
                    $fields['tax_type'] = $lt['name'];
                }
            }

//#8285:Start

//             $fields['is_ad_sales_slip_input']   = $fields['is_ad_sales_slip_input'] ? '済' :'未';
//             $fields['is_ad_invoice_issue']      = $fields['is_ad_invoice_issue'] ? '済' :'未';

			if($fields['is_ad_receive_payment'] == 1){
				$fields['is_ad_sales_slip_input']   = $fields['is_ad_sales_slip_input'] ? '済' :'未';
				$fields['is_ad_invoice_issue']      = $fields['is_ad_invoice_issue'] ? '済' :'未';
			}
			else{
				$fields['is_ad_sales_slip_input']   = $fields['is_ad_sales_slip_input'] ? '済' :'-';
				$fields['is_ad_invoice_issue']      = $fields['is_ad_invoice_issue'] ? '済' :'-';
			}
//#8285:End

            $fields['is_order_input']           = $fields['is_order_input'] ? '済' :'未';
            $fields['is_acceptance_input']      = $fields['is_acceptance_input'] ? '済' :'未';
            $fields['is_sales_slip_input']      = $fields['is_sales_slip_input'] ? '済' :'未';
            $fields['is_invoice_issue']         = $fields['is_invoice_issue'] ? '済' :'未';
            $fields['branch_cd']                = strlen($fields['branch_cd'])==1 ? "'0".$fields['branch_cd'] : $fields['branch_cd'];
            $fields['unit_price']               = number_format($fields['unit_price'],2);
            $fields['quantity']                 = number_format($fields['quantity']);

            // $fields['sales_slip_amount']        = 0;
            // If this order_detail has issued invoice
            /* if($fields['is_invoice_issue']=='済' && !empty($list_sales_slip_detail_amount)) {
                foreach($list_sales_slip_detail_amount AS $lssd) {
                    if(($fields['j_code'] == $lssd['j_code']) && ($fields['branch_cd'] == $lssd['branch_cd'])) {
                        $fields['sales_slip_amount'] += $lssd['amount'];
                    }
                }
            } */
            // Get sales slip amount
            $fields['sales_slip_amount'] = $this->get_amount_from_sales_slip_detail_by_jcode_branch_cd($fields['j_code'],$fields['branch_cd']);
            $fields['sales_slip_amount'] = ($fields['sales_slip_amount'] == "") ? 0 : $fields['sales_slip_amount'];

            // $fields['order_acceptance_amount'] = 0;
            // If this order_detail has been approved
            /* if($approved && !empty($list_order_acceptance_amount)) {
                foreach($list_order_acceptance_amount AS $loao) {
                    if(($fields['j_code'] == $loao['j_code']) && ($fields['branch_cd'] == $loao['branch_cd'])) {
                        $fields['order_acceptance_amount'] += $loao['amount'];
                    }
                }
            }*/
            // Get order acceptance amount
            $fields['order_acceptance_amount'] = $this->get_amount_from_order_acceptance_by_jcode_branch_cd($fields['j_code'],$fields['branch_cd']);
            $fields['order_acceptance_amount'] = ($fields['order_acceptance_amount'] == "") ? 0 : $fields['order_acceptance_amount'];

            // Edited 29/September/2015 - Redmine ID: #6727
            // Calculate again the last column of CSV file.
            // 確定粗利額を出力（「確定売上」ー「確定原価」）
            $fields['profit_amount'] = 0;
            $fields['profit_amount'] = ($fields['sales_slip_amount'] - $fields['order_acceptance_amount']);
            // $fields['profit_amount'] = number_format($fields['profit_amount'], 2);
            /* if($fields['sales_slip_amount']!=0) {
                $fields['profit_amount'] = (($fields['sales_slip_amount'] - $fields['order_acceptance_amount'])/$fields['sales_slip_amount'])*100;
                $fields['profit_amount'] = number_format($fields['profit_amount'], 2);
            } */
            // End editted 29/September/2015 - Redmine ID: #6727

//#7631:Start
            if ($fields['change_report_approval_date'] == "0000-00-00") $fields['change_report_approval_date'] = "";
//#7631:End
            foreach ($fields as &$field) {
                $field = mb_convert_encoding($field,'SJIS','UTF-8');
            }

        }
        unset($fields);
        unset($field);
//#7631:Start
//        $header_array = array("Ｊコード","枝","Ｊ担当営業","Ｓコード","社名","部署",
//                "お客様担当者","前受伝","前請求","発注","検収","売伝","請求","受注ステータス",
//                "商品名","内容","納品日","請求日","支払日","数量","税","単価","合計",
//                "原価税別単価","原価税別合計","粗利合計","粗利率","確定売上","確定原価","確定粗利");
        $header_array = array("Ｊコード","枝","Ｊ担当営業","Ｓコード","社名","部署",
                "お客様担当者","前受伝","前請求","発注","検収","売伝","請求","受注ステータス",
                "商品名","内容", "受注承認日", "変レポ承認日","納品日","請求日","支払日","数量","税","単価","合計",
                "原価税別単価","原価税別合計","粗利合計","粗利率","確定売上","確定原価","確定粗利");
//#7631:End

        foreach($header_array as &$h) {
            $h = mb_convert_encoding($h,'SJIS','UTF-8');
        }

        // Define file csv and when to click file (disposition)
        $filename = filename_to_urlencode('受注_' . date("Ymd") . '.csv');
        header( 'Content-Type: text/csv' );
        header( 'Content-Disposition: attachment;filename='.$filename);

        $out = fopen('php://output', 'w'); //Resource id #139

        // After file csv has define and out put, then get this out put and write content
        // write header array to file
        fputcsv($out, $header_array);

        // write content file
        foreach ($list as $fields) {
             unset($fields['is_ad_receive_payment']);
            fputcsv($out, $fields);
        }
        fclose($out);
        die();
    }

    public function get_all_order_status() {
        $sql = "
                SELECT code, name
                FROM gui_parts_element
                WHERE gui_parts_id = 9 AND disable = 0
                ";
        return $this->exec_query($sql);
    }

    public function get_all_tax_division() {
        $sql = "
                SELECT code, name
                FROM gui_parts_element
                WHERE gui_parts_id = 6 AND disable = 0
                ";
        return $this->exec_query($sql);
    }
    public function save($input) {

        $order = $input->order;
        $this->load->library('form_validation');

        if (!isset($order->id)) $order->id = null;
        if (!isset($order->bu_j_code)) $order->bu_j_code = null;
        if (!isset($order->zipcode)) $order->zipcode = null;
        if (!isset($order->address_1st)) $order->address_1st = null;
        if (!isset($order->address_2nd)) $order->address_2nd = null;
        if (!isset($order->phone_no)) $order->phone_no = null;
        if (!isset($order->fax_no)) $order->fax_no = null;
        if (!isset($order->division_name)) $order->division_name = null;
        if (!isset($order->charge_name)) $order->charge_name = null;
        if (!isset($order->is_ad_receive_payment)) $order->is_ad_receive_payment = null;
        if (!isset($order->j_account)) $order->j_account = null;
        if (!isset($order->remark)) $order->remark = null;
        if (!isset($order->tax_total)) $order->tax_total = null;
        if (!isset($order->consumption_tax)) $order->consumption_tax = null;
        if (!isset($order->credit_level)) $order->credit_level = null;

//#7427:Start --> Check client status = 24
        $this->load->model('client_model');
        // init object result error string.
        $result = new stdclass();
        $result->error_string = "";

        if (!$client = $this->client_model->get_client_by_id($order->id)) {
            $result->error_string.="<p>クライアントを選択してください。</p>";
            die(json_encode($result)); exit();
        }

        if ($client['status'] == CLIENT_STATUS_RE_CREDIT_WAITING) {
            $result->error_string.="<p>こちらのお客様は再与信督促です。</p>";
            die(json_encode($result)); exit();
        }
//#7427:End
        $date = date("Y-m-d H:i:s");
        // j_code
        $j_code = date("ym") . $order->bu_j_code;
        $query = $this->db->select_max('j_code')
                ->from('order')
                ->where('disable', 0)
                ->like('j_code', $j_code, 'after')
                ->get();
        $auto_increase = $query->result();
        if ($auto_increase) {
            $auto_increase = $auto_increase[0]->j_code;
            $auto_increase = str_replace($j_code, '', $auto_increase);

            $auto_increase = (int)$auto_increase;
            $auto_increase++;
            $auto_increase = str_pad($auto_increase, 3, '0', STR_PAD_LEFT);
            $j_code = $j_code . $auto_increase;
        }
        else {
            $j_code = $j_code . "001";
        }

        //--------------------------------VALIDATE ORDER && ORDER DETAIL-----------------------------------

        // if exists order detail (branch) on grid => check order data form.
        if(isset($order->s_code)) {
            $data = array(
                'client_id' => $order->id,
                'j_code' => $j_code,
                'zipcode' => $order->zipcode,
                'address_1st' => $order->address_1st,
                'address_2nd' => $order->address_2nd,
                'phone_no' => $order->phone_no,
                'fax_no' => $order->fax_no,
                'division_name' => $order->division_name,
                'charge_name' => $order->charge_name,
                'order_regist_date' => $date,
                'is_ad_receive_payment' => $order->is_ad_receive_payment,
                'j_account_id' => $order->j_account,
                'remark' => $order->remark,
//#8216:Start
//                 'total_amount' => $order->tax_total,
//                 'total_amount_tax' => $order->consumption_tax,
            	'total_amount' => $order->cost_total_price,
            	'total_amount_tax' => $order->tax_total,
//#8216:Start
                // 'is_order_input' => 1, => Move to order_detail
                'comment' => '',
                // 'order_status' => self::UNAPPROVED, => Move to order_detail
                'lastup_account_id' => $this->auth->get_account_id(),
                'create_datetime' => $date,
                'lastup_datetime' => $date,
            );

            // Check client credit level C and oder status is UNAPPROVED
            if ($credit_balance = $this->validate_save($order,'', $input)) {
                $result->error_string = $credit_balance;
            }

            // Validate order
            $this->form_validation->set_rules($this->get_rule('insert'));
            $this->form_validation->set_data($data);

            if ($this->form_validation->run() === FALSE) {
                $result->error_string = $result->error_string . validation_errors();
            }
        } else {
            $result->error_string.="<p>クライアントを選択してください。</p>";
        }

        if ($order->cost_total_price > MAX_SIGNED_INT_10 ||  $order->tax_total > MAX_SIGNED_INT_10
                || $order->gross_profit_total > MAX_SIGNED_INT_10) {
                    $result->error_string.="<p>税込合計額が大き過ぎます。</p>";
        }
        // Validate order details (branches) on grid.
        $this->load->model('order_detail_model');

        if ($input->branchs && count($input->branchs) > 0) {
            // Check every row branch on grid
            foreach ($input->branchs as $branch) {
                $branch->order_id = 0;

                $order_detail = $this->order_detail_model->validate_save($branch, $order->is_ad_receive_payment, true);

                if (isset($order_detail->errors) && $order_detail->errors != '') {
                    $result->error_string_branchs[$branch->branch] = $order_detail->errors;
                }
                else {
                    $result->data_branchs[$branch->branch] = $order_detail->data;
                }
            }
        } // End if ($input->branchs && count($input->branchs)
        else {
            // If no branch
            $result->error_string = $result->error_string . "<p>明細情報を入力して下さい。</p>";
        }

        if ($result->error_string != '' || isset($result->error_string_branchs)) {
            die(json_encode($result)); exit();
        }

        // insert order & order detail data.
        try {
            $this->order_detail_model->begin_transaction();

            if (!$this->db->insert('order', $data)) {
                throw new Exception();
            }

            $insert_id = $this->db->insert_id();

            foreach ($input->branchs as $branch) {

                $data_branch = $result->data_branchs[$branch->branch];
                $data_branch['order_status'] = self::UNAPPROVED;
                $data_branch['is_order_input'] = 0;
                $data_branch['order_id'] = $insert_id;
//7389:Start
                $data_branch['gross_profit_rate'] = (isset($data_branch['gross_profit_rate']))? $data_branch['gross_profit_rate'] : 0;
//7389:End
                if (!$this->db->insert('order_detail', $data_branch)) {
                    throw new Exception();
                }
            }

            //update client amount equal 0 when credit type C and is ad receive request = 0
            if ($order->credit_level == CLIENT_CREDIT_LEVEL_C && $order->is_ad_receive_payment == 0) {

                //update client
                $update_data = array(
                        'credit_amount' => 0,
                        'lastup_account_id' => $this->auth->get_account_id(),
                        'lastup_datetime' => $this->order_model->_db_now(),
                );

                $update_conditions = array(
                        'ids' => $order->id,
                );

                if ( ! $this->client_model->update_client($update_data, $update_conditions)) {
                    throw new Exception(lang('update_client_fail'));
                }
            }

            $this->order_detail_model->commit_transaction();
        } catch (Exception $ex) {
            $this->app_logger->error_log($ex->getMessage());

            $this->order_detail_model->rollback_transaction();
            $result->error_string = "<p>申込書が作成できません。</p>";
            die(json_encode($result));
        }
        exit();
    }

    private function validate_save($input, $order_id = '', $order = array(), $is_add = true) {
        $result = '';
        if ($input->is_ad_receive_payment == 0) {
                    $this->load->helper('zenrin_system_helper');
                    $new_order = array();
                    if ($order->branchs) {
                        foreach ($order->branchs as $branch) {
                            if (isset($branch->od_payment_date) && isset($branch->od_delivery_date)) {
                                $item['amount'] =  $branch->od_amount_value;
                                $item['delivery_date'] = $branch->od_delivery_date;
                                $item['payment_date'] = $branch->od_payment_date;
                                $item['branch']          = $branch->od_branch_cd;
//#9200:Start
                                $item['tax_type']	     = $branch->od_tax_type;
//#9200:End
                                $new_order[] = $item;
                            }
                        }
                    }
                    $over_credit = get_payment_remain($input->id, $order_id, $new_order);
                    if ($over_credit['is_over_credit']) {
                        $result = "<p class='error-item'>与信限度額を超えています。</p>";
                    }
        }
        return $result;
    }
public function get_detail($order_id) {
        /* $query = $this->db->select('o.id o_id, o.client_id o_client_id, o.comment o_comment, o.remark o_remark, o.j_code o_j_code, c.s_code c_s_code, c.name c_name,
                        c.name_kana c_name_kana, c.zipcode c_zipcode, c.credit_level c_credit_level, c.mail_address c_mail_address, o.address o_address,
                        o.phone_no o_phone_no, o.fax_no o_fax_no, o.division_name o_division_name, o.charge_name o_charge_name,
                        c.closing_date c_closing_date, c.payment_month_cd c_payment_month_cd, c.payment_day_cd c_payment_day_cd, a.name a_name,
                        o.order_regist_date o_order_regist_date, od.id od_id, od.branch_cd od_branch_cd, o.j_account_id as o_account_id,
                        o.is_ad_receive_payment o_is_ad_receive_payment, o.is_order_input o_is_order_input, o.is_acceptance_input o_is_acceptance_input, o.is_sales_slip_input o_is_sales_slip_input,
                        o.is_invoice_issue o_is_invoice_issue, o.order_status o_order_status, p.id p_id, p.name p_name, od.contents od_contents, od.delivery_date od_delivery_date,
                        od.billing_date od_billing_date, od.payment_date od_payment_date, od.quantity od_quantity, od.tax_type od_tax_type, od.unit_price od_unit_price,
                        od.amount od_amount, od.cost_unit_price od_cost_unit_price, od.cost_total_price od_cost_total_price, od.gross_profit_amount od_gross_profit_amount,
                        od.gross_profit_rate od_gross_profit_rate, o.is_change_report_apply o_is_change_report_apply, o.order_form_input_date o_order_form_input_date, d.id d_id, d.name d_name, bu.id bu_id, bu.name bu_name,
                        o.total_amount o_total_amount, o.total_amount_tax o_total_amount_tax, o.zipcode o_zipcode, c.credit_amount c_credit_amount')
                ->from('order o')
                ->join('order_detail od', 'od.order_id = o.id')
                ->join('client c', 'c.id = o.client_id')
                ->join('product p', 'p.id = od.product_id')
                ->join('account a', 'a.id = o.j_account_id')
                ->join('department d', 'a.department_id = d.id')
                ->join('business_unit bu', 'd.business_unit_id = bu.id')
                ->where('o.disable', '0')
                ->where('od.disable', '0')
                ->where('c.disable', '0')
                ->where('o.id', $order_id)
                ->get();
        return $query->result(); */


        $sql = "SELECT
                    o.id o_id,
                    o.client_id o_client_id,
                    o.comment o_comment,
                    o.remark o_remark,
                    o.j_code o_j_code,
                    c.s_code c_s_code,
                    c.id c_id,
                    c.name c_name,
                    c.name_kana c_name_kana,
                    c.zipcode c_zipcode,
                    c.credit_level c_credit_level,
                    c.mail_address c_mail_address,
                    c.is_ad_receive_payment c_is_ad_receive_payment,
                    o.address_1st o_address_1st,
                    o.address_2nd o_address_2nd,
                    o.phone_no o_phone_no,
                    o.fax_no o_fax_no,
                    o.division_name o_division_name,
                    o.charge_name o_charge_name,
                    a.name a_name,
                    o.order_regist_date o_order_regist_date,
                    od.id od_id,
                    od.branch_cd od_branch_cd,
                    o.j_account_id o_account_id,
                    o.is_ad_receive_payment o_is_ad_receive_payment,
                    od.is_order_input o_is_order_input,
                    od.is_acceptance_input o_is_acceptance_input,
                    od.is_sales_slip_input o_is_sales_slip_input,
                    od.is_invoice_issue o_is_invoice_issue,
                    od.order_status od_order_status,
                    od.is_cancel_request od_is_cancel_request,
                    od.change_report_applicant_account_id od_change_report_applicant_account_id,
                    od.change_report_approval_account_id od_change_report_approval_account_id,
                    od.is_ad_sales_slip_input AS od_is_ad_sales_slip_input,
                    od.is_ad_invoice_issue AS od_is_ad_invoice_issue,

                    CASE WHEN od.order_status >= 0 THEN
                    (SELECT name
                        FROM gui_parts_element
                        WHERE gui_parts_id = ?
                            AND code = od.order_status
                            AND disable = 0
                            AND is_no_add = 0
                        ORDER BY sequence
                        LIMIT 1
                    )
                    ELSE ''
                    END AS 'od_order_status_name',
                    p.id p_id,
                    p.name p_name,
                    od.contents od_contents,
                    od.delivery_date od_delivery_date,
                    od.billing_date od_billing_date,
                    od.payment_date od_payment_date,
                    od.quantity od_quantity,
                    od.tax_type od_tax_type,
                    od.unit_price od_unit_price,
                    od.amount od_amount,
                    od.cost_unit_price od_cost_unit_price,
                    od.cost_total_price od_cost_total_price,
                    od.gross_profit_amount od_gross_profit_amount,
                    od.gross_profit_rate od_gross_profit_rate,
                    od.is_change_report_apply o_is_change_report_apply,
                    o.order_form_input_date o_order_form_input_date,
                    d.id d_id,
                    d.name d_name,
                    bu.id bu_id,
                    bu.name bu_name,
                    bu.template bu_template,
                    o.total_amount o_total_amount,
                    o.total_amount_tax o_total_amount_tax,
                    o.zipcode o_zipcode,
                    c.credit_amount c_credit_amount,
                    c.closing_date c_closing_date,
                    c.payment_month_cd c_payment_month_cd,
                    c.payment_day_cd c_payment_day_cd,

                    CASE WHEN c.closing_date >= 0 THEN
                        (SELECT name FROM gui_parts_element WHERE gui_parts_id = 2 AND code = c.closing_date)
                    ELSE ''
                    END AS 'closing_date_name',

                    CASE WHEN c.payment_month_cd >= 0 THEN
                        (SELECT name FROM gui_parts_element WHERE gui_parts_id = 3 AND code = c.payment_month_cd)
                    ELSE ''
                    END AS 'payment_month_cd_name',

                    CASE WHEN c.payment_day_cd >= 0 THEN
                        (SELECT name FROM gui_parts_element WHERE gui_parts_id = 4 AND code = c.payment_day_cd)
                    ELSE ''
                    END AS 'payment_day_cd_name'
                FROM
                    `order` o JOIN order_detail od ON od.order_id = o.id
                    JOIN client c ON c.id = o.client_id
                    JOIN product p ON p.id = od.product_id
                    JOIN account a ON a.id = o.j_account_id
                    JOIN department d ON a.department_id = d.id
                    JOIN business_unit bu ON d.business_unit_id = bu.id
                WHERE
                    o.disable = 0 AND od.disable = 0 AND c.disable = 0 AND o.id IN ('" . $order_id . "')
                ";
        return $this->exec_query($sql, array(self::GUI_PART_ORDER_STATUS));
    }

    public function get_detail_with_log($order_id) {
        /* $query = $this->db->select('o.id o_id, o.client_id o_client_id, o.comment o_comment, o.remark o_remark, o.j_code o_j_code, c.s_code c_s_code, c.name c_name,
                        c.name_kana c_name_kana, c.mail_address c_mail_address, c.zipcode c_zipcode, c.credit_level c_credit_level, o.address o_address,
                        o.phone_no o_phone_no, o.fax_no o_fax_no, o.division_name o_division_name, o.charge_name o_charge_name,
                        c.closing_date c_closing_date, c.payment_month_cd c_payment_month_cd, c.payment_day_cd c_payment_day_cd, a.name a_name,
                        o.order_regist_date o_order_regist_date, od.branch_cd od_branch_cd, o.j_account_id as o_account_id,
                        o.is_ad_receive_payment o_is_ad_receive_payment, o.is_order_input o_is_order_input, o.is_acceptance_input o_is_acceptance_input, o.is_sales_slip_input o_is_sales_slip_input,
                        o.is_invoice_issue o_is_invoice_issue, o.order_status o_order_status, p.name p_name, p1.name p1_name, od.contents od_contents, od.delivery_date od_delivery_date,
                        od.billing_date od_billing_date, od.payment_date od_payment_date, od.quantity od_quantity, od.tax_type od_tax_type, od.unit_price od_unit_price,
                        od.amount od_amount, od.cost_unit_price od_cost_unit_price, od.cost_total_price od_cost_total_price, od.gross_profit_amount od_gross_profit_amount,
                        od.gross_profit_rate od_gross_profit_rate, o.is_change_report_apply o_is_change_report_apply, o.order_form_input_date o_order_form_input_date, d.id d_id, d.name d_name, bu.id bu_id, bu.name bu_name,
                        ol.id ol_id, ol.product_id ol_product_id, ol.contents ol_contents, ol.delivery_date ol_delivery_date, ol.billing_date ol_billing_date, ol.payment_date ol_payment_date, ol.quantity ol_quantity, ol.tax_type ol_tax_type,
                        ol.unit_price ol_unit_price, ol.amount ol_amount, ol.cost_unit_price ol_cost_unit_price, ol.cost_total_price ol_cost_total_price, ol.gross_profit_amount ol_gross_profit_amount, ol.gross_profit_rate ol_gross_profit_rate')
                            ->from('order o')
                            ->join('order_detail od', 'od.order_id = o.id')
                            ->join('order_detail_log ol', 'ol.order_detail_id = od.id and ol.disable = 0', 'left')
                            ->join('client c', 'c.id = o.client_id')
                            ->join('product p', 'p.id = od.product_id')
                            ->join('product p1', 'p1.id = ol.product_id', 'left')
                            ->join('account a', 'a.id = o.j_account_id')
                            ->join('department d', 'a.department_id = d.id')
                            ->join('business_unit bu', 'd.business_unit_id = bu.id')
                            ->where('o.disable', '0')
                            ->where('od.disable', '0')
                            ->where('c.disable', '0')
                            ->where('o.id', $order_id)
                            ->get();
        return $query->result(); */

        $sql="SELECT
                o.id o_id, o.client_id o_client_id, o.comment o_comment, o.remark o_remark, o.j_code o_j_code,
                c.s_code c_s_code, c.name c_name, c.name_kana c_name_kana, c.mail_address c_mail_address,
                c.zipcode c_zipcode, c.credit_level c_credit_level, o.address_1st o_address_1st, o.address_2nd o_address_2nd,
                o.phone_no o_phone_no, o.fax_no o_fax_no, o.division_name o_division_name, o.charge_name o_charge_name,
                c.closing_date c_closing_date, c.payment_month_cd c_payment_month_cd, c.payment_day_cd c_payment_day_cd,
                  CASE WHEN c.closing_date >= 0 THEN
                  (SELECT name FROM gui_parts_element WHERE gui_parts_id = 2 AND code = c.closing_date)
                  ELSE ''
                  END AS 'closing_date_name',

                  CASE WHEN c.payment_month_cd >= 0 THEN
                  (SELECT name FROM gui_parts_element WHERE gui_parts_id = 3 AND code = c.payment_month_cd)
                  ELSE ''
                  END AS 'payment_month_cd_name',

                  CASE WHEN c.payment_day_cd >= 0 THEN
                  (SELECT name FROM gui_parts_element WHERE gui_parts_id = 4 AND code = c.payment_day_cd)
                  ELSE ''
                  END AS 'payment_day_cd_name',

                  CASE WHEN o.is_ad_receive_payment = 1 THEN od.is_ad_sales_slip_input
                  ELSE ''
                  END AS 'o_is_ad_sales_slip_input',

                  CASE WHEN o.is_ad_receive_payment = 1 THEN od.is_ad_invoice_issue
                  ELSE ''
                  END AS 'o_is_ad_invoice_issue',

                a.name a_name, o.order_regist_date o_order_regist_date, od.branch_cd od_branch_cd,
                o.j_account_id as o_account_id, od.order_status od_order_status,
                od.is_order_input o_is_order_input, od.is_acceptance_input o_is_acceptance_input,
                od.is_sales_slip_input o_is_sales_slip_input, od.is_invoice_issue o_is_invoice_issue,
                CASE WHEN od.order_status>=0 THEN
                  (SELECT name FROM gui_parts_element WHERE gui_parts_id = 9 AND code = od.order_status)
                  ELSE ''
                  END AS 'od_order_status_name',
                p.name p_name, p1.name p1_name, od.id od_id,
                od.contents od_contents, od.is_cancel_request od_is_cancel_request, od.delivery_date od_delivery_date,
                od.billing_date od_billing_date, od.payment_date od_payment_date, od.quantity od_quantity,
                od.tax_type od_tax_type, od.unit_price od_unit_price, od.amount od_amount,
                od.cost_unit_price od_cost_unit_price, od.cost_total_price od_cost_total_price,
                od.gross_profit_amount od_gross_profit_amount, od.gross_profit_rate od_gross_profit_rate, od.shift_color_cd od_shift_color_cd,
                od.is_change_report_apply od_is_change_report_apply, o.order_form_input_date o_order_form_input_date,
                d.id d_id, d.name d_name, bu.id bu_id, bu.name bu_name, bu.template bu_template, ol.id ol_id, ol.product_id ol_product_id,
                ol.contents ol_contents, ol.delivery_date ol_delivery_date, ol.billing_date ol_billing_date,
                ol.payment_date ol_payment_date, ol.quantity ol_quantity, ol.tax_type ol_tax_type,
                ol.unit_price ol_unit_price, ol.amount ol_amount, ol.cost_unit_price ol_cost_unit_price,
                ol.cost_total_price ol_cost_total_price, ol.gross_profit_amount ol_gross_profit_amount,
                ol.gross_profit_rate ol_gross_profit_rate
            FROM
                `order` o JOIN order_detail od ON od.order_id = o.id
                LEFT JOIN order_detail_log ol ON ol.order_detail_id = od.id and ol.disable = 0
                JOIN client c ON c.id = o.client_id
                JOIN product p ON p.id = od.product_id
                LEFT JOIN product p1 ON p1.id = ol.product_id
                JOIN account a ON a.id = o.j_account_id
                JOIN department d ON a.department_id = d.id
                JOIN business_unit bu ON d.business_unit_id = bu.id
            WHERE
                o.disable = 0 AND od.disable = 0 AND c.disable = 0 AND o.id = ?
        ";
        return $this->exec_query($sql, array($order_id));
    }

public function get_order_detail_log($order_id) {
        $query = $this->db->select('
                od.id od_id,
                branch_cd od_branch_cd,
                od.product_id od_product_id,
                od.`contents` od_contents,
                od.delivery_date od_delivery_date,
                od.cost_total_price od_cost_total_price,
                od.amount od_amount,
                od.gross_profit_amount od_gross_profit_amount,
                od.gross_profit_rate od_gross_profit_rate,
                od.original_delivery_date od_original_delivery_date,
                od.change_report_applicant_account_id od_change_report_applicant_account_id,
                p1.name od_name,
                ol.id ol_id,
                ol.product_id ol_product_id,
                p2.name ol_name,
                ol.`contents` ol_contents,
                ol.delivery_date ol_delivery_date,
                ol.billing_date ol_billing_date,
                ol.payment_date ol_payment_date,
                ol.quantity ol_quantity,
                ol.tax_type ol_tax_type,
                ol.unit_price ol_unit_price,
                ol.amount ol_amount,
                ol.cost_unit_price ol_cost_unit_price,
                ol.cost_total_price ol_cost_total_price,
                ol.gross_profit_amount ol_gross_profit_amount,
                ol.gross_profit_rate ol_gross_profit_rate')
                    ->from('order_detail_log ol')
                    ->join('order_detail od', 'ol.order_detail_id = od.id')
                    ->join('product p1', 'od.product_id = p1.id')
                    ->join('product p2', 'ol.product_id = p2.id')
                    ->where('od.order_id', $order_id)
                    ->where('od.disable', '0')
                    ->where('ol.disable', '0')
                    ->get();
        return $query->result();
    }


    public function get_order_detai_by_jCode_and_BrandCode($jcode,$brand_code) {
        $query = $this->db->select('*')
                    ->from('order o')
                    ->join('order_detail od', 'od.order_id = o.id')
                    ->where('o.j_code', $jcode)
                    ->where('od.branch_cd', $brand_code)
                    ->where('od.disable', '0')
                    ->where('o.disable', '0')
                    ->get();
        return $query->result();
    }

    /**
     * Update order
     *
     * @param array $params
     * @return bool
     * @author hoang_minh
     * @since 2015-06-17
     */
    public function update_order($params = array()) {
        if ( ! is_array($params) || empty($params)) {
            return false;
        }

        return $this->update('`order`', $params['update_data'], $params['update_conditions']);
    }

    public function update_order_detail($params = array()) {
        if ( !is_array($params) || empty($params) ) {
            return false;
        }
        return $this->update('order_detail', $params['data'], $params['conditions']);
    }

    public function update_change_report($params = array()) {
        if (!is_array($params) || empty($params)) {
            return false;
        }

        return $this->update('`order`', $params['data'], $params['conditions']);
    }

    public function update_reject_change_report($params = array()) {
        if (!is_array($params) || empty($params)) {
            return false;
        }

        return $this->update('`order`', $params['data'], $params['conditions']);
    }

    public function update_order_detail_log($params = array()) {
        if (!is_array($params) || empty($params)) {
            return false;
        }

        return $this->update('order_detail_log', $params['data'], $params['conditions']);
    }

    public function get_j_account($account_id) {
        $query = $this->db->select('a.id, a.name, bu.id bu_id, bu.template, bu.j_code')
                ->from('business_unit bu')
                ->join('department d', 'd.business_unit_id = bu.id')
                ->join('account a', 'a.department_id = d.id')
                ->where('a.id', $account_id)
                ->where('a.resignation', 0)
                ->get();
        return $query->result();
    }

    public function delete($order_id) {
        $this->db->trans_start();

        $this->db->where('id', $order_id);
        $this->db->update('order', array('disable' => 1));

        $this->db->where('order_id', $order_id);
        $this->db->update('order_detail', array('disable' => 1));

        $this->db->trans_complete();
    }

    public function cancel_apply($order_id, $comment) {
        $this->db->trans_start();

        $current_order = $this->db->get_where('order', array('id' => $order_id, 'disable' => 0), 1)->row();

        $this->db->where('id', $order_id);
        $this->db->where('disable', 0);
        $this->db->update('order', array('comment' => $current_order->comment . (($comment) ? "\r\n" . $comment : '')));

        $this->db->where('order_id', $order_id);
        $this->db->where('order_status', self::UNDETERMINED);
        $this->db->where('disable', 0);
        $this->db->update('order_detail', array('order_status' => self::CANCELLED));

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return false;
        }
        return true;
    }

    public function delete_branch($order_id, $branch) {
        $this->db->where('order_id', $order_id);
        $this->db->where('branch_cd', $branch);
        $this->db->update('order_detail', array('disable' => 1));

        $query = $this->db->select('*')
                ->from('order_detail')
                ->where('id', $order_id)
                ->get();
        $order_details = $query->result();
        $i = 1;
        foreach ($order_details as $order_detail) {
            $this->db->where('id', $order_detail->id);
            $this->db->update('order_detail', array('branch_cd' => $i));
            $i++;
        }
    }

    public function change_report($input) {
        $order = $input->order;

        $this->load->library('form_validation');
        $this->load->library('email_template');

// #9242:Start
        $this->load->model('order_acceptance_model');
// #9242:End

        if (!isset($order->o_id)) $order->o_id = null;
        if (!isset($order->o_zipcode)) $order->o_zipcode = null;
        if (!isset($order->o_address_1st)) $order->o_address_1st = null;
        if (!isset($order->o_address_2nd)) $order->o_address_2nd = null;
        if (!isset($order->o_phone_no)) $order->o_phone_no = null;
        if (!isset($order->o_fax_no)) $order->o_fax_no = null;
        if (!isset($order->o_division_name)) $order->o_division_name = null;
        if (!isset($order->o_charge_name)) $order->o_charge_name = null;
        if (!isset($order->o_is_ad_receive_payment)) $order->o_is_ad_receive_payment = null;
        if (!isset($order->o_j_account)) $order->o_j_account = null;
        if (!isset($order->o_remark)) $order->o_remark = null;
        if (!isset($order->o_order_status)) $order->o_order_status = null;
        if (!isset($order->o_tax_total)) $order->o_tax_total = null;
        if (!isset($order->o_consumption_tax)) $order->o_consumption_tax = null;
        if (!isset($order->o_credit_level)) $order->o_credit_level = null;
        if (!isset($order->o_new_comment)) $order->o_new_comment = '';

        $date = date("Y-m-d H:i:s");
        $data = array(
                'id' => $order->o_id,
                'zipcode' => $order->o_zipcode,
                'address_1st' => $order->o_address_1st,
                'address_2nd' => $order->o_address_2nd,
                'phone_no' => $order->o_phone_no,
                'fax_no' => $order->o_fax_no,
                'division_name' => $order->o_division_name,
                'charge_name' => $order->o_charge_name,
//#7630:Start
//                 'order_regist_date' => $date,
//#7630:End
                'is_ad_receive_payment' => $order->o_is_ad_receive_payment,
                'j_account_id' => $order->o_account_id,
                'remark' => $order->o_remark,
                'comment' => $order->o_new_comment,
//#8216:Start
//                 'total_amount' => $order->o_tax_total,
//                 'total_amount_tax' => $order->o_consumption_tax,
//#8216:End
                'lastup_account_id' => $this->auth->get_account_id(),
//#7630:Start
//                 'create_datetime' => $date,
                'lastup_datetime' => $date,
//#7630:End
        );

        if (isset($input->is_change_report) && ($input->is_change_report) == false) {
        	$data['total_amount'] 		= $order->o_cost_total_price;
        	$data['total_amount_tax']   = $order->o_tax_total;
        }

        $result = new stdclass();

        $result->error_string = '';

//#7427:Start --> Check client status = 24
        $this->load->model('client_model');
        if (!$client = $this->client_model->get_client_by_id($order->o_client_id)) {
            $result->error_string.="<p>クライアントを選択してください。</p>";
            die(json_encode($result)); exit();
        }

        if ($client['status'] == CLIENT_STATUS_RE_CREDIT_WAITING) {
            $result->error_string.="<p>こちらのお客様は再与信督促です。</p>";
            die(json_encode($result)); exit();
        }
//#7427:End

        // Check client credit level
        $chk_order = new stdclass();
        $chk_order->id = $order->o_client_id;
        $chk_order->cost_total_price = $order->o_cost_total_price;
        $chk_order->is_ad_receive_payment = $order->o_is_ad_receive_payment;
        if ($credit_balance = $this->validate_save($chk_order, $order->o_id, $input, false)) {
            $result->error_string = $credit_balance;
        }

        $this->form_validation->set_rules($this->get_rule('update'));
        $this->form_validation->set_data($data);
        if($input->changed_report_type && empty($order->o_new_comment)) {
            $result->error_string.="変レポコメントを入力して下さい。";
        }
        if ($this->form_validation->run() === FALSE) {
            $result->error_string = $result->error_string . validation_errors();
        }

        if ($order->o_order_form_input_date)
        {
            if (!parse_date($order->o_order_form_input_date)) {
                $result->error_string = $result->error_string . "<p>申込書右上日付は無効な日付です</p>";
            } else {
                $data['order_form_input_date'] = parse_date($order->o_order_form_input_date);
            }
        } else {
            $data['order_form_input_date'] = '0000-00-00';
        }

        $this->load->model('order_detail_model');

        if (count($input->branchs) > 0) {
            if (in_array($order->o_order_status, array(self::UNDETERMINED, self::CONFIRMED)) && !$order->o_new_comment) {
                $result->error_string = $result->error_string . '<p>変レポコメントを入力して下さい。</p>';
            }

            if ($order->o_cost_total_price > MAX_SIGNED_INT_10 ||  $order->o_tax_total > MAX_SIGNED_INT_10
                    || $order->o_gross_profit_total > MAX_SIGNED_INT_10) {
                        $result->error_string.="<p>税込合計額が大き過ぎます。</p>";
            }
// #9242:Start
            $update_datas_order_acceptance = array();
// #9242:End

            foreach ($input->branchs as $branch_temp) {
                $branch = new stdclass();
                $branch->order_id = $data['id'];
                $branch->branch = (isset($branch_temp->od_branch_cd)) ? $branch_temp->od_branch_cd : null;
                $branch->product_name = (isset($branch_temp->p_id)) ? $branch_temp->p_id : null;
                $branch->contents = (isset($branch_temp->od_contents)) ? $branch_temp->od_contents : null;
                $branch->delivery_date = (isset($branch_temp->od_delivery_date)) ? $branch_temp->od_delivery_date : null;
                $branch->billing_date = (isset($branch_temp->od_billing_date)) ? $branch_temp->od_billing_date : null;
                $branch->payment_date = (isset($branch_temp->od_payment_date)) ? $branch_temp->od_payment_date : null;
                $branch->quantity = (isset($branch_temp->od_quantity)) ? $branch_temp->od_quantity : null;
                $branch->tax_type = (isset($branch_temp->od_tax_type)) ? $branch_temp->od_tax_type : null;
                $branch->unit_price = (isset($branch_temp->od_unit_price)) ? $branch_temp->od_unit_price : null;
                $branch->amount_value = (isset($branch_temp->od_amount_value)) ? $branch_temp->od_amount_value : null;
                $branch->cost_unit_price = (isset($branch_temp->od_cost_unit_price)) ? $branch_temp->od_cost_unit_price : null;
                $branch->cost_total_price = (isset($branch_temp->od_cost_total_price)) ? $branch_temp->od_cost_total_price : null;
                $branch->gross_profit_amount_value = (isset($branch_temp->od_gross_profit_amount_value)) ? $branch_temp->od_gross_profit_amount_value : null;
                $branch->gross_profit_rate_value = (isset($branch_temp->od_gross_profit_rate_value)) ? $branch_temp->od_gross_profit_rate_value : null;
                $branch->contents = (isset($branch_temp->od_contents)) ? $branch_temp->od_contents : null;
                $branch->is_cancel_request = (isset($branch_temp->od_is_cancel_request)) ? $branch_temp->od_is_cancel_request : null;
                $branch->disable = (isset($branch_temp->disable)) ? $branch_temp->disable : null;
                $branch->shift_color_cd = (isset($branch_temp->shift_color_cd)) ? $branch_temp->shift_color_cd : null;


//#9242:Start
                if($input->order->o_account_id != $branch_temp->o_account_id_old){
                    $list_order_acceptance_detail = $this->order_acceptance_model->get_order_acceptance_by_jcode_and_branch_cd($branch_temp->o_j_code.$branch_temp->od_branch_cd);
                    foreach ($list_order_acceptance_detail as $od){
                                $update_datas_order_acceptance[] =array(
                                                            'id'              => $od['id'],
                                                            'account_id'         => $input->order->o_account_id,
                                                            'lastup_datetime' => date('Y-m-d H:i:s'),
                                                            'lastup_account_id' => $this->auth->get_account_id()
                               ) ;
                    }
                }
//#9242:End
                //#6837: Start 2015/09/22
                $is_change_report = false;
                if (isset($input->is_change_report) && ($input->is_change_report) == false) {
                    $is_change_report = true;
                } elseif (isset($branch_temp->change_report) && ($branch_temp->change_report == 1)) {
                    $is_change_report = true;
                }
                //#6837: End
                $order_detail = $this->order_detail_model->validate_save($branch, $order->o_is_ad_receive_payment, $is_change_report);

                // Validate grid if have error save to array.
                if (isset($order_detail->errors) && $order_detail->errors != '') {
                    $result->error_string_branchs[$branch->branch] = $order_detail->errors;
                }
                else { // success
                    $result->data_branchs[$branch->branch] = $order_detail->data;
                }

            }

// #9242:Start

            if($input->order->o_account_id != $branch_temp->o_account_id_old){
                   if($update_datas_order_acceptance){
                                if (!$this->db->update_batch('order_acceptance', $update_datas_order_acceptance, 'id')) {
                                    throw new Exception("update error");
                                    $result->error_string = 'Update fail';
                                    die(json_encode($result));
                                }
                    }
            }
// #9242:End

        } else {
            $result->error_string = $result->error_string . "<p>明細情報を入力して下さい。</p>";
        }

        if ($result->error_string != '' || isset($result->error_string_branchs)) {
            die(json_encode($result));
        }

        try {

            $this->order_detail_model->begin_transaction();

            // begin update order data for change report follow order_id
            $this->db->where('id', $data['id']);
            $order_database = $this->db->get('order')->result();
//             $data['comment'] = $order_database[0]->comment . "\r\n" . $data['comment'];
            $data['comment'] = (!empty($order_database[0]->comment)) ? $order_database[0]->comment . "\r\n" . $data['comment'] : $data['comment'];
//             echo $data['comment'];exit;

            $this->db->where('id', $data['id']);

            if (!$this->db->update('order', $data)) {
                throw new Exception();
            }

            $order_detail_ids = array();
            foreach ($input->branchs_del as $br) {
                $order_detail_ids[] = $br->od_id;
            }

            if ($order_detail_ids) {
                $this->db->where_in('id', $order_detail_ids);
                if (!$this->db->update('order_detail', array('disable' => 1))) {
                    throw new Exception();
                }
            }

//         // send mail
                if(count($input->branchs) > 0) {
                    foreach ($input->branchs as $branch) {
                        $data_branch = $result->data_branchs[$branch->od_branch_cd];

                        if (isset($branch->change_report) && $branch->change_report == true) {

                            $update_data = array(
                                    'shift_color_cd' => $branch->shift_color_cd,
                                    'is_change_report_apply' => 1,
                                    'is_cancel_request' => (($data_branch['is_cancel_request']) ? 1:0),
                                    'change_report_applicant_account_id' => $this->auth->get_account_id(),
//                                    'product_id' => $data_branch['product_id'],
//                                    'contents'  => $data_branch['contents'],
//                                    'delivery_date'  => $data_branch['delivery_date'],
//                                    'billing_date'  => $data_branch['billing_date'],
//                                    'payment_date'  => $data_branch['payment_date'],
//                                    'quantity'  => $data_branch['quantity'],
//                                    'tax_type'  => $data_branch['tax_type'],
//                                    'unit_price'  => $data_branch['unit_price'],
//                                    'amount'  => $data_branch['amount'],
//                                    'cost_unit_price'  => $data_branch['cost_unit_price'],
//                                    'cost_total_price'  => $data_branch['cost_total_price'],
//                                    'gross_profit_amount'  => $data_branch['gross_profit_amount'],
//                                    'gross_profit_rate'  => $data_branch['gross_profit_rate'],
                                    'lastup_datetime'  => $data_branch['lastup_datetime'],
                            );

                            // get order detail old for send mail
                            $this->db->where('order_detail.id', $branch->od_id);
                            $this->db->join('product', 'product.id = order_detail.product_id');
                            $order_detail_old = $this->db->get('order_detail')->row();
//                            $order_detail_old = $order_detail_old[0];

                            //
                            if ($order_detail_old->product_id != $branch->p_id) {
                                $this->db->select('name');
                                $this->db->where('id', $branch->p_id);
                                $new_product_obj = $this->db->get('product')->row();
                                $new_product = $new_product_obj->name;
                            } else {
                                $new_product = $order_detail_old->name;
                            }

                            $this->db->where('id', $branch->od_id);
                            if (!$this->db->update('order_detail', $update_data)) {
                                throw new Exception();
                            }

                            if (!$data_branch['is_cancel_request']) {
                                // get order detail new to insert to log table
                                $order_detail_log = $data_branch;
                                //die(json_encode($order_detail_old));
                                unset($order_detail_log['order_id']);
                                $order_detail_log['order_detail_id'] = $branch->od_id;
                                unset($order_detail_log['id']);
                                unset($order_detail_log['branch_cd']);
                                unset($order_detail_log['shift_color_cd']);
                                unset($order_detail_log['is_ad_sales_slip_input']);
                                unset($order_detail_log['is_ad_invoice_issue']);
                                unset($order_detail_log['is_order_input']);
                                unset($order_detail_log['is_acceptance_input']);
                                unset($order_detail_log['is_sales_slip_input']);
                                unset($order_detail_log['is_invoice_issue']);
                                unset($order_detail_log['is_cancel_request']);

                                // write new data detail order has change report to order detail log
                                $this->db->insert('order_detail_log', $order_detail_log);

                                $mail_content = '■ 変更前' . "\r\n"
                                            . '商品名　　	:' . $order_detail_old->name . "\r\n"
                                            . '内容　　　	:' . $order_detail_old->contents ."\r\n"
                                            . '納品日　　	:' . str_replace("-", "/", $order_detail_old->delivery_date) ."\r\n"
                                            . '受注金額	:' . number_format($order_detail_old->amount) . "\r\n"
                                            . '粗利額　　	:' . number_format($order_detail_old->gross_profit_amount) . "\r\n"
                                            . '粗利率　　	:' . number_format($order_detail_old->gross_profit_rate, 2, '.', '') . '%' . "\r\n"
                                            . '----------------------------------------'
                                            . "\r\n\r\n"

                                            . '■ 変更後 ' ."\r\n"
                                            . '商品名　　	:' .$new_product. "\r\n"
                                            . '内容　　　	:' .$branch->od_contents ."\r\n"
                                            . '納品日　　	:' .str_replace("-", "/",$branch->od_delivery_date) ."\r\n"
                                            . '受注金額	:' .number_format($branch->od_amount_value) . "\r\n"
                                            . '粗利額　　	:' .number_format($branch->od_gross_profit_amount_value) . "\r\n"
                                            . '粗利率　　	:' .number_format($branch->od_gross_profit_rate_value, 2, '.', '') . '%' . "\r\n"
                                            . '----------------------------------------'
                                            . "\r\n";

                                // paramms for template email
                                $mail_params = array(
                                        'account_id' => (isset($order->a_name) ? $order->a_name : ''),
                                        'bu_name' => (isset($order->bu_name) ? $order->bu_name : ''),
                                        'client_name' => (isset($order->c_name) ? $order->c_name : ''),
                                        'j_code' => (isset($order->o_j_code) ? $order->o_j_code : ''),
                                        'branch_cd' => (isset($branch->od_branch_cd) ? $branch->od_branch_cd : ''),
                                        'mail_content' => $mail_content
                                );
#7625:Start
                                $send_mail_list = $this->get_mail_list_authority($order->o_id, $branch->od_change_report_approval_account_id);
#7625:End

                                // send email to client
                                $mail_address_list = array();

                                foreach ($send_mail_list as $item) {
                                    if ( ! in_array($item['mail_address'], $mail_address_list)) {
                                        if (!$this->email_template->send_mail('change_report_apply', array('system_to' => $item['mail_address']), $mail_params)) {
                                            throw new Exception(lang('send_mail_change_report_fail'));
                                        }

                                        $mail_address_list[] = $item['mail_address'];
                                    }
                                }
                            } else { // When change report cancel order detail on branch
#7625:Start
                                $mail_content = '■ キャンセル前' . "\r\n"
                                            . '商品名　　	:' . $branch->p_name . "\r\n"
                                            . '内容　　　	:' . $branch->od_contents ."\r\n"
                                            . '納品日　　	:' . str_replace("-", "/", $branch->od_delivery_date) ."\r\n"
                                            . '受注金額	:' . number_format($branch->od_amount_value) . "\r\n"
                                            . '粗利額　　	:' . number_format($branch->od_gross_profit_amount_value) . "\r\n"
                                            . '粗利率　　	:' . number_format($branch->od_gross_profit_rate_value, 2, '.', '') . '%' . "\r\n"
                                            . '----------------------------------------'
                                            . "\r\n";
                                $mail_params = array(
                                        'account_id'    => (isset($order->a_name) ? $order->a_name : ''),
                                        'bu_name'       => (isset($order->bu_name) ? $order->bu_name : ''),
                                        'client_name'   => (isset($order->c_name) ? $order->c_name : ''),
                                        'j_code'        => (isset($order->o_j_code) ? $order->o_j_code : ''),
                                        'branch_cd'     => (isset($branch->od_branch_cd) ? $branch->od_branch_cd : ''),
                                        'mail_content'  => $mail_content
                                );

                                $send_mail_list = $this->get_mail_list_authority($order->o_id, $branch->od_change_report_approval_account_id);

                                // send email to client
                                $mail_address_list = array();

                                foreach ($send_mail_list as $item) {

                                    if (!in_array($item['mail_address'], $mail_address_list)) {
                                        if (!$this->email_template->send_mail('change_report_cancel_request', array('system_to' => $item['mail_address']), $mail_params)) {
                                            throw new Exception(lang('send_mail_change_report_fail'));
                                        }
                                        $mail_address_list[] = $item['mail_address'];
                                    }
                                }
                            }
#7625:End
                        }

                        if (isset($input->is_change_report) && $input->is_change_report == false) {
                            if (isset($branch->od_id) && $branch->od_id != '') {
                                unset($data_branch['shift_color_cd']);
                                $this->db->where('id', $branch->od_id);
                                if (!$this->db->update('order_detail', $data_branch)) {
                                    throw new Exception();
                                }
                            }
                            else {
                                $data_branch['order_status'] =  self::UNAPPROVED;
                                if (!$this->db->insert('order_detail', $data_branch)) {
                                    throw new Exception();
                                }
                            }
                        }
                    }
                }
                $this->order_detail_model->commit_transaction();
           } catch(Exception $ex) {
                $this->order_detail_model->rollback_transaction();
                $result->error_string = 'Update fail';
                die(json_encode($result));
        }
    }

#7625:Start
    /**
     * Get_mail_list_authority description
     * @return array email - After check authority all && self - self need get j acccount && od_change_report_applicant_account_id for send email if exists
     */
    public function get_mail_list_authority($order_id, $o_change_report_approval_account_id)
    {
        $this->load->model('account_model');
        $this->load->model('authority_model');
        $send_mail_list = array();

        // Get list id of user account have persmission with quick setting column = mail_change_report_approval_all
        $authority_ids = $this->authority_model->get_authority_by_quick_setting('mail_change_report_approval_all');
        if (!empty($authority_ids)) {

            $params = array(
                'conditions' => array('authority_ids' => implode(',', $authority_ids)),
                'columns' => array('mail_address')
            );

            $send_mail_list = $this->account_model->get_accounts_by_parameters($params);

        }

        // Check self account have quick setting column = mail_change_report_approval_all
        $order_info     = $this->get_order_by_id($order_id);
        $authority_ids  = $this->authority_model->get_authority_by_quick_setting('mail_change_report_approval_self');

        if (!empty($authority_ids)) {

            $order_info['j_account_id'] = ($order_info['j_account_id']) ? $order_info['j_account_id'] : '0';
            // $branch->od_change_report_applicant_account_id = ($branch->od_change_report_applicant_account_id) ? $branch->od_change_report_applicant_account_id : '0';
            $o_change_report_approval_account_id = ($o_change_report_approval_account_id) ? $o_change_report_approval_account_id : '0';

            $params = array(
                'conditions' => array(
                    'authority_ids' => implode(',', $authority_ids),
                    'ids'           => implode(',', array($order_info['j_account_id'], $o_change_report_approval_account_id))
                ),
                'columns' => array('mail_address')
            );

            $send_mail_list = array_merge($send_mail_list, $this->account_model->get_accounts_by_parameters($params));
        }

        return $send_mail_list;
    }
#7625:End

    /**
     * Check and get data for change report approve
     * @param unknown $order_id
     */
    public function check_change_report_apply($order_id) {

        /* $query = $this->db->select('
                o.id, o.j_code o_j_code, a.name a_name, c.name c_name, o.j_account_id o_j_account_id, o.change_report_approval_account_id o_change_report_approval_account_id')
                ->from('order o')
                ->join('account a', 'o.j_account_id = a.id')
                ->join('client c', 'o.client_id = c.id')
                ->where('o.id', $order_id)
//                 ->where('is_change_report_apply', '1')
                ->get();

        return $query->result(); */
        $sql = "SELECT
                o.id,
                o.j_code o_j_code,
                o.order_approval_date,
                a.name a_name, c.name c_name,
                bu.name bu_name,
                o.j_account_id o_j_account_id,
                od.change_report_approval_account_id o_change_report_approval_account_id
                FROM
                `order` o JOIN account a ON o.j_account_id = a.id
                JOIN `department` d ON d.id = a.department_id
                JOIN `business_unit` bu ON bu.id = d.business_unit_id
                JOIN `client` c ON o.client_id = c.id
                JOIN order_detail od ON od.order_id = o.id
                WHERE
                o.id = ? AND o.disable= 0 AND a.disable = 0 AND od.is_change_report_apply = 1
                ";
        return $this->exec_query($sql, array($order_id));
    }

    /**
     * get_clients which closing_date, payment_month_cd, payment_day_cd => Get following name in gui part element
     * @author: Phong
     * @created date: 8/July/2015
     * @return multitype:
     */
    public function get_clients($params = array()) {
        $sql = "SELECT cl.id, cl.closing_date, cl.payment_month_cd, cl.payment_day_cd,
        cl.s_code, cl.status, cl.corporate_status_before, cl.corporate_status_after, cl.name,
        cl.name_kana, cl.zipcode,
        cl.address_1st, cl.address_2nd, cl.representative_name, cl.mail_address,
        cl.phone_no, cl.fax_no, cl.business_category,
        cl.division_name, cl.charge_name, cl.s_account_id_1st, cl.s_account_id_2nd,
        cl.closing_date, cl.payment_month_cd, cl.payment_day_cd,
        cl.delivery_month_cd, cl.delivery_amount, cl.gross_profit_amount, cl.gross_profit_rate,
        cl.total_delivery_amount, cl.total_gross_profit_amount, cl.total_gross_profit_rate,
        cl.remark,
        cl.credit_level, cl.is_ad_receive_payment_request, cl.credit_amount, cl.comment,
        cl.credit_date, cl.is_ad_receive_payment, cl.approval_account_id, cl.issue_s_code_date,

        CASE WHEN cl.closing_date>=0 THEN
        (SELECT name FROM gui_parts_element WHERE gui_parts_id = 2 AND code = cl.closing_date)
        ELSE ''
        END AS 'closing_date_name',

        CASE WHEN cl.payment_month_cd>=0 THEN
        (SELECT name FROM gui_parts_element WHERE gui_parts_id = 3 AND code = cl.payment_month_cd)
        ELSE ''
        END AS 'payment_month_cd_name',

        CASE WHEN cl.payment_day_cd>=0 THEN
        (SELECT name FROM gui_parts_element WHERE gui_parts_id = 4 AND code = cl.payment_day_cd)
        ELSE ''
        END AS 'payment_day_cd_name'

        FROM client cl ";

        $conditions = array();
        if (isset($params['conditions']) && ! empty($params['conditions'])) {
            foreach ($params['conditions'] as $key => $val) {
                switch ($key) {
                    case 'id':
                        $conditions[] = $key . "= '" . mysql_escape_string($val) . "'";
                        break;

                    case 's_code':
                        $conditions[] = $key . " LIKE '" . mysql_escape_string($val) . "%'";
                        break;

                    case 'name':
                        $conditions[] = $key . " LIKE '%" . mysql_escape_string($val) . "%'";
                        break;
//#7427:Start
                    case 'status':
                        $conditions[] = $key . " IN (" . implode(',', $val) .")";
                        break;
//#7427:End
                    default:
                        break;
                }
            }
        }

        $conditions[] = 'disable = ' . STATUS_ENABLE;
        $conditions[] = "s_code != ''";
        $sql = $sql . "WHERE " . implode(" AND ", $conditions);
//#7750:Start
        $sql .= 'ORDER BY cl.s_code ASC';
//#7750:End

        return $this->exec_query($sql);
    }

    /**
     * Count order which has status = 1 (unavailable)
     * @param unknown $client_id
     * @return multitype:
     */
    public function count_order_unavailable($client_id) {
        $sql = "SELECT count(*) AS count_order
                FROM `order`
                INNER JOIN order_detail
                ON (`order`.id = order_detail.order_id)
                WHERE `order`.client_id = ?
                AND order_detail.order_status = 1
                AND `order`.`disable` = 0
                AND order_detail.`disable` = 0";
        $this->set_single();
        return $this->exec_query($sql, array($client_id));
    }

    public function get_order_detail_for_print_report($id) {
        return $this->db->select('
                              od.*
                            , p.name as product_name
                            , gpe.name as tax_name
                            ')
                        ->from('order_detail od')
                        ->join('product p', 'p.id = od.product_id')
                        ->join('gui_parts_element gpe', 'gpe.code = od.tax_type')
                        ->where('od.order_id', $id)
                        ->where('gpe.gui_parts_id', GUI_PART_TAX_DIVISION)
                        ->where('od.disable', 0)
                        ->where('gpe.disable', 0)
                        ->get()
                        ->result_array();


    }

    public function get_order_for_print_report($id) {
        $this->load->model('client_model');

        $order = $this->db->select('
                                  o.id
                                , o.j_code
                                , o.zipcode
                                , o.address_1st
                                , o.address_2nd
                                , o.phone_no
                                , o.fax_no
                                , o.total_amount_tax
                                , o.total_amount
                                , o.division_name
                                , o.is_ad_receive_payment
                                , o.j_account_id
                                , o.remark
                                , o.lastup_datetime
                                , a.name as account_name
                                , c.s_code
                                , c.name as client_name
                                , c.name_kana as client_name_kana
                                , c.corporate_status_before
                                , c.corporate_status_after
                                , c.closing_date
                                , c.payment_month_cd
                                , c.payment_day_cd
                                , d.zipcode as department_zipcode
                                , d.address_1 as department_address_1
                                , d.address_2 as department_address_2
                                , d.fax_no as department_fax_no
                                , d.phone_no as department_phone_no
                                , bu.name as business_unit_name
                            ')
                            ->from('order o')
                            ->join('client c', 'c.id = o.client_id')
                            ->join('account a', 'a.id = o.j_account_id')
                            ->join('department d', 'd.id = a.department_id')
                            ->join('business_unit bu', 'bu.id = d.business_unit_id')
                            ->where('o.id', $id)
                            ->where('o.disable', 0)
                            ->where('c.disable', 0)
                            ->where('a.disable', 0)
                            ->where('d.disable', 0)
                            ->where('bu.disable', 0)
                            ->get()
                            ->row_array();
        if (!empty($order)) {
            $order['closing_date_name']  = $this->client_model->payment_closing_date_options($order['closing_date']);
            $order['payment_month_name'] = $this->client_model->payment_month_options($order['payment_month_cd']);
            $order['payment_date_name']  = $this->client_model->payment_date_options($order['payment_day_cd']);
            $order['details']            = $this->get_order_detail_for_print_report($order['id']);

            $this->load->model('consumption_tax_model');

            $tax_rate = $this->consumption_tax_model->get_rate();

            $cost_total_price = $consumption_tax = $tax_total = 0;
            $can_print = false;
            foreach ($order['details'] as $detail) {
                if ($detail['order_status'] == 1 || $detail['order_status'] == 2 || $detail['order_status'] == 3) {
                    $can_print = true;
                }
//#7619:Start --> Comment out source code
                // Only tax type is 「内」 has the tax included in amount
//                 $no_tax_amount = ($detail['tax_type'] != TAX_INCLUDED) ? $detail['amount'] : ($detail['amount'] / (1 + ($tax_rate['rate'] / 100)));
//                 $cost_total_price += $no_tax_amount;
//#7619:End
                // Only tax type is 「非」 is nontaxable, means no tax needed for the product price
//#7601:Start
//                 $consumption_tax += ($detail['tax_type'] != TAX_NON) ? ($no_tax_amount * ($tax_rate['rate'] / 100)) : 0;
//#7601:End
            }
//#7619:Start
//             $order['cost_total_price'] = f_ceil($cost_total_price);
//#8216:Start
//          $order['cost_total_price'] = $order['total_amount'] - $order['total_amount_tax'];
            $order['cost_total_price'] = $order['total_amount'];
//#8216:End
//#7619:End
//#7601:Start
//             $order['consumption_tax']  = f_floor($consumption_tax);
//#8216:Start
//            $order['consumption_tax']  = f_floor($order['total_amount_tax']);
              $order['consumption_tax']  = f_floor($order['total_amount_tax'] - $order['total_amount']);
//#8216:En
//#7601:End
//#7619:Start
//             $order['total_amount']     = $order['cost_total_price'] + $order['consumption_tax'];
//#7619:End
        }
        if (!$can_print) return false;
        return $order;
    }

    public function get_order_status($order_id, $order_status) {
        $order_detail = $this->db->select('*')
                ->from('order_detail')
                ->where('order_status', $order_status)
                ->where('order_id', $order_id)
                ->where('disable', 0)
                ->get()
                ->row_array();
        return $order_detail;
    }

    public function get_order_by_id($order_id) {
        return $this->get_item_by_id('`order`', $order_id);
    }
    public function get_order_detail_id_and_is_cancel_request($order_id) {

        //#6741 : MODIFY s
        $sql = "SELECT od.id, od.is_cancel_request, od.branch_cd, p.name p_name, od.`contents`, od.delivery_date, od.amount, od.gross_profit_amount, od.gross_profit_rate
                FROM order_detail od
                    INNER JOIN `order` o ON od.order_id = o.id
                    INNER JOIN `product` p ON p.id = od.product_id
                        WHERE o.id = ?
                    AND o.disable = 0
                    AND od.disable = 0
                    AND od.is_change_report_apply = 1";
        //#6741 : MODIFY E
        return $this->exec_query($sql, $order_id);
    }

    public function get_business_unit_of_logged_user($user_id) {
        $sql = "SELECT id, name, is_product_division, finish_work_report_template
                FROM business_unit
                WHERE id IN
                (SELECT business_unit_id AS id FROM department WHERE id IN
                    (SELECT  department_id AS id FROM account WHERE id = ?)
                )";
        $this->set_single();
        return $this->exec_query($sql, $user_id);
    }

    /**
     * Use in approve/reject (after change report)
     * @param unknown $order_id
     * @return multitype:
     */
    public function get_master_information($order_id) {
        $sql = "SELECT
                o.id AS o_id,
                o.`comment` AS o_comment,
                o.j_code AS o_j_code,
                o.j_account_id AS o_account_id,
                c.s_code AS c_s_code,
                c.name AS c_name,
                c.status AS client_status,
                c.name_kana AS c_name_kana,
                o.zipcode AS c_zipcode,
                   o.address_1st AS o_address_1st,
                o.address_2nd AS o_address_2nd,
                o.phone_no AS o_phone_no,
                o.fax_no AS o_fax_no,
                o.division_name AS o_division_name,
                o.charge_name AS o_charge_name,
                o.is_ad_receive_payment AS o_is_ad_receive_payment,
                CASE WHEN c.closing_date >= 0 THEN
                (SELECT name FROM gui_parts_element WHERE gui_parts_id = ? AND code = c.closing_date)
                ELSE ''
                END AS 'closing_date_name',

                CASE WHEN c.payment_month_cd >= 0 THEN
                (SELECT name FROM gui_parts_element WHERE gui_parts_id = ? AND code = c.payment_month_cd)
                ELSE ''
                END AS 'payment_month_cd_name',

                CASE WHEN c.payment_day_cd >= 0 THEN
                (SELECT name FROM gui_parts_element WHERE gui_parts_id = ? AND code = c.payment_day_cd)
                ELSE ''
                END AS 'payment_day_cd_name',
                a.name AS a_name,
                o.order_regist_date AS o_order_regist_date,
                o.remark AS o_remark, o.id AS o_id,
        		IF(o.order_approval_date = '0000-00-00', '', o.order_approval_date) AS o_order_approval_date
                FROM
                `order` o JOIN `client` c ON o.client_id = c.id
                JOIN account a ON o.j_account_id = a.id
                WHERE o.disable = 0 AND c.disable = 0 AND a.disable = 0 AND o.id = ?";
        $this->set_single();
        return $this->exec_query($sql, array(self::GUI_PART_CLOSING_DATE, self::GUI_PART_PAYMENT_MONTH, self::GUI_PART_PAYMENT_DATE, $order_id));
    }

    /**
     * Use in approve/reject (after change report)
     * @param unknown $order_id
     * @return multitype:
     */
    #7085:Start
    #
    public function get_detail_information($order_id) {
        $sql = "
                SELECT od.branch_cd AS od_branch_cd,
                od.id AS od_detail_id,
                od.order_status AS od_order_status,
                CASE WHEN od.order_status>=0 THEN
                    (SELECT name
                    FROM gui_parts_element
                    WHERE gui_parts_id = ?
                        AND code = od.order_status
                        AND disable = 0
                        AND is_no_add = 0
                        ORDER BY sequence
                        LIMIT 1
                    )
                ELSE ''
                END AS 'od_order_status_name',
                od.is_change_report_apply AS od_is_change_report_apply,
                p.name AS p_name,
                od.`contents` AS od_contents,
#7085:Start - format date
                DATE_FORMAT(od.delivery_date, '%Y/%m/%d') AS od_delivery_date,
                DATE_FORMAT(od.billing_date, '%Y/%m/%d') AS od_billing_date,
                DATE_FORMAT(od.payment_date, '%Y/%m/%d') AS od_payment_date,
#7085:End
                od.is_cancel_request AS od_is_cancel_request,
                od.shift_color_cd AS od_shift_color_cd,
                od.quantity AS od_quantity,
                od.tax_type AS od_tax_type,
                od.unit_price AS od_unit_price,
                od.amount AS od_amount,
                od.cost_total_price AS od_cost_total_price,
                od.cost_unit_price AS od_cost_unit_price,
                od.gross_profit_amount AS od_gross_profit_amount,
                od.gross_profit_rate AS od_gross_profit_rate
                FROM
                order_detail od JOIN `order` o ON od.order_id = o.id
                JOIN product p ON od.product_id = p.id
                WHERE o.disable = 0 AND od.disable = 0 AND o.id = ?
                ";
        return $this->exec_query($sql, array(self::GUI_PART_ORDER_STATUS, $order_id));
    }
    /**
     * Use in changed report approve/reject screen
     * @param unknown $order_id
     * @return multitype:
     */
    public function get_detail_log_information($order_id) {
        $sql = "SELECT
                odl.id AS order_detail_log_id,
                odl.order_detail_id AS od_detail_id,
                p.name AS p_name,
                odl.`contents` AS od_contents,
#7085:Start - format date
                DATE_FORMAT(odl.delivery_date, '%Y/%m/%d') AS od_delivery_date,
                DATE_FORMAT(odl.billing_date, '%Y/%m/%d') AS od_billing_date,
                DATE_FORMAT(odl.payment_date, '%Y/%m/%d') AS od_payment_date,
#7085:End
                odl.quantity AS od_quantity,
                odl.tax_type AS od_tax_type,
                odl.unit_price AS od_unit_price,
                odl.amount AS od_amount,
                odl.cost_unit_price AS od_cost_unit_price,
                odl.cost_total_price AS od_cost_total_price,
                odl.gross_profit_amount AS od_gross_profit_amount,
                odl.gross_profit_rate AS od_gross_profit_rate
                FROM
                order_detail_log odl LEFT JOIN product p ON odl.product_id = p.id
                WHERE odl.`disable` = 0 AND odl.order_detail_id IN
                (
                    SELECT * FROM
                    (
                        SELECT od.id
                        FROM order_detail od INNER JOIN `order` o ON o.id = od.order_id
                        WHERE o.id = ?
                    ) AS p
                )
                ";
        return $this->exec_query($sql, $order_id);
    }

    /**
     * Get detail for view normally (not changed report view)
     * @param unknown $order_id
     * @return multitype:
     */
    public function get_detail_information_view_normally($order_id) {
        $sql = "
                SELECT
                    od.id AS od_detail_id,
                    od.branch_cd AS od_branch_cd,
                    od.is_ad_sales_slip_input AS o_is_ad_receive_payment_sales_input,
                    od.is_order_input AS o_is_order_input,
                    o.is_ad_receive_payment AS o_is_ad_receive_payment,
                    od.is_acceptance_input AS o_is_acceptance_input,
                    od.is_sales_slip_input AS o_is_sales_slip_input,
                    od.is_invoice_issue AS o_is_invoice_issue,
                    od.is_ad_invoice_issue AS o_is_ad_invoice_issue,
                    o.j_account_id AS o_account_id,
                    a.name AS a_name,
                    c.s_code AS c_s_code,
                    c.name AS c_name,
                    od.order_status AS od_order_status,
                    CASE WHEN od.order_status>=0 THEN
                    (SELECT name FROM gui_parts_element WHERE gui_parts_id = 9 AND code = od.order_status)
                    ELSE ''
                    END AS 'od_order_status_name',
                    p.name AS p_name,
                    od.`contents` AS od_contents,
                    od.delivery_date AS od_delivery_date,
                    od.billing_date AS od_billing_date,
                    od.payment_date AS od_payment_date,
                    od.quantity AS od_quantity,
                    od.tax_type AS od_tax_type,
                    od.unit_price AS od_unit_price,
                    od.amount AS od_amount,
                    od.cost_unit_price AS od_cost_unit_price,
                    od.cost_total_price AS od_cost_total_price,
                    od.gross_profit_amount AS od_gross_profit_amount,
                    od.gross_profit_rate AS od_gross_profit_rate
                FROM
                    `order` o JOIN order_detail od ON o.id = od.order_id
                    JOIN account a ON a.id = o.j_account_id
                    JOIN product p ON p.id = od.product_id
                    JOIN `client` c ON o.client_id = c.id
                WHERE o.`disable` = 0 AND od.`disable` = 0 AND o.id = ?
                ";
        return $this->exec_query($sql, $order_id);
    }

    /**
     * check type of view in view detail screen is changed report view or normally view
     * @param unknown $order_id
     */
    public function check_type_of_view_is_changed_report($order_id) {
        $sql = "SELECT od.order_status, od.is_change_report_apply
                FROM `order` o JOIN order_detail od ON o.id = od.order_id
                WHERE o.`disable` = 0 AND od.`disable` = 0 AND o.id = ?
                ";
        $result =  $this->exec_query($sql, $order_id);
        $check_status = false;
        $check_is_change_report_apply = false;
        foreach($result as $r) {
            if($r['order_status'] == 2 || $r['order_status'] == 3) {
                $check_status = true;
            }
            if($r['is_change_report_apply'] == 1) {
                $check_is_change_report_apply = true;
            }
        }
        if($check_status && $check_is_change_report_apply) {
            return true;
        }
        return false;
    }

    public function get_detail_information_view_changed_report($order_id) {
        $sql = " SELECT
                o.j_account_id AS o_account_id,
                o.is_ad_receive_payment AS o_is_ad_receive_payment,
                od.branch_cd AS od_branch_cd,
                od.id AS od_detail_id,
                od.is_ad_sales_slip_input AS o_is_ad_sales_slip_input,
                od.is_ad_invoice_issue AS o_is_ad_invoice_issue,
                od.is_order_input AS o_is_order_input,
                od.is_acceptance_input AS o_is_acceptance_input,
                od.is_sales_slip_input AS o_is_sales_slip_input,
                od.is_invoice_issue AS o_is_invoice_issue,
                od.order_status AS od_order_status,
                CASE WHEN od.order_status>=0 THEN
                (SELECT name FROM gui_parts_element WHERE gui_parts_id = 9 AND code = od.order_status)
                ELSE ''
                END AS 'od_order_status_name',
                p.name AS p_name,
                od.`contents` AS od_contents,
#7085:Start
                DATE_FORMAT(od.delivery_date, '%Y/%m/%d') AS od_delivery_date,
                DATE_FORMAT(od.billing_date, '%Y/%m/%d') AS od_billing_date,
                DATE_FORMAT(od.payment_date, '%Y/%m/%d') AS od_payment_date,
#7085:End
                od.is_cancel_request AS od_is_cancel_request,
                od.shift_color_cd AS od_shift_color_cd,
                od.quantity AS od_quantity,
                od.tax_type AS od_tax_type,
                od.unit_price AS od_unit_price,
                od.amount AS od_amount,
                od.cost_unit_price AS od_cost_unit_price,
                od.cost_total_price AS od_cost_total_price,
                od.gross_profit_amount AS od_gross_profit_amount,
                od.gross_profit_rate AS od_gross_profit_rate,
                od.is_change_report_apply AS od_is_change_report_apply
                FROM
                 `order` o
                JOIN order_detail od ON o.id = od.order_id
                JOIN account a ON a.id = o.j_account_id
                JOIN product p ON p.id = od.product_id
                JOIN `client` c ON o.client_id = c.id
                WHERE o.`disable` = 0 AND od.`disable` = 0 AND o.id = ?
                ";
        return $this->exec_query($sql, $order_id);
    }

    /**
     * Get last month profit
     *
     * @param int $account_id , array $last_month
     * @return integer
     * @author Luan
     * @since 2015-07-30
     */
    public function get_sale_report_last_month_prophit($account_id,$last_month) {
        $this->load->model('consumption_tax_model');
        //closing_accountance
        $query = $this->db->select('*')
                    ->from('closing_accountant ca')
 //                   ->where('YEAR(`ca`.`close_date`)', $last_month[0])
   //                 ->where('MONTH(`ca`.`close_date`)', $last_month[1])
                    ->like('ca.close_date ', $last_month[0].'-'.$last_month[1], 'after')
                    ->where('ca.disable', '0')
                    ->get();
        $closing_accountant = $query->result();
        if(empty($closing_accountant)) return 0;
        $result = 0;
        //sales_slip_detail
        $query = $this->db->select('(o.j_code) as j_code , (od.branch_cd) as branch_cd ,(ssd.delivery_date) as ssd_delivery_date ,(ssd.amount) as ssd_amount , (ssd.tax_type) as ssd_tax_type')
                    ->from('order o')
                    ->join('order_detail od', 'o.id = od.order_id')
                    ->join('sales_slip_detail ssd', 'o.j_code = ssd.j_code AND od.branch_cd = ssd.branch_cd')
                    ->join('sales_slip ss', 'ssd.sales_slip_id = ss.id')
//                    ->where('YEAR(`ssd`.`delivery_date`)', $last_month[0])
//                    ->where('MONTH(`ssd`.`delivery_date`)', $last_month[1])
                    ->like('ssd.delivery_date ', $last_month[0].'-'.$last_month[1], 'after')
                    ->where('ssd.is_ad_receive_payment ', '0')
                    ->where('o.j_account_id', $account_id)
                    ->where('od.disable', '0')
                    ->where('o.disable', '0')
                    ->where('ssd.disable', '0')
                    ->where('ss.sales_slip_status', SALES_STATUS_INVOICE_ISSUED)
                    ->where('ssd.`sales_slip_id <>', '0')
//                    ->group_by('o.j_code , od.branch_cd')
                    ->get();
        $tmp = $query->result_array();
        foreach ($tmp as $record) {
            if($record['ssd_tax_type'] == '2'){
                $tax_rate = $this->consumption_tax_model->get_tax_rate_by_date(str_replace('-', '', $record['ssd_delivery_date']));
                $result += f_ceil($record['ssd_amount']/(1+(intval($tax_rate)/100)));
            }
            else{
                 $result += intval($record['ssd_amount']);
            }
        }

        //order_acceptance
//        $query = $this->db->select('SUM(oa.amount) as oa_amount')
//                ->from('order_acceptance oa')
//                ->where('oa.j_code',$record['j_code'])
//                ->where('oa.branch_cd',$record['branch_cd'])
//                ->where('oa.disable','0')
//                ->get();
//        $oa_amount = $query->result();
//        $result -= isset($oa_amount[0]->oa_amount)?$oa_amount[0]->oa_amount:0;

        // If no jcode in order_acceptance
        $query = $this->db->select('SUM(oa.amount) as oa_amount')
                ->from('order_acceptance oa')
                ->where('oa.summary_month', $last_month[0].'-'.$last_month[1])
//                ->where('oa.j_code <>', "")
//                ->where('oa.branch_cd <>', "")
                ->where('oa.disable','0')
                ->where('oa.account_id', $account_id)
                ->get();
        $oa_amount_no_jcode = $query->result();

        $result -= isset($oa_amount_no_jcode[0]->oa_amount)?$oa_amount_no_jcode[0]->oa_amount:0;

        //order_detail
        $query = $this->db->select(' (od.gross_profit_amount) as od_gross_profit_amount, (od.delivery_date) as od_delivery_date, (od.tax_type) as od_tax_type')
                    ->from('order o')
                    ->join('order_detail od', 'o.id = od.order_id')
                    ->join('sales_slip_detail ssd', 'o.j_code = ssd.j_code AND od.branch_cd = ssd.branch_cd')
//                    ->where('YEAR(`ssd`.`delivery_date`)', $last_month[0])
//                    ->where('MONTH(`ssd`.`delivery_date`)', $last_month[1])
                    ->like('ssd.delivery_date ', $last_month[0].'-'.$last_month[1], 'after')
                    ->where('ssd.is_ad_receive_payment ', '0')
                    ->where('o.j_account_id', $account_id)
                    ->where('od.order_status IN ('.ORDER_STATUS_CONFIRMED.')' )
                    ->where('od.disable', '0')
                    ->where('o.disable', '0')
                    ->where('ssd.disable', '0')
                    ->group_by('o.j_code , od.branch_cd')
                    ->get();
        $od_data = $query->result_array();

        if(empty($od_data)) return $result;
        foreach ($od_data as $record) {
//            if($record['od_tax_type'] == '2'){
//                $tax_rate = $this->consumption_tax_model->get_tax_rate_by_date(str_replace('-', '', $record['od_delivery_date']));
//                $result -= f_ceil($record['od_gross_profit_amount']/(1+(intval($tax_rate)/100)));
//            }
//            else{
//                 $result -= intval($record['od_gross_profit_amount']);
//            }
              $result -= intval($record['od_gross_profit_amount']);

        }
        return $result;

    }

     /**
     * Get last month summary
     *
     * @param int $account_id , array $last_month, array $current_month
     * @return integer
     * @author Luan
     * @since 2015-07-30
     */

    public function get_sale_report_last_month_summary_month($account_id, $last_month, $current_month) {
        $result = 0;
        $query = $this->db->select('(od.gross_profit_amount) as od_gross_profit_amount, (od.delivery_date) as od_delivery_date, (od.tax_type) as od_tax_type')
                    ->from('order o')
                    ->join('order_detail od', 'o.id = od.order_id')
//                    ->join('sales_slip_detail ssd', 'o.j_code = ssd.j_code AND od.branch_cd = ssd.branch_cd')
//                    ->where('YEAR(`od`.`delivery_date`)', $last_month[0])
//                    ->where('MONTH(`od`.`delivery_date`)', $last_month[1])
                    ->like('od.delivery_date', $last_month[0].'-'.$last_month[1], 'after')
//                    ->where('YEAR(`o`.`order_approval_date`)', $current_month[0])
//                    ->where('MONTH(`o`.`order_approval_date`)', $current_month[1])
                    ->like('o.order_approval_date',$current_month[0].'-'.$current_month[1], 'after')
                    ->where('o.j_account_id', $account_id)
                    ->where('od.disable', '0')
                    ->where('o.disable', '0')
                    ->group_by('o.j_code , od.branch_cd')
                    ->get();
        $tmp = $query->result_array();
        foreach ($tmp as $record) {
            $result += intval($record['od_gross_profit_amount']) ;
        }
//        foreach ($tmp as $record) {
//            if($record['od_tax_type'] == '2'){
//                $tax_rate = $this->consumption_tax_model->get_tax_rate_by_date(str_replace('-', '', $record['od_delivery_date']));
//                $result += f_ceil($record['od_gross_profit_amount']/(1+(intval($tax_rate)/100)));
//            }
//            else{
//                $result += intval($record['od_gross_profit_amount']);
//            }

//        }
        return $result;

    }

     /**
     * Get cancel
     *
     * @param int $account_id , array $last_month, array $current_month, array $next_month, array $month_after_next
     * @return integer
     * @author Luan
     * @since 2015-07-30
     */
    public function get_sale_report_cancel($account_id, $last_month, $current_month, $target_month) {
        $result = 0;
        $query = $this->db->select('(od.gross_profit_amount) as od_gross_profit_amount, (od.delivery_date) as od_delivery_date, (od.tax_type) as od_tax_type')
                    ->from('order o')
                    ->join('order_detail od', 'o.id = od.order_id')
//                    ->join('sales_slip_detail ssd', 'o.j_code = ssd.j_code AND od.branch_cd = ssd.branch_cd')
//                    ->where('((YEAR(`o`.`order_approval_date`) = '.$last_month[0].' AND MONTH(`o`.`order_approval_date`) <= '.$last_month[1].') OR ( YEAR(`o`.`order_approval_date`) > 2000 AND YEAR(`o`.`order_approval_date`) <  '.($last_month[0]).'))')
                    ->where('o.order_approval_date <= ',$last_month[0].'-'.$last_month[1].'-'.$last_month[2])
//                    ->where('YEAR(`od`.`cancel_datetime`)', $current_month[0])
//                    ->where('MONTH(`od`.`cancel_datetime`)', $current_month[1])
                    ->like('od.cancel_datetime', $current_month[0].'-'.$current_month[1], 'after')
//                    ->where('((YEAR(`ssd`.`delivery_date`) = '.$current_month[0].' AND MONTH(`ssd`.`delivery_date`) >= '.$current_month[1].') OR ( YEAR(`ssd`.`delivery_date`) > '.($current_month[0]).'))')
//                    ->where('((YEAR(`ssd`.`delivery_date`) = '.$target_month[0].' AND MONTH(`ssd`.`delivery_date`) <= '.$target_month[1].') OR ( YEAR(`ssd`.`delivery_date`) < '.($target_month[0]).'))')
                    ->where('od.delivery_date >= ',$last_month[0].'-'.$last_month[1].'-01')
                    ->where('od.delivery_date <= ',$target_month[0].'-'.$target_month[1].'-'.$target_month[2])
                    ->where('od.order_status IN ('.ORDER_STATUS_CANCELLED.')' )
                    ->where('o.j_account_id', $account_id)
                    ->where('od.disable', '0')
                    ->where('o.disable', '0')
//                    ->group_by('o.j_code , od.branch_cd')
                    ->get();
        $tmp = $query->result_array();
        foreach ($tmp as $record) {
            $result += intval($record['od_gross_profit_amount']) ;
        }
//        foreach ($tmp as $record) {
//            if($record['od_tax_type'] == '2'){
//                $tax_rate = $this->consumption_tax_model->get_tax_rate_by_date(str_replace('-', '', $record['od_delivery_date']));
//                $result += f_ceil($record['od_gross_profit_amount']/(1+(intval($tax_rate)/100)));
//            }
//            else{
//                $result += intval($record['od_gross_profit_amount']);
//            }

//        }
        return $result;
    }

     /**
     * Get chenge schedule ahead data
     *
     * @param int $account_id , array $last_month, array $current_month
     * @return integer
     * @author kobayashi
     * @since 2015-09-30
     */
    public function get_sale_change_delivery_date_ahead($account_id, $last_month, $current_month) {
        $result = 0;
        $query = $this->db->select('(od.gross_profit_amount) as od_gross_profit_amount, (od.delivery_date) as od_delivery_date, (od.tax_type) as od_tax_type')
                    ->from('order o')
                    ->join('order_detail od', 'o.id = od.order_id')
                    ->where('od.original_delivery_date <>', "0000-00-00")
                    ->like('od.change_report_approval_date', $current_month[0].'-'.$current_month[1], 'after')
                    ->where('DATE_FORMAT(od.original_delivery_date,"%Y/%m") > DATE_FORMAT(od.delivery_date, "%Y/%m")')
                    ->where('od.order_status IN ('.ORDER_STATUS_UNDETERMINED.','.ORDER_STATUS_CONFIRMED.')' )
                    ->where('o.j_account_id', $account_id)
                    ->where('od.disable', '0')
                    ->where('o.disable', '0')
                    ->get();
        $tmp = $query->result_array();
        foreach ($tmp as $record) {
            $result += intval($record['od_gross_profit_amount']) ;
        }
//        foreach ($tmp as $record) {
//            if($record['od_tax_type'] == '2'){
//                $tax_rate = $this->consumption_tax_model->get_tax_rate_by_date(str_replace('-', '', $record['od_delivery_date']));
//                $result += f_ceil($record['od_gross_profit_amount']/(1+(intval($tax_rate)/100)));
//            }
//            else{
//                $result += intval($record['od_gross_profit_amount']);
//            }

//        }
        return $result;
    }


     /**
     * Get month summary
     *
     * @param int $account_id , array $current_month, array $next_month, array $month_after_next
     * @return integer
     * @author Luan
     * @since 2015-07-30
     */
    public function get_sale_report_summary_month($account_id, $current_month, $target_month) {
        $result= 0;

        $query = $this->db->select('(od.gross_profit_amount) as od_gross_profit_amount, (od.delivery_date) as od_delivery_date, (od.tax_type) as od_tax_type')
                    ->from('order o')
                    ->join('order_detail od', 'o.id = od.order_id')
                    ->where('(od.delivery_date >= ', $current_month[0].'-'.$current_month[1].'-'.$current_month[2])
                    ->where('od.delivery_date <= ', $target_month[0].'-'.$target_month[1].'-'.$target_month[2])
                    ->where('od.original_delivery_date', "0000-00-00")
                    ->or_where('od.original_delivery_date >= ',$current_month[0].'-'.$current_month[1].'-'.$current_month[2])
                    ->where('od.original_delivery_date <= ', $target_month[0].'-'.$target_month[1].'-'.$target_month[2])
                    ->where('DATE_FORMAT(od.original_delivery_date, "%Y/%m") < DATE_FORMAT(od.delivery_date, "%Y/%m"))')
                    ->like('o.order_approval_date',$current_month[0].'-'.$current_month[1],'after')
                    ->where('od.order_status IN ('.ORDER_STATUS_UNDETERMINED.','.ORDER_STATUS_CONFIRMED.')' )
                    ->where('o.j_account_id', $account_id)
                    ->where('od.disable', '0')
                    ->where('o.disable', '0')
//                    ->group_by('o.j_code , od.branch_cd')
                    ->get();
        $tmp = $query->result_array();
        foreach ($tmp as $record) {
            $result += intval($record['od_gross_profit_amount']);
        }
//        foreach ($tmp as $record) {
//            if($record['od_tax_type'] == '2'){
//                $tax_rate = $this->consumption_tax_model->get_tax_rate_by_date(str_replace('-', '', $record['od_delivery_date']));
//                $result += f_ceil($record['od_gross_profit_amount']/(1+(intval($tax_rate)/100)));
//            }
//            else{
//                $result += intval($record['od_gross_profit_amount']);
//            }
//
//        }
        return $result;
    }

     /**
     * Get last month report
     *
     * @param int $account_id , array $last_month, array $current_month, array $next_month, array $month_after_next
     * @return integer
     * @author Luan
     * @since 2015-07-30
     */
    public function get_sale_report_last_month_report($account_id, $last_month, $target_month) {
        $result= 0;

        $query = $this->db->select('(od.gross_profit_amount) as od_gross_profit_amount, (od.delivery_date) as od_delivery_date, (od.tax_type) as od_tax_type')
                    ->from('order o')
                    ->join('order_detail od', 'o.id = od.order_id')
                    ->where('o.order_approval_date <= ',$last_month[0].'-'.$last_month[1].'-'.$last_month[2])
                    ->like('(od.delivery_date ',$target_month[0].'-'.$target_month[1], 'after')
                    ->where('od.original_delivery_date', "0000-00-00")
                    ->or_like('od.original_delivery_date ',$target_month[0].'-'.$target_month[1], 'after')
                    ->where('DATE_FORMAT(od.original_delivery_date, "%Y/%m") < DATE_FORMAT(od.delivery_date, "%Y/%m"))')
                    ->where('od.order_status IN ('.ORDER_STATUS_UNDETERMINED.','.ORDER_STATUS_CONFIRMED.','.ORDER_STATUS_CANCELLED.')' )
                    ->where('o.j_account_id', $account_id)
                    ->where('od.disable', '0')
                    ->where('o.disable', '0')
//                    ->group_by('o.j_code , od.branch_cd')
                    ->get();
        $tmp = $query->result_array();
        foreach ($tmp as $record) {
            $result += intval($record['od_gross_profit_amount']) ;
        }
//        foreach ($tmp as $record) {
//            if($record['od_tax_type'] == '2'){
//                $tax_rate = $this->consumption_tax_model->get_tax_rate_by_date(str_replace('-', '', $record['od_delivery_date']));
//                $result += f_ceil($record['od_gross_profit_amount']/(1+(intval($tax_rate)/100)));
//            }
//            else{
//                $result += intval($record['od_gross_profit_amount']);
//            }

//        }
        return $result;
    }

     /**
     * Get sales
     *
     * @param int $account_id , array $last_month, array $current_month, array $next_month, array $month_after_next
     * @return integer
     * @author Luan
     * @since 2015-07-30
     */
    public function get_sale_report_sales($account_id, $last_month, $current_month, $target_month) {
        $this->load->model('consumption_tax_model');
        $result = 0;

        //前月納品計上分
        $query = $this->db->select('(od.amount) as od_amount, (od.tax_type) as od_tax_type, (od.delivery_date) as od_delivery_date')
                    ->from('order o')
                    ->join('order_detail od', 'o.id = od.order_id')
//                    ->join('sales_slip_detail ssd', 'o.j_code = ssd.j_code AND od.branch_cd = ssd.branch_cd')
                    ->like('od.delivery_date', $last_month[0].'-'.$last_month[1], 'after')
                    ->like('o.order_approval_date',$current_month[0].'-'.$current_month[1], 'after')
//                    ->where('YEAR(`od`.`delivery_date`)', $last_month[0])
//                    ->where('MONTH(`od`.`delivery_date`)', $last_month[1])
//                    ->where('YEAR(`o`.`order_approval_date`)', $current_month[0])
//                    ->where('MONTH(`o`.`order_approval_date`)', $current_month[1])
                    ->where('o.j_account_id', $account_id)
                    ->where('od.disable', '0')
                    ->where('o.disable', '0')
                    ->group_by('o.j_code , od.branch_cd')
                    ->get();
        $tmp = $query->result_array();
        foreach ($tmp as $record) {
            if($record['od_tax_type'] == '2'){
                $tax_rate = $this->consumption_tax_model->get_tax_rate_by_date(str_replace('-', '', $record['od_delivery_date']));
                $result += f_ceil($record['od_amount']/(1+(intval($tax_rate)/100)));
            }
            else{
                $result += intval($record['od_amount']);
            }
        }

        //清算アップ
        $query = $this->db->select('*')
                    ->from('closing_accountant ca')
                    ->like('ca.close_date ', $last_month[0].'-'.$last_month[1], 'after')
//                    ->where('YEAR(`ca`.`close_date`)', $last_month[0])
//                    ->where('MONTH(`ca`.`close_date`)', $last_month[1])
                    ->where('ca.disable', '0')
                    ->get();
        $closing_accountant = $query->result();
        if(!empty($closing_accountant)) {
            $query = $this->db->select('(ssd.delivery_date) as ssd_delivery_date ,(ssd.amount) as ssd_amount , (ssd.tax_type) as ssd_tax_type')
                        ->from('order o')
                        ->join('order_detail od', 'o.id = od.order_id')
                        ->join('sales_slip_detail ssd', 'o.j_code = ssd.j_code AND od.branch_cd = ssd.branch_cd')
                        ->join('sales_slip ss', 'ssd.sales_slip_id = ss.id')
//                        ->where('YEAR(`ssd`.`delivery_date`)', $last_month[0])
//                        ->where('MONTH(`ssd`.`delivery_date`)', $last_month[1])
                        ->like('ssd.delivery_date ', $last_month[0].'-'.$last_month[1], 'after')
                        ->where('ssd.is_ad_receive_payment ', '0')
                        ->where('o.j_account_id', $account_id)
                        ->where('ss.sales_slip_status', SALES_STATUS_INVOICE_ISSUED)
                        ->where('ssd.`sales_slip_id <>', '0')
                        ->where('od.disable', '0')
                        ->where('o.disable', '0')
                        ->where('ssd.disable', '0')
                        ->get();
            $tmp = $query->result_array();
            foreach ($tmp as $record) {
                if($record['ssd_tax_type'] == '2'){
                    $tax_rate = $this->consumption_tax_model->get_tax_rate_by_date(str_replace('-', '', $record['ssd_delivery_date']));
                    $result += f_ceil($record['ssd_amount']/(1+(intval($tax_rate)/100)));
                }
                else{
                    $result += intval($record['ssd_amount']);
                }
            }

            $query = $this->db->select('(od.amount) as od_amount, (od.tax_type) as od_tax_type, (od.delivery_date) as od_delivery_date')
                        ->from('order o')
                        ->join('order_detail od', 'o.id = od.order_id')
                        ->join('sales_slip_detail ssd', 'o.j_code = ssd.j_code AND od.branch_cd = ssd.branch_cd')
//                      ->where('YEAR(`ssd`.`delivery_date`)', $last_month[0])
//                      ->where('MONTH(`ssd`.`delivery_date`)', $last_month[1])
                        ->like('ssd.delivery_date ', $last_month[0].'-'.$last_month[1], 'after')
                        ->where('ssd.is_ad_receive_payment ', '0')
                        ->where('o.j_account_id', $account_id)
                        ->where('od.disable', '0')
                        ->where('o.disable', '0')
                        ->where('ssd.disable', '0')
                        ->where('od.order_status IN ('.ORDER_STATUS_CONFIRMED.')' )
                        ->group_by('o.j_code , od.branch_cd')
                        ->get();
            $tmp = $query->result_array();
            foreach ($tmp as $record) {
                if($record['od_tax_type'] == '2'){
                    $tax_rate = $this->consumption_tax_model->get_tax_rate_by_date(str_replace('-', '', $record['od_delivery_date']));
                    $result -= f_ceil($record['od_amount']/(1+(intval($tax_rate)/100)));
                }
                else{
                     $result -= intval($record['od_amount']);
                }
            }
        }

        //キャンセル
        $query = $this->db->select('(od.amount) as od_amount, (od.tax_type) as od_tax_type, (od.delivery_date) as od_delivery_date')
                    ->from('order o')
                    ->join('order_detail od', 'o.id = od.order_id')
//                    ->join('sales_slip_detail ssd', 'o.j_code = ssd.j_code AND od.branch_cd = ssd.branch_cd')
//                    ->where('((YEAR(`o`.`order_approval_date`) = '.$last_month[0].' AND MONTH(`o`.`order_approval_date`) <= '.$last_month[1].') OR ( YEAR(`o`.`order_approval_date`) > 2000 AND YEAR(`o`.`order_approval_date`) <  '.($last_month[0]).'))')
                    ->where('o.order_approval_date <= ',$last_month[0].'-'.$last_month[1].'-'.$last_month[2])
//                    ->where('YEAR(`od`.`cancel_datetime`)', $current_month[0])
//                    ->where('MONTH(`od`.`cancel_datetime`)', $current_month[1])
                    ->like('od.cancel_datetime', $current_month[0].'-'.$current_month[1], 'after')
//                    ->where('((YEAR(`ssd`.`delivery_date`) = '.$current_month[0].' AND MONTH(`ssd`.`delivery_date`) >= '.$current_month[1].') OR ( YEAR(`ssd`.`delivery_date`) > '.($current_month[0]).'))')
//                    ->where('((YEAR(`ssd`.`delivery_date`) = '.$target_month[0].' AND MONTH(`ssd`.`delivery_date`) <= '.$target_month[1].') OR ( YEAR(`ssd`.`delivery_date`) < '.($target_month[0]).'))')
                    ->where('od.delivery_date >= ',$last_month[0].'-'.$last_month[1].'-01')
                    ->where('od.delivery_date <= ',$target_month[0].'-'.$target_month[1].'-'.$target_month[2])
                    ->where('od.order_status IN ('.ORDER_STATUS_CANCELLED.')' )
                    ->where('o.j_account_id', $account_id)
                    ->where('od.disable', '0')
                    ->where('o.disable', '0')
                    ->group_by('o.j_code , od.branch_cd')
                    ->get();
        $tmp = $query->result_array();
        foreach ($tmp as $record) {
            if($record['od_tax_type'] == '2'){
                $tax_rate = $this->consumption_tax_model->get_tax_rate_by_date(str_replace('-', '', $record['od_delivery_date']));
                $result -= f_ceil($record['od_amount']/(1+(intval($tax_rate)/100)));
            }
            else{
                 $result -= intval($record['od_amount']);
            }
        }

        //前倒し
        $query = $this->db->select('(od.amount) as od_amount, (od.delivery_date) as od_delivery_date, (od.tax_type) as od_tax_type')
                    ->from('order o')
                    ->join('order_detail od', 'o.id = od.order_id')
                    ->where('od.original_delivery_date <>', "0000-00-00")
                    ->like('od.change_report_approval_date', $current_month[0].'-'.$current_month[1], 'after')
                    ->where('DATE_FORMAT(od.original_delivery_date,"%Y/%m") > DATE_FORMAT(od.delivery_date, "%Y/%m")')
                    ->where('od.order_status IN ('.ORDER_STATUS_UNDETERMINED.','.ORDER_STATUS_CONFIRMED.')' )
                    ->where('o.j_account_id', $account_id)
                    ->where('od.disable', '0')
                    ->where('o.disable', '0')
                    ->get();
        $tmp = $query->result_array();
        foreach ($tmp as $record) {
            if($record['od_tax_type'] == '2'){
                $tax_rate = $this->consumption_tax_model->get_tax_rate_by_date(str_replace('-', '', $record['od_delivery_date']));
                $result += f_ceil($record['od_amount']/(1+(intval($tax_rate)/100)));
            }
            else{
                $result += intval($record['od_amount']);
            }

        }


        //今月計上分
        $query = $this->db->select('(od.amount) as od_amount, (od.tax_type) as od_tax_type, (od.delivery_date) as od_delivery_date')
                    ->from('order o')
                    ->join('order_detail od', 'o.id = od.order_id')
//                    ->join('sales_slip_detail ssd', 'o.j_code = ssd.j_code AND od.branch_cd = ssd.branch_cd')
//                    ->where('((YEAR(`ssd`.`delivery_date`) = '.$current_month[0].' AND MONTH(`ssd`.`delivery_date`) >= '.$current_month[1].') OR ( YEAR(`ssd`.`delivery_date`) > '.($current_month[0]).'))')
//                    ->where('((YEAR(`ssd`.`delivery_date`) = '.$target_month[0].' AND MONTH(`ssd`.`delivery_date`) <= '.$target_month[1].') OR ( YEAR(`ssd`.`delivery_date`) < '.($target_month[0]).'))')
//                    ->where('YEAR(`o`.`order_approval_date`)', $current_month[0])
//                    ->where('MONTH(`o`.`order_approval_date`)', $current_month[1])

//                    ->where('od.delivery_date >= ',$current_month[0].'-'.$current_month[1].'-'.$current_month[2])
//                    ->where('od.delivery_date <= ',$target_month[0].'-'.$target_month[1].'-'.$target_month[2])
//                    ->like('o.order_approval_date',$current_month[0].'-'.$current_month[1],'after')

                    ->where('(od.delivery_date >= ', $current_month[0].'-'.$current_month[1].'-'.$current_month[2])
                    ->where('od.delivery_date <= ', $target_month[0].'-'.$target_month[1].'-'.$target_month[2])
                    ->where('od.original_delivery_date', "0000-00-00")
                    ->or_where('od.original_delivery_date >= ',$current_month[0].'-'.$current_month[1].'-'.$current_month[2])
                    ->where('DATE_FORMAT(od.original_delivery_date, "%Y/%m") < DATE_FORMAT(od.delivery_date, "%Y/%m"))')
                    ->like('o.order_approval_date',$current_month[0].'-'.$current_month[1],'after')
                    ->where('od.order_status IN ('.ORDER_STATUS_UNDETERMINED.','.ORDER_STATUS_CONFIRMED.')' )
                    ->where('o.j_account_id', $account_id)
                    ->where('od.disable', '0')
                    ->where('o.disable', '0')
//                    ->group_by('o.j_code , od.branch_cd')
                    ->get();
        $tmp = $query->result_array();
        foreach ($tmp as $record) {
            if($record['od_tax_type'] == '2'){
                $tax_rate = $this->consumption_tax_model->get_tax_rate_by_date(str_replace('-', '', $record['od_delivery_date']));
                $result += f_ceil($record['od_amount']/(1+(intval($tax_rate)/100)));
            }
            else{
                 $result += intval($record['od_amount']);
            }
        }

        //前月末時点
        $query = $this->db->select('(od.amount) as od_amount, (od.tax_type) as od_tax_type, (od.delivery_date) as od_delivery_date')
                    ->from('order o')
                    ->join('order_detail od', 'o.id = od.order_id')
                    ->where('o.order_approval_date <= ',$last_month[0].'-'.$last_month[1].'-'.$last_month[2])
                    ->like('(od.delivery_date ',$target_month[0].'-'.$target_month[1], 'after')
                    ->where('od.original_delivery_date', "0000-00-00")
                    ->or_like('od.original_delivery_date ',$target_month[0].'-'.$target_month[1], 'after')
                    ->where('DATE_FORMAT(od.original_delivery_date, "%Y/%m") < DATE_FORMAT(od.delivery_date, "%Y/%m"))')
                    ->where('od.order_status IN ('.ORDER_STATUS_UNDETERMINED.','.ORDER_STATUS_CONFIRMED.', '.ORDER_STATUS_CANCELLED.')' )
                    ->where('o.j_account_id', $account_id)
                    ->where('od.disable', '0')
                    ->where('o.disable', '0')
                    ->group_by('o.j_code , od.branch_cd')
                    ->get();
        $tmp = $query->result_array();
        foreach ($tmp as $record) {
            if($record['od_tax_type'] == '2'){
                $tax_rate = $this->consumption_tax_model->get_tax_rate_by_date(str_replace('-', '', $record['od_delivery_date']));
                $result += f_ceil($record['od_amount']/(1+(intval($tax_rate)/100)));
            }
            else{
                 $result += intval($record['od_amount']);
            }
        }

        return $result;
    }
    /**
     * Get undefined orders in a month
     *
     * @param string $date
     * @return array
     *
     * @author van_thuat 2015-08-12 #5978
     */
    public function get_undefined_orders_in_month($date = '') {
        try {
            $date_obj = new DateTime($date);
        } catch (Exception $e) {
            $date_obj = new DateTime();
        }

        $next_date_obj = new DateTime($date_obj->format('Y-m-d'));
        $next_date_obj->modify('+1 months');

        $sql = "SELECT `order`.id, `order`.j_code, order_detail.branch_cd"
        . " FROM `order`"
        . " INNER JOIN order_detail ON order_detail.order_id = `order`.id"
        . "     AND order_detail.disable = " . STATUS_ENABLE
        . " WHERE `order`.disable = " . STATUS_ENABLE
        . "     AND ("
        . "         (DATE_FORMAT(order_detail.delivery_date, '%Y/%m') = '{$date_obj->format('Y/m')}'"
        . "             AND order_detail.order_status IN (" . ORDER_STATUS_UNAPPROVED . ", " . ORDER_STATUS_UNDETERMINED . ")"
        . "         )"
        . "         OR (DATE_FORMAT(order_detail.delivery_date, '%Y/%m') = '{$next_date_obj->format('Y/m')}'"
        . "             AND order_detail.order_status = " . ORDER_STATUS_UNDETERMINED
        . "             AND order_detail.is_change_report_apply = 1"
        . "             AND EXISTS ("
        . "                 SELECT 'x'"
        . "                 FROM order_detail_log"
        . "                 WHERE order_detail_log.order_detail_id = order_detail.id"
        . "                     AND DATE_FORMAT(order_detail_log.delivery_date, '%Y/%m') = '{$date_obj->format('Y/m')}'"
        . "                     AND order_detail_log.disable = " . STATUS_ENABLE
        . "                 LIMIT 1"
        . "             )"
        . "         )"
        . "     )"
        . " ORDER BY `order`.j_code, order_detail.branch_cd"
//      . "     AND ("
//      . "         EXISTS (" // Begin: check order has undefined order_detail.delivery_date in this month
//      . "             SELECT 'x'"
//      . "             FROM order_detail"
//      . "             WHERE order_detail.disable = " . STATUS_ENABLE
//      . "                 AND order_detail.order_id = `order`.id"
//      . "                 AND DATE_FORMAT(order_detail.delivery_date, '%Y/%m') = '{$date_obj->format('Y/m')}'"
//      . "                 AND order_detail.order_status IN (" . ORDER_STATUS_UNAPPROVED . ", " . ORDER_STATUS_UNDETERMINED . ")"
//      . "             LIMIT 1"
//      . "         )" // End: check order has undefined order_detail.delivery_date in this month
//      . "         OR EXISTS (" // Begin: check order has undefined order_detail.delivery_date in next month
//      . "             SELECT 'x'"
//      . "             FROM order_detail"
//      . "             INNER JOIN order_detail_log ON order_detail_log.order_detail_id = order_detail.id"
//      . "                 AND DATE_FORMAT(order_detail_log.delivery_date, '%Y/%m') = '{$date_obj->format('Y/m')}'"
//      . "                 AND order_detail_log.disable = " . STATUS_ENABLE
//      . "             WHERE order_detail.disable = " . STATUS_ENABLE
//      . "                 AND order_detail.order_id = `order`.id"
//      . "                 AND DATE_FORMAT(order_detail.delivery_date, '%Y/%m') = '{$next_date_obj->format('Y/m')}'"
//      . "                 AND order_detail.order_status = " . ORDER_STATUS_UNDETERMINED
//      . "                 AND order_detail.is_change_report_apply = 1"
//      . "             LIMIT 1"
//      ."          )" // End: check order has undefined order_detail.delivery_date in next month
//      . "     )"
        ;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /**
     * Update multiple orders by params
     *
     * @param array $params
     * @return bool $result
     *
     * @author hoang_minh
     * @since 2015-08-17
     */
    public function update_multiple_orders($params = array()) {
        if ( ! is_array($params) || empty($params)) {
            return false;
        }

        return $this->update_batch('`order`', $params['update_data'], $params['update_key']);
    }

    /**
     * @author van_don
     * Get total amount that flag is_invoice_issue of order detail = 0
     */
    public function caculating_credit_on_order_detail($client_id, $delivery_date, $payment_date, $order_id = '') {
        $list = array ();
        $query = " SELECT SUM(od.amount) AS total_amount"
                . " FROM order_detail AS od"
                . " INNER JOIN `order` AS o ON (o.id = od.order_id)"
                . " WHERE od.`disable` = 0"
                . "    AND o.`disable` = 0"
                . "    AND o.client_id = ?"
                . "    AND o.is_ad_receive_payment = " .IS_NOT_AD_RECEIVE_PAYMENT
                . "    AND od.is_invoice_issue = " .IS_NOT_INVOICE_ISSUE
                . "    AND od.payment_date > ?"
//#7983:Start
//                 . "    AND od.payment_date >= ?"
//                 . "    AND DATE_FORMAT(od.payment_date,'%Y-%m') <= DATE_FORMAT(?,'%Y-%m')"
                . "    AND od.order_status IN ?";
//                 . "    AND od.payment_date";
//#7983:End
        $list = array($client_id, $delivery_date, array(ORDER_STATUS_UNAPPROVED,ORDER_STATUS_UNDETERMINED, ORDER_STATUS_CONFIRMED));
        if ($order_id) {
            $query .= " AND o.id <> ?";
            $list[] = $order_id;
        }
        $this->set_single ();
        return $this->exec_query ($query, $list);
    }

    /**
     * @author van_don
     * Get total amount that flag is_invoice_issue of order detail = 0
     */
    public function caculating_credit_on_sales_slip($client_id, $delivery_date, $payment_date, $order_id = '') {
        $list = array ();
        $query = " SELECT SUM(distinct ss.total_amount) AS 'total_amount'"
                . " FROM sales_slip AS ss"
                . " INNER JOIN sales_slip_detail AS ssd ON (ss.id = ssd.sales_slip_id)"
                . " INNER JOIN `order` AS o ON (o.j_code = ssd.j_code )"
                . " INNER JOIN order_detail AS od ON (o.id = od.order_id  AND od.branch_cd = ssd.branch_cd)"
                . " WHERE o.`disable` = 0 "
                . " AND ((o.is_ad_receive_payment = " . IS_AD_RECEIVE_PAYMENT
                . "     AND od.is_ad_invoice_issue = " . IS_AD_INVOICE_ISSUE
                . "     AND od.is_invoice_issue = " . IS_INVOICE_ISSUE . ")"
                . "    OR ("
                . "     o.is_ad_receive_payment = " . IS_NOT_AD_RECEIVE_PAYMENT
                . "     AND od.is_invoice_issue = " . IS_INVOICE_ISSUE . "))"
                . " AND ss.is_checked = 0"
                . "    AND o.client_id = ?"
                . "    AND ss.payment_date >= ?"
                . "    AND od.order_status IN ?"
                . "    AND ss.sales_slip_status = ?";
//                 . "    AND DATE_FORMAT(ss.payment_date,'%Y-%m') <= DATE_FORMAT(?,'%Y-%m')";
        $list = array($client_id, $delivery_date, array(ORDER_STATUS_UNAPPROVED,ORDER_STATUS_UNDETERMINED, ORDER_STATUS_CONFIRMED), SALES_STATUS_INVOICE_ISSUED);
        if ($order_id) {
            $query .= " AND o.id <> ?";
            $list[] = $order_id;
        }
        $this->set_single ();
        return $this->exec_query ($query, $list);
    }

    /**
     * @author van_don
     * Get total amount that flag is_invoice_issue of order detail = 0
     */
    public function calculating_order_detail($order_id) {
        $query = " SELECT SUM(amount) AS 'total_amount'"
                ." FROM order_detail"
                ." WHERE order_detail.order_id = ?"
                ." AND order_detail.order_status IN ?"
                ." AND `disable` = 0";
        $this->set_single ();
        return $this->exec_query ($query, array($order_id, array(ORDER_STATUS_UNAPPROVED,ORDER_STATUS_UNDETERMINED, ORDER_STATUS_CONFIRMED)));
    }

    public function calculate_payment_billing_date($delivery_date, $client_id)
    {
        $this->load->models(array('client_model', 'holiday_model'));

        $client = $this->client_model->get_client_by_id($client_id);

        if ($client['is_ad_receive_payment']) {
            return false;
        }

        $billing_closing_date = $client['closing_date'];
        $payment_month = $client['payment_month_cd'];
        $payment_day_desired = $client['payment_day_cd'];

//#7769:Start
        if ($billing_closing_date == 0 || $payment_day_desired == 0) return false;
//#7769:End

        try {
            $delivery_date_obj = new DateTime($delivery_date);
            $billing_date_obj = new DateTime($delivery_date_obj->format('Y-m-1'));

            if ($delivery_date_obj->format('d') > $billing_closing_date) {
                $billing_date_obj->modify('+1 month');
            }

            $billing_day = ($billing_date_obj->format('t') < $billing_closing_date) ? $billing_date_obj->format('t') : $billing_closing_date;

            $billing_date_obj->setDate($billing_date_obj->format('Y'), $billing_date_obj->format('m'), $billing_day);

            $payment_date_obj = new DateTime($billing_date_obj->format('Y-m-1'));
            $payment_date_obj->modify('+' . $payment_month . ' month');

            $payment_day = ($payment_date_obj->format('t') < $payment_day_desired) ? $payment_date_obj->format('t') : $payment_day_desired;

            $payment_date_obj->setDate($payment_date_obj->format('Y'), $payment_date_obj->format('m'), $payment_day);

            while ($this->holiday_model->check_holiday($payment_date_obj->format('Y-m-d'))) {
                $payment_date_obj->modify('-1 day');
            }

            return array(
                'billing_date' => $billing_date_obj->format('Y-m-d'),
                'payment_date' => $payment_date_obj->format('Y-m-d')
            );

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Get sales slip details which related to orderdetail in search screen to export CSV
     * @author Phong
     * @param unknown $list
     * @return Ambigous <multitype:, mixed>
     */
    public function get_sales_slip_detail($list) {
        // Where condition of j_code. Ex: "AND j_code IN ('j1', 'j2')
        // j1, j2: Get from list of order_detail followed search result before
        $string_where_j_code = ' AND j_code IN (';
        // Where condition of branch_cd. Ex: "AND branch_cd IN ('b1', 'b2')
        // b1, b2: Get from list of order_detail followed search result before
        $string_where_branch_cd = ' AND branch_cd IN (';
        $list_j_code = array();
        $list_branch_cd = array();
        if(!empty($list)) {
            foreach($list AS $order_detail) {
                if($order_detail['j_code']!='' && !empty($order_detail['j_code'])) {
                    $list_j_code[] = $order_detail['j_code'];
                }
                if($order_detail['branch_cd']!='' && !empty($order_detail['branch_cd'])) {
                    $list_branch_cd[] = $order_detail['branch_cd'];
                }
            }
        }
        // If empty list_j_code or list_branch_cd => Return empty array
        if(!empty($list_j_code) && !empty($list_branch_cd)) {
            // Remove duplicated item
            $list_j_code = array_unique($list_j_code);
            $list_branch_cd = array_unique($list_branch_cd);
            // Create where condition clause
            foreach($list_j_code AS $j_code) {
                $string_where_j_code .= "'".$j_code."'".',';
            }
            // Remove ',' at the last string
            $string_where_j_code = substr($string_where_j_code, 0, -1);
            $string_where_j_code.=')';

            foreach($list_branch_cd AS $branch_cd) {
                $string_where_branch_cd .= "'".$branch_cd."'".',';
            }
            // Remove ',' at the last string
            $string_where_branch_cd = substr($string_where_branch_cd, 0, -1);
            $string_where_branch_cd.=')';
//#7002:Start
            // $sql = "
            //     SELECT j_code, branch_cd, tax_type, amount AS amount_original, delivery_date,
            //     IF(
            //         (tax_type = 1) OR (tax_type =3),
            //         amount,
            //         (
            //               CEIL(amount / (1 +
            //                     (
            //                         SELECT ct.rate
            //                         FROM consumption_tax ct INNER JOIN consumption_tax_adaptation cta
            //                             ON ct.consumption_tax_adaptation_id = cta.id
            //                         WHERE ct.`disable` = 0 AND cta.`disable` = 0 AND cta.adaptation_date <= delivery_date
            //                         ORDER BY cta.adaptation_date DESC
            //                         LIMIT 1
            //                     )/100
            //               ))
            //         )
            //     ) AS amount
            //     FROM sales_slip_detail
            //     WHERE disable = 0
            //     ";
            $sql = "
            SELECT j_code, branch_cd, tax_type, amount AS amount_original, delivery_date,
            IF(
                (tax_type = 1) OR (tax_type =3),
                amount,
                (
                      CEIL(amount / (1 +
                            (
                                SELECT ct.rate
                                FROM consumption_tax ct INNER JOIN consumption_tax_adaptation cta
                                    ON ct.consumption_tax_adaptation_id = cta.id
                                WHERE ct.`disable` = 0 AND cta.`disable` = 0 AND cta.adaptation_date <= delivery_date
                                ORDER BY cta.adaptation_date DESC
                                LIMIT 1
                            )/100
                      ))
                )
            ) AS amount
            FROM sales_slip_detail
            WHERE disable = 0 AND is_ad_receive_payment = 0
            ";
//#7002:End
            // Add condition search: Select only these jcode
            $sql.=$string_where_j_code;
            // Add condition search: Select only these branch_cd
            $sql.=$string_where_branch_cd;
            return $this->exec_query($sql);
        } else {
            return array();
        }
    }

    /**
     * Get order acceptance which have j_code and branch_cd in the list search of order screen to export CSV
     * @author Phong
     * @param unknown $list
     */
    public function get_order_acceptance_amount($list) {
        // Where condition of j_code. Ex: "AND j_code IN ('j1', 'j2')
        $string_where_j_code = ' AND j_code IN (';
        // Where condition of branch_cd. Ex: "AND branch_cd IN ('b1', 'b2')
        $string_where_branch_cd = ' AND branch_cd IN (';
        $list_j_code = array();
        $list_branch_cd = array();
        if(!empty($list)) {
            foreach($list AS $order_detail) {
                if($order_detail['j_code']!='' && !empty($order_detail['j_code'])) {
                    $list_j_code[] = $order_detail['j_code'];
                }
                if($order_detail['branch_cd']!='' && !empty($order_detail['branch_cd'])) {
                    $list_branch_cd[] = $order_detail['branch_cd'];
                }
            }
        }
        // If empty list_j_code or list_branch_cd => Return empty array
        if(!empty($list_j_code) && !empty($list_branch_cd)) {
            // Remove duplicated item
            $list_j_code = array_unique($list_j_code);
            $list_branch_cd = array_unique($list_branch_cd);
            // Create where condition clause
            foreach($list_j_code AS $j_code) {
                $string_where_j_code .= "'".$j_code."'".',';
            }
            // Remove ',' at the last string
            $string_where_j_code = substr($string_where_j_code, 0, -1);
            $string_where_j_code.=')';

            foreach($list_branch_cd AS $branch_cd) {
                $string_where_branch_cd .= "'".$branch_cd."'".',';
            }
            // Remove ',' at the last string
            $string_where_branch_cd = substr($string_where_branch_cd, 0, -1);
            $string_where_branch_cd.=')';

            $sql = "
                SELECT j_code, branch_cd, amount
                FROM order_acceptance
                WHERE disable = 0
                ";
            // Add condition search: Select only these jcode
            $sql.=$string_where_j_code;
            // Add condition search: Select only these branch_cd
            $sql.=$string_where_branch_cd;
            return $this->exec_query($sql);
        } else {
            return array();
        }

        /* $string_where_j_code = ' AND j_code IN (';
        $string_where_branch_cd = ' AND branch_cd IN (';
        if(!empty($list)) {
            foreach($list AS $order_detail) {
                if($order_detail['j_code']!='' && !empty($order_detail['j_code'])) {
                    $string_where_j_code .= "'".$order_detail['j_code']."'".',';
                }
                if($order_detail['branch_cd']!='' && !empty($order_detail['branch_cd'])) {
                    $string_where_branch_cd .= "'".$order_detail['branch_cd']."'".',';
                }
            }
            if(substr($string_where_j_code, -1, 1)==',') {
                // Remove ',' at the last string
                $string_where_j_code = substr($string_where_j_code, 0, -1);
            }
            $string_where_j_code.=')';
            if(substr($string_where_branch_cd, -1, 1)==',') {
                // Remove ',' at the last string
                $string_where_branch_cd = substr($string_where_branch_cd, 0, -1);
            }
            $string_where_branch_cd.=')';
        }
        $sql = "
                SELECT j_code, branch_cd, amount
                FROM order_acceptance
                WHERE disable = 0
                ";
        // Add condition search: Select only these jcode
        $sql.=$string_where_j_code;
        // Add condition search: Select only these branch_cd
        $sql.=$string_where_branch_cd;
        return $this->exec_query($sql); */
    }
//#6971: Start 2/10/2015
    /*
     * 2015-06-01 or 2015/06/01 => return array(2015, 06, 01)
     * 2015-06 or 2015/06 => return array(2015, 06)
     */
    public function separate_string($raw) {
        $delimiter = '-';
        if (strpos ( $raw, '/' ) !== false) {
            $delimiter = '/';
        }
        $parts = explode ( $delimiter, $raw );
        if($parts[1] > 0 && $parts[1] <10 && !(strpos ( $parts[1], '0' ) !== false)) {
            $parts[1] = '0'.$parts[1];
        }
        return $parts;
    }
//#6971: End 2/10/2015

    /*
     * is_ad_receive_payment = 0: Cam_tien added to fix 7002
     * Used for calculate amount sales slip detail for exporting CSV order
     * @author Phong
     * created 6/October/2015
     */
    public function get_amount_from_sales_slip_detail_by_jcode_branch_cd($j_code = "", $branch_cd = "") {
        $params = array();
        $sql = "
            SELECT SUM(t1.amount) AS amount
                FROM
                (
                    SELECT
                    IF(
                        (tax_type = 1) OR (tax_type =3),
                        amount,
                        (
                              CEIL(amount / (1 +
                                    (
                                        SELECT ct.rate
                                        FROM consumption_tax ct INNER JOIN consumption_tax_adaptation cta
                                            ON ct.consumption_tax_adaptation_id = cta.id
                                        WHERE ct.`disable` = 0 AND cta.`disable` = 0 AND cta.adaptation_date <= delivery_date
                                        ORDER BY cta.adaptation_date DESC
                                        LIMIT 1
                                    )/100
                              ))
                        )
                    ) AS amount";
//#8285:Start
//                    FROM sales_slip_detail
//                    WHERE disable = 0 AND is_ad_receive_payment = 0
             $sql .= "       FROM sales_slip_detail as ssd , sales_slip as ss
                    WHERE ssd.disable = 0 AND ss.id = ssd.sales_slip_id  AND  ssd.is_ad_receive_payment = 0 AND  ss.sales_slip_status <> ".SALES_STATUS_INVOICED_DISCARDED."
            ";
//#8285:End

        if($j_code!="" && $branch_cd!="") {
//#8285:Start
//            $sql.="  AND j_code = ? AND branch_cd = ?";
            $sql.="  AND ssd.j_code = ? AND ssd.branch_cd = ?";
//#8285:End
            $params[] = $j_code;
            $params[] = $branch_cd;
            $sql.=" )t1";
            $this->set_single();
            $result = $this->exec_query($sql, $params);
            return $result['amount'];
        }
        return "";
    }

    /*
     * Used for calculate amount order acceptance for exporting CSV order
     * @author Phong
     * @created 6/October/2015
     */
    public function get_amount_from_order_acceptance_by_jcode_branch_cd($j_code = "", $branch_cd = "") {
        $params = array();
        $sql = "SELECT SUM(amount) AS amount
                FROM order_acceptance
                WHERE disable = 0
                ";
        if($j_code!="" && $branch_cd!="") {
            $sql.=" AND j_code = ? AND branch_cd = ?";
            $params[] = $j_code;
            $params[] = $branch_cd;
            $this->set_single();
            $result = $this->exec_query($sql, $params);
            return $result['amount'];
        }
        return "";
    }
}
