<?php
class Payment_past_data extends ZR_Model
{
	public function rules () {
		$rules = array(
			'divide' => array(
				array(
					'field' => 'client_id',
					'label' => lang('lbl_s_code'),
					'rules' => 'required'
				),
				array(
					'field' => 'amount',
					'label' => lang('lbl_divide_money'),
					'rules' => 'required|numeric'
				),
				array(
					'field' => 'comment',
					'label' => lang('lbl_divide_before_comment'),
					'rules' => 'trim'
				),
				array(
					'field' => 'comment',
					'label' => lang('lbl_divide_after_comment'),
					'rules' => 'trim'
				),
			)
		);
	}

	public function get_client_past_data() {
		$query = $this->db->select('p.client_id,p.payment_date,p.amount,c.*,month(p.payment_date) as month_payment,year(p.payment_date) as year_payment,p.account_holder')
						->from('client c')

						->join('payment p','c.id = p.client_id')
						->where('p.disable',0)
						->group_by('p.client_id')
						->get()
						->result_array();
		return $query;
	}
	
	
	public function get_account_all() {
		$query = $this->db->select('c.*')
						->from('client c')
						->order_by('c.s_code', 'asc')
						->where('c.disable', 0)
						//tmp#6865: Start 2015/09/26
						->where('c.s_code !=', '')
						//tmp#6865: End
						->get()
						->result_array();
		return $query;
	}

	public function get_client_bank_name($id) {
		$query = $this->db->select('c.*,ai.account_holder')
						  ->from('client c')
						  ->join('payment ai','ai.client_id = c.id')
						  ->where('c.id', $id)
						  ->get()
						  ->result_array();
		return $query;
	}
	public function get_client_payment($id,$month,$year) {
		$query = $this->db->select('p.client_id,p.payment_date,p.amount,c.*,month(p.payment_date) as month_payment,year(p.payment_date) as year_payment,p.comment,p.account_holder')
						->from('client c')
						->join('payment p','c.id = p.client_id')
						->where('c.id', $id)
						->where('month(p.payment_date)',$month)
						->where('year(p.payment_date)',$year)
						->where('p.disable',0)
						->order_by('p.payment_date', 'ASC')
//#9588:Start
						->order_by('p.id', 'ASC')
//#9588:End
						->get()
						->result_array();

		return $query;
	}

	public function get_total_payment($id,$month_1,$year_1) {
		$query = $this->db->select('p.client_id,p.payment_date,p.amount,c.*,sum(amount) as payment_amount,month(p.payment_date) as month_payment,year(p.payment_date) as year_payment,p.account_holder')
						->from('client c')
						->join('payment p','c.id = p.client_id')
						->where('c.id', $id)
						->where('p.disable',0)
						->where('month(p.payment_date)', $month_1)
						->where('year(p.payment_date)', $year_1)
						->group_by('p.client_id')
						->get()
						->result_array();
		return $query;
	}

	public function get_client_s_code_payment($id) {
		$query = $this->db->select('c.*')
					->from('client c')
					->where('c.id', $id)
					->get()
					->result_array();

		return $query;
	}

	public function insert_data($data) {
		if(!$this->db->insert('payment', $data)) {
			return false;
		}
		return true;
	} 
	
} 