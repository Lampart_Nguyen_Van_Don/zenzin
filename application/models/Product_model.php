<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Product_model extends ZR_Model {

    /**
     * Define max lenght of product name
     */
    const MAX_LENGHT_NAME = 128;

    /**
     * Define lenght of product code
     */
    const CODE_LENGHT = 3;

    /**
     * Validation
     */
    public function rules($rule_name = 'default') {
        // rule for add new
        $validate_rule['default'] = array(
            array(
                'field' => 'name',
                'label' => 'name',
                'rules' => 'trim|max_length[' . self::MAX_LENGHT_NAME . ']|required',
                'errors' => array(
                    'max_length' => sprintf(lang('error_name_product_limit_character'), self::MAX_LENGHT_NAME),
                    'required' => lang('error_name_product_null'),
                )
            )
        );

        if (isset($validate_rule[$rule_name])) {
            return $validate_rule[$rule_name];
        }

        return array();
    }

    /**
     * get_all_products
     * @param unknown $business_unit_id
     * @author
     */
    public function get_all_products($business_unit_id = '') {
        if ($business_unit_id === '') {
            $query = "SELECT p.id, p.code, p.name, gpe.name AS is_no_add_name
                    FROM product p LEFT JOIN gui_parts_element gpe ON p.is_no_add = gpe.code AND gpe.`disable` = 0 AND gpe.gui_parts_id = 7
                    WHERE p.`disable` = 0
                    ORDER BY p.code
                ";
            return $this->exec_query($query);
        } else {
            $query = "SELECT p.id, p.code, p.name, gpe.name AS is_no_add_name
                    FROM product p LEFT JOIN gui_parts_element gpe ON p.is_no_add = gpe.code AND gpe.`disable` = 0 AND gpe.gui_parts_id = 7
                    WHERE p.`disable` = 0 AND p.business_unit_id = ?
                    ORDER BY p.code
                ";
            return $this->exec_query($query, array($business_unit_id));
        }
    }

    /**
     * get_business_unit_of_current_logged_user
     * @param unknown $id_logged_user
     * @author
     */
    public function get_business_unit_of_current_logged_user($id_logged_user) {
        $query = "SELECT id, name, product_manage_code
                  FROM business_unit
                  WHERE id IN
                      (SELECT business_unit_id
                       FROM department WHERE id IN
                            (SELECT department_id AS id
                             FROM account
                             WHERE id = ?
                            )
                      )";
        $this->set_single();
        return $this->exec_query($query, array($id_logged_user));
    }

    /**
     * get_product_by_id
     * @param unknown $id_product
     * @author
     */
    public function get_product_by_id($id_product) {
        $query = "SELECT id, code, name, is_no_add FROM product WHERE id = ? AND disable = 0";
        $this->set_single();
        return $this->exec_query($query, array($id_product));
    }

    /**
     * count_products_of_business_unit
     * @author
     * @param unknown $id_business_unit
     * @return Ambigous <multitype:, mixed>
     */
    public function count_products_of_business_unit($id_business_unit) {
        $query = "SELECT count(id) AS total
                  FROM product
                  WHERE business_unit_id = ? AND disable = 0";
        $this->set_single();
        return $this->exec_query($query, array($id_business_unit));
    }

    /**
     * check_record_exist
     * @author
     * @param unknown $id
     * @return boolean
     */
    public function check_record_exist($id) {
        $query = "SELECT id FROM product WHERE disable = 0 AND id = ?";
        $this->set_single();
        $data = $this->exec_query($query, array($id));
        return empty($data)? false : true;
    }

    /**
     * Check product_id have exist in order_detail
     * @param unknown $product_id
     * @return boolean
     */
    public function check_product_order($product_id) {
        $sql = "SELECT id FROM order_detail WHERE product_id = ? AND disable = 0";
        $data = $this->exec_query($sql, array($product_id));
        return empty($data) ? false : true;
    }

    /**
     * Get product code last
     *
     * @param int $business_unit_id
     * @return array
     * @author cong_tien
     */
    public function get_product_code_last($business_unit_id) {
        $this->db->select('code, disable')
                ->from('product')
                ->where('business_unit_id', $business_unit_id)
                ->order_by('SUBSTRING(code, -' . self::CODE_LENGHT . ')', 'DESC', false);

        return $this->db->get()->row_array();
    }

}