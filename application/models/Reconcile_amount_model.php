<?php
class reconcile_amount_model extends ZR_Model {
    protected $_list_client_bank_account;
    // list of client have been added, use for add aterisk mark in export csv
    protected $_list_client = array();
    // tax  rate
    protected $_tax_rate = null;
    // list of bugyo manage
    protected $_list_bugyo;
    // max closing accountant date
    protected $_max_closing_date;
    // list of sales slip
    protected $_list_sales_slips;
    // list of client display on reconcole grid
    protected $_list_client_on_grid = array();
    // previous months sales slip
    protected $_prev_month_sales_slip = array();
    // current sales slip
    protected $_cur_month_sales_slip = array();
    // next months sales slip
    protected $_next_month_sales_slip = array();
    // list fee of client
    protected $_client_payment_fee = array();
    // client payment amount base on summary month
    protected $_client_payment_amount = array();
    // list of client payment entry on current month
    protected $_client_payment_entry = array();
    // list of client payment entry on previous month
    protected $_client_prev_payment_entry = array();
    // list all payment past data;
    protected $_list_payment_past_data = array();
    // list of client past payment data
    protected $_client_past_data = array();
    // invoice status
    protected $_invoice_status = SALES_STATUS_INVOICE_ISSUED;
//#9218:Start
    // current summary month filter, full summary month, and small pieces of date
    protected $_summary_month_filter = array();
    // list sales slips that have the delivery date equal to the summary month
    protected $_list_sales_slips_delivery_date_amount = array();
//#9218:End
   
    public function get_client_by_scode($s_code, $exactly) {
        $this->load->model('client_model');

        if ($exactly) {
            $clients = $this->db->select('*')
                    ->from('client c')
                    ->where('c.s_code', $s_code)
                    ->where('c.disable', 0)
                    ->get()->result_array();
        } else {
            $clients = $this->client_model->filter(array('search_s_code' => $s_code))
                                    ->get_data();
        }
        

        $auto_complete_opts = array();
        foreach ($clients as $client) {
            $auto_complete_opts[] = array(
                'id'          => $client['id'],
                'client_name' => $client['name'],
                'label'       => $client['s_code'],
                'value'       => $client['s_code'],
            );
        }
        
        return $auto_complete_opts;
    }

    public function import_excel($datas) {
        $payment_datas = array();
        $bank_datas    = array();

        foreach ($datas as $data) {
            if (isset($data['add_new_bank'])) {
                $bank_datas[] = array(
                    'client_id'         => $data['client_id'],
                    'bank_account'      => $data['account_holder'],
                    'lastup_account_id' => $this->auth->get_account_id(),
                    'create_datetime'   => date('Y-m-d H:i:s'),
//#9547:Start
                    'lastup_datetime'   => date('Y-m-d H:i:s'),
//#9547:End
                );
            }

            $payment_datas[] = array(
                'client_id'         => $data['client_id'],
                'payment_date'      => $data['payment_date'],
                'account_holder'    => $data['account_holder'],
                'amount'            => $data['amount'],
                'lastup_account_id' => $this->auth->get_account_id(),
                'create_datetime'   => date('Y-m-d H:i:s'),
//#9547:Start
                'lastup_datetime'   => date('Y-m-d H:i:s'),
//#9547:End
            );
        }

//#9694:Start
        $bank_datas = array_unique($bank_datas, SORT_REGULAR);
//#9694:End
        
        $this->db->trans_start();
        if (!empty($bank_datas)) {
            $this->db->insert_batch('client_bank_account_info', $bank_datas);
        }

        if (!empty($payment_datas)) {
            $this->db->insert_batch('payment', $payment_datas);
        }

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    protected function _get_all_client_bank_account() {
        if ($this->_list_client_bank_account) return $this->_list_client_bank_account;

        $data = $this->db->select(
//#9694:Start
//                            'cbai.*'
//#9694:End
                            ' cbai.client_id
                            , cbai.bank_account
                            , c.s_code
                            , c.name as client_name')
                        ->from('client_bank_account_info cbai')
                        ->join('client c', 'c.id = cbai.client_id and c.disable = 0')
                        ->where('cbai.disable', 0)
//#7288:Start
//                        ->order_by('c.s_code', 'DESC')
                        ->order_by('c.s_code', 'ASC')
//#7288:End
                        ->get()->result_array();

        $this->_list_client_bank_account = $data;
        return $this->_list_client_bank_account;
    }

    protected function _get_client_bank_account($bank_account_id) {
        $client_banks = $this->_get_all_client_bank_account();
//#9694:Start
        //list clients of a bank
        $bank_clients = array();
//#9694:End
        foreach ($client_banks as $bank) {
//#9694:Start
//            if ($bank['bank_account'] == $bank_account_id) return $bank;
            if ($bank['bank_account'] == $bank_account_id) $bank_clients[] = $bank;
        }

//        return false;
        return array_unique($bank_clients, SORT_REGULAR);
//#9694:End
    }

    public function validate_import_data() {
        $this->load->library('PHPExcel/PHPExcel');

        // if file size > 10M, then $file is empty
        if (!isset($_FILES['file_reconcile_import'])) {
            return array(
                'status' => false,
                'messages' => lang('lbl_over_size'),
            );
        }

        $file      = $_FILES['file_reconcile_import']['tmp_name'];
        $file_name = $_FILES['file_reconcile_import']['name'];

        $arrayZips = array("application/zip", "application/vnd.ms-excel");
        $arrayExtensions = array(".xlsx", ".xls");
        $original_extension = (false === $pos = strrpos($file_name, '.')) ? '' : substr($file_name, $pos);
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $type = $finfo->file($file);
        $valid_file = true;

        if (!in_array($type, $arrayZips) || !in_array($original_extension, $arrayExtensions)) {
            $valid_file = false;
        }

        if (!$valid_file) {
            return array(
                'status'   => false,
                'messages' => lang('lbl_unsupported_file'),
            );
        }

        $objPHPExcel = PHPExcel_IOFactory::load($file);
        $excel_datas = array();


        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            $highestRow         = $worksheet->getHighestRow();
            $highestColumn      = "G"; //$worksheet->getHighestColumn();
            $highestColumnIndex = 7;  //PHPExcel_Cell::columnIndexFromString($highestColumn);

            for ($row = 2; $row <= $highestRow; ++ $row) {
                // PROGRESS
                // $progress_percent = ceil(($row - 1) * $percent);
                // $fp = fopen($file_name, 'w');
                // fwrite($fp, json_encode(array('status' => $progress_percent)));
                // fclose($fp);

                $row_data = array();
                for ($col = 1; $col < $highestColumnIndex; ++ $col) {
                    $cell = $worksheet->getCellByColumnAndRow($col, $row);
                    $val = trim($cell->getValue());
                    // Column: Datetime
                    if($col == 1) {
                        if (is_numeric($val)) {
                            $row_data[] = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($val));
                        } else {
                            $row_data[] = $val;
                        }
                    }
                    // Column: Transfer source
                    if($col == 3) {
                        $row_data[] = $val;
                    }
                    // Column: Amount money
                    if($col == 6) {
                        $row_data[] = $val;
                    }
                }
                $excel_datas[] = $row_data;
            }
            // only load the first sheet
            break;
        }
        
        foreach ($excel_datas as $row => $datas) {
            if (empty($datas[0]) && empty($datas[1]) && empty($datas[2])) unset($excel_datas[$row]);
        }
        
        if (empty($excel_datas)) {
            return array(
                'status'   => false,
                'messages' => lang('lbl_empty_file'),
            );
        }

        $unique_id   = $this->input->post('unique_id');
        $file_name   = FCPATH . 'public/admin/reconcile_amount/' . $unique_id.'.json';
        $total_data       = count($excel_datas);
        $percent          = 100 / $total_data;
        $final_datas      = array();
//#9547:Start
        $clients_total_amount = array();
//#9547:End
        foreach ($excel_datas as $row => $row_datas) {
            if(empty($row_datas[0]) && empty($row_datas[1]) && empty($row_datas[2])) continue;

            $import_data = array();
            foreach ($row_datas as $col => $data) {
                // $tmp_percent = ceil(($row + 1) * $percent + $progress_percent) < 100 ? ceil(($row + 1) * $percent + $progress_percent) : 100;
                $fp = fopen($file_name, 'w');
                fwrite($fp, json_encode(array('status' => f_ceil(($row + 1) * $percent))));
                fclose($fp);


                switch ($col) {
                    case 0:

                        $delimiter = strpos($col, '/') ? '/' : '-';
                        $parts = explode($delimiter, $data);
                        if (empty($data) || count($parts) != 3 || !checkdate($parts[1], $parts[2], $parts[0])) {
                            $import_data['errors']['payment_date'] = lang('error_invalid_date');
                        }

                        $import_data['payment_date'] = $data;
                        break;
                    case 1:
//#9694:Start
//                        $client_bank = $this->_get_client_bank_account($data);
                        $client_bank = false;

//                        if (empty($data) || !$client_bank) {
                        if (empty($data)) {
//                            $import_data['errors']['account_holder'] = lang('error_invalid_bank');
                            $import_data['errors']['account_holder_is_required'] = str_replace('%field%', lang('lbl_source_acc'), lang('error_is_required'));
                        } else {
                            $client_bank = $this->_get_client_bank_account($data);

                            if (!$client_bank) $import_data['errors']['account_holder'] = lang('error_invalid_bank');
                        }

                        // if (count($client_bank) == 1) {
                        //     $import_data['client_id']   = $client_bank['client_id'];
                        //     $import_data['s_code']      = $client_bank['s_code'];
                        //     $import_data['client_name'] = $client_bank['client_name'];
                        // }

                        // if (count($client_bank) > 1) {
                        //     $import_data['']
                        // }

//                        $import_data['client_id']      = $client_bank ? $client_bank['client_id'] : false;
//                        $import_data['s_code']         = $client_bank ? $client_bank['s_code'] : false;
//                        $import_data['client_name']    = $client_bank ? $client_bank['client_name'] : false;
                        if (count($client_bank) > 1) {
                            $import_data['errors']['account_holder'] = lang('error_please_select_s_code');
                        }

                        $import_data['clients']   = $client_bank ? $client_bank : false;
//#9694:End
                        $import_data['account_holder'] = $data;
                        break;
                    case 2:
                        if (!is_numeric($data)) {
                            $import_data['errors']['amount'] = lang('error_amount_must_be_number');
//#7315:Start
//                        } elseif ($data > 2147483647) {
                        } elseif ($data > MAX_SIGNED_INT_10) {
//#7315:End
                            $import_data['errors']['amount'] = lang('error_amount_out_of_range');
                        } elseif ($data < 0) {
                            $import_data['errors']['amount'] = lang('error_amount_can_not_be_negative');
//#9547:Start
                        } elseif ($data != (int)$data) {
                            $import_data['errors']['amount'] = str_replace('%field%', lang('lbl_amount'), lang('error_is_integer'));
//#9694:Start
//                        } elseif ($import_data['client_id']) {
                        } elseif ($import_data['clients'] && count($import_data['clients']) == 1) {
                            // The value of next line is the accumulation of the current line and the previous lines
//                            if (!isset($clients_total_amount[$import_data['s_code']])) {
//                                $clients_total_amount[$import_data['s_code']] = $data;
//                            } else {
//                                $clients_total_amount[$import_data['s_code']] += $data;
//                            }

//                            $validate_result = $this->validate_adjust_money($import_data['client_id'], $clients_total_amount[$import_data['s_code']]);
//                            if ($validate_result) {
//                                $errors = isset($import_data['errors']) ? $import_data['errors'] : array();
//                                $errors = array_merge($errors, $validate_result);
//                                $import_data['errors'] = $errors;
//                            }
                            $client = $import_data['clients'][0];
                            if (!isset($clients_total_amount[$client['s_code']])) {
                                $clients_total_amount[$client['s_code']] = $data;
                            } else {
                                $clients_total_amount[$client['s_code']] += $data;
                            }

                            $validate_result = $this->validate_adjust_money($client['client_id'], $clients_total_amount[$client['s_code']]);
                            if ($validate_result) {
                                $errors = isset($import_data['errors']) ? $import_data['errors'] : array();
                                $errors = array_merge($errors, $validate_result);
                                $import_data['errors'] = $errors;
                            }
                        }
//#9694:End
//#9547:End
                        $import_data['amount'] = $data;
                        break;
                    default:
                        break;
                }
            }
            $final_datas[] = $import_data;
        }
//#9547:Start
//        return $final_datas;
        return array(
            'excel_datas' => $final_datas,
            'clients_total_amount' => $clients_total_amount,
        );
//#9547:End
    }

    /**
     * list of date from max closing date in closing accountant and to min date in payment + sales_slip
     * @return array
     */
    public function get_pulldown_datetime() {
        $from_ss = $this->db->select('min(ss.payment_date) as payment_date')
                        ->from('sales_slip ss')
                        ->where('ss.disable', 0)
                        ->where('ss.payment_date !=', '0000-00-00')
                        ->get()->row_array();

        $from_p  = $this->db->select('min(p.payment_date) as payment_date')
                        ->from('payment p')
                        ->where('p.disable', 0)
                        ->get()->row_array();
        $from_ss = $from_ss['payment_date'] ? $from_ss['payment_date'] : date('Y-m-d');
        $from_p  = $from_p['payment_date'] ? $from_p['payment_date'] : date('Y-m-d');

        $from = strtotime($from_ss) < strtotime($from_p) ? $from_ss : $from_p;

        $to = $this->_get_max_closing_accountant_date();
        $start    = new DateTime($from);
        $start->modify('first day of this month');
        $end      = new DateTime($to);
        $end->modify('first day of next month');
        $interval = DateInterval::createFromDateString('+1 month');
        $period   = new DatePeriod($start, $interval, $end);

        $pull_down_list = array();
        foreach ($period as $dt) {
            $pull_down_list[] = $dt->format("Y/m");
        }

        $pull_down_list = array_reverse($pull_down_list);

        return array(
            'default' => date('Y/m', strtotime($to)),
            'list' => $pull_down_list
        );
    }

    public function _get_max_closing_accountant_date() {
        if ($this->_max_closing_date) return $this->_max_closing_date;

        $data = $this->db->select('max(close_date) as close_date')
                        ->from('closing_accountant ca')
                        ->where('ca.disable', 0)
                        ->get()->row_array();

        $this->_max_closing_date = $data['close_date'];
        return $this->_max_closing_date;
    }

    protected function _list_all_sales_slip($s_code = '') {
        if ($this->_list_sales_slips) return $this->_list_sales_slips;

        $this->db->select('
                      DISTINCT(ssd.sales_slip_id)
                    , ss.id
                    , ss.client_id
                    , ss.sales_slip_number
                    , ss.total_amount_tax
                    , ss.payment_date
                    , ss.is_checked as default_is_checked
                    , ss.check_date as default_check_date
                    , ssd.is_ad_receive_payment
                    , ssd.delivery_date
                    , c.name as client_name
                    , c.s_code')
                ->from('sales_slip_detail ssd')
                ->join('sales_slip ss', 'ss.id = ssd.sales_slip_id AND ss.sales_slip_status = '.$this->_invoice_status.' AND ss.disable = 0')
                ->join('client c', 'c.id = ss.client_id AND c.disable = 0')
                ->where('ssd.sales_slip_id !=', 0)
                ->where('ssd.is_ad_receive_payment', 1)
                ->where('ssd.disable', 0)
                ->group_by('ssd.sales_slip_id')
                ->order_by('ss.sales_slip_number', 'ASC');

        if (!empty($s_code)) {
            $this->db->like('c.s_code', $s_code, 'after');
        }

        $parents = $this->db->get()->result_array();

        $list_ids = array();
        foreach ($parents as $k_parent => $parent) {
            if ($parent['is_ad_receive_payment'] == 1) {
                $parents[$k_parent]['have_child']  = true;
                $parents[$k_parent]['children_ss'] = array();
                $list_ids[]                        = $parent['sales_slip_id'];

                $children = $this->db->select('ssd.ad_receive_sales_slip_detail_id')
                                    ->from('sales_slip_detail ssd')
                                    ->where('ssd.sales_slip_id', $parent['sales_slip_id'])
                                    ->where('ssd.disable', 0)
                                    ->get()->result_array();
                
                $total_empty = 0;

                foreach ($children as $k_child => $child) {
                    $children[$k_child]['have_child'] = false;
                    $ss_children = $this->db->select('
                                                , ssd.sales_slip_id
                                                , ss.id
                                                , ss.sales_slip_status
                                                , ss.client_id
                                                , ss.sales_slip_number
                                                , ss.total_amount_tax
                                                , ss.payment_date
                                                , ss.is_checked as default_is_checked
                                                , ss.check_date as default_check_date
                                                , ssd.is_ad_receive_payment
                                                , ssd.delivery_date
                                                , c.name as client_name
                                                , c.s_code')
                                            ->from('sales_slip_detail ssd')
                                            ->join('sales_slip ss', 'ss.id = ssd.sales_slip_id AND ss.disable = 0', 'left')
                                            ->join('client c', 'c.id = ss.client_id AND c.disable = 0', 'left')
                                            ->where('ssd.is_ad_receive_payment', 0)
                                            ->where('ssd.ad_receive_sales_slip_detail_id', $child['ad_receive_sales_slip_detail_id'])
                                            ->where('ssd.disable', 0)
                                            ->group_by('ssd.sales_slip_id')
                                            ->order_by('ss.sales_slip_number', 'ASC')
                                            ->get()->result_array();

                    if (!empty($ss_children)) {
                        foreach ($ss_children as $ss) {
//#9218:Start
//                            if ($ss['sales_slip_id'] == 0 || $ss['sales_slip_status'] != $this->_invoice_status) {
                            if ($children[$k_child]['have_child'] == false && ($ss['sales_slip_id'] == 0 || $ss['sales_slip_status'] != $this->_invoice_status)) {
                                $parents[$k_parent]['have_child'] = false;
                            } else {
//                                if (!in_array($ss['sales_slip_id'], $list_ids)) {
                                if (($ss['sales_slip_id'] != 0 && $ss['sales_slip_status'] == $this->_invoice_status) && !in_array($ss['sales_slip_id'], $list_ids)) {
                                    $ss['have_child'] = true;
                                    $parents[$k_parent]['children_ss'][] = $ss;
                                    $parents[$k_parent]['have_child'] = true;
                                }
                                $children[$k_child]['have_child'] = true;
                                $parents[$k_parent]['have_child'] = true;
                            }
//#9218:End
                            $list_ids[] = $ss['sales_slip_id'];
                        }

                        if ($parents[$k_parent]['have_child'] == false) {
                            $parents[$k_parent]['children_ss'] = array();
                        }
                    } else {
                        $total_empty++;
                    }
                }
                
                if ($total_empty == count($children)) {
                    $parents[$k_parent]['have_child'] = false;
                } else {
                    foreach ($children as $k_child => $child) {
                        if ($child['have_child'] == false) {
                            $parents[$k_parent]['have_child'] = false;
                            break;
                        }
                    }
                }
            }
        }

        $list_ids    = array_unique($list_ids);
        $sales_slips = array();
        foreach ($parents as $k_parent => $parent) {
            $tmp_parent = $parent;
            unset($parent['children_ss']);
            unset($parent['children_ids']);

            $sales_slips[] = $parent;

            if (isset($parent['have_child']) && $parent['have_child'] == 1) {
                foreach ($tmp_parent['children_ss'] as $child) {
                    $sales_slips[] = $child;
                }
            }
        }

        $this->db->select('
                          DISTINCT(ssd.sales_slip_id)
                        , ss.id
                        , ss.client_id
                        , ss.sales_slip_number
                        , ss.total_amount_tax
                        , ss.payment_date
                        , ss.is_checked as default_is_checked
                        , ss.check_date as default_check_date
                        , ssd.is_ad_receive_payment
                        , ssd.delivery_date
                        , c.name as client_name
                        , c.s_code
                        , 0 as have_child')
                    ->from('sales_slip_detail ssd')
                    ->join('sales_slip ss', 'ss.id = ssd.sales_slip_id AND ss.sales_slip_status = '.$this->_invoice_status.' AND ss.disable = 0')
                    ->join('client c', 'c.id = ss.client_id AND c.disable = 0')
                    ->where('ssd.sales_slip_id !=', 0)
                    ->where('ssd.is_ad_receive_payment', 0)
                    ->where('ssd.ad_receive_sales_slip_detail_id', 0)
                    ->where('ssd.disable', 0)
                    ->group_by('ssd.sales_slip_id')
                    ->order_by('ss.sales_slip_number', 'ASC');

        if (!empty($list_ids)) {
            $this->db->where_not_in('ssd.sales_slip_id', $list_ids);
        }

        if (!empty($s_code)) {
            $this->db->like('c.s_code', $s_code, 'after');
        }

        $parents = $this->db->get()->result_array();

        $sales_slips = array_merge($sales_slips, $parents);
        usort($sales_slips, array($this, '_sort_by_sales_slip_number'));

        $list_clients = array();
        foreach ($sales_slips as $sales_slip) {
            $list_clients[$sales_slip['client_id']] = array(
                'client_id'   => $sales_slip['client_id'],
                'client_name' => $sales_slip['client_name'],
                's_code'      => $sales_slip['s_code'],
            );
        }

        
    
        $this->_list_sales_slips = $sales_slips;

        $all_client_payment = $this->_get_all_client_payment($this->_get_max_closing_accountant_date(), $s_code);

        foreach ($all_client_payment as $payment) {
            /*tmp#6865: Start 2015/09/26 if (!isset($list_clients[$payment['client_id']])) {*/
            //tmp#6865: Start 2015/09/26
            if (!isset($list_clients[$payment['client_id']]) && $payment['s_code'] != '') {
            //tmp#6865: End
                $list_clients[$payment['client_id']] = array(
                    'client_id'   => $payment['client_id'],
                    's_code'      => $payment['s_code'],
                    'client_name' => $payment['client_name'],
                );
            }
        }

        $all_payment_past_datas = $this->_get_all_payment_past_data($s_code);
        foreach ($all_payment_past_datas as $payment) {
            if (!isset($list_clients[$payment['client_id']])) {
                $list_clients[$payment['client_id']] = array(
                    'client_id'   => $payment['client_id'],
                    's_code'      => $payment['s_code'],
                    'client_name' => $payment['client_name'],
                );
            }
        }
//#7291:Start
        usort($list_clients, array($this, '_sort_by_s_code_asc'));
//#7291:End
        $this->_list_client_on_grid = $list_clients;
        return $this->_list_sales_slips;
    }

//#7291:Start
    protected function _sort_by_s_code_asc($a, $b) {
        if ($a['s_code'] == $b['s_code']) {
            return 0;
        }
        return ($a['s_code'] < $b['s_code']) ? -1 : 1;
    }
//#7291:End

    protected function _get_list_client_on_grid($s_code) {
        if ($this->_list_client_on_grid) return $this->_list_client_on_grid;

        $this->_list_all_sales_slip($s_code);
        return $this->_list_client_on_grid;
    }



    protected function _get_prev_month_sales_slip($client_id, $summary_month) {
        if (isset($this->_prev_month_sales_slip[$client_id])) return $this->_prev_month_sales_slip[$client_id];
//#9723:Start
//#9218:Start        
//        $delimiter = strpos($summary_month, '/') !== false ? '/' : '-';
//        $parts     = explode($delimiter, $summary_month);
//        $year      = $parts[0];
//        $month     = $parts[1];
//#9218:End
//#9723:Start
        $list_sales_slips = $this->_list_all_sales_slip();
        $max_closing_date = $this->_get_max_closing_accountant_date();
        $prev_month_ss    = array();

        foreach ($list_sales_slips as $sales_slip) {
//#9723:Start
//            if ($sales_slip['client_id'] == $client_id && strtotime($sales_slip['payment_date']) <= strtotime($summary_month)) {
            if ($sales_slip['client_id'] == $client_id && $this->_compare_summary_month($sales_slip['payment_date'], $summary_month, '<=')) {
//#9218:Start
//                $ss_date_parts = explode('-', $sales_slip['default_check_date']);
//                $ss_year       = $ss_date_parts[0];
//                $ss_month      = $ss_date_parts[1];

//                if (!$sales_slip['default_is_checked'] || ($sales_slip['default_is_checked'] && $sales_slip['default_check_date'] == $max_closing_date)) {
//                if (!$sales_slip['default_is_checked'] || ($sales_slip['default_is_checked'] && $ss_year >= $this->_summary_month_filter['year'] && $ss_month >= $this->_summary_month_filter['month'])) {
                if (!$sales_slip['default_is_checked'] || ($sales_slip['default_is_checked'] && $this->_compare_summary_month($sales_slip['default_check_date'], $this->_summary_month_filter['date'], '>='))) {
//#9723:End
                    $prev_month_ss[] = $sales_slip;    
                }
//#9723:Start
//                if (date('Y-m', strtotime($sales_slip['delivery_date'])) == date('Y-m', strtotime($this->_summary_month_filter['date']))) {
                if ($this->_compare_summary_month($sales_slip['delivery_date'], $this->_summary_month_filter['date'], '==')) {
//#9723:End
                    if (!isset($this->_list_sales_slips_delivery_date_amount[$client_id]['prev_month'])) {
                        $this->_list_sales_slips_delivery_date_amount[$client_id]['prev_month'] = 0;
                    }
                    $this->_list_sales_slips_delivery_date_amount[$client_id]['prev_month'] += $sales_slip['total_amount_tax'];
                }

            }
        }

        $past_debt = $this->_get_client_payment_past_data($client_id);
        if ($past_debt && $past_debt['is_ad_receive_payment'] == 0) {
//#9723:Start
//#9218:Start
//            $ss_date_parts = explode('-', $past_debt['default_check_date']);
//            $ss_year       = $ss_date_parts[0];
//            $ss_month      = $ss_date_parts[1];

//            if (!$past_debt['default_is_checked'] || ($past_debt['default_is_checked'] && $past_debt['default_check_date'] == $max_closing_date)) {
//            if (!$past_debt['default_is_checked'] || ($past_debt['default_is_checked'] && $ss_year >= $this->_summary_month_filter['year'] && $ss_month >= $this->_summary_month_filter['month'])) {
            if (!$past_debt['default_is_checked'] || ($past_debt['default_is_checked'] && $this->_compare_summary_month($past_debt['default_check_date'], $this->_summary_month_filter['date'], '>='))) {
//#9723:End
//#9218:End
                $prev_month_ss[] = array(
                    'sales_slip_id'         => $past_debt['id'],
                    'id'                    => $past_debt['id'],
                    'sales_slip_number'     => lang('lbl_debt'),
                    'total_amount_tax'      => $past_debt['total_amount'],
                    'payment_date'          => $summary_month,
                    'default_is_checked'    => $past_debt['default_is_checked'],
                    'default_check_date'    => $past_debt['default_check_date'],
                    'is_ad_receive_payment' => 0,
                    'have_child'            => 0,
                    'is_past_data'          => 1,
                );
            }
        }
        
        $this->_prev_month_sales_slip[$client_id] = $prev_month_ss;
        return $this->_prev_month_sales_slip[$client_id];
    }

    protected function _get_cur_month_sales_slip($client_id, $summary_month) {
        if (isset($this->_cur_month_sales_slip[$client_id])) return $this->_cur_month_sales_slip[$client_id];
//#9723:Start
//        $delimiter = strpos($summary_month, '/') !== false ? '/' : '-';
//        $parts     = explode($delimiter, $summary_month);
//        $year      = $parts[0];
//        $month     = $parts[1];
//#9723:End

        $list_sales_slips = $this->_list_all_sales_slip();
        $max_closing_date = $this->_get_max_closing_accountant_date();
        $cur_month_ss     = array();
        
        foreach ($list_sales_slips as $sales_slip) {
//#9723:Start
//            $parts = explode('-', $sales_slip['payment_date']);
//            if ($sales_slip['client_id'] == $client_id && $year == $parts[0] && $month == $parts[1]) {
            if ($sales_slip['client_id'] == $client_id && $this->_compare_summary_month($sales_slip['payment_date'], $summary_month, '==')) {
//#9218:Start
//                $ss_date_parts = explode('-', $sales_slip['default_check_date']);
//                $ss_year       = $ss_date_parts[0];
//                $ss_month      = $ss_date_parts[1];

//                if (!$sales_slip['default_is_checked'] || ($sales_slip['default_is_checked'] && $sales_slip['default_check_date'] == $max_closing_date)) {
//                if (!$sales_slip['default_is_checked'] || ($sales_slip['default_is_checked'] && $ss_year >= $this->_summary_month_filter['year'] && $ss_month >= $this->_summary_month_filter['month'])) {
                if (!$sales_slip['default_is_checked'] || ($sales_slip['default_is_checked'] && $this->_compare_summary_month($sales_slip['default_check_date'], $this->_summary_month_filter['date'], '>='))) {
//#9723:End
                    $cur_month_ss[] = $sales_slip;
                }
//#9218:Start
//                if (date('Y-m', strtotime($sales_slip['delivery_date'])) == date('Y-m', strtotime($this->_summary_month_filter['date']))) {
                if ($this->_compare_summary_month($sales_slip['delivery_date'], $this->_summary_month_filter['date'], '==')) {
//#9218:End
                    if (!isset($this->_list_sales_slips_delivery_date_amount[$client_id]['cur_month'])) {
                        $this->_list_sales_slips_delivery_date_amount[$client_id]['cur_month'] = 0;
                    }
                    $this->_list_sales_slips_delivery_date_amount[$client_id]['cur_month'] += $sales_slip['total_amount_tax'];
                }
//#9218:End
            }
        }

        $this->_cur_month_sales_slip[$client_id] = $cur_month_ss;
        return $this->_cur_month_sales_slip[$client_id];
    }

    protected function _get_next_month_sales_slip($client_id, $summary_month) {
        if (isset($this->_next_month_sales_slip[$client_id])) return $this->_next_month_sales_slip[$client_id];

        $list_sales_slips = $this->_list_all_sales_slip();
        $max_closing_date = $this->_get_max_closing_accountant_date();
        $next_month_ss    = array();
        
        foreach ($list_sales_slips as $sales_slip) {
//#9723:Start
//            if ($sales_slip['client_id'] == $client_id && strtotime($sales_slip['payment_date']) >= strtotime($summary_month)) {
            if ($sales_slip['client_id'] == $client_id && $this->_compare_summary_month($sales_slip['payment_date'], $summary_month, '>=')) {
//#9218:Start
//                $ss_date_parts = explode('-', $sales_slip['default_check_date']);
//                $ss_year       = $ss_date_parts[0];
//                $ss_month      = $ss_date_parts[1];

               // if (!$sales_slip['default_is_checked'] || ($sales_slip['default_is_checked'] && $sales_slip['default_check_date'] == $max_closing_date)) {
//                if (!$sales_slip['default_is_checked'] || ($sales_slip['default_is_checked'] && $ss_year >= $this->_summary_month_filter['year'] && $ss_month >= $this->_summary_month_filter['month'])) {
                if (!$sales_slip['default_is_checked'] || ($sales_slip['default_is_checked'] && $this->_compare_summary_month($sales_slip['default_check_date'], $this->_summary_month_filter['date'], '>='))) {
//#9723:End
                    $next_month_ss[] = $sales_slip;
                }
//#9218:Start
//                if (date('Y-m', strtotime($sales_slip['delivery_date'])) == date('Y-m', strtotime($this->_summary_month_filter['date']))) {
                if ($this->_compare_summary_month($sales_slip['delivery_date'], $this->_summary_month_filter['date'], '==')) {
//#9723:End
                    if (!isset($this->_list_sales_slips_delivery_date_amount[$client_id]['next_month'])) {
                        $this->_list_sales_slips_delivery_date_amount[$client_id]['next_month'] = 0;
                    }
                    $this->_list_sales_slips_delivery_date_amount[$client_id]['next_month'] += $sales_slip['total_amount_tax'];
                }
//#9218:End
            }
        }

        $this->_next_month_sales_slip[$client_id] = $next_month_ss;
        return $this->_next_month_sales_slip[$client_id];
    }

    protected function _get_client_payment_amount($client_id, $summary_month, $cache = true) {
        if (isset($this->_client_payment_amount[$client_id][$summary_month]) && $cache) return $this->_client_payment_amount[$client_id][$summary_month];

        $delimiter = strpos($summary_month, '/') !== false ? '/' : '-';
        $parts     = explode($delimiter, $summary_month);
        $year      = $parts[0];
        $month     = $parts[1];

        $payment = $this->db->select('sum(amount) as total_amount')
                            ->from('payment p')
                            ->where('p.client_id', $client_id)
                            ->where('YEAR(p.payment_date)', $year)
                            ->where('MONTH(p.payment_date)', $month)
                            ->where('p.disable', 0)
                            ->get()
                            ->row_array();

        $this->_client_payment_amount[$client_id][$summary_month] = isset($payment['total_amount']) ? $payment['total_amount'] : 0;
        return $this->_client_payment_amount[$client_id][$summary_month];
    }

    protected function _get_client_payment_entry($client_id, $summary_month, $is_prev = false) {
        if (isset($this->_client_payment_entry[$client_id][$summary_month])) return $this->_client_payment_entry[$client_id][$summary_month];

        $delimiter = strpos($summary_month, '/') !== false ? '/' : '-';
        $parts     = explode($delimiter, $summary_month);
        $year      = $parts[0];
        $month     = $parts[1];

        $payment_entry = $this->db->select('*')
                                ->from('payment_entry pe')
                                ->where('pe.client_id', $client_id)
                                ->where('YEAR(entry_date)', $year)
                                ->where('MONTH(entry_date)', $month)
                                ->where('pe.disable', 0)
                                ->get()
                                ->row_array();
        
        // #6752 if this month does not have data in payment entry
        // then clone the previous payment_entry data and insert to db
        // only use when get prevous payment_entry
        if ($is_prev && !$payment_entry) {
            $min_entry_date    = $this->_get_min_closing_accountance();
            $min_entry_date    = date('Y-m-d', strtotime($min_entry_date['entry_date']));
            $tmp_summary_month = $summary_month;
            
            $payment_entry = array();
            do {
                $tmp_summary_month = $this->_decrease_month($tmp_summary_month);
                $tmp_summary_month = date('Y-m-d', strtotime("$tmp_summary_month first day of this month"));

                $delimiter     = strpos($tmp_summary_month, '/') !== false ? '/' : '-';
                $parts         = explode($delimiter, $tmp_summary_month);
                $year          = $parts[0];
                $month         = $parts[1];
                $payment_entry = $this->db->select('*')
                                ->from('payment_entry pe')
                                ->where('pe.client_id', $client_id)
                                ->where('YEAR(entry_date)', $year)
                                ->where('MONTH(entry_date)', $month)
                                ->where('pe.disable', 0)
                                ->get()
                                ->row_array();

            } while (!$payment_entry && strtotime($tmp_summary_month) >= strtotime("$min_entry_date first day of this month"));
            
            if ($payment_entry) {

                $all_summary_month = $this->_get_all_closing_accountances();
                $tmp_summary_month = date('Y-m-d' , strtotime($payment_entry['entry_date']));

                do {
                    $tmp_summary_month = $this->_increase_month($tmp_summary_month);
                    $tmp_summary_month = date('Y-m-d', strtotime("$tmp_summary_month first day of this month"));

                    if (strtotime($summary_month) - strtotime($tmp_summary_month)) {
                        $insert_data = array(
                            'client_id'                    => $payment_entry['client_id'],
                            'entry_date'                   => $tmp_summary_month,
                            'etc'                          => 0,
                            'fee'                          => 0,
                            'repayment'                    => 0,
                            'miscellaneous_loss'           => 0,
                            'miscellaneous_income'         => 0,
                            'offset_next_month_receive'    => $payment_entry['offset_next_month_receive'],
                            'offset_next_month_ad_receive' => $payment_entry['offset_next_month_ad_receive'],
                            'bugyo_next_month_receive'     => $payment_entry['bugyo_next_month_receive'],
                            'bugyo_next_month_ad_receive'  => $payment_entry['bugyo_next_month_ad_receive'],
                            'lastup_account_id'            => $this->auth->get_account_id(),
//#8353:Start
//                            'create_datetime'              => date('Y-m-d H:i:s'),
//                            'lastup_datetime'              => '0000-00-00',
                            'create_datetime'              => $this->_db_now(),
                            'lastup_datetime'              => $this->_db_now(),
//#8353:End
                        );

                        foreach ($all_summary_month as $v) {
                            $close_date = $v['close_date'];
                            if (strtotime("$close_date first day of this month") == strtotime($tmp_summary_month)) {
                                $insert_data['entry_date'] = $v['close_date'];
                                break;
                            }
                        }

                        $this->db->insert('payment_entry', $insert_data);
                    }

                } while (strtotime($tmp_summary_month) < strtotime("$summary_month first day of this month"));

                return $payment_entry;
            }

            return array(
                'id' => null,
                'etc' => 0,
                'fee' => 0,
                'repayment' => 0,
                'miscellaneous_loss' => 0,
                'miscellaneous_income' => 0,
                'offset_next_month_receive' => 0,
                'offset_next_month_ad_receive' => 0,
                'bugyo_next_month_receive' => 0,
                'bugyo_next_month_ad_receive' => 0,
            );
        }

        if (!$payment_entry) {
            $payment_entry = array(
                'id' => null,
                'etc' => 0,
                'fee' => 0,
                'repayment' => 0,
                'miscellaneous_loss' => 0,
                'miscellaneous_income' => 0,
                'offset_next_month_receive' => 0,
                'offset_next_month_ad_receive' => 0,
                'bugyo_next_month_receive' => 0,
                'bugyo_next_month_ad_receive' => 0,
            );
        }

        $this->_client_payment_entry[$client_id][$summary_month] = $payment_entry;
        return $this->_client_payment_entry[$client_id][$summary_month];
    }


    protected function _get_client_payment_past_data($client_id) {
        if (isset($this->_client_past_data[$client_id])) return $this->_client_past_data[$client_id];

        $all_payment_past_datas = $this->_get_all_payment_past_data();
        foreach ($all_payment_past_datas as $payment) {
            if ($payment['client_id'] == $client_id) {
                $this->_client_past_data[$client_id] = $payment;
                return $this->_client_past_data[$client_id];
            }
        }
        $this->_client_past_data[$client_id] = array();
        return $this->_client_past_data[$client_id];

    }

    protected function _get_all_payment_past_data($s_code = '') {
        if ($this->_list_payment_past_data) return $this->_list_payment_past_data;

        $this->db->select('
                      ppd.id
                    , ppd.client_id
                    , ppd.is_ad_receive_payment
                    , ppd.is_checked as default_is_checked
                    , ppd.check_date as default_check_date
                    , sum(ppd.amount) as total_amount
                    , c.name as client_name
                    , c.s_code')
                ->from('payment_past_data ppd')
                ->join('client c', 'c.id = ppd.client_id AND c.disable = 0')
                ->where('ppd.disable', 0)
                ->group_by('ppd.client_id');

        if (!empty($s_code)) {
            $this->db->like('c.s_code', $s_code, 'after');
        }

        $this->_list_payment_past_data = $this->db->get()->result_array();
        return $this->_list_payment_past_data;
    }

    protected function _get_prev_ad_receive_payment_amount($client_id, $summary_month) {
        $amount = 0;
        $payment_entry = $this->_get_client_payment_entry($client_id, $this->_decrease_month($summary_month), true);

        if ($payment_entry) {
            $amount = $payment_entry['offset_next_month_ad_receive'];
        }

        return $amount;
    }    

    public function get_clearance_data($summary_month, $s_code, $remaining_srub, $action = false) {
        $summary_month .= '/01';
//#9533:Start
        $this->_set_summary_month_filter($summary_month);
//#9533:End
        // echo "<pre>";print_r($this->_list_all_sales_slip());die;
        // $this->db->select('ss.client_id, c.name as client_name, c.s_code')
        //         ->from('sales_slip ss')
        //         ->join('client c', 'c.id = ss.client_id AND c.disable = 0')
        //         ->where('ss.disable', 0)
        //         ->where('ss.sales_slip_status', $this->_invoice_status)
        //         ->group_by('client_id')
        //         ->order_by('c.s_code', 'ASC');
        // $s_code = 'AN0007';

        // if (!empty($s_code)) $this->db->like('c.s_code', $s_code, 'after');

        // $clients = $this->db->get()->result_array();

        $clients = $this->_get_list_client_on_grid($s_code);

        if (!empty($clients)) {
            foreach ($clients as $key => $client) {
                $clients[$key]['prev_month']                     = $this->_get_prev_month_sales_slip($client['client_id'], $this->_decrease_month($summary_month));
                $clients[$key]['current_month']                  = $this->_get_cur_month_sales_slip($client['client_id'], $summary_month);
                $clients[$key]['next_month']                     = $this->_get_next_month_sales_slip($client['client_id'], $this->_increase_month($summary_month));
                $clients[$key]['prev_ad_receive_payment_amount'] = $this->_get_prev_ad_receive_payment_amount($client['client_id'], $summary_month);
                $clients[$key]['payment_amount']                 = $this->_get_client_payment_amount($client['client_id'], $summary_month);
                $clients[$key]['payment_entry']                  = $this->_get_client_payment_entry($client['client_id'], $summary_month);
                $clients[$key]['prev_payment_entry']             = $this->_get_client_payment_entry($client['client_id'], $this->_decrease_month($summary_month), true);
//#9218:Start
                $clients[$key]['total_delivery_date_amount']     = $this->_get_client_sales_slips_delivery_date_amount($client['client_id']);
//#9218:End

                if (!$clients[$key]['prev_month'] && !$clients[$key]['current_month'] && !$clients[$key]['next_month']) {
                    $clients[$key]['is_no_sales_slip'] = true;
                } else {
                    $clients[$key]['is_no_sales_slip'] = false;
                }
#7065:Start     
                // if ($clients[$key]['is_no_sales_slip'] && $clients[$key]['payment_amount'] == 0) {
                if ($clients[$key]['is_no_sales_slip'] && $clients[$key]['payment_amount'] == 0 && $clients[$key]['prev_ad_receive_payment_amount'] == 0) {
#7065:End 
                    unset($clients[$key]);
                }
            }
        }
        
        if ($remaining_srub) {
            foreach ($clients as $key => $client) {
                $is_remove = true;
                foreach ($client['prev_month'] as $sales_slip) {
                    if ($sales_slip['default_is_checked'] == false) {
                        $is_remove = false;
                    }
                }
                foreach ($client['current_month'] as $sales_slip) {
                    if ($sales_slip['default_is_checked'] == false) {
                        $is_remove = false;
                    }
                }

                if ($is_remove) unset($clients[$key]);
            }
        }
        
        // 当月入金＝当月支払 消し込み
        if ($action == 1) {
            foreach ($clients as $key => $client) {
                if ($client['payment_amount'] == 0) {
                    unset($clients[$key]);
                    continue;
                }
                
                $total = 0;
                foreach ($client['current_month'] as $sales_slip) {
//#8353:Start
//#7371:Start
                    $total += $sales_slip['total_amount_tax'];
//                    if ($sales_slip['is_ad_receive_payment'] == 0) {
//                        $total += $sales_slip['total_amount_tax'];
//                    }
//#7371:End
//#8353:End
                }
                if ($total != $client['payment_amount']) unset($clients[$key]);
            }
        }
        // 前受金＋当月入金＝当月支払＋前月以前売掛金 消し込み
        if ($action == 2) {
            foreach ($clients as $key => $client) {
                /*tmp#6683 Start 29/09/2015
                if ($client['payment_amount'] == 0) {
                    unset($clients[$key]);
                    continue;
                }
                tmp#6683 End*/

                $total = 0;
                foreach ($client['prev_month'] as $sales_slip) {
//#8353:Start
//#7371:Start
                    $total += $sales_slip['total_amount_tax'];
//                    if ($sales_slip['is_ad_receive_payment'] == 0) {
//                        $total += $sales_slip['total_amount_tax'];
//                    }
//#7371:End
//#8353:End
                }
                foreach ($client['current_month'] as $sales_slip) {
//#8353:Start
//#7371:Start
                    $total += $sales_slip['total_amount_tax'];
//                    if ($sales_slip['is_ad_receive_payment'] == 0) {
//                        $total += $sales_slip['total_amount_tax'];
//                    }
//#7371:End
//#8353:End
                }
                if ($total != ($client['payment_amount'] + $client['prev_ad_receive_payment_amount'])) unset($clients[$key]);
            }
        }
        // 当月入金＝当月入金予定分＋前月以前売掛金 消し込み
        if ($action == 3) {
            foreach ($clients as $key => $client) {
                if ($client['payment_amount'] == 0) {
                    unset($clients[$key]);
                    continue;
                }
                
                $total = 0;
                foreach ($client['prev_month'] as $sales_slip) {
//#8353:Start
//#7371:Start
                    $total += $sales_slip['total_amount_tax'];
//                    if ($sales_slip['is_ad_receive_payment'] == 0) {
//                        $total += $sales_slip['total_amount_tax'];
//                    }
//#7371:End
//#8353:End
                }

                foreach ($client['current_month'] as $sales_slip) {
//#8353:Start
//#7371:Start
                    $total += $sales_slip['total_amount_tax'];
//                    if ($sales_slip['is_ad_receive_payment'] == 0) {
//                        $total += $sales_slip['total_amount_tax'];
//                    }
//#7371:End
//#8353:End
                }
                if ($total != $client['payment_amount']) unset($clients[$key]);
            }
        }
        // echo "<pre>";print_r($clients);die;
        return $clients;
    }

    public function update_clearance_data($datas) {
        $sales_slip_checked_datas        = array();
        $payment_past_checked_datas      = array();
        $update_payment_entry_datas      = array();
        $insert_payment_entry_datas      = array();
        $update_prev_payment_entry_datas = array();
        $summary_month = $this->_get_max_closing_accountant_date();

        foreach($datas as $key => $data) {
            // pass the header and footer row
            if ($key == 0 || $key == 1 || $key == count($datas) - 1 || $key == count($datas) - 2) continue;

            // process to update sales_slip data
//#6892:Start
//            if (isset($data['prev_month']['is_checked'])) {
            if (isset($data['prev_month']['id']) && isset($data['prev_month']['is_checked'])) {
//#6892:End
//#9218:Start
//                if (!($data['prev_month']['is_checked'] == true && $data['prev_month']['default_is_checked'] == 1)) {
                if (isset($data['prev_month']['is_past_data'])) {
                    $payment_past_checked_datas[] = $this->_set_sales_slip_checked_data($data['prev_month'], $summary_month);
                } else {
                    $sales_slip_checked_datas[] = $this->_set_sales_slip_checked_data($data['prev_month'], $summary_month);
                }
//                }
//#9218:End
            }
//#6892:Start
//            if (isset($data['cur_month']['is_checked'])) {
            if (isset($data['cur_month']['id']) && isset($data['cur_month']['is_checked'])) {
//#6892:End
//#9218:Start
//                if (!($data['cur_month']['is_checked'] == true && $data['cur_month']['default_is_checked'] == 1)) {
                $sales_slip_checked_datas[] = $this->_set_sales_slip_checked_data($data['cur_month'], $summary_month);
//                }
//#9218:End
            }
//#6892:Start            
//            if (isset($data['next_month']['is_checked'])) {
            if (isset($data['next_month']['id']) && isset($data['next_month']['is_checked'])) {
//#6892:End
//#9218:Start
//                if (!($data['next_month']['is_checked'] == true && $data['next_month']['default_is_checked'] == 1)) {
                $sales_slip_checked_datas[] = $this->_set_sales_slip_checked_data($data['next_month'], $summary_month);
//                }
//#9218:End
            }

            // process to update payment_entry
            if ($key == $data['parent_row']) {
                $entry = array(
                    'etc'                          => $this->_set_payment_entry_data($data['payment_entry']['etc']),
                    'fee'                          => $this->_set_payment_entry_data($data['payment_entry']['fee']),
                    'repayment'                    => $this->_set_payment_entry_data($data['payment_entry']['repayment']),
                    'miscellaneous_loss'           => $this->_set_payment_entry_data($data['payment_entry']['miscellaneous_loss']),
                    'miscellaneous_income'         => $this->_set_payment_entry_data($data['payment_entry']['miscellaneous_income']),
                    'offset_next_month_receive'    => $this->_set_payment_entry_data($data['payment_entry']['offset_next_month_receive']),
                    'offset_next_month_ad_receive' => $this->_set_payment_entry_data($data['payment_entry']['offset_next_month_ad_receive']),
                    'bugyo_next_month_ad_receive'  => $this->_set_payment_entry_data($data['payment_entry']['bugyo_next_month_ad_receive']),
                    'bugyo_next_month_receive'     => $this->_set_payment_entry_data($data['payment_entry']['bugyo_next_month_receive']),
                    'lastup_account_id'            => $this->auth->get_account_id(),
                );

                if ($data['payment_entry']['id']) {
                    $entry['id'] = $data['payment_entry']['id'];
                    $entry['lastup_datetime'] = $this->_db_now();
                    $update_payment_entry_datas[] = $entry;
                } else {
                    $entry['row'] = $key;
                    $entry['entry_date'] = $summary_month;
                    $entry['client_id'] = $data['client_id'];
                    $entry['create_datetime'] = $this->_db_now();
//#8353:Start
                    $entry['lastup_datetime'] = $this->_db_now();
//#8353:End
                    $insert_payment_entry_datas[] = $entry;
                }

                if (isset($data['is_return_ad_receive'])) {
                    $update_prev_payment_entry_datas[] = array(
                        'id' => $data['prev_payment_entry']['id'],
                        'offset_next_month_ad_receive' => $data['prev_ad_receive_payment_amount'],
                    );
                }

                if (isset($data['payment_entry']['ad_receive_past_data_id']) && $data['payment_entry']['ad_receive_past_data_id'] != 0) {
                    $payment_past_checked_datas[] = $this->_set_sales_slip_checked_data(array('is_checked' => true, 'id' => $data['payment_entry']['ad_receive_past_data_id']), $summary_month);
                }
            }
        }

        $payment_entry_ids = array();

        $this->db->trans_start();

        if (!empty($sales_slip_checked_datas)) {
            $this->db->update_batch('sales_slip', $sales_slip_checked_datas, 'id');
        }

        if (!empty($payment_past_checked_datas)) {
            $this->db->update_batch('payment_past_data', $payment_past_checked_datas, 'id');
        }

        if (!empty($update_payment_entry_datas)) {
            $this->db->update_batch('payment_entry', $update_payment_entry_datas, 'id');
        }

        if (!empty($update_prev_payment_entry_datas)) {
            $this->db->update_batch('payment_entry', $update_prev_payment_entry_datas, 'id');
        }

        if (!empty($insert_payment_entry_datas)) {
            foreach ($insert_payment_entry_datas as $entry) {
                $row = $entry['row'];
                unset($entry['row']);
                $this->db->insert('payment_entry', $entry);
                $payment_entry_ids[] = array(
                    'row' => $row,
                    'id' => $this->db->insert_id(),
                );
            }
        }

        $this->db->trans_complete();

        if (!$this->db->trans_status()) {
            return array(
                'status' => false,
                'messages' => lang('lbl_edit_error'),
            );
        }
        return array(
            'status' => true,
            'messages' => lang('lbl_edit_success'),
            'payment_entry_ids' => $payment_entry_ids,
        );
    }

    protected function _set_payment_entry_data($data) {
        return $data ? $data : 0;
    }

    protected function _set_sales_slip_checked_data($data, $summary_month) {
        return array(
            'is_checked' => $data['is_checked'] == true ? 1 : 0,
            'check_date' => $data['is_checked'] == true ? $summary_month : '0000-00-00',
            'id'         => $data['id'],
//#8353:Start
            'lastup_account_id' => $this->auth->get_account_id(),
            'lastup_datetime'   => $this->_db_now(),
//#8353:End
        );
    }

    public function rules($rule_name) {
        $validate_rule['update'] = array (
                array(
                    'field'  => 'etc',
                    'label'  => 'etc',
                    'rules'  => 'trim|integer|greater_than_equal_to[0]|less_than_equal_to[999999999]',
                    'errors' => array(
                                    'integer'               => strip_tags(str_replace('%field%', lang('lbl_etc'), lang('error_is_integer'))),
                                    'greater_than_equal_to' => strip_tags(str_replace('%field%', lang('lbl_etc'), lang('error_can_not_be_negative'))),
                                    'less_than_equal_to'    => strip_tags(str_replace('%field%', lang('lbl_etc'), lang('error_9_length'))),
                                )
                ),
                array(
                    'field'  => 'fee',
                    'label'  => 'fee',
                    'rules'  => 'trim|integer|greater_than_equal_to[0]|less_than_equal_to[999999999]',
                    'errors' => array(
                                    'integer'               => strip_tags(str_replace('%field%', lang('lbl_fee'), lang('error_is_integer'))),
                                    'greater_than_equal_to' => strip_tags(str_replace('%field%', lang('lbl_fee'), lang('error_can_not_be_negative'))),
                                    'less_than_equal_to'    => strip_tags(str_replace('%field%', lang('lbl_fee'), lang('error_9_length'))),
                                )
                ),
                array(
                    'field'  => 'repayment',
                    'label'  => 'repayment',
                    'rules'  => 'trim|integer|greater_than_equal_to[0]|less_than_equal_to[999999999]',
                    'errors' => array(
                                    'integer'               => strip_tags(str_replace('%field%', lang('lbl_repayment'), lang('error_is_integer'))),
                                    'greater_than_equal_to' => strip_tags(str_replace('%field%', lang('lbl_repayment'), lang('error_can_not_be_negative'))),
                                    'less_than_equal_to'    => strip_tags(str_replace('%field%', lang('lbl_repayment'), lang('error_9_length'))),
                                )
                ),
                array(
                    'field'  => 'miscellaneous_loss',
                    'label'  => 'miscellaneous_loss',
                    'rules'  => 'trim|integer|greater_than_equal_to[0]|less_than_equal_to[999999999]',
                    'errors' => array(
                                    'integer'               => strip_tags(str_replace('%field%', lang('lbl_miscellaneous_loss'), lang('error_is_integer'))),
                                    'greater_than_equal_to' => strip_tags(str_replace('%field%', lang('lbl_miscellaneous_loss'), lang('error_can_not_be_negative'))),
                                    'less_than_equal_to'    => strip_tags(str_replace('%field%', lang('lbl_miscellaneous_loss'), lang('error_9_length'))),
                                )
                ),
                array(
                    'field'  => 'miscellaneous_income',
                    'label'  => 'miscellaneous_income',
                    'rules'  => 'trim|integer|greater_than_equal_to[0]|less_than_equal_to[999999999]',
                    'errors' => array(
                                    'integer'               => strip_tags(str_replace('%field%', lang('lbl_miscellaneous_income'), lang('error_is_integer'))),
                                    'greater_than_equal_to' => strip_tags(str_replace('%field%', lang('lbl_miscellaneous_income'), lang('error_can_not_be_negative'))),
                                    'less_than_equal_to'    => strip_tags(str_replace('%field%', lang('lbl_miscellaneous_income'), lang('error_9_length'))),
                                )
                ),
                array(
                    'field'  => 'offset_next_month_receive',
                    'label'  => 'offset_next_month_receive',
//#9547:Start
//                    'rules'  => 'trim|integer|greater_than_equal_to[0]|less_than_equal_to[999999999]',
                    'rules'  => 'trim|integer|greater_than_equal_to[0]|less_than_equal_to['. MAX_SIGNED_INT_10 .']',
                    'errors' => array(
                                    'integer'               => strip_tags(str_replace('%field%', lang('lbl_offset_next_month_receive'), lang('error_is_integer'))),
                                    'greater_than_equal_to' => strip_tags(str_replace('%field%', lang('lbl_offset_next_month_receive'), lang('error_can_not_be_negative'))),
//                                    'less_than_equal_to'    => strip_tags(str_replace('%field%', lang('lbl_offset_next_month_receive'), lang('error_9_length'))),
                                    'less_than_equal_to'    => strip_tags(str_replace('%field%', lang('lbl_offset_next_month_receive'), lang('lbl_out_of_range'))),
//#9547:End
                                )
                ),
                array(
                    'field'  => 'offset_next_month_ad_receive',
                    'label'  => 'offset_next_month_ad_receive',
//#9547:Start
//                    'rules'  => 'trim|integer|greater_than_equal_to[0]|less_than_equal_to[999999999]',
                    'rules'  => 'trim|integer|greater_than_equal_to[0]|less_than_equal_to['. MAX_SIGNED_INT_10 .']',
                    'errors' => array(
                                    'integer'               => strip_tags(str_replace('%field%', lang('lbl_offset_next_month_ad_receive'), lang('error_is_integer'))),
                                    'greater_than_equal_to' => strip_tags(str_replace('%field%', lang('lbl_offset_next_month_ad_receive'), lang('error_can_not_be_negative'))),
//                                    'less_than_equal_to'    => strip_tags(str_replace('%field%', lang('lbl_offset_next_month_ad_receive'), lang('error_9_length'))),
                                    'less_than_equal_to'    => strip_tags(str_replace('%field%', lang('lbl_offset_next_month_ad_receive'), lang('lbl_out_of_range'))),
//#9547:End
                                )
                ),
                array(
                    'field'  => 'bugyo_next_month_receive',
                    'label'  => 'bugyo_next_month_receive',
//#9547:Start
//                    'rules'  => 'trim|integer|greater_than_equal_to[0]|less_than_equal_to[999999999]',
                    'rules'  => 'trim|integer|greater_than_equal_to[0]|less_than_equal_to['. MAX_SIGNED_INT_10 .']',
                    'errors' => array(
                                    'integer'               => strip_tags(str_replace('%field%', lang('lbl_bugyo_next_month_receive'), lang('error_is_integer'))),
                                    'greater_than_equal_to' => strip_tags(str_replace('%field%', lang('lbl_bugyo_next_month_receive'), lang('error_can_not_be_negative'))),
//                                    'less_than_equal_to'    => strip_tags(str_replace('%field%', lang('lbl_bugyo_next_month_receive'), lang('error_9_length'))),
                                    'less_than_equal_to'    => strip_tags(str_replace('%field%', lang('lbl_bugyo_next_month_receive'), lang('lbl_out_of_range'))),
//#9547:End
                                )
                ),
                array(
                    'field'  => 'bugyo_next_month_ad_receive',
                    'label'  => 'bugyo_next_month_ad_receive',
//#9547:Start
//                    'rules'  => 'trim|integer|greater_than_equal_to[0]|less_than_equal_to[999999999]',
                    'rules'  => 'trim|integer|greater_than_equal_to[0]|less_than_equal_to['. MAX_SIGNED_INT_10 .']',
                    'errors' => array(
                                    'integer'               => strip_tags(str_replace('%field%', lang('lbl_bugyo_next_month_ad_receive'), lang('error_is_integer'))),
                                    'greater_than_equal_to' => strip_tags(str_replace('%field%', lang('lbl_bugyo_next_month_ad_receive'), lang('error_can_not_be_negative'))),
//                                    'less_than_equal_to'    => strip_tags(str_replace('%field%', lang('lbl_bugyo_next_month_ad_receive'), lang('error_9_length'))),
                                    'less_than_equal_to'    => strip_tags(str_replace('%field%', lang('lbl_bugyo_next_month_ad_receive'), lang('lbl_out_of_range'))),
                                )
                ),
                array(
                    'field'  => 'payment',
                    'label'  => 'payment',
                    'rules'  => 'trim|integer|greater_than_equal_to[0]',
                    'errors' => array(
                                    'integer'               => strip_tags(str_replace('%field%', lang('lbl_current_month_mount'), lang('error_is_integer'))),
                                    'greater_than_equal_to' => strip_tags(str_replace('%field%', lang('lbl_current_month_mount'), lang('error_can_not_be_negative'))),
                                )
                ),
//#9547:End
        );


        if (isset($validate_rule[$rule_name])) {
            return $validate_rule[$rule_name];
        }
        return array ();
    }

    public function run_update_validate($datas) {
        
        $this->load->library('form_validation');

        $errors = array();

        foreach ($datas as $key => $data) {
            if ($key == 0 || $key == 1 || $key == count($datas) - 1 || $key == count($datas) - 2) continue;

            if ($key == $data['parent_row']) {
                $this->form_validation->set_rules($this->rules('update'));
                $this->form_validation->set_data($data['payment_entry']);

                if ($this->form_validation->run() === FALSE) {
                    $errors[$data['s_code']] = $this->form_validation->error_array();
                }
                $this->form_validation->reset_validation();
            }
        }
        return $errors;
    }

    protected function _add_asterisk($client_id) {
        if (in_array($client_id, $this->_list_client)) return '';
        $this->_list_client[] = $client_id;
        return '*';
    }

    /**
     *  当月請求額 ≠ 0
     * @return array
     */
    protected function _condition_2($sales_slip, $summary_month) {
        $data                 = array();
        $have_tax             = false;
        $no_tax               = false;
        $flag                 = false;
        $total_non_tax_amount = 0;
//#7376:Start
        $total_amount_tax     = 0;

        $delimiter = strpos($summary_month, '/') !== false ? '/' : '-';
        $parts     = explode($delimiter, $summary_month);
        $year      = $parts[0];
        $month     = $parts[1];
        
        // $details = $this->_get_detail_by_sales_slip_id();
//        $details = sales_slip_export_pdf($sales_slip['id']);
        $details = $this->_get_fixed_sales_slip_detail($sales_slip['id']);
        // echo "<pre>";print_r($details);die;
//#7376:End

        // echo "<pre>";print_r($details);
        foreach($details as $detail) {
//#7376:Start
            $delivery_date_parts = explode('-', $detail['delivery_date']);
            $delivery_date_year  = $delivery_date_parts[0];
            $delivery_date_month = $delivery_date_parts[1];
//#7376:End

            if ($detail['tax_type'] == TAX_EXCLUSIVE || $detail['tax_type'] == TAX_INCLUDED) $have_tax = true;
            if ($detail['tax_type'] == TAX_NON) {
                $no_tax = true;
//#7376:Start
                if ($year == $delivery_date_year && $month == $delivery_date_month) {
                    $total_non_tax_amount += $detail['amount'];
                }
//#7376:End
            }

            if (!$flag) {
                $newest_delivery_date = strtotime($detail['delivery_date']);
                $flag = true;
            }

//#7376:Start
            if ($year == $delivery_date_year && $month == $delivery_date_month) {
                $total_amount_tax = $sales_slip['total_amount_tax'];
            }
//#7376:End
            $newest_delivery_date = $newest_delivery_date < strtotime($detail['delivery_date']) ? strtotime($detail['delivery_date']) : $newest_delivery_date;
        }

        //Trường hợp không có trộn lẫn có thuế 課税売上 và không thuế 非課税売上
        if (($have_tax && !$no_tax) || (!$have_tax && $no_tax)) {
            // có thuế
            if ($have_tax && !$no_tax) {
                $data['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
                $data['CSJS005'] = date('Y-m-d', $newest_delivery_date);
                $data['CSJS200'] = $sales_slip['bugyo_department_code'];
                $data['CSJS201'] = 1030;
//#7376:Start
//                $data['CSJS213'] = $sales_slip['total_amount_tax'];
                $data['CSJS213'] = $total_amount_tax;
//#7376:End
                $data['CSJS300'] = $sales_slip['bugyo_department_code'];
                $data['CSJS301'] = $sales_slip['bugyo_account_code'];
                $data['CSJS306'] = 2;
//#7376:Start
//                $data['CSJS313'] = $sales_slip['total_amount_tax'];
                $data['CSJS313'] = $total_amount_tax;
//#7376:End
                $data['CSJS314'] = $sales_slip['total_tax'];
                $data['CSJS208'] = $sales_slip['s_code'];
                $data['CSJS308'] = $sales_slip['s_code'];
                $data['CSJS100'] = $this->_get_field_CSJS100_data($sales_slip['bugyo_abstract_string'], $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)), true);
                // $data['condition NO'] = 'condition 2|c|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];


                return array($data);
            }
            // không thuế
            if (!$have_tax && $no_tax) {
                $data['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
                $data['CSJS005'] = date('Y-m-d', $newest_delivery_date);
                $data['CSJS200'] = $sales_slip['bugyo_department_code'];
                $data['CSJS201'] = 1030;
//#7376:Start
//                $data['CSJS213'] = $sales_slip['total_amount_tax'];
                $data['CSJS213'] = $total_amount_tax;
//#7376:End
                $data['CSJS300'] = $sales_slip['bugyo_department_code'];
                $data['CSJS301'] = $sales_slip['bugyo_account_code'];
                $data['CSJS303'] = "0080";
//#7376:Start
//                $data['CSJS313'] = $sales_slip['total_amount_tax'];
                $data['CSJS313'] = $total_amount_tax;
//#7376:End
                $data['CSJS208'] = $sales_slip['s_code'];
                $data['CSJS308'] = $sales_slip['s_code'];
                $data['CSJS100'] = $this->_get_field_CSJS100_data($sales_slip['bugyo_abstract_string'], $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)), true);
                // $data['condition NO'] = 'condition 2|d|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

                return array($data);
            }
        }

        //Trường hợp có trộn lẫn có thuế 課税売上 và không thuế 非課税売上
        if ($have_tax && $no_tax) {
            $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_1['CSJS005'] = date('Y-m-d', $newest_delivery_date);
            $record_1['CSJS200'] = $sales_slip['bugyo_department_code'];
            $record_1['CSJS201'] = 1030;
            $record_1['CSJS213'] = $sales_slip['total_amount_tax'];
            $record_1['CSJS300'] = $sales_slip['bugyo_department_code'];
            $record_1['CSJS301'] = $sales_slip['bugyo_account_code'];
            $record_1['CSJS306'] = 2;
//#7376:Start
//            $record_1['CSJS313'] = $sales_slip['total_amount_tax'] - $total_non_tax_amount;
            $record_1['CSJS313'] = $total_amount_tax - $total_non_tax_amount;
//#7376:End
            $record_1['CSJS314'] = $sales_slip['total_tax'];
            $record_1['CSJS308'] = $sales_slip['s_code'];
            $record_1['CSJS100'] = $this->_get_field_CSJS100_data($sales_slip['bugyo_abstract_string'], $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)), true);
            // $record_1['condition NO'] = 'condition 2|e|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            $record_2['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_2['CSJS005'] = date('Y-m-d', $newest_delivery_date);
            $record_2['CSJS300'] = $sales_slip['bugyo_department_code'];
            $record_2['CSJS301'] = $sales_slip['bugyo_account_code'];
            $record_2['CSJS303'] = "0080";
            $record_2['CSJS313'] = $total_non_tax_amount;
            $record_2['CSJS208'] = $sales_slip['s_code'];
            $record_2['CSJS308'] = $sales_slip['s_code'];
            $record_2['CSJS100'] = $this->_get_field_CSJS100_data($sales_slip['bugyo_abstract_string'], $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)), true);
            // $record_2['condition NO'] = 'condition 2|e|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            return array($record_1, $record_2);
        }
    }

    /**
     * 前月前受金 ＝ 0 AND （前月売掛金＋当月計上請求額） ≧ （当月入金＋手数料）
     * @return array
     */
    protected function _condition_3($sales_slip, $payment, $fee, $summary_month) {
        if ($payment > 0 && $fee > 0) {
            $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_ORDINARY_DEPOSIT);
            $record_1['CSJS201'] = 1014;
            $record_1['CSJS213'] = $payment;
            $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_1['CSJS301'] = 1030;
            $record_1['CSJS313'] = $payment;
            $record_1['CSJS208'] = $sales_slip['s_code'];
            $record_1['CSJS308'] = $sales_slip['s_code'];
            $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));

            // $record_1['condition NO'] = 'condition 3|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            $record_2['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_2['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_2['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_COMMISSION_PAID);
            $record_2['CSJS201'] = 7212;
            $record_2['CSJS206'] = 2;
            $record_2['CSJS213'] = $fee;
//            $record_2['CSJS214'] = f_ceil($fee - ($fee / (1 + $this->_get_tax_rate())));
            $record_2['CSJS214'] = f_ceil(bcsub($fee, bcdiv($fee, bcadd(1, $this->_get_tax_rate()))));
            $record_2['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_2['CSJS301'] = 1030;
            $record_2['CSJS313'] = $fee;
            $record_2['CSJS208'] = $sales_slip['s_code'];
            $record_2['CSJS308'] = $sales_slip['s_code'];
            $record_2['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_COMMISSION_PAID, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_2['condition NO'] = 'condition 3|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];
            return array($record_1, $record_2);
        }

        // 普通預金 ＞ 0 AND 手数料 ＝ 0
        if ($payment > 0 && $fee == 0) {
            $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_ORDINARY_DEPOSIT);
            $record_1['CSJS201'] = 1014;
            $record_1['CSJS213'] = $payment;
            $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_1['CSJS301'] = 1030;
            $record_1['CSJS313'] = $payment;
            $record_1['CSJS208'] = $sales_slip['s_code'];
            $record_1['CSJS308'] = $sales_slip['s_code'];
            $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_1['condition NO'] = 'condition 3|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];
            return array($record_1);
        }

        // 普通預金 ＝ 0 AND 手数料 ＞ 0
        if ($payment == 0 && $fee > 0) {
            $record_2['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_2['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_2['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_COMMISSION_PAID);
            $record_2['CSJS201'] = 7212;
            $record_2['CSJS206'] = 2;
            $record_2['CSJS213'] = $fee;
//            $record_2['CSJS214'] = f_ceil($fee - ($fee / (1 + $this->_get_tax_rate())));
            $record_2['CSJS214'] = f_ceil(bcsub($fee, bcdiv($fee, bcadd(1, $this->_get_tax_rate()))));
            $record_2['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_2['CSJS301'] = 1030;
            $record_2['CSJS313'] = $fee;
            $record_2['CSJS208'] = $sales_slip['s_code'];
            $record_2['CSJS308'] = $sales_slip['s_code'];
            $record_2['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_COMMISSION_PAID, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_2['condition NO'] = 'condition 3|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];
            return array($record_2);
        }
    }

    /**
     * 前月前受金 ＝ 0 AND （前月売掛金＋当月計上請求額） ＞ 0 AND （前月売掛金＋当月計上請求額）
     *  ＜ （当月入金＋手数料）
     * @return array
     */
    protected function _condition_4($sales_slip, $payment, $fee, $prev_payment_entry, $summary_month) {
        // 普通預金 ＞ 0 AND 手数料 ＞ 0
        if ($payment > 0 && $fee > 0) {
            $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_ORDINARY_DEPOSIT);
            $record_1['CSJS201'] = 1014;
            $record_1['CSJS213'] = $payment;
            $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_1['CSJS301'] = 1030;
            $record_1['CSJS313'] = $prev_payment_entry['bugyo_next_month_receive'] + $sales_slip['total_amount_tax'];
            $record_1['CSJS208'] = $sales_slip['s_code'];
            $record_1['CSJS308'] = $sales_slip['s_code'];
            $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_1['condition NO'] = 'condition 4|b|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            $record_2['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_2['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_2['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
            $record_2['CSJS301'] = 3017;
            $record_2['CSJS313'] = $payment + $fee - ($prev_payment_entry['bugyo_next_month_receive'] + $sales_slip['total_amount_tax']);
            $record_2['CSJS308'] = $sales_slip['s_code'];
            $record_2['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_2['condition NO'] = 'condition 4|b|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            $record_3['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_3['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_3['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_COMMISSION_PAID);
            $record_3['CSJS201'] = 7212;
            $record_3['CSJS206'] = 2;
            $record_3['CSJS213'] = $fee;
//            $record_3['CSJS214'] = f_ceil($fee - ($fee / (1 + $this->_get_tax_rate())));
            $record_3['CSJS214'] = f_ceil(bcsub($fee, bcdiv($fee, bcadd(1, $this->_get_tax_rate()))));
            $record_3['CSJS208'] = $sales_slip['s_code'];
            $record_3['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_COMMISSION_PAID, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));

            // $record_3['condition NO'] = 'condition 4|b|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            return array($record_1, $record_2, $record_3);
        }

        // 普通預金 ＞ 0 AND 手数料 ＝ 0
        if ($payment > 0 && $fee == 0) {
            $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_ORDINARY_DEPOSIT);
            $record_1['CSJS201'] = 1014;
            $record_1['CSJS213'] = $payment;
            $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_1['CSJS301'] = 1030;
            $record_1['CSJS313'] = $prev_payment_entry['bugyo_next_month_receive'] + $sales_slip['total_amount_tax'];
            $record_1['CSJS208'] = $sales_slip['s_code'];
            $record_1['CSJS308'] = $sales_slip['s_code'];
            $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_1['condition NO'] = 'condition 4|c|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            $record_2['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_2['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_2['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
            $record_2['CSJS301'] = 3017;
            $record_2['CSJS313'] = $payment + $fee - ($prev_payment_entry['bugyo_next_month_receive'] + $sales_slip['total_amount_tax']);
            $record_2['CSJS308'] = $sales_slip['s_code'];
            $record_2['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_2['condition NO'] = 'condition 4|c|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            return array($record_1, $record_2);
        }

        // 普通預金 ＝ 0 AND 手数料 ＞ 0
        if ($payment == 0 && $fee > 0) {
            $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_COMMISSION_PAID);
            $record_1['CSJS201'] = 7212;
            $record_1['CSJS206'] = 2;
            $record_1['CSJS213'] = $fee;
//            $record_1['CSJS214'] = f_ceil($fee - ($fee / (1 + $this->_get_tax_rate())));
            $record_1['CSJS214'] = f_ceil(bcsub($fee, bcdiv($fee, bcadd(1, $this->_get_tax_rate()))));
            $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_1['CSJS301'] = 1030;
            $record_1['CSJS313'] = $prev_payment_entry['bugyo_next_month_receive'] + $sales_slip['total_amount_tax'];
            $record_1['CSJS208'] = $sales_slip['s_code'];
            $record_1['CSJS308'] = $sales_slip['s_code'];
            $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_COMMISSION_PAID, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_1['condition NO'] = 'condition 4|d|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            $record_2['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_2['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_2['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
            $record_2['CSJS301'] = 3017;
            $record_2['CSJS313'] = $payment + $fee - ($prev_payment_entry['bugyo_next_month_receive'] + $sales_slip['total_amount_tax']);
            $record_2['CSJS308'] = $sales_slip['s_code'];
            $record_2['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_COMMISSION_PAID, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_2['condition NO'] = 'condition 4|d|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            return array($record_1, $record_2);
        }
    }

    /**
     * （当月入金＋手数料） ≧ 0 AND （前月売掛金＋当月計上請求額）＝ 0
     * @return array
     */
    protected function _condition_5($sales_slip, $payment, $fee, $prev_payment_entry, $summary_month) {
        if ($payment > 0 && $fee > 0) {
            $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_ORDINARY_DEPOSIT);
            $record_1['CSJS201'] = 1014;
            $record_1['CSJS213'] = $payment;
            $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
            $record_1['CSJS301'] = 3017;
            $record_1['CSJS313'] = $payment;
            $record_1['CSJS208'] = $sales_slip['s_code'];
            $record_1['CSJS308'] = $sales_slip['s_code'];
            $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_1['condition NO'] = 'condition 5|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            $record_2['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_2['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_2['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_COMMISSION_PAID);
            $record_2['CSJS201'] = 7212;
            $record_2['CSJS206'] = 2;
            $record_2['CSJS213'] = $fee;
//            $record_2['CSJS214'] = f_ceil($fee - ($fee / (1 + $this->_get_tax_rate())));
            $record_2['CSJS214'] = f_ceil(bcsub($fee, bcdiv($fee, bcadd(1, $this->_get_tax_rate()))));
            $record_2['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
            $record_2['CSJS301'] = 3017;
            $record_2['CSJS313'] = $fee;
            $record_2['CSJS208'] = $sales_slip['s_code'];
            $record_2['CSJS308'] = $sales_slip['s_code'];
            $record_2['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_COMMISSION_PAID, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_2['condition NO'] = 'condition 5|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];
            return array($record_1, $record_2);
        }

        // 普通預金 ＞ 0 AND 手数料 ＝ 0
        if ($payment > 0 && $fee == 0) {
            $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_ORDINARY_DEPOSIT);
            $record_1['CSJS201'] = 1014;
            $record_1['CSJS213'] = $payment;
            $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
            $record_1['CSJS301'] = 3017;
            $record_1['CSJS313'] = $payment;
            $record_1['CSJS208'] = $sales_slip['s_code'];
            $record_1['CSJS308'] = $sales_slip['s_code'];
            $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_1['condition NO'] = 'condition 5|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];
            return array($record_1);
        }

        // 普通預金 ＝ 0 AND 手数料 ＞ 0
        if ($payment == 0 && $fee > 0) {
            $record_2['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_2['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_2['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_COMMISSION_PAID);
            $record_2['CSJS201'] = 7212;
            $record_2['CSJS206'] = 2;
            $record_2['CSJS213'] = $fee;
//            $record_2['CSJS214'] = f_ceil($fee - ($fee / (1 + $this->_get_tax_rate())));
            $record_2['CSJS214'] = f_ceil(bcsub($fee, bcdiv($fee, bcadd(1, $this->_get_tax_rate()))));
            $record_2['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
            $record_2['CSJS301'] = 3017;
            $record_2['CSJS313'] = $fee;
            $record_2['CSJS208'] = $sales_slip['s_code'];
            $record_2['CSJS308'] = $sales_slip['s_code'];
            $record_2['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_COMMISSION_PAID, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_2['condition NO'] = 'condition 5|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];
            return array($record_2);
        }
    }

    /**
     * 前月前受金 ＞ 0 AND （前月売掛金＋当月計上請求額） ≧ （前月前受金＋当月入金＋手数料）
     * @return array
     */
    protected function _condition_6($sales_slip, $payment, $fee, $prev_payment_entry, $summary_month) {
        if ($prev_payment_entry['bugyo_next_month_ad_receive'] > 0 && $payment > 0 && $fee > 0) {
            $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
            $record_1['CSJS201'] = 3017;
            $record_1['CSJS213'] = $prev_payment_entry['bugyo_next_month_ad_receive'];
            $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_1['CSJS301'] = 1030;
            $record_1['CSJS313'] = $prev_payment_entry['bugyo_next_month_ad_receive'];
            $record_1['CSJS208'] = $sales_slip['s_code'];
            $record_1['CSJS308'] = $sales_slip['s_code'];
            $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_AD_RECEIVE, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_1['condition NO'] = 'condition 6|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            $record_2['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_2['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_2['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_ORDINARY_DEPOSIT);
            $record_2['CSJS201'] = 1014;
            $record_2['CSJS213'] = $payment;
            $record_2['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_2['CSJS301'] = 1030;
            $record_2['CSJS313'] = $payment;
            $record_2['CSJS208'] = $sales_slip['s_code'];
            $record_2['CSJS308'] = $sales_slip['s_code'];
            $record_2['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_2['condition NO'] = 'condition 6|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            $record_3['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_3['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_3['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_COMMISSION_PAID);
            $record_3['CSJS201'] = 7212;
            $record_3['CSJS206'] = 2;
            $record_3['CSJS213'] = $fee;
//            $record_3['CSJS214'] = f_ceil($fee - ($fee / (1 + $this->_get_tax_rate())));
            $record_3['CSJS214'] = f_ceil(bcsub($fee, bcdiv($fee, bcadd(1, $this->_get_tax_rate()))));
            $record_3['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_3['CSJS301'] = 1030;
            $record_3['CSJS313'] = $fee;
            $record_3['CSJS208'] = $sales_slip['s_code'];
            $record_3['CSJS308'] = $sales_slip['s_code'];
            $record_3['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_COMMISSION_PAID, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_3['condition NO'] = 'condition 6|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];
            return array($record_1, $record_2, $record_3);
        }

        if ($prev_payment_entry['bugyo_next_month_ad_receive'] > 0 && $payment > 0 && $fee == 0) {
            $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
            $record_1['CSJS201'] = 3017;
            $record_1['CSJS213'] = $prev_payment_entry['bugyo_next_month_ad_receive'];
            $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_1['CSJS301'] = 1030;
            $record_1['CSJS313'] = $prev_payment_entry['bugyo_next_month_ad_receive'];
            $record_1['CSJS208'] = $sales_slip['s_code'];
            $record_1['CSJS308'] = $sales_slip['s_code'];
            $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_AD_RECEIVE, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_1['condition NO'] = 'condition 6|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];
            
            $record_2['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_2['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_2['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_ORDINARY_DEPOSIT);
            $record_2['CSJS201'] = 1014;
            $record_2['CSJS213'] = $payment;
            $record_2['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_2['CSJS301'] = 1030;
            $record_2['CSJS313'] = $payment;
            $record_2['CSJS208'] = $sales_slip['s_code'];
            $record_2['CSJS308'] = $sales_slip['s_code'];
            $record_2['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_2['condition NO'] = 'condition 6|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];
            return array($record_1, $record_2);
        }

        if ($prev_payment_entry['bugyo_next_month_ad_receive'] > 0 && $payment == 0 && $fee > 0) {
            $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
            $record_1['CSJS201'] = 3017;
            $record_1['CSJS213'] = $prev_payment_entry['bugyo_next_month_ad_receive'];
            $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_1['CSJS301'] = 1030;
            $record_1['CSJS313'] = $prev_payment_entry['bugyo_next_month_ad_receive'];
            $record_1['CSJS208'] = $sales_slip['s_code'];
            $record_1['CSJS308'] = $sales_slip['s_code'];
            $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_AD_RECEIVE, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_1['condition NO'] = 'condition 6|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            $record_3['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_3['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_3['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_COMMISSION_PAID);
            $record_3['CSJS201'] = 7212;
            $record_3['CSJS206'] = 2;
            $record_3['CSJS213'] = $fee;
//            $record_3['CSJS214'] = f_ceil($fee - ($fee / (1 + $this->_get_tax_rate())));
            $record_3['CSJS214'] = f_ceil(bcsub($fee, bcdiv($fee, bcadd(1, $this->_get_tax_rate()))));
            $record_3['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_3['CSJS301'] = 1030;
            $record_3['CSJS313'] = $fee;
            $record_3['CSJS208'] = $sales_slip['s_code'];
            $record_3['CSJS308'] = $sales_slip['s_code'];
            $record_3['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_COMMISSION_PAID, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_3['condition NO'] = 'condition 6|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];
            return array($record_1, $record_3);
        }

        if ($prev_payment_entry['bugyo_next_month_ad_receive'] > 0 && $payment == 0 && $fee == 0) {
            $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
            $record_1['CSJS201'] = 3017;
            $record_1['CSJS213'] = $prev_payment_entry['bugyo_next_month_ad_receive'];
            $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_1['CSJS301'] = 1030;
            $record_1['CSJS313'] = $prev_payment_entry['bugyo_next_month_ad_receive'];
            $record_1['CSJS208'] = $sales_slip['s_code'];
            $record_1['CSJS308'] = $sales_slip['s_code'];
            $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_AD_RECEIVE, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_1['condition NO'] = 'condition 6|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];
            return array($record_1);
        }
    }

    /**
     * 前月前受金 ＞ 0 AND 当月計上請求額 ＜ （前月前受金＋当月入金＋手数料） AND 当月計上請求額 ≧ （当月入金＋手数料）
     * @return array
     */
    protected function _condition_7($sales_slip, $payment, $fee, $prev_payment_entry, $summary_month) {
        if ($prev_payment_entry['bugyo_next_month_ad_receive'] > 0 && $payment > 0 && $fee > 0) {
            if (($sales_slip['total_amount_tax'] - ($payment + $fee)) > 0) {
                $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
                $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();;
                $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
                $record_1['CSJS201'] = 3017;
                $record_1['CSJS213'] = $sales_slip['total_amount_tax'] - ($payment + $fee);
                $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
                $record_1['CSJS301'] = 1030;
                $record_1['CSJS313'] = $sales_slip['total_amount_tax'] - ($payment + $fee);
                $record_1['CSJS208'] = $sales_slip['s_code'];
                $record_1['CSJS308'] = $sales_slip['s_code'];
                $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_AD_RECEIVE, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
                // $record_1['condition NO'] = 'condition 7|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];
            }

            $record_2['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_2['CSJS005'] = $this->_get_max_closing_accountant_date();;
            $record_2['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_ORDINARY_DEPOSIT);
            $record_2['CSJS201'] = 1014;
            $record_2['CSJS213'] = $payment;
            $record_2['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_2['CSJS301'] = 1030;
            $record_2['CSJS313'] = $payment;
            $record_2['CSJS208'] = $sales_slip['s_code'];
            $record_2['CSJS308'] = $sales_slip['s_code'];
            $record_2['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_2['condition NO'] = 'condition 7|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            $record_3['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_3['CSJS005'] = $this->_get_max_closing_accountant_date();;
            $record_3['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_COMMISSION_PAID);
            $record_3['CSJS201'] = 7212;
            $record_3['CSJS206'] = 2;
            $record_3['CSJS213'] = $fee;
//            $record_3['CSJS214'] = f_ceil($fee - ($fee / (1 + $this->_get_tax_rate())));
            $record_3['CSJS214'] = f_ceil(bcsub($fee, bcdiv($fee, bcadd(1, $this->_get_tax_rate()))));
            $record_3['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_3['CSJS301'] = 1030;
            $record_3['CSJS313'] = $fee;
            $record_3['CSJS208'] = $sales_slip['s_code'];
            $record_3['CSJS308'] = $sales_slip['s_code'];
            $record_3['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_COMMISSION_PAID, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_3['condition NO'] = 'condition 7|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            if (isset($record_1)) return array($record_1, $record_2, $record_3);
            return array($record_2, $record_3);
        }

        if ($prev_payment_entry['bugyo_next_month_ad_receive'] > 0 && $payment > 0 && $fee == 0) {
            if (($sales_slip['total_amount_tax'] - ($payment + $fee)) > 0) {
                $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
                $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();;
                $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
                $record_1['CSJS201'] = 3017;
                $record_1['CSJS213'] = $sales_slip['total_amount_tax'] - ($payment + $fee);
                $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
                $record_1['CSJS301'] = 1030;
                $record_1['CSJS313'] = $sales_slip['total_amount_tax'] - ($payment + $fee);
                $record_1['CSJS208'] = $sales_slip['s_code'];
                $record_1['CSJS308'] = $sales_slip['s_code'];
                $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_AD_RECEIVE, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
                // $record_1['condition NO'] = 'condition 7|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];
            }

            $record_2['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_2['CSJS005'] = $this->_get_max_closing_accountant_date();;
            $record_2['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_ORDINARY_DEPOSIT);
            $record_2['CSJS201'] = 1014;
            $record_2['CSJS213'] = $payment;
            $record_2['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_2['CSJS301'] = 1030;
            $record_2['CSJS313'] = $payment;
            $record_2['CSJS208'] = $sales_slip['s_code'];
            $record_2['CSJS308'] = $sales_slip['s_code'];
            $record_2['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_2['condition NO'] = 'condition 7|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            if (isset($record_1)) return array($record_1, $record_2);
            return array($record_2);
        }

        if ($prev_payment_entry['bugyo_next_month_ad_receive'] > 0 && $payment == 0 && $fee > 0) {
            if (($sales_slip['total_amount_tax'] - ($payment + $fee)) > 0) {
                $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
                $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();;
                $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
                $record_1['CSJS201'] = 3017;
                $record_1['CSJS213'] = $sales_slip['total_amount_tax'] - ($payment + $fee);
                $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
                $record_1['CSJS301'] = 1030;
                $record_1['CSJS313'] = $sales_slip['total_amount_tax'] - ($payment + $fee);
                $record_1['CSJS208'] = $sales_slip['s_code'];
                $record_1['CSJS308'] = $sales_slip['s_code'];
                $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_AD_RECEIVE, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
                // $record_1['condition NO'] = 'condition 7|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];
            }

            $record_3['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_3['CSJS005'] = $this->_get_max_closing_accountant_date();;
            $record_3['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_COMMISSION_PAID);
            $record_3['CSJS201'] = 7212;
            $record_3['CSJS206'] = 2;
            $record_3['CSJS213'] = $fee;
//            $record_3['CSJS214'] = f_ceil($fee - ($fee / (1 + $this->_get_tax_rate())));
            $record_3['CSJS214'] = f_ceil(bcsub($fee, bcdiv($fee, bcadd(1, $this->_get_tax_rate()))));
            $record_3['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_3['CSJS301'] = 1030;
            $record_3['CSJS313'] = $fee;
            $record_3['CSJS208'] = $sales_slip['s_code'];
            $record_3['CSJS308'] = $sales_slip['s_code'];
            $record_3['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_COMMISSION_PAID, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_3['condition NO'] = 'condition 7|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            if (isset($record_1)) return array($record_1, $record_3);
            return array($record_3);
        }
        
    }

    /**
     * 前月前受金 ＞ 0 AND 当月計上請求額 ＞ 0 AND 当月計上請求額 ＜ （前月前受金＋当月入金＋手数料）
     * AND 当月計上請求額 ＜ （当月入金＋手数料）
     * @return array
     */
    protected function _condition_8($sales_slip, $payment, $fee, $prev_payment_entry, $summary_month) {
        // 普通預金 ＞ 0 AND 手数料 ＞ 0
        if ($payment > 0 && $fee > 0) {
            $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_ORDINARY_DEPOSIT);
            $record_1['CSJS201'] = 1014;
            $record_1['CSJS213'] = $payment;
            $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_1['CSJS301'] = 1030;
            $record_1['CSJS313'] = $sales_slip['total_amount_tax'];
            $record_1['CSJS208'] = $sales_slip['s_code'];
            $record_1['CSJS308'] = $sales_slip['s_code'];
            $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_1['condition NO'] = 'condition 8|b|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            $record_2['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_2['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_2['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
            $record_2['CSJS301'] = 3017;
            $record_2['CSJS313'] = $payment + $fee - $sales_slip['total_amount_tax'];
            $record_2['CSJS308'] = $sales_slip['s_code'];
            $record_2['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_2['condition NO'] = 'condition 8|b|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            $record_3['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_3['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_3['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_COMMISSION_PAID);
            $record_3['CSJS201'] = 7212;
            $record_3['CSJS206'] = 2;
            $record_3['CSJS213'] = $fee;
//            $record_3['CSJS214'] = f_ceil($fee - ($fee / (1 + $this->_get_tax_rate())));
            $record_3['CSJS214'] = f_ceil(bcsub($fee, bcdiv($fee, bcadd(1, $this->_get_tax_rate()))));
            $record_3['CSJS208'] = $sales_slip['s_code'];
            $record_3['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_COMMISSION_PAID, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_3['condition NO'] = 'condition 8|b|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            return array($record_1, $record_2, $record_3);
        }

        // 普通預金 ＞ 0 AND 手数料 ＝ 0の時
        if ($payment > 0 && $fee == 0) {
            $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_ORDINARY_DEPOSIT);
            $record_1['CSJS201'] = 1014;
            $record_1['CSJS213'] = $payment;
            $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_1['CSJS301'] = 1030;
            $record_1['CSJS313'] = $sales_slip['total_amount_tax'];
            $record_1['CSJS208'] = $sales_slip['s_code'];
            $record_1['CSJS308'] = $sales_slip['s_code'];
            $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_1['condition NO'] = 'condition 8|c|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            $record_2['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_2['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_2['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
            $record_2['CSJS301'] = 3017;
            $record_2['CSJS313'] = $payment + $fee - $sales_slip['total_amount_tax'];
            $record_2['CSJS308'] = $sales_slip['s_code'];
            $record_2['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_ORDINARY_DEPOSIT, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_2['condition NO'] = 'condition 8|c|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            return array($record_1, $record_2);
        }

        // 普通預金 ＝ 0 AND 手数料 ＞ 0の時
        if ($payment == 0 && $fee > 0) {
            $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_COMMISSION_PAID);
            $record_1['CSJS201'] = 7212;
            $record_1['CSJS206'] = 2;
            $record_1['CSJS213'] = $fee;
//            $record_1['CSJS214'] = f_ceil($fee - ($fee / (1 + $this->_get_tax_rate())));
            $record_1['CSJS214'] = f_ceil(bcsub($fee, bcdiv($fee, bcadd(1, $this->_get_tax_rate()))));
            $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
            $record_1['CSJS301'] = 1030;
            $record_1['CSJS313'] = $sales_slip['total_amount_tax'];
            $record_1['CSJS208'] = $sales_slip['s_code'];
            $record_1['CSJS308'] = $sales_slip['s_code'];
            $record_1['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_COMMISSION_PAID, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_1['condition NO'] = 'condition 8|d|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            $record_2['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
            $record_2['CSJS005'] = $this->_get_max_closing_accountant_date();
            $record_2['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
            $record_2['CSJS301'] = 3017;
            $record_2['CSJS313'] = $payment + $fee - $sales_slip['total_amount_tax'];
            $record_2['CSJS308'] = $sales_slip['s_code'];
            $record_2['CSJS100'] = $this->_get_field_CSJS100_data(BUGYO_COMMISSION_PAID, $sales_slip['s_code'], $sales_slip['client_name'], date('m', strtotime($summary_month)));
            // $record_2['condition NO'] = 'condition 8|d|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

            return array($record_1, $record_2);
        }
    }

    /**
     * 前月前受金 ＞ 0 AND 当月入金 ＝ 0 AND 手数料 ＝ 0 AND 当月計上請求額 ＞ 0 AND 当月計上請求額 ＜ 前月前受金
     * @return array
     */
    protected function _condition_9($sales_slip, $payment, $fee, $prev_payment_entry) {
        $record_1['OBCD001'] = $this->_add_asterisk($sales_slip['client_id']);
        $record_1['CSJS005'] = $this->_get_max_closing_accountant_date();
        $record_1['CSJS200'] = $this->_get_bugyo_by_name(BUGYO_AD_RECEIVE);
        $record_1['CSJS201'] = 3017;
        $record_1['CSJS213'] = $sales_slip['total_amount_tax'];
        $record_1['CSJS300'] = $this->_get_bugyo_by_name(BUGYO_ACCOUNT_RECEIVABLE);
        $record_1['CSJS301'] = 1030;
        $record_1['CSJS313'] = $sales_slip['total_amount_tax'];
        $record_1['CSJS208'] = $sales_slip['s_code'];
        $record_1['CSJS308'] = $sales_slip['s_code'];
        // $record_1['condition NO'] = 'condition 9|client_id: '.$sales_slip['client_id'].'|sales_slip_id: '.$sales_slip['id'];

        return array($record_1);
    }


    protected function _sort_by_sales_slip_number($a, $b) {
        if ($a['sales_slip_number'] == $b['sales_slip_number']) {
            if ($a['is_ad_receive_payment'] == $b['is_ad_receive_payment']) return 0;
            return $a['is_ad_receive_payment'] > $b['is_ad_receive_payment'] ? -1 : 1;
        }
        return ($a['sales_slip_number'] < $b['sales_slip_number']) ? -1 : 1;
    }

    public function export_csv($summary_month, $s_code, $remaining_srub) {
        $this->load->helper('zenrin_system_helper');

        $summary_month .= '/01';
        $export_datas = array();
        $sales_slips = $this->_get_sales_slip_by_delivery_date($summary_month, $s_code, $remaining_srub);

        $list_client_prev_payment_entry = $this->_get_all_client_payment_entry($this->_decrease_month($summary_month), $s_code);
        $list_client_payment            = $this->_get_all_client_payment($summary_month, $s_code);
        $list_client_fee                = $this->_get_all_client_fee($summary_month, $s_code);

        foreach ($list_client_prev_payment_entry as $entry) {
            $entry['is_prev_payment_entry'] = 1;
            $sales_slips[]                  = $entry;
        }

        foreach ($list_client_payment as $payment) {
            $payment['is_payment'] = 1;
            $sales_slips[]         = $payment;
        }

        foreach ($list_client_fee as $fee) {
            $fee['is_fee']    = 1;
            $sales_slips[]    = $fee;
        }
        
        $unset_key      = array();

        foreach ($sales_slips as $k => $v) {
            for ($i = $k + 1; $i < count($sales_slips); $i++) {
                if ($v['client_id'] == $sales_slips[$i]['client_id']) {
                    if (isset($sales_slips[$i]['is_prev_payment_entry'])) {
                        $sales_slips[$k]['bugyo_next_month_receive']    = $sales_slips[$i]['bugyo_next_month_receive'];
                        $sales_slips[$k]['bugyo_next_month_ad_receive'] = $sales_slips[$i]['bugyo_next_month_ad_receive'];
                        $unset_key[] = $i;
                    } elseif (isset($sales_slips[$i]['is_payment'])) {
                        $sales_slips[$k]['payment'] = $sales_slips[$i]['payment'];
                        $unset_key[] = $i;
                    } elseif (isset($sales_slips[$i]['is_fee'])) {
                        $sales_slips[$k]['fee'] = $sales_slips[$i]['fee'];
                        $unset_key[] = $i;
                    }
                }
            }
        }
        $unset_key = array_unique($unset_key);
        foreach ($unset_key as $key) {
            unset($sales_slips[$key]);
        }
//#7302:Start
        usort($sales_slips, array($this, '_sort_by_s_code_asc'));
//#7302:End

//#7376:Start
        $list_client_ids = array();
        foreach ($sales_slips as $sales_slip) {
            $list_client_ids[]  = $sales_slip['client_id'];            
        }

        $list_client_ids                = array_unique($list_client_ids);
        $clients                        = array();
        $list_client_have_been_exported = array();
        foreach ($sales_slips as $sales_slip) {
            if (!isset($clients[$sales_slip['client_id']])) {
                $clients[$sales_slip['client_id']] = array(
                    'client_id'                   => $sales_slip['client_id'],
                    's_code'                      => $sales_slip['s_code'],
                    'client_name'                 => $sales_slip['client_name'],
                    'bugyo_department_code'       => $sales_slip['bugyo_department_code'],
                    'bugyo_account_code'          => $sales_slip['bugyo_account_code'],
                    'bugyo_abstract_string'       => $sales_slip['bugyo_abstract_string'],
                    'bugyo_next_month_receive'    => isset($sales_slip['bugyo_next_month_receive']) ? $sales_slip['bugyo_next_month_receive'] : 0,
                    'bugyo_next_month_ad_receive' => isset($sales_slip['bugyo_next_month_ad_receive']) ? $sales_slip['bugyo_next_month_ad_receive'] : 0,
                    'payment'                     => isset($sales_slip['payment']) ? $sales_slip['payment'] : 0,
                    'fee'                         => isset($sales_slip['fee']) ? $sales_slip['fee'] : 0,
                );

                $detail = $this->_get_client_sales_slip_details($list_client_ids, $sales_slip['client_id']);
                $clients[$sales_slip['client_id']] += $detail;
            }
        }
//#7376:End

        foreach ($sales_slips as $sales_slip) {
//#7376:Start
//            $payment            = isset($sales_slip['payment']) ? $sales_slip['payment'] : 0;            
//            $fee                = isset($sales_slip['fee']) ? $sales_slip['fee'] : 0;
//            $prev_payment_entry = array(
//                'bugyo_next_month_receive'    => isset($sales_slip['bugyo_next_month_receive']) ? $sales_slip['bugyo_next_month_receive'] : 0,
//                'bugyo_next_month_ad_receive' => isset($sales_slip['bugyo_next_month_ad_receive']) ? $sales_slip['bugyo_next_month_ad_receive'] : 0,
//            );
//            $sales_slip['id'] = isset($sales_slip['id']) ? $sales_slip['id'] : 0;
//#7376:End
            // 当月請求額 ≠ 0
            if ($sales_slip['total_amount_tax'] != 0) {
                $export_datas[] = $this->_condition_2($sales_slip, $summary_month);
            }

//#7376:Start
//            // 前月前受金 ＝ 0 AND （前月売掛金＋当月計上請求額） ≧ （当月入金＋手数料）
//            if ($prev_payment_entry['bugyo_next_month_ad_receive'] == 0 && (($prev_payment_entry['bugyo_next_month_receive'] + $sales_slip['total_amount_tax']) >= ($payment + $fee))) {
//                $export_datas[] = $this->_condition_3($sales_slip, $payment, $fee, $summary_month);
//
//            // 前月前受金 ＝ 0 AND （前月売掛金＋当月計上請求額） ＞ 0 AND （前月売掛金＋当月計上請求額） ＜ （当月入金＋手数料）
//            } elseif ($prev_payment_entry['bugyo_next_month_ad_receive'] == 0 && ($prev_payment_entry['bugyo_next_month_receive'] + $sales_slip['total_amount_tax']) > 0 && ($prev_payment_entry['bugyo_next_month_receive'] + $sales_slip['total_amount_tax']) < ($payment + $fee)) {
//                $export_datas[] = $this->_condition_4($sales_slip, $payment, $fee, $prev_payment_entry, $summary_month);
//
//            // （当月入金＋手数料） ≧ 0 AND （前月売掛金＋当月計上請求額）＝ 0
//            } elseif (($payment + $fee >= 0) && ($prev_payment_entry['bugyo_next_month_receive'] + $sales_slip['total_amount_tax'] == 0)) {
//                $export_datas[] = $this->_condition_5($sales_slip, $payment, $fee, $prev_payment_entry, $summary_month);
//
//            // 前月前受金 ＞ 0 AND （前月売掛金＋当月計上請求額） ≧ （前月前受金＋当月入金＋手数料）
//            } elseif ($prev_payment_entry['bugyo_next_month_ad_receive'] > 0 && (($prev_payment_entry['bugyo_next_month_receive'] + $sales_slip['total_amount_tax']) >= ($prev_payment_entry['bugyo_next_month_ad_receive'] + $payment + $fee))) {
//                // ※前受金、普通預金、支払手数料は、それぞれ金額＞0の時、レコード発生させる
//                $export_datas[] = $this->_condition_6($sales_slip, $payment, $fee, $prev_payment_entry, $summary_month);
//
//            // 前月前受金 ＞ 0 AND 当月計上請求額 ＜ （前月前受金＋当月入金＋手数料） AND 当月計上請求額 ≧ （当月入金＋手数料）
//            } elseif ($prev_payment_entry['bugyo_next_month_ad_receive'] > 0 && ($sales_slip['total_amount_tax'] < ($prev_payment_entry['bugyo_next_month_ad_receive'] + $payment + $fee)) && ($sales_slip['total_amount_tax'] >= ($payment + $fee))) {
//                // ※前受金、普通預金、支払手数料は、それぞれ金額＞0の時、レコード発生させる
//                // 前受金 = 当月計上請求額−（当月入金＋手数料）但し、計算結果が0以下になる時は出力しない
//                $export_datas[] = $this->_condition_7($sales_slip, $payment, $fee, $prev_payment_entry, $summary_month);
//
//            // 前月前受金 ＞ 0 AND 当月計上請求額 ＞ 0 AND 当月計上請求額 ＜ （前月前受金＋当月入金＋手数料） AND 当月計上請求額 ＜ （当月入金＋手数料）  
//            } elseif ($prev_payment_entry['bugyo_next_month_ad_receive'] > 0 && $sales_slip['total_amount_tax'] > 0 && $sales_slip['total_amount_tax'] < ($prev_payment_entry['bugyo_next_month_ad_receive'] + $payment + $fee) && $sales_slip['total_amount_tax'] < ($payment + $fee)) {
//                $export_datas[] = $this->_condition_8($sales_slip, $payment, $fee, $prev_payment_entry, $summary_month);
//
//
//            // 前月前受金 ＞ 0 AND 当月入金 ＝ 0 AND 手数料 ＝ 0 AND 当月計上請求額 ＞ 0 AND 当月計上請求額 ＜ 前月前受金  
//            } elseif ($prev_payment_entry['bugyo_next_month_ad_receive'] > 0 && $payment == 0 && $fee == 0 && $sales_slip['total_amount_tax'] > 0 && $sales_slip['total_amount_tax'] < $prev_payment_entry['bugyo_next_month_ad_receive']) {
//                // $export_datas[] = $this->_condition_9($sales_slip, $payment, $fee, $prev_payment_entry);
//            }
            if (!in_array($sales_slip['client_id'], $list_client_have_been_exported)) {
                $list_client_have_been_exported[] = $sales_slip['client_id'];
                $client                           = $clients[$sales_slip['client_id']];
//#7376:Start
                $payment            = isset($client['payment']) ? $client['payment'] : 0;            
                $fee                = isset($client['fee']) ? $client['fee'] : 0;
                $prev_payment_entry = array(
                    'bugyo_next_month_receive'    => isset($client['bugyo_next_month_receive']) ? $client['bugyo_next_month_receive'] : 0,
                    'bugyo_next_month_ad_receive' => isset($client['bugyo_next_month_ad_receive']) ? $client['bugyo_next_month_ad_receive'] : 0,
                );
                $client['id'] = isset($client['id']) ? $client['id'] : 0;
//#7376:End
                // 前月前受金 ＝ 0 AND （前月売掛金＋当月計上請求額） ≧ （当月入金＋手数料）
                if ($prev_payment_entry['bugyo_next_month_ad_receive'] == 0 && (($prev_payment_entry['bugyo_next_month_receive'] + $client['total_amount_tax']) >= ($payment + $fee))) {
                    $export_datas[] = $this->_condition_3($client, $payment, $fee, $summary_month);

                // 前月前受金 ＝ 0 AND （前月売掛金＋当月計上請求額） ＞ 0 AND （前月売掛金＋当月計上請求額） ＜ （当月入金＋手数料）
                } elseif ($prev_payment_entry['bugyo_next_month_ad_receive'] == 0 && ($prev_payment_entry['bugyo_next_month_receive'] + $client['total_amount_tax']) > 0 && ($prev_payment_entry['bugyo_next_month_receive'] + $client['total_amount_tax']) < ($payment + $fee)) {
                    $export_datas[] = $this->_condition_4($client, $payment, $fee, $prev_payment_entry, $summary_month);

                // （当月入金＋手数料） ≧ 0 AND （前月売掛金＋当月計上請求額）＝ 0
                } elseif (($payment + $fee >= 0) && ($prev_payment_entry['bugyo_next_month_receive'] + $client['total_amount_tax'] == 0)) {
                    $export_datas[] = $this->_condition_5($client, $payment, $fee, $prev_payment_entry, $summary_month);

                // 前月前受金 ＞ 0 AND （前月売掛金＋当月計上請求額） ≧ （前月前受金＋当月入金＋手数料）
                } elseif ($prev_payment_entry['bugyo_next_month_ad_receive'] > 0 && (($prev_payment_entry['bugyo_next_month_receive'] + $client['total_amount_tax']) >= ($prev_payment_entry['bugyo_next_month_ad_receive'] + $payment + $fee))) {
                    // ※前受金、普通預金、支払手数料は、それぞれ金額＞0の時、レコード発生させる
                    $export_datas[] = $this->_condition_6($client, $payment, $fee, $prev_payment_entry, $summary_month);

                // 前月前受金 ＞ 0 AND 当月計上請求額 ＜ （前月前受金＋当月入金＋手数料） AND 当月計上請求額 ≧ （当月入金＋手数料）
                } elseif ($prev_payment_entry['bugyo_next_month_ad_receive'] > 0 && ($client['total_amount_tax'] < ($prev_payment_entry['bugyo_next_month_ad_receive'] + $payment + $fee)) && ($client['total_amount_tax'] >= ($payment + $fee))) {
                    // ※前受金、普通預金、支払手数料は、それぞれ金額＞0の時、レコード発生させる
                    // 前受金 = 当月計上請求額−（当月入金＋手数料）但し、計算結果が0以下になる時は出力しない
                    $export_datas[] = $this->_condition_7($client, $payment, $fee, $prev_payment_entry, $summary_month);

                // 前月前受金 ＞ 0 AND 当月計上請求額 ＞ 0 AND 当月計上請求額 ＜ （前月前受金＋当月入金＋手数料） AND 当月計上請求額 ＜ （当月入金＋手数料）  
                } elseif ($prev_payment_entry['bugyo_next_month_ad_receive'] > 0 && $client['total_amount_tax'] > 0 && $client['total_amount_tax'] < ($prev_payment_entry['bugyo_next_month_ad_receive'] + $payment + $fee) && $client['total_amount_tax'] < ($payment + $fee)) {
                    $export_datas[] = $this->_condition_8($client, $payment, $fee, $prev_payment_entry, $summary_month);


                // 前月前受金 ＞ 0 AND 当月入金 ＝ 0 AND 手数料 ＝ 0 AND 当月計上請求額 ＞ 0 AND 当月計上請求額 ＜ 前月前受金  
                } elseif ($prev_payment_entry['bugyo_next_month_ad_receive'] > 0 && $payment == 0 && $fee == 0 && $client['total_amount_tax'] > 0 && $client['total_amount_tax'] < $prev_payment_entry['bugyo_next_month_ad_receive']) {
                    // $export_datas[] = $this->_condition_9($sales_slip, $payment, $fee, $prev_payment_entry);
                }
            }
        }
//#7376:End
        
        $headers    = array('OBCD001','CSJS001','CSJS002','CSJS003','CSJS004','CSJS005','CSJS006','CSJS007','CSJS009','CSJS200','CSJS201','CSJS202','CSJS203','CSJS204','CSJS220','CSJS205','CSJS206','CSJS207','CSJS208','CSJS213','CSJS214','CSJS216','CSJS217','CSJS218','CSJS219','CSJS300','CSJS301','CSJS302','CSJS303','CSJS304','CSJS320','CSJS305','CSJS306','CSJS307','CSJS308','CSJS313','CSJS314','CSJS316','CSJS317','CSJS318','CSJS319','CSJS100','CSJS101','CSJS102');
        $csv_header = implode(',', $headers) . "\r\n";
        $csv_body   = '';

        foreach ($export_datas as $datas) {
            if (!is_array($datas)) continue;
            foreach ($datas as $data) {
                foreach ($headers as $header) {
                    if (isset($data[$header])) {
                        $csv_body .= $data[$header] . ',';
                    } else {
                        $csv_body .= ',';
                    }
                }
                $csv_body = rtrim($csv_body, ',');
                $csv_body .= "\r\n";
            }
        }
        $csv = $csv_header . $csv_body;
        return $csv;
    }

    protected function _get_all_client_payment($summary_month, $s_code) {
//#7302:Start
//        $delimiter = strpos('/', $summary_month) ? '/' : '-';
        $delimiter = strpos($summary_month, '/') ? '/' : '-';
//#7302:End
        $parts     = explode($delimiter, $summary_month);
        $year      = $parts[0];
        $month     = $parts[1];

        $this->db->select('
                      p.client_id
                    , sum(p.amount) as payment
                    , c.name as client_name
                    , c.s_code
                    , 0 as total_tax
                    , 0 as total_amount_tax
                    , 0 as bugyo_department_code
                    , 0 as bugyo_account_code
                    , 0 as bugyo_abstract_string')
                ->from('payment p')
                ->join('client c', 'c.id = p.client_id AND c.disable = 0')
                ->where('YEAR(p.payment_date)', $year)
                ->where('MONTH(p.payment_date)', $month)
                ->where('p.disable', 0)
                ->group_by('p.client_id');

        if (!empty($s_code)) {
            $this->db->like('c.s_code', $s_code, 'after');
        }

        return $this->db->get()->result_array();
    }

    protected function _get_all_client_payment_entry($summary_month, $s_code) {
        $delimiter = strpos($summary_month, '/') !== false ? '/' : '-';
        $parts     = explode($delimiter, $summary_month);
        $year      = $parts[0];
        $month     = $parts[1];
        
        $this->db->select('
                          pe.client_id
                        , pe.bugyo_next_month_receive
                        , pe.bugyo_next_month_ad_receive
                        , c.name as client_name
                        , c.s_code
                        , 0 as total_tax
                        , 0 as total_amount_tax
                        , 0 as bugyo_department_code
                        , 0 as bugyo_account_code
                        , 0 as bugyo_abstract_string')
                ->from('payment_entry pe')
                ->join('client c', 'c.id = pe.client_id AND c.disable = 0')
                ->where('YEAR(pe.entry_date)', $year)
                ->where('MONTH(pe.entry_date)', $month)
                ->where('pe.disable', 0);

        if (!empty($s_code)) {
            $this->db->like('c.s_code', $s_code, 'after');
        }

        $payment_entries = $this->db->get()->result_array();

        $past_datas = $this->_get_all_unchecked_payment_past_data($s_code);
        
        $client_past_data_arr = array();
        foreach ($past_datas as $data) {
            $client_past_data_arr[] = array(
                'client_id'                   => $data['client_id'],
                'client_name'                 => $data['client_name'],
                's_code'                      => $data['s_code'],
                'total_tax'                   => $data['total_tax'],
                'total_amount_tax'            => $data['total_amount_tax'],
                'bugyo_department_code'       => $data['bugyo_department_code'],
                'bugyo_account_code'          => $data['bugyo_account_code'],
                'bugyo_abstract_string'       => $data['bugyo_abstract_string'],
                'bugyo_next_month_receive'    => $data['is_ad_receive_payment'] == 0 ? $data['amount'] : 0,
                'bugyo_next_month_ad_receive' => $data['is_ad_receive_payment'] == 1 ? $data['amount'] : 0,
            );
        }

        $list_payment_entry = $this->_add_array($payment_entries, $client_past_data_arr);
        

        return $list_payment_entry;
    }

    protected function _add_array($arr1, $arr2) {
        if (!$arr1 || !$arr2) {
            return array_merge($arr1, $arr2);
        }

        $arr = $arr1 + $arr2;

        foreach ($arr as $k => $v) {
            for ($i = $k + 1; $i < count($arr) - 1; $i++) {
                if ($v['client_id'] == $arr[$i]['client_id']) {
                    $arr[$k]['bugyo_next_month_receive']    += $arr[$i]['bugyo_next_month_receive'];
                    $arr[$k]['bugyo_next_month_ad_receive'] += $arr[$i]['bugyo_next_month_ad_receive'];
                    unset($arr[$i]);
                }
            }
        }
        
        return $arr;
    }

    protected function _get_all_client_fee($summary_month, $s_code) {
        $delimiter = strpos($summary_month, '/') !== false ? '/' : '-';
        $parts     = explode($delimiter, $summary_month);
        $year      = $parts[0];
        $month     = $parts[1];

        $this->db->select('
                          pe.client_id
                        , pe.fee
                        , c.name as client_name
                        , c.s_code
                        , 0 as total_tax
                        , 0 as total_amount_tax
                        , 0 as bugyo_department_code
                        , 0 as bugyo_account_code
                        , 0 as bugyo_abstract_string')
                ->from('payment_entry pe')
                ->join('client c', 'c.id = pe.client_id AND c.disable = 0')
                ->where('YEAR(pe.entry_date)', $year)
                ->where('MONTH(pe.entry_date)', $month)
                ->where('pe.disable', 0);

        if (!empty($s_code)) {
            $this->db->like('c.s_code', $s_code, 'after');
        }

        return $this->db->get()->result_array();
    }

    protected function _get_all_unchecked_payment_past_data($s_code) {
        $this->db->select('
                          ppd.*
                        , c.name as client_name
                        , c.s_code
                        , 0 as total_tax
                        , 0 as total_amount_tax
                        , 0 as bugyo_department_code
                        , 0 as bugyo_account_code
                        , 0 as bugyo_abstract_string')
                ->from('payment_past_data ppd')
                ->join('client c', 'c.id = ppd.client_id AND c.disable = 0')
                ->where('ppd.is_checked', 0)
                ->where('ppd.disable', 0)
                ->order_by('ppd.client_id');

        if (!empty($s_code)) {
            $this->db->like('c.s_code', $s_code, 'after');
        }
        
        return $this->db->get()->result_array();
    }

    protected function _get_sales_slip_by_delivery_date($summary_month, $s_code, $remaining_srub) {
        $s_code        = trim($s_code);
        $summary_month = explode('/', $summary_month);
        $year          = $summary_month[0];
        $month         = $summary_month[1];

//#7376:Start
//        $this->db->select('
//                      ss.id
//                    , ss.client_id
//                    , ss.client_name
//                    , ss.total_tax
//                    , ss.total_amount_tax
//                    , bu.bugyo_department_code
//                    , bu.bugyo_account_code
//                    , bu.bugyo_abstract_string
//                    , c.s_code')
//                ->from('sales_slip ss')
//                ->join('sales_slip_detail ssd', 'ss.id = ssd.sales_slip_id AND ssd.disable = 0 AND YEAR(ssd.delivery_date) = '.$year.' AND MONTH(ssd.delivery_date) = ' . $month, 'inner', false)
//                ->join('order o', 'o.j_code = ssd.j_code AND o.disable = 0')
//                ->join('account a', 'a.id = o.j_account_id AND a.disable = 0')
//                ->join('department d', 'd.id = a.department_id AND d.disable = 0')
//                ->join('business_unit bu', 'bu.id = d.business_unit_id AND bu.disable = 0')
//                ->join('client c', 'c.id = ss.client_id AND c.disable = 0')
//                ->where('ss.sales_slip_status', $this->_invoice_status)
//                ->where('ss.disable', 0)
//                ->group_by('ss.id')
//                ->order_by('c.s_code', 'DESC');

        $this->db->select('
                      DISTINCT(ss.sales_slip_number)
                    , ss.id
                    , ss.client_id
                    , ss.client_name
                    , ss.total_tax
                    , ss.total_amount_tax
                    , bu.bugyo_department_code
                    , bu.bugyo_account_code
                    , bu.bugyo_abstract_string
                    , c.s_code')
                ->from('sales_slip ss')
                ->join('sales_slip_detail ssd', 'ss.id = ssd.sales_slip_id AND ssd.disable = 0')
                ->join('order o', 'o.j_code = ssd.j_code AND o.disable = 0')
                ->join('account a', 'a.id = o.j_account_id AND a.disable = 0')
                ->join('department d', 'd.id = a.department_id AND d.disable = 0')
                ->join('business_unit bu', 'bu.id = d.business_unit_id AND bu.disable = 0')
                ->join('client c', 'c.id = ss.client_id AND c.disable = 0')
                ->where('ss.sales_slip_status', $this->_invoice_status)
                ->where('EXTRACT(YEAR_MONTH FROM ssd.delivery_date) = ', $year . $month)
                ->where('ss.disable', 0)
                ->order_by('c.s_code', 'DESC');
//#7376:End

        if (!empty($s_code)) {
            $this->db->like('c.s_code', $s_code, 'after');
        }

        if ($remaining_srub) {
            $this->db->select('pe.*')
                    ->join('payment_entry pe', 'pe.client_id = ss.client_id AND YEAR(pe.entry_date) = '.$year.' AND MONTH(pe.entry_date) = '.$month, 'left', false)
                    ->where('pe.bugyo_next_month_receive !=', 0)
                    ->where('pe.bugyo_next_month_ad_receive !=', 0);
        }
 
 //#7376:Start       
        $query = clone $this->db;
        $datas_non_ad_receive = $this->db->where('ssd.is_ad_receive_payment', 0)
                                        ->get()->result_array();
        $datas_ad_receive     = $query->where('ssd.is_ad_receive_payment', 1)
                                    ->get()->result_array();
        unset($query);

        $result = array();
        foreach ($datas_non_ad_receive as $non_receive) {
            $sales_slip_number = $non_receive['sales_slip_number'];
            if (!isset($result[$sales_slip_number])) {
                $result[$sales_slip_number] = array(
                    'sales_slip_number'     => $non_receive['sales_slip_number'],
                    'id'                    => $non_receive['id'],
                    'client_id'             => $non_receive['client_id'],
                    'client_name'           => $non_receive['client_name'],
                    'total_tax'             => $non_receive['total_tax'],
                    'total_amount_tax'      => $non_receive['total_amount_tax'],
                    'bugyo_department_code' => $non_receive['bugyo_department_code'],
                    'bugyo_account_code'    => $non_receive['bugyo_account_code'],
                    'bugyo_abstract_string' => $non_receive['bugyo_abstract_string'],
                    's_code'                => $non_receive['s_code'],
                );
            } else {
                $result[$sales_slip_number]['total_tax']        += $non_receive['total_tax'];
                $result[$sales_slip_number]['total_amount_tax'] += $non_receive['total_amount_tax'];
            }
        }

        foreach ($datas_ad_receive as $ad_receive) {
            $sales_slip_number = $ad_receive['sales_slip_number'];
            if (!isset($result[$sales_slip_number])) {
                $result[$sales_slip_number] = array(
                    'sales_slip_number'     => $non_receive['sales_slip_number'],
                    'id'                    => $non_receive['id'],
                    'client_id'             => $non_receive['client_id'],
                    'client_name'           => $non_receive['client_name'],
                    'total_tax'             => $non_receive['total_tax'],
                    'total_amount_tax'      => $non_receive['total_amount_tax'],
                    'bugyo_department_code' => $non_receive['bugyo_department_code'],
                    'bugyo_account_code'    => $non_receive['bugyo_account_code'],
                    'bugyo_abstract_string' => $non_receive['bugyo_abstract_string'],
                    's_code'                => $non_receive['s_code'],
                );
            } else {
                $result[$sales_slip_number]['total_tax']        += $ad_receive['total_tax'];
                $result[$sales_slip_number]['total_amount_tax'] += $ad_receive['total_amount_tax'];
            }
        }
//        return $this->db->get()->result_array();
        return array_values($result);
//#7376:End
    }

    protected function _get_detail_by_sales_slip_id($sales_slip_id) {
        return $this->db->select('*')
                        ->from('sales_slip_detail ssd')
                        ->where('ssd.sales_slip_id', $sales_slip_id)
                        ->where('ssd.disable', 0)
                        ->get()->result_array();
    }

    protected function _get_bugyo() {
        return $this->db->select('*')
                        ->from('bugyo_cd_manage bcm')
                        ->where('bcm.disable', 0)
                        ->get()->result_array();
    }

    protected function _get_bugyo_by_name($name) {
        if (!$this->_list_bugyo) {
            $this->_list_bugyo = $this->_get_bugyo();
        }
        foreach ($this->_list_bugyo as $bugyo) {
            if ($bugyo['column_name'] == $name) return $bugyo['bugyo_dp_code'];
        }
        return '';
    }

    protected function _get_tax_rate() {
        if ($this->_tax_rate) return $this->_tax_rate;
        $this->load->model('consumption_tax_model');

//#7376:Start
//        $data = $this->consumption_tax_model->get_rate();
//        $this->_tax_rate = $data['rate'] / 100;
        $data = $this->consumption_tax_model->get_tax_rate_by_date($this->_get_max_closing_accountant_date());
        $this->_tax_rate = $data / 100;
//#7376:End
        return $this->_tax_rate;
    }

    protected function _get_field_CSJS100_data($bugyo_name, $s_code, $client_name, $month, $is_business = false) {
        $client_name = mb_convert_encoding($client_name,'SJIS','UTF-8');
        if ($is_business == true) {
            $bugyo_name  = mb_convert_encoding($bugyo_name,'SJIS','UTF-8');
            return $month . mb_convert_encoding('月','SJIS','UTF-8') . $bugyo_name . '_' . $s_code . '_' . $client_name;
        }

        if (!$this->_list_bugyo) {
            $this->_list_bugyo = $this->_get_bugyo();
        }

        $remark = '';
        foreach ($this->_list_bugyo as $bugyo) {
            if ($bugyo['column_name'] == $bugyo_name) $remark = mb_convert_encoding($bugyo['remarks'],'SJIS','UTF-8');
        }
        
        return $month . mb_convert_encoding('月','SJIS','UTF-8') . $remark . '_' . $s_code . '_' . $client_name;
    }

    /**
     * check a client have blance or not
     * @param  int $client_id
     * @param  string $month_entry
     * @param  string $year_entry
     * @return boolean
     */
    public function check_client_can_balance($client_id,$month_entry,$year_entry) {
        $summary_month = $year_entry .'/'.$month_entry .'/' . '01';
//#9533:Start
        $this->_set_summary_month_filter($summary_month);
//#9533:End
        $prev_ss = $this->_get_prev_month_sales_slip($client_id, $this->_decrease_month($summary_month));
        $cur_ss  = $this->_get_cur_month_sales_slip($client_id, $summary_month);
        $next_ss = $this->_get_next_month_sales_slip($client_id, $this->_increase_month($summary_month));
//#9155:Start
        if (!$prev_ss && !$cur_ss && !$next_ss) return true;
//#9155:End
        foreach ($prev_ss as $sales_slip) {
            if ($sales_slip['default_is_checked'] == 0) return true;
        }

        foreach ($cur_ss as $sales_slip) {
            if ($sales_slip['default_is_checked'] == 0) return true;
        }

        foreach ($next_ss as $sales_slip) {
            if ($sales_slip['default_is_checked'] == 0) return true;
        }

        return false;
    }

//#9547:Start
//    public function check_can_devide_money($client_id, $devide_money) {
    public function validate_adjust_money($client_id, $adjust_money) {
//#9547:End
        $summary_month                  = $this->_get_max_closing_accountant_date();
//#9533:Start
        $this->_set_summary_month_filter($summary_month);
//#9533:End
        $client_payment                 = $this->_get_client_payment_amount($client_id, $summary_month);
//        $payment_after_devide           = $client_payment + $devide_money;
        $payment_after_adjust           = $client_payment + $adjust_money;
        $prev_ad_receive_payment_amount = $this->_get_prev_ad_receive_payment_amount($client_id, $summary_month);
        $payment_entry                  = $this->_get_client_payment_entry($client_id, $summary_month);
        $prev_payment_entry             = $this->_get_client_payment_entry($client_id, $this->_decrease_month($summary_month), true);
//#9547:Start
//        $payment_entry_data_result = $this->_cal_payment_entry_data($client_id, $prev_ad_receive_payment_amount, $payment_after_devide, $payment_entry['etc'], $payment_entry['fee'], $payment_entry['repayment'], $payment_entry['miscellaneous_loss'], $payment_entry['miscellaneous_income'], $prev_payment_entry['bugyo_next_month_receive'], $prev_payment_entry['bugyo_next_month_ad_receive']);
        $payment_entry_data_result = $this->_cal_payment_entry_data($client_id, $prev_ad_receive_payment_amount, $payment_after_adjust, $payment_entry['etc'], $payment_entry['fee'], $payment_entry['repayment'], $payment_entry['miscellaneous_loss'], $payment_entry['miscellaneous_income'], $prev_payment_entry['bugyo_next_month_receive'], $prev_payment_entry['bugyo_next_month_ad_receive']);
        $payment_entry_data_result['payment'] = $payment_after_adjust;
//#9167:Start
//        return $payment_entry_data_result['offset_next_month_ad_receive'] > 0;
//        return $payment_entry_data_result['offset_next_month_ad_receive'] >= 0;
//#9167:End
        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->rules('update'));
        $this->form_validation->set_data($payment_entry_data_result);
        $this->form_validation->run();

        $validate_result = $this->form_validation->error_array();
        // override validate messages
        // if 4 fields below exist, then just show 1 error message for 4 fields
        if (isset($validate_result['offset_next_month_receive']) || isset($validate_result['offset_next_month_ad_receive']) || isset($validate_result['bugyo_next_month_receive']) || isset($validate_result['bugyo_next_month_ad_receive'])) {
            unset($validate_result['offset_next_month_receive']);
            unset($validate_result['offset_next_month_ad_receive']);
            unset($validate_result['bugyo_next_month_receive']);
            unset($validate_result['bugyo_next_month_ad_receive']);
            $validate_result['field'] = lang('error_system_limit_range');
        }
        return $validate_result;
    }
//#9547:End


    protected function _cal_payment_entry_data($client_id, $prev_ad_receive_payment_amount, $payment_amount, $etc, $fee, $repayment, $miscellaneous_loss, $miscellaneous_income, $prev_bugyo_next_month_receive, $prev_bugyo_next_month_ad_receive) {
        $summary_month = $this->_get_max_closing_accountant_date();
        $prev_ss       = $this->_get_prev_month_sales_slip($client_id, $this->_decrease_month($summary_month));
        $cur_ss        = $this->_get_cur_month_sales_slip($client_id, $summary_month);
        $next_ss       = $this->_get_next_month_sales_slip($client_id, $this->_increase_month($summary_month));
 //#9218:Start
        $sales_slips_delivery_date_amount = $this->_get_client_sales_slips_delivery_date_amount($client_id);
 //#9218:End
    
        $offset_next_month_receive    = 0;
        $offset_next_month_ad_receive = 0;
        $bugyo_next_month_receive     = 0;
        $bugyo_next_month_ad_receive  = 0;
        
        $offset_next_month_ad_receive += $prev_ad_receive_payment_amount + $payment_amount + $etc + $fee - $repayment + $miscellaneous_loss - $miscellaneous_income;
//#9218:Start
//        $bugyo_next_month_receive     += $prev_bugyo_next_month_receive - ($payment_amount + $etc + $fee - $repayment + $miscellaneous_loss - $miscellaneous_income + $prev_bugyo_next_month_ad_receive);
//        $bugyo_next_month_ad_receive  += $prev_bugyo_next_month_ad_receive + $payment_amount + $etc + $fee - $repayment + $miscellaneous_loss - $miscellaneous_income - $prev_bugyo_next_month_receive;
        $bugyo_next_month_receive     += $prev_bugyo_next_month_receive - ($payment_amount + $etc + $fee - $repayment + $miscellaneous_loss - $miscellaneous_income + $prev_bugyo_next_month_ad_receive) + $sales_slips_delivery_date_amount;
        $bugyo_next_month_ad_receive  += $prev_bugyo_next_month_ad_receive + $payment_amount + $etc + $fee - $repayment + $miscellaneous_loss - $miscellaneous_income - $prev_bugyo_next_month_receive - $sales_slips_delivery_date_amount;;

//#9218:End
        $year_month              = date('Y-m', strtotime($summary_month));
        $total_sales_slip_amount = 0;

//#8353:Start
//#7371:Start
        foreach ($prev_ss as $sales_slip) {
            if ($sales_slip['default_is_checked']) {
//            if ($sales_slip['default_is_checked'] && $sales_slip['is_ad_receive_payment'] == 0) {
                $offset_next_month_ad_receive -= $sales_slip['total_amount_tax'];
            }

            if (!$sales_slip['default_is_checked']) {
//            if (!$sales_slip['default_is_checked'] && $sales_slip['is_ad_receive_payment'] == 0) {
                $offset_next_month_receive += $sales_slip['total_amount_tax'];
            }
//#9218:Start
//            if (isset($sales_slip['delivery_date']) && date('Y-m', strtotime($sales_slip['delivery_date'])) == $year_month) {
////            if (isset($sales_slip['delivery_date']) && date('Y-m', strtotime($sales_slip['delivery_date'])) == $year_month && $sales_slip['is_ad_receive_payment'] == 0) {
//                $bugyo_next_month_receive    += $sales_slip['total_amount_tax'];
//                $bugyo_next_month_ad_receive -= $sales_slip['total_amount_tax'];
//            }
//#9218:End
        }


        foreach ($cur_ss as $sales_slip) {
            if ($sales_slip['default_is_checked']) {
//            if ($sales_slip['default_is_checked'] && $sales_slip['is_ad_receive_payment'] == 0) {
                $offset_next_month_ad_receive -= $sales_slip['total_amount_tax'];
            }

            if (!$sales_slip['default_is_checked']) {
//            if (!$sales_slip['default_is_checked'] && $sales_slip['is_ad_receive_payment'] == 0) {
                $offset_next_month_receive += $sales_slip['total_amount_tax'];
            }
//#9218:Start
//            if (isset($sales_slip['delivery_date']) && date('Y-m', strtotime($sales_slip['delivery_date'])) == $year_month) {
////            if (isset($sales_slip['delivery_date']) && date('Y-m', strtotime($sales_slip['delivery_date'])) == $year_month && $sales_slip['is_ad_receive_payment'] == 0) {
//                $bugyo_next_month_receive    += $sales_slip['total_amount_tax'];
//                $bugyo_next_month_ad_receive -= $sales_slip['total_amount_tax'];
//            }
//#9218:End
        }

        foreach ($next_ss as $sales_slip) {
            if ($sales_slip['default_is_checked']) {
//            if ($sales_slip['default_is_checked'] && $sales_slip['is_ad_receive_payment'] == 0) {
                $offset_next_month_ad_receive -= $sales_slip['total_amount_tax'];
            }

            if (!$sales_slip['default_is_checked']) {
//            if (!$sales_slip['default_is_checked'] && $sales_slip['is_ad_receive_payment'] == 0) {
                $offset_next_month_receive += $sales_slip['total_amount_tax'];
            }
//#9218:Start
//            if (isset($sales_slip['delivery_date']) && date('Y-m', strtotime($sales_slip['delivery_date'])) == $year_month) {
////            if (isset($sales_slip['delivery_date']) && date('Y-m', strtotime($sales_slip['delivery_date'])) == $year_month && $sales_slip['is_ad_receive_payment'] == 0) {
//                $bugyo_next_month_receive    += $sales_slip['total_amount_tax'];
//                $bugyo_next_month_ad_receive -= $sales_slip['total_amount_tax'];
//            }
//#9218:End
        }
//#7371:End
//#8353:End

//#9547:Start
//        return array(
//            'offset_next_month_receive'    => $offset_next_month_receive,
//            'offset_next_month_ad_receive' => $offset_next_month_ad_receive,
//            'bugyo_next_month_receive'     => $bugyo_next_month_receive,
//            'bugyo_next_month_ad_receive'  => $bugyo_next_month_ad_receive,
//        );
        return array(
            'offset_next_month_receive'    => $offset_next_month_receive > 0 ? $offset_next_month_receive : 0,
            'offset_next_month_ad_receive' => $offset_next_month_ad_receive,
            'bugyo_next_month_receive'     => $bugyo_next_month_receive > 0 ? $bugyo_next_month_receive : 0,
            'bugyo_next_month_ad_receive'  => $bugyo_next_month_ad_receive > 0 ? $bugyo_next_month_ad_receive : 0,
        );
//#9547:End
    }

    public function update_payment_entry_after_edit($client_id) {
        $summary_month                  = $this->_get_max_closing_accountant_date();
//#9533:Start
        $this->_set_summary_month_filter($summary_month);
//#9533:End
        $client_payment                 = $this->_get_client_payment_amount($client_id, $summary_month, false);
        $prev_ad_receive_payment_amount = $this->_get_prev_ad_receive_payment_amount($client_id, $summary_month);
        $payment_entry                  = $this->_get_client_payment_entry($client_id, $summary_month);
        $prev_payment_entry             = $this->_get_client_payment_entry($client_id, $this->_decrease_month($summary_month), true);

        $payment_entry_data_result = $this->_cal_payment_entry_data($client_id, $prev_ad_receive_payment_amount, $client_payment, $payment_entry['etc'], $payment_entry['fee'], $payment_entry['repayment'], $payment_entry['miscellaneous_loss'], $payment_entry['miscellaneous_income'], $prev_payment_entry['bugyo_next_month_receive'], $prev_payment_entry['bugyo_next_month_ad_receive']);
        foreach ($payment_entry_data_result as $k => $v) {
            if ($v < 0) {
                $payment_entry_data_result[$k] = 0;
            }
        }
//#9118:Start
        // if there is no payment entry for this client, then create it
        $delimiter     = strpos($summary_month, '/') !== false ? '/' : '-';
        $parts         = explode($delimiter, $summary_month);
        $year          = $parts[0];
        $month         = $parts[1];

        $have_payment_entry = $this->db->where('pe.client_id', $client_id)
                                    ->where('pe.disable', 0)
                                    ->where('YEAR(pe.entry_date)', $year)
                                    ->where('MONTH(pe.entry_date)', $month)
                                    ->from('payment_entry pe')
                                    ->count_all_results();

        if (!$have_payment_entry) {
            $payment_entry_data_result['client_id']            = $client_id;
            $payment_entry_data_result['entry_date']           = $summary_month;
            $payment_entry_data_result['etc']                  = 0;
            $payment_entry_data_result['fee']                  = 0;
            $payment_entry_data_result['repayment']            = 0;
            $payment_entry_data_result['miscellaneous_loss']   = 0;
            $payment_entry_data_result['miscellaneous_income'] = 0;
            $payment_entry_data_result['lastup_account_id']    = $this->auth->get_account_id();
            $payment_entry_data_result['create_datetime']      = $this->_db_now();
            $payment_entry_data_result['lastup_datetime']      = $this->_db_now();
            
            $this->db->insert('payment_entry', $payment_entry_data_result);
        } else {
            $this->db->where('client_id', $client_id);
            $this->db->where('entry_date', $summary_month);
            $this->db->update('payment_entry', $payment_entry_data_result);
        }
//        $this->db->where('client_id', $client_id);
//        $this->db->where('entry_date', $summary_month);
//        $this->db->update('payment_entry', $payment_entry_data_result);
//#9118:End
    }

    protected function _increase_month($summary_month, $increase_month = 1) {
        $date = new DateTime($summary_month);
        $date->modify('first day of +' . $increase_month .' month');
        return $date->format('Y/m/d');
    }

    protected function _decrease_month($summary_month, $decrease_month = 1) {
        $date = new DateTime($summary_month);
        $date->modify('last day of -' . $decrease_month .' month');
        return $date->format('Y/m/t');
    }

    protected function _get_all_closing_accountances() {
        static $_closing_accountances = null;

        if (!$_closing_accountances) {
            $this->load->model('closing_accountant_model');
            $_closing_accountances = $this->closing_accountant_model->get_closing_accountant();
        }
        return $_closing_accountances;
    }

    protected function _get_min_closing_accountance() {
        static $_oldest = null;

        if (!$_oldest) {
            $this->load->model('closing_accountant_model');
            $_oldest = $this->closing_accountant_model->get_min_closed_accountant();
        }
        return $_oldest;
    }

//#7376:Start
    protected function _get_export_csv_sales_slip_details($clients) {
        $summary_month = $this->_get_max_closing_accountant_date();
        $delimiter     = strpos($summary_month, '/') !== false ? '/' : '-';
        $parts         = explode($delimiter, $summary_month);
        $year          = $parts[0];
        $month         = $parts[1];

        // return $this->db->select('
        //                       ss.client_id
        //                     , sum(ss.total_amount) as total_amount
        //                     , sum(ss.total_tax) as total_tax
        //                     , sum(ss.total_amount_tax) as total_amount_tax')
        //                 ->from('sales_slip_detail ssd')
        //                 ->join('sales_slip ss', 'ss.id = ssd.sales_slip_id AND ss.disable = 0 AND ss.sales_slip_status = ' . SALES_STATUS_INVOICE_ISSUED)
        //                 ->where_in('ss.client_id', $clients)
        //                 ->where('MONTH(ssd.delivery_date)', $month)
        //                 ->where('YEAR(ssd.delivery_date)', $year)
        //                 ->where('ssd.disable', 0)
        //                 ->where('ssd.is_ad_receive_payment', 0)
        //                 ->group_by('client_id')
        //                 ->get()->result_array();

        $datas_non_ad_receive = $this->db->select('
                                              DISTINCT(ss.id)
                                            , ss.sales_slip_number
                                            , ss.client_id
                                            , ss.total_amount
                                            , ss.total_tax
                                            , ss.total_amount_tax')
                                        ->from('sales_slip_detail ssd')
                                        ->join('sales_slip ss', 'ss.id = ssd.sales_slip_id AND ss.disable = 0 AND ss.sales_slip_status = ' . $this->_invoice_status)
                                        ->where_in('ss.client_id', $clients)
                                        ->where('MONTH(ssd.delivery_date)', $month)
                                        ->where('YEAR(ssd.delivery_date)', $year)
                                        ->where('ssd.disable', 0)
                                        ->where('ssd.is_ad_receive_payment', 0)
                                        ->get()->result_array();

        $datas_ad_receive = $this->db->select('
                                          DISTINCT(ss.id)
                                        , ss.sales_slip_number
                                        , ss.client_id
                                        , ss.total_amount
                                        , ss.total_tax
                                        , ss.total_amount_tax')
                                    ->from('sales_slip_detail ssd')
                                    ->join('sales_slip ss', 'ss.id = ssd.sales_slip_id AND ss.disable = 0 AND ss.sales_slip_status = ' . $this->_invoice_status)
                                    ->where_in('ss.client_id', $clients)
                                    ->where('MONTH(ssd.delivery_date)', $month)
                                    ->where('YEAR(ssd.delivery_date)', $year)
                                    ->where('ssd.disable', 0)
                                    ->where('ssd.is_ad_receive_payment', 1)
                                    ->get()->result_array();

        $result = array();
        foreach ($datas_non_ad_receive as $non_receive) {
            $client_id = $non_receive['client_id'];
            if (!isset($result[$client_id])) {
                $result[$client_id] = array(
                    'client_id'        => $non_receive['client_id'],
                    'total_amount'     => $non_receive['total_amount'],
                    'total_tax'        => $non_receive['total_tax'],
                    'total_amount_tax' => $non_receive['total_amount_tax'],
                );
            } else {
                $result[$client_id]['total_amount']     += $non_receive['total_amount'];
                $result[$client_id]['total_tax']        += $non_receive['total_tax'];
                $result[$client_id]['total_amount_tax'] += $non_receive['total_amount_tax'];
            }
        }

        foreach ($datas_ad_receive as $ad_receive) {
            $client_id = $ad_receive['client_id'];
            if (!isset($result[$client_id])) {
                $result[$client_id] = array(
                    'client_id'        => $ad_receive['client_id'],
                    'total_amount'     => $ad_receive['total_amount'],
                    'total_tax'        => $ad_receive['total_tax'],
                    'total_amount_tax' => $ad_receive['total_amount_tax'],
                );
            } else {
                $result[$client_id]['total_amount']     += $ad_receive['total_amount'];
                $result[$client_id]['total_tax']        += $ad_receive['total_tax'];
                $result[$client_id]['total_amount_tax'] += $ad_receive['total_amount_tax'];
            }
        }

        return array_values($result);
    }

    protected function _get_client_sales_slip_details($list_client, $search_client_id) {
        static $_export_csv_sales_slip_details = null;

        if (!$_export_csv_sales_slip_details) {
            $_export_csv_sales_slip_details = $this->_get_export_csv_sales_slip_details($list_client);
        }

        foreach ($_export_csv_sales_slip_details as $detail) {
            if ($detail['client_id'] == $search_client_id) {
                return array(
                    'total_amount'     => $detail['total_amount'],
                    'total_tax'        => $detail['total_tax'],
                    'total_amount_tax' => $detail['total_amount_tax'],
                );
            }
        }

        return array(
            'total_amount'     => 0,
            'total_tax'        => 0,
            'total_amount_tax' => 0,
        );

    }

    protected function _get_fixed_sales_slip_detail($sales_slip_id) {
        return $this->db->select('ssd.*')
                        ->from('sales_slip_detail ssd')
                        ->where('ssd.is_ad_receive_payment', 0)
                        ->where('ssd.sales_slip_id', $sales_slip_id)
                        ->where('ssd.disable', 0)
                        ->get()->result_array();
    }
//#7376:End

//#9533:Start
    protected function _get_client_sales_slips_delivery_date_amount($client_id) {
        if (!isset($this->_list_sales_slips_delivery_date_amount[$client_id])) {
            return 0;
        }
        $ss = $this->_list_sales_slips_delivery_date_amount[$client_id];
        $total = 0;

        foreach ($ss as $k => $amount) {
            $total += $amount;
        }
        return $total;
    }

    protected function _set_summary_month_filter($summary_month) {
        $this->_summary_month_filter['date']  = date('Y-m-d', strtotime($summary_month));
        $this->_summary_month_filter['year']  = date('Y', strtotime($summary_month));
        $this->_summary_month_filter['month'] = date('m', strtotime($summary_month));
        $this->_summary_month_filter['day']   = date('d', strtotime($summary_month));
    }
//#9533:End

//#9723:Start
    protected function _compare_summary_month($summary_month_1, $summary_month_2, $operator) {
        $summary_month_1 = strtotime(date('Y-m', strtotime($summary_month_1)));
        $summary_month_2 = strtotime(date('Y-m', strtotime($summary_month_2)));

        switch ($operator) {
            case '==': return $summary_month_1 == $summary_month_2;
            case '!=': return $summary_month_1 != $summary_month_2;
            case '<=': return $summary_month_1 <= $summary_month_2;
            case '>=': return $summary_month_1 >= $summary_month_2;
            case '<':  return $summary_month_1 < $summary_month_2;
            case '>':  return $summary_month_1 > $summary_month_2;
            
            default:   return false;
        }
    }
//#9723:ENd
}