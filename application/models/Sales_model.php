<?php
class Sales_model extends ZR_Model
{
    protected $status_options = null;
    protected $tax_options = null;

    public function status_options($code = null)
    {
        return $this->config_options('sale_slip_invoice_status', $this->status_options, $code);
    }

    public function tax_options($code = null)
    {
        return $this->config_options('tax_division', $this->tax_options, $code);
    }

    protected function config_options($config_name, &$cache_var, $code)
    {
        if ($cache_var === null) {
            $this->load->model('gui_parts_element_model');
            $code_data = $this->gui_parts_element_model->get_by_gui_parts_column_name($config_name, 'code, name');
            $cache_var = map_element($code_data, 'code', 'name');
        }

        if ($code === null) {
            return $cache_var;
        }

        return isset($cache_var[$code]) ? $cache_var[$code] : false;
    }

    public function get_data()
    {
        return $this->db->select('sales_slip_detail.id AS sales_slip_detail_id, sales_slip_number, s_code, client.name, client.charge_name, order.j_code, order_detail.branch_cd')
            ->select('IF(sales_slip_detail.id IS NULL, IF((order_acceptance.delivery_date IS NULL) OR (order_acceptance.delivery_date = \'0000-00-00\'), order_detail.delivery_date, order_acceptance.delivery_date), sales_slip_detail.delivery_date) AS delivery_date', false)
            ->select('IF(sales_slip_detail.id IS NULL, order_detail.product_id, sales_slip_detail.product_id) AS product_id', false)
            ->select('IF(sales_slip_detail.id IS NULL, order_detail.contents, sales_slip_detail.contents) AS contents', false)
            ->select('IF(sales_slip.id IS NULL, ' . SALES_STATUS_NO_INPUT . ', sales_slip.sales_slip_status) AS status', false)
            ->select('IF(sales_slip_detail.id IS NULL, order_detail.quantity, sales_slip_detail.quantity) AS quantity', false)
            ->select('IF(sales_slip_detail.id IS NULL, order_detail.tax_type, sales_slip_detail.tax_type) AS tax_type', false)
            ->select('IF(sales_slip_detail.id IS NULL, order_detail.unit_price, sales_slip_detail.unit_price) AS unit_price', false)
            ->select('IF(sales_slip_detail.id IS NULL, order_detail.amount, sales_slip_detail.amount) AS amount', false)
            ->select('IF(sales_slip_detail.id IS NULL, order.is_ad_receive_payment, sales_slip_detail.is_ad_receive_payment) AS is_ad_receive_payment', false)
            ->select('IF(sales_slip_detail.id IS NULL, 0, sales_slip_detail.ad_receive_sales_slip_detail_id) AS ad_receive_sales_slip_detail_id', false)
            ->select('order_detail.delivery_date AS order_delivery_date, order_detail.billing_date AS order_billing_date, order_detail.payment_date AS order_payment_date')
            ->select('order_acceptance.quantity AS order_quantity, order_acceptance.tax_free AS order_tax_type, order_acceptance.unit_price AS order_unit_price, order_acceptance.amount AS order_amount')
            ->select('order_detail.cost_unit_price, order_detail.cost_total_price')
            ->select('order_acceptance.amount AS acceptance_amount, order.j_account_id AS j_account_id')
            ->join('order_detail', 'order_detail.order_id = order.id')
            ->join('sales_slip_detail', 'sales_slip_detail.j_code = order.j_code AND sales_slip_detail.branch_cd = order_detail.branch_cd', 'left')
            ->join('sales_slip', 'sales_slip.id = sales_slip_detail.sales_slip_id', 'left')
            ->join('order_acceptance', 'order_acceptance.j_code = order.j_code AND order_acceptance.branch_cd = order_detail.branch_cd', 'left')
            ->join ('client', 'client.id = order.client_id')
            ->order_by('j_account_id, s_code, delivery_date, j_code, branch_cd')
//            ->limit(100)
            ->get('order')
            ->result_array();
    }
}