<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sales_slip_detail_model extends ZR_Model {
    public function get_all_by_sales_slip_id($sales_slip_id) {
        return $this->db->select('
                        ssd.id
                        , ssd.j_code
                        , ssd.branch_cd
                        , ssd.contents
                        , ssd.tax_type
                        , ssd.quantity
                        , ssd.unit_price
                        , ssd.amount
                        , ssd.is_ad_receive_payment as sales_slip_is_ad_receive_payment
                        , ssd.ad_receive_sales_slip_detail_id
                        , p.name as product_name
                        , gpe.name as tax_name
                        , oa.order_acceptance_status
                        , max(oa.delivery_date) as order_acceptance_delivery_date
                        , od.delivery_date as order_detail_delivery_date')
                        ->from('sales_slip_detail ssd')
                        ->join('product p', 'p.id = ssd.product_id')
                        ->join('gui_parts_element gpe', 'gpe.code = ssd.tax_type')
                        ->join('sales_slip ss', 'ss.id = ssd.sales_slip_id')
                        ->join('order_acceptance oa', 'oa.j_code = ssd.j_code and oa.branch_cd = ssd.branch_cd', 'left')
                        ->join('order o', 'o.j_code = ssd.j_code')
                        ->join('order_detail od', 'od.order_id = o.id and od.branch_cd = ssd.branch_cd')
                        ->where('ssd.sales_slip_id', $sales_slip_id)
                        ->where('ssd.disable', 0)
                        ->where('gpe.gui_parts_id', GUI_PART_TAX_DIVISION)
                        ->group_by('ssd.id')
                        ->get()
                        ->result_array();
    }
    public function insert_sale_slip_detail($data) {
       return  $this->db->insert('sales_slip_detail', $data);

    }
}