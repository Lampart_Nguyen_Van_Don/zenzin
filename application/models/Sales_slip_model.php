<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sales_slip_model extends ZR_Model {

    protected $status_options = null;
    protected $tax_options = null;

    protected $closing_accountant_date = null;

    public $error = '';

//#6688:Start
    /**
     * Define delivery date limit
     */
    const DELIVERY_DATE_END = '9999/12/31';
//#6688:End

    public function status_options($code = null)
    {
        return $this->config_options('sale_slip_invoice_status', $this->status_options, $code);
    }

    public function tax_options($code = null)
    {
        return $this->config_options('tax_division', $this->tax_options, $code);
    }

    protected function config_options($config_name, &$cache_var, $code)
    {
        if ($cache_var === null) {
            $this->load->model('gui_parts_element_model');
            $code_data = $this->gui_parts_element_model->get_by_gui_parts_column_name($config_name, 'code, name');
            $cache_var = map_element($code_data, 'code', 'name');
        }

        if ($code === null) {
            return $cache_var;
        }

        return isset($cache_var[$code]) ? $cache_var[$code] : false;
    }
    // change date
    public function config_changedate($date){
        $str_search    = array("/", "-", ".");
        $str_replace   = array("", "", "");
        $date = str_replace ($str_search,$str_replace,$date);
        $n_to_day_delvie = strlen($date);
        $year = ""; $moth = ""; $day="";

        $dayss = "";
        if($n_to_day_delvie < 4 ){
            $year = $date;
            $dayss = $year;
        }
        else{
            $year = substr($date,0,4);
            $dayss = $year;
        }

        if($n_to_day_delvie > 4 ){
            $moth =  substr($date,4,2);
            $dayss .= "-".$moth;
        }

        if(($n_to_day_delvie > 6)){
            $day =  substr($date,6,2);
            $dayss .= "-".$day;
        }
        return $dayss;
    }
    public function get_data($return_query = false, $limit_result = 0)
    {
        $query = $this->db->select('ss.id AS sales_slip_id, ssd.id AS sales_slip_detail_id, ss.sales_slip_number, s_code, c.name, c.charge_name, o.charge_name AS order_charge_name, o.j_code, od.branch_cd, c.id AS client_id, o.id AS order_id')
            ->select('ssd.delivery_date AS ssd_delivery_date')
            ->select('
                (SELECT MAX(sub_oa.delivery_date)
                 FROM order_acceptance sub_oa
                 WHERE sub_oa.j_code = o.j_code AND sub_oa.branch_cd = od.branch_cd AND sub_oa.disable = 0
                ) AS acceptance_delivery_date')
            ->select('IF(ssd.id IS NULL, o_product.name, ssd.product_name) AS product_name', false)
            ->select('IF(ssd.id IS NULL, od.contents, ssd.contents) AS contents', false)
            ->select('IF(ss.id IS NULL, IF(ssd.id IS NULL, ' . SALES_STATUS_NO_INPUT . ', ' . SALES_STATUS_UNCREATED . '), ss.sales_slip_status) AS status', false)
            ->select('IF(ssd.id IS NULL, od.quantity, ssd.quantity) AS quantity', false)
            ->select('IF(ssd.id IS NULL, od.tax_type, ssd.tax_type) AS tax_type', false)
            ->select('IF(ssd.id IS NULL, od.unit_price, ssd.unit_price) AS unit_price', false)
            ->select('IF(ssd.id IS NULL, od.amount, ssd.amount) AS amount', false)
            ->select('IF(ssd.id IS NULL, o.is_ad_receive_payment, ssd.is_ad_receive_payment) AS is_ad_receive_payment', false)
            ->select('IF(ssd.id IS NULL, 0, ssd.ad_receive_sales_slip_detail_id) AS ad_receive_sales_slip_detail_id', false)
//#7769:Start
            ->select('IF(ssd.id IS NULL, od.billing_date, ssd.billing_date) AS ssd_billing_date', false)
            ->select('IF(ssd.id IS NULL, od.payment_date, ssd.payment_date) AS ssd_payment_date', false)
            ->select('o.is_ad_receive_payment AS order_is_ad_receive_payment')
//#7769:End
            ->select('od.delivery_date AS order_delivery_date, od.billing_date AS order_billing_date, od.payment_date AS order_payment_date')
            ->select('od.product_id AS order_product_id, od.contents AS order_contents, od.tax_type AS order_tax_type, od.quantity AS order_quantity, od.unit_price AS order_unit_price, od.amount AS order_amount, od.cost_unit_price, od.cost_total_price')
//#6900:Start
            ->select('od.id AS order_detail_id, od.is_change_report_apply')
//#6900:End
            ->select('(
                SELECT SUM(sum_oa.amount)
                FROM order_acceptance sum_oa
                WHERE sum_oa.j_code = o.j_code AND sum_oa.branch_cd = od.branch_cd AND sum_oa.disable = 0
            ) AS sum_acceptance_amount', false)
            ->select('o.j_account_id AS j_account_id')
            ->select('jd.business_unit_id AS jbu_id')
            ->join('client c', 'c.id = o.client_id AND c.disable = 0')
            ->join('account ja', 'o.j_account_id = ja.id')
            ->join('department jd', 'ja.department_id = jd.id')
            ->join('order_detail od', 'od.order_id = o.id AND od.disable = 0')
            ->join('product o_product', 'od.product_id = o_product.id AND o_product.disable = 0')
            ->join('sales_slip_detail ssd', 'ssd.j_code = o.j_code AND ssd.branch_cd = od.branch_cd AND ssd.disable = 0', 'left')
            ->join('sales_slip ss', 'ss.id = ssd.sales_slip_id AND ss.disable = 0', 'left')
            ->group_start()
            ->where('od.order_status', ORDER_STATUS_UNDETERMINED)
            ->or_where('od.order_status', ORDER_STATUS_CONFIRMED)
            ->group_end()
            ->where('o.disable', STATUS_ENABLE)
//#6949:Start
            ->order_by('j_account_id, s_code, EXTRACT(YEAR_MONTH FROM order_delivery_date), j_code, branch_cd, is_ad_receive_payment DESC, status, ssd.id')
//#6949:End
            ->get('order o');

//#7087:Start
        if ($limit_result > 0 && $query->num_rows() > $limit_result) {
            return array(
                'is_over_record' => true
            );
        }
//#7087:End

        if ($return_query == true) {
            return $query;
        } else {
            $result = $query->result_array();
            foreach ($result as &$row) {
                if ($row['acceptance_delivery_date'] && substr($row['acceptance_delivery_date'], 0, 7) == substr($row['order_delivery_date'], 0, 7)) {
                    $row['default_delivery_date'] = $row['acceptance_delivery_date'];
                } else {
                    $row['default_delivery_date'] = $row['order_delivery_date'];
                }

                if (!$row['ssd_delivery_date']) {
                    $row['ssd_delivery_date'] = $row['default_delivery_date'];
                }
            }
            return $result;
        }
    }

    public function filter($params)
    {
        if (isset($params['sales_slip_detail_id'])) {
            if (is_array($params['sales_slip_detail_id'])) {
                $this->db->where_in('ssd.id', $params['sales_slip_detail_id']);
            } elseif (is_numeric($params['sales_slip_detail_id'])) {
                $this->db->where('ssd.id', $params['sales_slip_detail_id']);
            }
        }

        if (isset($params['status'])) {
            $status_cond = array();
            if (in_array(SALES_STATUS_NO_INPUT, $params['status'])) {
                $status_cond[] =
                    '(
                        NOT EXISTS (
                           SELECT \'x\' FROM DUAL WHERE `ssd`.`id` IS NOT NULL AND `ssd`.`is_ad_receive_payment` = 0
                           UNION
                           SELECT \'x\'
                           FROM `sales_slip_detail` `ssd_fixed`
                           WHERE `ssd`.`is_ad_receive_payment` = 1
                           AND `ssd_fixed`.`j_code` = `o`.`j_code`
                           AND `ssd_fixed`.`branch_cd` = `od`.`branch_cd`
                           AND `ssd_fixed`.`ad_receive_sales_slip_detail_id` = `ssd`.`id`
                           AND `ssd_fixed`.`is_ad_receive_payment` = 0
                           AND `ssd_fixed`.`disable` = 0
                        )
                    )';
            }
            if (in_array(SALES_STATUS_UNCREATED, $params['status'])) {
                $status_cond[] =
                '(
                  (`ssd`.`id` IS NOT NULL AND `ss`.`id` IS NULL)
                  OR
                  EXISTS (
                   SELECT \'x\'
                   FROM `sales_slip_detail` `ssd_fixed`
                   LEFT JOIN `sales_slip` `ss_fixed` ON `ss_fixed`.`id` = `ssd_fixed`.`sales_slip_id` AND `ss_fixed`.`disable` = 0 -- 売上伝票が無い
                   WHERE `ssd`.`is_ad_receive_payment` = 1
                   AND  `ssd_fixed`.`j_code` = `o`.`j_code`
                   AND `ssd_fixed`.`branch_cd` = `od`.`branch_cd`
                   AND `ssd_fixed`.`ad_receive_sales_slip_detail_id` = `ssd`.`id`
                   AND `ssd_fixed`.`is_ad_receive_payment` = 0
                   AND `ssd_fixed`.`disable` = 0
                   AND `ss_fixed`.`id` IS NULL
                  )
                )';
            }

            $other_status = array_diff($params['status'], array(SALES_STATUS_NO_INPUT, SALES_STATUS_UNCREATED));

            if (!empty($other_status)) {
                $status_cond[] =
                '(
                  (`ssd`.`id` IS NOT NULL AND `ss`.`id` IS NOT NULL AND `ss`.`sales_slip_status` IN (' . implode(',', $other_status) . '))
                  OR
                  EXISTS (
                       SELECT \'x\'
                       FROM `sales_slip_detail` `ssd_fixed`
                       JOIN `sales_slip` `ss_fixed` ON `ss_fixed`.`id` = `ssd_fixed`.`sales_slip_id` AND `ss_fixed`.`disable` = 0
                       WHERE `ssd`.`is_ad_receive_payment` = 1
                       AND  `ssd_fixed`.`j_code` = `o`.`j_code`
                       AND `ssd_fixed`.`branch_cd` = `od`.`branch_cd`
                       AND `ssd_fixed`.`ad_receive_sales_slip_detail_id` = `ssd`.`id`
                       AND `ssd_fixed`.`is_ad_receive_payment` = 0
                       AND `ss_fixed`.`sales_slip_status` IN (' . implode(',', $other_status) . ')
                       AND `ssd_fixed`.`disable` = 0
                  UNION
                       SELECT \'x\'
                       FROM sales_slip_detail ssd_adv
                       LEFT JOIN sales_slip ss_adv ON ssd_adv.sales_slip_id = ss_adv.id AND ss_adv.`disable` = 0
                       WHERE ssd_adv.j_code = o.j_code
                       AND ssd_adv.branch_cd = od.branch_cd
                       AND ssd.is_ad_receive_payment = 0
                       AND ssd_adv.is_ad_receive_payment = 1
                       AND ssd_adv.id = ssd.ad_receive_sales_slip_detail_id
                       AND `ss_adv`.`sales_slip_status` IN (' . implode(',', $other_status) . ')
                       AND ssd_adv.`disable` = 0
                  )
                )';
            }

            $this->db->where('(' . implode(' OR ', $status_cond). ')', NULL, false);
        }

        if (isset($params['is_ad_receive_payment'])) {
            $this->db->where('o.is_ad_receive_payment', $params['is_ad_receive_payment']);
        }

        if (isset($params['j_account']) && $params['j_account'] != '0') {
            $this->db->where('o.j_account_id', $params['j_account']);
        } elseif (isset($params['j_business_unit']) && $params['j_business_unit'] != '0') {
                $this->db->where(
                    'EXISTS (
                       SELECT \'x\'
                       FROM `account` `ja`
                       JOIN `department` `d` ON `ja`.`department_id` = `d`.`id` AND d.business_unit_id = ' . $params['j_business_unit'] . '
                       WHERE `o`.`j_account_id` = `ja`.`id`
                    )');
        }

        if (isset($params['client_name'])) {
            $this->db->like('c.name', $params['client_name']);
        }

        if (isset($params['j_code_branch_cd_from'])) {
//         	echo 'sdsdsdsd';
//         	print_r($params['j_code_branch_cd_from']);
//             if (isset($params['j_code_branch_cd_to'])) {
//                 $this->db->where('CONCAT(o.j_code, od.branch_cd) >=', str_pad($params['j_code_branch_cd_from'], 10, '0', STR_PAD_RIGHT));
//                 $this->db->where('CONCAT(o.j_code, od.branch_cd) <=', str_pad($params['j_code_branch_cd_to'], 10, '9', STR_PAD_RIGHT));
//             } else {
//                 $this->db->like('CONCAT(o.j_code, od.branch_cd)', $params['j_code_branch_cd_from'], 'after');
//             }
//#7067:Start
            $data['from_jcode_branch'] = $params['j_code_branch_cd_from'];
            if (isset($params['j_code_branch_cd_to'])) {
            		$data['to_jcode_branch'] =  $params['j_code_branch_cd_to'];
            }
            $wheresql_jocde_branch = $this->get_where_sql_by_branchcode_and_jcode($data,'o.j_code','od.branch_cd');
            if(!empty($wheresql_jocde_branch)){
            	$this->db->where($wheresql_jocde_branch);
            }
//#7067:End
        }

        if (isset($params['sales_slip_number'])) {
            $this->db->where('(ss.sales_slip_number = \'' . $params['sales_slip_number'] . '\'
            OR
                EXISTS (
                    SELECT \'x\'
                    FROM sales_slip_detail ssd_adv
                    LEFT JOIN sales_slip ss_adv ON ssd_adv.sales_slip_id = ss_adv.id AND ss_adv.`disable` = 0
                    WHERE ssd_adv.j_code = o.j_code
                    AND ssd_adv.branch_cd = od.branch_cd
                    AND ssd.is_ad_receive_payment = 0
                    AND ssd_adv.is_ad_receive_payment = 1
                    AND ssd_adv.id = ssd.ad_receive_sales_slip_detail_id
                    AND ssd_adv.`disable` = 0
                    AND ss_adv.sales_slip_number = \'' . $params['sales_slip_number'] . '\'
                )
            )');
        }

        if (isset($params['delivery_date_from']) && $params['delivery_date_from']) {
            if (isset($params['delivery_date_to']) && $params['delivery_date_to']) {
                $this->db->where('
                (
                    (`ssd`.`id` IS NOT NULL AND `ssd`.`delivery_date` >= \'' . $params['delivery_date_from'] . '\' AND `ssd`.`delivery_date` <= \'' . $params['delivery_date_to'] . '\'
                        AND NOT EXISTS(
                        SELECT \'x\'
                        FROM `sales_slip_detail` `ssd_fixed`
                        WHERE `ssd`.`is_ad_receive_payment` = 1
                        AND `ssd_fixed`.`j_code` = `o`.`j_code`
                        AND `ssd_fixed`.`branch_cd` = `od`.`branch_cd`
                        AND `ssd_fixed`.`ad_receive_sales_slip_detail_id` = `ssd`.`id`
                        AND `ssd_fixed`.`is_ad_receive_payment` = 0
                        AND `ssd_fixed`.`disable` = 0
                        )
                    )
                     OR
                     EXISTS (
                      SELECT \'x\'
                      FROM `sales_slip_detail` `ssd_fixed`
                      -- JOIN `sales_slip` `ss_fixed` ON `ss_fixed`.`id` = `ssd_fixed`.`sales_slip_id` AND `ss_fixed`.`disable` = 0
                      WHERE `ssd`.`is_ad_receive_payment` = 1
                      AND  `ssd_fixed`.`j_code` = `o`.`j_code`
                      AND `ssd_fixed`.`branch_cd` = `od`.`branch_cd`
                      AND `ssd_fixed`.`ad_receive_sales_slip_detail_id` = `ssd`.`id`
                      AND `ssd_fixed`.`is_ad_receive_payment` = 0
                      AND `ssd_fixed`.`disable` = 0
                      AND `ssd_fixed`.`delivery_date` >= \'' . $params['delivery_date_from'] . '\'
                      AND `ssd_fixed`.`delivery_date` <= \'' . $params['delivery_date_to'] . '\'
                      UNION
                      SELECT \'x\'
                      FROM order_acceptance sub_oa
                      WHERE ((`ssd`.`id` IS NULL
                        AND `ss`.`id` IS NULL)
                        OR (`ssd`.is_ad_receive_payment = 1
                        AND NOT EXISTS(
                            SELECT \'x\'
                            FROM `sales_slip_detail` `ssd_fixed`
                            WHERE `ssd`.`is_ad_receive_payment` = 1
                            AND `ssd_fixed`.`j_code` = `o`.`j_code`
                            AND `ssd_fixed`.`branch_cd` = `od`.`branch_cd`
                            AND `ssd_fixed`.`ad_receive_sales_slip_detail_id` = `ssd`.`id`
                            AND `ssd_fixed`.`is_ad_receive_payment` = 0
                            AND `ssd_fixed`.`disable` = 0
                            )
                        )
                      )
                      AND sub_oa.j_code = o.j_code
                      AND sub_oa.branch_cd = od.branch_cd
                      AND sub_oa.disable = 0
                      AND sub_oa.id = (
                      	SELECT id
                      	FROM order_acceptance max_oa
                      	WHERE sub_oa.j_code = max_oa.j_code
                      	AND sub_oa.branch_cd = max_oa.branch_cd
                      	AND max_oa.`disable` = 0
                      	ORDER BY max_oa.delivery_date DESC
                      	LIMIT 1
                      )
                      AND IF(EXTRACT(YEAR_MONTH FROM sub_oa.delivery_date) = EXTRACT(YEAR_MONTH FROM od.delivery_date),
                       sub_oa.delivery_date >= \'' . $params['delivery_date_from'] . '\' AND sub_oa.delivery_date <= \'' . $params['delivery_date_to'] . '\',
                       od.delivery_date >= \'' . $params['delivery_date_from'] . '\' AND od.delivery_date <= \'' . $params['delivery_date_to'] . '\'
                      )
                      UNION
                      SELECT \'x\'
                      FROM DUAL
                      WHERE ((`ssd`.`id` IS NULL
                        AND `ss`.`id` IS NULL)
                        OR (`ssd`.is_ad_receive_payment = 1
                        AND NOT EXISTS(
                            SELECT \'x\'
                            FROM `sales_slip_detail` `ssd_fixed`
                            WHERE `ssd`.`is_ad_receive_payment` = 1
                            AND `ssd_fixed`.`j_code` = `o`.`j_code`
                            AND `ssd_fixed`.`branch_cd` = `od`.`branch_cd`
                            AND `ssd_fixed`.`ad_receive_sales_slip_detail_id` = `ssd`.`id`
                            AND `ssd_fixed`.`is_ad_receive_payment` = 0
                            AND `ssd_fixed`.`disable` = 0
                            )
                        )
                      )
                      AND NOT EXISTS (
                       SELECT \'x\'
                       FROM order_acceptance sub_oa
                       WHERE sub_oa.j_code = o.j_code
                       AND sub_oa.branch_cd = od.branch_cd
                       AND sub_oa.disable = 0
                      )
                      AND od.delivery_date >= \'' . $params['delivery_date_from'] . '\'
                      AND od.delivery_date <= \'' . $params['delivery_date_to'] . '\'
                     )
                 )', NULL, false);
            } else {
                if (substr_count($params['delivery_date_from'], '-') == 2) {
                    $delivery_date_field = array(
                        'ssd' => 'ssd.delivery_date',
                        'ssd_fixed' => 'ssd_fixed.delivery_date',
                        'ssd_adv' => 'ssd_adv.delivery_date',
                        'sub_oa' => 'sub_oa.delivery_date',
                        'od' => 'od.delivery_date'
                    );
                } else {
                    $delivery_date_field = array(
                        'ssd' => 'EXTRACT(YEAR_MONTH FROM ssd.delivery_date)',
                        'ssd_fixed' => 'EXTRACT(YEAR_MONTH FROM ssd_fixed.delivery_date)',
                        'ssd_adv' => 'EXTRACT(YEAR_MONTH FROM ssd_adv.delivery_date)',
                        'sub_oa' => 'EXTRACT(YEAR_MONTH FROM sub_oa.delivery_date)',
                        'od' => 'EXTRACT(YEAR_MONTH FROM od.delivery_date)'
                    );
                }
                $this->db->where('
                (
                    (`ssd`.`id` IS NOT NULL AND ' . $delivery_date_field['ssd'] . ' = \'' . $params['delivery_date_from'] . '\'
                    AND NOT EXISTS(
                        SELECT \'x\'
                        FROM `sales_slip_detail` `ssd_fixed`
                        WHERE `ssd`.`is_ad_receive_payment` = 1
                        AND `ssd_fixed`.`j_code` = `o`.`j_code`
                        AND `ssd_fixed`.`branch_cd` = `od`.`branch_cd`
                        AND `ssd_fixed`.`ad_receive_sales_slip_detail_id` = `ssd`.`id`
                        AND `ssd_fixed`.`is_ad_receive_payment` = 0
                        AND `ssd_fixed`.`disable` = 0
                        )
                    )
                     OR
                     EXISTS (
                      SELECT \'x\'
                      FROM `sales_slip_detail` `ssd_fixed`
                      -- JOIN `sales_slip` `ss_fixed` ON `ss_fixed`.`id` = `ssd_fixed`.`sales_slip_id` AND `ss_fixed`.`disable` = 0
                      WHERE `ssd`.`is_ad_receive_payment` = 1
                      AND  `ssd_fixed`.`j_code` = `o`.`j_code`
                      AND `ssd_fixed`.`branch_cd` = `od`.`branch_cd`
                      AND `ssd_fixed`.`ad_receive_sales_slip_detail_id` = `ssd`.`id`
                      AND `ssd_fixed`.`is_ad_receive_payment` = 0
                      AND `ssd_fixed`.`disable` = 0
                      AND ' . $delivery_date_field['ssd_fixed'] . ' = \'' . $params['delivery_date_from'] . '\'
                      UNION
                      SELECT \'x\'
                      FROM order_acceptance sub_oa
                      WHERE ((`ssd`.`id` IS NULL
                        AND `ss`.`id` IS NULL)
                        OR (`ssd`.is_ad_receive_payment = 1
                        AND NOT EXISTS(
                            SELECT \'x\'
                            FROM `sales_slip_detail` `ssd_fixed`
                            WHERE `ssd`.`is_ad_receive_payment` = 1
                            AND `ssd_fixed`.`j_code` = `o`.`j_code`
                            AND `ssd_fixed`.`branch_cd` = `od`.`branch_cd`
                            AND `ssd_fixed`.`ad_receive_sales_slip_detail_id` = `ssd`.`id`
                            AND `ssd_fixed`.`is_ad_receive_payment` = 0
                            AND `ssd_fixed`.`disable` = 0
                            )
                        )
                      )
                      AND sub_oa.j_code = o.j_code
                      AND sub_oa.branch_cd = od.branch_cd
                      AND sub_oa.disable = 0
                      AND sub_oa.id = (
                      	SELECT id
                      	FROM order_acceptance max_oa
                      	WHERE sub_oa.j_code = max_oa.j_code
                      	AND sub_oa.branch_cd = max_oa.branch_cd
                      	AND max_oa.`disable` = 0
                      	ORDER BY max_oa.delivery_date DESC
                      	LIMIT 1
                      )
                      AND IF(EXTRACT(YEAR_MONTH FROM sub_oa.delivery_date) = EXTRACT(YEAR_MONTH FROM od.delivery_date),
                       ' . $delivery_date_field['sub_oa'] . ' = \'' . $params['delivery_date_from'] . '\',
                       ' . $delivery_date_field['od'] . ' = \'' . $params['delivery_date_from'] . '\'
                      )
                      UNION
                      SELECT \'x\'
                      FROM DUAL
                      WHERE ((`ssd`.`id` IS NULL
                        AND `ss`.`id` IS NULL)
                        OR (`ssd`.is_ad_receive_payment = 1
                        AND NOT EXISTS(
                            SELECT \'x\'
                            FROM `sales_slip_detail` `ssd_fixed`
                            WHERE `ssd`.`is_ad_receive_payment` = 1
                            AND `ssd_fixed`.`j_code` = `o`.`j_code`
                            AND `ssd_fixed`.`branch_cd` = `od`.`branch_cd`
                            AND `ssd_fixed`.`ad_receive_sales_slip_detail_id` = `ssd`.`id`
                            AND `ssd_fixed`.`is_ad_receive_payment` = 0
                            AND `ssd_fixed`.`disable` = 0
                            )
                        )
                      )
                      AND NOT EXISTS (
                       SELECT \'x\'
                       FROM order_acceptance sub_oa
                       WHERE sub_oa.j_code = o.j_code
                       AND sub_oa.branch_cd = od.branch_cd
                       AND sub_oa.disable = 0
                      )
                      AND ' . $delivery_date_field['od'] . ' = \'' . $params['delivery_date_from'] . '\'
                     )
                 )', NULL, false);
            }
        }

        if (isset($params['order_regist_date_from']) && $params['order_regist_date_from']) {
            if (isset($params['order_regist_date_to']) && $params['order_regist_date_to']) {
                $this->db->where('o.order_regist_date >=', $params['order_regist_date_from']);
                $this->db->where('o.order_regist_date <=', $params['order_regist_date_to']);
            } else {
                if (substr_count($params['order_regist_date_from'], '-') == 2) {
                    $this->db->where('o.order_regist_date =', $params['order_regist_date_from']);
                } else {
                    $this->db->where('EXTRACT(YEAR_MONTH FROM o.order_regist_date) =', $params['order_regist_date_from']);
                }
            }
        }

        if (isset($params['approval_date_from']) && $params['approval_date_from']) {
            if (isset($params['approval_date_to']) && $params['approval_date_to']) {
                $this->db->where('o.order_approval_date >=', $params['approval_date_from']);
                $this->db->where('o.order_approval_date <=', $params['approval_date_to']);
            } else {
                if (substr_count($params['approval_date_from'], '-') == 2) {
                    $this->db->where('o.order_approval_date =', $params['approval_date_from']);
                } else {
                    $this->db->where('EXTRACT(YEAR_MONTH FROM o.order_approval_date) =', $params['approval_date_from']);
                }
            }
        }

        if (isset($params['payment_date_from']) && $params['payment_date_from']) {
            if (isset($params['payment_date_to']) && $params['payment_date_to']) {
//#7769:Start
                $this->db->having('ssd_payment_date >=', $params['payment_date_from']);
                $this->db->having('ssd_payment_date <=', $params['payment_date_to']);
//#7769:End
            } else {
                if (substr_count($params['payment_date_from'], '-') == 2) {
//#7769:Start
                    $this->db->having('ssd_payment_date =', $params['payment_date_from']);
                } else {
                    $this->db->having('EXTRACT(YEAR_MONTH FROM ssd_payment_date) =', $params['payment_date_from']);
//#7769:End
                }
            }
        }

        if (isset($params['billing_date_from']) && $params['billing_date_from']) {
            if (isset($params['billing_date_to']) && $params['billing_date_to']) {
//#7769:Start
                $this->db->having('ssd_billing_date >=', $params['billing_date_from']);
                $this->db->having('ssd_billing_date <=', $params['billing_date_to']);
//#7769:End
            } else {
                if (substr_count($params['billing_date_from'], '-') == 2) {
//#7769:Start
                    $this->db->having('ssd_billing_date =', $params['billing_date_from']);
                } else {
                    $this->db->having('EXTRACT(YEAR_MONTH FROM ssd_billing_date) =', $params['billing_date_from']);
//#7769:End
                }
            }
        }

        if (isset($params['s_code'])) {
            $this->db->like('c.s_code', $params['s_code'], 'after');
        }
        if (isset($params['client_name_furigana'])) {
            $this->db->like('c.name_kana', $params['client_name_furigana']);
        }
        if (isset($params['client_department_name'])) {
            $this->db->like('c.division_name', $params['client_department_name']);
        }
        if (isset($params['s_account']) && $params['s_account'] != '0') {
            $this->db->group_start()
                ->where('c.s_account_id_1st', $params['s_account'])
                ->or_where('c.s_account_id_2nd', $params['s_account'])
                ->group_end();
        } elseif (isset($params['s_business_unit']) && $params['s_business_unit'] != '0') {
            $this->db->where('
            EXISTS (
               SELECT \'x\'
               FROM `account` `sa`
               JOIN `department` `d` ON `sa`.`department_id` = `d`.`id` AND d.business_unit_id = ' . $params['s_business_unit'] . '
               WHERE `c`.`s_account_id_1st` = `sa`.`id` OR `c`.`s_account_id_2nd` = `sa`.`id`
            )');
        }

        return $this;
    }

    public function search_sales_slip_validate($post)
    {
        foreach ($post as $key => $value) {
            if (!is_array($value) && !trim($value)) {
                unset($post[$key]);
                continue;
            }
            if (strpos($key, 'search_') === 0) {
                $post[substr($key, 7)] = $value;
                unset($post[$key]);
            }
        }

        $error = array();
        foreach ($post as $key => $value) {
            $field_type = substr($key, strrpos($key, '_') + 1);
            $field_name = substr($key, 0, strrpos($key, '_') + 1);
            switch ($key) {
                case 'delivery_date_from':
                case 'delivery_date_to':
                case 'order_regist_date_from':
                case 'order_regist_date_to':
                case 'approval_date_from':
                case 'approval_date_to':
                case 'payment_date_from':
                case 'payment_date_to':
                case 'billing_date_from':
                case 'billing_date_to':
                    if ($date = parse_date($value)) {
                        if (substr_count($date, '-') == 2) {
                            $post[$key] = $date;
                        } else {
                            if ($field_type == 'from') {
                                if (isset($post[$field_name . 'to'])) {
                                    $post[$key] = $date . '-01';
                                } else {
                                    $post[$key] = str_replace('-', '', $date);
                                }
                            } else {
                                $post[$key] = date('Y-m-t', strtotime($date));
                            }
                        }
                        if ($field_type == 'to') {
                            if (!isset($post[$field_name . 'from'])) {
                                $error[$field_name . 'from'] = lang('common_msg_invalid_date_range');
                            } elseif (parse_date($post[$field_name . 'from']) > parse_date($post[$field_name . 'to'])) {
                                $error[$field_name . 'from'] = lang('common_msg_invalid_date_range');
                            }
                        }
                    } else {
                        $field_type = ucfirst($field_type);
                        $error[$key] = str_replace('%field%', $field_type, lang('common_msg_invalid_date'));
                    }
                    break;
                case 'j_code_branch_cd_from':
                case 'j_code_branch_cd_to':

                    $sub_part = explode('-', $post[$key]);
                    if (isset($sub_part[1]) && strlen($sub_part[1]) > 2) {
                        $error[$key] = str_replace('%field%', ucfirst($field_type), lang('common_msg_search_range_invalid'));
                        break;
                    }

                    if ($field_type == 'to') {
                        if (!isset($post[$field_name . 'from'])) {
                            $error[$field_name . 'from'] = lang('common_msg_invalid_date_range');
//#7067:Start
//                        } elseif (str_pad($post[$field_name . 'from'], 10, '0', STR_PAD_RIGHT) > str_pad($post[$field_name . 'to'], 10, '9', STR_PAD_RIGHT)) {
                        } elseif (!$this->_validate_jcode_branchcd_range($post[$field_name . 'from'], $post[$field_name . 'to'])) {
//#7067:End
                            $error[$field_name . 'from'] = lang('common_msg_invalid_date_range');
                        }
                    }
                    break;
            }
        }
        if (empty($error)) {
            return array(true, $post);
        } else {
            return array(false, $error);
        }
    }

//#7067:Start
    /**
     * Compare and return false if $from > $to
     * Required both $from and $to to be input
     *
     * @param $from
     * @param $to
     * @return bool
     * @author quang_duc
     */
    public function _validate_jcode_branchcd_range($from, $to)
    {
        if (strlen($from) == 0 || strlen($to) == 0) {
            return false;
        }
        $sub_from = explode('-', $from);
        $sub_to = explode('-', $to);
        $max_j_length = (strlen($sub_from[0]) > strlen($sub_to[0])) ? strlen($sub_from[0]) : strlen($sub_to[0]);
        if (strlen($sub_from[0]) < $max_j_length) {
            $sub_from[0] = str_pad($sub_from[0], $max_j_length, '0', STR_PAD_RIGHT);
        } elseif (strlen($sub_to[0]) < $max_j_length) {
            $sub_to[0] = str_pad($sub_to[0], $max_j_length, '9', STR_PAD_RIGHT);
        }

        if ($sub_from[0] > $sub_to[0]) {
            return false;
        } elseif ($sub_from[0] < $sub_to[0]) {
            return true;
        }

        if (isset($sub_from[1]) && isset($sub_to[1])) {
            return ($sub_from[1] > $sub_to[1]) ? false : true;
        } else {
            return true;
        }
    }
//#7067:End

    public function apply_approval($ids)
    {
        if (!$this->is_sales_slip_accountant_opened($ids)) {
            $this->error = lang('msg_sales_slip_accountant_is_closed');
            return false;
        }
        if ($this->check_confirm_amount_included_all_adv_amount_by_sales_slip($ids) !== true) {
            $this->error = lang('msg_confirm_amount_not_included_all_adv_amount');
            return false;
        }

        $data = array(
            'sales_slip_status' => SALES_STATUS_APPROVAL_PENDING,
            'lastup_datetime' => $this->_db_now(),
            'lastup_account_id' => $this->auth->get_account_id()
        );

        return $this->db->where_in('id', $ids)->update('sales_slip', $data);
    }

    public function approve($ids)
    {
        if (!$this->auth->has_permission('sales_approval', 'column')) {
            return false;
        }

        if (!$this->is_sales_slip_accountant_opened($ids)) {
            $this->error = lang('msg_sales_slip_accountant_is_closed');
            return false;
        }
        if ($this->check_confirm_amount_included_all_adv_amount_by_sales_slip($ids) !== true) {
            $this->error = lang('msg_confirm_amount_not_included_all_adv_amount');
            return false;
        }

        $sales_slip = $this->db->select('id, sales_slip_status')
            ->where_in('id', $ids)
            ->where('disable', STATUS_ENABLE)
            ->get('sales_slip')
            ->result_array();
        $update_id = array();
        foreach ($sales_slip as $row) {
            if ($row['sales_slip_status'] == SALES_STATUS_APPROVAL_PENDING) {
                $update_id[] = $row['id'];
            }
        }

        $data = array(
            'sales_slip_status'              => SALES_STATUS_INVOICE_APPROVAL_PENDING,
            'sales_slip_approval_account_id' => $this->auth->get_account_id(),
            'sales_slip_approval_date'       => $this->_db_now('Y-m-d'),
            'lastup_datetime'                => $this->_db_now(),
            'lastup_account_id'              => $this->auth->get_account_id()
        );

        $this->db->trans_start();

        $this->db->where_in('id', $update_id)->update('sales_slip', $data);

//#8260:Start
        $j_code_branch_cd = $this->get_j_code_branch_cd_by_sales_slip_id($ids);

        if (!$this->update_order_flag($j_code_branch_cd)) {
            $this->error = 'Error update order detail flags';
            return false;
        }

        $this->load->model('order_detail_model');
        if (!$this->order_detail_model->update_status_by_flag($j_code_branch_cd)) {
            $this->error = 'Error update order detail status';
            return false;
        }
//#8260:End

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    /**
     * Approves Sales slip
     * @param array $params
     * @return boolean
     * @author cong_tien
     */
    public function approves(array $params) {
        if (!$this->auth->has_permission('sales_approval', 'column')) {
            return false;
        }

        $sales_slip_ids = array();

        foreach ($params as $value) {
            $sales_slip_ids[] = $value['id'];
        }

        if (!$this->is_sales_slip_accountant_opened($sales_slip_ids)) {
            $this->error = lang('msg_sales_slip_accountant_is_closed');
            return false;
        }
        if ($this->check_confirm_amount_included_all_adv_amount_by_sales_slip($sales_slip_ids) !== true) {
            $this->error = lang('msg_confirm_amount_not_included_all_adv_amount');
            return false;
        }

        $sales_slip = $this->db->select('id, sales_slip_status')
                ->where_in('id', $sales_slip_ids)
                ->where('disable', STATUS_ENABLE)
                ->get('sales_slip')
                ->result_array();

        $data_updates = array();

        $db_now = $this->_db_now();
        $db_now_format = $this->_db_now('Y-m-d');
        $account_id = $this->auth->get_account_id();

        foreach ($sales_slip as $row) {
            if ($row['sales_slip_status'] == SALES_STATUS_APPROVAL_PENDING) {
                $data_updates[] = array(
                    'id' => $row['id'],
                    'comment' => $params[$row['id']]['comment'],
                    'sales_slip_status' => SALES_STATUS_INVOICE_APPROVAL_PENDING,
                    'sales_slip_approval_account_id' => $account_id,
                    'sales_slip_approval_date' => $db_now_format,
                    'lastup_datetime' => $db_now,
                    'lastup_account_id' => $account_id
                );
            }
        }

        $this->db->trans_start();

        if (!empty($data_updates)) {
            $this->db->update_batch('sales_slip', $data_updates, 'id');
        }

//#8260:Start
        $j_code_branch_cd = $this->get_j_code_branch_cd_by_sales_slip_id($sales_slip_ids);

        if (!$this->update_order_flag($j_code_branch_cd)) {
            $this->error = 'Error update order detail flags';
            return false;
        }

        $this->load->model('order_detail_model');
        if (!$this->order_detail_model->update_status_by_flag($j_code_branch_cd)) {
            $this->error = 'Error update order detail status';
            return false;
        }
//#8260:End

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function reject($params) {
        if (!$this->auth->has_permission('sales_approval', 'column')) {
            return false;
        }

        $sales_slip = $this->db->select('id, sales_slip_status')
                ->where('id', $params['id'])
                ->where('disable', STATUS_ENABLE)
                ->get('sales_slip')
                ->row_array();

        if ($sales_slip && $sales_slip['sales_slip_status'] == SALES_STATUS_APPROVAL_PENDING) {
            $data = array(
                'comment' => $params['comment'],
                'sales_slip_status' => SALES_STATUS_CREATED,
                'lastup_datetime' => $this->_db_now(),
                'lastup_account_id' => $this->auth->get_account_id()
            );

            return $this->db->where('id', $params['id'])->update('sales_slip', $data);
        }

        return false;
    }

    public function approval_to_pending($params) {

        $this->load->model('sales_slip_model');
        if (!$sales_data = $this->sales_slip_model->get_sales_slip_info($params['id'])) {
            return false;
        }

        $is_approval = false;
        if ($this->auth->has_permission('all_invoices', 'column')) {
            if ($sales_data['sales_slip_status'] == SALES_STATUS_CREATED) {
                $is_approval = true;
            }
        } elseif ($this->auth->has_permission('self_invoices', 'column')) {
            if ($this->auth->get_account_id() == $sales_data['j_account_id']) {
                if ($sales_data['sales_slip_status'] == SALES_STATUS_CREATED) {
                    $is_approval = true;
                }
            }
        }

        if (!$this->is_sales_slip_accountant_opened($params['id'])) {
            $this->error = lang('msg_sales_slip_accountant_is_closed');
            return false;
        }
        if ($this->check_confirm_amount_included_all_adv_amount_by_sales_slip($params['id']) !== true) {
            $this->error = lang('msg_confirm_amount_not_included_all_adv_amount');
            return false;
        }

        if ($is_approval && $sales_data['sales_slip_status'] == SALES_STATUS_CREATED) {
            $data = array(
                'sales_slip_status' => SALES_STATUS_APPROVAL_PENDING,
                'lastup_datetime' => $this->_db_now(),
                'lastup_account_id' => $this->auth->get_account_id()
            );

            return $this->db->where('id', $params['id'])->update('sales_slip', $data);
        }

        return false;
    }

    public function add_to_sales_slip($slip_number, $sales_slip_detail_ids)
    {
        $sales_slip = $this->filter(array('sales_slip_number' => $slip_number))->get_data();

        if (empty($sales_slip)) {
            $this->error = lang('msg_slip_number_not_exists');
            return false;
        }
        $sales_slip_ids = array();
        foreach ($sales_slip as $index => $row) {
            if (in_array(intval($row['status']), array(SALES_STATUS_CREATED, SALES_STATUS_APPROVAL_PENDING)) && !isset($sales_slip_ids[$row['sales_slip_id']])) {
                $sales_slip_ids[$row['sales_slip_id']] = $index;
            }
        }
        if (empty($sales_slip_ids)) {
            $this->error = lang('msg_sales_slip_status_cant_add');
            return false;
        } elseif (count($sales_slip_ids) > 1) {
            $this->error = 'This sales slip number has more than one sales slip.'; // This message shouldn't appear in normal circumstance
            return false;
        } else {
            $sales_slip = $sales_slip[reset($sales_slip_ids)];
        }

        if (!$this->is_sales_slip_accountant_opened($sales_slip['sales_slip_id'])) {
            $this->error = lang('msg_sales_slip_accountant_is_closed');
            return false;
        }

        $this->load->model('product_model');
//        $products = $this->product_model->get_all_products();
//        $products = map_element($products, 'id', 'name');

        $sales_slip_detail = $this->filter(array('sales_slip_detail_id' => $sales_slip_detail_ids))->get_data();

//        $ssd = array();
        foreach ($sales_slip_detail as $detail) {
            if ($sales_slip['s_code'] != $detail['s_code']) {
                $this->error = lang('msg_same_s_code');
                return false;
            }
//#7769:Start
//            if (($sales_slip['order_payment_date'] != $detail['order_payment_date']) ||
//                ($sales_slip['order_billing_date'] != $detail['order_billing_date'])) {
            if (($sales_slip['ssd_payment_date'] != $detail['ssd_payment_date']) ||
                ($sales_slip['ssd_billing_date'] != $detail['ssd_billing_date'])) {
//#7769:End
                $this->error = lang('msg_same_payment_billing_date');
                return false;
            }
            if (substr($sales_slip['order_delivery_date'], 0, 7) != substr($detail['order_delivery_date'], 0, 7)) {
                $this->error = lang('msg_same_delivery_date');
                return false;
            }
            if ($sales_slip['is_ad_receive_payment'] != $detail['is_ad_receive_payment']) {
                $this->error = lang('msg_mix_adv_payment_type');
                return false;
            }

            if ($detail['ad_receive_sales_slip_detail_id'] != 0 && $detail['is_ad_receive_payment'] == 0) {
                $sales_slip_number = $this->db->select('ss.sales_slip_number AS number')
                                            ->from('sales_slip_detail ssd')
                                            ->join('sales_slip ss', 'ssd.sales_slip_id = ss.id AND ss.disable = 0')
                                            ->where('ssd.id', $detail['ad_receive_sales_slip_detail_id'])
                                            ->where('ssd.disable = 0')
                                            ->get()
                                            ->row_array();
                if (isset($sales_slip_number) && $sales_slip_number['number'] != $sales_slip['sales_slip_number']) {
                    $this->error = lang('msg_not_same_parent_slip_number');
                    return false;
                }
            }

//            $ssd[$detail['sales_slip_detail_id']] = array(
//                'product_name'  => isset($products[$detail['order_product_id']]) ? $products[$detail['order_product_id']] : '',
//                'contents'      => $detail['order_contents']
//            );
        }

        $data = array();
        foreach ($sales_slip_detail_ids as $id) {
            $data[] = array(
                'id' => $id,
                'sales_slip_id' => $sales_slip['sales_slip_id'],
//                'product_name' => isset($ssd[$id]['product_name']) ? $ssd[$id]['product_name'] : '',
//                'contents' => isset($ssd[$id]['contents']) ? $ssd[$id]['contents'] : '',
                'lastup_account_id' => $this->auth->get_account_id(),
                'lastup_datetime' => $this->_db_now()
            );
        }

        $this->db->trans_start();

        if (!empty($data)) {
            $this->db->update_batch('sales_slip_detail', $data, 'id');
        }

        $this->_calculate_sales_slip_total_amount($sales_slip['sales_slip_id']);

//#8260:Start
//        $j_code_branch_cd = $this->get_j_code_branch_cd_by_sales_slip_id($sales_slip['sales_slip_id']);
//
//        if (!$this->update_order_flag($j_code_branch_cd)) {
//            $this->error = 'Error update order detail flags';
//            return false;
//        }
//
//        $this->load->model('order_detail_model');
//        if (!$this->order_detail_model->update_status_by_flag($j_code_branch_cd)) {
//            $this->error = 'Error update order detail status';
//            return false;
//        }
//#8260:End

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function do_update($data)
    {
        $closing_accountant_date = $this->get_closing_accountant_date();
//#6900:Start
        $order_detail_id = get_element($data, 'order_detail_id');

        $order_detail = $this->db->select('id, is_change_report_apply')
            ->from('order_detail')
            ->where_in('id', $order_detail_id)
            ->where('disable = 0')
            ->get()->result_array();
        $order_detail = index_element($order_detail, 'id');
//#6900:End

        $this->db->trans_start();

        $update_data = array();
        $delete_data = array();
        $recalculate_sales_slip_ids = $update_payment_billing_sales_slip_ids = array();
        $update_order_flag = array();
        $last_inserted_id = 0;
        foreach ($data as $index => $row) {
            if (str_replace('/', '-', substr($row['delivery_date'], 0, 7)) <= substr($closing_accountant_date, 0, 7)) {
                $this->error = lang('msg_sales_slip_accountant_is_closed');
                return false;
            }

//#6900:Start
            if (isset($order_detail[$row['order_detail_id']]) && $order_detail[$row['order_detail_id']]['is_change_report_apply']) {
                $this->error = lang('msg_order_is_change_report');
                return false;
            }
//#6900:End

//#7791:Start
            $row['product_name'] = str_replace("\n", '', $row['product_name']);
            $row['contents'] = str_replace("\n", '', $row['contents']);
//#7791:End

            if ($row['to_be_deleted'] && $row['sales_slip_detail_id'] > 0) {
                $delete_data[] = array(
                    'id' => $row['sales_slip_detail_id'],
                    'disable' => STATUS_DISABLE,
                    'lastup_account_id' => $this->auth->get_account_id(),
                    'lastup_datetime' => $this->_db_now()
                );
                $update_order_flag[] = array(
                    'j_code' => $row['j_code'],
                    'branch_cd' => $row['branch_cd']
                );

                continue;
            }

//#7769:Start
//            if ($row['order_is_ad_receive_payment'] || str_replace('/', '-', $row['delivery_date']) == $row['order_delivery_date']) {
            if ($row['order_is_ad_receive_payment'] == 1) {
                $payment_billing = array(
                    'billing_date' => $row['order_billing_date'],
                    'payment_date' => $row['order_payment_date']
                );
            } else {
                $this->load->model('order_model');
                $payment_billing = $this->order_model->calculate_payment_billing_date($row['delivery_date'], $row['client_id']);
                if (!is_array($payment_billing)) {
                    return false;
                }
            }
//#7769:End

            if (is_numeric($row['sales_slip_detail_id']) && $row['sales_slip_detail_id'] > 0) {
                $update_row = array(
                    'id' => $row['sales_slip_detail_id'],
                    'delivery_date' => $row['delivery_date'],
//#7769:Start
                    'billing_date' => $payment_billing['billing_date'],
                    'payment_date' => $payment_billing['payment_date'],
//#7769:End
                    'product_name' => $row['product_name'],
                    'contents' => $row['contents'],
                    'quantity' => $row['quantity'],
                    'tax_type' => $row['tax_type'],
                    'unit_price' => $row['unit_price'],
//                    'amount' => floor(strip($row['unit_price'] * $row['quantity'])),
                    'amount' => floor(bcmul($row['unit_price'], $row['quantity'])),
                    'lastup_account_id' => $this->auth->get_account_id(),
                    'lastup_datetime' => $this->_db_now()
                );
                $update_data[] = $update_row;

                if ($row['sales_slip_id']) {
                    $recalculate_sales_slip_ids[] = $row['sales_slip_id'];

//#7769:Start
                    if (!$row['order_is_ad_receive_payment']) {
                        $update_payment_billing_sales_slip_ids[] = $row['sales_slip_id'];
                    }
//#7769:End
                }
            } else {

                $insert_data = array(
                    'j_code' => $row['j_code'],
                    'branch_cd' => $row['branch_cd'],
                    'is_ad_receive_payment' => $row['is_ad_receive_payment'],
                    'delivery_date' => $row['delivery_date'],
//#7769:Start
                    'billing_date' => $payment_billing['billing_date'],
                    'payment_date' => $payment_billing['payment_date'],
//#7769:End
                    'product_name' => $row['product_name'],
                    'contents' => $row['contents'],
                    'quantity' => $row['quantity'],
                    'tax_type' => $row['tax_type'],
                    'unit_price' => $row['unit_price'],
//                    'amount' => floor(strip($row['unit_price'] * $row['quantity'])),
                    'amount' => floor(bcmul($row['unit_price'], $row['quantity'])),
                    'lastup_account_id' => $this->auth->get_account_id(),
                    'create_datetime' => $this->_db_now(),
                    'lastup_datetime' => $this->_db_now()
                );
                $update_order_flag[] = array(
                    'j_code' => $row['j_code'],
                    'branch_cd' => $row['branch_cd']
                );
                if ($row['ad_receive_sales_slip_detail_id']) {
                    $insert_data['ad_receive_sales_slip_detail_id'] = $row['ad_receive_sales_slip_detail_id'];
                }
                if ($row['sales_slip_id']) {
                    $insert_data['sales_slip_id'] = $row['sales_slip_id'];
                }

                $this->db->insert('sales_slip_detail', $insert_data);
                if ($insert_data['is_ad_receive_payment'] && !isset($insert_data['ad_receive_sales_slip_detail_id'])) {
                    $last_inserted_id = $this->db->insert_id();
                    $this->db->update('sales_slip_detail',
                        array('ad_receive_sales_slip_detail_id' => $last_inserted_id),
                        array('id' => $last_inserted_id));
                }
            }
        }

        if (!empty($update_data)) {
            $this->db->update_batch('sales_slip_detail', $update_data, 'id');
        }

        if (!empty($delete_data)) {
            $this->db->update_batch('sales_slip_detail', $delete_data, 'id');
        }

//#7769:Start
        if (!$this->_update_sales_slip_payment_billing_date($update_payment_billing_sales_slip_ids)) {
            return false;
        }
//#7769:End

        $this->_calculate_sales_slip_total_amount($recalculate_sales_slip_ids);

        if (!empty($update_order_flag)) {
            if (!$this->update_order_flag($update_order_flag)) {
                return false;
            }
            $this->load->model('order_detail_model');
            if (!$this->order_detail_model->update_status_by_flag($update_order_flag)) {
                return false;
            }
        }

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

//#7769:Start
    protected function _update_sales_slip_payment_billing_date($sales_slip_ids)
    {
        if (is_numeric($sales_slip_ids)) {
            $sales_slip_ids = array($sales_slip_ids);
        } elseif (!is_array($sales_slip_ids)) {
            $this->error = 'Invalid sales slip id';
            return false;
        }

        $processed_sales_slip = $update_sales_slip = array();
        foreach ($sales_slip_ids as $sales_slip_id) {

            if (isset($processed_sales_slip[$sales_slip_id])) {
                continue;
            }

            $processed_sales_slip[$sales_slip_id] = true;

            $sales_slip_detail = $this->db->select('billing_date, payment_date')
                ->from('sales_slip_detail')
                ->where('sales_slip_id', $sales_slip_id)
                ->where('disable', 0)
                ->get()->result_array();

            $payment_date = $sales_slip_detail[0]['payment_date'];
            $billing_date = $sales_slip_detail[0]['billing_date'];
            foreach ($sales_slip_detail as $ssd) {
                if ($payment_date != $ssd['payment_date'] || $billing_date != $ssd['billing_date']) {
                    $this->error = lang('msg_sales_closing_date_different');
                    return false;
                }
            }

            $update_sales_slip[] = array(
                'id' => $sales_slip_id,
                'payment_date' => $payment_date,
                'billing_date' => $billing_date,
                'lastup_datetime' => $this->_db_now(),
                'lastup_account_id' => $this->auth->get_account_id()
            );
        }

        if (!empty($update_sales_slip)) {
            return $this->update_batch('sales_slip', $update_sales_slip, 'id');
        }

        return true;
    }
//#7769:End

    protected function _calculate_sales_slip_total_amount($sales_slip_ids)
    {
        if (is_numeric($sales_slip_ids)) {
            $sales_slip_ids = array($sales_slip_ids);
        } elseif (!is_array($sales_slip_ids)) {
            $this->error = 'Invalid sales slip id';
            return false;
        }

        $update_data = array();
        foreach ($sales_slip_ids as $sales_slip_id) {
            $sales_slip_detail = $this->db->select('ssd1.*, ssd2.id AS adv_id, ssd2.delivery_date AS adv_delivery_date, ssd2.tax_type AS adv_tax_type, ssd2.amount AS adv_amount')
                ->join('sales_slip_detail ssd2', 'ssd1.ad_receive_sales_slip_detail_id = ssd2.id', 'left')
                ->where('ssd1.sales_slip_id', $sales_slip_id)
                ->where('ssd1.disable', STATUS_ENABLE)
                ->get('sales_slip_detail ssd1')
                ->result_array();

            $this->load->model('consumption_tax_model');

            $amount_tax_non = 0;
            $total_amount = $total_tax = $total_amount_tax = 0;
            $conf_amount = $adv_amount = 0;
            $adv_processed = array();
            $tax_rate = array();
            $max_dd = $sales_slip_detail[0]['delivery_date'];
            foreach ($sales_slip_detail as $row) {

                if (!isset($tax_rate[$row['delivery_date']])) {
                    $tax_rate[$row['delivery_date']] = $this->consumption_tax_model->get_tax_rate_by_date($row['delivery_date']);
                    if ($row['delivery_date'] > $max_dd) {
                        $max_dd = $row['delivery_date'];
                    }
                }

                if ($row['tax_type'] == TAX_INCLUDED) {
//                    $row['amount'] = ceil(strip($row['amount'] / (1 + intval($tax_rate[$row['delivery_date']]) / 100)));
                    $row['amount'] = ceil(bcdiv($row['amount'], bcadd(1, bcdiv($tax_rate[$row['delivery_date']], 100))));
                }
                if ($row['adv_tax_type'] == TAX_INCLUDED) {
//#6949:Start
                    if (!isset($tax_rate[$row['adv_delivery_date']])) {
                        $tax_rate[$row['adv_delivery_date']] = $this->consumption_tax_model->get_tax_rate_by_date($row['adv_delivery_date']);
                    }
//#6949:End
//                    $row['adv_amount'] = ceil(strip($row['adv_amount'] / (1 + intval($tax_rate[$row['adv_delivery_date']]) / 100)));
                    $row['adv_amount'] = ceil(bcdiv($row['adv_amount'], bcadd(1, bcdiv($tax_rate[$row['adv_delivery_date']], 100))));
                }

                if ($row['is_ad_receive_payment'] == 0 && $row['ad_receive_sales_slip_detail_id'] != 0 && !in_array($row['adv_id'], $adv_processed)) {
                    $amount = $row['amount'] - $row['adv_amount'];
                    $adv_processed[] = $row['adv_id'];
                    $adv_amount += $row['adv_amount'];
                } else {
                    $amount = $row['amount'];
                }

                $conf_amount += $row['amount'];

                switch ($row['tax_type']) {
                    case TAX_EXCLUSIVE:
                    case TAX_INCLUDED:
                        $total_amount += $amount;
                        break;
                    case TAX_NON:
                        $amount_tax_non += $amount;
                        $total_amount += $amount;
                        break;
                }
            }

            if (!empty($adv_processed)) {
                $total_amount = $conf_amount - $adv_amount;
//                $total_tax = floor(strip( $conf_amount * (intval($tax_rate[$max_dd]) / 100) )) - floor(strip( $adv_amount * (intval($tax_rate[$max_dd]) / 100) ));
                $total_tax = floor(bcmul($conf_amount, bcdiv($tax_rate[$max_dd], 100))) - floor(bcmul($adv_amount, bcdiv($tax_rate[$max_dd], 100)));
            } else {
//                $total_tax = ($total_amount - $amount_tax_non) * (intval($tax_rate[$max_dd]) / 100);
                $total_tax = bcmul($total_amount - $amount_tax_non, bcdiv($tax_rate[$max_dd], 100));
//                $total_tax = floor(strip($total_tax));
                $total_tax = floor($total_tax);

            }

            $total_amount_tax = $total_amount + $total_tax;

            $update_data[] = array(
                'id'                => $sales_slip_id,
                'total_amount'      => $total_amount,
                'total_tax'         => $total_tax,
                'total_amount_tax'  => $total_amount_tax,
                'lastup_datetime'   => $this->_db_now(),
                'lastup_account_id' => $this->auth->get_account_id()
            );
        }

        if (!empty($update_data)) {
            return $this->db->update_batch('sales_slip', $update_data, 'id');
        }
    }

    public function get_closing_accountant_date($format = 'Y-m-d')
    {
        if ($this->closing_accountant_date === null) {
            $this->load->model('closing_accountant_model');
            $closing_accountant_date = $this->closing_accountant_model->get_closest_closing_date();
            $this->closing_accountant_date = new DateTime($closing_accountant_date[0]['closest_closing_date']);
        }

        return $this->closing_accountant_date->format($format);
    }

    public function is_sales_slip_accountant_opened($sales_slip_id)
    {
        if (is_numeric($sales_slip_id)) {
            $sales_slip_id = array($sales_slip_id);
        } elseif (!is_array($sales_slip_id)) {
            log_message('error', __FILE__ . '(line ' . __LINE__ . '): Invalid sales slip detail id');
            return false;
        }

        $sales_slip_accountant_opened = $this->db->select('id')
            ->from('sales_slip_detail')
            ->where_in('sales_slip_id', $sales_slip_id)
            ->where('EXTRACT(YEAR_MONTH FROM delivery_date) > ' . $this->get_closing_accountant_date('Ym'))
            ->where('disable', 0)
            ->get()
            ->result_array();

        if ($sales_slip_accountant_opened) {
            return true;
        }

        return false;
    }

    /**
     * search sales slip in invoice's screen
     * @param  array $form_data
     * @return array
     * @author  cam_tien
     */

    public function  get_is_product_division(){
        $account_business_unit      = $this->auth->get_business_unit();
        $j_account_business_units   = $account_business_unit;
        $is_product_division      = $account_business_unit[0]['is_product_division'];
        return $is_product_division;
    }
    public function  get_seft_invoices(){

        $is_self_invoices = $this->auth->has_permission('self_invoices', 'column');


        return $is_self_invoices;

    }
    public function search_invoices($form_data) {

        $form_data = array_map_trim($form_data);
        $to_billing_date ="";
        $from_billing_date="";

        if (!empty($form_data['delivery_date_from']) && empty($form_data['delivery_date_to'])) {

            $delivery_date_from = str_replace('/', '-', $form_data['delivery_date_from']);

            list($from_year, $from_month, $from_day) = array_pad(explode('-', $delivery_date_from), 3, null);

            if ($from_year) {
                if (strlen($from_year) == 2) {
                    $from_year = date('Y', strtotime($from_year . '-01-01'));
                    $to_billing_date =  date('Y', strtotime($from_year . '-01-31'));
                }
            }

            if($from_day==""){
                $from_day_form = '01';
                $from_day_to = "31";
            }
            else{
                $from_day_form = $from_day;
                $from_day_to = $from_day;
            }
            $to_billing_date = $from_year."-".$from_month.'-'.$from_day_to;
            $from_billing_date = $from_year."-".$from_month.'-'.$from_day_form;


        } elseif (!empty($form_data['delivery_date_from']) && !empty($form_data['delivery_date_to'])) {


            $delivery_date_from  = str_replace('/', '-', $form_data['delivery_date_from']);
            list($from_year, $from_month, $from_day) = array_pad(explode('-', $delivery_date_from), 3, null);

            if (strlen($from_year) == 2) {
                $from_year = date('Y', strtotime($from_year . '-01-01'));
            }

            if($from_day == ""){
                $delivery_date_from = $from_year . '-' . $from_month . '-01';
            }
            $from_billing_date = $from_year;

            $delivery_date_to = str_replace('/', '-', $form_data['delivery_date_to']);

            list($to_year, $to_month, $to_day) = array_pad(explode('-', $delivery_date_to), 3, null);

            if (strlen($to_year) == 2) {
                $to_year = date('Y', strtotime($to_year . '-01-01'));
            }

            if ($to_day == "") {
                $end_day = cal_days_in_month(CAL_GREGORIAN, $to_month, $to_year);

                $delivery_date_to = $to_year . '-' . $to_month . '-' . $end_day;
            }
            if($from_day == ""){
                $from_day ="01";
            }
            $from_billing_date = $from_billing_date."-".$from_month."-".$from_day;
            $to_billing_date =  $delivery_date_to;

        }
        // Modify Search date invice -S
         if (empty($form_data['delivery_date_from']) && empty($form_data['delivery_date_to'])){
//#7087:Start
         	if(!isset($_REQUEST['loadpage'])){
//#7087:End
                $this->load->model('closing_accountant_model');
                if ($list_closing = $this->closing_accountant_model->get_closing_month_recent()) {

//                     $close_date = date('Y-m-d', strtotime('+1 month', strtotime($list_closing[0]['close_date'])));

                	$close_date = $list_closing[0]['close_date'];
                	$close_dates = explode('-', $close_date);
                	$close_date =   $close_dates[0]."-".$close_dates[1]."-01";

                    $closedaydevilyday =  date('Y-m-d', strtotime('+1 month', strtotime($close_date)));

                }
                else{

                    $closedaydevilyday = date("Y-m-d");

                }
                $to_billing_date = '9999-12-31';
                $from_billing_date =  $closedaydevilyday;
//#7087:Start
         	}
//#7087:End
        }
       // Modify Search date invice -E
        $conde ="";
        $conde2 ="";
        if($to_billing_date <> "" && $from_billing_date <> ""){
            $conde = 'AND  ssd4.delivery_date >= "'.$from_billing_date .'" AND  ssd4.delivery_date <= "'.$to_billing_date.'"';

        }

//#8213:Start
        // Modify Search date invice -S
        $this->db->select(' DISTINCT
                                 if(( SELECT count(ssd4.id)
                                 FROM  sales_slip_detail as ssd4
                                 WHERE ssd4.sales_slip_id in(
                                     SELECT ssd3.sales_slip_id
                                        FROM sales_slip_detail as ssd3
                                        WHERE  ssd3.is_ad_receive_payment = 0 AND ssd3.ad_receive_sales_slip_detail_id  in(
                                        SELECT ssd2.id
                                        FROM sales_slip_detail as ssd2
                                        WHERE ssd2.sales_slip_id = ss.id)  and ssd4.`disable` =0 and ssd4.sales_slip_id <> 0
                                        ))>=1,1,0) is_have_parent,
                                if(( SELECT count(ssd4.id)
                                 FROM  sales_slip_detail as ssd4
                                 WHERE ssd4.sales_slip_id in(
                                     SELECT ssd3.sales_slip_id
                                        FROM sales_slip_detail as ssd3
                                        WHERE  ssd3.is_ad_receive_payment = 0 AND ssd3.ad_receive_sales_slip_detail_id  in(
                                        SELECT ssd2.id
                                        FROM sales_slip_detail as ssd2
                                        WHERE ssd2.sales_slip_id = ss.id)
                                )'.$conde.' and ssd4.`disable` =0  and ssd4.sales_slip_id <> 0  ) >=1,1,0)
                                is_have_parent_condition,
                            if((SELECT count(ssd4.id)
                            FROM  sales_slip_detail as ssd4
                            WHERE ssd4.sales_slip_id = ss.id  '.$conde.' and ssd4.`disable` =0 and ssd4.sales_slip_id <> 0 ) >= 1,1,0)
                            as is_self_condition,
                            ssd.is_ad_receive_payment, ss.billing_date as billing_date, ss.payment_date as payment_date,
                            o.j_account_id,
                            ss.client_name AS client_name,
                            c.s_code,
                            a.name AS account_name,
                            ss.id,
                            ss.sales_slip_status,
                            ss.client_id,
                            ss.charge_name,
                            ss.total_amount,
                            ss.total_tax, ss.
                            total_amount_tax,
                            ss.sales_slip_number,
                            ss.is_checked,
        					ss.invoice_approval_account_date,
                            ss.check_date,
                            DATE_FORMAT(ssd.delivery_date, "%Y-%m-01") AS delivery_date',false)
                                ->from('sales_slip ss')
                                ->join('sales_slip_detail ssd', 'ssd.`disable` = 0 AND  ssd.sales_slip_id = ss.id  ',false, false)
                                ->join('client c', 'c.id = ss.client_id and c.disable = 0')
                                ->join('order o', 'o.j_code = ssd.j_code and o.disable = 0')
                                ->join('order_detail od', 'od.order_id = o.id   and od.disable = 0')
                                ->join('account a', 'a.id = o.j_account_id and a.disable = 0')
//#8266:Start
//                                ->join('department d', 'd.id  = a.department_id  and d.disable =0')
        						->join('department d', 'd.id  = a.department_id')
//#8266:End
                                ->where('ss.disable', 0)
                                ->where('ssd.sales_slip_id <> ', 0,"", false)
                                ->order_by('o.j_account_id')
                                ->order_by('c.s_code asc')
                                ->order_by('ssd.is_ad_receive_payment desc')
                                ->order_by('sales_slip_number asc');
        $this->db->group_by('ss.id');

//#8213:End
        // Modify Search date invice -E
        $statusexsitRelease = 0;
        if(isset($form_data['sales_slip_status'])){
            $liststatus = $form_data['sales_slip_status'];
            foreach ($liststatus as $status){

                if($status == 29){
                    $statusexsitRelease = 1;
                    break;
                }
            }
        }


        $approve_permission = Sales_slip_model::get_seft_invoices();

        if($statusexsitRelease == 0){
            $this->db->where('ss.sales_slip_status <>',29);
        }

        if (isset($form_data['sales_slip_status']) && $form_data['sales_slip_status']) {
            $this->db->where_in('ss.sales_slip_status', $form_data['sales_slip_status']);
        } else {
            $this->db->where_in('ss.sales_slip_status', array(SALES_STATUS_INVOICE_APPROVAL_PENDING, SALES_STATUS_INVOICE_APPROVED, SALES_STATUS_INVOICE_ISSUED, SALES_STATUS_INVOICED_DISCARDED));
        }

        if (isset($form_data['j_account_business_unit_id']) && $form_data['j_account_business_unit_id'] && $form_data['j_account_business_unit_id'] != -1) {
//#7616:start

//             $this->db->join('department d', 'd.id = a.department_id')
            $this->db->where('d.business_unit_id', $form_data['j_account_business_unit_id']);
//#7616:End

        }

        //print_r( $form_data);

        if (isset($form_data['j_account_id']) && $form_data['j_account_id'] && $form_data['j_account_id'] != -1) {

            $this->db->where('o.j_account_id', $form_data['j_account_id']);
        }

        if (isset($form_data['sales_slip_number']) && $form_data['sales_slip_number']) {
            $this->db->where('ss.sales_slip_number', $form_data['sales_slip_number']);
        }

        if (isset($form_data['client_name']) && $form_data['client_name']) {
            $this->db->where('ss.client_name like ','%'.$form_data['client_name'].'%');
        }



        if (!empty($form_data['from_billing_date']) && empty($form_data['to_billing_date'])) {


            $from_billing_date = str_replace('/', '-', $form_data['from_billing_date']);
            list($from_year, $from_month, $from_day) = array_pad(explode('-', $from_billing_date), 3, null);

            if ($from_year) {
                if (strlen($from_year) == 2) {
                    $from_year = date('Y', strtotime($from_year . '-01-01'));
                }

                $this->db->having('YEAR(billing_date)', $from_year);
            }
            if ($from_month)
                $this->db->having('MONTH(billing_date)', $from_month);
            if ($from_day)
                $this->db->having('DAY(billing_date)', $from_day);
        } elseif (!empty($form_data['from_billing_date']) && !empty($form_data['from_billing_date'])) {
            $from_billing_date = str_replace('/', '-', $form_data['from_billing_date']);
            list($from_year, $from_month, $from_day) = array_pad(explode('-', $from_billing_date), 3, null);

            if ($from_day == "") {

                if ($from_year)
                    $this->db->having('YEAR(billing_date) >= ', $from_year);
                if ($from_month)
                    $this->db->having('MONTH(billing_date) >= ', $from_month);
            }
            else {
                $this->db->having('billing_date >= ', $from_billing_date);
            }

            $to_billing_date = str_replace('/', '-', $form_data['to_billing_date']);

            list($to_year, $to_month, $to_day) = array_pad(explode('-', $to_billing_date), 3, null);

            if ($to_day == "") {

                if ($to_year) {
                    if (strlen($to_year) == 2) {
                        $to_year = date('Y', strtotime($to_year . '-01-01'));
                    }

                    $this->db->having('YEAR(billing_date) <= ', $to_year);
                }
                if ($to_month)
                    $this->db->having('MONTH(billing_date) <= ', $to_month);
            }
            else {
                $this->db->having('billing_date <= ', $to_billing_date);
            }
        }
        if (!empty($form_data['from_payment_date']) && empty($form_data['to_payment_date'])) {
            $from_payment_date = str_replace('/', '-', $form_data['from_payment_date']);
            list($from_year, $from_month, $from_day) = array_pad(explode('-', $from_payment_date), 3, null);

            if ($from_year) {
                if (strlen($from_year) == 2) {
                    $from_year = date('Y', strtotime($from_year . '-01-01'));
                }

                $this->db->having('YEAR(payment_date)', $from_year);
            }

            if ($from_month) $this->db->having('MONTH(payment_date)', $from_month);
            if ($from_day) $this->db->having('DAY(payment_date)', $from_day);
        } elseif (!empty($form_data['from_payment_date']) && !empty($form_data['to_payment_date'])) {


            $from_payment_date = str_replace('/', '-', $form_data['from_payment_date']);
            list($from_year, $from_month, $from_day) = array_pad(explode('-', $from_payment_date), 3, null);

            if ($from_day == "") {
                if ($from_year) {
                    if (strlen($from_year) == 2) {
                        $from_year = date('Y', strtotime($from_year . '-01-01'));
                    }

                    $this->db->having('YEAR(payment_date) >=', $from_year);
                }

                if ($from_month) {
                    $this->db->having('MONTH(payment_date) >=', $from_month);
                }
            }
            else {
                $this->db->having('payment_date >=', $from_payment_date);
            }



            $to_payment_date = str_replace('/', '-', $form_data['to_payment_date']);
            list($to_year, $to_month, $to_day) = array_pad(explode('-', $to_payment_date), 3, null);

            if($to_day == ""){

                if ($to_year) {
                    if (strlen($to_year) == 2) {
                        $to_year = date('Y', strtotime($to_year . '-01-01'));
                    }

                    $this->db->having('YEAR(payment_date) <=', $to_year);
                }
                if ($to_month) $this->db->having('MONTH(payment_date) <=', $to_month);

            }
            else{
                $this->db->having('payment_date  <=', $to_payment_date);
            }

        }

        if (isset($form_data['s_code']) && $form_data['s_code']) {
            $this->db->like('c.s_code', $form_data['s_code'], 'after');
        }

        if (isset($form_data['name_kana']) && $form_data['name_kana']) {
            $this->db->like('c.name_kana', $form_data['name_kana']);
        }

        if (isset($form_data['division_name']) && $form_data['division_name']) {
            $this->db->like('c.division_name', $form_data['division_name']);
        }

        if (isset($form_data['s_account_id']) && $form_data['s_account_id'] && $form_data['s_account_id'] != -1) {
            $this->db->group_start()
            ->where('c.s_account_id_1st', $form_data['s_account_id'])
            ->or_where('c.s_account_id_2nd', $form_data['s_account_id'])
            ->group_end();
        }
//#7434:Start
		if(isset($form_data['s_account_id_all'])  && $form_data['s_account_id_all']&&( $form_data['s_account_id'] == -1)){

		       		$s_account_id_all = $form_data['s_account_id_all'];
		       		if(count($s_account_id_all) > 0){
		       		$sql_in = "c.s_account_id_1st in (";
		       		$sql_in2 = "c.s_account_id_2nd in (";
		       		for($i = 0 ; $i < count($s_account_id_all); $i++){
		       			$i_last = count($s_account_id_all) -1;
		       			if($i == $i_last){
		       				$sql_in .= $s_account_id_all[$i]['id'];
		       				$sql_in2 .= $s_account_id_all[$i]['id'];
		       			}
		       			else{
		       				$sql_in .= $s_account_id_all[$i]['id'].",";
		       				$sql_in2 .= $s_account_id_all[$i]['id'].",";
		       			}

		       		}
		       		 $sql_in .=")";
		       		 $sql_in2 .=")";
		       		$this->db->group_start()
		       		->where($sql_in)
		       		->or_where($sql_in2)
		       		->group_end();
		       		}
		}
//#7434:End
        $this->db->group_by('ss.id');
        $this->db->having("( NOT (is_ad_receive_payment=1      AND is_have_parent = 1 and is_have_parent_condition = 0)
                        AND NOT (is_ad_receive_payment=1  AND  is_have_parent = 0 AND is_self_condition = 0 )
                        AND NOT (is_ad_receive_payment=0  AND  is_self_condition = 0 ))  ","",false);
        $data_return = $this->db->get()->result_array();
        return $data_return;
    }
    /**
     * validate user input data in search invoice form
     * @param  array $form_data
     * @return array
     * @author  cam_tien
     */
    public function run_search_invoice_validate($form_data) {
        $form_data = array_map_trim($form_data);
        $error_messages = array();
        if (isset($form_data['from_billing_date']) && $form_data['from_billing_date'] && !parse_date($form_data['from_billing_date'], true)) {
            $error_messages['billing_date'] = lang('error_billing_date_invalid');

            if (isset($form_data['to_billing_date']) && !$this->_check_value_date_two_character($form_data['from_billing_date'], $form_data['to_billing_date'])) {
                $error_messages['billing_date'] = lang('error_delivery_date_from_bigger_to');
            }
        }

        if (isset($form_data['to_billing_date']) && $form_data['to_billing_date'] && !parse_date($form_data['to_billing_date'], true)) {
            $error_messages['billing_date'] = lang('error_billing_date_invalid');
        }

        if (isset($form_data['to_billing_date']) && $form_data['to_billing_date'] && !$form_data['from_billing_date']) {
            $error_messages['billing_date'] = lang('error_incorrect_search_range');
        }

//#7457:Start
        if(isset($form_data['to_billing_date']) && $form_data['to_billing_date'] && isset($form_data['from_billing_date']) && $form_data['from_billing_date']){
      		$checkpaymentday  = $this->_check_value_date_two_character($form_data['from_billing_date'], $form_data['to_billing_date']);
      		if(!$checkpaymentday){
      			$error_messages['billing_date'] =  lang('error_incorrect_search_range');
      		}

        }
//#7457:End

        if (isset($form_data['from_payment_date']) && $form_data['from_payment_date'] && !parse_date($form_data['from_payment_date'], true)) {
            $error_messages['payment_date'] = lang('error_payment_date_invalid');

            if (isset($form_data['to_payment_date']) && !$this->_check_value_date_two_character($form_data['from_payment_date'], $form_data['to_payment_date'])) {
                $error_messages['payment_date'] = lang('error_delivery_date_from_bigger_to');
            }
        }

        if (isset($form_data['to_payment_date']) && $form_data['to_payment_date'] && !parse_date($form_data['to_payment_date'], true)) {
            $error_messages['payment_date'] = lang('error_payment_date_invalid');
        }

        if (isset($form_data['to_payment_date']) && $form_data['to_payment_date'] && !$form_data['from_payment_date']) {
            $error_messages['payment_date'] = lang('error_incorrect_search_range');
        }

//#7457:Start
        if(isset($form_data['to_payment_date']) && $form_data['to_payment_date'] && isset($form_data['from_payment_date']) && $form_data['from_payment_date']){
        	$checkpaymentday  = $this->_check_value_date_two_character($form_data['from_payment_date'], $form_data['to_payment_date']);
        	if(!$checkpaymentday){
        		$error_messages['payment_date'] =  lang('error_incorrect_search_range');
        	}

        }
//#7457:End

        if (isset($form_data['delivery_date_from'])) {
            if ($form_data['delivery_date_from'] && !parse_date($form_data['delivery_date_from'], true)) {
                $error_messages['delivery_date'] = lang('error_delivery_date_invalid');
            }

            if (isset($form_data['delivery_date_to']) && !$this->_check_value_date_two_character($form_data['delivery_date_from'], $form_data['delivery_date_to'])) {
                $error_messages['delivery_date'] = lang('error_delivery_date_from_bigger_to');
            }
        }

        if (isset($form_data['delivery_date_to']) && $form_data['delivery_date_to'] && !parse_date($form_data['delivery_date_to'], true)) {
            $error_messages['delivery_date'] = lang('error_delivery_date_invalid');
        }

        if (isset($form_data['delivery_date_to']) && $form_data['delivery_date_to'] && !$form_data['delivery_date_from']) {
            $error_messages['delivery_date'] = lang('error_incorrect_search_range');
        }

        return $error_messages;
    }

    private function _check_value_date_two_character($date_from_input,$date_to_input){
        if (!empty($date_from_input) && !empty($date_to_input)) {
            $date_from = parse_date($date_from_input, true);
            $date_to = parse_date($date_to_input, true);

            if (count(explode('-', $date_from)) == 2) {
                $date_from .= '-01';
            }

            if (count(explode('-', $date_to)) == 2) {
                $to_year = date('Y', strtotime($date_to . '-01'));
                $to_month = date('m', strtotime($date_to . '-01'));
                $end_day = cal_days_in_month(CAL_GREGORIAN, $to_month, $to_year);
                $date_to .= '-' . $end_day;
            }

            if (strtotime($date_from) > strtotime($date_to)) {
                 return false;
            }
        }
        return true;
    }

    /**
     * get sales slip data for print invoice report
     * @param  string $sales_slip_ids
     * @return array
     * @author  cam_tien
     */
    public function get_data_for_print($sales_slip_ids) {
        $this->db->select('
                      ss.id
        			,ss.client_id
                    , ss.sales_slip_number
                    , ss.sales_slip_status
                    , ss.fax_no as sales_slip_fax_no
                    , ss.phone_no as sales_slip_phone_no
                    , ss.client_name
                    , ss.zipcode
                    , ss.address_1st
                    , ss.address_2nd
                    , ss.total_amount
                    , ss.total_tax
                    , ss.total_amount_tax
        			, ss.division_name
        			, ss.charge_name
                    , c.s_code
                    , ssd.is_ad_receive_payment
                    , ss.billing_date
                    , ss.payment_date'.
//#9759:start
                ' , ss.invoice_approval_account_date'.
//#9759:End
                    ', d.fax_no as department_fax_no
                    , d.phone_no as department_phone_no
                    , d.zipcode as department_zipcode
                    , d.address_1 as department_address_1
                    , d.address_2 as department_address_2
                    , a.name as account_name
                    , DATE_FORMAT(ssd.delivery_date, "%Y-%m-01") AS delivery_date')
                ->from('sales_slip ss')
                ->join('sales_slip_detail ssd', 'ssd.sales_slip_id = ss.id')
                ->join('order o', 'o.j_code = ssd.j_code')
                ->join('order_detail od', 'od.order_id = o.id')
                ->join('client c', 'c.id = ss.client_id')
                ->join('account a', 'a.id = o.j_account_id')
                ->join('department d', 'd.id = a.department_id')
                ->where('ss.disable', 0)
                ->where('ssd.disable', 0)
                ->where('o.disable', 0)
                ->where('od.disable', 0)
                ->where('c.disable', 0)
                ->where('a.disable', 0)
//#8266:Start
//                 ->where('d.disable', 0)
//#8266:End
               ->where_in('ss.id', $sales_slip_ids, false)
                ->group_by('ss.id');
		return $this->db->get()->result_array();

    }

    /**
     * get sales slip info use in invoice's screen
     * @param  int $id
     * @return array
     * @author  cam_tien
     */
    public function get_by_sales_slip_id($id) {
//#9700:Start
        $this->db->select('ss.client_id');
//#9700:End
        return $this->db->select('
                          ss.id
                        , ss.sales_slip_number
                        , ss.sales_slip_status
                        , ss.charge_name
                        , ss.address_1st
                        , ss.address_2nd
                        , ss.zipcode
                        , ss.phone_no
                        , ss.fax_no
                        , ss.client_name
                        , ss.total_amount
                        , ss.total_tax
                        , ss.total_amount_tax
                        , a.name as account_name
                        , ss.division_name
                        , c.s_code
                        , ssd.is_ad_receive_payment
                        , ss.billing_date
                        , ss.payment_date
                        , s_a1.name as s_account_name_1st
                        , s_a2.name as s_account_name_2nd
                        , DATE_FORMAT(ssd.delivery_date, "%Y-%m-01") AS delivery_date
                        ')
                        ->from('sales_slip ss')
                        ->join('sales_slip_detail ssd', 'ssd.sales_slip_id = ss.id')
                        ->join('order o', 'o.j_code = ssd.j_code')
                        ->join('order_detail od', 'od.order_id = o.id')
                        ->join('client c', 'c.id = ss.client_id')
                        ->join('account a', 'a.id = o.j_account_id')
                        ->join('account s_a1', 's_a1.id = c.s_account_id_1st and s_a1.disable = 0', 'LEFT')
                        ->join('account s_a2', 's_a2.id = c.s_account_id_2nd and s_a2.disable = 0', 'LEFT')
                        ->where('ss.id', $id)
                        ->where('ss.disable', 0)
                        ->where('o.disable', 0)
                        ->where('od.disable', 0)
                        ->where('c.disable', 0)
                        ->where('a.disable', 0)
                        ->where('a.resignation', 0)
                        ->group_by('ss.id')
                        ->get()
                        ->row_array();

    }


public function get_sales_slip_info($sales_slip_id) {
        return $this->db->select('
                          ss.id
                        , ss.sales_slip_number
                        , ss.sales_slip_status
                        , ss.total_amount
                        , ss.total_tax
                        , ss.total_amount_tax
                        , ss.invoice_approval_account_date
                        , ss.billing_date
                        , ss.payment_date
                        , a.name as j_account_name
                        , ac.name as s_account_name
                        , c.s_code
                        , ss.zipcode
                        , ss.address_1st
                        , ss.address_2nd
                        , ss.client_name
                        , ss.division_name
                        , o.id as order_id
                        , ss.charge_name
                        , o.j_account_id
                        , o.client_id
                        , ss.phone_no
                        , ss.fax_no
                        , c.closing_date
                        , c.payment_month_cd
                        , c.payment_day_cd
                        , c.delivery_month_cd
                        ')
                            ->from('sales_slip ss')
                            ->join('sales_slip_detail ssd', 'ssd.sales_slip_id = ss.id')
                            ->join('order o', 'o.j_code = ssd.j_code')
                            ->join('client c', 'c.id = ss.client_id')
                            ->join('account a', 'a.id = o.j_account_id')
                            ->join('account ac', 'ac.id = c.s_account_id_1st')
                            ->where('ss.id', $sales_slip_id)
                            ->where('ss.disable', 0)
                            ->where('o.disable', 0)
                            ->where('c.disable', 0)
                            ->where('a.disable', 0)
                            ->where('a.resignation', 0)
                            ->get()
                            ->row_array();
    }

    public function get_sales_slip_by_id($sales_slip_id) {
        return $this->db->select('*')
        ->from('sales_slip')
        ->where('id', $sales_slip_id)
        ->where('disable', 0)
        ->get()
        ->row_array();
    }

    /**
     * Get data for exporting pfd of sales slip features
     * @param int or array $sales_slip_id
     * @author van_don
     */
    public function get_sales_slip_export_pdf($sales_slip_id, $fix_sales_slip_id = '') {
//#7073:Start
//#7045:Start
        $this->db->select('
                             sld.id
                            ,sld.j_code
                            ,o.j_account_id
                            ,o.is_ad_receive_payment
                            ,sld.is_ad_receive_payment as is_ad_receive_payment_sales
                            ,sld.ad_receive_sales_slip_detail_id
                            ,sld.branch_cd
                            ,sld.delivery_date
                            ,sld.product_name
                            ,sld.contents
                            ,sld.tax_type
                            ,sld.quantity
                            ,sld.unit_price
                            ,sld.amount
                            ,sld.sequence')
                                ->from('sales_slip_detail sld')
                                ->join('order o', 'o.j_code = sld.j_code')
                                ->where('sld.disable', 0)
                                ->where('o.disable', 0)
                                ->order_by('sld.sequence ASC');
//#7045:End
//#7073:End
        //case fix payment
        if ($fix_sales_slip_id) {
            $this->db->where_in('sld.ad_receive_sales_slip_detail_id', $fix_sales_slip_id);
            $this->db->where('sld.is_ad_receive_payment', 1);
        //Case already get payment
        } else {
            $this->db->where('sld.sales_slip_id', $sales_slip_id);
        }
        $this->db->distinct();
        return $this->db->get()->result_array();
    }

    public function get_sales_slip_detail_full($sales_slip_id) {
        return $this->db->from('sales_slip_detail sld')
                                ->where('sld.sales_slip_id', $sales_slip_id)
                                ->where('sld.disable', 0)
                                ->get()
                                ->result_array();
    }

    public function get_order_acceptance_info($order_id) {
        $this->db->select('*')
            ->from('order_acceptance')
            ->where('disable', 0)
            ->where('order_id', $order_id)
            ->where('order_acceptance_status', 2)
            ->order_by('id DESC');
            return $this->db->get()->row();
    }

    /**
     * only update invoice status to sales_slip.sales_slip_status
     * @param  string $ids
     * @param  int $status
     * @param  string $comment
     * @return boolen
     * @author  cam_tien
     */
    public function update_invoice_status($ids, $status, $comment = '',$modify = 0,$account_login = 0) {
        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }
        // print_r($ids);
        switch ($status) {
            case SALES_STATUS_INVOICED_DISCARDED:
                $this->db->where_in('ss.id', $ids);
                $data = array('ss.sales_slip_status' => SALES_STATUS_INVOICED_DISCARDED);
                break;

            case SALES_STATUS_INVOICE_ISSUED:
                $data =  array(
                    'ss.sales_slip_status'             => SALES_STATUS_INVOICE_ISSUED,
                    'ss.invoice_approval_account_id'   => $this->auth->get_account_id(),
                    'ss.invoice_approval_account_date' => date('Y-m-d'),
                );
                $this->db->where_in('ss.id', $ids);
                break;

            case SALES_STATUS_INVOICE_APPROVED:
                $data =  array(
                    'ss.comment'                       => $comment,
                    'ss.sales_slip_status'             => SALES_STATUS_INVOICE_APPROVED,
                    'ss.invoice_approval_account_id'   => $this->auth->get_account_id(),
                    'ss.invoice_approval_account_date' => date('Y-m-d'),
                );
                $this->db->where_in('ss.id', $ids);
                break;

            case SALES_STATUS_CREATED:
                $data =  array(
                    'ss.comment'                       => $comment,
                    'ss.sales_slip_status'             => SALES_STATUS_CREATED,
                    'ss.invoice_approval_account_id'   => '0',
                    'ss.invoice_approval_account_date' => '0000-00-00',
                );

                $this->db->where_in('ss.id', $ids);
                break;
            default:
                # code...
                break;
        }
        if($modify == 1){
            $data['ss.lastup_datetime'] = $this->_db_now();
            if($account_login != 0){
                $data['ss.lastup_account_id'] = $account_login;
            }
        }

      //  print_r($data);
       return $this->db->update('sales_slip as ss', $data);
    }

    public function rules($rule_name, $params = null)
    {
        $rule = array(
            'change' => array(
                array(
                    'field' => 'delivery_date',
                    'label' => lang('lbl_delivery_date'),
                    'rules' => 'trim|required|valid_full_date'
                ),
                array(
                    'field' => 'product_name',
                    'label' => lang('lbl_product'),
                    'rules' => 'trim|required|max_length[30]',
                    'errors' => array(
                        'max_length' => strtr(lang('msg_str_max_length'), array('%field%' => lang('lbl_product'), '%length%' => '30')),
                    )
                ),
                array(
                    'field' => 'contents',
                    'label' => lang('lbl_contents'),
                    'rules' => 'trim|required|max_length[30]',
                    'errors' => array(
                        'max_length' => strtr(lang('msg_str_max_length'), array('%field%' => lang('lbl_contents'), '%length%' => '30')),
                    )
                ),
                array(
                    'field' => 'amount',
                    'label' => lang('lbl_total'),
                    'rules' => 'trim|required|numeric|less_than[1000000000]|greater_than[-1000000000]',
                    'errors' => array(
                        'less_than' => strtr(lang('msg_num_max_length'), array('%field%' => lang('lbl_total'), '%length%' => '10')),
                        'greater_than' => strtr(lang('msg_num_max_length'), array('%field%' => lang('lbl_total'), '%length%' => '10'))
                    )
                ),
//#10119:Start
                array(
                    'field' => 'tax_type',
                    'label' => lang('lbl_tax'),
                    'rules' => 'required|in_list[' . implode(',', array_keys($this->tax_options())) . ']',
                    'errors' => array(
                        'in_list' => lang('msg_tax_type_invalid')
                    )
                ),
//#10119:End
            ),
            'create_sales_slip' => array(
                array(
                    'field' => 'zipcode',
                    'label' => lang('lbl_zipcode'),
                    'rules' => 'trim|required|validate_zipcode_department|min_length[7]|max_length[7]',
                    'errors' => array(
                        'min_length' => lang('common_msg_digit_not_enough'),
                        'max_length' => lang('common_msg_enter_7_digit_')
                    )
                ),
                array(
                    'field' => 'address_1',
                    'label' => lang('lbl_address_1'),
//#8537:Start
//                    'rules' => 'trim|required'
                    'rules' => 'trim|required|only_2byte'

                ),
               array(
                   'field' => 'address_2',
                   'label' => lang('lbl_address_1'),
                   'rules' => 'only_2byte'
               ),
               array(
                   'field' => 'department',
                   'label' => lang('lbl_department'),
                   'rules' => 'only_2byte'
               ),
                array(
                    'field' => 'customer',
                    'label' => lang('lbl_customer'),
//                    'rules' => 'trim|required'
                    'rules' => 'trim|required|only_2byte'
//#8537:End
                ),
                array(
                    'field' => 'tel',
                    'label' => lang('lbl_tel'),
                    'rules' => 'trim|required|phone|max_length[16]',
                    'errors' => array(
                        'phone' => str_replace('%field%', lang('lbl_tel'), lang('common_msg_enter_hyphen_and_num'))
                    )
                ),
                array(
                    'field' => 'fax',
                    'label' => lang('lbl_fax'),
                    'rules' => 'trim|phone|max_length[16]',
                    'errors' => array(
                        'phone' => str_replace('%field%', lang('lbl_fax'), lang('common_msg_enter_hyphen_and_num'))
                    )
                ),
            ),
            'update_sales_slip' => array(
                array(
                    'field' => 'zipcode',
                    'label' => lang('lbl_zipcode'),
                    'rules' => 'trim|required|validate_zipcode_department|min_length[7]|max_length[7]',
                    'errors' => array(
                        'min_length' => lang('common_msg_digit_not_enough'),
                        'max_length' => lang('common_msg_enter_7_digit_')
                    )
                ),
//#8537:Start
                array(
                    'field' => 'address_1',
                    'label' => lang('lbl_address_1'),
//                    'rules' => 'trim|required'
                    'rules' => 'trim|required|only_2byte'
                ),
               array(
                   'field' => 'address_2',
                   'label' => lang('lbl_address_1'),
                   'rules' => 'only_2byte'
               ),
               array(
                   'field' => 'department',
                   'label' => lang('lbl_department'),
                   'rules' => 'only_2byte'
               ),
                array(
                    'field' => 'customer',
                    'label' => lang('lbl_customer'),
//                    'rules' => 'trim|required'
                    'rules' => 'trim|required|only_2byte'
                ),
//#8537:End
                array(
                    'field' => 'tel',
                    'label' => lang('lbl_tel'),
                    'rules' => 'trim|required|phone|max_length[16]',
                    'errors' => array(
                        'phone' => str_replace('%field%', lang('lbl_tel'), lang('common_msg_enter_hyphen_and_num'))
                    )
                ),
                array(
                    'field' => 'fax',
                    'label' => lang('lbl_fax'),
                    'rules' => 'trim|phone|max_length[16]',
                    'errors' => array(
                        'phone' => str_replace('%field%', lang('lbl_fax'), lang('common_msg_enter_hyphen_and_num'))
                    )
                ),
            ),
            'update_sales_slip_detail' => array(
                array(
                    'field' => 'product_name',
                    'label' => lang('lbl_product'),
                    'rules' => 'trim|required|max_length[30]'
                ),
                array(
                    'field' => 'contents',
                    'label' => lang('lbl_contents'),
                    'rules' => 'trim|required|max_length[30]'
                ),
            ),
        );

        switch ($rule_name) {
            case 'change':
                if (is_array($params)) {
                    if ($params['is_ad_receive_payment'] == 1) {
                        $rule[$rule_name][] = array(
                            'field' => 'unit_price',
                            'label' => lang('lbl_unit_price'),
                            'rules' => 'trim|required|numeric|greater_than_equal_to[0]|less_than[1000000000]',
                            'errors' => array(
                                'greater_than_equal_to' => lang('msg_positive_number'),
                                'less_than' => strtr(lang('msg_num_max_length'), array('%field%' => lang('lbl_unit_price'), '%length%' => '10'))
                            )
                        );
                        $rule[$rule_name][] = array(
                            'field' => 'quantity',
                            'label' => lang('lbl_quantity'),
//                          'rules' => 'trim|required|numeric|is_natural|less_than[1000000]',
                            'rules' => 'trim|required|numeric|is_natural|less_than[10000000]',
                            'errors' => array(
                                'is_natural' => lang('msg_positive_number'),
                                'less_than' => strtr(lang('msg_num_max_length'), array('%field%' => lang('lbl_unit_price'), '%length%' => '7'))
                            )
                        );
                        break;
                    }
                }
                $rule[$rule_name][] = array(
                    'field' => 'unit_price',
                    'label' => lang('lbl_unit_price'),
                    'rules' => 'trim|required|numeric|less_than[1000000000]|greater_than[-1000000000]',
                    'errors' => array(
                        'less_than' => strtr(lang('msg_num_max_length'), array('%field%' => lang('lbl_unit_price'), '%length%' => '10')),
                        'greater_than' => strtr(lang('msg_num_max_length'), array('%field%' => lang('lbl_unit_price'), '%length%' => '10'))
                    )
                );
                $rule[$rule_name][] = array(
                    'field' => 'quantity',
                    'label' => lang('lbl_quantity'),
//                  'rules' => 'trim|required|numeric|less_than[1000000]|greater_than[-1000000]',
                    'rules' => 'trim|required|numeric|less_than[10000000]|greater_than[-10000000]',
                    'errors' => array(
                        'less_than' => strtr(lang('msg_num_max_length'), array('%field%' => lang('lbl_quantity'), '%length%' => '7')),
                        'greater_than' => strtr(lang('msg_num_max_length'), array('%field%' => lang('lbl_quantity'), '%length%' => '7'))
                    )
                );
                break;
            case 'create_sales_slip':
            case 'update_sales_slip':
                if (is_array($params)) {
//#7620:Start
//                    if ($params[0]['is_ad_receive_payment'] == 1) {
                    if ($params[0]['order_is_ad_receive_payment'] == 1) {
//#7620:End
                        $rule[$rule_name][] = array(
                            'field' => 'billing_closing_date',
                            'label' => lang('lbl_bill_closing_date'),
                            'rules' => 'trim|required|valid_full_date'
                        );
                        $rule[$rule_name][] = array(
                            'field' => 'payment_expire_date',
                            'label' => lang('lbl_payment_expire_date'),
                            'rules' => 'trim|required|valid_full_date'
                        );
                    }
                }
            break;
        }

        return $rule[$rule_name];
    }

    public function validate_ssd($sales_slip_detail)
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules($this->rules('change', $sales_slip_detail));
        $this->form_validation->set_data($sales_slip_detail);
        $this->form_validation->set_error_delimiters('<span class="validation-error">', '</span>');

        if (!$this->form_validation->run()) {
            $error = $this->form_validation->error_array();
        } else {
            $error = array();
        }

        $this->form_validation->reset_validation();

        return $error;
    }

    /**
     * Get sales_slip by j_code
     * @param string|number $j_code
     * @return array
     * @author cong_tien
     */
    public function get_sales_slip_by_j_code_branch_cd($j_code, $branch_cd, $type = 1) {
        return $this->db->select('SUM(sales_slip_detail.amount) AS amount_total')
                        ->from('sales_slip_detail')
                        ->join('sales_slip', 'sales_slip.id = sales_slip_detail.sales_slip_id AND sales_slip.disable = 0')
                        ->where('sales_slip_detail.j_code', $j_code)
                        ->where('sales_slip_detail.branch_cd', $branch_cd)
                        ->where('sales_slip_detail.is_ad_receive_payment', $type)
                        ->where('sales_slip_detail.ad_receive_sales_slip_detail_id <>', 0)
                        ->where('sales_slip_detail.disable', 0)
                        ->get()
                        ->row_array();
    }

    function getallSaleSlipDetailBySalesSlipId($sales_slip_id) {
        return $this->db->from('sales_slip_detail sld')
                        ->where('sld.sales_slip_id', $sales_slip_id)
                        ->where('sld.disable', 0)
                        ->get()
                        ->result_array();
    }

    /**
     * Get sales_slip by j_code and ad_receive_sales_slip_detail_id
     * @param string|number $j_code
     * @param number $ad_receive
     * @return array
     * @author cong_tien
     */
    public function get_sales_slip_by_j_code_ad_receive($j_code, $ad_receive) {
        return $this->db->select('sales_slip_detail.id AS sales_slip_detail_id,
                            sales_slip.total_amount,
                            sales_slip_detail.ad_receive_sales_slip_detail_id')
                        ->from('sales_slip_detail')
                        ->join('sales_slip', 'sales_slip.id = sales_slip_detail.sales_slip_id AND sales_slip.disable = 0')
                        ->where('sales_slip_detail.j_code', $j_code)
                        ->where('sales_slip_detail.ad_receive_sales_slip_detail_id', $ad_receive)
                        ->where('sales_slip_detail.is_ad_receive_payment', 0)
                        ->where('sales_slip_detail.disable', 0)
                        ->get()
                        ->row_array();
    }

//#6930:Start

    /**
     * Get sale slip by sales slip number and is_receive_payment
     * @param  number $sale_slip_number
     * @param  int $is_receive_payment //
     * @return array $sale_slips
     */

    public function get_sales_slip_by_sales_slip_number_and_is_receive_payment($sales_slip_number = UNDEFINED, $is_receive_payment = UNDEFINED ){


    	$this->db->select('*')
    	         ->from('sales_slip ss');

    	if($sales_slip_number !=  UNDEFINED ){

    		$this->db->where('ss.sales_slip_number', $sales_slip_number );
    	}

    	if($is_receive_payment !=  UNDEFINED  ){
    		$this-> db ->join('sales_slip_detail ssd', 'ss.id = ssd.sales_slip_id AND ssd.disable = 0');
    		$this-> db ->where('ssd.is_ad_receive_payment', $is_receive_payment );
    	}


    	$this->db->where('ss.disable',0);
    	$this->db->where('ss.sales_slip_status <> ',SALES_STATUS_INVOICED_DISCARDED);
    	$this->db->group_by('ss.id');

    	return $this->db->get()->result_array();
    }

    /**
     * End
     **/

//#6930:End

    public function insert_sales_slip($sales_slip, $sales_slip_detail, $sales_slip_detail_sequence) {
        $this->load->library('form_validation');

//#7620:Start
        $sales_slip_detail[0]['order_is_ad_receive_payment'] = $sales_slip['is_ad_receive_payment'];
//#7620:End

        $this->form_validation->set_rules($this->rules('create_sales_slip', $sales_slip_detail));
        $this->form_validation->set_data($sales_slip);

        $error = array();

        if (!$this->form_validation->run()) {
            $error['sales_slip'] = $this->form_validation->error_array();
        }

        $this->form_validation->reset_validation();

        foreach ($sales_slip_detail as $key => $detail) {
            $this->form_validation->set_rules($this->rules('update_sales_slip_detail'));
            $this->form_validation->set_data($detail);

            if (!$this->form_validation->run()) {
                $error['sales_slip_detail'][$key] = $this->form_validation->error_array();
            }

            $this->form_validation->reset_validation();

//#7791:Start
            $sales_slip_detail[$key]['product_name'] = str_replace("\n", '', $sales_slip_detail[$key]['product_name']);
            $sales_slip_detail[$key]['contents'] = str_replace("\n", '', $sales_slip_detail[$key]['contents']);
//#7791:End
        }

        if ($sales_slip_detail[0]['is_ad_receive_payment'] == 1 && !(isset($error['sales_slip']['billing_closing_date']) || isset($error['sales_slip']['payment_expire_date']))) {
            $min_delivery_date = $sales_slip_detail[0]['delivery_date'];
            foreach ($sales_slip_detail as $ssd) {
                if ($ssd['delivery_date'] < $min_delivery_date) {
                    $min_delivery_date = $ssd['delivery_date'];
                }
            }

            if (!(strtotime($min_delivery_date) > strtotime($sales_slip['payment_expire_date']) && strtotime($sales_slip['payment_expire_date']) > strtotime($sales_slip['billing_closing_date']))) {
//#7620:Start
//                $error['sales_slip']['billing_payment_date'] = lang('msg_ss_add_edit_payment_billing_date_validate');
                $error['sales_slip']['billing_closing_date'] = lang('msg_ss_add_edit_payment_billing_date_validate');
                $error['sales_slip']['payment_expire_date'] = lang('msg_ss_add_edit_payment_billing_date_validate');
//#7620:End
            }
        }

        if ($error) {
            $this->form_validation->set_error_delimiters('<span class="validation-error">', '</span>');
            return $error;
        }

        try {
            $this->begin_transaction();

            // create slae slip number
            if ($sales_slip['is_ad_receive_payment']) {
                // check if exist old sales slip
                foreach ($sales_slip_detail as $key => $detail) {
                    if ($detail['ad_receive_sales_slip_detail_id'] != $detail['id']) {
                        if (!$parent_sales_slip_detail = $this->get_item_by_id('sales_slip_detail', $detail['ad_receive_sales_slip_detail_id'], '', array('sales_slip_id'))) {
//#6949:Start
                            //break;
                            continue;
//#6949:End
                        }

                        if (!$parent_sales_slip = $this->get_item_by_id('sales_slip', $parent_sales_slip_detail['sales_slip_id'], '', array('id', 'sales_slip_number', 'sales_slip_status'))) {
//#6949:Start
                            //break;
                            continue;
//#6949:End
                        }

                        if ($parent_sales_slip['sales_slip_status'] == SALES_STATUS_INVOICE_ISSUED) {
//#6949:Start
                            $query = 'SELECT sales_slip_id '
                                   . 'FROM sales_slip_detail '
                                   . 'JOIN sales_slip ON sales_slip_detail.sales_slip_id = sales_slip.id AND sales_slip.disable = 0 '
                                   . 'WHERE ad_receive_sales_slip_detail_id IN ('
                                   . '    SELECT ad_receive_sales_slip_detail_id'
                                   . '    FROM sales_slip_detail'
                                   . '    WHERE sales_slip_id = ?'
                                   . '    AND disable = 0'
                                   . ') '
                                   . 'AND sales_slip_id <> ? '
                                   . 'AND sales_slip_id <> 0 '
                                   . 'AND sales_slip.sales_slip_status <> ' . SALES_STATUS_INVOICED_DISCARDED . ' '
                                   . 'AND sales_slip_detail.disable = 0 '
                                   . 'LIMIT 1';

                            $list = array($parent_sales_slip['id'], $parent_sales_slip['id']);

                            if ($related_sales_slip_detail = $this->exec_query($query, $list)) {
                                $sales_slip_id = $related_sales_slip_detail[0]['sales_slip_id'];
                            } else {
                                $sales_slip_number = $parent_sales_slip['sales_slip_number'];
                            }

                            break;
//#6949:End
                        }
//#6949:Start
//                        break;
//#6949:End
                    }
                }
            }

            if (!isset($sales_slip_id)) {
                if (!isset($sales_slip_number)) {
                    $last_sales_slip = $this->get_items_by_parameters(
                        'sales_slip',
                        array('sales_slip_number'),
                        array('sales_slip_number LIKE "' . date('ym') . '%"', 'disable = 0'),
                        array('sales_slip_number DESC'),
                        array(1)
                    );

                    if ($last_sales_slip) {
                        $current_number = (int)substr($last_sales_slip[0]['sales_slip_number'], 4, 4);
                    } else {
                        $current_number = 0;
                    }

                    $sales_slip_number = date('ym') . str_pad(++$current_number, 4, '0', STR_PAD_LEFT);
                }

                $sales_slip_data = array(
                    'sales_slip_status' => SALES_STATUS_CREATED,
                    'sales_slip_number' => $sales_slip_number,
                    'client_id' => $sales_slip['client_id'],
                    'zipcode' => $sales_slip['zipcode'],
                    'address_1st' => $sales_slip['address_1'],
                    'address_2nd' => $sales_slip['address_2'],
                    'client_name' => $sales_slip['client_name'],
                    'division_name' => $sales_slip['department'],
                    'charge_name' => $sales_slip['customer'],
                    'phone_no' => $sales_slip['tel'],
                    'fax_no' => $sales_slip['fax'],
                    'billing_date' => $sales_slip['billing_closing_date'],
                    'payment_date' => $sales_slip['payment_expire_date'],
                    'comment' => '' // dummy data but has its purpose
                );

                $this->insert('sales_slip', $sales_slip_data);

                $sales_slip_id = $this->db->insert_id();
            }

//#7769:Start
            if ($sales_slip_detail[0]['order_is_ad_receive_payment']) {
//#7769:End
                $order_detail_data = array(
                    'billing_date' => $sales_slip['billing_closing_date'],
                    'payment_date' => $sales_slip['payment_expire_date'],
                );

                $order_id_branch_cd = get_element($sales_slip_detail, 'branch_cd', 'order_id');

                foreach ($order_id_branch_cd as $order_id => $branch_cds) {
                    $this->db->where('order_id', $order_id)
                        ->where_in('branch_cd', array_unique($branch_cds))
                        ->update('order_detail', $order_detail_data);
                }
            }

            $sales_slip_detail_data = array();
//            $sequence = 1;
            foreach ($sales_slip_detail as $detail) {
                // only add sales slip detail which is not belong to any sales slip
                if ($detail['sales_slip_id'] != 0) {
                    continue;
                }

//#7769:Start
                if ($detail['order_is_ad_receive_payment']) {
                    $sales_slip_detail_data[] = array(
                        'id' => $detail['id'],
                        'sales_slip_id' => $sales_slip_id,
                        'product_name' => $detail['product_name'],
                        'contents' => $detail['contents'],
//#8149:Start
//                        'sequence' => $sequence++,
//#8149:End
                        'billing_date' => $sales_slip['billing_closing_date'],
                        'payment_date' => $sales_slip['payment_expire_date']
                    );
                } else {
//#7769:End
                    $sales_slip_detail_data[] = array(
                        'id' => $detail['id'],
                        'sales_slip_id' => $sales_slip_id,
                        'product_name' => $detail['product_name'],
                        'contents' => $detail['contents'],
//#8149:Start
//                        'sequence' => $sequence++
//#8149:End
                    );
                }
            }

            if (!$sales_slip_detail_data) {
                throw new Exception(lang('common_could_not_add'));
            }

            $this->update_batch('sales_slip_detail', $sales_slip_detail_data, 'id');

//#8149:Start
            $this->update_batch('sales_slip_detail', $sales_slip_detail_sequence, 'id');
//#8149:End

            $this->_calculate_sales_slip_total_amount($sales_slip_id);

//#8260:Start
//            $j_code_branch_cd = $this->get_j_code_branch_cd_by_sales_slip_id($sales_slip_id);
//
//            if (!$this->update_order_flag($j_code_branch_cd)) {
//                throw new Exception('Error update order detail flags');
//            }
//
//            $this->load->model('order_detail_model');
//            if (!$this->order_detail_model->update_status_by_flag($j_code_branch_cd)) {
//                throw new Exception('Error update order detail status');
//            }
//#8260:End

            $this->commit_transaction();
        } catch (Exception $e) {
            $this->rollback_transaction();
            return array('common' => $e->getMessage());
        }

        return true;
    }

    public function get_sales_slip_detail_by_params($conditions) {

        $this->db->select('sales_slip_detail.*'
                        . ', order.id AS order_id, order.j_account_id, order.client_id, order.is_ad_receive_payment AS order_is_ad_receive_payment'
                        . ', order.zipcode, order.address_1st, order.address_2nd, order.division_name, order.charge_name, order.phone_no, order.fax_no')
//#7769:Start
//                        . ', order_detail.billing_date, order_detail.payment_date')
//#7769:End
                 ->from('sales_slip_detail')
                 ->join('order', 'order.j_code = sales_slip_detail.j_code AND order.disable = 0', 'left')
                 ->join('order_detail', 'order_detail.order_id = order.id AND order_detail.branch_cd = sales_slip_detail.branch_cd AND order_detail.disable = 0', 'left');

        foreach ($conditions as $key => $value) {
            $this->db->where($key, $value, false);
        }

        $this->db->where('sales_slip_detail.disable', STATUS_ENABLE)
                 ->order_by('sequence ASC, j_code ASC, branch_cd ASC, create_datetime ASC');

        return $this->db->get()->result_array();
    }

    public function update_sales_slip($sales_slip, $sales_slip_detail, $old_sales_slip_detail, $sales_slip_detail_sequence) {

        $this->load->library('form_validation');

//#7620:Start
        $sales_slip_detail[0]['order_is_ad_receive_payment'] = $sales_slip['is_ad_receive_payment'];
//#7620:End

        $this->form_validation->set_rules($this->rules('update_sales_slip', $sales_slip_detail));
        $this->form_validation->set_data($sales_slip);

        $error = array();

        if (!$this->form_validation->run()) {
            $error['sales_slip'] = $this->form_validation->error_array();
        }

        $this->form_validation->reset_validation();

        foreach ($sales_slip_detail as $key => $detail) {
            $this->form_validation->set_rules($this->rules('update_sales_slip_detail'));
            $this->form_validation->set_data($detail);

            if (!$this->form_validation->run()) {
                $error['sales_slip_detail'][$key] = $this->form_validation->error_array();
            }

            $this->form_validation->reset_validation();

//#7791:Start
            $sales_slip_detail[$key]['product_name'] = str_replace("\n", '', $sales_slip_detail[$key]['product_name']);
            $sales_slip_detail[$key]['contents'] = str_replace("\n", '', $sales_slip_detail[$key]['contents']);
//#7791:End

            $sales_slip_detail[$key]['order_is_ad_receive_payment'] = $sales_slip['is_ad_receive_payment'];
        }

        if ($sales_slip_detail[0]['is_ad_receive_payment'] == 1 && !(isset($error['sales_slip']['billing_closing_date']) || isset($error['sales_slip']['payment_expire_date']))) {
            $min_delivery_date = $sales_slip_detail[0]['delivery_date'];
            foreach ($sales_slip_detail as $ssd) {
                if ($ssd['delivery_date'] < $min_delivery_date) {
                    $min_delivery_date = $ssd['delivery_date'];
                }
            }

            if (!(strtotime($min_delivery_date) > strtotime($sales_slip['payment_expire_date']) && strtotime($sales_slip['payment_expire_date']) > strtotime($sales_slip['billing_closing_date']))) {
//#7620:Start
//                $error['sales_slip']['billing_payment_date'] = lang('msg_ss_add_edit_payment_billing_date_validate');
                $error['sales_slip']['billing_closing_date'] = lang('msg_ss_add_edit_payment_billing_date_validate');
                $error['sales_slip']['payment_expire_date'] = lang('msg_ss_add_edit_payment_billing_date_validate');
//#7620:End
            }
        }

        if ($error) {
            $this->form_validation->set_error_delimiters('<span class="validation-error">', '</span>');
            return $error;
        }

        try {
            $this->begin_transaction();

//#8260:Start
//            $j_code_branch_cd = $this->get_j_code_branch_cd_by_sales_slip_id($sales_slip['id']);
//#8260:End

            $sales_slip_data = array(
                'zipcode' => $sales_slip['zipcode'],
                'address_1st' => $sales_slip['address_1'],
                'address_2nd' => $sales_slip['address_2'],
                'division_name' => $sales_slip['department'],
                'charge_name' => $sales_slip['customer'],
                'phone_no' => $sales_slip['tel'],
                'fax_no' => $sales_slip['fax'],
                'billing_date' => $sales_slip['billing_closing_date'],
                'payment_date' => $sales_slip['payment_expire_date'],
            );

            $this->update('sales_slip', $sales_slip_data, $sales_slip['id']);

//#7769:Start
            if ($sales_slip_detail[0]['order_is_ad_receive_payment']) {
//#7769:End
                $order_detail_data = array(
                    'billing_date' => $sales_slip['billing_closing_date'],
                    'payment_date' => $sales_slip['payment_expire_date'],
                );

                $order_id_branch_cd = get_element($sales_slip_detail, 'branch_cd', 'order_id');

                foreach ($order_id_branch_cd as $order_id => $branch_cds) {
                    $this->db->where('order_id', $order_id)
                        ->where_in('branch_cd', array_unique($branch_cds))
                        ->update('order_detail', $order_detail_data);
                }
            }

            // this variable is used to check the sales slip detail exist and to remove detail from sales slip
            $old_sales_slip_detail_ids = get_element($old_sales_slip_detail, 'id');

            $sales_slip_detail_data = array();
//            $sequence = 1;
            foreach ($sales_slip_detail as $detail) {
                if (!in_array($detail['id'], $old_sales_slip_detail_ids)) {
                    continue;
                }

                $key = array_search($detail['id'], $old_sales_slip_detail_ids);
                unset($old_sales_slip_detail_ids[$key]);

//#7769:Start
                if ($detail['order_is_ad_receive_payment']) {
                    $sales_slip_detail_data[] = array(
                        'id' => $detail['id'],
                        'product_name' => $detail['product_name'],
                        'contents' => $detail['contents'],
//#8149:Start
//                        'sequence' => $sequence++,
//#8149:End
                        'billing_date' => $sales_slip['billing_closing_date'],
                        'payment_date' => $sales_slip['payment_expire_date']
                    );
                } else {
//#7769:End
                    $sales_slip_detail_data[] = array(
                        'id' => $detail['id'],
                        'product_name' => $detail['product_name'],
                        'contents' => $detail['contents'],
//#8149:Start
//                        'sequence' => $sequence++
//#8149:End
                    );
                }
            }

            if (!$sales_slip_detail_data) {
                throw new Exception(lang('common_could_not_edit'));
            }

            $this->update_batch('sales_slip_detail', $sales_slip_detail_data, 'id');

//#8149:Start
            $this->update_batch('sales_slip_detail', $sales_slip_detail_sequence, 'id');
//#8149:End

            if ($old_sales_slip_detail_ids) {
                $this->db->where_in('id', $old_sales_slip_detail_ids)
                         ->set('sales_slip_id', 0)
                         ->set('sequence', 0)
                         ->set('lastup_account_id', $this->auth->get_account_id())
                         ->set('lastup_datetime', 'NOW()', false)
                         ->update('sales_slip_detail');
            }

            $this->_calculate_sales_slip_total_amount($sales_slip['id']);

//#8260:Start
//            if (!$this->update_order_flag($j_code_branch_cd)) {
//                throw new Exception('Error update order detail flags');
//            }
//
//            $this->load->model('order_detail_model');
//            if (!$this->order_detail_model->update_status_by_flag($j_code_branch_cd)) {
//                throw new Exception('Error update order detail status');
//            }
//#8260:End

            $this->commit_transaction();
        } catch (Exception $e) {
            $this->rollback_transaction();
            return array('common' => $e->getMessage());
        }

        return true;
    }

    public function delete_sales_slip($sales_slip_id) {
        try {
            $this->begin_transaction();

//#8260:Start
//            $j_code_branch_cd = $this->get_j_code_branch_cd_by_sales_slip_id($sales_slip_id);
//#8260:End

            $this->delete('sales_slip', $sales_slip_id);

            $this->update(
                'sales_slip_detail',
                array(
                    'sales_slip_id' => 0,
                    'sequence' => 0,
                ),
                array(
                    'sales_slip_id' => $sales_slip_id
                )
            );

//#8260:Start
//            if (!$this->update_order_flag($j_code_branch_cd)) {
//                throw new Exception('Error update order detail flags');
//            }
//
//            $this->load->model('order_detail_model');
//            if (!$this->order_detail_model->update_status_by_flag($j_code_branch_cd)) {
//                throw new Exception('Error update order detail status');
//            }
//#8260:End

            $this->commit_transaction();
        } catch (Exception $e) {
            $this->rollback_transaction();
            return array('common' => $e->getMessage());
        }

        return true;
    }

    public function update_order_flag($order_input)
    {
        if (!is_array($order_input)) {
            log_message('error', __FILE__ . '(line ' . __LINE__ . '): $order_input is not an array');
            return false;
        }
        if (isset($order_input['j_code']) && isset($order_input['branch_cd'])) {
            $order_input = array(
                array(
                    'j_code'    => $order_input['j_code'],
                    'branch_cd' => $order_input['branch_cd']
                )
            );
        }

        $batch_order_data = array();
        foreach ($order_input as $input) {
            if (!isset($input['j_code']) || !isset($input['branch_cd'])) {
                log_message('error', __FILE__ . '(line ' . __LINE__ . '): $order_input format is invalid');
                return false;
            }

            $order = $this->db->select('od.id AS od_id, o.is_ad_receive_payment, od.is_ad_sales_slip_input, od.is_ad_invoice_issue, od.is_invoice_issue, od.is_sales_slip_input')
                ->from('order o')
                ->join('order_detail od', 'od.order_id = o.id AND od.disable = 0')
                ->where('j_code', $input['j_code'])
                ->where('branch_cd', $input['branch_cd'])
                ->where('o.disable', STATUS_ENABLE)
                ->get()->result_array();
            if (count($order) == 0) {
                log_message('error', __FILE__ . '(line ' . __LINE__ . '): j_code branch_cd ' . $input['j_code'] . $input['branch_cd'] . ' not found.');
                return false;
            }
            $order = $order[0];

            $update_order = array();

            if ($order['is_ad_receive_payment'] == 1) {
//#6949:Start
                $ssd_query_1 = $this->db->select('ssd.id')
                    ->from('sales_slip_detail ssd')
                    ->join('sales_slip ss', 'ssd.sales_slip_id = ss.id AND ss.disable = 0', 'left')
                    ->where('j_code', $input['j_code'])
                    ->where('branch_cd', $input['branch_cd'])
                    ->where('is_ad_receive_payment', 1)
                    ->group_start()
                    ->where('ss.sales_slip_status !=', SALES_STATUS_INVOICED_DISCARDED)
                    ->or_where('ss.sales_slip_status IS NULL')
                    ->group_end()
                    ->where('ssd.disable', STATUS_ENABLE)
                    ->get();
//#6949:End

                if ($ssd_query_1->num_rows() == 0) {
                    $is_ad_sales_slip_input = 0;
                    $is_ad_invoice_issue = 0;
                } else {
                    $ssd_query_2 = $this->db->select('ssd.id, ss.sales_slip_status')
                        ->from('sales_slip_detail ssd')
                        ->join('sales_slip ss', 'ssd.sales_slip_id = ss.id AND ss.disable = 0', 'left')
//#6949:Start
//                        ->join('order o', 'o.j_code = ssd.j_code AND o.disable = 0')
//                        ->join('order_detail od', 'od.order_id = o.id AND ssd.branch_cd = od.branch_cd AND od.disable = 0')
//#6949:End
                        ->where('ssd.j_code', $input['j_code'])
                        ->where('ssd.branch_cd', $input['branch_cd'])
                        ->where('ssd.is_ad_receive_payment', 1)
                        ->group_start()
                        ->where('ss.sales_slip_status <', SALES_STATUS_INVOICE_ISSUED)
                        ->or_where('ss.sales_slip_status IS NULL')
                        ->group_end()
                        ->where('ssd.disable', STATUS_ENABLE)
                        ->get();

                    if ($ssd_query_2->num_rows() == 0) {
                        $is_ad_sales_slip_input = 1;
                        $is_ad_invoice_issue = 1;
                    } else {
                        $is_ad_invoice_issue = 0;
                        $is_ad_sales_slip_input = 1;
                        foreach ($ssd_query_2->result_array() as $ssd_2) {
//#8260:Start
//                          if ($ssd_2['sales_slip_status'] < SALES_STATUS_CREATED) {
                            if ($ssd_2['sales_slip_status'] < SALES_STATUS_INVOICE_APPROVAL_PENDING) {
//#8260:End
                                $is_ad_sales_slip_input = 0;
                                break;
                            }
                        }
                    }
                }

                if ($is_ad_sales_slip_input != $order['is_ad_sales_slip_input']) {
                    $update_order['is_ad_sales_slip_input'] = $is_ad_sales_slip_input;
                }
                if ($is_ad_invoice_issue != $order['is_ad_invoice_issue']) {
                    $update_order['is_ad_invoice_issue'] = $is_ad_invoice_issue;
                }
            }

//#6949:Start
            $ssd_query_1 = $this->db->select('ssd.id')
                ->from('sales_slip_detail ssd')
                ->join('sales_slip ss', 'ssd.sales_slip_id = ss.id AND ss.disable = 0', 'left')
                ->where('j_code', $input['j_code'])
                ->where('branch_cd', $input['branch_cd'])
                ->where('is_ad_receive_payment', 0)
                ->group_start()
                ->where('ss.sales_slip_status !=', SALES_STATUS_INVOICED_DISCARDED)
                ->or_where('ss.sales_slip_status IS NULL')
                ->group_end()
                ->where('ssd.disable', STATUS_ENABLE)
                ->get();
//#6949:End

            if ($ssd_query_1->num_rows() == 0) {
                $is_sales_slip_input = 0;
                $is_invoice_issue = 0;
            } else {
                $ssd_query_2 = $this->db->select('ssd.id, ss.sales_slip_status')
                    ->from('sales_slip_detail ssd')
                    ->join('sales_slip ss', 'ssd.sales_slip_id = ss.id AND ss.disable = 0', 'left')
//#6949:Start
//                    ->join('order o', 'o.j_code = ssd.j_code AND o.disable = 0')
//                    ->join('order_detail od', 'od.order_id = o.id AND ssd.branch_cd = od.branch_cd AND od.disable = 0')
//#6949:End
                    ->where('ssd.j_code', $input['j_code'])
                    ->where('ssd.branch_cd', $input['branch_cd'])
                    ->where('ssd.is_ad_receive_payment', 0)
                    ->group_start()
                    ->where('ss.sales_slip_status <', SALES_STATUS_INVOICE_ISSUED)
                    ->or_where('ss.sales_slip_status IS NULL')
                    ->group_end()
                    ->where('ssd.disable', STATUS_ENABLE)
                    ->get();

                if ($ssd_query_2->num_rows() == 0) {
                    $is_sales_slip_input = 1;
                    $is_invoice_issue = 1;
                } else {
                    $is_invoice_issue = 0;
                    $is_sales_slip_input = 1;
                    foreach ($ssd_query_2->result_array() as $ssd_2) {
//#8260:Start
//                      if ($ssd_2['sales_slip_status'] < SALES_STATUS_CREATED) {
                        if ($ssd_2['sales_slip_status'] < SALES_STATUS_INVOICE_APPROVAL_PENDING) {
//#8260:End
                            $is_sales_slip_input = 0;
                            break;
                        }
                    }
                }

//#9692:Start When an adv amount status is 23, find if its fixed amount is created and is status 23? If it isn't, turn off sales_slip_input flag
                if ($is_sales_slip_input == 1) {
                    $count_ad_receive = $this->db->select('ssd.id')
                        ->from('sales_slip_detail ssd')
                        ->join('sales_slip ss', 'ssd.sales_slip_id = ss.id AND ss.disable = 0', 'left')
                        ->where('ssd.j_code', $input['j_code'])
                        ->where('ssd.branch_cd', $input['branch_cd'])
                        ->where('ssd.ad_receive_sales_slip_detail_id >', 0)
                        ->group_start()
                        ->where('ss.sales_slip_status >=', SALES_STATUS_INVOICE_APPROVAL_PENDING)
                        ->where('ss.sales_slip_status !=', SALES_STATUS_INVOICED_DISCARDED)
                        ->group_end()
                        ->where('ssd.disable', STATUS_ENABLE)
                        ->group_by('ad_receive_sales_slip_detail_id')
                        ->having('count(ssd.id) != 2')
                        ->get();
                    if ($count_ad_receive->num_rows() > 0) {
                        $is_sales_slip_input = 0;
                    }
                }
                if ($is_invoice_issue == 1) {
                    $count_ad_receive = $this->db->select('ssd.id')
                        ->from('sales_slip_detail ssd')
                        ->join('sales_slip ss', 'ssd.sales_slip_id = ss.id AND ss.disable = 0', 'left')
                        ->where('ssd.j_code', $input['j_code'])
                        ->where('ssd.branch_cd', $input['branch_cd'])
                        ->where('ssd.ad_receive_sales_slip_detail_id >', 0)
//                        ->group_start()
                        ->where('ss.sales_slip_status =', SALES_STATUS_INVOICE_ISSUED)
//                        ->where('ss.sales_slip_status !=', SALES_STATUS_INVOICED_DISCARDED)
//                        ->group_end()
                        ->where('ssd.disable', STATUS_ENABLE)
                        ->group_by('ad_receive_sales_slip_detail_id')
                        ->having('count(ssd.id) != 2')
                        ->get();
                    if ($count_ad_receive->num_rows() > 0) {
                        $is_invoice_issue = 0;
                    }
                }
//#9692:End
            }

            if ($is_sales_slip_input != $order['is_sales_slip_input']) {
                $update_order['is_sales_slip_input'] = $is_sales_slip_input;
            }
            if ($is_invoice_issue != $order['is_invoice_issue']) {
                $update_order['is_invoice_issue'] = $is_invoice_issue;
            }

//#8260:Start
            if ($order['is_ad_receive_payment'] == 1) {
                if ((isset($update_order['is_ad_sales_slip_input']) && $update_order['is_ad_sales_slip_input'] == 0) ||
                    (!isset($update_order['is_ad_sales_slip_input']) && $order['is_ad_sales_slip_input'] == 0)) {
                    $update_order['is_sales_slip_input'] = 0;
                }
                if ((isset($update_order['is_ad_invoice_issue']) && $update_order['is_ad_invoice_issue'] == 0) ||
                    (!isset($update_order['is_ad_invoice_issue']) && $order['is_ad_invoice_issue'] == 0)) {
                    $update_order['is_invoice_issue'] = 0;
                }
            }
//#8260:End

            if (!empty($update_order)) {
                $update_order['id'] = $order['od_id'];
                $update_order['lastup_account_id'] = $this->auth->get_account_id();
                $update_order['lastup_datetime'] = $this->_db_now();
                $batch_order_data[] = $update_order;
            }
        }

        if (!empty($batch_order_data)) {
            return $this->db->update_batch('order_detail', $batch_order_data, 'id');
        }

        return true;
    }

    public function get_j_code_branch_cd_by_sales_slip_id($sales_slip_id)
    {
        if (is_numeric($sales_slip_id)) {
            $sales_slip_id = array($sales_slip_id);
        }

        $result = array();
        foreach ($sales_slip_id as $id) {
            if (!is_numeric($id)) {
                log_message('error', __FILE__ . '(line ' . __LINE__ . '): Invalid sales slip id');
                return false;
            }

            $sales_slip_detail = $this->db->select('j_code, branch_cd')
                ->from('sales_slip_detail')
                ->where('sales_slip_id', $id)
                ->where('disable', STATUS_ENABLE)
                ->distinct()
                ->get()->result_array();

            if (count($sales_slip_detail) > 0) {
                $result = array_merge($result, $sales_slip_detail);
            }
        }

        return array_values(array_unique($result, SORT_REGULAR));
    }

    public function check_confirm_amount_included_all_adv_amount_by_sales_slip($sales_slip_ids)
    {
        if (is_numeric($sales_slip_ids)) {
            $sales_slip_ids = array($sales_slip_ids);
        }
        if (!is_array($sales_slip_ids)) {
            log_message('error', __FILE__ . '(line ' . __LINE__ . '): Invalid sales slip detail id');
            return false;
        } elseif (empty($sales_slip_ids)) {
            log_message('error', __FILE__ . '(line ' . __LINE__ . '): sales slip detail id is empty');
            return false;
        }

        $fail_sales_slip_number = array();

        foreach ($sales_slip_ids as $id) {
            $sales_slip_detail = $this->db->select('sales_slip_detail.id, sales_slip_number')
                ->from('sales_slip_detail')
                ->join('sales_slip', 'sales_slip_detail.sales_slip_id = sales_slip.id AND sales_slip.disable = 0')
                ->where('sales_slip_id', $id)
                ->where('sales_slip_detail.disable', STATUS_ENABLE)
                ->get()->result_array();

            if (!empty($sales_slip_detail)) {
                if (!$this->check_confirm_amount_included_all_adv_amount(get_element($sales_slip_detail, 'id'))) {
                    $fail_sales_slip_number[] = $sales_slip_detail[0]['sales_slip_number'];
                }
            }
        }

        return (empty($fail_sales_slip_number) ? true : $fail_sales_slip_number);
    }

    public function check_confirm_amount_included_all_adv_amount($sales_slip_detail_ids)
    {
        if (is_numeric($sales_slip_detail_ids)) {
            $sales_slip_detail_ids = array($sales_slip_detail_ids);
        }
        if (!is_array($sales_slip_detail_ids)) {
            log_message('error', __FILE__ . '(line ' . __LINE__ . '): Invalid sales slip detail id');
            return false;
        } elseif (empty($sales_slip_detail_ids)) {
            log_message('error', __FILE__ . '(line ' . __LINE__ . '): sales slip detail id is empty');
            return false;
        }

        $sales_slip_detail = $this->db->select('ad_receive_sales_slip_detail_id')
            ->from('sales_slip_detail')
            ->where_in('id', $sales_slip_detail_ids)
            ->where('is_ad_receive_payment', 0)
            ->where('ad_receive_sales_slip_detail_id !=', 0)
            ->where('disable', STATUS_ENABLE)
            ->get()->result_array();

        if (empty($sales_slip_detail)) {
            return true;
        }

        $ad_receive = get_element($sales_slip_detail, 'ad_receive_sales_slip_detail_id');

        $sales_slip_detail_adv = $this->db->select('sales_slip_id')
            ->from('sales_slip_detail')
            ->where_in('ad_receive_sales_slip_detail_id', $ad_receive)
            ->where('is_ad_receive_payment', 1)
            ->where('sales_slip_id !=', 0)
            ->where('disable', STATUS_ENABLE)
            ->get()->result_array();

        $sales_slip_id = get_element($sales_slip_detail_adv, 'sales_slip_id');
        $sales_slip_id_count = array_keys(array_flip($sales_slip_id));

        if (count($sales_slip_id_count) == 0) {
            return true;
        } elseif (count($sales_slip_id_count) > 1) {
            log_message('error', __FILE__ . '(line ' . __LINE__ . '): sales slip detail has adv amount belongs to more than 1 sales slip');
            return false;
        }

//#6949:Start
        $sales_slip_detail_conf = $this->db->select('ssd1.id AS adv_id, ssd2.id AS conf_id, conf_ss.sales_slip_status AS conf_status')
            ->from('sales_slip_detail ssd1')
            ->join('sales_slip_detail ssd2', 'ssd2.ad_receive_sales_slip_detail_id = ssd1.ad_receive_sales_slip_detail_id AND ssd2.is_ad_receive_payment = 0 AND ssd2.disable = 0', 'left')
            ->join('sales_slip conf_ss', 'ssd2.sales_slip_id = conf_ss.id', 'left')
            ->where('ssd1.sales_slip_id', $sales_slip_id[0])
            ->where('ssd1.disable', STATUS_ENABLE)
            ->get()->result_array();
//#6949:End

        foreach ($sales_slip_detail_conf as $ssd) {
//#6949:Start
            if ($ssd['conf_status'] == SALES_STATUS_INVOICED_DISCARDED) {
                continue;
            }
//#6949:End
            if (!$ssd['conf_id'] || !in_array($ssd['conf_id'], $sales_slip_detail_ids)) {
                return false;
            }
        }

        return true;
    }

    /*
     * Get Status Id by jcode and brand code
     */
    function  get_all_status_saleslip_bybrandcodejcode($jcode,$banchcode,$IsSaleslipid = 0,$IsstatusIssue = 0,$IsadreceivePayment = -1){

        $wheresql ="";
        if ($IsSaleslipid == 1){ // only search id = 0

            $wheresql = ' and ssd.sales_slip_id ='.STATUS_ENABLE;
        }
        if ($IsSaleslipid == 0){ // only search id = 0

            $wheresql = ' and ssd.sales_slip_id !='.STATUS_ENABLE;
        }
        if($IsstatusIssue == 1){
            $wheresql .= ' and  ss.sales_slip_status !='.SALES_STATUS_INVOICE_ISSUED;

//#7022:Started

            $wheresql .= " and ss.id NOT IN
            				 	(SELECT DISTINCT ss4.id
								FROM sales_slip as ss4
            					inner join sales_slip_detail ssd4 on ssd4.sales_slip_id = ss4.id
            					inner join sales_slip as ss5 on ss5.sales_slip_number = ss4.sales_slip_number
            					WHERE ss4.sales_slip_number = ss.sales_slip_number
            						  and ss4.sales_slip_status = ".SALES_STATUS_INVOICED_DISCARDED."
            						  and  ssd4.is_ad_receive_payment= ssd.is_ad_receive_payment )";

//#7022:End

        }
        if($IsstatusIssue == 2){
            $wheresql .= ' and  ss.sales_slip_status ='.SALES_STATUS_INVOICE_ISSUED;
        }
        if($IsadreceivePayment !=-1){
            $wheresql .= ' and ssd.is_ad_receive_payment = '.$IsadreceivePayment;
        }
        $data_return = $sales_slip_detail = $this->db->select('count(ssd.id) as n_ss')
        ->from('sales_slip as ss')
        ->join('sales_slip_detail as ssd', ' ss.id = ssd.sales_slip_id or ssd.sales_slip_id ='.STATUS_ENABLE)
        ->where('ssd.`disable`', STATUS_ENABLE)
        ->where('ss.`disable`',STATUS_ENABLE)
        ->where('ssd.j_code', $jcode)
        ->where('ssd.branch_cd ='. $banchcode .$wheresql,"",false )
        ->order_by("ssd.sales_slip_id","ASC")
        ->distinct()
        ->get()->result_array();
        return  $data_return;
    }


    function  get_all_status_saleslip_hidden_have_underfine($jcode,$banchcode){

        $wheresql ="";
                $data_return = $sales_slip_detail = $this->db->select('count(ssd.id) as n_ss')
                ->from('sales_slip_detail as ssd')
                ->where('ssd.`disable`', STATUS_ENABLE)
                ->where('ssd.j_code', $jcode)
                ->where('ssd.is_ad_receive_payment', 1)
                ->where('ssd.sales_slip_id', 0)
                ->where('ssd.branch_cd ='. $banchcode .$wheresql,"",false )
                ->order_by("ssd.sales_slip_id","ASC")
                ->distinct()
                ->get()->result_array();
        return  $data_return;
    }
    public function get_saslip_is_reivce_payment_dont_have_undefine($jcode,$banchcode){
        $wheresql ="";
        $sales_slip_detail =  $this->db->select('ss.sales_slip_number, count(ss.sales_slip_number) as count_sales_slip,ssd.is_ad_receive_payment')
                  ->from('sales_slip as ss')
                  ->join('sales_slip_detail as ssd', ' ss.id = ssd.sales_slip_id or ssd.sales_slip_id ='.STATUS_ENABLE)
                  ->join('order as o', ' o.j_code =  ssd.j_code')
                  ->where('ssd.`disable`', STATUS_ENABLE)
                  ->where('ss.`disable`',STATUS_ENABLE)
                  ->where('o.`disable`',STATUS_ENABLE)
                  ->where('ssd.j_code', $jcode)
                  ->where('o.is_ad_receive_payment', 1)
                  ->where('ssd.branch_cd ='. $banchcode .$wheresql,"",false )
                  ->group_by('ss.sales_slip_number')
                  ->having('count_sales_slip',1)
                   ->get()->result_array();
        return  $sales_slip_detail;

    }
//#6900:Start
    public function have_sales_slip_detail($order_id, $branch_cd, $is_cancel_request)
    {
        $sales_slip_detail = $this->db->select()
            ->from('sales_slip_detail ssd')
            ->join('order o', 'o.j_code = ssd.j_code AND o.disable = 0')
            ->join('sales_slip ss', 'ss.id = ssd.sales_slip_id AND ss.disable = 0', 'left')
            ->where('o.id', $order_id)
            ->where('ssd.branch_cd', $branch_cd)
//#6956:Start
//          ->where('(ss.sales_slip_status NOT IN (' . implode(',', array(SALES_STATUS_INVOICE_ISSUED, SALES_STATUS_INVOICED_DISCARDED)) . ') OR ss.id IS NULL)')
            ->where('(ss.sales_slip_status NOT IN (' . implode(',', array(SALES_STATUS_INVOICED_DISCARDED)) . ') OR ss.id IS NULL)');
//#9700:Start
            if (!$is_cancel_request) {
              $this->db->where('ssd.is_ad_receive_payment = 0');
            }
//#9700:End
//#6956:End
            $this->db->where('ssd.disable = 0');
            $sales_slip_detail = $this->db->get()->result_array();
        return !empty($sales_slip_detail);
    }
//#6900:End

//#7769:Start
//    public function update_payment_billing_date() {
//        $this->load->model('order_model');
//
//        $sales_slip_details = $this->db->select('ssd.id
//                                                , ssd.sales_slip_id
//                                                , ssd.j_code
//                                                , ssd.branch_cd
//                                                , ssd.delivery_date
//                                                , ss.payment_date as sales_slip_payment_date
//                                                , ss.billing_date as sales_slip_billing_date')
//                                        ->from('sales_slip_detail ssd')
//                                        ->join('sales_slip ss', 'ss.id = ssd.sales_slip_id AND ss.disable = 0', 'left')
//                                        ->where('ssd.disable', 0)
//                                        ->get()->result_array();
//
//        $update_datas        = array();
//        $list_j_code_branch  = array();
//        $no_sales_slip_datas = array();
//
//        foreach ($sales_slip_details as $detail) {
//            if ($detail['sales_slip_id'] != 0) {
//                $update_datas[] = array(
//                    'id'           => $detail['id'],
//                    'payment_date' => $detail['sales_slip_payment_date'],
//                    'billing_date' => $detail['sales_slip_billing_date'],
//                );
//            } else {
//                $list_j_code_branch[] = $detail['j_code'] . $detail['branch_cd'];
//                $no_sales_slip_datas[] = array(
//                    'id'            => $detail['id'],
//                    'j_code'        => $detail['j_code'],
//                    'branch_cd'     => $detail['branch_cd'],
//                    'delivery_date' => $detail['delivery_date'],
//                );
//            }
//        }
//
//        $order_details = $this->db->select('o.j_code
//                                          , od.branch_cd
//                                          , o.client_id
//                                          , o.is_ad_receive_payment
//                                          , od.payment_date
//                                          , od.billing_date')
//                                ->from('order_detail od')
//                                ->join('order o', 'o.id = od.order_id AND o.disable = 0')
//                                ->where('CONCAT(o.j_code, od.branch_cd) IN (' . implode(',', $list_j_code_branch) . ')')
//                                ->where('od.disable', 0)
//                                ->get()->result_array();
//
//        try {
//            foreach ($order_details as $detail) {
//                foreach ($no_sales_slip_datas as $data) {
//                    if ($data['j_code'] != $detail['j_code'] || $data['branch_cd'] != $detail['branch_cd']) continue;
//
//                    if ($detail['is_ad_receive_payment'] == 1) {
//                        $update_datas[] = array(
//                            'id'           => $data['id'],
//                            'payment_date' => $detail['payment_date'],
//                            'billing_date' => $detail['billing_date'],
//                        );
//                    } else {
//                        $payment_billing_date = $this->order_model->calculate_payment_billing_date($data['delivery_date'], $detail['client_id']);
//                        if (!$payment_billing_date) throw new Exception("Calculate payment_date, billing_date return false", 1);
//
//                        $update_datas[] = array(
//                            'id'           => $data['id'],
//                            'payment_date' => $payment_billing_date['payment_date'],
//                            'billing_date' => $payment_billing_date['billing_date'],
//                        );
//                    }
//                }
//            }
//
//            $this->db->trans_begin();
//            if ($this->db->update_batch('sales_slip_detail', $update_datas, 'id') === false) {
//                throw new Exception("Update sales_slip_detail payment_date, billing_date", 2);
//            }
//
//        } catch (Exception $e) {
//            if ($e->getCode() == 2) $this->db->trans_rollback();
//
//            log_message('error', $e->getMessage());
//            return false;
//        }
//        $this->db->trans_commit();
//
//        return true;
//    }
    
    //#9700:Start
    /**
     * reset sales slip checked status to 0
     * @param  int $sales_slip_number
     * @return boolean
     */
    public function reset_sales_slip_checked($sales_slip_number) {
        return $this->db->where('sales_slip_number', $sales_slip_number)
                        ->where('sales_slip_status', SALES_STATUS_INVOICE_ISSUED)
                        ->set('is_checked', 0)
                        ->set('check_date', '0000-00-00')
                        ->update('sales_slip');
    }
//#9700:Start
}
//#7769:End