<?php
/**
 *
 * @author truong_thanh
 */
class Sales_Target_model extends ZR_Model {


    public function search_sales_target($date_from, $date_to, $business_id, $is_product_division) {
        $sql = "SELECT A.id as account_id,  DE.name as department_name , A.name as name
                FROM account A, department DE, business_unit BU
                WHERE A.department_id = DE.id AND DE.business_unit_id = BU.id AND A.resignation = " .STATUS_ENABLE ." AND A.disable = " .STATUS_ENABLE;

        $array_params = array();
        if(!empty($business_id)) {
            $sql.=" AND BU.id = ?";
            $array_params[] = $business_id;
        }
        $sql.=" ORDER BY BU.sequence, BU.id, DE.id, A.id";
        $all_account_by_business_unit = $this->exec_query($sql, $array_params);

        $sql = "SELECT ST.*,  DE.name as department_name , A.name as name
                FROM sales_target ST LEFT JOIN account A ON ST.account_id = A.id
                LEFT JOIN department DE ON A.department_id = DE.id
                LEFT JOIN business_unit BU ON DE.business_unit_id = BU.id
                WHERE A.resignation = " .STATUS_ENABLE;

        if($is_product_division) {
            $sql.= " AND is_product_division = 1";
        }
        $string_where = "";
        $array_params = array();
        if(!empty($business_id)) {
            $string_where.=" AND BU.id = ?";
            $array_params[] = $business_id;
        }

        //$string_where.=" AND ((ST.id IS NULL AND NOT EXISTS (select sales_target.account_id from sales_target where sales_target.account_id = A.id)) OR (";
        $string_where.=" AND (";
        if(!empty($date_from)) {
            $string_where.=" ST.closing_date >= ?";
            $array_params[] = $date_from;
        }

        if(!empty($date_to)) {
            $string_where.= " AND ST.closing_date <= ?";
            $array_params[] = $date_to;
        }
        $string_where.= ")";
        // Join two strings
        if(!empty($string_where)) {
            $sql.=$string_where;
        }
        $sql.=" ORDER BY BU.sequence, BU.id, DE.id, A.id";

        $result = $this->exec_query($sql, $array_params);

        $sales_target_search_records = array();
        foreach($all_account_by_business_unit as $account) {
            $account['target_amount'] = null;
            $account['closing_date'] = null;
            $account['create_datetime'] = null;
            $account['id'] = null;
            foreach ($result as $value) {
                if($value['account_id'] == $account['account_id']) {
                    $account['target_amount'] = $value['target_amount'];
                    $account['closing_date'] = $value['closing_date'];
                    $account['create_datetime'] = $value['create_datetime'];
                    $account['id'] = $value['id'];
                }
            }
            $sales_target_search_records[] = $account;
        }

        return $sales_target_search_records;
    }

    /**
     * @author Thanh
     * @created date: 3/August/2015
     * @param unknown $data
     */
    public function save_all_grids($data, $searchDate, $login_account_id) {
        $query = "INSERT INTO sales_target
                (id, account_id, closing_date, create_datetime, target_amount, lastup_account_id, lastup_datetime)
                VALUES ";
        $list_params = array();
        $row_count_id = $this->db->query("SELECT MAX(id) as max_id FROM sales_target;")->row();
        $new_id = $row_count_id->max_id + 1;

        $date_from_split = explode ( "/", $searchDate );
        $search_date = $date_from_split [0] . "-" . $date_from_split [1] . "-" . "01";
        foreach($data as $key=>$value) {
                foreach($value as $row) {
                        if(isset($row['id'])) {
                            $query.=" (?,?,?,?,?,?, NOW()),";
                            $list_params[] = $row['id'];
                            $list_params[] = $row['account_id'];
                            $list_params[] = $row['closing_date'];
                            $list_params[] = $row['create_datetime'];
                        }
                        else{
                             $query.=" ({$new_id},?,?,NOW(),?,?, NOW()),";
                             $list_params[] = $row['account_id'];
                             $list_params[] = $search_date;
                             $new_id++;
                        }
                        $list_params[] = $row['target_amount'];
                        $list_params[] = $login_account_id;
                }
        }

        $query = substr($query, 0, -1);
        $query .= " ON DUPLICATE KEY UPDATE account_id=VALUES(account_id), closing_date=VALUES(closing_date),
                        create_datetime=VALUES(create_datetime), target_amount=VALUES(target_amount), lastup_account_id=VALUES(lastup_account_id), lastup_datetime=VALUES(lastup_datetime);";

        return $this->db->query($query, $list_params);
    }

    /**
     * check all input data from the search form
     * @param  array $form_data
     * @return array - empty if all data is valid
     */
    public function run_search_condition_validate($form_data) {
        $error_messages = array();

        if (!empty($form_data->datepicker_from)) {
            if (!parse_date($form_data->datepicker_from)) {
                $error_messages['datepicker_from'] = lang('error_from_date_invalid');
            }
            $form_data->datepicker_from = parse_date($form_data->datepicker_from);
        }

        return $error_messages;
    }

    /**
     * get_all_account_by_business_unit
     * @author: Thanh
     * @created date: 3/August/2015
     */
    public function get_all_account_by_business_unit($business_unit_id) {
        $this->db->select('a.*')
                ->from('account a')
                ->join('department d', 'd.id = a.department_id')
                ->join('business_unit bu', 'bu.id = d.business_unit_id')
                ->where('bu.id', $business_unit_id)
                ->where('a.resignation', 0);

        return $this->db->get()->result_array();
    }

    public function get_all_account_no_month_target($business_unit_id, $date_from, $date_from) {
        $sql = "SELECT ST.*, A.id as account_id,  DE.name as department_name , A.name as name
                FROM account A LEFT JOIN sales_target ST ON ST.account_id = A.id
                LEFT JOIN department DE ON A.department_id = DE.id
                LEFT JOIN business_unit BU ON DE.business_unit_id = BU.id
                WHERE A.resignation = " .STATUS_ENABLE;

        $string_where = "";
        $array_params = array();
        if(!empty($business_id)) {
            $string_where.=" AND BU.id = ?";
            $array_params[] = $business_id;
        }

        //$string_where.=" AND ((ST.id IS NULL AND NOT EXISTS (select sales_target.account_id from sales_target where sales_target.account_id = A.id)) OR (";
        $string_where.=" AND (";
        if(!empty($date_from)) {
            $string_where.=" ST.closing_date >= ?";
            $array_params[] = $date_from;
        }
    }

}