<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Sample_model extends CI_Model {

    // Product

    public function get_rule_product($rule_name) {
        // Validate login form
        $validate_rule['insert'] = array(
                        array(
                                'field' => 'business_unit',
                                'rules' => 'required',
                                'errors' => array (
                                            'required' => lang('error_business_unit_null')
                                        )
                        ),
                        array(
                                'field' => 'code',
                                'rules' => 'required|is_unique[product.code]|max_length[5]',
                                'errors' => array (
                                        'required' => lang('error_code_null'),
                                        'is_unique' => lang('error_code_unique'),
                                        'max_length' => lang('error_code_max_length')
                                )
                        ),
                        array(
                                'field' => 'name_product',
                                'rules' => 'required',
                                'errors' => array (
                                        'required' => lang('error_name_product_null'),
                                )
                        ),
                        array(
                                'field' => 'is_no_add',
                                'rules' => 'required',
                                'errors' => array (
                                        'required' => lang('error_is_no_add_null'),
                                )
                        )
                );

         $validate_rule['update'] = array(
                        array(
                                'field' => 'business_unit',
                                'rules' => 'required'
                        ),
                        array(
                                'field' => 'code',
                                'rules' => 'required|max_length[5]'
                        ),
                        array(
                                'field' => 'name_product',
                                'rules' => 'required'
                        ),
                        array(
                                'field' => 'is_no_add',
                                'rules' => 'required'
                        )
                );
         // account
         $validate_rule['insert_account'] = array(
                 array(
                         'field' => 'department_id',
                         'rules' => 'required'
                 ),
                 array(
                         'field' => 'name',
                         'rules' => 'required|is_unique[account.name]'
                 ),
                 array(
                         'field' => 'login_id',
                         'rules' => 'required'
                 ),
                 array(
                         'field' => 'authority_id',
                         'rules' => 'required'
                 ),
                 array(
                         'field' => 'mail_address',
                         'rules' => 'trim|required|valid_email|is_unique[account.mail_address]'
                 )
         );

         $validate_rule['update_account'] = array(
                 array(
                         'field' => 'department_id',
                         'rules' => 'required'
                 ),
                 array(
                         'field' => 'name',
                         'rules' => 'required'
                 ),
                 array(
                         'field' => 'login_id',
                         'rules' => 'required'
                 ),
                 array(
                         'field' => 'authority_id',
                         'rules' => 'required'
                 ),
                 array(
                         'field' => 'mail_address',
                         'rules' => 'trim|required|valid_email'
                 )
         );
        if (isset ( $validate_rule [$rule_name] )) {
            return $validate_rule [$rule_name];
        }
        return array ();
    }

    /**
     *
     * Get list products by conditions
     *
     * @param json $condition = {
                                currentPage : 1,
                                pageSize : 10,
                                condition : [ [ "orderby", "where" ], ...]
                                load : true
                            }
     * @return object   list products and real total products
     * @author Cong_Minh
     */
    public function get_products_by_conditions($condition) {
        $condition = json_decode($condition);
        $condition = $condition->paginationOptions;
        $this->db->select('p.id, p.code, p.name name_product, p.business_unit_id business_unit, p.is_no_add')
                    ->from('product as p')
                    ->where('p.disable', '0');

        if ($condition->load) {
            $this->db->limit(($condition->currentPage + 1) * $condition->pageSize);
        } else {
            $this->db->limit($condition->pageSize * 2, ($condition->currentPage - 1) * $condition->pageSize);
        }

        // where
        if (isset($condition->condition[0][1])) {
            $this->db->like('p.code', $condition->condition[0][1]);
        }
        if (isset($condition->condition[1][1])) {
            $this->db->like('p.business_unit_id', $condition->condition[1][1]);
        }
        if (isset($condition->condition[2][1])) {
            $this->db->like('p.name', $condition->condition[2][1]);
        }
        if (isset($condition->condition[3][1])) {
            $this->db->like('p.is_no_add', $condition->condition[3][1]);
        }

        // orderby
        if ($condition->condition[0][0] == "asc") {
            $this->db->order_by("p.code", "asc");
        }
        elseif ($condition->condition[0][0] == "desc") {
            $this->db->order_by("p.code", "desc");
        }

        if ($condition->condition[1][0] == "asc") {
            $this->db->order_by("p.business_unit_id", "asc");
        }
        elseif ($condition->condition[1][0] == "desc") {
            $this->db->order_by("p.business_unit_id", "desc");
        }

        if ($condition->condition[2][0] == "asc") {
            $this->db->order_by("p.name", "asc");
        }
        elseif ($condition->condition[2][0] == "desc") {
            $this->db->order_by("p.name", "desc");
        }

        if ($condition->condition[3][0] == "asc") {
            $this->db->order_by("p.is_no_add", "asc");
        }
        elseif ($condition->condition[3][0] == "desc") {
            $this->db->order_by("p.is_no_add", "desc");
        }

        $products = $this->db->get()->result();

        $business_units = $this->db->select('id, name')->get('business_unit')->result_array();
        foreach($products as $product) {
            $product->business_options = $business_units;
        }

        // count total products
        $this->db->select('*')
                        ->from('product as p')
                        ->join('business_unit as b', 'b.id = p.business_unit_id', 'left')
                        ->where('p.disable', '0');
        if (isset($condition->condition[0][1])) {
            $this->db->like('p.code', $condition->condition[0][1]);
        }
        if (isset($condition->condition[1][1])) {
            $this->db->like('p.business_unit_id', $condition->condition[1][1]);
        }
        if (isset($condition->condition[2][1])) {
            $this->db->like('p.name', $condition->condition[2][1]);
        }
        if (isset($condition->condition[3][1])) {
            $this->db->like('p.is_no_add', $condition->condition[3][1]);
        }
        $numProducts = $this->db->count_all_results();

        $result->products = $products;
        $result->numProducts = $numProducts;
        return $result;
    }

    /**
     *
     * Update list products
     *
     * @param json $data
     * @return json   list error validate
     * @author Cong_Minh
     */
    public function update_list_product($data) {
        $data = json_decode($data);
        $this->load->library('form_validation');

        // validate
        $result;
        foreach ($data->products as $product) {
            if (isset($product->id)) {
                $this->form_validation->set_rules($this->get_rule_product('update'));
            } else {
                $this->form_validation->set_rules($this->get_rule_product('insert'));
            }

            $this->form_validation->set_data((array)$product);
            if ($this->form_validation->run() === FALSE) {
                $result->error_list[$product->index] = validation_errors();
            }
            $this->form_validation->reset_validation();
        }
        if (isset($result->error_list)) { die(json_encode($result)); }

        // update
        foreach ($data->products as $product) {
            if (isset($product->id)) {
                $data = array(
                        'code' => $product->code,
                        'business_unit_id' => $product->business_unit,
                        'name' => $product->name_product,
                        'is_no_add' => $product->is_no_add
                );

                $this->db->where('id', $product->id);
                $this->db->update('product', $data);
            }
            else {
                $data = array(
                        'code' => $product->code,
                        'business_unit_id' => $product->business_unit,
                        'name' => $product->name_product,
                        'is_no_add' => $product->is_no_add
                );
                $this->db->insert('product', $data);
            }
        }
        //die(json_encode($this->get_all()));
    }

    /**
     *
     * Remove list products
     *
     * @param json $data
     * @author Cong_Minh
     */
    public function remove_list_product($data) {
        $data = json_decode($data);
        foreach ($data->products as $product) {
            if (isset($product->id)) {
                $data = array(
                        'disable' => 1
                );

                $this->db->where('id', $product->id);
                $this->db->update('product', $data);
            }
        }
        exit();
    }


    // Account
    /**
     *
     * Get list products by conditions
     *
     * @param json $condition = {
     currentPage : 1,
     pageSize : 10,
     condition : [ [ "orderby", "where" ], ...]
     load : true
     }
     * @return object   list products and real total products
     * @author Cong_Minh
     */
    public function get_accounts_by_conditions($condition) {
        $condition = json_decode($condition);
        $condition = $condition->paginationOptions;
        $this->db->select('*')
        ->from('account')
        ->where('disable', '0');

        if ($condition->load) {
            $this->db->limit(($condition->currentPage + 1) * $condition->pageSize);
        } else {
            $this->db->limit($condition->pageSize * 2, ($condition->currentPage - 1) * $condition->pageSize);
        }

        // where
        if (isset($condition->condition[0][1])) {
            $this->db->like('name', $condition->condition[0][1]);
        }
        if (isset($condition->condition[1][1])) {
            $this->db->like('department_id', $condition->condition[1][1]);
        }
        if (isset($condition->condition[2][1])) {
            $this->db->like('login_id', $condition->condition[2][1]);
        }
        if (isset($condition->condition[3][1])) {
            $this->db->like('authority_id', $condition->condition[3][1]);
        }

        // orderby
        if ($condition->condition[0][0] == "asc") {
            $this->db->order_by("name", "asc");
        }
        elseif ($condition->condition[0][0] == "desc") {
            $this->db->order_by("name", "desc");
        }

        if ($condition->condition[1][0] == "asc") {
            $this->db->order_by("department_id", "asc");
        }
        elseif ($condition->condition[1][0] == "desc") {
            $this->db->order_by("department_id", "desc");
        }

        if ($condition->condition[2][0] == "asc") {
            $this->db->order_by("login_id", "asc");
        }
        elseif ($condition->condition[2][0] == "desc") {
            $this->db->order_by("login_id", "desc");
        }

        if ($condition->condition[3][0] == "asc") {
            $this->db->order_by("authority_id", "asc");
        }
        elseif ($condition->condition[3][0] == "desc") {
            $this->db->order_by("authority_id", "desc");
        }

        $accounts = $this->db->get()->result();

        $departments = $this->db->select('id, name')->get('department')->result_array();
        foreach($accounts as $account) {
            $account->department_options = $departments;
        }

        $authorities = $this->db->select('id, name')->get('authority')->result_array();
        foreach($accounts as $account) {
            $account->authority_options = $authorities;
        }

        // count total products
        $this->db->select('*')
        ->from('account')
        ->where('disable', '0');
        if (isset($condition->condition[0][1])) {
            $this->db->like('name', $condition->condition[0][1]);
        }
        if (isset($condition->condition[1][1])) {
            $this->db->like('department_id', $condition->condition[1][1]);
        }
        if (isset($condition->condition[2][1])) {
            $this->db->like('login_id', $condition->condition[2][1]);
        }
        if (isset($condition->condition[3][1])) {
            $this->db->like('authority_id', $condition->condition[3][1]);
        }
        $numAccounts = $this->db->count_all_results();

        $result->accounts = $accounts;
        $result->numAccounts = $numAccounts;
        return $result;
    }

    public function save_account($data) {
        $data = json_decode($data);
        $account = $data->rowEntity;
        $this->load->library('form_validation');

        // validate
        $result;
        if (isset($account->id)) {
            $this->form_validation->set_rules($this->get_rule_product('update_account'));
        } else {
            $this->form_validation->set_rules($this->get_rule_product('insert_account'));
        }

        $this->form_validation->set_data((array)$account);
        if ($this->form_validation->run() === FALSE) {
            $result->error_list = validation_errors();
            die(json_encode($result));
        }

        // update
        if (isset($account->id)) {
            $data = array(
                    'department_id' => $account->department_id,
                    'name' => $account->name,
                    'login_id' => $account->login_id,
                    'authority_id' => $account->authority_id,
                    'mail_address' => $account->mail_address
            );

            $this->db->where('id', $account->id);
            $this->db->update('account', $data);
        }
        else {
            $data = array(
                    'department_id' => $account->department_id,
                    'name' => $account->name,
                    'login_id' => $account->login_id,
                    'authority_id' => $account->authority_id,
                    'mail_address' => $account->mail_address
            );
            $this->db->insert('account', $data);
        }
        die();
    }

    public function get_all_department() {
        return $this->db->get('department')->result();
    }

    public function get_all_authority() {
        return $this->db->select('id, name')->get('authority')->result();
    }


    // TMP

    /**
     *
     * @author Cong_Minh
     */
    public function get_all_tmp() {
        $tmp_records = $this->db->select('*')
        ->from('tmp_grid')
        ->where('disable', '0')
        ->get()->result();
        return $tmp_records;
    }
}