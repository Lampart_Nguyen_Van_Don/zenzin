<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shipping_type_model extends ZR_Model {
	public function get_all() {
		$query = "SELECT  st.id
						, st.code
						, st.name
						, st.is_remaining_confirm
						, st.is_advance_payment
						, st.is_reporting
						, st.is_no_add
						, st.sequence
						, gp.name as new_input
				 FROM   shipping_type as st
						,gui_parts_element  as gp
				 WHERE  st.disable = 0
					AND st.is_no_add = gp.code
					AND gp.gui_parts_id = ".GUI_PART_NEN_INPUT."
				 ORDER  BY st.sequence, st.id ASC";
		return $this->exec_query($query);
	}

	public function get_by_id($id) {
		$query = "SELECT id
					, code
					, name
					, is_remaining_confirm
					, is_advance_payment
					, is_reporting
					, is_no_add
					, sequence
				 FROM shipping_type
				 WHERE disable = 0 AND id = ?";
		$this->set_single();
		return $this->exec_query($query, array($id));
	}

	public function update_shipping_type($id, $data) {
		return $this->update('shipping_type', $data, array('id' => $id));
	}

	public function add_new_shipping_type($data) {
		$max_code = $this->get_max_code();
		$data['code'] = $data['sequence'] = $max_code['max_code'] ? $max_code['max_code'] + 1 : 1;
        $data['lastup_account_id'] = $this->auth->get_account_id();
		return $this->insert('shipping_type', $data);
	}

	private function get_max_code() {
        $query = "SELECT MAX(code) as max_code
                  FROM shipping_type";
        $this->set_single();
        return $this->exec_query($query);
    }

    /**
     * delete shipping type
     * @param  Shipping_type object $shipping_type
     * @return mixed                return -1 if shipping type is in used else true | false;
     */
    public function delete_shipping_type($shipping_type) {

    	if (!$this->can_delete_by_code($shipping_type['code'])) return -1;

    	return $this->delete('shipping_type', array('id' => $shipping_type['id'])) ? true : false;
    }

    public function list_all() {
    	$this->db->select('*')
    			->from('shipping_type')
                ->where('disable', 0);
    	return $this->db->get()->result_array();
    }

    /**
     * check given shipping type code is exist in order_acceptance or not
     * @param  string $code
     * @return boolean
     */
    private function can_delete_by_code($code) {
    	$this->db->select('id')
    			->from('order_acceptance')
    			->where('shipping_type_cd', $code)
                ->where('disable', 0);

    	return $this->db->get()->num_rows() ? false : true;
    }

    /**
     * get all shipping type which have is_no_add field and disable field is 0
     * @return array
     */
    public function get_all_can_display() {
//#7846:Start
//        return $this->db->select('*')
        return $this->db->select('code, name')
                        ->from('shipping_type')
                        ->where('disable', 0)
                        ->where('is_no_add', 0)
                        ->get()
                        ->result_array();
//#7846:End
    }



	public function rules($rule_name) {
		$validate_rule['edit'] = array(
									array(
										'field'  => 'name',
										'label'  => 'Name',
										'rules'  => 'required|trim|max_length[128]',
										'errors' => array(
														'required'   => lang('error_name_required'),
														'max_length' => lang('error_name_max_length'),
													)
									),
									array(
										'field'  => 'sequence',
										'label'  => lang('lbl_sequence'),
										'rules'  => 'required|trim|max_length[5]|integer|is_natural|between[{0, '.MAX_SEQUENCE_ALLOW.'}]',
										'errors' => array(
														'required'    => lang('error_sequence_required'),
														'max_length'  => lang('error_sequence_max_length'),
														'integer'     => lang('error_sequence_numeric'),
												        'is_natural'  => lang('error_common_number_invalid') //lang('lbl_invalid_input_minus'),
													)
									),
									array(
										'field'  => 'is_remaining_confirm',
										'label'  => 'Remaning confirm',
										'rules'  => 'in_list[0,1]',
										'errors' => array(
														'in_list' => lang('lbl_invalid_input'),
													)
									),
									array(
										'field'  => 'is_advance_payment',
										'label'  => 'Advance payment',
										'rules'  => 'in_list[0,1]',
										'errors' => array(
														'in_list' => lang('lbl_invalid_input'),
													)
									),
									array(
										'field'  => 'is_reporting',
										'label'  => 'Reporting',
										'rules'  => 'in_list[0,1]',
										'errors' => array(
														'in_list' => lang('lbl_invalid_input'),

													)
									),

									array(
										'field'  => 'is_no_add',
										'label'  => 'Display',
										'rules'  => 'in_list[0,1]',
										'errors' => array(
														'in_list' => lang('lbl_invalid_input'),
													)
									),
								);

		$validate_rule['add'] = array(
									array(
										'field'  => 'name',
										'label'  => 'Name',
										'rules'  => 'required|trim|max_length[128]',
										'errors' => array(
														'required'   => lang('error_name_required'),
														'max_length' => lang('error_name_max_length'),
													)
									),
									array(
										'field'  => 'is_remaining_confirm',
										'label'  => 'Remaning confirm',
										'rules'  => 'in_list[0,1]',
										'errors' => array(
														'in_list' => lang('lbl_invalid_input'),
													)
									),
									array(
										'field'  => 'is_advance_payment',
										'label'  => 'Advance payment',
										'rules'  => 'in_list[0,1]',
										'errors' => array(
														'in_list' => lang('lbl_invalid_input'),
													)
									),
									array(
										'field'  => 'is_reporting',
										'label'  => 'Reporting',
										'rules'  => 'in_list[0,1]',
										'errors' => array(
														'in_list' => lang('lbl_invalid_input'),
													)
									),
								);

		if (isset($validate_rule[$rule_name])) {
            return $validate_rule[$rule_name];
        }
        return array ();
	}
}