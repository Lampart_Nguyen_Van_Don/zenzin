<?php
/**
 * This class contain functions which work with subcontractor table.
 *
 * @author hoang_minh
 * @since 2015-05-11
 */

class Subcontractor_model extends ZR_Model {

    /** Config **/
    private $_config = array();

    public function __construct() {
        parent::__construct();

        //set config
        $this->_config = array(

                            //is new input
                            'is_no_add' => array(
                                    0 => array(
                                            'display' => lang('lbl_is_no_add_checkbox'),
                                            'value'   => 0
                                    ),
                                    1 => array(
                                            'display' => lang('lbl_is_no_add_checkbox'),
                                            'value'   => 1
                                    ),
                            ),

                            // subcontractor type
                            'direct_consignment' => array(
                                0 => array(
                                        'display' => lang('default_consignment'),
                                        'value' => -1,
                                ),
                                1 => array(
                                        'display' => lang('direct_consignment'),
                                        'value' => 1,
                                ),
                                2 => array(
                                        'display' => lang('indirect_consignment'),
                                        'value' => 0,
                                ),
                            ),
         );
    }

    /**
     * Get config
     *
     * @param string
     * @return array
     * @author hoang_minh
     * @since 2015-05-11
     */
    public function get_configs($key) {
        if ( ! isset($this->_config[$key])) {
            return array();
        }

        return $this->_config[$key];
    }

    /**
     * Get all subcontractors.
     *
     * @param null
     * @return mixed null|array
     * @author hoang_minh
     * @since 2015-05-11
     *
     * @modify cam_tien add column name is_no_add
     */
    public function get_subcontractors() {
        //get columns
        $columns = array(
                        'id',
                        'code',
                        'name',
                        'abbr_name',
                        'direct_consignment',
                        'is_no_add'
        );

        return $this->get_items('subcontractor', null, $columns);
    }

    /**
     * Get subcontractors by conditions.
     *
     * @param array $params
     * @return mixed null|array
     * @author hoang_minh
     * @since 2015-05-11
     */
    public function get_subcontractors_by_params($params = array()) {
        if (empty($params)) {
            return $this->get_subcontractors();
        }
        //init variables
        $conditions = array();
        $orders = array();

        //make search conditions
        if (isset($params['conditions'])) {
            foreach ($params['conditions'] as $key => $val) {
                switch ($key) {
                    case 'id':
                        $conditions[] = "sub.".$key . " = '" . mysql_escape_string($val) . "'";
                        break;

                    case 'code':
                        $conditions[] = "sub.".$key . " LIKE '" . mysql_escape_string($val) . "%'";
                        break;

                    case 'name':
                    case 'abbr_name':
                        $conditions[] =  "sub.".$key . " LIKE '%" . mysql_escape_string($val) . "%'";
                        break;

                    case 'direct_consignment':
                        $conditions[] =  "sub.".$key . " ='" . mysql_escape_string($val) . "'";
                        break;

                    case 'except_id':
                        $conditions[] = "sub.id NOT IN(" . mysql_escape_string($val) . ")";
                        break;
//#9695:Start
                    case 'business_unit_id':
                        $ids = implode(', ', array_map(function ($entry) { return $entry['id'];}, $val));
                        $conditions[] =  "sub.".$key . " IN (" . $ids . ")";
                        break;
//#9695:End

                    default:
                        break;
                }
            }
        }

        $conditions[] = 'sub.disable = ' . STATUS_ENABLE;

        //make order conditions
        if (isset($params['orders'])) {
            foreach ($params['orders'] as $key => $val) {
                $orders[] = $key . " " . $val;
            }
        }

        if (! empty($conditions) || ! empty($orders)) {
            //get columns
            $columns = array(
                    'sub.id',
                    'sub.code',
                    'sub.name',
                    'sub.abbr_name',
                    'sub.direct_consignment',
                    'sub.is_no_add',
            		'gp.name as new_input'
            );
            $conditions[] =  " sub.is_no_add = gp.code"
            				." AND gp.gui_parts_id = ".GUI_PART_NEN_INPUT;
            return $this->get_items_by_parameters('subcontractor as sub ,gui_parts_element  as gp', $columns, $conditions, $orders);
        } else {
            return $this->get_subcontractors();
        }

    }

    /**
     * Set rules for validation.
     *
     * @param null
     * @return bool
     * @author hoang_minh
     * @since 2015-05-11
     */
     public function rules($rule_name, $params) {
        $is_no_add_configs = $this->get_configs('is_no_add');
        $direct_consignment_configs = $this->get_configs('direct_consignment');

        $validate_rule['subcontractor'] = array (
                array (
                        'field' => 'code',
                        'rules' => 'trim|required|min_length[2]|max_length[6]|allow_character[upercase_alpha + numeric]|is_unique[subcontractor.code,'. (isset($params['id']) ? $params['id'] : '') . ']',
                        'errors' => array (
                                'required' => str_replace('%label%', lang('lbl_code'), lang('require')),
                                'min_length' => str_replace(array('%label%', '%min%', '%max%'), array(lang('code'), 2, 6), lang('min_max_[0-9A-Z]')),
                                'max_length' => str_replace(array('%label%', '%min%', '%max%'), array(lang('code'), 2, 6), lang('min_max_[0-9A-Z]')),
                                'allow_character'   => str_replace(array('%label%', '%min%', '%max%'), array(lang('code'), 2, 6), lang('min_max_[0-9A-Z]')),
                                'is_unique'  => str_replace('%label%', lang('code'), lang('unique'))
                        )
                 ),
                 array (
                        'field' => 'name',
                        'rules' => 'trim|required|max_length[128]',
                        'errors' => array (
                                'required' => str_replace('%label%', lang('lbl_name'), lang('require')),
                                'max_length' => str_replace(array('%label%', '%max%'), array(lang('lbl_name'), 128), lang('max_character')),
                        )
                ),
                array (
                        'field' => 'abbr_name',
                        'rules' => 'trim|required|max_length[64]',
                        'errors' => array (
                                'required' => str_replace('%label%', lang('lbl_abbr_name'),lang('require')),
                                'max_length' => str_replace(array('%label%', '%max%'), array(lang('lbl_name'), 64), lang('error_attr_name_max_length')),
                        )
                ),

                array (
                        'field' => 'is_no_add',
                        'rules' => 'in_list[' . $is_no_add_configs[0]['value'] . ',' . $is_no_add_configs[1]['value'] . ']',
                        'errors' => array (
                                'in_list' => str_replace('%label%', lang('lbl_is_no_add'), lang('invalid_data'))
                        )
                ),
                array (
                        'field' => 'direct_consignment',
                        'rules' => 'in_list[' . $direct_consignment_configs[2]['value'] . ',' . $direct_consignment_configs[1]['value'] . ']',
                        'errors' => array (
                                'in_list' => str_replace('%label%', lang('lbl_direct_consignment'), lang('invalid_data'))
                        )
                ),
        );
        if (isset($validate_rule [$rule_name])) {
            return $validate_rule [$rule_name];
        }
        return array();
    }
    /**
     * Update subcontractor
     *
     * @param array $params
     * @return bool
     * @author hoang_minh
     * @since 2015-05-12
     */
    public function update_subcontractor($params = array()) {
        if ( ! is_array($params) || empty($params)) {
            return false;
        }

        return $this->update('subcontractor', $params['update_data'], $params['update_conditions']);
    }

    /**
     * Delete subcontractor
     *
     * @param array $params
     * @return bool
     * @author hoang_minh
     * @since 2015-05-12
     */
    public function delete_subcontractor($params = array()) {
        if ( ! is_array($params) || empty($params)) {
            return false;
        }

        //init variables
        $update_data = $params['update_data'];
        $update_data += array(
                            'disable' => STATUS_DISABLE
        );
        return $this->update_subcontractor(array('update_data' => $update_data, 'update_conditions' => $params['update_conditions']));
    }

    /**
     * Insert a subcontractor
     * @param array $data
     * @return int
     */
    public function insert_subcontractor($data) {
        return $this->insert('subcontractor', $data);
    }

    /**
     * get id, code, name of parent subcontractor and it own children subcontractor
     * @return array
     * @author  cam_tien
     */
    public function get_id_code_of_all_subcontractor_and_relation() {
        $subcontractors = $this->db->select('
                                  s.id as parent_id
                                , s.code as parent_code
                                , s.name as parent_name
                                , s.abbr_name as parent_abbr_name
                                , s_child.id as child_id
                                , s_child.code as child_code
                                , s_child.name as child_name
                                , s_child.abbr_name as child_abbr_name
                                ')
                            ->from('subcontractor s')
                            ->join('subcontractor_relation sr', 'sr.parent_subcontractor_id = s.id and sr.disable = 0', 'left')
                            ->join('subcontractor s_child', 's_child.id = sr.child_subcontractor_id and s_child.is_no_add = 0 and s_child.disable = 0', 'left')
                            ->where('s.direct_consignment', 1)
                            ->where('s.is_no_add', 0)
                            ->where('s.disable', 0)
                            ->get()
                            ->result_array();

        if (empty($subcontractors)) return array();
        // echo "<pre>";print_r($subcontractors);die;
        $datas = array();
        $data_parents = array();
        foreach ($subcontractors as $subcontractor) {
            if (empty($data_parents)) {
                $data_parents[] = array(
                    'parent_id'        => $subcontractor['parent_id'],
                    'parent_code'      => $subcontractor['parent_code'],
                    'parent_name'      => $subcontractor['parent_name'],
                    'parent_abbr_name' => $subcontractor['parent_abbr_name'],
                );
            } else {
                $is_exist = false;
                foreach ($data_parents as $parent) {
                    if ($parent['parent_id'] == $subcontractor['parent_id']) $is_exist = true;
                }

                if (!$is_exist) {
                    $data_parents[] = array(
                        'parent_id'        => $subcontractor['parent_id'],
                        'parent_code'      => $subcontractor['parent_code'],
                        'parent_name'      => $subcontractor['parent_name'],
                        'parent_abbr_name' => $subcontractor['parent_abbr_name'],
                    );
                }
            }
        }

        foreach ($data_parents as $data_parent) {
            $parent_child = array();
            foreach ($subcontractors as $subcontractor) {
                if ($data_parent['parent_id'] == $subcontractor['parent_id']) {
                    if ($subcontractor['child_id']) {
                        $parent_child[] = array (
                            'child_id'        => $subcontractor['child_id'] ? $subcontractor['child_id'] : '',
                            'child_code'      => $subcontractor['child_code'] ? $subcontractor['child_code'] : '',
                            'child_name'      => $subcontractor['child_name'] ? $subcontractor['child_name'] : '',
                            'child_abbr_name' => $subcontractor['child_abbr_name'] ? $subcontractor['child_abbr_name'] : '',
                        );
                    }
                }
            }
            $datas[] = array(
                'parent_id'        => $data_parent['parent_id'],
                'parent_code'      => $data_parent['parent_code'],
                'parent_name'      => $data_parent['parent_name'],
                'parent_abbr_name' => $data_parent['parent_abbr_name'],
                'children'         => $parent_child,
            );
        }
        return $datas;

    }

//#9695:Start
    /**
     * list all subcontractors with business unit data
     * @return array
     */
    public function get_subcontractors_with_business() {
        return $this->db->select('s.id
                                , s.code
                                , s.name
                                , s.abbr_name
                                , s.direct_consignment
                                , s.is_no_add
                                , bu.finish_work_report_template')
                        ->from('subcontractor s')
                        ->join('business_unit bu', 'bu.id = s.business_unit_id')
                        ->where('s.disable', 0)
                        ->where('bu.disable', 0)
                        ->get()
                        ->result_array();
    }
//#9695:End
}

