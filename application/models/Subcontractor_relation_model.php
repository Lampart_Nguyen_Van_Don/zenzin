<?php
/**
 * This class contain functions which work with subcontractor table.
 *
 * @author hoang_minh
 * @since 2015-05-13
 */

class Subcontractor_relation_model extends ZR_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Update subcontractor relation
     *
     * @param array $params
     * @return bool
     * @author hoang_minh
     * @since 2015-05-13
     */
    public function update_subcontractor_relation($params = array()) {
        if ( ! is_array($params) || empty($params)) {
            return false;
        }

        return $this->update('subcontractor_relation', $params['update_data'], $params['update_conditions']);
    }

    /**
     * Delete subcontractor relation
     *
     * @param array $params
     * @return bool
     * @author hoang_minh
     * @since 2015-05-13
     */
    public function delete_subcontractor_relation($params = array()) {
        if ( ! is_array($params) || empty($params)) {
            return false;
        }

        //init variables
        $update_data = $params['update_data'];
        $update_data += array(
                            'disable' => STATUS_DISABLE
        );

        return $this->update_subcontractor_relation(array('update_data' => $update_data, 'update_conditions' => $params['update_conditions']));
    }

    /**
     * Insert a subcontractor
     *
     * @param array $data
     * @return int
     */
    public function insert_subcontractor_relations($data) {
        return $this->insert_batch('subcontractor_relation', $data);
    }

    /**
     * Get subcontractor relations by conditions.
     *
     * @param array $params
     * @return mixed null|array
     * @author hoang_minh
     * @since 2015-05-11
     */
    public function get_subcontractors_relations_by_params($params = array()) {
        if (empty($params)) {
            return null;
        }

        //init variables
        $conditions = array();
        $orders = array();
        $table = 'subcontractor_relation ';

        //make search conditions
        if (isset($params['conditions'])) {
            foreach ($params['conditions'] as $key => $val) {
                switch ($key) {
                    case 'parent_subcontractor_id':

                        $conditions[] = "subcontractor_relation.parent_subcontractor_id = '" . mysql_escape_string($val) . "'";
                        $table .= 'JOIN subcontractor ON subcontractor_relation.child_subcontractor_id = subcontractor.id';
                        break;

                    case 'child_subcontractor_id':
                        $conditions[] = "subcontractor_relation.child_subcontractor_id = '" . mysql_escape_string($val) . "'";
                        $table .= 'JOIN subcontractor ON subcontractor_relation.parent_subcontractor_id = subcontractor.id';
                        break;

                    default:
                        break;
                }
            }
        }

        $conditions[] = 'subcontractor_relation.disable = ' . STATUS_ENABLE;
        $conditions[] = 'subcontractor.disable = ' . STATUS_ENABLE;

        //make order conditions
        if (isset($params['orders'])) {
            foreach ($params['orders'] as $key => $val) {
                $orders[] = $key . " " . $val;
            }
        }

        if (! empty($conditions) || ! empty($orders)) {
            //get columns
            $columns = array(
                    'subcontractor.id',
                    'subcontractor.code',
                    'subcontractor.name',
                    'subcontractor.abbr_name',
                    'subcontractor.direct_consignment',
                    'subcontractor.is_no_add'
            );

            return $this->get_items_by_parameters($table, $columns, $conditions, $orders);
        } else {
            return null;
        }
    }

    public function get_subcontractors_relations_misc($key, $val) {

    	$sql = "SELECT  * FROM subcontractor_relation WHERE $key = '" . mysql_escape_string($val) . "' AND disable = " . STATUS_ENABLE;

    	return $this->exec_query ( $sql );
    }

    /**
     * Delete subcontractor relation
     *
     * @param array $params
     * @return bool
     * @author hoang_minh
     * @since 2015-05-13
     */
    public function delete_subcontractor_relations($params = array()) {
        if ( ! is_array($params) || empty($params)) {
            return false;
        }

        $where = '';
        if (isset($params['update_conditions'])) {
            $where = 'WHERE ';

            $update_conditions = array();
            foreach ($params['update_conditions'] as $key => $val) {
                switch ($key) {
                    case 'parent_subcontractor_id':
                    case 'child_subcontractor_id':
                        $update_conditions[] = $key . ' IN (' . $val . ')';
                        break;

                    default:
                        break;
                }
            }
            $where .= implode(' AND ', $update_conditions);
        }

        $set = 'SET disable = ' . STATUS_DISABLE;
        if (isset($params['update_data'])) {
            $set .= ', ';

            $update_data = array();
            foreach ($params['update_data'] as $key => $val) {
                $update_data[] = $key . "='" . mysql_escape_string($val) . "'";
            }
            $set .= implode(' , ', $update_data);
        }

        $query = 'UPDATE subcontractor_relation '
               . $set . ' '
               . $where;

        return $this->db->query($query, array());
    }

}