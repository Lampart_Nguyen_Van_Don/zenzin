<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Tmp_grid_model extends CI_Model {

    /**
     *
     * @author Cong_Minh
     */
    public function get_all() {
        $tmp_records = $this->db->select('*')
                        ->from('tmp_grid')
                        ->where('disable', '0')
                        ->get()->result();
        return $tmp_records;
    }
}