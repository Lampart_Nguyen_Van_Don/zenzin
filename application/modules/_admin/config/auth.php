<?php  (defined('BASEPATH')) OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Authenticate Namespace
|--------------------------------------------------------------------------
|
| Namespace of session which is used to store user data
|
*/
$config['namespace'] = '_admin_authenticate';

/*
|--------------------------------------------------------------------------
| Database Configures
|--------------------------------------------------------------------------
|
| Authentication will fetch user record directly from database.
|
| table       - table name which fetch user record from
| usernamecol - column name of login_id
| passwordcol - column name of password
| db_where    - extra condition to fetch user record (can be either a string or an array)
|
*/
$config['table'] = 'account';
$config['usernamecol'] = 'login_id';
$config['passwordcol'] = 'password';
$config['db_where'] = array('disable' => 0, 'resignation' => 0);

/*
|--------------------------------------------------------------------------
| Password Encryption
|--------------------------------------------------------------------------
|
| use_encrypt_key - whether or not to be appended with encryption_key in
|                   config.php
| crypt_type      - encryption method
|
| use_encrypt_sql - Encryption method for checking admin password
|
*/
$config['use_encrypt_key'] = true;
$config['crypt_type']      = 'md5';
$config['use_encrypt_sql'] = false; // Use Blowfish algorithm to verify user password

/*
|--------------------------------------------------------------------------
| User Data Fields
|--------------------------------------------------------------------------
|
| Field names of what information will store in session.
|
*/
$config['db_fields'] = array('authority_id','login_id','name');

/*
|--------------------------------------------------------------------------
| User Data Fields
|--------------------------------------------------------------------------
|
| Field names of what information will store in session.
|
*/
$config['messages'] = array(
        'default' => 'ログインIDまたはパスワードが異なります。',
);