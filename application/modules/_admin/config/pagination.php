<?php  (defined('BASEPATH')) OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Pagination Method
|--------------------------------------------------------------------------
|
| Using which method to get page number and generate page links.
|
| + get
| + post
|
*/
$config['method'] = 'post';


/*
|--------------------------------------------------------------------------
| Page Query String
|--------------------------------------------------------------------------
|
| Using a page query string or not.
|
| Example:
|   + Default format:
|         http://example.com/index.php/test/page/20
|   + Page query string format:
|         http://example.com/index.php?c=test&m=page&per_page=20
|
| page_uri_array - using uri array (turn URI segments into and array) or not
| segment_start  - offset of URI segments to convert uri into array
|
*/
$config['page_query_string'] = false;
$config['page_uri_array'] = true;
$config['segment_start'] = 4;


/*
|--------------------------------------------------------------------------
| Pagination Query String Segment
|--------------------------------------------------------------------------
|
| Segment name or segment number to get page number.
|
| uri_segment          - segment number contain page number
| query_string_segment - segment string contain page number
|                        [default: 'per_page']
|
*/
// $config['uri_segment'] = 3;
$config['query_string_segment'] = 'pg';


/*
|--------------------------------------------------------------------------
| Use Page Number
|--------------------------------------------------------------------------
|
| Using starting index for the item (false|default) or actual page
| number (true).
|
*/
$config['use_page_numbers'] = true;


/*
|--------------------------------------------------------------------------
| Number of Items per Page
|--------------------------------------------------------------------------
|
| per_page - the number of items you intend to show per page.
|
| use_per_page_segment - whether to get number of items from segment or not
| per_page_segment     - segment string contain number of items
|
*/
$config['per_page'] = 20;
$config['use_per_page_segment'] = false;
$config['per_page_segment'] = 'pp';


/*
|--------------------------------------------------------------------------
| Number of Links
|--------------------------------------------------------------------------
|
| The number of "digit" links you would like before and after the selected
| page number.
|
*/
$config['num_links'] = 6;


/*
|--------------------------------------------------------------------------
| Links Content
|--------------------------------------------------------------------------
|
| Content to display for some special links.
| Set to false disable link.
|
|
*/
$config['first_link'] = '';

$config['prev_link']  = '';
$config['next_link']  = '';

$config['last_link']  = '';
/*
|--------------------------------------------------------------------------
| Configuration for GET Method
|--------------------------------------------------------------------------
|
| Add div with class prev and next to last and next link
|
*/

$config['get_template'] = array(
        'anchor_class'    => '',
        'full_tag_open'   => '<ul class="pagination">',
        'full_tag_close'  => '</ul>',
        'first_tag_open'  => '<li>',
        'first_tag_close' => '</li>',
        'last_tag_open'   => '<li>',
        'last_tag_close'  => '</li>',
        'cur_tag_open'    => '<li class="disabled"><li class="active"><a href="">',
        'cur_tag_close'   => '<span class="sr-only"></span></a></li>',
        'next_tag_open'   => '<li>',
        'next_tag_close'  => '</li>',
        'prev_tag_open'   => '<li>',
        'prev_tag_close'  => '</li>',
        'num_tag_open'    => '<li>',
        'num_tag_close'   => '</li>',
);


/*
|--------------------------------------------------------------------------
| Configuration for POST Method
|--------------------------------------------------------------------------
|
| post_prefix  : Prefix to filter which POST is used to search
| post_template: Add div with class prev and next to last and next link
|
*/

$config['post_prefix'] = 'search_';
$config['post_template'] = array(
        'form_class'      => '',
        'button_class'    => '',
        'first_button_class'    => 'genericon genericon-rewind',
        'prev_button_class'    => 'genericon genericon-skip-back',
        'last_button_class'    => 'genericon genericon-fastforward',
        'next_button_class'    => 'genericon genericon-skip-ahead',
        'full_tag_open'   => '<ul class="pager a-left">',
        'full_tag_close'  => '</ul>',
        'first_tag_open'  => '<li>',
        'first_tag_close' => '</li>',
        'last_tag_open'   => '<li>',
        'last_tag_close'  => '</li>',
        'cur_tag_open'    => '<li><button class="active" disabled>',
        'cur_tag_close'   => '</button></li>',
        'next_tag_open'   => '<li>',
        'next_tag_close'  => '</li>',
        'prev_tag_open'   => '<li>',
        'prev_tag_close'  => '</li>',
        'num_tag_open'    => '<li>',
        'num_tag_close'   => '</li>',
);



/*
|--------------------------------------------------------------------------
| Configuration for AJAX Method
|--------------------------------------------------------------------------
|
| ajax_template: Add div with class prev and next to last and next link
|
*/
$config['ajax_template'] = array(
        'anchor_class'    => '',
        'full_tag_open'   => '<div class="pager">',
        'full_tag_close'  => '</div>',
        'first_tag_open'  => '<div class="prev">',
        'first_tag_close' => '</div>',
        'last_tag_open'   => '<div class="next">',
        'last_tag_close'  => '</div>',
        'cur_tag_open'    => '<strong>',
        'cur_tag_close'   => '</strong>',
        'next_tag_open'   => '<div class="next">',
        'next_tag_close'  => '</div>',
        'prev_tag_open'   => '<div class="prev">',
        'prev_tag_close'  => '</div>',
        'num_tag_open'    => '',
        'num_tag_close'   => '',
);

