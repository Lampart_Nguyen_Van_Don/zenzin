<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @author cam_tien
 *
 */

class Account extends ZR_Controller {
    public function show() {
        $this->load->model('business_unit_model');
        $this->load->model('account_model');
        $this->load->model('department_model');
        $this->load->model('authority_model');

        $business_units = $this->business_unit_model->get_all();

        if ($this->input->post('business_unit_id')) {
            $business_unit_id = $this->input->post('business_unit_id');

            $departments = $this->department_model->get_departments_by_business_unit_id($business_unit_id);
            $def_arr = array('id' => '-1', 'name' => '＝＝すべて＝＝');
            array_unshift($departments, $def_arr);

            $this->assign('departments', $departments);
        }

        $accounts = $this->account_model->set_pager()->search_by_name_and_department($this->input->post());

        $auth = $this->auth->get_auth_data();
        $auth_accessible = true;

        // FOR ADD TEMPLATE
        $business_departments = $this->department_model->get_all_departments();
        $authorities = $this->authority_model->get_authorities();

        $this->assign('auth_accessible', $auth_accessible);
        $this->assign('accounts', $accounts);
        $this->assign('business_units', $business_units);
        $this->assign('business_departments', $business_departments);
        $this->assign('authorities', $authorities);
        $this->view('account/show.tpl');
    }

    public function ajax_get_department() {
        if ($this->input->is_ajax_request()) {
            $this->load->model('department_model');

            $business_unit_id = $this->input->post('business_unit_id');
            $departments = $this->department_model->get_departments_by_business_unit_id($business_unit_id);
            if (empty($departments)) {
                echo 0; return;
            }

            $this->ajax_json($departments);
        } else {
            show_404();
        }
    }

    public function ajax_get_detail() {
        if ($this->input->is_ajax_request()) {
            $this->load->model('account_model');

            $account_id = $this->input->post('account_id');
            $account = $this->account_model->get_detail($account_id);
            if (empty($account)) {
                $this->ajax_json(array(
                                    'error_code' => 400,
                                    'messages' => lang('lbl_invalid_input')
                                ));
                return;
            }

            $auth = $this->auth->get_auth_data();
            $auth_accessible = true;
//             if ($auth['userdata']['authority_id'] == AUTH_SYSTEM_ADMINISTRATOR) $auth_accessible = true;

            $this->assign('auth_accessible', $auth_accessible);
            $this->assign('account', $account);
            $this->view('account/detail.tpl');
        } else {
            show_404();
        }
    }

    public function ajax_edit() {
        if ($this->input->is_ajax_request()) {
            $this->load->model('department_model');
            $this->load->model('account_model');
            $this->load->model('authority_model');

            $is_copy = $this->input->post('is_copy') ? true : false;
            $account_id = $this->input->post('account_id');

            $account = $this->account_model->get_detail($account_id);

            $auth = $this->auth->get_auth_data();
            $auth_accessible = true;
//             if ($auth['userdata']['authority_id'] == AUTH_SYSTEM_ADMINISTRATOR) $auth_accessible = true;

            if (empty($account) || !$auth_accessible) {
                $this->ajax_json(array(
                                    'error_code' => 400,
                                    'messages' => lang('lbl_invalid_input')
                                ));
                return;
            }

            $departments = $this->department_model->get_all_departments();
            $authorities = $this->authority_model->get_authorities();

            $this->assign('is_copy', $is_copy);
            // echo $is_copy;
            $this->assign('departments', $departments);
            $this->assign('authorities', $authorities);
            $this->assign('account', $account);

            $this->view('account/edit.tpl');
        } else {
            show_404();
        }
    }

    public function ajax_update() {
        if ($this->input->is_ajax_request()) {
            
            $this->load->model('department_model');
            $this->load->model('authority_model');
            $this->load->model('account_model');

            $account_id    = $this->input->post('account_id');
            $department_id = $this->input->post('department_id');
            $authority_id  = $this->input->post('authority_id');
            $mail_address  = $this->input->post('mail_address');
            $name          = $this->input->post('name');
            $password      = $this->input->post('password');
            $resignation   = $this->input->post('resignation') ? 1 : 0;

            $account    = $this->account_model->get_account_by_id($account_id);
            $department = $this->department_model->get_department_by_id($department_id);
            $authority  = $this->authority_model->get_authority_by_id($authority_id);

            $auth = $this->auth->get_auth_data();
            $auth_accessible = true;
//             if ($auth['userdata']['authority_id'] == AUTH_SYSTEM_ADMINISTRATOR) $auth_accessible = true;

            if (empty($account) || empty($department) || empty($authority) || !$auth_accessible){
                var_dump($auth_accessible);die;
                $this->ajax_json(array(
                                    'error_code' => 400,
                                    'messages' => lang('lbl_invalid_input')
                                ));
                return;
            }

            $validate_email = true;
            $validate_info = $this->account_model->validate('edit');
            if ($mail_address != $account['mail_address']) {
                $validate_email = $this->account_model->validate('email');
            }

            if ($validate_info && $validate_email) {
                $data = array (
                            'name'          => $name,
                            'department_id' => $department_id,
                            'mail_address'  => $mail_address,
                            'resignation'   => $resignation,
                            'lastup_account_id' => $this->auth->get_account_id(),
                        );

                // CAN NOT CHANGE OWN AUTHORITY
                if ($this->auth->get_account_id() != $account_id) {
                    $data['authority_id'] = $authority_id;
                }

                // IF HAVE CHANGE PASSWORD THEN INSERT TO LOG
                if ($password) {
                    $data['password'] = md5($password);
                    $data_log = array(
                            'account_id'        => $account_id,
                            'password'          => $data['password'],
                            'lastup_account_id' => $this->auth->get_account_id(),
                        );
                    $this->account_model->insert_account_password_log($data_log);
                }

                $success = $this->account_model->update_account($data, $account_id);
                $status = array(
                            'status'   => $success ? true : false,
                            'messages' => $success ? lang('lbl_edit_success') : lang('lbl_edit_error'),
                        );
            } else {
                $status = array(
                            'status' => false,
                            'messages' => array(
                                            'name'             => form_error('name'),
                                            'password'         => form_error('password'),
                                            'confirm_password' => form_error('confirm_password'),
                                            'mail_address'     => form_error('mail_address'),
                                            'resignation'      => form_error('resignation'),
                                        ),
                        );
            }
            $this->ajax_json($status);
        } else {
            show_404();
        }
    }

    public function ajax_add() {
        if ($this->input->is_ajax_request()) {
            $this->load->model('department_model');
            $this->load->model('authority_model');
            $this->load->model('account_model');

            $department_id = $this->input->post('department_id');
            $name          = $this->input->post('name');
            $login_id      = $this->input->post('login_id');
            $password      = $this->input->post('password');
            $mail_address  = $this->input->post('mail_address');
            $resignation   = $this->input->post('resignation') ? 1 : 0;
            $authority_id  = $this->input->post('authority_id');

            $auth = $this->auth->get_auth_data();
            $auth_accessible = true;
//             if ($auth['userdata']['authority_id'] == AUTH_SYSTEM_ADMINISTRATOR) $auth_accessible = true;

            if (!$auth_accessible){
                $this->ajax_json(array(
                                    'error_code' => 400,
                                    'messages' => lang('lbl_invalid_input')
                                ));
                return;
            }

            if ($this->account_model->validate('add')) {
                $department = $this->department_model->get_department_by_id($department_id);
                $authority  = $this->authority_model->get_authority_by_id($authority_id);
                if (empty($department) || empty($authority)){
                    $this->ajax_json(array(
                                        'error_code' => 400,
                                        'messages' => lang('lbl_invalid_input')
                                    ));
                    return;
                }

                $data = array (
                        'name'          => $name,
                        'login_id'      => $login_id,
                        'password'      => md5($password),
                        'department_id' => $department_id,
                        'mail_address'  => $mail_address,
                        'resignation'   => $resignation,
                        'authority_id'  => $authority_id,
                        'lastup_account_id' => $this->auth->get_account_id(),
                    );
                $success = $this->account_model->insert_account($data);

                $data_log = array(
                        'account_id'        => $this->account_model->get_insert_id(),
                        'password'          => md5($password),
                        'lastup_account_id' => $this->auth->get_account_id(),
                    );
                if ($success) $this->account_model->insert_account_password_log($data_log);
                $status = array(
                            'status' => $success ? true : false,
                            'messages' => $success ? lang('lbl_add_success') : lang('lbl_add_error'),
                        );
            } else {
                $status = array(
                            'status'   => false,
                            'messages' => array(
                                            'name'             => form_error('name'),
                                            'login_id'         => form_error('login_id'),
                                            'password'         => form_error('password'),
                                            'confirm_password' => form_error('confirm_password'),
                                            'mail_address'     => form_error('mail_address'),
                                            'resignation'      => form_error('resignation'),
                                            'department'       => form_error('department_id'),
                                            'authority'        => form_error('authority_id'),
                                        ),
                        );
            }
            $this->ajax_json($status);
        } else {
            show_404();
        }
    }

    public function export_csv() {
        if ($this->input->post()) {
            $this->load->model('account_model');
            $this->load->model('department_model');

            $auth = $this->auth->get_auth_data();

            $datas = $this->account_model->export_csv($this->input->post());

            $header = "";
            $header = mb_convert_encoding(lang('lbl_login_id'),'SJIS','UTF-8'). ","
                     . mb_convert_encoding(lang('lbl_name'),'SJIS','UTF-8') . ","
                     . mb_convert_encoding(lang('lbl_email'),'SJIS','UTF-8') . ","
                     . mb_convert_encoding(lang('lbl_department_name'),'SJIS','UTF-8') . ","
                     . mb_convert_encoding(lang('lbl_authority_name'),'SJIS','UTF-8') . ","
                     . mb_convert_encoding(lang('lbl_resign_flag'),'SJIS','UTF-8'). "\r\n";

            $content = "";
            foreach ($datas as $fields) {
                $content .= mb_convert_encoding($fields['login_id'],'SJIS','UTF-8') . ",";
                $content .= mb_convert_encoding($fields['name'],'SJIS','UTF-8') . ",";
                $content .= mb_convert_encoding($fields['mail_address'],'SJIS','UTF-8') . ",";
                $content .= mb_convert_encoding($fields['department_name'],'SJIS','UTF-8') . ",";
                $content .= mb_convert_encoding($fields['authority_name'],'SJIS','UTF-8') . ",";
                $content .= mb_convert_encoding($fields['resignation'] ? "退職" : " ",'SJIS','UTF-8') . "\r\n";
            }

            $csv = $header . $content;

            $name = 'user_list_' . date('Y') . date('m') . '.csv';

            $this->load->helper('download');
            force_download($name, $csv);
            exit();
        } else {
            show_404();
        }
    }

    public function ajax_delete() {
        if ($this->input->is_ajax_request()) {
            $this->load->model('account_model');

            $account_id = $this->input->post('account_id');
            if ($this->auth->get_account_id() === $account_id) {
                return $this->ajax_json(array(
                        'status' => false,
                        'messages' => lang('lbl_disable_own_account'),
                ));
            }

            $success = $this->account_model->delete_account($account_id);
            if ($success == -1) {
                return $this->ajax_json(array(
                    'status' => false,
                    'messages' => lang('lbl_delete_in_use_error'),
                ));
            }

            return $this->ajax_json(array(
                    'status' => $success ? true : false,
                    'messages' => $success ? lang('lbl_delete_success') : lang('lbl_delete_error'),
            ));

        } else {
            show_404();
        }
    }

//#7069:Start
    /**
     * Change password account
     * 
     * @throws Exception
     * @author cong_tien
     */
    public function change_password() {
        $this->load->language('authenticate');

        if ($values = $this->input->post()) {
            $this->load->model('account_model');

            if ($this->account_model->validate('change_password')) {
                try {
                    //Begin transaction
                    $this->db->trans_begin();

                    // account id current
                    $account_id = $this->auth->get_account_id();

                    //Get current info account
                    if (!$current_user = $this->account_model->get_account_by_id($account_id)) {
                        throw new Exception('User not found');
                    }

                    //Compare current password and new password
                    if (md5($values['new_password']) == $current_user['password']) {
                        throw new Exception(lang('err_equal_old_password'));
                    }

                    //Get old password from history
                    if ($history_password = $this->account_model->get_password_log_by_account_id($account_id)) {
                        foreach ($history_password as $history) {
                            if (md5($values['new_password']) == $history['password']) {
                                throw new Exception(lang('err_equal_old_password'));
                            }
                        }
                    }

                    //Insert update new password
                    $update = array(
                        'password' => md5($values['new_password']),
                        'lastup_account_id' => $account_id
                    );

                    if (!$this->account_model->update_account($update, $account_id)) {
                        throw new Exception(lang('err_can_not_change_password'));
                    }

                    //Insert new password to history
                    $insert_data = array(
                        'account_id' => $account_id,
                        'password' => md5($values['new_password']),
                        'lastup_account_id' => $account_id
                    );

                    if (!$this->account_model->insert_account_password_log($insert_data)) {
                        throw new Exception(lang('err_can_not_change_password'));
                    }

                    //Write log
                    $this->app_logger->info_log('Account id: ' . $account_id . ' has changed password: ' . $current_user['password'] . ' to ' . $values['new_password']);

                    //Transaction commit
                    $this->db->trans_commit();

                    // logout user
                    $this->auth->logout();

                    //Redirect to home page - set page history - and login after
                    # redirect('_admin/home');
                    
                    $this->assign('change_password_success', true);
                } catch (Exception $ex) {
                    $this->db->trans_rollback();

                    $this->assign('err_msg', $ex->getMessage());
                    $this->app_logger->error_log('Can not change password.' . print_r($this->db->last_query(), true));
                }
            }
        }

        $this->view('account/change_password.tpl');
    }
//#7069:End

}