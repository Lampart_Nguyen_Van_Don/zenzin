<?php
/**
 * @created date: 31/July/2015
 * @author van_phong
 *
 */
class Account_title extends ZR_Controller {
    public function __construct() {
        parent::__construct ();
        // Load model for department management
        $this->load->model('Account_title_model');
    }
    /**
     * @created date: 31/July/2015
     */
    public function show() {
        $account_titles = array();
        $account_titles = $this->Account_title_model->set_pager ()->get_account_title ();
        if(!empty($account_titles)) {
            $this->assign('account_titles', $account_titles);
        }
        $this->view('account_title/show.tpl');
    }

    /**
     * View detail of one record
     * @created date: 31/July/2015
     */
    public function detail () {
        // Check if user uses direct link
        if (! $this->input->is_ajax_request ()) {
            show_404 ();
        }
        if($id = $this->input->post('id')) {
            $account_title = $this->Account_title_model->get_account_title_by_id($id);
            if(!empty($account_title)) {
                $this->assign("account_title", $account_title);
            }
        }
        $this->view('account_title/detail.tpl');
    }

    /**
     * View edit and after that saving to db
     * @created date: 31/July/2015
     */
    public function edit() {
        if (! $this->input->is_ajax_request ()) {
            show_404 ();
        }
        if($id = $this->input->post('id')) {
            // Get department
            $account_title = $this->Account_title_model->get_account_title_by_id($id);
            // print_r($account_title);
            if(!empty($account_title)) {
                $this->assign("account_title", $account_title);
            }
            $this->view('account_title/edit.tpl');
        }
        if($this->input->post('type') == "edit_save_data") {
            // Validation
            if ($this->Account_title_model->validate('edit_account_title')) {
                // Fields to update
                $data = array (
                        //'name' => $this->input->post('name'),
                        'calculate_code' => $this->input->post('calculate_code'),
                        'support_code' => $this->input->post('support_code'),
                        'remarks' => $this->input->post('remarks'),
                        'bugyo_dp_code' => $this->input->post('bugyo_dp_code'),
                );
                // Update
                $condition = array (
                       'id' => $this->input->post('id_item')
                );
                // print_r($data);
                // print_r($condition);
                if($this->Account_title_model->update_account_title('bugyo_cd_manage', $data, $condition)) {
                    $errors ['error'] = 0;
                    echo json_encode($errors);
                } else {
                    $errors ['error'] = 2;
                    echo json_encode($errors);
                }
            } else {
                $errors ['error'] = 1;
                // $errors ['name'] = form_error('name');
                $errors ['calculate_code'] = form_error('calculate_code');
                $errors ['support_code'] = form_error('support_code');
                $errors ['remarks'] = form_error('remarks');
                $errors ['bugyo_dp_code'] = form_error('bugyo_dp_code');
                echo json_encode($errors);
            }
        }
    }
}