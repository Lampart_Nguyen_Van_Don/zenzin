<?php
class Advance_payment extends ZR_Controller {
    private $_ci;
    private $_total_page_print = 0;
    private $_fpdi;

    public function __construct() {
        parent::__construct ();
        // Load model for department management
        $this->load->model ( 'advance_payment_model' );
        $this->load->model ( 'closing_accountant_model');
        $this->_ci = & get_instance ();
        $this->_ci->load->library ( 'tcpdf/tcpdf' );
    }
    public function show() {
        ini_set("memory_limit","1024M");
        // Check if last month, accounting has not been closed => Get default last month
        $date = new DateTime ( 'NOW' );
        $date_for_search = new DateTime ( 'NOW' );
        $interval = new DateInterval ( 'P1M' );
        $date->sub ( $interval );
        $last_month = false;
        if (! $this->advance_payment_model->check_last_month_closed_accounting ( ( string ) $date->format ( 'Y-m' ) )) {
            $date_for_search->sub ( $interval );
            $last_month = true;
        }
        // End check if last month, accounting has not been closed => Get default last month

        // Date from begins at the first day of current month
        $date_from = $date_for_search->format ( "Y" ) . "-" . $date_for_search->format ( "m" ) . "-01";

        // Date to begins at the last day of current month
        $last_day_of_month = cal_days_in_month ( CAL_GREGORIAN, $date_for_search->format ( "m" ), $date_for_search->format ( "Y" ) );
        $date_to = ""; //$date_for_search->format ( "Y" ) . "-" . $date_for_search->format ( "m" ) . "-" . $last_day_of_month;

        // Get all order acceptances of current month
        $data_order_acceptances = $this->advance_payment_model->search_order_acceptance ( $date_from, $date_to, "", "", "" );
        // Get all close date of accouting confirms
        $accounting_confirms = $this->advance_payment_model->get_accounting_confirm ();

        // $list_order_acceptance_limit_by_time_to: Data which used to get value of first day of each month
        $list_order_acceptance_limit_by_time_to = $this->advance_payment_model->get_all_order_acceptance_limit_by_time_to($date_to, "", "", "");
        // Sort data and mix with accounting confirms
        $subcontractors = $this->data_processing ( $data_order_acceptances, $accounting_confirms, $date_from, $date_to, $list_order_acceptance_limit_by_time_to);
        // Check authority
        $result = $this->auth->has_permission ( "advance_payment_edit", 'column' );

        // Create data to send to view by json encode at first load
        $data = json_encode ( array (
                'subcontractors' => $subcontractors,
                'has_authority' => $result,
                'last_month' => $last_month,
                'date_from' => $date_from,
                'date_to' => $date_to,
                'time_frame' => "",
                'nearest_closed_accounting' => $this->advance_payment_model->get_nearest_closed_accounting ()
        ) );
        $this->assign ( "data_list", $data );
        $this->view ( 'advance_payment/show.tpl' );
    }

    /**
     * get_data when user click search
     */
    public function get_data() {
        if ($post = json_decode ( $this->input->raw_input_stream )) {
            $date_from = $post->datepicker_from;
            $date_to = $post->datepicker_to;
            // print_r($post);
            // exit;
            $arr_min_max_date = $this->advance_payment_model->get_min_max_delivery_date();
            $time_frame = "";
            if($date_from=="" && $date_to=="") {
                // Get biggest and smallest value from delivery date from db to show
                $time_frame = $arr_min_max_date['time_frame'];
            }
            // <!--------------------------------------------------- Validate time search from ---------------------------------------------->
            if($date_from!="") {
                $parts = $this->separate_string ( $date_from );
                $date_from = $parts [0] . "-" . $parts [1] . "-01";
            }
            // End validate time search from

            // <!----------------------------------------------------------- Validate time search to ----------------------------------------->
            if($date_to!="") {
                $parts = $this->separate_string ( $date_to );
                // int cal_days_in_month ( int $calendar , int $month , int $year )
                $last_day_of_month = cal_days_in_month ( CAL_GREGORIAN, $parts [1], $parts [0] );
                $date_to = $parts [0] . "-" . $parts [1] . "-" . $last_day_of_month;
            }
            // End validate time search to
            // Get subcontractor code
            $sub_code = $post->subcontractor_code;
            // Subcontractor name
            $sub_name = $post->subcontractor_name; // trim($this->input->post("subcontractor_name"));
                                                   // Subcontractor abbr name
            $sub_abbr_name = $post->subcontractor_abbr_name; // trim($this->input->post("subcontractor_abbr_name"));
                                                             // Get list of order acceptances
            $data_order_acceptances = $this->advance_payment_model->search_order_acceptance ( $date_from, $date_to, $sub_code, $sub_name, $sub_abbr_name );
            $accounting_confirms = $this->advance_payment_model->get_accounting_confirm ();
            // $list_order_acceptance_limit_by_time_to: Data which used to get value of first day of each month
            $list_order_acceptance_limit_by_time_to = $this->advance_payment_model->get_all_order_acceptance_limit_by_time_to($date_to, $sub_code, $sub_name, $sub_abbr_name);

            $subcontractors = $this->data_processing ( $data_order_acceptances, $accounting_confirms, $date_from, $date_to, $list_order_acceptance_limit_by_time_to );
            $result = $this->auth->has_permission ( "advance_payment_edit", 'column' );

            // <------------------------------------PRINT JSON TO SHOW ON VIEW-------------------------------------->
            echo json_encode ( array (
                    'subcontractors' => $subcontractors,
                    'has_authority' => $result,
                    'date_from' => $date_from,
                    "date_to" => $date_to,
                    'nearest_closed_accounting' => $this->advance_payment_model->get_nearest_closed_accounting (),
                    'time_frame' => $time_frame
            ) );
            exit ();
        }
    }

    /**
     * 2015-06-01 or 2015/06/01 => return array(2015, 06, 01)
     * @param unknown $raw
     * @return unknown
     */
    public function separate_string($raw) {
        $delimiter = '-';
        if (strpos ( $raw, '/' ) !== false) {
            $delimiter = '/';
        }
        $parts = explode ( $delimiter, $raw );
        if($parts[1] > 0 && $parts[1] <10 && !(strpos ( $parts[1], '0' ) !== false)) {
            $parts[1] = '0'.$parts[1];
        }
        return $parts;
    }

    /**
     * Sort array following columns
     * @param unknown $array
     * @param unknown $cols
     * @return multitype:unknown
     */
    public function array_msort($array, $cols) {
        $colarr = array ();
        foreach ( $cols as $col => $order ) {
            $colarr [$col] = array ();
            foreach ( $array as $k => $row ) {
                $colarr [$col] ['_' . $k] = strtolower ( $row [$col] );
            }
        }
        $eval = 'array_multisort(';
        foreach ( $cols as $col => $order ) {
            $eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
        }
        $eval = substr ( $eval, 0, - 1 ) . ');';
        eval ( $eval );
        $ret = array ();
        foreach ( $colarr as $col => $arr ) {
            foreach ( $arr as $k => $v ) {
                $k = substr ( $k, 1 );
                if (! isset ( $ret [$k] ))
                    $ret [$k] = $array [$k];
                $ret [$k] [$col] = $array [$k] [$col];
                // $ret[$col] = $array[$k][$col];
            }
        }
        return $ret;
    }

    /**
     * save_grid when user clicks "save" button
     * @author Phong
     * @created date: 18/June/2015
     */
    public function save_grid() {
        $grid = $this->input->post ( "order_acceptances" );
        $id_subcontractor = $this->input->post ( "id_subcontractor" );
        // Save grid to database
        $list_to_insert = array ();
        $list_to_delete = array();
        $list_to_update = array();

        // Separate which to insert, which to delete and which to update
        foreach ( $grid as $row ) {
            if (isset ( $row ['is_extra_item'] ) && ($row ['is_extra_item']==1)) {

                // To update list:
                // $row['id']: This row get from DB
                // !isset($row['is_new_added_row']): Not the added row by hand in the screen
                // ($row['is_closed_acc_record']==0): Not the automatic created or closed_acc row
                // $row['is_deleted']: Must be 0 or isNaN (Not deleted)
                // $row['has_changed']: Must be true (has changed)
                if(isset($row['id']) && (!isset($row['is_new_added_row'])) && ($row['is_closed_acc_record']==0)) {
                    if(isset($row['has_changed'])) {
                        if($row['has_changed']) {
                            if(isset($row['is_deleted'])) {
                                if(($row['is_deleted'] == 0) ) {
                                    $list_to_update[] = $row;
                                }
                            } else {
                                $list_to_update[] = $row;
                            }
                        }
                    }
                }
                // To delete list
                if(isset($row['id'])) {
                    if(isset($row['is_deleted'])) {
                        if($row['is_deleted'] == 1) {
                            $list_to_delete[] = $row;
                        }
                    }
                }
                // To insert list
                if(isset($row['is_new_added_row'])) {
                    if(isset($row['is_deleted'])) {
                        if($row['is_deleted'] == 0) {
                            $list_to_insert[] = $row;
                        }
                    } else {
                        $list_to_insert[] = $row;
                    }
                }
            }
        }

        if (empty ( $list_to_update ) && empty ( $list_to_delete ) && empty ( $list_to_insert ) ) {
            $errors ['error'] = 2;
            echo json_encode ( $errors );
        } else {
            $this->db->trans_begin();
            if(!empty($list_to_insert)) {
                // $insert = $this->advance_payment_model->save_grid ( $list_to_insert, $id_subcontractor, "INSERT" );
                $this->advance_payment_model->save_grid ( $list_to_insert, "INSERT" );
            }
            if(!empty($list_to_update)) {
                // $update = $this->advance_payment_model->save_grid ( $list_to_update, $id_subcontractor, "UPDATE" );
                $this->advance_payment_model->save_grid ( $list_to_update, "UPDATE" );
            }
            if(!empty($list_to_delete)) {
                // $delete = $this->advance_payment_model->save_grid ( $list_to_delete, $id_subcontractor, "DELETE" );
                $this->advance_payment_model->save_grid ( $list_to_delete, "DELETE" );
            }

            if ($this->db->trans_status() === TRUE)
            // if($insert && $delete && $update)
            {
                $this->db->trans_commit();
                $errors ['error'] = 0;
                echo json_encode ( $errors );
            }
            else
            {
                $this->db->trans_rollback();
                $errors ['error'] = 1;
                echo json_encode ( $errors );
            }
            die;
        }
    }

    /**
     * save_all_grids
     * @author : Phong
     * @created date: 18/June/2015
     */
    public function save_all_grids() {
        $all_grids = json_decode ( $this->input->raw_input_stream, true );
        $all_grids = $all_grids ['allGrids'];
        $all_grids_shortage = array ();

        $list_to_insert = array ();
        $list_to_delete = array();
        $list_to_update = array();

        if (! empty ( $all_grids )) {
            foreach ( $all_grids as $key => $value ) {
                $value_order_acceptances = array ();
                if (count ( $value ['order_acceptances'] ) > 1) {
                    // $all_grids_shortage[$key] = $value['order_acceptances'];
                    foreach ( $value ['order_acceptances'] as $row ) {
                        if (isset ( $row ['is_extra_item'] ) && ($row ['is_extra_item']==1)) {
                        // if (isset ( $row ['can_change_or_not'] )) {
                            // $value_order_acceptances [] = $row;
                            // To update list
                            if(isset($row['id']) && (!isset($row['is_new_added_row'])) && ($row['is_closed_acc_record']==0)) {
                                if(isset($row['has_changed'])) {
                                    if($row['has_changed']) {
                                        if(isset($row['is_deleted']) ) {
                                            if(($row['is_deleted'] == 0) ) {
                                                // $row['source_subcontractor_id'] = $key;
                                                $list_to_update[] = $row;
                                            }
                                        } else {
                                            // $row['source_subcontractor_id'] = $key;
                                            $list_to_update[] = $row;
                                        }
                                    }
                                }
                            }

                            // To delete list
                            if(isset($row['id'])) {
                                if(isset($row['is_deleted'])) {
                                    if($row['is_deleted'] == 1) {
                                        $list_to_delete[] = $row;
                                    }
                                }
                            }

                            // To insert list
                            if(isset($row['is_new_added_row'])) {
                                if(isset($row['is_deleted'])) {
                                    if($row['is_deleted'] == 0) {
                                        $list_to_insert[] = $row;
                                    }
                                } else {
                                    $list_to_insert[] = $row;
                                }
                            } // End to insert list
                        } // End if (isset ( $row ['is_extra_item'] ) && ($row ['is_extra_item']==1)) {
                    } // End foreach ( $value ['order_acceptances'] as $row ) {
                } // End if (count ( $value ['order_acceptances'] ) > 1) {
            }
        } else {
            $errors ['error'] = 2;
            echo json_encode ( $errors );
        }
        if (empty ( $list_to_update ) && empty ( $list_to_delete ) && empty ( $list_to_insert ) ) {
            $errors ['error'] = 2;
            echo json_encode ( $errors );
        } else {
            $this->db->trans_begin();
            if(!empty($list_to_insert)) {
                // $insert = $this->advance_payment_model->save_grid ( $list_to_insert, $id_subcontractor, "INSERT" );
                $this->advance_payment_model->save_grid ( $list_to_insert, "INSERT" );
            }
            if(!empty($list_to_update)) {
                // $update = $this->advance_payment_model->save_grid ( $list_to_update, $id_subcontractor, "UPDATE" );
                $this->advance_payment_model->save_grid ( $list_to_update, "UPDATE" );
            }
            if(!empty($list_to_delete)) {
                // $delete = $this->advance_payment_model->save_grid ( $list_to_delete, $id_subcontractor, "DELETE" );
                $this->advance_payment_model->save_grid ( $list_to_delete, "DELETE" );
            }
            if ($this->db->trans_status() === TRUE)
            {
                $this->db->trans_commit();
                $errors ['error'] = 0;
                echo json_encode ( $errors );
            }
            else
            {
                $this->db->trans_rollback();
                $errors ['error'] = 1;
                echo json_encode ( $errors );

            }
        }
        die;
    }



    /**
     * Sort data, add accounting confirm....
     * @param unknown $data_order_acceptances
     * @param unknown $accounting_confirms
     * @param unknown $post
     * @param unknown $date_from
     * @param unknown $date_to
     * @return multitype:multitype:unknown
     */
    public function data_processing($data_order_acceptances, $accounting_confirms, $date_from, $date_to, $list_order_acceptance_limit_by_time_to) {


        // Get closest close accountant
        $closest_accountant = $this->closing_accountant_model->get_closest_closing_date();
        if(!empty($closest_accountant)) {
            $closest_accountant_date = $closest_accountant[0]['closest_closing_date'];
        }

        /*
        * subcontractors: list of array contain []:
        * ---> information: one array of header information (id, code, name, abbr_name)
        * ---> order_acceptances: array of order_acceptances []
        */
        $subcontractors = array ();
        // <---------------------------------------------------- CREATE GRID ---------------------------------------------------->
        foreach ( $data_order_acceptances as $row ) {
            if (! isset ( $subcontractors [$row ['id']] )) {
                $subcontractors [$row ['id']] ['information'] = array (
                        'id' => $row ['id'],
                        'code' => $row ['code'],
                        'name' => $row ['name'],
                        'abbr_name' => $row ['abbr_name']
                );
                $subcontractors [$row ['id']] ['order_acceptances'] = array ();
            }
            $subcontractors [$row ['id']] ['order_acceptances'] [] = array (
                    'id' => $row ['OAid'],
                    'shipping_type_cd' => $row['shipping_type_cd'],
                    // 'shipping_is_advance_payment' => $row['shipping_is_advance_payment'],
                    'client_name' => $row ['client_name'],
                    //'delivery_date' => ($row ['delivery_date']=="0000-00-00" | $row ['delivery_date']== null) ? "" : $row ['delivery_date'],
                    'delivery_date' =>$row ['delivery_date'],
                    'j_code' => $row ['j_code'],
                    'branch_cd' => $row ['branch_cd'],
                    'unit_price' => $row ['unit_price'],
                    'quantity' => $row ['quantity'],
                    'adjusted_amount' => (    (($row['close_date']!="0000-00-00") && $row['close_date']!=null)   &&($row['is_extra_item']==1))? 0 : $row['adjusted_amount'],//$row ['adjusted_amount'],
                    'order_acceptance_status' => $row ['order_acceptance_status'],
                    //'shipping_charge_tax' => $row ['shipping_charge_tax'],
//#7116: Start 9/Oct/2015
                    'shipping_charge_tax' => ($row['tax_free'] + $row['shipping_charge_tax']),
//#7116: End 9/Oct/2015
                    'is_extra_item' => $row['is_extra_item'],
                    'is_deleted' => 0,
                    'close_date' => $row['close_date'],
                    'cummulative_amount_from_previous_month' => (($row['close_date']!="0000-00-00")&&($row['is_extra_item']==1))? $row['adjusted_amount'] : 0,
                    'is_closed_acc_record' => ( (($row['close_date']!="0000-00-00") && $row['close_date']!=null) && ($row['is_extra_item']==1) )? 1 : 0,
                    'first_day_closed_acc' => ( (($row['close_date']!="0000-00-00") && $row['close_date']!=null) && ($row['is_extra_item']==1) )? 1 : 2,
                    'source_subcontractor_id' => $row['source_subcontractor_id']

                    /* ====> cummulative_amount_from_previous_month: if this row was created by closing accountant
                     * , set the accumulative amount (from adjusted_amount field) to this field
                     * ====> is_closed_acc_record: Check if this record was created by closing accountant
                     * ====> first_day_closed_acc: This for sorting. If this is not record of default first day of closed accounting, default = 2.
                     * In the other hand, default = 1 to be the first record of each month.
                     */
            );
        } // End foreach ($data_order_acceptanc

        // <------------------------------------------ADD ACCOUNTING CONFIRM TO GRID----------------------------------------------->
        $subcontractor2 = array ();
        /**
         * $subcontractors:
         * -> value['information'] => 1 array of header information (id, code, name, abbr_name)
         * -> value['order_acceptances'] => list of order acceptances []: id, branch_cd, unit_price...
         */
        // print_r($subcontractors);
        foreach ( $subcontractors as $sub => $value ) {

            /**
             * Get the smallest and biggest delivery date of each subcontractor
             * Only add row notice closed_account if the closed_account date between smallest and biggest date above
             */

            // List delivery date
            $list_date = array();
            if(count($value['order_acceptances'] > 0)) {
                // Get list of delivery date then sort as month (unique sort)
                foreach($value['order_acceptances'] as $date_value) {
                    // if($date_value['close_date']!="0000-00-00" && $date_value['close_date']!="" && $date_value['close_date']!=null) {
                        $list_date[] =  date ( "Y-m", strtotime ( $date_value['delivery_date'] ) ); // $date_value['delivery_date'];
                    // }
                }
                // Get list of months of list delivery_date
                $list_date = array_unique($list_date);
                sort($list_date);
                // Editted on 29/September/2015 - Redmine ID: 6806
                // Add default row to begin of each month data (if no added closed accounting date)
                // EX: If August has been closed, there is a record which marked this on 01/September
                // so that the next month, calculate by 01/September to the last day of September
                // so no need co insert the beginning of September. Start with October

                // Mark insert temp closed acc and its accumulative count all data before
                // (only one time after previous month row created by closed_accountant, next time in the grid auto calculate)
                $insert_temp_closed_acc = false;

                // List_date means list month of each subcontractor
                foreach($list_date as $d) {
                    // Has_closed: means this month has the beginning month record
                    // $has_closed = false;
                    /* foreach($value['order_acceptances'] as $date_value) {
                        if(($date_value['delivery_date'] == $d."-01") && ($date_value['close_date']!="0000-00-00") && ($date_value['close_date']!=null) && ($date_value['is_extra_item']==1)) {
                            $has_closed = true;
                        }
                    } */
                    // If has closed_accountant_date data
                    // => Calculate from the closed_accountant_date + 1 month the first day -> delivery date
                    if(isset($closest_accountant_date) && $closest_accountant_date!='') {
                        $closest_accountant_date = date("Y-m", strtotime($closest_accountant_date));
                        // FX: August has been closed
                        // => Move to October because September has got a closed_acc record at the beginning
                        if($d >= date('Y-m',strtotime($closest_accountant_date . "+2 months"))) {
                            // If this month has not got beginning record
                            // if(!$has_closed) {
                            // If this month has not been inserted beginning record
                            // echo "insert_temp_closed_acc:".$insert_temp_closed_acc."<br/>";
                            if(!$insert_temp_closed_acc) {
                                $begin_acc_cf = array (
                                        'client_name' => "■繰越",
                                        'delivery_date' => (string)$d."-01",
                                        'j_code' => "", // jcode & branch_cd must have although null value because for sorting item of arrays
                                        'branch_cd' => "",
                                        'first_day_closed_acc' =>1,
                                        'id'=>0
                                        // ,'cummulative_amount_from_previous_month' => $this->advance_payment_model->calculate_accumulation_at_fist_day_of_month($sub, $d."-01")
                                        ,'cummulative_amount_from_previous_month' => $this->calculate_accumulation_at_fist_day_of_month($list_order_acceptance_limit_by_time_to, $sub, $d."-01", $closest_accountant[0]['closest_closing_date'])
                                        ,'source_subcontractor_id'=>$sub // For creating new record from this record
                                );
                                // echo (string)$d."-01 <br/>";
                                $insert_temp_closed_acc = true;
                            } else {
                                $begin_acc_cf = array (
                                        'client_name' => "■繰越",
                                        'delivery_date' => (string)$d."-01",
                                        'j_code' => "", // jcode & branch_cd must have although null value because for sorting item of arrays
                                        'branch_cd' => "",
                                        'first_day_closed_acc' =>1,
                                        'id'=>0
                                        , 'source_subcontractor_id'=>$sub // For creating new record from this record
                                );
                            }
                            // Add $begin_acc_cf to the first of array
                            array_unshift ( $value ['order_acceptances'], $begin_acc_cf );
                            // }
                        } // End if($d >= date('Y-m',strtotime($closest_accountant_date . "+2 months"))) {
                    } // End if(isset($closest_accountant_date) && $closest_accountant_date!='') {
                    else {
                        if(!$insert_temp_closed_acc) {
                            $begin_acc_cf = array (
                                    'client_name' => "■繰越",
                                    'delivery_date' => (string)$d."-01",
                                    'j_code' => "", // jcode & branch_cd must have although null value because for sorting item of arrays
                                    'branch_cd' => "",
                                    'first_day_closed_acc' =>1,
                                    'id'=>0
                                    // ,'cummulative_amount_from_previous_month' => $this->advance_payment_model->calculate_accumulation_at_fist_day_of_month($sub, $d."-01")
                                    ,'cummulative_amount_from_previous_month' => $this->calculate_accumulation_at_fist_day_of_month($list_order_acceptance_limit_by_time_to, $sub, $d."-01", "")
                                    ,'source_subcontractor_id'=>$sub // For creating new record from this record
                            );
                            $insert_temp_closed_acc = true;
                        } else {
                            $begin_acc_cf = array (
                                    'client_name' => "■繰越",
                                    'delivery_date' => (string)$d."-01",
                                    'j_code' => "", // jcode & branch_cd must have although null value because for sorting item of arrays
                                    'branch_cd' => "",
                                    'first_day_closed_acc' =>1,
                                    'id'=>0
                                    , 'source_subcontractor_id'=>$sub // For creating new record from this record
                            );
                        }
                        // Add $begin_acc_cf to the first of array
                        array_unshift ( $value ['order_acceptances'], $begin_acc_cf );
                    }
                } // End foreach($list_date as $d) {
                // End editted on 29/September/2015 - Redmine ID: 6806
            } // End if(count($value['order_acceptances'] > 0)) {
            $subcontractor2 [$sub] = $value;
        } // End foreach ( $subcontractors as $sub => $value )
        // <---------------------------------- SORT VALUES ORDER ACCEPTANCES ------------------------------>
        $subcontractor3 = array ();
        foreach ( $subcontractor2 as $key => $value ) {
            // print_r($value["order_acceptances"]);
            $value ["order_acceptances"] = $this->array_msort ( $value ["order_acceptances"], array (
                    'delivery_date' => SORT_ASC,
                    'j_code' => SORT_ASC,
                    'branch_cd' => SORT_ASC,
                    'first_day_closed_acc'=> SORT_ASC,
                    'id'=> SORT_DESC
            ) );
            $subcontractor3 [$key] = $value;
        }
        // <------------------------------------ CHANGE KEY VALUE TO 0, 1, 2, 3 ------------------------->
        $subcontractor4 = array ();
        foreach ( $subcontractor3 as $key => $value ) {
            $va_temp = array ();
            foreach ( $value ['order_acceptances'] as $va_key => $va_value ) {
                $va_temp [] = $va_value;
            }
            $value ['order_acceptances'] = $va_temp;
            $subcontractor4 [$key] = $value;
        }
        return $subcontractor4;
    }

    /**
     * calculate_accumulation_at_fist_day_of_month
     * @param $data: $list_order_acceptance_limit_by_time_to
     * @param $sub: subcontractor id
     * @param $date: first day of beginning month
     * @param $closest_accountant_date
     * @return number
     */
    public function calculate_accumulation_at_fist_day_of_month($data, $sub, $date, $closest_accountant_date) {
        // Editted on 29/September/2015 - Redmine ID: 6806
        // EX: If August has been closed, there is a record which marked this on 01/September
        // so that the next month, calculate by 01/September to the last day of September
        // so no need co insert the beginning of September. Start with October
        if($closest_accountant_date!=null && $closest_accountant_date!='') {
            $tomorrow_of_closet_date = date('Y-m-d',strtotime($closest_accountant_date . "+1 days"));
        }
        $adjusted_amount = 0;
        $total_amount = 0;
        foreach($data as $d) {
            // From the beginning to now, have closed accountant
            if(isset($tomorrow_of_closet_date)) {
//#7116: Start 9/Oct/2015
                if(($d['source_subcontractor_id'] == $sub) && ($d['delivery_date'] >= $tomorrow_of_closet_date ) && ($d['delivery_date'] < $date)) {
                    // If this is not close_accounting record
                    // if(!($d['close_date']!= 'NULL' && $d['close_date']!='0000-00-00' && $d['is_extra_item'] == 1)) {
                    $adjusted_amount += $d['adjusted_amount'];
                    // }
                    // Phong editted 18 Dec 2015 - Change specs - Redmine ID: 8538
                    // If order_acceptance_status = 1 AND shipping_charge_tax=0 AND tax_free= 0
                    // => Total amount = price*quantity
                    // Else => Total amount = shipping_charge_tax + tax_free
                    if(($d['order_acceptance_status'] == 1) & $d['shipping_charge_tax']==0 & $d['tax_free']==0) {
                        $total_amount += f_floor(bcmul($d['unit_price'],$d['quantity']));
                    } else {
                        $total_amount += ($d['shipping_charge_tax'] + $d['tax_free']);
                    }
                }
            } else {
                if(($d['source_subcontractor_id'] == $sub) && ($d['delivery_date'] < $date)) {
                    // If this is not close_accounting record
                    // if(!($d['close_date']!= 'NULL' && $d['close_date']!='0000-00-00' && $d['is_extra_item'] == 1)) {
                    $adjusted_amount += $d['adjusted_amount'];
                    // }
                    // Phong editted 18 Dec 2015 - Change specs - Redmine ID: 8538
                    // If order_acceptance_status = 1 AND shipping_charge_tax=0 AND tax_free= 0
                    // => Total amount = price*quantity
                    // Else => Total amount = shipping_charge_tax + tax_free
                    if(($d['order_acceptance_status'] == 1) & $d['shipping_charge_tax']==0 & $d['tax_free']==0) {
                        $total_amount += f_floor(bcmul($d['unit_price'],$d['quantity']));
                    } else {
                        $total_amount += ($d['shipping_charge_tax'] + $d['tax_free']);
                    }
                }
            }
//#7116: End 9/Oct/2015
        }
        // End editted on 29/September/2015 - Redmine ID: 6806
        return ($adjusted_amount - $total_amount);
    }

    /**
     * print_pdf_report
     * @author Phong
     * @created date: 19/June/2015
     */
    public function print_pdf_report() {
        ini_set("memory_limit","1024M");
        // Check permission to access
        $result = $this->auth->has_permission ( "advance_payment_print_pdf", 'column' );
        if(!$result) {
            redirect('/_admin/error/access_deny');
        }
        // $data = $this->input->post ( "PDFData" );
        $data = json_decode ( $this->input->post ( "PDFData" ) );
        if ( ! $data) {
            show_404();
            exit;
        }
        $this->load->library('tcpdf/Tcpdf.php');
        $this->load->library('tcpdf/fpdi.php');
        $this->load->helper('zenrin_pdf_helper');

        $this->_fpdi = new FPDI();
        $regularFont     = 'msgothici';
        $this->_fpdi->setPrintHeader(false);
        $this->_fpdi->setPrintFooter(false);
        $this->_fpdi->SetAutoPageBreak(TRUE, 0);

        foreach ($data as $d) {
            $item_in_gridOption = 0;
            $data_gridOption = array();
            foreach($d->gridOption as $r) {
                /* if(
                        ((isset($r->unit_price) && $r->unit_price !=0 && $r->unit_price != ''))
                        || ((isset($r->quantity) && $r->quantity !=0 && $r->quantity != ''))
                        || ((isset($r->adjusted_amount) && $r->adjusted_amount != 0 && $r->adjusted_amount != ''))
                        //|| ((isset($r->accumulative) && $r->accumulative !=0 && $r->accumulative != ''))
                ) { */
                // If this is new added row and has not been saved to DB, not show in PDF
                if(!isset($r->is_new_added_row)) {
                    $item_in_gridOption+=1;
                    $data_gridOption[] = $r;
                }
                /* } else {
                    if(
                            (isset($r->is_closed_acc_record) && $r->is_closed_acc_record==1)
                            || (isset($r->first_day_closed_acc) && $r->first_day_closed_acc==1)
                    ) {
                        if(((isset($r->accumulative) && $r->accumulative !=0 && $r->accumulative != ''))) {
                            $item_in_gridOption+=1;
                            $data_gridOption[] = $r;
                        }
                    }
                } */
            }

            if($item_in_gridOption!=0) {
                // Create template (file pdf only, no data)
                // $template_configs = $this->_get_template_from_data($d->gridOption);
                $template_configs = $this->_get_template_from_data($data_gridOption);
                // Fill data to pdf file
                $this->_generate_pdf_data($d->name, $data_gridOption, $template_configs);
            }
        }

        $this->_fpdi->Output();
    }

    /**
     * Get template config from data
     * @param array $data
     * @return array
     * @author hoang_minh
     * @since 2015-08-12
     */
    private function _get_template_from_data($data) {
        if (empty($data)) {
            return false;
        }

        // Get number pages to generate data

        $number_rows = count($data);
        // First page contain 50 lines of records
        if ($number_rows < 51) {
            $number_pages = 1;
        } else {
            // First + second pape contain 102 lines
            $number_pages = 2;
            $number_rows -= 100;
            // From second page to last, 50 lines a page
            while ($number_rows > 0) {
                $number_rows -= 50;
                $number_pages++;
            }
        }

        // Prepare template for data
        $template_configs = array();

        // Data contains only 1 page
        if ($number_pages == 1) {
            $template_configs[0]['path'] = APPPATH . 'config/pdf_template/advance_payment/PDF_TEMPLATE.pdf';
            $template_configs[0]['posX'] = 43;
            $template_configs[0]['page_number'] = 1 + $this->_total_page_print;
            $template_configs[0]['begin'] = 0;
            $template_configs[0]['end'] = count($data);
        } else if ($number_pages == 2) {
            // Data contains 2 pages
            $template_configs[0]['path'] = APPPATH . 'config/pdf_template/advance_payment/PDF_TEMPLATE.pdf';
            $template_configs[0]['posX'] = 43;
            $template_configs[0]['page_number'] = 1 + $this->_total_page_print;
            $template_configs[0]['begin'] = 0;
            $template_configs[0]['end'] = 50;

            $template_configs[1]['path'] = APPPATH . 'config/pdf_template/advance_payment/PDF_TEMPLATE.pdf';
            $template_configs[1]['posX'] = 43;
            $template_configs[1]['page_number'] = $number_pages + $this->_total_page_print;
            $template_configs[1]['begin'] = 50;
            $template_configs[1]['end'] = count($data);
        } else {
            // Data contains 3 or more pages
            // First page:
            $template_configs[0]['path'] = APPPATH . 'config/pdf_template/advance_payment/PDF_TEMPLATE.pdf';
            $template_configs[0]['posX'] = 43;
            $template_configs[0]['page_number'] = 1 + $this->_total_page_print;
            $template_configs[0]['begin'] = 0;
            $template_configs[0]['end'] = 50;
            // End first page
            $start = 50;
            // Second page => (total-1) page
            for ($i = 1; $i < $number_pages - 1; $i++) {
                $template_configs[$i]['path'] = APPPATH . 'config/pdf_template/advance_payment/PDF_TEMPLATE.pdf';
                $template_configs[$i]['posX'] = 43;
                $template_configs[$i]['page_number'] = $i + 1 + $this->_total_page_print;
                $template_configs[$i]['begin'] = $start;
                $template_configs[$i]['end'] = $start + 50;
                $start += 50;
            }
            // Last page
            $template_configs[$number_pages - 1]['path'] = APPPATH . 'config/pdf_template/advance_payment/PDF_TEMPLATE.pdf';
            $template_configs[$number_pages - 1]['posX'] = 43;
            $template_configs[$number_pages - 1]['page_number'] = $number_pages + $this->_total_page_print;
            $template_configs[$number_pages - 1]['begin'] = $start;
            $template_configs[$number_pages - 1]['end'] = count($data);
            // End last page
        }
        // Total page of all data = sum of each all page of each data
        $this->_total_page_print += $number_pages;
        return $template_configs;
    }

    /**
     * Generate pdf data
     * @param array $data
     * @param string $template
     * @author hoang_minh
     * @since 2015-08-12
     */
    private function _generate_pdf_data($data, $data_gridOption, $template_configs) {
        $page = 1;
        // Each item of template_configs reflects a pdf page
        $total_page = count($template_configs);
        $regularFont = 'msgothici';

        foreach ($template_configs as $cfg) {
            // Vertical coordinate axis (auto + 6 each line)
            $y = $cfg['posX'];
            // Add a format pdf: Left and type A4
            $this->_fpdi->AddPage('L', 'A4');
            // $cfg['path']: Source template
            $this->_fpdi->setSourceFile($cfg['path']);

            $header_id = $this->_fpdi->importPage(1);
            $this->_fpdi->useTemplate($header_id, null, null, null, null, true);
            $this->_fpdi->setPage($cfg['page_number']);
            // Square header and title header
            {
                $y_square_header = 30;
                // Square
                $this->_fpdi->setXY(7, 12);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(242, 25, "", 1, '', 'L');
                $this->_fpdi->SetFont($regularFont, '', 21);
                // setXY(X,Y) : X: Horizental, Y: Vertical
                $this->_fpdi->setXY(8, 13);
                $this->_fpdi->cell(23.9, 6, '■'.$data. ' 御中', 0, '', 'L');

                $this->_fpdi->SetFont($regularFont, '', 9);

                $this->_fpdi->setXY(8, $y_square_header);
                $this->_fpdi->cell(20, 6, '本日付で、￥', 0,'','L');

                $this->_fpdi->setXY(50, $y_square_header);
                $this->_fpdi->cell(25, 6, '入金いたしました。', 0,'','L');

                $this->_fpdi->setXY(90, $y_square_header);
                $this->_fpdi->cell(15, 6, '月', 0,'','L');

                $this->_fpdi->setXY(108, $y_square_header);
                $this->_fpdi->cell(15, 6, '日にはご確認頂けます。よろしくお願いいたします。', 0,'','L');
                // End square

                $y_header = 37.5;
                $this->_fpdi->setXY(13, $y_header);
                $this->_fpdi->cell(15, 6, '発送日', 0,'','L');

                $this->_fpdi->setXY(33.5, $y_header);
                $this->_fpdi->cell(15, 6, 'Jコード', 0,'','L');

                $this->_fpdi->setXY(52, $y_header);
                $this->_fpdi->cell(15, 6, '枝', 0,'','L');

                $this->_fpdi->setXY(93, $y_header);
                $this->_fpdi->cell(15, 6, '会社名', 0,'','L');

                $this->_fpdi->setXY(142, $y_header);
                $this->_fpdi->cell(15, 6, '単価', 0,'','L');

                $this->_fpdi->setXY(158, $y_header);
                $this->_fpdi->cell(15, 6, '件数', 0,'','L');

                $this->_fpdi->setXY(175, $y_header);
                $this->_fpdi->cell(15, 6, '合計額', 0,'','L');

                $this->_fpdi->setXY(200, $y_header);
                $this->_fpdi->cell(15, 6, '入金', 0,'','L');

                $this->_fpdi->setXY(220, $y_header);
                $this->_fpdi->cell(15, 6, '累計額', 0,'','L');

                $this->_fpdi->setXY(238, $y_header);
                $this->_fpdi->cell(15, 6, '確定', 0,'','L');
            }
            // End square header and title header

            // Generate data
            //$rowData = $data->gridOption;
            $rowData = $data_gridOption;
            // Generate data for each page
            for ($i = $cfg['begin']; $i < $cfg['end']; $i++) {
                // Set value for status column
                $status="";
                if (isset($rowData[$i]->order_acceptance_status)) {
                    if($rowData[$i]->order_acceptance_status == 2) {
                        $status="確定";
                    }
                }
                // set data
                // Delivery date
                // setXY(X,Y) : X: Horizental, Y: Vertical
                $this->_fpdi->setXY(7, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                // Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])
                // w: Cell width. If 0, the cell extends up to the right margin.
                // h: Cell height. Default value: 0.
                // txt: String to print. Default value: empty string.
                // border: 0: no border - 1: frame
                // ln: 0: to the right - 1: to the beginning of the next line - 2: below
                // align: L or empty string: left align (default value) C: center R: right align
                // fill: Indicates if the cell background must be painted (true) or transparent (false). Default value: false.
                // link: URL or identifier returned by AddLink()
                $this->_fpdi->cell(23, 6, date('Y/m/d', strtotime($rowData[$i]->delivery_date)), 1, '', 'R');
                // J_code
                $this->_fpdi->setXY(30, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(20, 6, (isset($rowData[$i]->j_code)? $rowData[$i]->j_code: ""), 1, '', 'L');
                // Branch cd
                $this->_fpdi->setXY(50, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(9, 6, (isset($rowData[$i]->branch_cd) ? $rowData[$i]->branch_cd : ""), 1, '', 'L');
                // Client name
                $stretch = 0;
                $c = $rowData[$i]->client_name;
                // If client name is too long => Auto manage to fit with the cell
                if(mb_strlen($c, 'UTF-8') > 25) {
                    $stretch = 1;
                    // If client name is more than 40 chars, cut the rest
                    if(mb_strlen($c, 'UTF-8') > 40) {
                        $c = mb_substr($c, 0, 40, 'UTF-8');
                    }
                }
                $this->_fpdi->setXY(59, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(80, 6, $c, 1, '', 'L',false,'',$stretch);
                // $this->_fpdi->cell(80, 6, $rowData[$i]->client_name, 1, '', 'L');
                $this->_fpdi->SetFont($regularFont, '', 9);
                // Unit price 単価
                // $up = $rowData[$i]->unit_price;
                // $stretch = 0;
                /* if(mb_strlen($up, 'UTF-8') > 9) {
                    $stretch = 1;
                } */
                $this->_fpdi->setXY(139, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(16, 6, (isset($rowData[$i]->unit_price) && $rowData[$i]->unit_price !=0)  ? number_format(floatval($rowData[$i]->unit_price), 2, '.', ',') : '', 1, '', 'R', false,'', true);
                // Quantity, 件数
                /* $q = $rowData[$i]->quantity;
                $stretch = 0;
                if(mb_strlen($q, 'UTF-8') > 9) {
                    $stretch = 1;
                } */
                $this->_fpdi->setXY(155, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(16, 6, (isset($rowData[$i]->quantity)&& $rowData[$i]->quantity!=0)? number_format(floatval($rowData[$i]->quantity)) : '', 1, '', 'R', false, '', true);
                // Total Amount, 合計額
                /* $ta = $rowData[$i]->totalAmount;
                $stretch = 0;
                if(mb_strlen($ta, 'UTF-8') > 12) {
                    $stretch = 1;
                } */
                $this->_fpdi->setXY(171, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(22, 6, (isset($rowData[$i]->totalAmount)&& $rowData[$i]->totalAmount != 0)? number_format(floatval($rowData[$i]->totalAmount)) : '', 1, '', 'R', false, '', true);
                // Adjusted Amount, 入金
                /* $aa = $rowData[$i]->adjusted_amount;
                $stretch = 0;
                if(mb_strlen($aa, 'UTF-8') > 12) {
                    $stretch = 1;
                } */
                $this->_fpdi->setXY(193, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(22, 6, (isset($rowData[$i]->adjusted_amount) && $rowData[$i]->adjusted_amount) ? number_format(floatval($rowData[$i]->adjusted_amount)) : '', 1, '', 'R', false, '', true);
                // Accumulative, 累計額
                /* $acc = $rowData[$i]->accumulative;
                $stretch = 0;
                if(mb_strlen($acc, 'UTF-8') > 12) {
                    $stretch = 1;
                } */
                $this->_fpdi->setXY(215, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(22, 6, (isset($rowData[$i]->accumulative) && $rowData[$i]->accumulative!=0) ? number_format(floatval($rowData[$i]->accumulative)) : '0', 1, '', 'R', false, '', true);
                // Status
                $this->_fpdi->setXY(237, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(12, 6, $status, 1, '', 'R');

                $y += 6;
            } // End for ($i = $cfg['begin']; $i < $cfg['end']; $i++) => Generate data for each page

            // Remark pagination
            if ($total_page > 1) {
                $this->_fpdi->setXY(119, 347);
                $this->_fpdi->cell(20, 6, $page . '／' . $total_page . 'ページ', 0, '', 'L');
            }

            // If this is the last page
            if ($page == $total_page) {
                $this->_fpdi->setXY(209, 347);
                $this->_fpdi->cell(16.9, 6, date('Y/m/d H:i'), 0, '', 'L');

                // Bottom text
                $this->_fpdi->SetFont($regularFont, '', 9);
                $this->_fpdi->setXY(15, 347);
                $this->_fpdi->cell(60, 6, '（株）ゼンリンビズネクサス　ブレーンコントロール課', 0,'','L');

                // Count remain rows to insert null rows
               $total_fake_rows = 50 - $i;
                /* if ($total_page > 1) {
                    // $total_fake_rows = 54 - $i;
                    $total_fake_rows = 50 - $i;
                } */
                // Insert null rows
                for ($i = 0; $i < $total_fake_rows; $i++) {
                    $this->_fpdi->setXY(7, $y);
                    $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                    $this->_fpdi->cell(23, 6, '', 1, '', 'R');

                    $this->_fpdi->setXY(30, $y);
                    $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                    $this->_fpdi->cell(20, 6, '', 1, '', 'L');

                    $this->_fpdi->setXY(50, $y);
                    $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                    $this->_fpdi->cell(9, 6, '', 1, '', 'L');

                    $this->_fpdi->setXY(59, $y);
                    $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                    $this->_fpdi->cell(80, 6, '', 1, '', 'L');

                    $this->_fpdi->setXY(139, $y);
                    $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                    $this->_fpdi->cell(16, 6, '', 1, '', 'L');

                    $this->_fpdi->setXY(155, $y);
                    $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                    $this->_fpdi->cell(16, 6, '', 1, '', 'L');
                    // Total Amount, 合計額
                    $this->_fpdi->setXY(171, $y);
                    $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                    $this->_fpdi->cell(22, 6, '', 1, '', 'R');
                    // Adjusted Amount, 入金
                    $this->_fpdi->setXY(193, $y);
                    $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                    $this->_fpdi->cell(22, 6, '', 1, '', 'R');
                    // Accumulative, 累計額
                    $this->_fpdi->setXY(215, $y);
                    $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                    $this->_fpdi->cell(22, 6, '', 1, '', 'R');
                    // Status
                    $this->_fpdi->setXY(237, $y);
                    $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                    $this->_fpdi->cell(12, 6, '', 1, '', 'R');

                    $y += 6;
                } // End for ($i = 0; $i < $total_fake_rows; $i++) => Insert null rows
            }
            $page++;
        }
    }
}
