<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @author van_don
 *
 */
class Authenticate extends ZR_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('encrypt');
    }

    /**
     * Processing login
     * @author van_don
     */
    public function login() {
        $modules = config_item('modules');
        $current_module = $this->module;
        $default_controller = $modules[$current_module]['default_controller'];

        $this->load->library('session');
        $missing_url = $this->session->userdata('missing_url');
        if ($missing_url) {
            $default_controller = $missing_url;
        }

        if (!$this->auth->authenticate()) {
            if ($values = $this->input->post()) {
                $this->load->model('account_model');
                $message = '';
                if ($this->account_model->validate('login')) {

                    try {
                        // Check username and password
                        if (!$this->auth->login($values['login_id'], $values['password'])) {
                            throw new Exception();
                        }

                        // create logs file for login
                        $message .= 'Datetime: [' . date('Y/m/d H:i:s') . "] logined (MyPage) creator_id: ".  $this->auth->get_username() . "\r\n";
                        $this->auth->create_log_login($message);
                        if ($this->auth->is_update_password()) {
                            redirect('/_admin/authenticate/change_password');
                        }
                        redirect($default_controller);
                    } catch(Exception $ex) {
                        $this->assign('err_msg', lang('erro_login_is_incorrect'));
                    }
                }
            }
            $this->view('authenticate/login.tpl');
        } else {
            redirect($default_controller);
        }
    }

    /**
     * Reset password when longer 3 months.
     * @author van_don
     */
    public function change_password() {
        if ($this->auth->authenticate() && $this->auth->is_update_password()) {
            if ($values = $this->input->post()) {
                $this->load->model('account_model');
                $message = '';
                if ($this->account_model->validate('change_password')) {
                    try {
                        $is_rollback = false;
                        //Get current account id
                        if (!$account_id = $this->auth->get_account_id()) {
                            throw new Exception();
                        }

                        //Get current info account
                        if (!$current_user = $this->account_model->get_account_by_id($account_id)) {
                            throw new Exception();
                        }

                        //Compare current password and new password
                        if (md5($values['new_password']) == $current_user['password']) {
                            $message = lang('err_equal_old_password');
                            throw new Exception();
                        }

                        //Get old password from history
                        if ($history_password = $this->account_model->get_password_log_by_account_id($account_id)) {
                            foreach ($history_password as $key => $history) {
                                if (md5($values['new_password']) == $history['password']) {
                                    $message = lang('err_equal_old_password');
                                    throw new Exception();
                                }
                            }
                        }

                        //Begin transaction
                        $this->db->trans_begin();

                        //Insert update new password
                        $update = array(
                                'password' => md5($values['new_password']),
                                'lastup_account_id' => $account_id
                        );
                        if (!$this->account_model->update_account($update, $account_id)) {
                            $message = lang('err_can_not_change_password');
                            throw new Exception();
                        }
                        $is_rollback = true;
                        //Insert new password to history
                        $insert_data = array();
                        $insert_data['account_id'] = $account_id;
                        $insert_data['password'] = md5($values['new_password']);
                        $insert_data['lastup_account_id'] = $account_id;

                        if (!$this->account_model->insert_account_password_log($insert_data)) {
                            $message = lang('err_can_not_change_password');
                            throw new Exception();
                        }

                        //Write log
                        $this->app_logger->info_log('Account id: ' . $account_id . ' has changed password: ' . $current_user['password'] . ' to ' . $values['new_password']);

                        //Transaction commit
                        $this->db->trans_commit();
                        $this->auth->logout();

                        //Login againt to update status change password
                        $this->auth->login($current_user['login_id'], $values['new_password']);

                        //Redirect to default method
                        redirect('_admin/account/show');
                    } catch (Exception $ex) {

                        $this->assign('err_msg', $message);
                        $this->app_logger->error_log('Can not change password.' . print_r($this->db->last_query(), true));
                        if ($is_rollback) {
                            $this->db->trans_rollback();
                        }
                    }
                }
            }
            $this->view('authenticate/change_password.tpl');
        } else {
            redirect('_admin/authenticate/login');
        }
    }
    /**
     * Action: Logout
     */
    public function logout() {
        $this->auth->logout();
        redirect('_admin/authenticate/login');
    }
}

/* End of file authenticate.php */
/* Location: {module_location}/mypage/controllers/authenticate.php */
