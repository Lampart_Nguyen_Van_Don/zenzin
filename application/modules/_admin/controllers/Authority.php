<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Authority management
 * @Created date May 14 2015
 * @author Phong
 *
 */
class Authority extends ZR_Controller {

    /**
     * Show all authorities
     * @author Phong
     * @return
     */
    public function show() {
        $this->load->model('authority_model');
        $authorities = $this->authority_model->set_pager()->get_authorities();
        if(!empty($authorities)) {
            $this->assign('authorities', $authorities);
        }
        $this->view('authority/show.tpl');
    }

    /**
     * Add new authority
     * @author Phong
     * @return
     */
    public function add() {
        // Check if user uses direct link
        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        // Load models
        $this->load->model('authority_model');
        $this->load->model('action_model');
        $this->load->model('Action_group_model');

        // If user click in show page to go to add page => Get data & go to add page
        if($this->input->post('go_to_add_page')) {

            // Get all groups in action_group table
            $list_groups = $this->Action_group_model->get_all_name_and_id();

            //Get group authority setting in table "quick_setting"
            $quick_settings = array();
            if ($quick_setting = $this->authority_model->get_items("quick_setting")) {
                foreach ($quick_setting as $key => $row) {
                    $quick_settings[$row['action_group_id']][] = $row;
                }
            }

            // Assign values & go to view
            $this->assign('quick_settings', $quick_settings);
            $this->assign('list_groups', $list_groups);
            $this->view('authority/add.tpl');
        } else if ($this->input->post('save_add')) { // User click "Save" in edit page
            // Get data to save to db:
            // Get all functions that user has checked
            $list_function_save = $this->input->post('functions');
            // Get authority name
            $authority_name = $this->input->post('authority_name');
            // If validation OK
            if ($this->authority_model->validate('edit_authority', true)) {
                // Insert new authority
                $data = array('name'=>$authority_name);

                try {
                    // Start transactions
                    $this->db->trans_begin();

                    if(!$this->authority_model->insert('authority', $data)){
                        throw new Exception($this->db->last_query());
                    }

                    if(!empty($list_function_save)) {
                        $insert_id = $this->db->insert_id();
                        if(!$this->authority_model->insert_functions_of_authority_by_id($insert_id, $list_function_save)) {
                            throw new Exception($this->db->last_query());
                        }
                    }

                    // Commit transactions
                    $this->db->trans_commit();

                    $errors ['error'] = 0;
                    echo json_encode ( $errors );
                } catch (Exception $e) {
                    // Rollback transactions
                    $this->db->trans_rollback();
                    // Write to log
                    $this->app_logger->error_log($e->getMessage());
                    // Send messages to view
                    $errors ['error'] = 2;
                    $errors ['error_message'] = $e->getMessage();
                    $errors ['error_name'] = lang('msg_insert_fail');
                    echo json_encode ( $errors );
                }
            } else { // Validation fail
                $errors ['error'] = 1;
                $errors ['authority_name'] = form_error ( 'authority_name' );
                echo json_encode ( $errors );
            }
        }
    }

    /**
     * Edit authority
     * @author Phong
     * @return
     */
    public function edit() {

        // Add on 11/5/2015
        // Check if user uses direct link
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->model('authority_model');
        $this->load->model('action_group_model');

        // If user click in view detail page to go to edit page (not click save)
        if($this->input->post('to_edit_page')) {

            // Get id and authority
            if($authority_id = $this->input->post('authority_id')) {

                $authority = $list_groups = $quick_setting = array();

                // Get authority and its functions, its groups which this authority can interact
                $authority = $this->authority_model->get_authority_name_by_id($authority_id);

                $list_groups = $this->action_group_model->get_all_name_and_id();

                //Get group authority setting in table "quick_setting"
                $quick_settings = array();
                if ($quick_setting = $this->authority_model->get_items("quick_setting")) {
                    foreach ($quick_setting as $key => $row) {
                        $quick_settings[$row['action_group_id']][] = $row;
                    }
                }

                //Get authority in table "action_authority"
                $action_authorities = array();
                if ($authorities_data = $this->authority_model->get_action_authorities_by_authority_id($authority_id)) {
                    foreach ($authorities_data as $key => $row) {
                        $action_authorities[$row['quick_setting_id']][$row['authority_id']] = $row;
                    }
                }

                // Assign values to view
                $this->assign('list_groups', $list_groups);
                $this->assign('authority', $authority);

                $this->assign('quick_settings', $quick_settings);
                $this->assign('action_authorities', $action_authorities);
            }
            $this->view('authority/edit.tpl');
        } else if($this->input->post('save_edit_page')) { // If user click save in edit page

            // Get all functions which user has checked
            $list_function_save = $this->input->post('functions');

            // Get authority id and name (name perharp has been changed)
            $authority_id = $this->input->post('authority_id');
            $authority_name = $this->input->post('authority_name');

            $is_val  = false;

            $authority = $this->authority_model->get_authority_name_by_id($authority_id);

            if ($authority['name'] != $authority_name) {
                $is_val = true;
            }

            // Validate authority name
            if ($this->authority_model->validate('edit_authority', $is_val)) {

                // Update name of authority:
                $data = array('name'=>$authority_name);
                $condition = array('id'=>$authority_id);

                //Get authority in table "action_authority"
                $up_authority = array();
                if ($authorities_data = $this->authority_model->get_action_authorities_by_authority_id($authority_id)) {
                    foreach ($authorities_data as $key => $row) {
                        $up_authority[$row['quick_setting_id']] = $row['quick_setting_id'];
                    }
                }

                try {
                    // Transaction starts
                    $this->db->trans_begin();

                    // Update name of authority
                    if(!$this->authority_model->update("authority", $data, $condition)){
                        throw new Exception($this->db->last_query());
                    }

                    $new_authority = array();
                    if ($list_function_save) {
                        foreach ($list_function_save as $key => $val) {
                            if (isset($up_authority[$val])) {
                                unset($up_authority[$val]);
                            } else {
                                $new_authority[] = $val;
                            }
                        }
                    }

                    // Delete old relations
                    if ($up_authority) {
                        if(!$this->authority_model->remove_authority($this->auth->get_account_id(), $authority_id, $up_authority)) {
                            throw new Exception($this->db->last_query());
                        }
                    }

                    // Insert new relations
                    if(!empty($new_authority)) {
                        if(!$this->authority_model->insert_functions_of_authority_by_id($authority_id, $new_authority)) {
                            throw new Exception($this->db->last_query());
                        }
                    }

                    // Commit transaction OK
                    $this->db->trans_commit();

                    // All is OK
                    $errors ['error'] = 0;
                    echo json_encode($errors);

                } catch (Exception $e) {

                    // Rollback transaction
                    $this->db->trans_rollback();

                    // Write to log
                    $this->app_logger->error_log($e->getMessage());

                    // Show error to view
                    $errors['error'] = 2;
                    $errors['error_name'] = lang('msg_update_fail');
                    echo json_encode($errors);
                }
            } else {
                // Validate fail
               $errors['error'] = 1;
               $errors['authority_name'] = form_error('authority_name');
               echo json_encode($errors);
            }
        }
    }

    /**
     * delete
     * @author Phong
     * @return
     */
    public function delete() {
        // Check if user uses direct link
        if (! $this->input->is_ajax_request ()) {
            show_404 ();
        }
        $this->load->model('authority_model');
        $authority_id = $this->input->post('authority_id');
        // Check authority exist
        $list = array_filter($this->authority_model->check_authority_exist($authority_id));
        if (!empty($list)) {
            // Check account belong to this authority (If yes, no delete because of constrain => Go to else)
            if($this->authority_model->check_accounts_constrain_authority($authority_id)) {
                $condition = array('id'=>$authority_id);
                $this->authority_model->delete("authority", $condition);
                // Json
                $errors ['error'] = 0;
                echo json_encode ( $errors );
            } else {
                // Delete fail because of constrains
                $errors ['error'] = 1;
                $errors ['error_name'] = lang('msg_could_not_delete');
                echo json_encode ( $errors );
            }
        } else {
            $errors ['error'] = 1;
            $errors ['error_name'] = lang('common_record_not_exist');
            echo json_encode ( $errors );
        }
    }

    /**
     * View detail of authority
     * @author Phong
     * @return
     */
    public function detail() {

        // Check if user uses direct link
        if (! $this->input->is_ajax_request ()) {
            show_404 ();
        }

        if($id_authority = $this->input->post('id')) {
            $this->load->model('authority_model');

            $authority =  $list_functions = $list_groups = $quick_setting = array();

            // Get authority and its functions, its groups which this authority can interact
            $authority = $this->authority_model->get_authority_name_by_id($id_authority);

            $this->load->model('action_group_model');
            $list_groups = $this->action_group_model->get_all_name_and_id();

            //Get group authority setting in table "quick_setting"
            $quick_settings = array();
            if ($quick_setting = $this->authority_model->get_items("quick_setting")) {
                foreach ($quick_setting as $key => $row) {
                    $quick_settings[$row['action_group_id']][] = $row;
                }
            }

            //Get authority in table "action_authority"
            $action_authorities = array();
            if ($authorities_data = $this->authority_model->get_action_authorities_by_authority_id($id_authority)) {
                foreach ($authorities_data as $key => $row) {
                    $action_authorities[$row['quick_setting_id']][$row['authority_id']] = $row;
                }
            }

            // Assign values to view
            $this->assign('list_groups', $list_groups);
            $this->assign('authority', $authority);

            //New
            $this->assign('quick_settings', $quick_settings);
            $this->assign('action_authorities', $action_authorities);
        }

        $this->view('authority/detail.tpl');
    }

    /**
     * Manage matrix which contain action group, authority and action
     * @author Phong
     */
    public function authority_features() {

        $this->load->model('authority_model');
        $list_actions = $list_actions_not_array_authority = $group_quick_setting = array();

        // Get list group actions and list authorities
        $list_group_actions = $this->authority_model->get_all_action_groups();

        $list_authorities = $this->authority_model->get_authorities();

        $this->assign('list_group_actions', $this->_make_option_data($list_group_actions));
        $this->assign('list_authorities', $list_authorities);
        $this->assign('action_groups', $list_group_actions);

        if ($this->input->post('action_group_id')) {
            $this->assign('is_post', true);
        }
        // Go to view
        $this->view('authority/authority_features.tpl');
    }

    /**
     * Get all authority
     * @author van_don
     */
    public function ajax_authority_features() {

        $post = (array)json_decode($this->input->raw_input_stream);
        $this->load->model('authority_model');
        $list_actions = $list_actions_not_array_authority = $group_quick_setting = array();

        //Get permisson on each authority
        $authority_group = $this->authority_model->get_authority_by_params($post);

        //Get all quick_setting by group_action_id
        $quick_setting = $this->authority_model->get_quick_setting_by_params($post);

        foreach ($quick_setting as $key => $val) {
            $group_quick_setting[$val['action_group_id']][] = $val;
        }

        $quick_setting_ids = $all_authorities = array();

        //Get authority
        if ($quick_setting) {
            foreach ($quick_setting as $key => $val) {
                $quick_setting_ids[] = $val['id'];
            }
            if ($tmp_authorities = $this->authority_model->get_action_authority_by_quick_setting_id($quick_setting_ids)) {
                foreach ($tmp_authorities as $key => $val) {
                    $all_authorities[$val['quick_setting_id']][$val['authority_id']] = $val;
                }
            }
        }

        // Get list group actions and list authorities
        $list_group_actions = $this->authority_model->get_all_action_groups();

        $list_authorities = $this->authority_model->get_authorities();

        $data = array();

        $is_post = false;
        if (isset($post['action_group_id']) && ($post['action_group_id'] != '')) {
           $is_post =  true;
        }

        foreach ($list_group_actions as  $key => $action_group) {

            if (isset($group_quick_setting[$action_group['id']])) {

                $item = array();
                $item['name'] = $menu_group_name = $action_group['name'];

                $authority_name = array();
                foreach ($group_quick_setting[$action_group['id']] as $key1 => $a) {
                    $authority_name = array(
                            'id' => $a['id'],
                            'name' => $a['name'],
                            'menu_group_name' => $menu_group_name
                    );

                    $menu_group_name = '';

                    foreach ($list_authorities as $key2 => $au) {
                        if (isset($all_authorities[$a['id']][$au['id']])) {
                            $authority_name['id_' . $au['id']] = '◯';
                        } else {
                            $authority_name['id_' . $au['id']] = '_';
                        }
                    }
                    $data[] = $authority_name;
                }
            }
        }
//         print_r($data);exit;
        echo $this->ajax_json(array('data' => $data));
    }

    /**
     * authority_setting: Manage authorities which can manipulate exactly action
     * @author Phong
     * @return
     * @throws Exception
     */
    public function authority_setting() {
        // Check if user uses direct link
        if (!$this->input->is_ajax_request ()) {
            show_404 ();
        }
        $this->load->model('authority_model');
        $this->load->model('action_group_model');


        $quick_setting_id = $this->input->post('quick_setting_id');

        if($this->input->post('navigate') && $quick_setting_id) {


            // Get information of current action
            $quick_setting = $this->authority_model->get_item_by_id('quick_setting', $quick_setting_id);
            
//#7568:Start
            $action_group = $this->authority_model->get_item_by_id('action_group', $quick_setting['action_group_id']);
//#7568:End
            // Get all authorities
            $list_authorities = $this->authority_model->get_authorities();

            if($group_action_id = $this->input->post("group_action_id")) {
                $this->assign('group_action_id', $group_action_id);
            }

            //Get authority
            $authorities = array();
            if ($tmp_authority = $this->authority_model->get_action_authority_by_quick_setting_id($quick_setting_id)) {
                foreach ($tmp_authority as $key => $val) {
                    $authorities[$val['authority_id']] = $val;
                }
            }

 //#7568:Start
            $this->assign('nameaction',$action_group['name']);
 //#7568:End
 
            $this->assign('authorities', $authorities);
            $this->assign('list_authorities', $list_authorities);
            $this->assign('quick_setting', $quick_setting);

            $this->view(('authority/authority_setting.tpl'));
        } else if($this->input->post('save_edit_setting') && $quick_setting_id) {

            // Click save in page authority setting:
            $list_authorities_save = $this->input->post('functions');

            //Get authority
            $authorities = array();
            if ($tmp_authority = $this->authority_model->get_action_authority_by_quick_setting_id($quick_setting_id)) {
                foreach ($tmp_authority as $key => $val) {
                    $authorities[$val['authority_id']] = $val['authority_id'];
                }
            }

            try {
                $this->db->trans_begin();

                $new_authority = array();
                foreach ($list_authorities_save as $key => $val) {
                    if (isset($authorities[$val])) {
                        unset($authorities[$val]);
                    } else {
                        $new_authority[] = $val;
                    }
                }

                // Disable all former relations
                if ($authorities) {
                    if(!$this->authority_model->remove_authority($this->auth->get_account_id (), $authorities,$quick_setting_id)) {
                        throw new Exception($this->db->last_query());
                    }
                }

                // Insert new relations
                if(!empty($new_authority)) {
                    if(!$this->authority_model->insert_authorities_of_function_by_id($quick_setting_id, $new_authority)) {
                        throw new Exception($this->db->last_query());
                    }
                }
                // Stop transaction
                $this->db->trans_commit();

                // No error
                $errors['error'] = 0;
                echo json_encode($errors);

            } catch (Exception $e) {

                // Rollback transaction
                $this->db->trans_rollback();
                // Write to log
                $this->app_logger->error_log($e->getMessage());
                // Show error to view
                $errors ['error'] = 1;
                $errors ['error_name'] = lang('msg_update_fail');
                echo json_encode ( $errors );
            }
        }
    }

    /**
     *Update sequence
     */
    public function update_sequence() {
        $data = $this->input->post();
        $this->load->helper('zenrin_system');
        update_sequence('authority', $data);
        redirect_post('_admin/authority/show');
    }
}
