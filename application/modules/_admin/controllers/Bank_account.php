<?php
/**
 * Bank Account
 * @author thanh_trung
 *
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bank_account extends ZR_Controller {

    public function __construct() {
        parent::__construct ();
        $this->load->model('bank_account_model');
        $this->load->library('auth');

    }

    public function show() {
        $data = $this->bank_account_model->get_bank_account();

        // Load view
        $this->assign('item', $data);
        $this->view('bank_account/show.tpl');
    }

    /**
     * Using AJAX
     * Load edit form with submit = edit
     * Save edit form with submit = save_edit
     */
    public function edit() {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        // Load edit form from ajax append dialog
        if ($this->input->post('name') == 'edit') {
            $data = $this->bank_account_model->get_bank_account();
            $this->assign('item', $data);
            $this->view('bank_account/edit.tpl');
        }
        // Save edit
        else if ($this->input->post('name') == 'save_edit') {

            if ($this->bank_account_model->validate('edit_bank_account')) {

                $data = array(
                    'financial_institution_name'    => $this->input->post('bank_name'),
                    'branch_name'                   => $this->input->post('branch_name'),
                    'account_number'                => $this->input->post('account_number'),
                    'account_holder'                => $this->input->post('account_holder'),
                    'lastup_account_id'             => $this->auth->get_account_id()
                );

                $this->bank_account_model->update_bank_account('zbn_account', $data, array('id' => $this->input->post('b_id')));
                $errors['error'] = 0;

                $this->ajax_json($errors);
            } else {
                $errors['error'] = 1;
                $errors['bank_name'] = form_error('bank_name');
                $errors['branch_name'] = form_error('branch_name');
                $errors['account_number'] = form_error('account_number');
                $errors['account_holder'] = form_error('account_holder');

                $this->ajax_json($errors);
            }
        }
    }

}