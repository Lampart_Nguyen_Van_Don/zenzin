<?php
class Business_unit extends ZR_Controller {
    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('business_unit_model');
    }

    public function get_all() {
        $this->load->model('business_unit_model');
        $business_units = $this->business_unit_model->get_business_unit_id_name();
        die(json_encode($business_units));
    }

    /**
     * Show list business unit
     * @author thanh_trung
     */
    public function show() {
        $business_units = array();
        $business_units = $this->business_unit_model->set_pager()->list_business_unit();

        if (!empty($business_units)) {
            $this->assign('business_units', $business_units);
        }

        $this->view('business_unit/show.tpl');
    }

    /**
     * Show dialog detail business unit when receive call ajax
     * Param: business_unit_id
     * Return: json object
     * @author thanh_trung
     */
    public function detail() {

        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        $this->load->model('gui_parts_element_model');

        $business_unit_id = $this->input->post('business_unit_id');
        $business_unit = $this->business_unit_model->get_business_unit_by_id($business_unit_id);
        
        $business_unit_template = $this->gui_parts_element_model->get_by_gui_parts_column_name('business_unit_template', 'code,name');

        foreach ($business_unit_template as $item) {
            if ($business_unit['finish_work_report_template'] == $item['code']) {
                $business_unit['finish_work_report_template_name'] = $item['name'];
            }
        }

        if (empty($business_unit)) {
            $this->ajax_json(array(
                'error_code' => 400,
                'messages' => lang('lbl_invalid_input'),
            ));
            return ;
        } else {
            $this->assign('business_unit', $business_unit);
            $this->view('business_unit/detail.tpl');
        }
    }

    /**
     * Add business unit - when receive ajax call 'save_add', validate' => ok => insert, else => errors.
     * Return: json object
     * @author thanh_trung
     */
    public function add() {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        // Add new (save_add)
        if ($this->input->post('btn_name') == 'save_add') {
            // exists is_product_division
            if ($this->input->post('txt_is_product_div')) {
                // Validate data input
                if ($this->business_unit_model->validate('add_business_unit', true)) {

                    $data = array(
                        'name'                       => $this->input->post('txt_bu_name'),
                        'j_code'                     => $this->input->post('txt_j_code'),
                        'bugyo_department_code'      => $this->input->post('txt_bugyo_department_code'),
                        'bugyo_account_code'         => $this->input->post('txt_bugyo_account_code'),
                        'bugyo_abstract_string'      => $this->input->post('txt_bugyo_abstract_string'),
                        'product_manage_code'        => $this->input->post('txt_product_mn_code'),
                        'template'                   => $this->input->post('rtb_template'),
                        'is_product_division'        => $this->input->post('txt_is_product_div'),
                        'totalization_target_months' => $this->input->post('txt_target_months'),
                        'finish_work_report_template'=> $this->input->post('work_finish_report_template'),
                        'lastup_account_id'          => $this->auth->get_account_id()
                    );

                    $insert = $this->business_unit_model->insert('business_unit', $data);
                    $status = array(
                        'status' => $insert ? true : false,
                        'message' => $insert ? lang('common_insert_successfully') : lang('lbl_invalid_id')
                    );

                    $this->ajax_json($status);
                }
                else {
                    $errors['txt_bu_name']                    = form_error('txt_bu_name');
                    $errors['txt_is_product_div']             = form_error('txt_is_product_div');
                    $errors['txt_target_months']              = form_error('txt_target_months');
                    $errors['txt_j_code']                     = form_error('txt_j_code');
                    $errors['txt_bugyo_department_code']      = form_error('txt_bugyo_department_code');
                    $errors['txt_bugyo_account_code']         = form_error('txt_bugyo_account_code');
                    $errors['txt_bugyo_abstract_string']      = form_error('txt_bugyo_abstract_string');
                    $errors['txt_product_mn_code']            = form_error('txt_product_mn_code');
                    $errors['rtb_template']                   = form_error('rtb_template');
                    $errors['work_finish_report_template']    = form_error('work_finish_report_template');

                    $status = array(
                        'status' => false,
                        'errors' => $errors,
                        'is_product_division' => true
                    );

                    $this->ajax_json($status);
                }

                // save
            } else {

                if ($this->business_unit_model->validate('business_unit_one_rule', true)) {
                    $data = array(
                        'name'                       => $this->input->post('txt_bu_name'),
                        'template'                   => '',
                        'lastup_account_id'          => $this->auth->get_account_id()
                    );

                    $insert = $this->business_unit_model->insert('business_unit', $data);
                    $status = array(
                        'status' => $insert ? true : false,
                        'message' => $insert ? lang('common_insert_successfully') : lang('lbl_invalid_id')
                    );
                    $this->ajax_json($status);
                } else {
                    $errors['txt_bu_name'] = form_error('txt_bu_name');

                    $status = array(
                        'status' => false,
                        'errors' => $errors,
                        'is_product_division' => false
                    );
                    $this->ajax_json($status);
                }
            }
        }

        // Get array code-name for work_finish_report_template
        $this->load->model('gui_parts_element_model');

        $business_unit_template = $this->gui_parts_element_model->get_by_gui_parts_column_name('business_unit_template', 'code, name');
        
        $this->assign('business_unit_template', $business_unit_template);
        $this->assign('pg', $this->input->post('pg'));
        $this->view('business_unit/add.tpl');
    }

    /**
     * when receive ajax call 'edit' => load edit.tpl
     * when receive ajax call 'save_edit' from btn_name = 'save_edit' => save content
     * params: id
     * @author thanh_trung
     */
    public function edit() {

        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        // Check id
        $business_unit = $this->business_unit_model->get_business_unit_by_id($this->input->post('id'));

        if (!$business_unit) {
            return false;
        }

        $is_val = false;
        if ($business_unit['name'] != $this->input->post('txt_bu_name')) {
            $is_val = true;
        }
        // When receive ajax save_edit
        if ($this->input->post('btn_name') == 'save_edit') {
            // have checkbox is_product_div
            if ($this->input->post('txt_is_product_div')) {
                // check duplicate name
                if ($this->business_unit_model->validate('add_business_unit', $is_val)) {
                    $data = array(
                        'name'                        => $this->input->post('txt_bu_name'),
                        'j_code'                      => $this->input->post('txt_j_code'),
                        'bugyo_department_code'       => $this->input->post('txt_bugyo_department_code'),
                        'bugyo_account_code'          => $this->input->post('txt_bugyo_account_code'),
                        'bugyo_abstract_string'       => $this->input->post('txt_bugyo_abstract_string'),
                        'product_manage_code'         => $this->input->post('txt_product_mn_code'),
                        'template'                    => $this->input->post('rtb_template'),
                        'is_product_division'         => $this->input->post('txt_is_product_div') ? 1 : 0,
                        'totalization_target_months'  => $this->input->post('txt_target_months'),
                        'finish_work_report_template' => $this->input->post('work_finish_report_template'),
                        'lastup_account_id'           => $this->auth->get_account_id()
                    );
                    // Update business unit with id
                    $update = $this->business_unit_model->update('business_unit', $data, array('id' => $this->input->post('id')));

                    $status = array(
                        'status' => $update ? true : false,
                        'message' => $update ? lang('lbl_update_success') : lang('lbl_update_failed'),
                    );

                    $this->ajax_json($status);

                } else {

                    $errors['txt_bu_name']                  = form_error('txt_bu_name');
                    $errors['txt_is_product_div']           = form_error('txt_is_product_div');
                    $errors['txt_target_months']            = form_error('txt_target_months');
                    $errors['txt_j_code']                   = form_error('txt_j_code');
                    $errors['txt_bugyo_department_code']    = form_error('txt_bugyo_department_code');
                    $errors['txt_bugyo_account_code']       = form_error('txt_bugyo_account_code');
                    $errors['txt_bugyo_abstract_string']    = form_error('txt_bugyo_abstract_string');
                    $errors['txt_product_mn_code']          = form_error('txt_product_mn_code');
                    $errors['rtb_template']                 = form_error('rtb_template');
                    $errors['finish_work_report_template']  = form_error('work_finish_report_template');
                    
                    $status = array(
                        'status' => false,
                        'errors' => $errors,
                        'is_product_division' => true
                    );

                    $this->ajax_json($status);
                }
            } else {
                if ($this->business_unit_model->validate('business_unit_one_rule', $is_val)) {
                    $data = array(
                        'name'                       => $this->input->post('txt_bu_name'),
                        'j_code'                     => '',
                        'bugyo_department_code'      => '',
                        'bugyo_account_code'         => '',
                        'bugyo_abstract_string'      => '',
                        'product_manage_code'        => '',
                        'template'                   => '',
                        'totalization_target_months' => 0,
                        'is_product_division'        => 0,
                        'lastup_account_id'          => $this->auth->get_account_id()
                    );

                    // Update business unit with id
                    $update = $this->business_unit_model->update('business_unit', $data, array('id' => $this->input->post('id')));

                    $status = array(
                        'status' => $update ? true : false,
                        'message' => $update ? lang('lbl_update_success') : lang('lbl_update_failed'),
                    );

                    $this->ajax_json($status);
                } else {
                    $errors['txt_bu_name'] = form_error('txt_bu_name');
                    $status = array(
                        'status' => false,
                        'message' => lang('lbl_insert_failed'),
                        'errors' => $errors,
                        'is_product_division' => false
                    );
                    $this->ajax_json($status);
                }
            }
        }
        
        // Get array code-name for work_finish_report_template
        $this->load->model('gui_parts_element_model');

        $business_unit_template = $this->gui_parts_element_model->get_by_gui_parts_column_name('business_unit_template', 'code, name');
        
        $this->assign('business_unit_template', $business_unit_template);

        $this->assign('business_unit', $business_unit);
        $this->view('business_unit/edit.tpl');
    }

    /**
     * Delete business unit id, just delete when this business unit (FK) dont used in department.
     * Params: business_unit_id
     * Return: json object
     * @author thanh_trung
     */
    public function delete() {

        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        if (!$this->business_unit_model->get_business_unit_by_id($this->input->post('business_unit_id'))) {
            $status = array(
                'status' => false,
                'message' => lang('lbl_invalid_id'),
            );

        } else {
            // Check exist foreign key business_unit_id in table department, if exist => cannot delete
            $this->load->model('department_model');
            $exist = $this->department_model->check_business_unit_id_exist($this->input->post('business_unit_id'));
            if (!empty($exist)) {
                $status = array(
                    'status' => false,
                    'message' => lang('lbl_delete_exist'),
                );
            } else {
                $success = $this->business_unit_model->delete_business_unit_by_id($this->input->post('business_unit_id'));
                $status = array(
                    'status' => $success ? true : false,
                    'message' => $success ? lang('lbl_delete_success') : lang('lbl_delete_error')
                );
            }
        }
        $this->ajax_json($status);
    }
}
