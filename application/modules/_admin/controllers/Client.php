<?php

class Client extends ZR_Controller {
    
    /**
     * Constructer
     */
    public function __construct() {
        parent::__construct();

        $this->load->model('client_model');
    }

    private $gui_parts = array(
        'payment_closing_date_options',
        'payment_date_options',
        'payment_month_options',
        'credit_level_options',
        'business_category_options'
    );

    /**
     * Show and filter account
     *
     * @author quang_duc
     */
    public function show() {
        $this->load->models(array('client_model', 'order_model', 'account_model', 'business_unit_model', 'gui_parts_element_model'));

        $accounts = $this->account_model->get_account_department_business_unit('', 'is_product_division', false);

//#6994:Start
        $search_params = array();
        if (!$this->input->post()) {
            $current_login_account = $this->auth->get_account_id();
            foreach ($accounts as $account) {
                if ($account['id'] == $current_login_account && $account['is_product_division'] == 1) {
                    $search_params['search_business'] = $account['business_unit_id'];
                    $search_params['search_account'] = 0;
                    break;
                }
            }
        } else {
            $search_params = $this->input->post();
        }

        $this->assign('data_search', $search_params);
        $clients = $this->client_model->filter($search_params)->order_by_s_code()->get_data(false, Client_model::LIMIT);
//#6994:End

        $date_from = $this->input->post('search_issue_s_code_date_from');
        $date_to = $this->input->post('search_issue_s_code_date_to');
        $date_errors = array();

        if ($date_from && !parse_date($date_from)) {
            $date_errors[] .= str_replace('%field%', 'From', lang('msg_invalid_date'));
        }
        if ($date_to && ((!$date_to = parse_date($date_to)) || (substr_count($date_to, '-') == 1))) {
            $date_errors[] .= str_replace('%field%', 'To', lang('msg_invalid_date'));
        }

        // Order data init
        $tax_division = json_encode($this->gui_parts_element_model->get_by_gui_parts_column_name('tax_division', 'code,name'));
        $this->order_model->set_account_follow_bussiness_unit();
        $auth_data = $this->auth->get_auth_data();
        $j_accounts = $this->order_model->get_j_account($auth_data['account_id']);
        $products = json_encode($this->order_model->get_products_by_bu_id($j_accounts[0]->bu_id));
//#9941:Start
        if ($this->auth->has_permission('order_add_edit_all', 'column')) {
//#9941:End
            $j_accounts = $this->order_model->get_j_account_by_bu_id($j_accounts[0]->bu_id);
        }

        //$od_clients = $this->order_model->get_clients();
        //$od_clients = json_encode($od_clients);
        //$this->assign('od_clients', $od_clients);
        
        $this->lang->load('order', $this->config->item('language'));

        $this->assign('model', $this->client_model);
        $this->assign('clients', $clients);
        $this->assign('account_data', $accounts);
        
        $this->assign('login_account_id', $this->auth->get_account_id());
        $this->assign('business_unit_options', map_element($this->business_unit_model->get_product_business_unit_id_name(), 'id', 'name'));
        $this->assign('date_errors', $date_errors);

        // Order assign
        $this->assign('j_accounts', $j_accounts);
        $this->assign('products', $products);
        $this->assign('tax_division', $tax_division);
        
        // get client_status name
        $client_status_name = $this->gui_parts_element_model->get_by_gui_parts_column_name('client_status', 'code, name');
        $this->assign('client_status_name', json_encode(index_element($client_status_name, 'code')));
        
        // get company title
        $company_title = $this->gui_parts_element_model->get_by_gui_parts_column_name(Gui_parts_model::COLUMN_NAME_COMPANY_TITLE, 'code, name');
        $this->assign('company_title', json_encode(index_element($company_title, 'code')));
        
        $this->assign('is_edit_all', $this->auth->has_permission('order_add_edit_all', 'column') ? 1 : 0);
        $this->assign('is_edit_only', $this->auth->has_permission('order_add_edit_his_only', 'column') ? 1 : 0);
        $this->assign('is_load', (count($clients) < Client_model::LIMIT) ? 0 : 1);
        
        $this->view('client/show.tpl');
    }

    /**
     * Export client list CSV, short form (S_code, client name)
     * (This function required client data to be input by $_POST['client_data'])
     *
     * @author quang_duc
     */
    public function export_csv() {
        /*if (!$this->input->post('client_data')) {
            redirect('/_admin/client/show');
        }
        $clients = json_decode($this->input->post('client_data'), true);
        */
        
        $search_data = json_decode($this->input->post('search_data'), true);
        $business_default = $this->input->post('business_default');
        
        if (empty($search_data)) {
            $search_data['search_business'] = $business_default;
        }
        
        $clients = $this->client_model->get_clients(null, $search_data);
        
        $header = "BCON001,BCON003";
        $content = '';

        foreach ($clients as $client) {
            $content .= "\n" . $client['s_code'] . ',' . $this->client_model->name_with_title($client['name'], $client['corporate_status_before'], $client['corporate_status_after']);
        }

        $csv = $header . $content;

        $csv = mb_convert_encoding($csv,'SJIS','UTF-8');

        $this->load->helper('download');

        $filename = filename_to_urlencode('奉行インポート用クライアントデータ_' . date('Ymd') . '.csv');
        force_download($filename, $csv);

        exit;
    }

    /**
     * Export client list CSV, full form
     * (This function required client data to be input by $_POST['client_data'])
     *
     * @author quang_duc
     */
    public function export_csv_full() {
        /*if (!$this->input->post('client_data')) {
            redirect('/_admin/client/show');
        }
        $clients = json_decode($this->input->post('client_data'), true);
        */
        
        $search_data = json_decode($this->input->post('search_data'), true);
        $business_default = $this->input->post('business_default');
        
        if (empty($search_data)) {
            $search_data['search_business'] = $business_default;
        }
//#6986:Start
        $clients = $this->client_model->get_clients(null, $search_data, array('client_credit_approval_log'));
//#6986:End
        $this->load->models(array('client_model', 'account_model'));
        $accounts = $this->account_model->get_accounts();
        $accounts = index_element($accounts, 'id');
        $bank_account = $this->client_model->get_bank_account_info();
        $bank_account = index_element($bank_account, 'client_id');
//#6986:Start
        $header = "クライアントステータス,Ｓコード,社名,社名（フリガナ）,郵便番号,住所１,住所２,代表者名,TEL,FAX,業種,部署名,お客様担当者,メールアドレス,支払条件-締め日,支払条件-支払月,支払条件-支払日,前受金フラグ,Ｓ担当営業１,Ｓ担当営業２,初回納品-納品月,初回納品-納品額,初回納品-粗利額,初回納品-粗利率,年合計-納品額,年合計-粗利額,年合計-粗利率,特記事項（背景・情報）,与信結果-与信レベル,与信結果-前受金フラグ,与信結果-与信額,コメント,与信申請日,与信担当者,与信最終更新日,与信初回回答日,クライアント承認担当者,Ｓコード発番日, 入金元口座名";
        $column_order = array('status', 's_code', 'name', 'name_kana', 'zipcode', 'address_1st', 'address_2nd', 'representative_name', 'phone_no', 'fax_no', 'business_category', 'division_name', 'charge_name', 'mail_address', 'closing_date', 'payment_month_cd', 'payment_day_cd', 'is_ad_receive_payment_request', 's_account_id_1st', 's_account_id_2nd', 'delivery_month_cd', 'delivery_amount', 'gross_profit_amount', 'gross_profit_rate', 'total_delivery_amount', 'total_gross_profit_amount', 'total_gross_profit_rate', 'remark', 'credit_level', 'is_ad_receive_payment', 'credit_amount', 'comment', 'create_datetime', 'credit_account_id', 'credit_date', 'credit_approval_log_date', 'approval_account_id', 'issue_s_code_date', 'bank_account');
//#6986:End
        $content = '';

        foreach ($clients as $client) {
            $line = "\n";
            foreach ($column_order as $column) {
                switch ($column) {
                    case 'name':
                        $line .= $this->client_model->name_with_title($client['name'], $client['corporate_status_before'], $client['corporate_status_after']);
                        break;
                    case 'status':
                        $line .= $this->client_model->status_options($client[$column]);
                        break;
//#7021:Start
                    case 'zipcode':
                        $line .= substr($client[$column], 0, 3) . '-' . substr($client[$column], 3);
                        break;
//#7021:End
                    case 'business_category':
                        $line .= $this->client_model->business_category_options($client[$column]);
                        break;
                    case 'closing_date':
                        if (!$client['is_ad_receive_payment_request']) {
                            $line .= $this->client_model->payment_closing_date_options($client[$column]);
                        }
                        break;
                    case 'payment_month_cd':
                        if (!$client['is_ad_receive_payment_request']) {
                            $line .= $this->client_model->payment_month_options($client[$column]);
                        }
                        break;
                    case 'payment_day_cd':
                        if (!$client['is_ad_receive_payment_request']) {
                            $line .= $this->client_model->payment_date_options($client[$column]);
                        }
                        break;
                    case 'credit_level':
                        $line .= $this->client_model->credit_level_options($client[$column]);
                        break;
                    case 's_account_id_1st':
                    case 's_account_id_2nd':
                    case 'credit_account_id':
                    case 'approval_account_id':
                        if (isset($accounts[$client[$column]])) {
                            $line .= $accounts[$client[$column]]['name'];
                        }
                        break;
                    case 'remark':
                    case 'comment':
                        $line .= str_replace(array("\r", "\n"), " ", $client[$column]);
                        break;
                    case 'bank_account':
                        if (isset($bank_account[$client['id']])) {
                            $line .= '"' . $bank_account[$client['id']]['bank_account'] . '"';
                        }
                        break;
//#6986:Start
                    case 'create_datetime':
                        $line .= str_replace('-', '/', substr($client[$column], 0, 10));
                        break;
                    case 'credit_date':
                    case 'credit_approval_log_date':
                    case 'issue_s_code_date':
                        $line .= str_replace('-', '/', $client[$column]);
                        break;
//#6986:End
                    default:
                        $line .= $client[$column];
                }
                $line .= ',';
            }
            $content .= rtrim($line, ',');
        }

        $csv = $header . $content;

        $csv = mb_convert_encoding($csv,'SJIS','UTF-8');

        $this->load->helper('download');

        $filename = filename_to_urlencode('クライアント_' . date('Ymd') . '.csv');
        force_download($filename, $csv);
        exit;
    }

    /**
     * View client detail
     *
     * @author quang_duc
     */
    public function detail()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->models(array('client_model', 'account_model'));
        $this->load->library('auth');

        if ((!$id = $this->input->post('id')) || (!is_numeric($id))) {
            echo lang('msg_invalid_id');
            exit;
        }

        $client = $this->client_model->filter(array('search_id' => $id))->get_data();

        if (empty($client)) {
            echo lang('msg_id_not_exist');
            exit;
        }
        $client = $client[0];

        $can_approve_client = false;

        // Get business unit id of user loggin current.
        $arr_info_user_logged = $this->account_model->get_account_department_business_unit($this->auth->get_account_id());
        $business_user_logged = $arr_info_user_logged[0]['business_unit_id'];

        // Check business unit of accounts client follow business unit user loggin.
        $accounts = $this->account_model->get_account_by_id(array($client['s_account_id_1st'], $client['s_account_id_2nd']));

        foreach ($accounts as $item) {
            $arr_info_client = $this->account_model->get_account_department_business_unit($item['id'], '', false);
            if ($arr_info_client[0]['business_unit_id'] == $business_user_logged) {
                $can_approve_client = true;
            }
        }

        // Trung: 13/08/2015
        $this->assign('client', $client);
        $this->assign('account_data', $accounts);
        $this->assign('can_approve_client', $can_approve_client);

        if ($gui_parts = $this->input->post('gui_parts')) {
            if (is_array($gui_parts)) {
                foreach ($gui_parts as $key => $config) {
                    $this->assign($key, $config);
                }
            }
        } else {
            foreach ($this->gui_parts as $option) {
                $this->assign($option, $this->client_model->{$option}());
            }
        }

        if ($dialog_selector = $this->input->post('dialog_selector')) {
            $this->assign('dialog_selector', $dialog_selector);
        }

        if (isset($_POST['from_finish_work_report'])) {
        	$this->assign('is_edit', false);
        } else {
        	$this->assign('is_edit', true);
        }

        $this->view('client/detail.tpl');
    }

    /**
     * This action process apply new application, re-apply application and delete application
     *
     * @author quang_duc
     */
    public function apply_retry_delete()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->models(array('client_model', 'account_model'));

        $accounts = $this->account_model->get_accounts();
        $accounts = map_element($accounts, 'id', 'name');
        // If this is a submit action, process submit data
        if ($action = $this->input->post('action')) {

            // Fetch POST data
            $post = fetch_post(array(
                    'status' => CLIENT_STATUS_CREDIT_WAITING,
                    'corporate_status_before' => 0,
                    'corporate_status_after' => 0,
                    'name',
                    'name_kana',
                    'zipcode',
                    'address_1st',
                    'address_2nd',
                    'representative_name',
                    'phone_no',
                    'fax_no',
                    'business_category',
                    'division_name',
                    'charge_name',
                    'mail_address',
                    'closing_date',
                    'payment_month_cd',
                    'payment_day_cd',
                    'is_ad_receive_payment_request' => 0,
                    's_account_id_1st',
                    's_account_id_2nd' => 0,
                    'delivery_month_cd',
                    'delivery_amount',
                    'gross_profit_amount',
                    'total_delivery_amount',
                    'total_gross_profit_amount',
                    'remark',
                    'comment' => ' ',
                    'lastup_account_id' => $this->auth->get_account_id(),
            ));

            $form_valid = true;
            $custom_errors = array();

            if (!$post['corporate_status_before'] && !$post['corporate_status_after']) {
                $form_valid = false;
                $custom_errors['corporate_status_before'] = str_replace('%field%', lang('lbl_company_title'), lang('msg_field_required'));
            }

            if (!$post['is_ad_receive_payment_request']) {
                if (!$this->client_model->check_payment_date($post['closing_date'], $post['payment_month_cd'], $post['payment_day_cd'])) {
                    $form_valid = false;
                    $custom_errors['payment_day_cd'] = lang('msg_payment_date_after_closing_date');
                }
            }

            // Process new application action
            if ($action == 'apply') {
                $form_valid = $this->client_model->validate('apply') && $form_valid;
                if ($form_valid) {
                    if ($this->client_model->apply($post)) {
                        $result = array(
                                'status'  => AJAX_SUCCESS,
                                'message' => lang('common_bulk_successfully_edit'),
                        );
                    } else {
                        $result = array(
                                'status'  => AJAX_FAIL,
                                'message' => lang('common_could_not_add'),
                        );
                    }
                }
            } else {
                if ($id = $this->input->post('id')) {
                    // Process re-apply action
                    if ($action == 'reapply') {
                        $form_valid = $this->client_model->validate('apply') && $form_valid;
                        if ($form_valid) {
                            if ($this->client_model->reapply($id, $post)) {
                                $result = array(
                                        'status'  => AJAX_SUCCESS,
                                        'message' => lang('common_edit_successfully'),
                                );
                            } else {
                                $result = array(
                                        'status'  => AJAX_FAIL,
                                        'message' => lang('common_could_not_edit'),
                                );
                            }
                        }
                    // Process delete action
                    }  elseif ($action == 'check_order') {
                        $this->_check_order($id);
                    } elseif ($action == 'delete') {
                        if ($this->client_model->delete('client', $id)) {
                            $result = array(
                                    'status'  => AJAX_SUCCESS,
                                    'message' => '',
                            );
                        } else {
                            $result = array(
                                    'status'  => AJAX_FAIL,
                                    'message' => lang('common_could_not_delete'),
                            );
                        }

                        $this->ajax_json($result);
                    }
                } else {
                    $result = array(
                            'status'  => AJAX_FAIL,
                            'message' => lang('msg_invalid_id'),
                    );
                }
            }

            if (!$form_valid) {
                $result = array(
                        'status'  => AJAX_FAIL,
                        'message' => '',
                        'errors'  => $this->form_validation->error_array() + $custom_errors
                );
            } else {
                if ($result['status'] == AJAX_SUCCESS) {
                    $this->load->library('email_template');

                    if ($post['is_ad_receive_payment_request']) {
                        $payment_term = lang('lbl_advance_payment');
                    } else {
                        $payment_term = lang('lbl_closing_date') .   ' : ' . $this->client_model->payment_closing_date_options($post['closing_date']) . lang('common_lbl_date') . "   "
                                      . lang('lbl_payment_month') .  ' : ' . $this->client_model->payment_month_options($post['payment_month_cd']) . lang('common_lbl_month') . "   "
                                      . lang('lbl_payment_date') . ' : ' . $this->client_model->payment_date_options($post['payment_day_cd']) . lang('common_lbl_date');
                    }
                    $replace_params = array(
                            'name'                => htmlspecialchars($this->client_model->name_with_title($post['name'], $post['corporate_status_before'], $post['corporate_status_after'])),
                            'name_kana'           => htmlspecialchars($post['name_kana']),
                            's_account_id'        => $accounts[$post['s_account_id_1st']] . (isset($accounts[$post['s_account_id_2nd']]) ? '，' . $accounts[$post['s_account_id_2nd']] : ''),
                            'payment_term'        => $payment_term,
                            'zipcode'             => $post['zipcode'],
                            'address1'            => htmlspecialchars($post['address_1st']),
                            'address2'            => htmlspecialchars($post['address_2nd']),
                            'phone_no'            => $post['phone_no'],
                            'business_category'   => $this->client_model->business_category_options($post['business_category']),
                            'remark'              => htmlspecialchars($post['remark']),
                    );

                    if ($action == 'apply') {
                        $template = 'client_apply';
                    } elseif ($action == 'reapply') {
                        $template = 'client_reapply';
                    }
//#7427:Start
//                    $this->_send_mail_by_authority('mail_client_apply', $template, $replace_params);
                    $this->send_mail_by_authority('mail_client_apply', $template, $replace_params);
//#7427:End
                }
            }

            $this->ajax_json($result);
        }

        $reapp = false;
        // Check if we have id, if we do, get client data and pass it to input form
        if ($id = $this->input->post('id')) {
            if (!$this->input->post('copy')) {
                $reapp = true;
            }

            $client = $this->client_model->filter(array('search_id' => $id))->get_data(true);

            if (empty($client)) {
                echo lang('msg_id_not_exist');
                exit;
            }

            $this->assign('client', $client->row(0, 'Client_model'));
        // Don't have id, this is a new application form
        } else {
            $this->client_model->s_account_id_1st = $this->auth->get_account_id();
            $this->assign('client', $this->client_model);
        }

        $this->assign('accounts', $accounts);
        $this->assign('is_reapp', $reapp);
        $this->view('client/apply_retry.tpl');
    }

    /**
     * Apply for approval
     *
     * @author quang_duc
     */
    public function apply_approval()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->models(array('client_model'));

        $id = $this->input->post('id');

        $client = $this->_load_client($id, true);

        if ($client['status'] != CLIENT_STATUS_CREDIT_ALREADY) {
            log_message('error', __FILE__ . '(line ' . __LINE__ . '): wrong client status.');
            $this->ajax_json(array(
                'status' => AJAX_FAIL,
                'message' => lang('msg_already_approve_request')
            ));
            return false;
        }

        if ($this->client_model->update('client', array('status' => CLIENT_STATUS_CL_APPROVAL_PENDING), $id)) {
            $this->ajax_json(array(
                    'status' => AJAX_SUCCESS
            ));
        } else {
            $this->ajax_json(array(
                    'status' => AJAX_FAIL,
                    'message' => lang('common_could_not_edit')
            ));
        }
    }

    /**
     * Input credit result
     *
     * @author quang_duc
     */
    public function input_credit()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->models(array('client_model', 'account_model'));

        $action = $this->input->post('action');
        $id = $this->input->post('id');

        $client = $this->_load_client($id, $action);

        $accounts = $this->account_model->get_account_department_business_unit(array($client['s_account_id_1st'], $client['s_account_id_2nd']), '', false);
        $accounts = index_element($accounts, 'id');

        if ($action) {
            if ($this->client_model->validate('credit')) {
                $post = fetch_post(array(
                        'credit_level',
                        'is_ad_receive_payment' => 0,
                        'credit_amount' => 0,
                        'comment',
                ));

                $post['is_ad_receive_payment_request'] = $client['is_ad_receive_payment_request'];

//                ($client['status'] == CLIENT_STATUS_RE_CREDIT_WAITING) ? $is_recredit = true : $is_recredit = false;
                $is_recredit = false;

                if ($this->client_model->input_credit($id, $post)) {
                    $result = array(
                            'status'  => AJAX_SUCCESS,
                            'message' => lang('common_edit_successfully'),
                    );
                } else {
                    $result = array(
                            'status'  => AJAX_FAIL,
                            'message' => $this->client_model->error ? $this->client_model->error : lang('common_could_not_edit'),
                    );
                }
            } else {
                $result = array(
                        'status'  => AJAX_FAIL,
                        'message' => '',
                        'errors'  => $this->form_validation->error_array()
                );
            }

            if ($result['status'] == AJAX_SUCCESS) {
                $this->load->library('email_template');

                if ($client['is_ad_receive_payment_request']) {
                    $payment_term = lang('lbl_advance_payment');
                } else {
                    $payment_term = lang('lbl_closing_date') .   ' : ' . $this->client_model->payment_closing_date_options($client['closing_date']) . lang('common_lbl_date') . "   "
                                  . lang('lbl_payment_month') .  ' : ' . $this->client_model->payment_month_options($client['payment_month_cd']) . lang('common_lbl_month') . "   "
                                  . lang('lbl_payment_date') . ' : ' . $this->client_model->payment_date_options($client['payment_day_cd']) . lang('common_lbl_date');
                }
                $business_unit_follow_acounts = $this->account_model->get_account_department_business_unit(array($client['s_account_id_1st'], $client['s_account_id_2nd']), '', false);
                    foreach ($business_unit_follow_acounts as $business_unit_follow_acount) {
                            $list_business_unit[] = $business_unit_follow_acount['business_unit_name'];
                }
                $replace_params = array(
                        'subject'        => '【与信結果】',
                        'name'           => $this->client_model->name_with_title($client['name'], $client['corporate_status_before'], $client['corporate_status_after']),
                        'account_1st'    => '所属部署名　：' . $accounts[$client['s_account_id_1st']]['business_unit_name'] . $accounts[$client['s_account_id_1st']]['department_name'] . "\n"
                                          . '営業担当者　：' . $accounts[$client['s_account_id_1st']]['name'],
                        'business_name'  => implode(",", $list_business_unit),
                        'client_id'      => $client['id'],
                        'is_recredit'    => $is_recredit ? '取引先再与信' : '取引先新規申請',
                        'payment_terms'  => $payment_term,
                        'zipcode'        => $client['zipcode'],
                        'address_1'      => $client['address_1st'],
                        'address_2'      => $client['address_2nd'],
                        'phone_no'       => $client['phone_no'],
                        'business_category'   => $this->client_model->business_category_options($client['business_category']),
                        'representative_name' => $client['representative_name'],
                        'delivery_amount' => number_format($client['delivery_amount']),
                        'credit_status'  => $this->client_model->credit_level_options($post['credit_level']),
                        'credit_amount'  => $post['is_ad_receive_payment'] ? '前金' : number_format($post['credit_amount']),
                        'comment'        => $post['comment'],
                );

                if ($client['s_account_id_2nd']) {
                    if (isset($accounts[$client['s_account_id_2nd']])) {
                        $replace_params['account_2nd'] = '所属部署名　：' . $accounts[$client['s_account_id_2nd']]['business_unit_name'] . $accounts[$client['s_account_id_2nd']]['department_name'] . "\n"
                            . '営業担当者　：' . $accounts[$client['s_account_id_2nd']]['name']
                            . "\n";
                    } else {
                        $replace_params['account_2nd'] = "\n";
                    }
                } else {
                    $replace_params['account_2nd'] = "\n";
                }

                if (!$is_recredit) {
                    $replace_params['expire'] = '期限       ：' . date('Y-m-d', strtotime('now + 3 months')) . "\n"
                                              . '　　　　      ※上記期限までにＳ登録がない場合、再度の審査が必要になります。'
                                              . "\n";
                }
//#7427:Start
//                 $this->_send_mail_by_authority('mail_client_credit_result_all', 'client_credit', $replace_params);
//                 $this->_send_mail_by_authority('mail_client_credit_result_self', 'client_credit', $replace_params, array($client['s_account_id_1st'], $client['s_account_id_2nd'], $client['applicant_account_id']));
                $this->send_mail_by_authority('mail_client_credit_result_all', 'client_credit', $replace_params);
                $this->send_mail_by_authority('mail_client_credit_result_self', 'client_credit', $replace_params, array($client['s_account_id_1st'], $client['s_account_id_2nd'], $client['applicant_account_id']));
//#7427:End
            }

            $this->ajax_json($result);
        }

        $this->assign('client', $client);
        $this->assign('accounts', $accounts);

        if ($gui_parts = $this->input->post('gui_parts')) {
            if (is_array($gui_parts)) {
                foreach ($gui_parts as $key => $config) {
                    $this->assign($key, $config);
                }
            }
        } else {
            foreach ($this->$gui_parts as $option) {
                $this->assign($option, $this->client_model->{$option}());
            }
        }

        $credit_level_options = $this->client_model->credit_level_options();
        if ($client['is_ad_receive_payment_request'] == 1) {
            unset($credit_level_options[CLIENT_CREDIT_LEVEL_C]);
        }
        $this->assign('credit_level_options', $credit_level_options);

        $this->view('client/input_credit.tpl');
    }

    /**
     * Approve or reject application
     *
     * @author quang_duc
     */
    public function approve_reject()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->models(array('client_model', 'account_model'));

        $action = $this->input->post('action');
        $id = $this->input->post('id');

        $client = $this->_load_client($id, $action);

        if ($action) {
                $post = fetch_post(array('comment'));
                if ($action == 'approve') {
                    if ($this->client_model->approve($id, $post)) {
                        $result = array(
                                'status'  => AJAX_SUCCESS,
                                'message' => lang('common_edit_successfully'),
                        );
                    } else {
                        $result = array(
                                'status'  => AJAX_FAIL,
                                'message' => $this->client_model->error ? $this->client_model->error : lang('common_could_not_edit'),
                        );
                    }
                } elseif ($action == 'reject') {
                    if ($this->client_model->reject($id, $post)) {
                        $result = array(
                                'status'  => AJAX_SUCCESS,
                                'message' => lang('msg_reject_success'),
                        );
                    } else {
                        $result = array(
                                'status'  => AJAX_FAIL,
                                'message' => $this->client_model->error ? $this->client_model->error : lang('common_could_not_edit'),
                        );
                    }
                }

            $this->ajax_json($result);
        } else {
            if ($client['status'] == CLIENT_STATUS_CL_APPROVED) {
                echo lang('msg_already_approve');
                exit;
            } elseif ($client['status'] == CLIENT_STATUS_CL_APPROVAL_REJECTED) {
                echo lang('msg_already_reject');
                exit;
            }
        }

        $accounts = $this->account_model->get_account_department_business_unit(array($client['s_account_id_1st'], $client['s_account_id_2nd']), '', false);
        $accounts = index_element($accounts, 'id');

        $this->assign('client', $client);
        $this->assign('accounts', $accounts);

        if ($gui_parts = $this->input->post('gui_parts')) {
            if (is_array($gui_parts)) {
                foreach ($gui_parts as $key => $config) {
                    $this->assign($key, $config);
                }
            }
        } else {
            foreach ($this->$gui_parts as $option) {
                $this->assign($option, $this->client_model->{$option}());
            }
        }

        $this->view('client/approve_reject.tpl');
    }

    /**
     * Edit application
     *
     * @author quang_duc
     */
    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->models(array('client_model', 'account_model'));

        $action = $this->input->post('action');
        $id = $this->input->post('id');

        $client = $this->_load_client($id, $action, true);

        $accounts = $this->account_model->get_account_department_business_unit('', '', false);

        if ($action) {
            if ($action == 'edit') {

                $form_valid = true;
                $custom_errors = array();

                if ($this->auth->has_permission('client/input_credit')) {
                    if (!$this->input->post('corporate_status_before') && !$this->input->post('corporate_status_after')) {
                        $form_valid = false;
                        $custom_errors['corporate_status_before'] = str_replace('%field%', lang('lbl_company_title'), lang('msg_field_required'));
                    }

                    if (!$this->client_model->check_payment_date($this->input->post('closing_date'), $this->input->post('payment_month_cd'), $this->input->post('payment_day_cd'))) {
                        $form_valid = false;
                        $custom_errors['payment_day_cd'] = lang('msg_payment_date_after_closing_date');
                    }
                }

                $form_valid = $this->client_model->validate('edit') && $form_valid;

                if ($form_valid) {
                    // Fetch POST data
                    $post = fetch_post(array(
                            'corporate_status_before' => 0,
                            'corporate_status_after' => 0,
                            'name',
                            'name_kana',
                            'zipcode',
                            'address_1st',
                            'address_2nd',
                            'representative_name',
                            'phone_no',
                            'fax_no',
                            'business_category',
                            'division_name',
                            'charge_name',
                            'mail_address',
                            'closing_date',
                            'payment_month_cd',
                            'payment_day_cd',
                            'is_ad_receive_payment_request' => 0,
                            's_account_id_1st',
                            's_account_id_2nd' => 0,
                            'update_credit' => 0,
                            'credit_level',
                            'is_ad_receive_payment' => 0,
                            'credit_amount' => 0,
                            'comment',
                            'bank_account' => array(),
                            'lastup_account_id' => $this->auth->get_account_id(),
                    ));
                    if ($this->client_model->edit($id, $post)) {
                        $result = array(
                                'status'  => AJAX_SUCCESS,
                                'message' => lang('common_successfully_edit'),
                        );
                    } else {
                        $result = array(
                                'status'  => AJAX_FAIL,
                                'message' => lang('common_could_not_edit'),
                        );
                    }
                } else {
                    $result = array(
                            'status'  => AJAX_FAIL,
                            'message' => '',
                            'errors'  => $this->form_validation->error_array() + $custom_errors
                    );
                }
            } elseif ($action == 'check_order') {
                $this->_check_order($id);

            } elseif ($action == 'delete') {
                if ($this->client_model->delete('client', $id)) {
                    $result = array(
                            'status'  => AJAX_SUCCESS,
                            'message' => '',
                    );
                } else {
                    $result = array(
                            'status'  => AJAX_FAIL,
                            'message' => lang('common_could_not_delete'),
                    );
                }
            }

            if ($result['status'] == AJAX_SUCCESS && $post['update_credit']) {
                $this->load->library('email_template');

                $accounts = index_element($accounts, 'id');
//                ($client->status == CLIENT_STATUS_RE_CREDIT_WAITING) ? $is_recredit = true : $is_recredit = false;
                $is_recredit = true;

                if (isset($post['update_credit']) && $post['update_credit']) {
                    $credit = array(
                            'credit_level'          => $post['credit_level'],
                            'is_ad_receive_payment' => $post['is_ad_receive_payment'],
                            'credit_amount'         => $post['credit_amount'],
                            'comment'               => $post['comment'],
                            'credit_date'           => date('Y-m-d')
                    );
                } else {
                    $credit = array(
                            'credit_level'          => $client->credit_level,
                            'is_ad_receive_payment' => $client->is_ad_receive_payment,
                            'credit_amount'         => $client->credit_amount,
                            'comment'               => $client->comment,
                            'credit_date'           => $client->credit_date
                    );
                }
//#7504:Start
                if (!isset($post['s_account_id_1st'])) {
                    $post['s_account_id_1st'] = $client->s_account_id_1st;
                    $post['s_account_id_2nd'] = $client->s_account_id_2nd;
                }
//#7504:End
                if ($post['is_ad_receive_payment_request']) {
                    $payment_term = lang('lbl_advance_payment');
                } else {
                    $payment_term = lang('lbl_closing_date') .   ' : ' . $this->client_model->payment_closing_date_options($post['closing_date']) . lang('common_lbl_date') . "   "
                                  . lang('lbl_payment_month') .  ' : ' . $this->client_model->payment_month_options($post['payment_month_cd']) . lang('common_lbl_month') . "   "
                                  . lang('lbl_payment_date') . ' : ' . $this->client_model->payment_date_options($post['payment_day_cd']) . lang('common_lbl_date');
                }
                $business_unit_follow_acounts = $this->account_model->get_account_department_business_unit(array($post['s_account_id_1st'], $post['s_account_id_2nd']), '', false);
                    foreach ($business_unit_follow_acounts as $business_unit_follow_acount) {
                            $list_business_unit[] = $business_unit_follow_acount['business_unit_name'];
                }
                $replace_params = array(
                        'subject'        => $is_recredit ? '【再与信結果】' : '【与信結果】',
                        'name'           => $this->client_model->name_with_title($post['name'], $post['corporate_status_before'], $post['corporate_status_after']),
                        'account_1st'    => '所属部署名　：' . $accounts[$post['s_account_id_1st']]['business_unit_name'] . $accounts[$post['s_account_id_1st']]['department_name'] . "\n"
                                          . '営業担当者　：' . $accounts[$post['s_account_id_1st']]['name'],
                        'business_name'  => implode(",", $list_business_unit),
                        'client_id'      => $client->id,
                        'is_recredit'    => $is_recredit ? '取引先再与信' : '取引先新規申請',
                        'payment_terms'  => $payment_term,
                        'zipcode'        => $post['zipcode'],
                        'address_1'      => $post['address_1st'],
                        'address_2'      => $post['address_2nd'],
                        'phone_no'       => $post['phone_no'],
                        'business_category'   => $this->client_model->business_category_options($post['business_category']),
                        'representative_name' => $post['representative_name'],
                        'delivery_amount' => number_format($client->delivery_amount),
                        'credit_status'  => $this->client_model->credit_level_options($credit['credit_level']),
                        'credit_amount'  => $credit['is_ad_receive_payment'] ? '前金' : number_format($credit['credit_amount']),
                        'comment'        => $credit['comment'],
                );

                if ($post['s_account_id_2nd']) {
                    if (isset($accounts[$post['s_account_id_2nd']])) {
                        $replace_params['account_2nd'] = '所属部署名　：' . $accounts[$post['s_account_id_2nd']]['business_unit_name'] . $accounts[$post['s_account_id_2nd']]['department_name'] . "\n"
                            . '営業担当者　：' . $accounts[$post['s_account_id_2nd']]['name']
                            . "\n";
                    } else {
                        $replace_params['account_2nd'] = "\n";
                    }
                } else {
                    $replace_params['account_2nd'] = "\n";
                }

                if (!$is_recredit) {
                    $replace_params['expire'] = '期限　　　　　　：　' . date('Y-m-d', strtotime($credit['credit_date'] . ' + 3 months')) . "\n"
                                              . '　　　　　　　　　　※上記期限までにＳ登録がない場合、再度の審査が必要になります。'
                                              . "\n";
                }
//#7427:Start
//                 $this->_send_mail_by_authority('mail_client_credit_result_all', 'client_credit', $replace_params);
//                 $this->_send_mail_by_authority('mail_client_credit_result_self', 'client_credit', $replace_params, array($post['s_account_id_1st'], $post['s_account_id_2nd'], $client->applicant_account_id));
                $this->send_mail_by_authority('mail_client_credit_result_all', 'client_credit', $replace_params);
                $this->send_mail_by_authority('mail_client_credit_result_self', 'client_credit', $replace_params, array($post['s_account_id_1st'], $post['s_account_id_2nd']));
//#7427:End
            }

            $this->ajax_json($result);
        }

        $accounts = map_element($accounts, 'id', 'name');

        $bank_accounts = get_element($this->client_model->get_bank_account_info_($id, 'client_id, bank_account'), 'bank_account');

        $this->assign('accounts', $accounts);
        $this->assign('bank_accounts', $bank_accounts);
        $this->assign('client', $client);

        $this->view('client/edit.tpl');
    }

    protected function _check_order($id)
    {
        if ((!$id = $this->input->post('id')) || (!is_numeric($id))) {
            $result = array(
                    'status' => AJAX_FAIL,
                    'message' => lang('msg_invalid_id')
            );
        }

        if (!$this->client_model->has_order($id)) {
            $result = array(
                    'status' => AJAX_SUCCESS
            );
        } else {
            $result = array(
                    'status' => AJAX_FAIL,
                    'message' => lang('msg_client_has_order')
            );
        }

        $this->ajax_json($result);
    }

    /**
     * Check the id and load client of that id
     *
     * @param int $id - For now this function will accept SINGLE ID ONLY
     * @param string $error_json - Set TRUE if you want the error is format as JSON
     * @param boolean $return_object - Set TRUE if you want the result is object instead of array
     * @return array|object
     *
     * @author quang_duc
     */
    protected function _load_client($id, $error_json = false, $return_object = FALSE)
    {
        if ((!$id) || (!is_numeric($id))) {
            $error_msg = lang('msg_invalid_id');
            if ($error_json) {
                $this->ajax_json(array(
                        'status' => AJAX_FAIL,
                        'message' => $error_msg
                ));
            } else {
                log_message('error', __FILE__ . '(line ' . __LINE__ . '): ID is null or not a number.');
                echo $error_msg;
                exit;
            }
        }

        $query = $this->client_model->filter(array('search_id' => $id))->get_data(true);

        if (!$query->num_rows()) {
            $error_msg = lang('msg_id_not_exist');
            if ($error_json) {
                $this->ajax_json(array(
                        'status' => AJAX_FAIL,
                        'message' => $error_msg
                ));
            } else {
                log_message('error', __FILE__ . '(line ' . __LINE__ . '): There are no client with id=' . $id);
                echo $error_msg;
                exit;
            }
        }

        if ($return_object) {
            $client = $query->row(0, 'Client_model');
        } else {
            $client = $query->row_array();
        }

        return $client;
    }
//#7427:Start --> Move this function to ZR_Controller
//     /**
//      * Send mail to user of authority name, see {@link Client_model::authority()}
//      *
//      * @param string $column_name - quick setting column name
//      * @param string $template
//      * @param array $params
//      *
//      * @author quang_duc
//      */
//     protected function _send_mail_by_authority($column_name, $template, $params, $account_ids = array())
//     {
//         $this->load->model('authority_model');

//         $authority_ids = $this->authority_model->get_authority_by_quick_setting($column_name);

//         if (empty($authority_ids)) {
//             return;
//         }

//         $accounts = $this->db->select('id, mail_address')
//                              ->where_in('authority_id', $authority_ids)
//                              ->where('disable', STATUS_ENABLE)
//                              ->get('account')
//                              ->result_array();
//         $sent_address = array();

// //        $accounts = array(array('mail_address' => 'quang_duc@lampart.com.vn'));     // Change to your email when testing

//         foreach ($accounts as $account) {
//             if (!isset($sent_address[$account['mail_address']])) {
//                 if (!empty($account_ids)) {
//                     if (in_array($account['id'], $account_ids)) {
//                         if (!$this->email_template->send_mail($template, array('system_to' => $account['mail_address']), $params)) {
//                             log_message('error', 'Cannot send mail to ' . $account['mail_address'] . ', template "' . $template . '"');
//                         } else {
//                             $sent_address[$account['mail_address']] = true;
//                         }
//                     }
//                 } else {
//                     if (!$this->email_template->send_mail($template, array('system_to' => $account['mail_address']), $params)) {
//                         log_message('error', 'Cannot send mail to ' . $account['mail_address'] . ', template "' . $template . '"');
//                     } else {
//                         $sent_address[$account['mail_address']] = true;
//                     }
//                 }
//             }
//         }
//     }
//#7427:End
    public function ajax_check_payment() {
        $input = json_decode($this->input->raw_input_stream);
        $this->load->helper('zenrin_system_helper');
        $new_order = array();

        $order_id = '';
        if (isset($input->order_id)) {
            if ($input->branchs) {
                foreach ($input->branchs as $branch) {
                    /*tmp#6913: Start 2015/09/29
                    if (isset($branch->od_payment_date) && isset($branch->od_delivery_date)) {
                    tmp#6913: End */
                    if (isset($branch->od_delivery_date)) {
                        $item['amount'] =  $branch->od_amount_value;
                        $item['delivery_date'] = $branch->od_delivery_date;
//#7983:Start
                        $item['tax_type'] = $branch->od_tax_type;
//#7983:End
                        /*tmp#6913: Start 2015/09/29
                        $item['payment_date'] = $branch->od_payment_date;
                        tmp#6913: End */
                        $item['branch']          = $branch->od_branch_cd;
                        if (($branch->od_is_cancel_request == 1) || !in_array($branch->od_order_status, array(ORDER_STATUS_UNAPPROVED,ORDER_STATUS_UNDETERMINED, ORDER_STATUS_CONFIRMED))) {
                            continue;
                        }
                        $new_order[] = $item;
                    }
                }
            }
            $order_id = $input->order_id;
        } else {
            if ($input->branchs) {
                foreach ($input->branchs as $branch) {
                    /*tmp#6913: Start 2015/09/29
                    if (isset($branch->payment_date) && isset($branch->delivery_date)) {
                    tmp#6913: End */
                    if (isset($branch->delivery_date)) {
                        $item['amount'] =  $branch->amount_value;
                        $item['delivery_date'] = $branch->delivery_date;
//#7983:Start
                        $item['tax_type'] = $branch->tax_type;
//#7983:End
                        /*tmp#6913: Start 2015/09/29
                        $item['payment_date'] = $branch->payment_date;
                        tmp#6913: End */
                        $item['branch']       = $branch->branch;
                        $new_order[] = $item;
                    }
                }
            }
        }

        $this->ajax_json(get_payment_remain($input->client_id, $order_id, $new_order));
        exit;
    }


    /**
     * Ajax get clients
     *
     * @author cong_tien
     */
    public function ajax_get_clients() {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $offset = $this->input->post('offset');
        $search_data = $clients = json_decode($this->input->post('search_data'), true);

        if (empty($search_data)) {
            $business_default = $this->input->post('business_default');

            $search_data['search_business'] = $business_default;
        }

        $clients = $this->client_model->get_clients($offset, $search_data);

        $this->ajax_json(array('status' => true, 'clients' => $clients));
    }

}
