<?php
class Closing_accountant extends ZR_Controller {
    // Constructor
    public function __construct() {
        parent::__construct ();
        // Load model
        $this->load->model('closing_accountant_model');
    }

    /**
     * Main screen
     * @author Phong
     * @created date: 25/June/2015
     *
     * @modify van_thuat 2015-08-12 #5978
     */
    public function show() {
        $models = array('holiday_model', 'order_acceptance_model');
        $this->load->model($models);

        // Init date variable
        $prev_month = new DateTime('-1 months');

        // Check accountant 2 months ago is closed
        $is_closed = false;
        if (($closed_accountant = $this->closing_accountant_model->get_closed_accountant('-2 months'))
            && ($closed_accountant['disable'] == STATUS_ENABLE)) {
            $is_closed = true;
        } else {
            $prev_month->modify('-1 months');
        }

        // When accountant 2 months ago is closed, continue check accountant 1 months ago
        if ($is_closed) {
            // Check accountant 1 months ago is closed
            if (($closed_accountant = $this->closing_accountant_model->get_closed_accountant())
                && ($closed_accountant['disable'] == STATUS_ENABLE)) {
                $is_closed = true;
            } else {
                $is_closed = false;
            }
        }

        // Check needing take_action button
        $show_take_action = false;
        // If not closed or working day <= 7
        if (!$is_closed || ($this->holiday_model->count_working_days() <= ACCOUNTANT_UNCLOSE_BUSINESS_DAYS)) {
            $show_take_action = true;
        }
        // #7133: Start 12/Oct/2015
        // If it is 7 days from the month beginning, and today is SUN or SAT or holiday => Not allow unclose accountant
        $today = new DateTime();
        $this->load->model('holiday_model');
        $unclose_btn = true;
        if(($this->holiday_model->count_working_days() == ACCOUNTANT_UNCLOSE_BUSINESS_DAYS) &&
                ($this->holiday_model->check_holiday($today->format("Y-m-d"))
                        || ($today->format("D")=="Sun") || ($today->format("D")=="Sat")
                )
        ){
            $unclose_btn = false;
        }
        // #7133: End 12/Oct/2015
        // Assign values
        $this->assign('prev_month', $prev_month->format('Y年m月'));
        $this->assign('is_closed', $is_closed);
        $this->assign('show_take_action', $show_take_action);
        $this->assign('unclose_btn', $unclose_btn);

        // Set view file
        $this->view('closing_accountant/show.tpl');
    }

    /**
     * Close or unclose acceptance
     * @author Phong
     * @created date: 26/June/2015
     *
     * @modify van_thuat 2015-08-12 #5978
     */
    public function take_action() {
        if (!$this->input->is_ajax_request()) redirect('/_admin/closing_accountant/show');

        $models = array('holiday_model');
        $this->load->model($models);

        // Init variables
        $take_action_type = $this->input->post('take_action_type');

        switch ($take_action_type) {
            case 'close':
                $this->ajax_json($this->closing_accountant_model->close_closing_accountant());
                break;

            case 'unclose':
                $this->ajax_json($this->closing_accountant_model->unclose_closing_accountant());
                break;

            default:
                $this->ajax_json(array('status'  => 0, 'message' => $take_action_type));
                break;
        }
    }

    /**
     * Check time, if accountance has been closed more than 7 days working from now. No unclose.
     * @param unknown $val
     * @author Phong
     * @created date: 26/June/2015
     * @return boolean
     */
    /* public function check_time_limit_7days_working($val) {
        if($val==date('Y/m')) {
            $val = $val."/01";
            $val = str_replace("/","-", $val);
            $begin = new DateTime($val);
            $today = new DateTime();
            // #7133: Start 12/Oct/2015
            $today_is_off = false;
            if(($today->format("D")=="Sat") || ($today->format("D")=="Sun")) {
                $today_is_off = true;
            }
            $interval = new DateInterval('P1D');
            $daterange = new DatePeriod($begin, $interval ,$today);
            $list_holiday = $this->closing_accountant_model->get_holiday_date($val, date('Y-m-d'));
            // Total of day
            $plus = 0;
            // Total of non-working day
            $subtract = 0;
            foreach($daterange as $date){
                $plus+=1;
                // If the day in holiday period day
                foreach($list_holiday as $h) {
                    if(date('Ymd',strtotime($h['holiday'])) == $date->format("Ymd")) {
                        $subtract+=1;
                    }
                    if(date('Ymd',strtotime($today)) == date('Ymd',strtotime($h['holiday']))) {
                        $today_is_off = true;
                    }
                }
                // If the day is Sunday or Saturday (default non-working day)
                if(($date->format("D")=="Sat") || ($date->format("D")=="Sun")) {
                    $subtract+=1;
                }
            }
            if(($plus-$subtract)==7 && $today_is_off) {
                return false;
            }
            // #7133: End 12/Oct/2015
            if(($plus-$subtract)<=7) {
                return true;
            }
        } // End if($val==date('Y/m')) {
        return false;
    } */
}