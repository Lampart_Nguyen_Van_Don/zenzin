<?php
class Consumption_tax extends ZR_Controller {
    public function get_tax_rate() {
    	if (!$this->input->is_ajax_request()) {
    		show_404();
    	}

        $this->load->model('Consumption_tax_model');

        $data = json_decode($this->input->raw_input_stream);
        $tax_type = $data->tax_type;
        $tax_rate = $this->Consumption_tax_model->gex_tax_rate($tax_type);

        $this->ajax_json($tax_rate);
    }
}