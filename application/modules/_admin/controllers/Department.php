<?php
/**
 * Department management
 * @author van_phong
 *
 */
class Department extends ZR_Controller {
    // Constructor
    public function __construct() {
        parent::__construct ();
        // Load model for department management
        $this->load->model('department_model');
    }

    /**
     * Show all departments
     * @return
     * @author
     */
    public function show() {

        // Load business unit model
        $this->load->model('business_unit_model');
        $departments = array();

        // User clicks search
        $departments = $this->department_model->set_pager ()->get_all_departments ();

        if(!empty($departments)) {
            $this->assign('departments', $departments);
        }

        $this->view('department/show.tpl');
    }

    /**
     * From list department screen, user click "Add" to add new department
     * Only for navigation, not save
     * @return
     * @author
     */
    public function add() {
        // Check if user uses direct link
        if (! $this->input->is_ajax_request ()) {
            show_404 ();
        }
        // Load business unit model
        $this->load->model('business_unit_model');
        // Get all business unit
        $list_business_unit = $this->business_unit_model->get_business_unit_id_name ();
        if(!empty($list_business_unit)) {
            $this->assign('bu', $list_business_unit);
        }
        // If user click "Add"
        // if (isset($_POST ['add_new'])) {
        $this->view('department/add.tpl');
    }

    /**
     * Load detail of department by popup (dialogbox) (c)
     * @author
     * @return
     */
    public function detail() {
        // Check if user uses direct link
        if (! $this->input->is_ajax_request ()) {
            show_404 ();
        }
        if($id = $this->input->post('id')) {
            $department = $this->department_model->get_department_by_id($id);
            if(!empty($department)) {
                $this->assign("d", $department);
            }
        }
        $this->view('department/detail.tpl');
    }

    /**
     * Edit/Copy department by popup (dialogbox) (c): only load page
     * @return
     * @author
     */
    public function edit() {
        // Check if user uses direct link
        if (! $this->input->is_ajax_request ()) {
            show_404 ();
        }
        if($id = $this->input->post('id')) {
            // Load business unit model
            $this->load->model('business_unit_model');
            // Get all business unit
            $list_business_unit = $this->business_unit_model->get_business_unit_id_name ();
            if(!empty($list_business_unit)) {
                $this->assign('bu', $list_business_unit);
            }
            // Get department
            $department = $this->department_model->get_department_by_id($id);
            if(!empty($department)) {
                $this->assign("d", $department);
            }
        }
        if ($this->input->post('type') == "btn_edit") {
            $this->view('department/edit.tpl');
        } else {
            $this->assign("type", "copy");
            $this->view('department/edit.tpl');
        }
    }

    /**
     * Save editting/copying department on dialogbox (c)
     * @return
     * @author
     * Use for adding, copying and editting
     */
    public function edit_ajax() {
        // Check if user uses direct link
        if (! $this->input->is_ajax_request ()) {
            show_404 ();
        }

        // Validation
        if ($this->department_model->validate('edit_department')) {
            // Fields to update

            $data = array (
                    'business_unit_id' => $this->input->post('business_unit_id'),
                    'name' => $this->input->post('name'),
                    'zipcode' => $this->input->post('zipcode'),
                    'address_1' => $this->input->post('address_1'),
                    'address_2' => $this->input->post('address_2'),
                    'phone_no' => $this->input->post('phone_no'),
                    'fax_no' => $this->input->post('fax_no'),
                    'lastup_account_id' => $this->auth->get_account_id ()
           );

            // Add or Copy => Insert to DB
            if($this->input->post('type')) {
                $check_duplicated = $this->department_model->check_duplicated_item($data['name'],$data['business_unit_id']);
                if(empty($check_duplicated)) {
                    $this->department_model->insert('department', $data);
                    $errors ['error'] = 0;
                    $errors ['type'] = "copy/add";
                    echo json_encode($errors);
                } else {
                    $errors ['error'] = 2;
                    $msg = lang('common_duplicated_records');
                    $msg = str_replace("<<business_unit>>",$check_duplicated['business_unit_name'],$msg);
                    $msg = str_replace("<<department>>",$check_duplicated['department_name'],$msg);
                    $errors ['msg_error'] = $msg;
                    echo json_encode($errors);
                }
            } else {
                // Case: Edit
                $check_duplicated = $this->department_model->check_duplicated_item_edit($data['name'],$data['business_unit_id'], $this->input->post('d_id'));
                if(empty($check_duplicated)) {
                    $condition = array (
                            'id' => $this->input->post ( 'd_id' )
                    );
                    $this->department_model->update_department ( 'department', $data, $condition );
                    $errors ['error'] = 0;
                    $errors ['type'] = "edit";
                    echo json_encode ( $errors );
                } else {
                    $errors ['error'] = 2;
                    $msg = lang('common_duplicated_records');
                    $msg = str_replace("<<business_unit>>",$check_duplicated['business_unit_name'],$msg);
                    $msg = str_replace("<<department>>",$check_duplicated['department_name'],$msg);
                    $errors ['msg_error'] = $msg;
                    echo json_encode($errors);
                }
            }
        } else {
            $errors ['error'] = 1;
            $errors ['name'] = form_error('name');
            $errors ['business_unit_id'] = form_error('business_unit_id');
            $errors ['zipcode'] = form_error('zipcode');
            $errors ['address_1'] = form_error('address_1');
            $errors ['address_2'] = form_error('address_2');
            $errors ['phone_no'] = form_error('phone_no');
            $errors ['fax_no'] = form_error('fax_no');
            echo json_encode($errors);
        }
    }

    /**
     * delete
     * @author
     * @return
     */
    public function delete() {
        // Check if user uses direct link
        if (! $this->input->is_ajax_request ()) {
            show_404 ();
        }
        // Check if this exists
        $list = array_filter($this->department_model->check_department_exist($this->input->post('d_id')));
        if (!empty($list)) {
            // Check account constrains department
            $check_constraint = $this->department_model->check_account_constraint($this->input->post('d_id'));
            if(empty($check_constraint)) {
                if($this->department_model->delete("department",$this->input->post('d_id'))) {
                    $errors ['error'] = 0;
                    echo json_encode($errors);
                } else {
                    $errors ['error'] = 1;
                    $errors ['error_name'] = lang("common_could_not_delete");
                    echo json_encode($errors);
                }
            } else {
                $errors ['error'] = 1;
                $errors ['error_name'] = lang("alert_could_not_delete_account_constraint");
                echo json_encode($errors);
            }
        } else {
            $errors ['error'] = 1;
            $errors ['error_name'] = lang("common_record_not_exist");
            echo json_encode($errors);
        }
    }
}