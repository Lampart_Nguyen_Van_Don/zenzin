<?php
class Error extends ZR_Controller{
    public function access_deny() {
        $this->view('error/access_deny.tpl');
    }

    public function show_404() {
        $this->view('error/show_404.tpl');
    }

    public function uri_error() {
        $this->view('error/uri_error.tpl');
    }
}