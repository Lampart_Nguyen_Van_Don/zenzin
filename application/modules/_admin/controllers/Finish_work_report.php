<?php
/**
 *    Finish work report screen
 *
 *    @author hoang_minh
 *  @since 2015-06-02
*/

class Finish_work_report extends ZR_Controller {

    /**
     * Config excel template file name
     */
    private $_excel_template_file_name = 'finish_work_report.xlsx';

    public function __construct() {
        parent::__construct();
    }

    /**
     * Showing items when searched or without
     *
     * @author hoang_minh
     * @since 2015-06-02
     */
    public function show() {
         //load model
        $this->load->model('gui_parts_element_model');
        $this->load->model('account_model');
        $this->load->model('order_model');
        $this->load->library('auth');

        $auth_data = $this->auth->get_auth_data();
        $this->order_model->set_account_follow_bussiness_unit();
        $this->order_model->set_order_status();
        $this->order_model->set_account_follow_bussiness_unit();

        //init variables
        $columns = array('code', 'name');
        $finish_work_status_configs = array();
		$tax_division = $this->gui_parts_element_model->get_by_gui_parts_column_name('tax_division', 'code,name');
		$order_status_configs = $this->gui_parts_element_model->get_by_gui_parts_column_name('order_status', 'code,name');
		$tax_division = json_encode($tax_division);

		$logged_id = $this->auth->get_account_id ();
        $logged_user_bu = $this->order_model->get_business_unit_of_logged_user($logged_id);
        $logged_id = $logged_user_bu['is_product_division']==1? $logged_id : "";
//7910:Start
		$finish_work_report_template = $logged_user_bu['finish_work_report_template'];
        $business_unit_template = $this->gui_parts_element_model->get_by_gui_parts_column_name('business_unit_template', 'code, name');
        $bu_code = "others";
        if ($logged_user_bu['is_product_division'] == 1) {
        	foreach ($business_unit_template as $val) {
        		$business_unit_template_code = $val['code'];
        		if ($business_unit_template_code == $finish_work_report_template){
        			if ($business_unit_template_code == DM_FWR_TEMPLATE_CODE) //DM
        				$bu_code = "DM";
        			elseif ($business_unit_template_code == DP_FWR_TEMPLATE_CODE) //DP
        			$bu_code = "DP";
        		}
        	}
        }

        $config_info = $this->config->item('bu_roles');
        $header_text = $config_info[$bu_code][$this->router->fetch_class()]['header_text'];
        $hide_column = $config_info[$bu_code][$this->router->fetch_class()]['hide_column'];
        $show_column = $config_info[$bu_code][$this->router->fetch_class()]['show_column'];
        $business_unit_name = $config_info[$bu_code][$this->router->fetch_class()]['bu_name'];

        if (!in_array($bu_code, array("DM", "DP"))) {
			redirect('/_admin/error/access_deny');
        }
//7910:End
        $search_result = $this->order_model->search_order($logged_id, $order_status = 2);

        if ($search_result) {
            foreach ($search_result as &$item) {
                foreach ($order_status_configs as $val) {
                    if ($item->od_order_status == $val['code']) {
                        $item->od_order_status_name = $val['name'];
                        break;
                    }
                }
                $item->od_gross_profit_rate_percent = $item->od_gross_profit_rate . '%';
            }
        }

        //get finish work status configs
        $finish_work_status = $this->gui_parts_element_model->get_by_gui_parts_column_name('report_is_complete_status', $columns);
        $fake_row = $finish_work_status[0];
        $fake_row['code'] = 0;
        $fake_row['name'] = lang('all');
        $finish_work_status_configs[] = $fake_row;

        foreach ($finish_work_status as $val) {
            $finish_work_status_configs[] = $val;
        }

        $can_edit = 0;
        if ($this->auth->has_permission('order_add_edit_all', 'column')) {
        	$can_edit = 2;
        	$this->assign("can_add_edit_all","can");
        } elseif ($this->auth->has_permission('order_add_edit_his_only', 'column')) {
        	$can_edit = 1;
        }

		$can_invoices = 'none';
        if ($this->auth->has_permission('all_invoices', 'column')) {
            $can_invoices = 'all';
        } elseif ($this->auth->has_permission('self_invoices', 'column')) {
            $can_invoices = 'self';
        }

        $j_accounts = $this->order_model->get_j_account($this->auth->get_account_id ());

        $products = $this->order_model->get_products_by_bu_id($j_accounts[0]->bu_id);
        $products = json_encode($products);

        if ($this->auth->has_permission('order_add_edit_all', 'column')) {
        	$j_accounts = $this->order_model->get_j_account_by_bu_id($j_accounts[0]->bu_id);
        }
        //get accounts
        $accounts = $this->account_model->get_accounts();
        $clients = $this->order_model->get_clients();
        $clients = json_encode($clients);

		// acceptance_input_change permission
        $order_acceptance_input_change = $this->auth->has_permission('acceptance_input_change', 'column');
        // order_acceptance_view
        $order_acceptance_view = $this->auth->has_permission('order_acceptance_view', 'column');

       //get closest closing date
        $this->load->model('closing_accountant_model');
        $closest_closing_accountant = $this->closing_accountant_model->get_closest_closing_date();


        //assign value
        $this->assign('closest_month_closing_date', date('m', strtotime($closest_closing_accountant[0]['closest_closing_date'])));
        $this->assign('closest_year_closing_date', date('Y', strtotime($closest_closing_accountant[0]['closest_closing_date'])));
        $this->assign('accounts', $accounts);
        $this->assign('detail', APPPATH . 'modules/_admin/views/order/detail');
        $this->assign('finish_work_status_configs', $finish_work_status_configs);
        $this->assign('can_approve', json_encode($this->auth->has_permission('order_approve', 'column')));
        $this->assign('can_add', $this->auth->has_permission('order/add'));
        $this->assign('can_export', $this->auth->has_permission('order/export_order'));
        $this->assign('can_edit', json_encode($can_edit));
        $this->assign('can_invoices', $can_invoices);
        $this->assign('can_acceptance', json_encode($this->auth->has_permission('order_acceptance_view', 'column')));
        $this->assign('j_accounts', $j_accounts);
		$this->assign('tax_division', $tax_division);
		$this->assign('search_result', json_encode($search_result));
		$this->assign('products', $products);
		$this->assign('clients', $clients);
		$this->assign('can_invoices', $can_invoices);
		$this->assign('order_acceptance_input_change', json_encode($order_acceptance_input_change));
        $this->assign('order_acceptance_view', json_encode($order_acceptance_view));
        $this->assign('order_model', $this->order_model);
//7910:Start
        //$this->assign('search_result', json_encode($search_result)); //REDUNDANT
//7910:End
        $this->assign('can_invoice_issue', json_encode($this->auth->has_permission('invoice_view', 'column')));

        $this->assign('add', APPPATH . 'modules/_admin/views/order/add');
        $this->assign('detail', APPPATH . 'modules/_admin/views/order/detail');
        $this->assign('edit', APPPATH . 'modules/_admin/views/order/edit');
        $this->assign('approve_reject', APPPATH . 'modules/_admin/views/order/approve_reject');
        $this->assign('change_report', APPPATH . 'modules/_admin/views/order/change_repo_approve_reject');
//7910:Start
        $this->assign('header_text', $header_text);
        $this->assign('hide_column', $hide_column);
        $this->assign('show_column', $show_column);
        $this->assign('business_unit_name', $business_unit_name);
//7910:End
        //load view
        $this->view('finish_work_report/show.tpl');
    }

    /**
     * Ajax get order acceptances
     *
     * @param unknow
     * @return json
     * @author hoang_minh
     * @since 2015-06-03
     */
    public function ajax_get_order_acceptance() {

        //load model
        $this->load->model('gui_parts_element_model');
        $this->load->model('order_acceptance_model');
        $max_jcode_length = $this->order_acceptance_model->get_lenght_max_jcode_order();

        //init variables
        $columns = array('code', 'name');
        $finish_work_status_configs = array();
        $order_acceptances = array();
        $params = array();
        $params['conditions'] = array();
        $error_msg = array();
        $empty_date_from = false;
        $is_valid_date_to = false;
#7203:Start
        $is_valid_date_from = false;
#7203:End
        if ($post = json_decode($this->input->raw_input_stream)) {
            if (empty($post->delivery_date_from) && !empty($post->delivery_date_to)) {
                $error_msg['delivery_date_error_msg'] = lang('delivery_date_invalid_range');
            }

            if (!empty($post->delivery_date_from)) {

                $delivery_date_from = parse_date($post->delivery_date_from, true);

                if ( ! $delivery_date_from) {
                    $error_msg['delivery_date_error_msg'] = lang('delivery_date_from_invalid');

                    $empty_date_from = true;
                } else {
                    $params['conditions']['delivery_date_from'] = $delivery_date_from;

                    if (count(explode('-', $delivery_date_from)) == 2) {
                        $params['conditions']['delivery_date_from'] .= '-01';
                    }
#7203:Start
                    $is_valid_date_from = true;
#7203:End
                }
            } else {
                $empty_date_from = true;
            }

            if (!empty($post->delivery_date_to)) {

                $delivery_date_to = parse_date($post->delivery_date_to, true);
                if ( ! $delivery_date_to) {
                    $error_msg['delivery_date_error_msg'] = lang('delivery_date_to_invalid');
                } else {
                    $params['conditions']['delivery_date_to'] = $delivery_date_to;

                    if (count(explode('-', $delivery_date_to)) == 2) {
                        $params['conditions']['delivery_date_to'] .= '-31';
                    }
                    $is_valid_date_to = true;
                }

            }

            if ($is_valid_date_to) {
                if ($empty_date_from) {
                    $error_msg['delivery_date_error_msg'] = lang('delivery_date_invalid_range');
                } else {
                    if (strtotime($params['conditions']['delivery_date_to']) < strtotime($params['conditions']['delivery_date_from'])) {
#7203:Start
                        $error_msg['delivery_date_error_msg'] = lang('delivery_date_invalid_range_from_less_than_to');
#7203:End
                    }
                }
                if (!$is_valid_date_from && !empty($post->delivery_date_from))
                	$error_msg['delivery_date_error_msg'] = lang('delivery_date_from_invalid');
            }
#7203:Start
            else {
            	if ($empty_date_from) {
            		if (empty($post->delivery_date_from))
            			$error_msg['delivery_date_error_msg'] = lang('delivery_date_invalid_range');
            	}

            	if (!$is_valid_date_from && !empty($post->delivery_date_from))
            		$error_msg['delivery_date_error_msg'] = lang('delivery_date_from_invalid');
            }
#7214:Start
            if (empty($post->delivery_date_from) && empty($post->delivery_date_to)) {
            	if (isset($error_msg['delivery_date_error_msg']))
            		unset($error_msg['delivery_date_error_msg']);
            }
#7214:End

            // ---------------------------- j_code branch_cd --------------------------- //
            $error = false;


            if (empty($post->j_code_branch_code_from) && !empty($post->j_code_branch_code_to)) {
                $error_msg['j_code_error_msg'] = lang('j_code_error');
            } elseif (!empty($post->j_code_branch_code_from) && !empty($post->j_code_branch_code_to)) {
//#7067:Start

// 		    	$standadize_jcode_range = $this->standadize_jcode_range($post->j_code_branch_code_from, 0, $max_jcode_length);
//            	if (!is_null($standadize_jcode_range))
//            		$post->j_code_branch_code_from = $standadize_jcode_range;
//
//            	$standadize_jcode_range = $this->standadize_jcode_range($post->j_code_branch_code_to, 9, $max_jcode_length);
//            	if (!is_null($standadize_jcode_range))
//            		$post->j_code_branch_code_to = $standadize_jcode_range;

            	//echo "From: " . $post->j_code_branch_code_from . "<br>";
            	//echo "To: " . $post->j_code_branch_code_to . "<br>";

            	#7067:End
//                $char_buff_from = str_split($post->j_code_branch_code_from);
//                $char_buff_to = str_split($post->j_code_branch_code_to);
//
//                if (count($char_buff_from) < count($char_buff_to)) {
//                	#7067:Start
//                	/*
//                    foreach ($char_buff_from as $key => $value) {
//                        if (ord($char_buff_to[$key]) > ord($value)) {
//                            $error_msg['j_code_error_msg'] = lang('j_code_error');
//                            $error = true;
//                            break;
//                        }
//                    }
//                    */
//                	$error = $this->validate_search_range($post->j_code_branch_code_from, $post->j_code_branch_code_to);
//                	if ($error)
//                		$error_msg['j_code_error_msg'] = lang('j_code_error');
//                    #7067:End
//                } else {
//                	#7067:Start
//                    //if (strpos($post->j_code_branch_code_from, $post->j_code_branch_code_to) !== false && count($char_buff_from) != count($char_buff_to)) {
//
//                	if (strcmp(trim($post->j_code_branch_code_from), trim($post->j_code_branch_code_to)) > 0) {
//                    #7067:End
//                        $error_msg['j_code_error_msg'] = lang('j_code_error');
//                        $error = true;
//                    }
//
//                    if (!$error) {
//                    	#7067:Start
//						/*
//                        foreach ($char_buff_to as $key => $value) {
//                            if (ord($char_buff_from[$key]) > ord($value)) {
//                                $error_msg['j_code_error_msg'] = lang('j_code_error');
//                                $error = true;
//                                break;
//                            }
//                        }
//						*/
//                    	if ($error)
//                    		$error_msg['j_code_error_msg'] = lang('j_code_error');
//                        #7067:End
//                    }
//                }
                $this->load->model('sales_slip_model');
                if (!$this->sales_slip_model->_validate_jcode_branchcd_range($post->j_code_branch_code_from, $post->j_code_branch_code_to)) {
                    $error_msg['j_code_error_msg'] = lang('j_code_error');
                    $error = true;
                }
//#7067:End
            }

            if ((strpos($post->j_code_branch_code_from, '-') !== FALSE) && (strlen($post->j_code_branch_code_from) - strpos($post->j_code_branch_code_from, '-') > 3)) {
                $error_msg['j_code_error_msg'] = str_replace('%field%', 'From', lang('common_msg_search_range_invalid'));
                $error = true;
            }

            if (!$error) {
                $params['conditions']['j_code_branch_cd'] = array(
                    'from' => $post->j_code_branch_code_from,
                    'to' => $post->j_code_branch_code_to
                );
            }

            // ---------------------------- End j_code branch_cd --------------------------- //

            if (!empty($post->s_code)) {
                $params['conditions']['s_code'] = $post->s_code;
            }

            if (isset($post->client_name) && ! empty($post->client_name)) {
                $params['conditions']['client_name'] = $post->client_name;
            }

            if (isset($post->report_is_complete) && ! empty($post->report_is_complete)) {
                $params['conditions']['report_is_complete'] = $post->report_is_complete;
            }

            if (isset($post->arrange_rep) && ! empty($post->arrange_rep)) {
                $params['conditions']['arrange_rep'] = $post->arrange_rep;
            }
        }

        $params['conditions']['is_reporting'] = true;

        if (!empty($error_msg)) {
            echo $this->ajax_json(array('result' => false, 'msg' => $error_msg, 'data' => array())); exit();
        }
//9876:Start
        $bu_role_info = $this->get_bu_role_info();
        $bu_name = $bu_role_info["bu_name"];
        $params['conditions']['bu_name'] = $bu_name;
//9876:End
        //get data
//7185:Start
        $params['conditions']['require_bu_id'] = true;
        $order_acceptances = $this->order_acceptance_model->get_order_acceptance_by_conditions($params);
//7185:End
        $finish_work_status_configs = $this->gui_parts_element_model->get_by_gui_parts_column_name('report_is_complete_status', $columns);
        foreach ($order_acceptances as &$item) {
            foreach ($finish_work_status_configs as $val) {
                if ($val['code'] == $item['report_is_complete']) {
                    $item['report_is_complete'] = $val['name'];

                    if ($val['code'] == 2) {
                        $item['report_is_complete'] = '済';
                    }

                }
            }
        }

        //return data
        echo $this->ajax_json(array('result' => true, 'msg' => array(), 'data' => $order_acceptances)); exit();
    }

    #7067:Start

    public function validate_search_range($from_value, $to_value) {

    	$error = false;

    	$char_buff_from = str_split($from_value);
    	$char_buff_to = str_split($to_value);

    	$tmp_j_code_branch_code_from = $from_value;
    	$tmp_j_code_branch_code_to = $to_value;

    	$tmp_j_code_branch_code_from = str_replace("-", "", $tmp_j_code_branch_code_from);
    	$tmp_j_code_branch_code_to = str_replace("-", "", $tmp_j_code_branch_code_to);

    	if (is_numeric($tmp_j_code_branch_code_from) && is_numeric($tmp_j_code_branch_code_to)) {
    		if (strlen($tmp_j_code_branch_code_from) == strlen($tmp_j_code_branch_code_to)) {
    			if ((int) $tmp_j_code_branch_code_from > (int) $tmp_j_code_branch_code_to) {
    				$error = true;
    			}
    		} else {

    			if (strlen($tmp_j_code_branch_code_from) > strlen($tmp_j_code_branch_code_to)) {
    				$tmp_j_code_branch_code_from = substr($tmp_j_code_branch_code_from, 0, strlen($tmp_j_code_branch_code_to));
    			} else {
    				$tmp_j_code_branch_code_to = substr($tmp_j_code_branch_code_to, 0, strlen($tmp_j_code_branch_code_from));
    			}

    			if ((int) $tmp_j_code_branch_code_from > (int) $tmp_j_code_branch_code_to) {
    				$error = true;
    			}
    		}
    	} else {
    		foreach ($char_buff_to as $key => $value) {
    			if (ord($char_buff_from[$key]) > ord($value)) {
    				$error = true;
    				break;
    			}
    		}
    	}

    	return $error;

    }
    #7067:End

    /**
     * Ajax update order acceptance
     *
     * @param unknow
     * @author hoang_minh
     * @since 2015-06-04
     */
    public function ajax_update_order_acceptance() {

        //load model
        $this->load->model('gui_parts_element_model');
        $this->load->model('order_acceptance_model');

        //init data
        $params = array();
        $error_msg = array();

        //check data
        if ($post = json_decode($this->input->raw_input_stream)) {

            $this->load->library('form_validation');
            // create rules check fax no
            $config = array(
                array(
                    'field'  => 'fax_no',
                    'label'  => 'FAX',
                    'rules'  => 'trim|phone[{2}-{4}-{4}]',
                    'errors' => array(
                        'required' => lang('report_fax_no_required'),
                        'phone' => str_replace('%field%', 'FAX', lang('msg_enter_hyphen_and_num'))
                    )
                )
            );

            $data = array('fax_no' => $post->report_fax_no);

            $this->form_validation->set_rules($config);
            $this->form_validation->set_data($data);

            if ($this->form_validation->run() === FALSE) {
                $error_msg['title'] = lang('update_fail');
                $error_msg['message'] = str_replace(array('<p>', '</p>'), '', validation_errors());
            }

            if (empty($error_msg)) {

                $params['update_data'] = array(
                        'report_charge_name'    => $post->report_charge_name,
                        'report_division_name'  => $post->report_division_name,
                        'report_fax_no'         => $post->report_fax_no,
                        'report_contents'       => $post->report_contents,
                        'report_remarks'        => $post->report_remarks,
                        'lastup_account_id'     => $this->auth->get_account_id(),
                        'lastup_datetime'       => $this->order_acceptance_model->_db_now()
                );

                $params['update_conditions'] = array('id' => $post->id);

                if ( ! $this->order_acceptance_model->update_order_acceptance_by_params($params)) {
                    $error_msg[] = lang('update_fail');
                    echo $this->ajax_json(array('result' => false, 'msg' => $error_msg)); exit();
                }

                $error_msg[] = lang('update_success');
                echo $this->ajax_json(array('result' => true, 'msg' => $error_msg)); exit();
            }
            echo $this->ajax_json(array('result' => false, 'msg' => $error_msg)); exit();
        }

        redirect('/_admin/error/show_404');
    }

    /**
     * Print report
     *
     * @author hoang_minh
     * @since 2015-06-05
     */
    public function print_report() {
        //load libaray
        $this->load->library('report_template');
        $this->load->model('order_acceptance_model');
//7910:Start
        $this->load->model('order_detail_model');
//7910:End
        $this->load->library('tcpdf/Tcpdf.php');
        $this->load->library('tcpdf/fpdi.php');
        $this->load->helper('zenrin_pdf_helper');

        //config fpdi
        $fpdi = new FPDI();
        $regularFont     = 'msgothic';
        $fpdi->SetFont($regularFont, '', 11);
        $fpdi->setPrintHeader(false);
        $fpdi->setPrintFooter(false);
        $fpdi->SetAutoPageBreak(TRUE, 0);

//7910:Start
        $bu_role_info = $this->get_bu_role_info();
        $bu_name = $bu_role_info["bu_name"];
//7910:End

        //init variables
        $params = array();

        if ($post = $this->input->post()) {
            if (empty($post['ids'])) {
                redirect('/_admin/finish_work_report/show');
            }

            $params['conditions'] = array(
                    'ids' => $post['ids'],
                    'is_reporting' => true,
            		'bu_name' => $bu_name
            );

            //get order acceptance
            $order_acception = $this->order_acceptance_model->get_order_acceptance_by_conditions($params);
            $page = 1;
            foreach ($order_acception as $item) {
                $update_data = array(
                        'report_is_complete' => ORDER_ACCEPTANCE_IS_REPORT_ALREADY,
                        'lastup_account_id' => $this->auth->get_account_id(),
                        'lastup_datetime' =>  $this->order_acceptance_model->_db_now()
                );

                $update_conditions = array(
                        'id' => $item['id']
                );

                //update order acceptance
                if ( ! $this->order_acceptance_model->update_order_acceptance_by_params(array('update_data' => $update_data, 'update_conditions' => $update_conditions))) {
                    $this->app_logger->error_log('Print report fail. Update order acceptance id: ' . $item['id'] . 'fail');

                    redirect('/_admin/finish_work_report/show');
                }

                $fpdi->AddPage('L', 'A4');
//7910:Start
                //$fpdi->setSourceFile(APPPATH . 'config/pdf_template/finish_work_report/template.pdf');
                $fpdi->setSourceFile(APPPATH . 'config/pdf_template/finish_work_report/' . $bu_role_info["pdf_template_file"]);
//7910:End
                $y = 0;

                $header_id = $fpdi->importPage(1);
                $fpdi->useTemplate($header_id, null, null, null, null, true);
                $fpdi->setPage($page);

                //set data
                //7185:Start
                if (strtotime($item['delivery_date']) <= 0 && $item['delivery_date'] == "0000-00-00") {
                	$delivery_year = "0000";
                	$delivery_month = "00";
                	$delivery_day = "00";
                } else {
                	$delivery_year = date('Y', strtotime($item['delivery_date']));
                	$delivery_month = date('m', strtotime($item['delivery_date']));
                	$delivery_day = date('d', strtotime($item['delivery_date']));
                }
                //7185:End
//7910:Start
//7989:Start
                $this->load->model('gui_parts_element_model');
                $client_name = $item['client_name'];
                $code_to_found = 0;
                $merchandise_name = "";

                if (in_array($item['merchandise_name_pos'], array("A","B"))) {

                	if ($item['merchandise_name_pos'] == "A")
                		$code_to_found = $item['corporate_status_after'];
                	else
                		$code_to_found = $item['corporate_status_before'];

                	$gui_parts_elements = $this->gui_parts_element_model->get_business_status_info_by_code($code_to_found);
                	foreach ($gui_parts_elements as $gui_parts_element) {
                		$merchandise_name = $gui_parts_element['name'];
                	}

                	if (strlen(trim($merchandise_name)) > 0) {
                		if ($item['merchandise_name_pos'] == "B")
                			$client_name = $merchandise_name . $client_name;
                		elseif ($item['merchandise_name_pos'] == "A")
                			$client_name = $client_name . $merchandise_name;
                	}
                }

//7989:End
                if ($bu_name == 'DP') {
                	//Client Name: ....御中
                	//$fpdi->setXY(18, 24 + $y);

//7989:Start
                	//$fpdi->cell(100, 98, $item['client_name'], 0, '', 'R');
                	//$fpdi->cell(100, 98, $client_name . "(" . $client_name_length. ")", 0, '', 'R');

//                 	$client_name_length = mb_strlen($client_name, 'UTF-8');

//                 	if ($client_name_length <= 14) {
                		$fpdi->setFontSize(14);
//                 	} elseif ($client_name_length > 14 && $client_name_length <= 20) {
//                 		$fpdi->setFontSize(9.5);
//                 	} else {
//                 		$fpdi->setFontSize(6);
//                 	}
                	$fpdi->setXY(54, 67 + $y);
					$fpdi->cell(67, 12, $client_name, 0, '', 'R', false,'',1);
					//$this->_fpdi->cell(59.3, 6, $data[$i]['client_name'], 1, '', 'L', false, '', $stretch);
//7989:End
                	//J Code + Branch CD
                	$fpdi->setXY(122, 45.5 + $y); //130
                	$fpdi->setFontSize(14);
                	$fpdi->cell(167, 108, $item['j_code'] . '-' . str_pad($item['branch_cd'], 2, '0', STR_PAD_LEFT), 0, '', 'C');

                	//Current Year-Month-Date
                	$cur_year = date("Y");
                	$cur_month = date("m");
                	$cur_day = date("d");

                	$fpdi->setXY(55, 64 + $y);
                	$fpdi->setFontSize(14);
                	$fpdi->cell(100, 98, $cur_year, 0, '', 'L');
                	$fpdi->setXY(71, 64 + $y);
                	$fpdi->cell(100, 98, $cur_month, 0, '', 'L');
                	$fpdi->setXY(84, 64 + $y);
                	$fpdi->cell(100, 98, $cur_day, 0, '', 'L');

                	//広告主名
                	$fpdi->setFontSize(16);
//8225:Start
                	//$fpdi->setXY(60, 71.2 + $y);
                	//$fpdi->cell(200, 115, $item['client_name'], 0, '', 'C');
                	$fpdi->setXY(97, 125 + $y);
                	$fpdi->cell(129, 8, $client_name, 0, '', 'C', false,'',1);
//8225:End
                	//媒体名
                	$dp_report_content = "";
                	$order_detail_info = $this->order_detail_model->get_order_detail_info_by_orderid_branchcd($item['root_order_id'], $item['branch_cd']);
                	if (count($order_detail_info) > 0) {
                		foreach ($order_detail_info as $val) {
                			if (strlen(trim($val["contents"])) > 0)
                				$dp_report_content = $val["contents"];
                		}
                	}
//8225:Start
                	//$fpdi->setXY(60, 93 + $y);
                	//$fpdi->cell(200, 115, $item['report_division_name'], 0, '', 'C');
                	//$fpdi->cell(200, 115, $dp_report_content, 0, '', 'C');
                	$fpdi->setXY(97, 146.5 + $y);
                	$fpdi->cell(129, 8, $dp_report_content, 0, '', 'C', false, '', 1);
//8225:End
                	//掲載部数
                	$fpdi->setXY(60, 102 + $y);
                	$fpdi->cell(200, 115, $item['quantity'], 0, '', 'C');

                	//配布期間（掲載期間） 納品日
                	$fpdi->setXY(60, 111 + $y);
                	$fpdi->cell(200, 115, $delivery_year . '年' . $delivery_month . '月' . $delivery_day . '日', 0, '', 'C');

                	//＜備考＞
                	$fpdi->setXY(55, 175 + $y);
                	//$fpdi->MultiCell(170, 100, $item['report_remarks'], "0" , 'L', false);
//8225:Start
					//This line of code stretch long value in one cell.
                	$fpdi->MultiCell(170, 48, $item['report_remarks'], 0, 'L', false, 0, 56, 175, true, 0, false, false, 0, 'T', true);
//8225:End

//7910:End
            } else {
//7989:Start
	                //$fpdi->setXY(13, 3 + $y);
	                //$fpdi->setFontSize(14);

	                //$fpdi->cell(100, 98, $item['client_name'], 0, '', 'R');
	                //$fpdi->cell(100, 98, $client_name, 0, '', 'R');

					/*
	                $fpdi->setXY(13, 7 + $y);
	                $fpdi->setFontSize(14);
	                $fpdi->cell(100, 115, $item['report_division_name'], 0, '', 'R');
					*/

	                /*
	                 $fpdi->setXY(13, 11 + $y);
	                 $fpdi->setFontSize(14);
	                 $fpdi->cell(100, 115, $item['report_division_name'], 0, '', 'R');
					*/

	                //$client_name_length = mb_strlen($client_name, 'UTF-8');
					//echo $client_name_length;

	                //if ($client_name_length <= 17) {
	                	$fpdi->setFontSize(14);
	                //} elseif ($client_name_length > 17 && $client_name_length <= 23) {
	                //	$fpdi->setFontSize(10.5);
	                //} else {
	                //	$fpdi->setFontSize(7.5);
	                //}
	                	//$fpdi->setXY(54, 67 + $y);
	                	//$fpdi->cell(67, 12, $client_name, 0, '', 'R', false,'',1);

	                $fpdi->setXY(28, 48 + $y);
	                $fpdi->cell(85, 12, $client_name, 0, '', 'R', false, '', 1);
//7989:End
	                $fpdi->setXY(13, 7 + $y);
	                $fpdi->setFontSize(14);
	                $fpdi->cell(100, 115, $item['report_division_name'], 0, '', 'R');

	                $fpdi->setXY(14, 7 + $y);
	                $fpdi->setFontSize(14);
	                $fpdi->cell(92, 135, $item['report_charge_name'], 0, '', 'R');

	                $fpdi->setXY(10, 6 + $y);
	                $fpdi->setFontSize(14);
	                $fpdi->cell(100, 157, $item['report_fax_no'], 0, '', 'R');

	                //7185:Start
	                $fpdi->setXY(35, 55 + $y); //61
	                //$fpdi->setXY(35, 63 + $y); //61
	                //7185:End
	                $fpdi->setFontSize(16);
	                $fpdi->cell(167, 225, $delivery_year . '年' . $delivery_month . '月' . $delivery_day . '日', 0, '', 'C');

	                //7185:Start

	                $fpdi->setXY(35, 125.5 + $y); //130
	                $fpdi->setFontSize(16);
	                $fpdi->cell(167, 108, $item['j_code'] . '-' . str_pad($item['branch_cd'], 2, '0', STR_PAD_LEFT), 0, '', 'C');


	                //$fpdi->setXY(35, 126 + $y);
	                $report_contents_length = mb_strlen($item['report_contents'], 'UTF-8');
	                $font_size_setting = 16;
	                if ($report_contents_length > 20)
	                	$font_size_setting = 11;

	                //$fpdi->setXY(35, 121 + $y);
//8225:Start
	                //$fpdi->setXY(43, 126 + $y);
	                $fpdi->setXY(70, 186 + $y);
//8225:End
	                $report_contents = $item['report_contents'];
	                $fpdi->setFontSize($font_size_setting);
//8225:Start
	                //$fpdi->cell(167, 131, $report_contents, 0, '', 'C');
	                $fpdi->cell(113.5, 10, $report_contents, 0, '', 'C', false, '', 1);
//8225:End
	                //7185:End

	                //7185:Start
//8225:Start
	                //$fpdi->setXY(35, 136.5 + $y);
	                $fpdi->setXY(70, 197 + $y);
//8225:End
	                //7185:End
	                //$fpdi->setXY(35, 132 + $y);
	                $fpdi->setFontSize(16);
//8225:Start
	                //$fpdi->cell(167, 131, $item['report_remarks'], 0, '', 'C');
	                $fpdi->cell(113.5, 10, $item['report_remarks'], 0, '', 'C', false, '', 1);
//8225:End
            	}
                $page++;
            }
            $fpdi->Output();
        }

        redirect('/_admin/finish_work_report/show');
    }

    /**
     * Print report
     *
     * @author hoang_minh
     * @since 2015-06-09
     */
    public function export_excel() {

        //load model
        $this->load->model('order_acceptance_model');

//7910:Start
        $this->load->model('order_detail_model');
//7910:End

        //load library
        $this->load->library('PHPExcel/PHPExcel');

        //init data
        $params = array();

//7910:Start
        $bu_role_info = $this->get_bu_role_info();
        $bu_name = $bu_role_info["bu_name"];
//7910:End

        if ($post = $this->input->post()) {
            $params['conditions'] = array(
                    'id' => $post['id'],
                    'is_reporting' => true,
            		'bu_name' => $bu_name
            );

            //get order acceptance
            $order_acception = $this->order_acceptance_model->get_order_acceptance_by_conditions($params);
            //if ( ! empty($order_acception) && $order_acception[0]['report_fax_no']) { //comment out due to the request #6483
                //get config
//7910:Start
    			//$excel_template_path = config_item('excel_template_path') . $this->_excel_template_file_name;
    			$excel_template_path = config_item('excel_template_path') . $bu_role_info["excel_template_file"];
//7910:End

                //processing download
                if (file_exists($excel_template_path) && count($order_acception) > 0) {

                    //Read your Excel workbook
                    try {
                        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                        $objPHPExcel = $objReader->load($excel_template_path);
                        $objPHPExcel->setActiveSheetIndex(0);

                        //replace content
                        //7185:Start
                        if (strtotime($order_acception[0]['delivery_date']) <= 0 && $order_acception[0]['delivery_date'] == "0000-00-00") {
                        	$delivery_year = "0000";
                        	$delivery_month = "00";
                        	$delivery_day = "00";
                        } else {
                        	$delivery_year = date('Y', strtotime($order_acception[0]['delivery_date']));
                        	$delivery_month = date('m', strtotime($order_acception[0]['delivery_date']));
                        	$delivery_day = date('d', strtotime($order_acception[0]['delivery_date']));
                        }
                        //7185:End
//7910:Start
//7989:Start
                        $this->load->model('gui_parts_element_model');
                        $client_name = $order_acception[0]['client_name'];
                        $code_to_found = 0;
                        $merchandise_name = "";

                        if (in_array($order_acception[0]['merchandise_name_pos'], array("A","B"))) {

                        	if ($order_acception[0]['merchandise_name_pos'] == "A")
                        		$code_to_found = $order_acception[0]['corporate_status_after'];
                        	else
                        		$code_to_found = $order_acception[0]['corporate_status_before'];

                        	$gui_parts_elements = $this->gui_parts_element_model->get_business_status_info_by_code($code_to_found);
                        	foreach ($gui_parts_elements as $gui_parts_element) {
                        		$merchandise_name = $gui_parts_element['name'];
                        	}

                        	if (strlen(trim($merchandise_name)) > 0) {
                        		if ($order_acception[0]['merchandise_name_pos'] == "B")
                        			$client_name = $merchandise_name . $client_name;
                        		elseif ($order_acception[0]['merchandise_name_pos'] == "A")
                        			$client_name = $client_name . $merchandise_name;
                        	}
                        }

//7989:End
                        if ($bu_name == "DP") {
//7989:Start
                        	//$objPHPExcel->getActiveSheet()->setCellValue('B8', $order_acception[0]['client_name']);
                        	$objPHPExcel->getActiveSheet()->setCellValue('B8', $client_name);
//7989:End
                        	$objPHPExcel->getActiveSheet()->setCellValue('H14', '弊社案件コード：  ' . $order_acception[0]['j_code'] . '-' . str_pad($order_acception[0]['branch_cd'], 2, '0', STR_PAD_LEFT));
                        	$objPHPExcel->getActiveSheet()->setCellValue('B17', ' ' . date('Y') . ' 年' . ' ' . date('m') . ' 月 ' . date('d') . ' 日');
                        	#$objPHPExcel->getActiveSheet()->setCellValue('D20', $order_acception[0]['client_name']);
                        	$objPHPExcel->getActiveSheet()->setCellValue('D20', $client_name);

                        	$dp_report_content = "";
                        	$order_detail_info = $this->order_detail_model->get_order_detail_info_by_orderid_branchcd($order_acception[0]['root_order_id'], $order_acception[0]['branch_cd']);
                        	if (count($order_detail_info) > 0) {
                        		foreach ($order_detail_info as $val) {
                        			if (strlen(trim($val["contents"])) > 0)
                        				$dp_report_content = $val["contents"];
                        		}
                        	}

                        	//$objPHPExcel->getActiveSheet()->setCellValue('D24', $order_acception[0]['report_division_name']);
                        	$objPHPExcel->getActiveSheet()->setCellValue('D24', $dp_report_content);
                        	$objPHPExcel->getActiveSheet()->setCellValue('D26', $order_acception[0]['quantity']);
                        	$objPHPExcel->getActiveSheet()->setCellValue('D28', $delivery_year . '年' . $delivery_month . '月' . $delivery_day . '日');
                        	$objPHPExcel->getActiveSheet()->setCellValue('B30', $order_acception[0]['report_remarks']);
//7910:End
                        } else { //means: DM or other Business Units defined in business_unit table
//7989:Start
	                        //$objPHPExcel->getActiveSheet()->setCellValue('A3', $order_acception[0]['client_name']);
                        	$objPHPExcel->getActiveSheet()->setCellValue('A3', $client_name);
//7989:End
	                        $objPHPExcel->getActiveSheet()->setCellValue('A4', $order_acception[0]['report_division_name']);
	                        $objPHPExcel->getActiveSheet()->setCellValue('A5', $order_acception[0]['report_charge_name']);
	                        $objPHPExcel->getActiveSheet()->setCellValue('J6', $order_acception[0]['report_fax_no']);
	                        $objPHPExcel->getActiveSheet()->setCellValue('J18', $delivery_year . '年' . $delivery_month . '月' . $delivery_day . '日');
	                        $objPHPExcel->getActiveSheet()->setCellValue('J20', $order_acception[0]['j_code'] . '-' . str_pad($order_acception[0]['branch_cd'], 2, '0', STR_PAD_LEFT));
	                        $objPHPExcel->getActiveSheet()->setCellValue('J22', $order_acception[0]['report_contents']);
	                        $objPHPExcel->getActiveSheet()->setCellValue('J24', $order_acception[0]['report_remarks']);

	                        //replace image
	                        $objDrawing = new PHPExcel_Worksheet_Drawing();
	                        $objDrawing->setName("logo");
	                        $objDrawing->setDescription("logo");
	                        $objDrawing->setPath('./public/admin/finish_work_report/img/excel_logo.png');
	                        $objDrawing->setCoordinates('U3');
	                        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

	                        $objDrawing = new PHPExcel_Worksheet_Drawing();
	                        $objDrawing->setName("logo");
	                        $objDrawing->setDescription("logo");
	                        $objDrawing->setPath('./public/admin/finish_work_report/img/excel_note.png');
	                        $objDrawing->setCoordinates('B27');
	                        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
                    	}
                        //output
                        $output_filename = filename_to_urlencode('作業完了報告_' . $order_acception[0]['client_name'] . '_' . $order_acception[0]['j_code'] . '_' . $order_acception[0]['branch_cd'] . '_' . date('Ymd', now()) . '.xlsx');

                        header("Pragma: public");
                        header("Expires: 0");
                        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                        header("Content-Type: application/force-download");
                        header("Content-Type: application/octet-stream");
                        header("Content-Type: application/download");
                        header('Content-Disposition: attachment;filename="' . $output_filename . '"');
                        header("Content-Transfer-Encoding: binary ");
                        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                        $objWriter->save('php://output');

                        unset($objPHPExcel);
                        unset($objWriter);
                        exit();

                    } catch (Exception $ex) {
                        $this->app_logger->error_log('Export excel fail.' . $ex->getMessage());
                        exit();
                    }
                }
            //} //comment out due to the request #6483
        }

        redirect('/_admin/finish_work_report/show');
    }

//7910:Start
    public function get_bu_role_info() {

    	$this->load->model('order_model');
    	$this->load->model('gui_parts_element_model');
    	$this->load->library('auth');

    	$logged_id = $this->auth->get_account_id();
    	$logged_user_bu = $this->order_model->get_business_unit_of_logged_user($logged_id);
    	$finish_work_report_template = $logged_user_bu['finish_work_report_template'];
		$business_unit_template = $this->gui_parts_element_model->get_by_gui_parts_column_name('business_unit_template', 'code, name');
		$bu_code = "others";

		if ($logged_user_bu['is_product_division'] == 1) {
			foreach ($business_unit_template as $val) {
				$business_unit_template_code = $val['code'];
				if ($business_unit_template_code == $finish_work_report_template){
					if ($business_unit_template_code == DM_FWR_TEMPLATE_CODE) //DM
						$bu_code = "DM";
					elseif ($business_unit_template_code == DP_FWR_TEMPLATE_CODE) //DP
					$bu_code = "DP";
				}
			}
		}

    	$config_info = $this->config->item('bu_roles');

    	return $config_info[$bu_code][$this->router->fetch_class()];

    }
//7910:End
}