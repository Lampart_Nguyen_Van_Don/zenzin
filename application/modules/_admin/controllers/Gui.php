<?php

class Gui extends ZR_Controller {
    public function show() {
        $this->load->model('gui_parts_model');
        $this->load->model('gui_parts_element_model');

        $gui_parts = $this->gui_parts_model->get_all();

        $default_gui_part_id = null;
        if (!empty($gui_parts)) {
            $default_gui_part_id = $gui_parts[0]['id'];
        }

        if ($this->input->post('type')) {
            $temp_gui_parts = $this->gui_parts_model->get_by_id($this->input->post('type'));
            if (!empty($temp_gui_parts)) {
                $default_gui_part_id = $temp_gui_parts['id'];
            }
        }

        $elements = $this->gui_parts_element_model->get_all_by_gui_parts_id($default_gui_part_id);

        $this->assign('gui_parts', $gui_parts);
        $this->assign('elements', $elements);
        $this->view('gui/show.tpl');
    }

    public function detail() {
        if ($this->input->is_ajax_request()) {
            $this->load->model('gui_parts_element_model');
            $id = $this->input->post('element_id');
            $element = $this->gui_parts_element_model->get_by_id($id);

            if (empty($element)) {
                $this->ajax_json(array(
                                    'error_code' => 400,
                                    'messages' => lang('lbl_invalid_input')
                                ));
                return;
            }

            $this->assign('element', $element);
            $this->view('gui/detail.tpl');
        } else {
            show_404();
        }
    }

    public function edit() {
        if ($this->input->is_ajax_request()) {
            $this->load->model('gui_parts_element_model');
            $this->load->model('gui_parts_model');

            $id = $this->input->post('element_id');
            $element = $this->gui_parts_element_model->get_by_id($id);

            if (empty($element)) {
                $this->ajax_json(array(
                                    'error_code' => 400,
                                    'messages' => lang('lbl_invalid_input')
                                ));
                return;
            }

            $gui_part = $this->gui_parts_model->get_by_id($element['gui_parts_id']);

            $this->assign('element', $element);
            $this->assign('gui_part', $gui_part);
            $this->view('gui/edit.tpl');
        } else {
            show_404();
        }
    }

    public function update() {

        if ($this->input->is_ajax_request()) {

            $id = $this->input->post('element_id');

            $this->load->model('gui_parts_model');
            $this->load->model('gui_parts_element_model');

            $element = $this->gui_parts_element_model->get_by_id($id);
            if (empty($element)) {
                $status = array(
                            'status'   => false,
                            'messages' => lang('lbl_edit_error'),
                        );
            } else {

                if ($this->gui_parts_element_model->validate('edit')) {

                	$gui_part = $this->gui_parts_model->get_by_id($element['gui_parts_id']);
                	$data = array();

                	if ($gui_part['is_change_name_flg']) {
                		$data['name'] = $this->input->post('name');
                	}

                    if ($gui_part['is_change_new_add_flg']) {
                        $data['is_no_add'] = $this->input->post('is_no_add') ? 1 : 0;
                    }

                    if ($gui_part['is_change_sort_flg']) {
                    	$data['sequence']  = $this->input->post('sequence');
                    }

                    $data = array_map('trim', $data);

                    $success = $this->gui_parts_element_model->update_gui_parts_element($id, $data);
                    $status = array(
                                'status'   => $success ? true : false,
                                'messages' => $success ? lang('lbl_edit_success') : lang('lbl_edit_error'),
                            );
                } else {
                    $status = array(
                                'status'   => false,
                                'messages' => array(
                                                'name'      => form_error('name'),
                                                'sequence'  => form_error('sequence'),
                                                'is_no_add' => form_error('is_no_add'),
                                            )
                            );
                }
            }

            echo json_encode($status);
        } else {
            show_404();
        }
    }

    public function add() {
        if ($this->input->is_ajax_request()) {
            $this->load->model('gui_parts_model');
            $this->load->model('gui_parts_element_model');

            $gui_parts_id = $this->input->post('gui_parts_id');
            $name = $this->input->post('name');

            $gui_part = $this->gui_parts_model->get_by_id($gui_parts_id);
            if (empty($gui_part) || !$gui_part['is_add_flg']) {
                $status = array(
                            'status'   => false,
                            'messages' => lang('lbl_add_error'),
                        );
            } else {
                if ($this->gui_parts_element_model->validate('add')) {
                    $data = array(
                            'gui_parts_id' => $gui_parts_id,
                            'name' => $name,
                        );
                    $data = array_map('trim', $data);

                    $success = $this->gui_parts_element_model->add_new_gui_parts_element($data);
                    $status = array(
                                'status' => $success ? true : false,
                                'messages' => $success ? lang('lbl_add_success') : lang('lbl_add_error'),
                            );
                } else {
                    $status = array(
                                'status'   => false,
                                'messages' => array('name' => form_error('name')),
                            );
                }
            }

            echo json_encode($status);
        } else {
            show_404();
        }
    }

    public function delete() {
        if ($this->input->is_ajax_request()) {
            $this->load->model('gui_parts_model');
            $this->load->model('gui_parts_element_model');

            $id = $this->input->post('element_id');

            $element = $this->gui_parts_element_model->get_by_id($id);

            if (empty($element)) {
                 $status = array(
                            'status'   => false,
                            'messages' => lang('lbl_invalid_input'),
                        );
            } else {
                // is this element can be deleted
                $gui_part = $this->gui_parts_model->get_by_id($element['gui_parts_id']);

				/*
                if ($gui_part['is_delete_flg']) {
                    $this->ajax_json(array(
                            'status'   => false,
                            'messages' => lang('lbl_invalid_input'),
                        ));
                    return;
                }
				*/
                //check gui part is using
                $this->load->model('gui_parts_check_delete_model');
                $gui_parts_check_delete = $this->gui_parts_check_delete_model->get_by_gui_part_id($element['gui_parts_id']);

                if ($gui_parts_check_delete) {
                    foreach ($gui_parts_check_delete as $val) {

                        $table = "`" . $val['check_table'] . "`";
                        $columns = array();
                        $conditions = array(
                                $val['colmun_name'] => $element['code']
                        );

                        $data_using_gui = $this->gui_parts_check_delete_model->get_item_by_parameters($table, $columns, $conditions);
                        if ( ! empty($data_using_gui)) {
                            echo $this->ajax_json(array('status' => false, 'messages' => lang('lbl_delete_using_error'))); exit();
                        }
                    }
                }

                $success = $this->gui_parts_element_model->delete_gui_part_element($id);
                $status = array(
                            'status'   => $success ? true : false,
                            'messages' => $success ? lang('lbl_delete_success') : lang('lbl_delete_error'),
                        );
            }
            $this->ajax_json($status);
        } else {
            show_404();
        }
    }
}