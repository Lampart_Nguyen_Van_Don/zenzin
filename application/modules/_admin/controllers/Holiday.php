<?php

class Holiday extends ZR_Controller {
    public function show() {
        $this->load->model('holiday_model');

        $current_year = $year_filter = date('Y');
        for ($year = $current_year - GAP_YEARS; $year <= $current_year + GAP_YEARS; $year++) {
            $list_of_years[] = $year;
        }

        if ($this->input->post('year')) {
            $year = $this->input->post('year');
            $year_filter = in_array($year, $list_of_years) ? $year : $current_year;
        }

        $established_years = $this->holiday_model->get_established_years();
        $holidays          = $this->holiday_model->get_all_by_year($year_filter);

        $months = array();
        foreach ($holidays as $holiday) {
            $tmp_month = intval(date('m', strtotime($holiday['holiday'])));
            $month_holidays[$tmp_month][] = intval(date('d', strtotime($holiday['holiday'])));
        }

        //CUSTOMIZE CALENDAR'S TEMPLATE
        $prefs = array(
                'start_day'    => 'sunday',
                'month_type'   => 'short',
                'day_type'     => 'short',
        );
        $prefs['template'] = array(
                'table_open'                => '<table class="holiday-calendar">',
                'week_row_start'            => '<tr class="weekday">',
                'cal_cell_content'          => '<b class="holiday-day date" data-date="{date}">{day}</b>',
                'cal_cell_content_today'    => '<b class="holiday-day date" data-date="{date}">{day}</b>',
                'cal_cell_no_content_today' => '<span class="date" data-date="{date}">{day}</span>',
                'cal_cell_no_content'       => '<span class="date" data-date="{date}">{day}</span>'
        );

        $this->load->library('calendar', $prefs);
        for ($month = 1; $month <= 12; $month++) {
            $filter = isset($month_holidays[$month]) ? array_flip($month_holidays[$month]) : array();
            $calendars[] = $this->calendar->generate($year_filter, $month, $filter);
        }

        $this->check_message();
        $this->assign('established_years', $established_years);
        $this->assign('list_of_years', $list_of_years);
        $this->assign('year_filter', $year_filter);
        $this->assign('calendars', $calendars);
        $this->view('holiday/show.tpl');
    }

    public function ajax_update() {
        if ($this->input->is_ajax_request()) {
            $this->load->model('holiday_model');

            $weekdays = $this->input->post('weekdays');
            $year     = $this->input->post('year');

            $current_year = date('Y');
            for ($tmp_year = $current_year - GAP_YEARS; $tmp_year <= $current_year + GAP_YEARS; $tmp_year++) {
                $list_of_years[] = $tmp_year;
            }

            if (!in_array($year, $list_of_years) || $year < date('Y')) {
                $this->set_message(lang('lbl_insert_error'));
                return false;
            }

            if ($this->holiday_model->insert_holiday($year, $weekdays)) {
                $this->set_message(lang('lbl_insert_success'), 'success');
                return true;
            } else {
                $this->set_message(lang('lbl_insert_error'));
                return false;
            }
        } else {
            show_404();
        }
    }

    public function ajax_set_day() {
        //load library
        $this->load->library('ical');

        if ($this->input->is_ajax_request()) {
            $this->load->model('holiday_model');

            $date  = date('Y-m-d', strtotime($this->input->post('date')));
            $today = date('Y-m-d');

            $current_year = date('Y');
            for ($tmp_year = $current_year - GAP_YEARS; $tmp_year <= $current_year + GAP_YEARS; $tmp_year++) {
                $list_of_years[] = $tmp_year;
            }

            if (strtotime($date) <= strtotime($today) || !in_array(date('Y', strtotime($date)), $list_of_years)) {
                $status = array(
                    'status'   => false,
                    'messages' => (strtotime($date) < strtotime($today)) ? lang('error_can_not_change') : lang('error_can_not_change_current_date'),
                );
            } else {
                if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {

                    //status message
                    $status = array(
                        'status'   => true,
                        'messages' => lang('lbl_insert_success'),
                    );

                    $params = array(
                            'conditions' => array(
                                'holiday' => $date,
                            ),
                            'columns' => array('id')
                    );

                    //if date which be set holiday was existing, disable it.
                    if ($holidays = $this->holiday_model->get_holidays_by_parameters($params)) {
                        if ( ! $this->holiday_model->delete_holiday(array('id' => $holidays[0]['id']))) {
                            $status = array(
                                    'status'   => false,
                                    'messages' => lang('lbl_insert_error'),
                            );
                        }
                    } else {
                        //holiday list to update
                        $insert_holiday_list   = array();
                        $response_holiday_list = array();

                        //get common holiday list
                        $year = date('Y', strtotime($date));
                        $common_holiday_list = array_keys($this->ical->apple_holiday_calendar(array('from_year'=> $year, 'to_year'=> $year)));

                        $list_can_not_be_holiday = array();
                        $db_holiday_list = $this->holiday_model->get_all_by_year($year);
                        foreach ($db_holiday_list as $holiday) {
                            $list_can_not_be_holiday[] = date('Y-m-d', strtotime($holiday['holiday']));
                        }
                        $list_can_not_be_holiday = array_merge($common_holiday_list, $list_can_not_be_holiday);
                        
                        
                        while (in_array($date, $list_can_not_be_holiday)) {
                            $insert_holiday_list[]   = $date;
                            $response_holiday_list[] = date('Y-m-d', strtotime($date));
                            $date                    = date('Y-m-d', strtotime($date . ' +1 days'));
                        }
                        $insert_holiday_list[]   = $date;
                        $response_holiday_list[] = date('Y-m-d', strtotime($date));

                        // echo "<pre>";print_r($response_holiday_list);die;

                        $status['holiday_list'] = $response_holiday_list;
                        if ( ! $this->holiday_model->insert_batch_holidays($insert_holiday_list, $year)) {
                            $status = array(
                                'status'   => false,
                                'messages' => lang('lbl_insert_error'),
                            );
                        }
                    }
                } else {
                    $status = array(
                            'status'   => false,
                            'messages' => lang('lbl_invalid_input'),
                    );
                }
            }

            $this->ajax_json($status);
        }
    }

    /**
     * @param  int $id
     * @return array
     * @author cong_minh 2015/6/1
     */
    public function check_holiday() {
        $this->load->model('holiday_model');
        $date = json_decode($this->input->raw_input_stream);
        $date = $date->date;
        $this->ajax_json($this->holiday_model->check_holiday($date));
    }

}