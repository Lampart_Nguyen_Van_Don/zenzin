<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @author cam_tien
 *
 */

class Invoice extends ZR_Controller {

    public function ajax_get_account_by_business_unit_id_invoice() {

        if (!$this->input->is_ajax_request()) show_404();

        $this->load->model('account_model');

        $business_unit_id = $this->input->post('business_unit_id');


        $accounts = $this->account_model->get_all_account_by_business_unit_resignation($business_unit_id);
        if (empty($accounts)) {
            echo 0; return;
        }

        $this->ajax_json($accounts);
    }

    public function ajax_get_all_accounts_invoice() {

        if (!$this->input->is_ajax_request()) show_404();

        $this->load->model('account_model');

        $accounts = $this->account_model->get_working_accounts('id, name');
        if (empty($accounts)) {
            echo 0; return;
        }
        $this->ajax_json($accounts);
    }


//#7434:Start
    public function export_csv(){

        $this->load->model('business_unit_model');
        $this->load->model('account_model');


        //  $date = date('Y-m-d', strtotime('+1 month'));

        // One month from a specific date
        $form_data = array();
        $form_data = $_REQUEST;
//     	print_r($form_data);
        $this->load->model('sales_slip_model');
        $this->load->model('closing_accountant_model');
        $j_accounts = array();
        $default_j_account = $default_j_account_business_unit = null;

        $account_business_unit    = $this->auth->get_business_unit();
        $j_account_business_units = $account_business_unit;
        $is_product_division      = $account_business_unit[0]['is_product_division'];

        $s_account_business_units = $this->business_unit_model->get_product_business_unit_id_name();
        $s_accounts = $this->account_model->get_working_accounts('id, name');


        $is_self_invoices = $this->auth->has_permission('self_invoices', 'column');

        if (!$is_self_invoices) {
            $j_account_business_units = $s_account_business_units;
        }

        if (!$is_self_invoices && $is_product_division) {

            $j_accounts = $this->account_model->get_all_account_by_business_unit_resignation($account_business_unit[0]['id']);

            $default_j_account               = $this->auth->get_account_id();
            $default_j_account_business_unit = $account_business_unit[0]['id'];
        } else {
            $j_accounts = $s_accounts;
        }


        if ($is_self_invoices) {
            $auth_data = $this->auth->get_auth_data();
            $j_accounts = array(array(
                    'id' => $auth_data['account_id'],
                    'name' => $auth_data['userdata']['name'],
            ));
        }
        $error_messages = $sales_slips = array();


        //#6688:Start
        $is_submit_default = 0;
        //#6688:End

        if(isset($_REQUEST['j_account_id'])){

                $form_data['j_account_id'] = $_REQUEST['j_account_id'];

        }else{
            if ($is_self_invoices && $is_product_division)
                $form_data['j_account_id'] = $this->auth->get_account_id();
        }

//     	echo $form_data['j_account_business_unit_id'];
//     	die();
        if(isset($_REQUEST['j_account_business_unit_id'])){

                $form_data['j_account_business_unit_id'] = $_REQUEST['j_account_business_unit_id'];

        }
        else{

            if ($is_product_division)
                $form_data['j_account_business_unit_id'] = $default_j_account_business_unit;

        }



        if (isset($_REQUEST['is_link_from_order'])) {
            $is_submit_default = 1;
        }
        else{
            $is_submit_default = 0;
        }

        $this->assign('is_link_from_order', $is_submit_default);

        if (isset($_REQUEST['s_code'])) {
            $form_data['s_code'] = $_REQUEST['s_code'];
        }

        //#6688:Start
        if (isset($_REQUEST['delivery_date'])) {
            if (parse_date($_REQUEST['delivery_date'])) {
                    $closedaydevilyday = date('Y/m', strtotime($_REQUEST['delivery_date']));
            } else {
                $closedaydevilyday = $_REQUEST['delivery_date'];
            }
        }

        if (isset($form_data['j_account_business_unit_id']) && ($form_data['j_account_business_unit_id'] != -1) && !$is_self_invoices) {

            $j_accounts = $this->account_model->get_all_account_by_business_unit($form_data['j_account_business_unit_id']);

        }

        if(($form_data['s_account_business_unit_id'] != -1)&& ($form_data['s_account_id'] == -1)){

                $j_accounts_all = $this->account_model->get_all_account_by_business_unit($form_data['s_account_business_unit_id']);
                $form_data['s_account_id_all'] = $j_accounts_all;
        }

        $sales_slips = $this->sales_slip_model->search_invoices($form_data);


         $date = date('Y-m-d', strtotime('+1 month'));

//     	One month from a specific date



        $this->load->model('order_acceptance_model');
        $this->load->model('account_model');
        $this->load->model('subcontractor_model');
        $this->load->model('shipping_type_model');
        $this->load->model('gui_parts_element_model');

        $order_acceptances = $this->order_acceptance_model->search($this->input->post());
        $working_accounts = $this->account_model->get_working_accounts('id, name');
        $subcontractors = $this->subcontractor_model->get_subcontractors();
        $shipping_types = $this->shipping_type_model->get_all();
        $shipments_shapes = $this->gui_parts_element_model->get_all_shipment_shape_elements();

        $header = mb_convert_encoding('請求書番号 ','SJIS','UTF-8') . ','
                . mb_convert_encoding('ステータス','SJIS','UTF-8') . ','
                . mb_convert_encoding('J担当','SJIS','UTF-8'). ','
                . mb_convert_encoding('Sコード','SJIS','UTF-8'). ','
                . mb_convert_encoding('クライアント名','SJIS','UTF-8').','
                . mb_convert_encoding('請求日','SJIS','UTF-8').','
                . mb_convert_encoding('支払日','SJIS','UTF-8').','
                . mb_convert_encoding('税別合計','SJIS','UTF-8').','
                . mb_convert_encoding('消費税','SJIS','UTF-8').','
                . mb_convert_encoding('税込合計','SJIS','UTF-8').',';

        $content .= "\r\n";
        foreach ($sales_slips as $sales_slip) {

            $is_ad_receive_payment =  $sales_slip['is_ad_receive_payment'];

            if($is_ad_receive_payment == 1){
                $ss_number = "AR".$sales_slip['sales_slip_number'];
            }
            else{
                $ss_number = "BN".$sales_slip['sales_slip_number'];
            }

            $sales_slip_status = $sales_slip['sales_slip_status'];
            $text_ss_status=  "";
            if($sales_slip_status == SALES_STATUS_INVOICE_APPROVAL_PENDING ){
                $text_ss_status = " 請求書発行承認申請中";
            }
            else if($sales_slip_status == CLIENT_STATUS_CL_APPROVED ){
                $text_ss_status = " 請求書発行承認済み";
            }
            else if($sales_slip_status == CLIENT_STATUS_CREDIT_EVERYTIME ){
                $text_ss_status = " 請求書発行済み";
            }
            else{
                $text_ss_status  = "請求書発行破棄";
            }
            $jcode = $sales_slip['account_name'];
            $s_code = $sales_slip['s_code'];
            $clientname =  $sales_slip['client_name'];
            $billing_date = $sales_slip['billing_date'];
            $payment_date  = $sales_slip['payment_date'];
            $total_amount = $sales_slip['total_amount'];

            $content .= mb_convert_encoding($ss_number,'SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding($text_ss_status,'SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding($jcode,'SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding($s_code,'SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding($clientname,'SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding($billing_date,'SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding($payment_date,'SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding($total_amount,'SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding($sales_slip['total_tax'],'SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding($sales_slip['total_amount_tax'],'SJIS','UTF-8') . ',';

            $content .= "\r\n";
        }

        $csv = $header . $content;

        $this->load->helper('download');

        $filename = filename_to_urlencode("請求書管理_" . date('Ymd') .'.csv');
        force_download($filename, $csv);


    }
//#7434:End
    public function show() {

        $this->load->model('business_unit_model');
        $this->load->model('account_model');


      //  $date = date('Y-m-d', strtotime('+1 month'));

        // One month from a specific date

        $this->load->model('sales_slip_model');
        $this->load->model('closing_accountant_model');
        $j_accounts = array();
        $default_j_account = $default_j_account_business_unit = null;

        $account_business_unit    = $this->auth->get_business_unit();
        $j_account_business_units = $account_business_unit;
        $is_product_division      = $account_business_unit[0]['is_product_division'];

        $s_account_business_units = $this->business_unit_model->get_product_business_unit_id_name();
        $s_accounts = $this->account_model->get_working_accounts('id, name');


        $is_self_invoices = $this->auth->has_permission('self_invoices', 'column');

        if (!$is_self_invoices) {
            $j_account_business_units = $s_account_business_units;
        }

        if (!$is_self_invoices && $is_product_division) {

            $j_accounts = $this->account_model->get_all_account_by_business_unit_resignation($account_business_unit[0]['id']);


            $default_j_account               = $this->auth->get_account_id();
            $default_j_account_business_unit = $account_business_unit[0]['id'];
        } else {
            $j_accounts = $s_accounts;
        }

        if ($is_self_invoices) {
            $auth_data = $this->auth->get_auth_data();
            $j_accounts = array(array(
                'id' => $auth_data['account_id'],
                'name' => $auth_data['userdata']['name'],
            ));
        }
        $error_messages = $sales_slips = array();
        $form_data = array();

//#6688:Start
        $is_submit_default = 0;
//#6688:End

        if ($is_self_invoices && $is_product_division)
            $form_data['j_account_id'] = $this->auth->get_account_id();

        if ($is_product_division)
            $form_data['j_account_business_unit_id'] = $default_j_account_business_unit;



        /**
         * Change to if have ajax then search
         * Logic old: Receive data from search form
         */

        if ($this->input->is_ajax_request()) {

            $form_data = $this->input->post();

            if ($form_data['s_account_business_unit_id'] && $form_data['s_account_business_unit_id'] != -1) {
                $s_accounts = $this->account_model->get_all_account_by_business_unit($form_data['s_account_business_unit_id']);
            }

            if (isset($form_data['j_account_business_unit_id']) && ($form_data['j_account_business_unit_id'] != -1) && !$is_self_invoices) {

                $j_accounts = $this->account_model->get_all_account_by_business_unit($form_data['j_account_business_unit_id']);

            }
            else{
                if($is_self_invoices && $is_product_division == 1){

//#6688:Start - variable not used
                   // echo 'id  chinh no';
                    //$account_login    = $this->auth->get_auth_data();
                    //$account_login_id = $account_login['account_id'];
//#6688:End
                    //$j_accounts = $account_login_id;
                    $form_data['j_account_id'] = $this->auth->get_account_id();

                }
            }
//             echo "<pre>";
//             print_r($form_data);
//             die();
            if(($form_data['s_account_business_unit_id'] != -1)&& ($form_data['s_account_id'] == -1)){


                $j_accounts_all = $this->account_model->get_all_account_by_business_unit($form_data['s_account_business_unit_id']);
                $form_data['s_account_id_all'] = $j_accounts_all;
            }

            $error_messages = $this->sales_slip_model->run_search_invoice_validate($form_data);

            if (count($error_messages) > 0) {
                echo $this->ajax_json(array('status' => 'error', 'mesg' => $error_messages));exit;
            } else {

                $sales_slips = $this->sales_slip_model->search_invoices($form_data);
                if (count($sales_slips) > OVER_RECORD) {
//#7087:Start
                    echo $this->ajax_json(array('status' =>'is_over_record',true));exit;
//#7087:End
                } else {
                    echo $this->ajax_json(array('status' => 'success', 'sales_slips' => $sales_slips));exit;
                }
            }

        } else {
            // Default search invoices when no errors message
//#6688:Start
            // if (empty($error_messages)) {
            // check is request from Order Management

//#7087:Start
            if (isset($_POST['is_link_from_order'])) {
                $is_submit_default = 1;
            }
            else{
                $is_submit_default = 0;
            }

            $this->assign('is_link_from_order', $is_submit_default);

//#7087:End
            // don't search term j_account_business_unit_id
            unset($form_data['j_account_business_unit_id']);
//#6688:End

            if (isset($_POST['s_code'])) {
                $form_data['s_code'] = $_POST['s_code'];
            }

//#6688:Start
            if (isset($_POST['delivery_date'])) {
                if (parse_date($_POST['delivery_date'])) {
                    $closedaydevilyday = date('Y/m', strtotime($_POST['delivery_date']));
                } else {
                    $closedaydevilyday = $_POST['delivery_date'];
                }

                $delivery_date_to = '';
                $form_data['delivery_date_from'] = $closedaydevilyday;
            }

            if (!$error_messages = $this->sales_slip_model->run_search_invoice_validate($form_data)) {
                $sales_slips = $this->sales_slip_model->search_invoices($form_data);
            } else {
                $this->assign('error_messages', $error_messages);
            }
//#6688:End


//#7087:Start
            if (count($sales_slips) > OVER_RECORD) {
                $sales_slips = array();
                $sales_slips['is_over_record'] = true;
            }
//#7087:End

            $this->assign('sales_slips', json_encode($sales_slips, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT));
//#6688:Start
            //}
//#6688:End

        }
        $approve_permission = $this->auth->has_permission('invoice_approve', 'column');
         //var_dump()
//#7087:Start

         if(!isset( $sales_slips['is_over_record'])){
                    foreach ($sales_slips  as &$sales_slip) {
                        $sales_slip['can_approve']  = $approve_permission;
                    }
         }

//#7087:End

        if($is_self_invoices){
            $this->assign('is_self_invoices', 1);
        }
        else{
            $this->assign('is_self_invoices', 0);
        }
        $this->assign('is_product_division', $is_product_division);
        $this->assign('j_account_business_units', $j_account_business_units);
        $this->assign('s_account_business_units', $s_account_business_units);
        $this->assign('j_accounts', $j_accounts);
        $this->assign('s_accounts', $s_accounts);
        $this->assign('default_j_account_business_unit', $default_j_account_business_unit);
        $this->assign('default_j_account', $default_j_account);
        $this->assign('error_messages', $error_messages);
        $this->assign('approve_permission', $approve_permission);

        $list_closing = $this->closing_accountant_model->get_closing_month_recent();

//#6688:Start
        if (!isset($closedaydevilyday)) {
//#6688:End
            if ($list_closing = $this->closing_accountant_model->get_closing_month_recent()) {

//#7088:Start
//
//             $close_date = date('Y-m-d', strtotime('+1 month', strtotime($list_closing[0]['close_date'])));
                $closedaydevilyday = $this->_get_closing_month(1, "/");

//#7088:End
            } else {
                $closedaydevilyday = date("Y-m-d");
            }
//#6688:Start
        }

        if (!isset($delivery_date_to)) {
            $delivery_date_to = Sales_slip_model::DELIVERY_DATE_END;
        }

        $this->assign('delivery_date_to', $delivery_date_to);
        $this->assign('is_submit_default', $is_submit_default);
//#6688:End

        $this->assign('closedaydevilyday',$closedaydevilyday);

        $this->view('invoice/show.tpl');
    }

    public function update_flag_invoice($sales_slip_ids,$static_invoice){

        $this->load->model('sales_slip_model');
        $this->load->model('order_model');
        $this->load->model('order_detail_model');

        $order_inputs = $this->sales_slip_model->get_sales_slip_detail_full($sales_slip_ids);
        $this->db->trans_start();
        $this->sales_slip_model->update_order_flag($order_inputs);
        $this->order_detail_model->update_status_by_flag($order_inputs);
        $this->db->trans_complete();
//#9787:Start
        return $this->db->trans_status();
//#9787:End

/*        $listsaleslipdetail = $this->sales_slip_model->get_sales_slip_detail_full($sales_slip_ids);
        $listdata = array();
        $order_inputs = array();
        if($static_invoice == SALES_STATUS_INVOICE_ISSUED || $static_invoice == SALES_STATUS_INVOICED_DISCARDED || $static_invoice == SALES_STATUS_CREATED){
                    if($sales_slip_ids){
                        foreach ($listsaleslipdetail as $rows){
                            $jcode = $rows['j_code'];
                            $branch_cd = $rows['branch_cd'];
                            $is_recive_payment = $rows['is_ad_receive_payment'];
                            $listoderdetail = $this->order_model->get_order_detai_by_jCode_and_BrandCode($jcode,$branch_cd);
                            if(count($listoderdetail)>0){
                                $order_input['j_code'] = $jcode;
                                $order_input['branch_cd'] = $branch_cd;

                                if($static_invoice == SALES_STATUS_INVOICE_ISSUED){

                                            // Get all saleslip detail have id_sale_slip = 0
//#8260:Start
//                                          $listsaleslipallstatushaveidsaleslipZEzo= $this->sales_slip_model->get_all_status_saleslip_bybrandcodejcode($jcode, $branch_cd,STATUS_DISABLE);
                                            $listsaleslipallstatushaveidsaleslipZEzo= $this->sales_slip_model->get_all_status_saleslip_bybrandcodejcode($jcode, $branch_cd,STATUS_DISABLE, 0, $is_recive_payment);
//#8260:End

                                            $n_l1 = $listsaleslipallstatushaveidsaleslipZEzo[0]['n_ss'];
                                            // Get all saleslip detail have id_sale_slip = 0 va status != issue

                                            $listsaleslipallstatusNOTISSSUE = $this->sales_slip_model->get_all_status_saleslip_bybrandcodejcode($jcode, $branch_cd,STATUS_ENABLE,STATUS_ENABLE);
                                            $n_l2 = $listsaleslipallstatushaveidsaleslipZEzo[0]['n_ss'];
                                            if($n_l1 != 0 || $n_l2!=0){
                                                $data['is_ad_invoice_issue'] = IS_NOT_AD_INVOICE_ISSUE;
                                                $data['is_invoice_issue'] = IS_NOT_INVOICE_ISSUE;
                                            }
                                            else{

//#7702:Start
                                                if(!$is_recive_payment){
                                                    // Get all saleslip detail have id_sale_slip = 23 (sales slip has been approved) and fixed amount
                                                    $listsaleslipallstatus_Invoiceissue = $this->sales_slip_model->get_all_status_saleslip_bybrandcodejcode($jcode, $branch_cd,STATUS_ENABLE,1,0);
                                                    // Get satus have Sale_slip_id = 0
                                                    $listsaleslipallstatus_ADInvoiceissue = $this->sales_slip_model->get_all_status_saleslip_hidden_have_underfine($jcode, $branch_cd);
                                                    if($listsaleslipallstatus_Invoiceissue[0]['n_ss'] != 0){
                                                        $data['is_invoice_issue'] = IS_NOT_INVOICE_ISSUE;
                                                    }
                                                    else {
                                                        if($listsaleslipallstatus_ADInvoiceissue[0]['n_ss'] != 0){
                                                            $data['is_invoice_issue'] = IS_NOT_INVOICE_ISSUE;
                                                        }
                                                        else{
                                                             $list_compare = $this->sales_slip_model->get_saslip_is_reivce_payment_dont_have_undefine($jcode, $branch_cd);
                                                                $ss_not_approved = 0;
                                                                if(count($list_compare) > 0 ){

                                                                    foreach($list_compare as $lp){
                                                                       if( $lp['is_ad_receive_payment'] == 1){
                                                                           $ss_not_approved = 1;
                                                                       }
                                                                    }
                                                                }
                                                                if($ss_not_approved == 1){
                                                                    $data['is_invoice_issue'] = IS_NOT_INVOICE_ISSUE;
                                                                }
                                                                else{
                                                                $data['is_invoice_issue'] = IS_INVOICE_ISSUE;
                                                                }
                                                        }
                                                    }
                                                }
                                                else{
                                                    // Get all saleslip detail have id_sale_slip = 23 (sales slip has been approved) and is receive payment
                                                    $listsaleslipallstatus_ADInvoiceissue = $this->sales_slip_model->get_all_status_saleslip_bybrandcodejcode($jcode, $branch_cd,STATUS_ENABLE,1,1);
                                                    if($listsaleslipallstatus_ADInvoiceissue[0]['n_ss'] != 0){

                                                        $data['is_ad_invoice_issue'] = IS_NOT_AD_INVOICE_ISSUE;
                                                    }
                                                    else{
                                                        $data['is_ad_invoice_issue'] = IS_AD_INVOICE_ISSUE;
                                                    }
                                                }
//#7702:End
                                            }

                                }
                                if($static_invoice == SALES_STATUS_INVOICED_DISCARDED){
                                    if($is_recive_payment == IS_AD_RECEIVE_PAYMENT){
                                        $data['is_ad_invoice_issue'] = IS_NOT_AD_INVOICE_ISSUE;
                                        $data['is_ad_sales_slip_input'] = IS_NOT_AD_SALES_SLIP_INPUT;
                                    }
                                    if($is_recive_payment == IS_NOT_AD_RECEIVE_PAYMENT ){
                                        $data['is_invoice_issue'] =  IS_NOT_INVOICE_ISSUE;
                                        $data['is_sales_slip_input'] =  IS_NOT_SALES_SLIP_INPUT;
                                    }
                                }
                                if($static_invoice == SALES_STATUS_CREATED){
                                    if($is_recive_payment == IS_AD_RECEIVE_PAYMENT){
                                        $data['is_ad_invoice_issue']    = IS_NOT_AD_INVOICE_ISSUE;
                                        $data['is_ad_sales_slip_input'] = IS_NOT_AD_SALES_SLIP_INPUT;
                                    }
                                    if($is_recive_payment == IS_NOT_AD_RECEIVE_PAYMENT ){
                                        $data['is_invoice_issue']    =  IS_NOT_INVOICE_ISSUE;
                                        $data['is_sales_slip_input'] =  IS_NOT_SALES_SLIP_INPUT;

                                    }
                                }
                                if(isset($listoderdetail[0]->id)){
                                    $data ['id'] = $listoderdetail[0]->id;
                                }
                                else{
                                    $listoderdetail[0]->id = -1;
                                }

                                $listdata[]=$data;
                                $order_inputs[] = $order_input;
                            }
                        }
                        if(count($listdata) > 0 ){
                                $this->order_detail_model->update_batch('order_detail', $listdata,'id');
                                 $this->order_detail_model->update_status_by_flag($order_inputs);
                        }

                }
        } */

    }

    public function approve_and_print() {

        if ($this->input->method() != 'post') {
            show_404();
        }
        //  $is_download = false;
        // #6818-S
        ini_set("memory_limit","1024M");
        ini_set('max_execution_time', 300);
        // #6818-E
        $this->load->model('client_model');
        $this->load->model('sales_slip_model');
        $this->load->model('bank_account_model');
        $this->load->library('tcpdf/Tcpdf.php');
        $this->load->library('tcpdf/fpdi.php');
        $this->load->helper('zenrin_pdf_helper');
        $this->load->helper('zenrin_system_helper');

        $is_print2 = 0;
        $arr_sales_slip_ids = explode(',', $this->input->post('sales_slip_ids'));

        $is_approve_and_print_all = 0;
        if ($this->input->post('approve_and_print_all')) {
            $is_approve_and_print_all = $this->input->post('approve_and_print_all');
        }

        $is_download = $this->input->post('is_download') ? true : false;

        $closing_month = strtotime($this->_get_closing_month(0));

         foreach ($arr_sales_slip_ids as $sales_slip_ids) {

            $invoices = $this->sales_slip_model->get_data_for_print($sales_slip_ids);
            $isUpdate = 0;

            if(count($invoices)>0){

                $sales_slip_status = $invoices[0]['sales_slip_status'];

                    if (!$is_download) { // print

                        $account_login = $this->auth->get_auth_data();
                        $account_login_id = $account_login['account_id'];

                        if ($is_approve_and_print_all == 1) {

                            if (($sales_slip_status == SALES_STATUS_INVOICE_APPROVAL_PENDING || $sales_slip_status == SALES_STATUS_INVOICE_APPROVED) && strtotime($invoices[0]['delivery_date']) > $closing_month) {
                                $isUpdate = 1;
                                $this->sales_slip_model->update_invoice_status($sales_slip_ids, SALES_STATUS_INVOICE_ISSUED, "", SALES_STATUS_INVOICED_MODIFY, $account_login_id);
                                $this->update_flag_invoice($sales_slip_ids, SALES_STATUS_INVOICE_ISSUED);
                            }
                        } else {

                            if ($sales_slip_status == SALES_STATUS_INVOICE_APPROVED && strtotime($invoices[0]['delivery_date']) > $closing_month) {
                                $isUpdate = 1;
                                $this->sales_slip_model->update_invoice_status($sales_slip_ids, SALES_STATUS_INVOICE_ISSUED, "", SALES_STATUS_INVOICED_MODIFY, $account_login_id);
                                $this->update_flag_invoice($sales_slip_ids, SALES_STATUS_INVOICE_ISSUED);
                            }
                        }
                    }
              }
        }
        foreach ($arr_sales_slip_ids as $sales_slip_ids) {

            if ($sales_slip_ids <> "") {

                $sales_slip_ids = explode(',', $this->input->post('sales_slip_ids'));

                $invoices = $this->sales_slip_model->get_data_for_print($sales_slip_ids);

                $bank = $this->bank_account_model->get_bank_account();

                if (($this->input->post('is_print2'))) {
                    $is_print2 = $this->input->post('is_print2');
                }

                if (empty($invoices))
                    show_404();

                $fpdi = new FPDI();

                $fpdi->setPrintHeader(false);

                $fpdi->setPrintFooter(false);

                $fpdi->SetAutoPageBreak(TRUE, 0);

                $regularFont = 'msgothici';
//#7616:Start
                $regularFont2 = 'ipam';

                // Use for convert a ttf/otf font
                // $fontPathRegular = APPPATH . 'libraries/tcpdf/fonts/minchosubst.ttf' ;
                // $regularFont2     = TCPDF_FONTS::addTTFfont($fontPathRegular, '', '', 32);
//#7616:End

                $fpdi->SetFont($regularFont, '', 9);

                foreach ($invoices as &$invoice) {

//#7616:Start

                   $invoice['details'] = sales_slip_export_pdf($invoice['id']);
//#8273:Start
                   $invoice = remove_non_printable_character($invoice);
//#8273:End
                   $is_ad_re_payment  = $invoice['is_ad_receive_payment'];
                   $date_delivey_date_invoice = "";

                   if($is_ad_re_payment == 1){
                        $date_delivey_date_invoice =  date("Y年m月d日 ");
                   }
                   else{
//                    		print_r($invoice['details']);

                        if( count($invoice['details']) > 0){
                            $date_delivey_date_invoice = $invoice['details'][0]['delivery_date'];
                            foreach ($invoice['details'] as $d ){
                                 $devrydate = $d['delivery_date'];
                                 if(strtotime($date_delivey_date_invoice) < strtotime($devrydate)){
                                     $date_delivey_date_invoice = $devrydate;
                                 }
                            }
                        }

                        $dsdate_delivey_date_invoice = explode("-", $date_delivey_date_invoice);
                        if(count($dsdate_delivey_date_invoice) > 0){
                            $date_delivey_date_invoice = $dsdate_delivey_date_invoice[0]."年".$dsdate_delivey_date_invoice[1]."月".$dsdate_delivey_date_invoice[2]."日 ";
                        }



                   }

//#7616:End

                    $clientnamess = $invoice['client_name'];

                    if(!empty($clientnamess)){
                            $id_client =  $invoice['client_id'];
                            if(!empty($id_client)){
                                    $listclient = $this->client_model->get_client_by_id($id_client);
                                    if(count($listclient) > 0){
                                        $typeclientname = $this->client_model->name_with_title($listclient['name'],$listclient['corporate_status_before'],$listclient['corporate_status_after']);
                                        $clientnamess = str_replace($listclient['name'], $clientnamess, $typeclientname);
                                    }
                            }
                   }
                    $page_num = 1;
                    $page_estimator = page_estimator(count($invoice['details']), 23, 24, 33, 34);

                    // Initialize
                    $header_total_row = 23;
                    $detail_total_row = 34;
                    $footer_total_row = 33;

                    // Add pages
                    $fpdi->AddPage('P', 'A4');
                    $fpdi->setSourceFile(APPPATH . 'config/pdf_template/invoice/bk/header_footer.pdf');
                    if (count($invoice['details']) > 23) {
                        $header_total_row = 24;
                        $fpdi->setSourceFile(APPPATH . 'config/pdf_template/invoice/bk/header.pdf');
                    }
                    $tplIdx = $fpdi->importPage(1);
                    $fpdi->useTemplate($tplIdx, null, null, null, null, true);

                    if (count($invoice['details']) > 23) {
                        $fpdi->SetXY(0, 315);
//#8130:Start
//						$fpdi->Cell(228, 5, $page_num . '/' . $page_estimator['total'] . ' ページ', 0, false, 'C');
                        $fpdi->Cell(228, 5, $page_num . '/' . $page_estimator['total'] . ' ページ', 0, false, 'C',false,false,true);
//#8130:End
                    }

                    // stamp + title
                    $title = '請求書';
                    if ($sales_slip_status == SALES_STATUS_INVOICED_DISCARDED) {
                        $title .= "（控え）";
                    } elseif ($sales_slip_status == SALES_STATUS_INVOICE_APPROVAL_PENDING) {
                        if (!$is_approve_and_print_all) {
                            $title .= "（ドラフト）";
                        }
                    } elseif ($sales_slip_status != SALES_STATUS_INVOICE_APPROVAL_PENDING && !$this->auth->has_permission('invoice_approve', 'column') && $isUpdate == 0) {
                        $title .= '（再発行）';
                    }

                    if ($sales_slip_status != SALES_STATUS_INVOICE_APPROVAL_PENDING && $is_download) {
                        $title = "請求書（控え）";
                    }
//#7616:start
                    // remove trash
//                     $fpdi->SetXY(137.5, 28);
//                     $fpdi->SetFillColor(255, 255, 255);
//                     $fpdi->cell(60, 1, '', 0, '', 'L', true);
//#7616:End
                    if ($invoice['sales_slip_status'] != SALES_STATUS_INVOICE_APPROVAL_PENDING) {
                        if (($sales_slip_status != SALES_STATUS_INVOICED_DISCARDED)) {
//#7616:start
//  							$fpdi->Image('/public/admin/invoice/stamp_new.png', 194, 27.8, 25, 25, '', '', '', false, 300, '', false, false);
                            $fpdi->Image('/public/admin/invoice/stamp_new.png', 191, 39.5, 21.6, 21.6, '', '', '', false, 300, '', false, false);
//#7616:end
                        }
                    }


                    $fpdi->SetFont($regularFont, 'B', 18);
                    $fpdi->SetTextColor(0, 51, 102);
                    $fpdi->SetXY(0, 0);
                    $fpdi->cell(228, 0, $title, 0, 0, 'C');
                    // client zipcode
                    $fpdi->SetFont($regularFont, '', 9);
                    $fpdi->SetTextColor(0, 0, 0);
                    $fpdi->SetXY(20, 6);
                    $fpdi->SetFillColor(255, 255, 255);
                    $fpdi->cell(45, 0, '', 0, '', 'L', true);
//#7616:Start
                    $fpdi->SetFont($regularFont, '', 12);

                    if($date_delivey_date_invoice <> ""){
                        $fpdi->SetXY(110,24.5);
                        $fpdi->cell(0, 5, "発行日".$date_delivey_date_invoice, 0, 0, 'C');
                    }
                    else{
                        $fpdi->SetXY(83,24.5);
                        $fpdi->cell(0, 5, "発行日".$date_delivey_date_invoice, 0, 0, 'C');
                    }
//#7616:End
//#7616:Start

                    $fpdi->SetFont($regularFont2, '', 11);
//#7616:End
                    $fpdi->SetXY(22,4);
                    $zipcode = substr_replace($invoice['zipcode'], '-', 3, 0);
                    $fpdi->Write(0, '〒 ' . $zipcode);
                    // client address 1
                    $fpdi->SetFont($regularFont2, '', 10);
                    $fpdi->SetXY(23, 8.5);
                    $fpdi->Write(0, $invoice['address_1st']);
                    // client address 2
                    $fpdi->SetXY(23, 14);
                    $fpdi->Write(0, $invoice['address_2nd']);

                    // company name
//#7616:Start
//#7616:End
                    $fpdi->SetFont($regularFont2, 'B', 12);
                    $fpdi->SetXY(23,19);
                    $fpdi->Write(0,$clientnamess);
                    //----

                    $fpdi->SetXY(23,25);
                    $fpdi->SetFont($regularFont2, 'B', 10);
                    $namdivi="";
                    if(!empty($invoice['division_name'])){$namdivi = $invoice['division_name']; }

                    $fpdi->cell(100,0,''.$namdivi ,0, "", 'L', true);
                    //
                    //$fpdi->SetXY(24, 35.8);
                    //  $fpdi->Write(0,);
//#7616:Start
                    $fpdi->SetFont($regularFont2, 'B', 14);
//#7616:End
                    $fpdi->SetXY(23,30);
                    // $fpdi->SetFillColor(255,255,255);
                    $fpdi->cell(100,20,"" ,0, "", 'L', true);
                    $fpdi->SetXY(37,30.3);
                    $fpdi->SetFillColor(255,255,255);
                    $charge_name ="";
                    if(!empty($invoice['charge_name'])){$charge_name = $invoice['charge_name'].'様'; }
                    $fpdi->cell(100,0,$charge_name ,0, "", 'L', true);

                    $fpdi->SetFont($regularFont2, 'B', 9);
                    // client tel
                    $fpdi->SetXY(23, 35.7);
                    $fpdi->Write(0, 'TEL:'.$invoice['sales_slip_phone_no']);
                    // client fax
                    $fpdi->SetXY(60, 35.7);
                    $fpdi->Write(0, 'FAX:'.$invoice['sales_slip_fax_no']);
                    //#7616:Start
                    $fpdi->SetFont($regularFont, '', 9);
                    //#7616:End
                    $fpdi->SetXY(23, 57.5);
                    $fpdi->SetFillColor(255, 255, 255);
                    $fpdi->cell(100, 0, '毎度格別のお引き立てを賜り、誠にありがとうございます。 ',0, '', 'L', true);


                    $fpdi->SetXY(23, 61.5);
                    $fpdi->cell(100, 0, '下記の通り、ご請求申し上げます。',0, "", 'L', true);
                    //s_code



                    $fpdi->SetXY(23, 68.3);
                    $fpdi->SetFillColor(204, 255, 204);
                    $fpdi->cell(34, 1, 'お客様コード ',     1, '', 'C', true);

                    $fpdi->SetXY(57, 68.3);
                    $fpdi->SetFillColor(255, 255, 255);
                    $fpdi->cell(23, 1,$invoice['s_code'],1, '', 'C', true);
                    //$fpdi->cell(20, 4.5, $invoice['s_code'], 0, 0, 'C');
                    // sales slip number
                    $fpdi->SetXY(23, 72.5);
                    $fpdi->SetFillColor(204, 255, 204);
                    $fpdi->cell(35, 1, '請求書番号',     1, '', 'C', true);

                    $sales_slip_number = $invoice['is_ad_receive_payment'] ? 'AR' : 'BN';
                    $sales_slip_number .= $invoice['sales_slip_number'];

                    $fpdi->SetXY(57, 72.5);
                    $fpdi->SetFillColor(255, 255, 255);
                    $fpdi->cell(23, 1,$sales_slip_number,1, '', 'C', true);
                    /*
                    $fpdi->SetXY(51, 64);
                    $fpdi->cell(20, 4.5, $sales_slip_number, 1, 0, 'C');
                    */
                    // tax total
                    $fpdi->SetXY(60, 80.7);
                    $fpdi->SetFontSize(14);
                    if ($invoice['total_amount_tax'] < 0) {
                        $fpdi->SetTextColor(255, 0, 0);
                    }
//#8130:Start
//					$fpdi->cell(38, 8, number_format($invoice['total_amount_tax']), 0, 0, 'R');
                    $fpdi->cell(38, 8, number_format($invoice['total_amount_tax']), 0, 0, 'R',false,false,true);
//#8130:End

                    // zenrin bizu
                    $fpdi->SetFontSize(11);
                    $fpdi->SetTextColor(0, 0, 0);
                    $fpdi->SetXY(141,35);
                    $fpdi->SetFillColor(255, 255, 255);
                    $fpdi->cell(40, 1.3, '株式会社ゼンリンビズネクサス', 0, '', 'L', true);
                    // zenrin zipcode
                    $fpdi->SetFontSize(9);
                    $fpdi->SetXY(141, 40.2);
                    $fpdi->SetFillColor(255, 255, 255);
//#9250:Start
//                     $fpdi->cell(15, 1, '〒 ' . $invoice['department_zipcode'], 0, '', 'L', true);
                    $fpdi->cell(15, 1, '〒 ' . substr_replace($invoice['department_zipcode'], '-', 3, 0), 0, '', 'L', true);
//#9250:End
                    // zenrin address 1
                    $fpdi->SetXY(141, 44.8);
                    $fpdi->SetFillColor(255, 255, 255);
                    $fpdi->cell(45, 0, $invoice['department_address_1'], 0, '', 'L', true);
                    // zenrin address 1
                    $fpdi->SetXY(141,48.5);
                    $fpdi->SetFillColor(255, 255, 255);
                    $fpdi->SetFontSize(9);
                    $fpdi->cell(45, 0, $invoice['department_address_2'], 0, '', 'L', true);
                    // zenrin tel
                    $fpdi->SetXY(141.5, 52.5);
                    $fpdi->SetFillColor(255, 255, 255);
                    $fpdi->cell(45, 1, 'TEL ' . $invoice['department_phone_no'], 0, '', 'L', true);
                    // zenrin account
                    $fpdi->SetXY(141.5,56.5);
                    $fpdi->cell(45, 1, '担当者名 ' . $invoice['account_name'], 0, '', 'L', true);
                    // zenrin bank financial institution name
                    // $fpdi->SetXY(170.5, 59.5);
                    // $fpdi->cell(15, 1, $bank['financial_institution_name'], 1);
                    $fpdi->SetFillColor(255, 255, 255);
                    $fpdi->MultiCell(45, 4, $bank['financial_institution_name'], 0, 'L', false, 1, 164.6, 68.3, true, 0, false, '', 0, 'M', true);
                    // zenrin bank branch name
                    // $fpdi->SetXY(170.5, 63.5);
                    // $fpdi->cell(15, 1, $bank['branch_name']);
                    $fpdi->MultiCell(45, 4, $bank['branch_name'], 0, 'L', false, 1, 164.6,72.2, true, 0, false, '', 0, 'M', true);
                    // zenrin bank account number
                    // $fpdi->SetXY(170.5, 67.5);
                    // $fpdi->cell(15, 1, $bank['account_number']);
                    $fpdi->MultiCell(45, 4, $bank['account_number'], 0, 'L', false, 1, 164.6, 76.5, true, 0, false, '', 0, 'M', true);
                    // zenrin bank account holder
                    // $fpdi->SetXY(170.5, 71.5);
                    // $fpdi->cell(15, 1, $bank['account_holder']);
                    $fpdi->MultiCell(45, 4, $bank['account_holder'], 0, 'L', false, 1, 164.6, 80.1, true, 0, false, '', 0, 'M', true);
                    // zenrin bank account holder
                    $fpdi->SetXY(164.6, 84.2);
                    $fpdi->cell(15, 1, str_replace('-', '/', $invoice['billing_date']));
                    // zenrin bank account holder
                    $fpdi->SetXY(164.6, 88.2);
                    $fpdi->cell(15, 1, str_replace('-', '/', $invoice['payment_date']));
//#7616:start
                    // remove trash
//                     $fpdi->SetXY(0, 102);
//                     $fpdi->SetFillColor(255, 255, 255);
//                     $fpdi->cell(20, 0, '', 0, '', 'L', true);
                    // remove trash
//                     $fpdi->SetXY(0, 210);
//                     $fpdi->SetFillColor(255, 255, 255);
//                     $fpdi->cell(20, 0, '', 0, '', 'L', true);
//#7616:End
                    /*                     * *************** DETAIL ***************** */


                    $y = 8.6;
                    $row_uint_price = 0;
                    foreach ($invoice['details'] as $row => $detail) {
                        if ($detail['unit_price'] == 0) {
                            $row_uint_price ++;
                        }
                    }

                    $total = count($invoice['details']) - 1 -$row_uint_price;

                    $footer_flag = false;
                    $count = -1;
                    $fpdi->SetFont($regularFont, '', 9);
                    foreach ($invoice['details'] as $row => $detail) {
                        if ($detail['unit_price'] != 0) {
//7389:Start
                        /*
                        if ($detail['amount'] == 0)
                            continue;
                        */
//7389:End
                        $count++;

                        if (($row == $header_total_row || ($count - 1) == $detail_total_row) && ( $total - $row >= $footer_total_row)) {
                            $y = -84;
                            $fpdi->AddPage('P', 'A4');
                            $fpdi->setSourceFile(APPPATH . 'config/pdf_template/invoice/bk/detail.pdf');
                            $detailIdx = $fpdi->importPage(1);
                            $fpdi->useTemplate($detailIdx, null, null, null, null, true);

                            $page_num++;
                            $fpdi->SetXY(0, 315);
                            $fpdi->Cell(228, 5, $page_num . '/' . $page_estimator['total'] . ' ページ', 0, false, 'C');
                            $count = 0;
                        }

                        if (($row == $header_total_row || ($count - 1) == $detail_total_row) && $total - $row <= $detail_total_row) {
                            $y = -84;
                            $fpdi->AddPage('P', 'A4');
                            $fpdi->setSourceFile(APPPATH . 'config/pdf_template/invoice/bk/footer.pdf');
                            $detailIdx = $fpdi->importPage(1);
                            $fpdi->useTemplate($detailIdx, null, null, null, null, true);

                            $page_num++;
                            $fpdi->SetXY(0, 315);
                            $fpdi->Cell(228, 5, $page_num . '/' . $page_estimator['total'] . ' ページ', 0, false, 'C');
                            $count = 0;
                            $footer_flag = true;
                        }

                        // j_code branch_cd
                        $jcode_branch = $detail['j_code'] ."-".$detail['branch_cd'];
                        // echo $jcode_branch;die;
                        //$fpdi->SetXY(20.5, 100.4 + $y);


                        $fpdi->SetXY(25.5, 100.4 + $y);

//#7837:Start
                        if ($detail['unit_price'] == 0) {
                            $fpdi->SetTextColor(255, 255, 255);
                        }
//#7837:End

                        $fpdi->cell(25.2, 7.7, $jcode_branch, 0, 0, 'C');

                        // delivery_date
                        //$fpdi->SetXY(50.8, 100.4 + $y);
                        $fpdi->SetXY(53.5, 100.4 + $y);
                        $fpdi->cell(18.2, 7.7, str_replace('-', '/', $detail['delivery_date']), 0, 0, 'C');
                        // product name
                        //$fpdi->SetXY(70.8, 100.4 + $y);
                        $fpdi->SetXY(72, 100.4 + $y);
                        $fpdi->SetFont($regularFont, '', 6);
                        $strlenname = strlen($detail['product_name']);
//8130:Start
//                        $fpdi->cell(72, 3.74, $detail['product_name']);
                        $fpdi->cell(69, 3.74, $detail['product_name'],0,0,'L',false,false,true);

//8130:End
                        $fpdi->SetFont($regularFont, '', 9);
                        // product content
                        $stretch = 0;
                        $c = $detail['contents'];
                        // If client name is too long => Auto manage to fit with the cell
                        if(mb_strlen($c, 'UTF-8') > 22) {
                            $stretch = 1;
                            // If client name is more than 40 chars, cut the rest
                            if(mb_strlen($c, 'UTF-8') > 30) {
                                $c = mb_substr($c, 0, 30, 'UTF-8');
                            }
                        }
                        //$fpdi->SetXY(70.8, 104.2 + $y);
                        $fpdi->SetXY(72, 104.71 + $y);
//8130:Start
//                        $fpdi->cell(69, 3.74, $c,'','','','','',$stretch,false,false,true);
                        $fpdi->cell(69,3.74, $c,'','','',false,false,true);
//8130:End
                        // product tax type
                        //$fpdi->SetXY(142, 100.4 + $y);
                        $fpdi->SetXY(141.2, 100.4 + $y);
                        if (isset($detail['is_show_minus'])) {
                            $fpdi->SetTextColor(255, 0, 0);
                        }
                        if ($detail['tax_type'] == TAX_EXCLUSIVE)
                            $tax_name = '外';
                        elseif ($detail['tax_type'] == TAX_INCLUDED)
                            $tax_name = '内';
                        else
                            $tax_name = '非';
                        $fpdi->cell(5, 7.7, $tax_name, 0, 0, 'C');
                        // product quantity
                        //$fpdi->SetXY(147, 100.4 + $y);
                        $fpdi->SetXY(146.5, 100.4 + $y);
                        $quantity = isset($detail['is_show_minus']) ? (0 - $detail['quantity']) : $detail['quantity'];

                        if (isset($detail['is_show_minus']) || $detail['quantity'] < 0) {
                            $fpdi->SetTextColor(255, 0, 0);
                        }
                        /*
                         * 6480: Start
                        //$fpdi->cell(20.2, 7.7, $quantity, 0, 0, 'C');
                         * 6480: End
                        */

                        //6480: Start
                        $fpdi->cell(20.2, 7.7, number_format($quantity), 0, 0, 'C');
                        //6480: End

                        // product unit price
                        $fpdi->SetTextColor(0, 0, 0);
                        //6480: Start
                        //$fpdi->SetXY(167.2, 100.4 + $y);
//#8130:Start
                        $fpdi->SetXY(166.2, 100.6 + $y);

//#8130:End
                        //6480: End
                        if (isset($detail['is_show_minus']) || $detail['unit_price'] < 0) {
                            $fpdi->SetTextColor(255, 0, 0);
                        }

// #7837:Start
                        if ($detail['unit_price'] == 0) {
                            $fpdi->SetTextColor(255, 255, 255);
                        }
// #7837:End

//#8130:Start
//						$fpdi->cell(20.2, 7.7, number_format($detail['unit_price'], 2), 0, 0, 'R');
                        $fpdi->cell(19, 7.5, number_format($detail['unit_price'], 2), 0, 0, 'R',false,false,true);

//#8130:End

                     // product amount
                        $fpdi->SetTextColor(0, 0, 0);
                        //6480: Start
                        //$fpdi->SetXY(187.5, 100.4 + $y);
                        $fpdi->SetXY(186, 100.6 + $y);
                        //6480: End
                        $amount = isset($detail['is_show_minus']) ? (0 - $detail['amount']) : $detail['amount'];
                        if (isset($detail['is_show_minus']) || $detail['amount'] < 0) {
                            $fpdi->SetTextColor(255, 0, 0);
                        }
//#7837:Start
                        if ($detail['unit_price'] == 0) {
                            $fpdi->SetTextColor(255, 255, 255);
                        }
//#7837:End
//#8130:Start
// 						$fpdi->cell(25.2, 7.7, number_format($amount), 0, 0, 'R');
                        $fpdi->cell(23.5, 7.5, number_format($amount), 0, 0, 'R',false,false,true);
//#8130:End
                        $fpdi->SetTextColor(0, 0, 0);

                        $y += 8;
                        }
                    }

                    $footer_amount_y = 0;
                    if (!$footer_flag && count($invoice['details']) > 23) {
                        $fpdi->AddPage('P', 'A4');
                        $fpdi->setSourceFile(APPPATH . 'config/pdf_template/invoice/bk/footer.pdf');
                        $detailIdx = $fpdi->importPage(1);
                        $fpdi->useTemplate($detailIdx, null, null, null, null, true);

                        $page_num++;
                        $fpdi->SetXY(0, 315);
                        $fpdi->Cell(228, 5, $page_num . '/' . $page_estimator['total'] . ' ページ', 0, false, 'C');
                        $footer_amount_y = 0;
                    }

                    if (count($invoice['details']) <= 23) $footer_amount_y = 4;

                    // cost total price
                    //$fpdi->SetXY(187.5, 280.5 + $footer_amount_y);
                    $fpdi->SetFont($regularFont, '', 11);
                     $fpdi->SetXY(185, 288.8 + $footer_amount_y);
                    // if ($invoice['total_amount'] < 0) {
                    //     $fpdi->SetXY(25.2, 4.1 + $footer_amount_y);
                    //     $fpdi->SetTextColor(255, 0, 0);
                    // }
                    if ($invoice['total_amount'] < 0) {
                        $fpdi->SetTextColor(255, 0, 0);
                    }


//#8130:Start
//					 $fpdi->cell(27.2, 4.1, number_format($invoice['total_amount']), 0, 0, 'C');
                    $fpdi->cell(25, 4.1, number_format($invoice['total_amount']), 0, 0, 'C',false,false,true);
//#8130:End

                    $fpdi->SetTextColor(0, 0, 0);

                    // total_tax
                     $fpdi->SetXY(185, 293.2 + $footer_amount_y);
                    if ($invoice['total_tax'] < 0) {
                        $fpdi->SetTextColor(255, 0, 0);
                    }
//#8130:Start


// 					$fpdi->cell(27.2, 4.1, number_format($invoice['total_tax']), 0, 0, 'C');
                    $fpdi->cell(25, 1.0, number_format($invoice['total_tax']), 0, 0, 'C',false,false,true);
//#8130:End
                    $fpdi->SetTextColor(0, 0, 0);

                    // total_amount_tax
//#8130:Start
                    $fpdi->SetXY(185, 297.1 + $footer_amount_y);
//#8130:End
                    if ($invoice['total_amount_tax'] < 0) {
                        $fpdi->SetTextColor(255, 0, 0);
                    }

//#8130:Start
//					$fpdi->cell(27.2, 4.1, number_format($invoice['total_amount_tax']), 0, 0, 'C');
                    $fpdi->cell(25, 4.1, number_format($invoice['total_amount_tax']), 0, 0, 'C',false,false,true);
//#8130:End
                    $fpdi->SetTextColor(0, 0, 0);

                    //remark
                    $fpdi->SetFont($regularFont, '', 8);
                    $fpdi->SetXY(22.5, 289.8+ $footer_amount_y + 0.3);
                    $fpdi->SetFillColor(255,255,255);
                    $fpdi->cell(120, 7, "なお、本請求書には内消費税額も含め、" . number_format($invoice['total_tax']) . "円の消費税が含まれております。", 0, "", 'L', true);

                    $fpdi->SetXY(22.5, 294.8 + $footer_amount_y + 0.3);
                    $fpdi->SetFillColor(255,255,255);
                    $fpdi->cell(122, 7, "ご請求額 " . number_format($invoice['total_amount_tax']) . "円を上記口座にお振込下さるようお願い申し上げます。", 0, "", 'L', true);
                    // consumption tax
                }

                if ($is_download) {
                    if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE || strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/') !== FALSE || strpos($_SERVER['HTTP_USER_AGENT'], 'Edge/') !== FALSE)) {
                        $file_name = urlencode('[請求書]' . $invoices[0]['client_name'] . '様' . $sales_slip_number . '.pdf');
                    } else {
                        $file_name = '[請求書]' . $invoices[0]['client_name'] . '様' . $sales_slip_number . '.pdf';
                    }

                    $fpdi->Output($file_name, 'D');
                    //  exit;
                }
                $fpdi->Output();
            }

        }
    }
    public function ajax_detail() {
       if ($this->input->method() != 'post' || !$this->input->post('sales_slip_id')) show_404();

        $this->load->model('sales_slip_model');
        $this->load->model('department_model');
        $this->load->helper('zenrin_system_helper');
        $sales_slip_id = $this->input->post('sales_slip_id');

        $sales_slip = $this->sales_slip_model->get_by_sales_slip_id($sales_slip_id);

        if (!$sales_slip) {
            $this->ajax_json(array(
                'status'   => false,
                'messages' => lang('error_invalid_input'),
            ));
            die;
        }

        // 6846: Start
        $closing_month = $this->_get_closing_month();
        $is_settlement = false;

        if (strtotime($sales_slip['delivery_date']) < strtotime($closing_month)) {
            $is_settlement = true;
        }

        $this->assign('is_settlement', $is_settlement);
        // 6846: End

        $sales_slip['details'] = sales_slip_export_pdf($sales_slip_id);

        $account_business_unit =  $this->auth->get_business_unit();

        $account_business_unit_login = $account_business_unit[0]['id'];
        $business_unit_id = -1;
        if(isset($sales_slip['details'][0])){
                $j_account_id = $sales_slip['details'][0]['j_account_id'];
                $dsaccountid   =  $this->department_model->get_department_by_account_id($j_account_id);
                $business_unit_id = $dsaccountid['business_unit_id'];
        }
        $isdiscardIinvoice = 0;
        if($account_business_unit_login == $business_unit_id){
                $isdiscardIinvoice = 1;
        }
        $this->assign('isdiscardIinvoice', $isdiscardIinvoice);
        $this->assign('sales_slip', $sales_slip);
        $this->view('invoice/detail.tpl');
        die;
    }

    public function ajax_discard_invoice_sale() {

        if ($this->input->method() != 'post') show_404();
        $sales_slip_id = $this->input->post('sales_slip_id');

        if (!$sales_slip_id || !$this->auth->has_permission('invoice/ajax_discard_invoice_sale')) {
            $this->ajax_json(array(
                'status'   => false,
                'messages' => !$sales_slip_id ? lang('error_invalid_input') : lang('common_msg_access_deny'),
            ));
            die;
        }

        $this->load->model('sales_slip_model');



        $this->load->model('sales_slip_detail_model');
        $sales_slip = $this->sales_slip_model->get_by_sales_slip_id($sales_slip_id);

 //#8064:Start
        if (!$sales_slip && $sales_slip['sales_slip_status'] != SALES_STATUS_INVOICE_ISSUED &&  $sales_slip['sales_slip_status'] != SALES_STATUS_INVOICE_APPROVED ) {
 //#8064:End
            $this->ajax_json(array(
                'status'   => false,
                'messages' => lang('error_invalid_input'),
            ));
            die;
        }

//#6930:Start


        $sales_slip_is_receive_payment = $sales_slip['is_ad_receive_payment'];


        if($sales_slip_is_receive_payment == 1){

                $sales_slip_number_is_checked = $sales_slip['sales_slip_number'];

                $sales_slip_not_receive_payment = $this->sales_slip_model->get_sales_slip_by_sales_slip_number_and_is_receive_payment($sales_slip_number_is_checked,0);

                $n_sales_slip_not_receive_payment = count($sales_slip_not_receive_payment);

                if($n_sales_slip_not_receive_payment > 0 ){
                    $this->ajax_json(array(
                            'status' => false,
                            'messages' => lang('error_check_sales_lip_not_receive_payment')
                    ));

                    die();
                }


        }

//#6930:End

        // 6846: Start
        $closing_month = $this->_get_closing_month();

        if (strtotime($sales_slip['delivery_date']) < strtotime($closing_month)) {
            $this->ajax_json(array(
                'status' => false,
                'messages' => lang('error_is_settlement_discard')
            ));
        }
        // 6846: End
//#6949:Start
        $this->db->trans_start();
//#6949:End
        //-------New-------
        $sales_slip_details = $this->sales_slip_model->getallSaleSlipDetailBySalesSlipId($sales_slip_id);
//#6949:Start
        $update_ssd_params = array();
//#6949:End
        foreach ($sales_slip_details as $sales_slip_detail){
            $datainsert = array(
                    'sales_slip_id' => 0,
                    'j_code'=> $sales_slip_detail['j_code'],
                    'branch_cd'=> $sales_slip_detail['branch_cd'],
                    'is_ad_receive_payment'=> $sales_slip_detail['is_ad_receive_payment'],
                    'ad_receive_sales_slip_detail_id'=> $sales_slip_detail['ad_receive_sales_slip_detail_id'],
                    'delivery_date'=> $sales_slip_detail['delivery_date'],
                    'billing_date' => $sales_slip_detail['billing_date'],
                    'payment_date' => $sales_slip_detail['payment_date'],
//#7363:Start
                    'product_name'=> $sales_slip_detail['product_name'],
//#7363:End
                    'contents'=> $sales_slip_detail['contents'],
                    'product_name'=> $sales_slip_detail['product_name'],
                    'contents'=> $sales_slip_detail['contents'],
                    'quantity'=> $sales_slip_detail['quantity'],
                    'tax_type'=> $sales_slip_detail['tax_type'],
                    'unit_price'=> $sales_slip_detail['unit_price'],
                    'amount'=> $sales_slip_detail['amount'],
                    'lastup_account_id'=> $this->auth->get_account_id(),
                    'create_datetime'=> date('Y-m-d H:i:s'),
                    'disable'=> 0);

            $this->sales_slip_detail_model->insert_sale_slip_detail($datainsert);

//#6949:Start
            if ($sales_slip_detail['is_ad_receive_payment'] == 1) {
                $last_sales_slip_detail_id = $this->db->insert_id();
                $update_ssd_params[] = array(
                    'id' => $last_sales_slip_detail_id,
                    'ad_receive_sales_slip_detail_id' => $last_sales_slip_detail_id,
                    'lastup_account_id'=> $this->auth->get_account_id(),
                    'lastup_datetime' => $this->sales_slip_model->_db_now(),
                );
                $conf_ssd = $this->db->select('ssd.id')
                    ->from('sales_slip_detail ssd')
                    ->join('sales_slip ss', 'ssd.sales_slip_id = ss.id AND ss.disable = 0', 'left')
                    ->where('(ss.id IS NULL OR (ss.id != ' . $sales_slip_id . ' AND ss.sales_slip_status != ' . SALES_STATUS_INVOICED_DISCARDED . '))')
                    ->where('is_ad_receive_payment', 0)
                    ->where('ad_receive_sales_slip_detail_id', $sales_slip_detail['ad_receive_sales_slip_detail_id'])
                    ->where('ssd.disable = 0')
                    ->get()->result_array();

                foreach ($conf_ssd as $ssd) {
                    $update_ssd_params[] = array(
                        'id' => $ssd['id'],
                        'disable' => 1,
                        'lastup_account_id'=> $this->auth->get_account_id(),
                        'lastup_datetime' => $this->sales_slip_model->_db_now(),
                    );
                }

            }
        }

        if (!empty($update_ssd_params)) {
            $this->db->update_batch('sales_slip_detail', $update_ssd_params, 'id');
        }
//#6949:End

        $account_login    = $this->auth->get_auth_data();
        $account_login_id = $account_login['account_id'];

        $success = $this->sales_slip_model->update_invoice_status($sales_slip_id, SALES_STATUS_INVOICED_DISCARDED,"",SALES_STATUS_INVOICED_MODIFY,$account_login_id);
        if($success){
            $this->update_flag_invoice($sales_slip_id,SALES_STATUS_INVOICED_DISCARDED);



        }
//#6949:Start
        $this->db->trans_complete();
//#6949:End
        //-------New-------
        $this->ajax_json(array(
            'status'   => $success ? true : false,
            'messages' => $success ? lang('lbl_edit_success') : lang('lbl_edit_error'),
        ));
        die;
    }

    public function approval_reject() {


        if (!$this->auth->has_permission('invoice/approval_reject')) show_404();

        $sales_slip_id = $this->input->post('sales_slip_id');

        $account_login    = $this->auth->get_auth_data();
        $account_login_id = $account_login['account_id'];

        if (!$sales_slip_id) show_404();

        $this->load->model('sales_slip_model');
        $this->load->helper('zenrin_system_helper');

        $sales_slip = $this->sales_slip_model->get_by_sales_slip_id($sales_slip_id);

        if (!$sales_slip || $sales_slip['sales_slip_status'] != SALES_STATUS_INVOICE_APPROVAL_PENDING) show_404();

        // 6846: Start
        $closing_month = $this->_get_closing_month();

        if (strtotime($sales_slip['delivery_date']) < strtotime($closing_month)) {
                echo lang('error_is_settlement_approve_reject');
                return;
        }
        // 6846: End

        if ($this->input->post('approve')) {

            $this->sales_slip_model->update_invoice_status($sales_slip_id, SALES_STATUS_INVOICE_APPROVED, $this->input->post('comment'),SALES_STATUS_INVOICED_MODIFY,$account_login_id);
            redirect('/_admin/invoice/show');
            return;
        }

        if ($this->input->post('reject')) {
#9787:Start
            $this->load->library('email_template');
            $this->load->model('account_model');
#9787:End

            
#9787:Start
            try {
                // $this->update_flag_invoice($sales_slip_id,SALES_STATUS_CREATED);
                // $this->sales_slip_model->update_invoice_status($sales_slip_id, SALES_STATUS_CREATED, $this->input->post('comment'),SALES_STATUS_INVOICED_MODIFY,$account_login_id);
                
                if (!$this->update_flag_invoice($sales_slip_id,SALES_STATUS_CREATED)) {
                    throw new Exception(lang('system_error'));
                }

                if (!$this->sales_slip_model->update_invoice_status($sales_slip_id, SALES_STATUS_CREATED, $this->input->post('comment'),SALES_STATUS_INVOICED_MODIFY,$account_login_id)) {
                    throw new Exception(lang('system_error'));
                }

                $account_approved = $this->account_model->get_account_by_id($sales_slip['sales_slip_approval_account_id']);

                $mail_params = array(
                        'client_name'       =>$sales_slip['client_name'],
                        'sales_slip_number' => $sales_slip['sales_slip_number']
                );

                $to_account = $account_approved['mail_address'];
                $subject = ' 【請求書発行却下】｛'.$sales_slip['client_name'].'｝_｛'.$sales_slip['sales_slip_number'].'｝';
                if (!$this->email_template->send_mail('discard_invoice',array('system_to' =>$to_account), $mail_params)) {
                    throw new Exception(lang('send_mail_change_report_fail'));
                }

                $this->session->set_flashdata('reject_result', array('status' => true, 'message' => lang('lbl_reject_success')));
            } catch (Exception $e) {
                $this->session->set_flashdata('reject_result', array('status' => false, 'message' => $e->getMessage()));
            }
#9787:End
            redirect('/_admin/invoice/show');
            return;
        }

        $sales_slip['details'] = sales_slip_export_pdf($sales_slip_id);

        $this->assign('sales_slip', $sales_slip);
        $this->view('invoice/approval_reject.tpl');
        die;
    }

    /**
     * Get date recent of closing_accountant
     *
     * @return date (Y-m-d)
     * @author cong_tien
     */
    private function _get_closing_month($plus_month = 1, $underscore  = "-") {
        $this->load->model('closing_accountant_model');

        if ($list_closing = $this->closing_accountant_model->get_closing_month_recent()) {

//#7088:Start

            $close_date = $list_closing[0]['close_date'];
            $close_dates = explode('-', $close_date);
            $close_date =   $close_dates[0]."-".$close_dates[1]."-01";
            $close_date =  date('Y'.$underscore.'m'.$underscore.'01', strtotime("+{$plus_month} month", strtotime($close_date)));

//#7088:End

        } else {
            $close_date = date("Y".$underscore."m".$underscore."01");
        }

        return $close_date;
    }

}
