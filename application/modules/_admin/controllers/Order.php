<?php
class Order extends ZR_Controller {
    public function show() {
        ini_set("memory_limit","1024M");
        $this->load->model('order_model');
        $this->load->model('gui_parts_element_model');
        $this->load->model('business_unit_model');
        //#7644:Start
        $this->load->model('client_model');
        //#7644:End
        $this->load->library('auth');
        $auth_data = $this->auth->get_auth_data();
        $this->order_model->set_order_status();
        $this->order_model->set_account_follow_bussiness_unit();

        $list_business_unit = array();
        $list_business_unit = $this->business_unit_model->get_product_business_unit_id_name();

        $tax_division = $this->gui_parts_element_model->get_by_gui_parts_column_name('tax_division', 'code,name');
        $order_status_configs = $this->gui_parts_element_model->get_by_gui_parts_column_name('order_status', 'code,name');
        $tax_division = json_encode($tax_division);

        $logged_id = $this->auth->get_account_id ();
        $logged_user_bu = $this->order_model->get_business_unit_of_logged_user($logged_id);

        $logged_id = $logged_user_bu['is_product_division']==1? $logged_id : "";

//#7644:Start
        //$search_result = $this->order_model->search_order($logged_id, $order_status = 2);
        $search_result = $this->order_model->search_order($logged_id, $order_status = 1);
//#7644:End

        if ($search_result) {
            if (count($search_result) > OVER_RECORD) {
                $search_result = array();
                $search_result['is_over_record'] = true;
            } else {
//#7644:Start
            	$close_date = $this->client_model->get_nearest_closing_date();
            	$closing_date = new DateTime($close_date['close_date']);
//#7644:End
                foreach ($search_result as &$item) {
//#7644:Start
                	$is_edit = true;
                	$delivery_date = new DateTime($item->od_delivery_date);
                	if ($delivery_date->format('Ym') <= $closing_date->format('Ym')) {
                		$is_edit = false;
                	}
                	$item->isCanEdit = $is_edit;
//#7644:End
                    foreach ($order_status_configs as $val) {
                        if ($item->od_order_status == $val['code']) {
                            $item->od_order_status_name = $val['name'];
                            break;
                        }
                    }
                    $item->od_gross_profit_rate_percent = $item->od_gross_profit_rate . '%';
                }
            }
        }

        // Get id, name, bu_id of account loggin
        $j_accounts = $this->order_model->get_j_account($this->auth->get_account_id ());
        $bu_id_user_logged = $j_accounts[0]->bu_id;

        $products = $this->order_model->get_products_by_bu_id($j_accounts[0]->bu_id);
        $products = json_encode($products);

        $j_accounts_ext = array();

        if ($this->auth->has_permission('order_add_edit_all', 'column')) {
            $j_accounts = $this->order_model->get_j_account_by_bu_id($j_accounts[0]->bu_id);
        } else {
            $j_accounts_ext = $this->order_model->get_j_account_by_bu_id($j_accounts[0]->bu_id);
        }

        //#7427:Start
        $params = array();
        $params['conditions']['status'] = array(
                CLIENT_STATUS_CL_APPROVED,
                CLIENT_STATUS_CREDIT_EVERYTIME
        );
        //#7427:End

//        $clients = $this->order_model->get_clients($params);
//        $clients = json_encode($clients);

        $can_edit = 0;
        if ($this->auth->has_permission('order_add_edit_all', 'column')) {
            $can_edit = 2;
            $this->assign("can_add_edit_all","can");
        } elseif ($this->auth->has_permission('order_add_edit_his_only', 'column')) {
            $can_edit = 1;
        }

        $can_invoices = 'none';
        if ($this->auth->has_permission('all_invoices', 'column')) {
            $can_invoices = 'all';
        } elseif ($this->auth->has_permission('self_invoices', 'column')) {
            $can_invoices = 'self';
        }
//#7066: Start
        //Have permission to view sale slip detail
        $is_sales_view = false;
        if ($this->auth->has_permission('sales_view', 'column')) {
            $is_sales_view = true;
        }
//#7066: End
        // acceptance_input_change permission
        $order_acceptance_input_change = $this->auth->has_permission('acceptance_input_change', 'column');
        // order_acceptance_view
        $order_acceptance_view = $this->auth->has_permission('order_acceptance_view', 'column');

        //get closest closing date
//        $this->load->model('closing_accountant_model');
//        $closest_closing_accountant = $this->closing_accountant_model->get_closest_closing_date();

        $this->assign('bu_id_user_logged', $bu_id_user_logged);
        $this->assign('list_business_unit', $list_business_unit);
//        $this->assign('closest_month_closing_date', date('m', strtotime($closest_closing_accountant[0]['closest_closing_date'])));
//        $this->assign('closest_year_closing_date', date('Y', strtotime($closest_closing_accountant[0]['closest_closing_date'])));
        $this->assign('j_accounts', $j_accounts);
        $this->assign('j_accounts_ext', $j_accounts_ext);
        $this->assign('order_model', $this->order_model);
        $this->assign('search_result', json_encode($search_result));
        $this->assign('products', $products);
        $this->assign('add', APPPATH . 'modules/_admin/views/order/add');
        $this->assign('detail', APPPATH . 'modules/_admin/views/order/detail');
        $this->assign('edit', APPPATH . 'modules/_admin/views/order/edit');
        $this->assign('approve_reject', APPPATH . 'modules/_admin/views/order/approve_reject');
        $this->assign('change_report', APPPATH . 'modules/_admin/views/order/change_repo_approve_reject');
        // $this->assign('auth_data', $auth_data);
        $this->assign('can_approve', json_encode($this->auth->has_permission('order_approve', 'column')));
        $this->assign('can_add', $this->auth->has_permission('order/add'));
        $this->assign('can_export', $this->auth->has_permission('order/export_order'));
        $this->assign('can_edit', json_encode($can_edit));
        $this->assign('can_invoices', $can_invoices);
//#7066: Start
        $this->assign('is_sales_view', $is_sales_view);
//#7066: Start
        $this->assign('can_acceptance', json_encode($this->auth->has_permission('order_acceptance_view', 'column')));
        $this->assign('tax_division', $tax_division);
//        $this->assign('clients', $clients);
        $this->assign('can_invoice_issue', json_encode($this->auth->has_permission('invoice_view', 'column')));
        $this->assign('order_acceptance_input_change', json_encode($order_acceptance_input_change));
        $this->assign('order_acceptance_view', json_encode($order_acceptance_view));
        $this->assign('is_edit', true);
        $this->view('order/show.tpl');
    }

    public function search_order() {
        $this->load->model('order_model');
        $this->load->model('gui_parts_element_model');

        $order_status_configs = $this->gui_parts_element_model->get_by_gui_parts_column_name('order_status', 'code,name');
        $error_msg = array();
        if ($input = json_decode($this->input->raw_input_stream)) {
            $order = $input->order;
            if (isset($order->j_code_branch_cd_to) && strlen(trim($order->j_code_branch_cd_to)) == 0) {
                unset($order->j_code_branch_cd_to);
            }
            $empty_date_from = false;
            $valid_date_to = false;
            $valid_date_from = false;
            if (isset($order->delivery_date_from) && $order->delivery_date_from) {
                if ( ! $order->delivery_date_from = parse_date($order->delivery_date_from)) {
                    $error_msg['error_delivery_date_from'] = 'Fromは無効な日付です';
                } else {
                    if (count(explode('-', $order->delivery_date_from)) == 2) {
                        $order->delivery_date_from = $order->delivery_date_from . '-01';
                    }
                    $valid_date_from = true;
                }
            } else {
                $empty_date_from = true;
            }

            if (isset($order->delivery_date_to) && $order->delivery_date_to) {
                if ( ! $order->delivery_date_to = parse_date($order->delivery_date_to)) {
                    $error_msg['error_delivery_date_to'] = 'Toは無効な日付です';
                } else {
                    if (count(explode('-', $order->delivery_date_to)) == 2) {

                        $listdateform = str_replace("-", "/", $order->delivery_date_from);
                        $n_count_dateform = count(explode("/",$listdateform)) ;

                        if($n_count_dateform = 3){

                            $listdateto = str_replace("-", "/", $order->delivery_date_to);
                            $listdatetoarra = explode("/",$listdateto);
                            $daydef = cal_days_in_month(CAL_GREGORIAN, $listdatetoarra[1], $listdatetoarra[0]);
                            $order->delivery_date_to = $order->delivery_date_to . '-'.$daydef;
                        }
                        else{
                         $order->delivery_date_to = $order->delivery_date_to . '-01';
                        }
                    }

                    $valid_date_to = true;
                }
            }

            if ($valid_date_to) {
                if ($empty_date_from) {
                    $error_msg['error_delivery_date'] = '範囲が正しくありません。';
                } else {
                    if ($valid_date_from && strtotime($order->delivery_date_to) < strtotime($order->delivery_date_from)) {

                        $error_msg['error_delivery_date'] = '検索範囲が正しくありません。';
                    }
                }
            }

            $empty_date_from = false;
            $valid_date_to = false;
            $valid_date_from = false;
            if (isset($order->order_regist_date_from) && $order->order_regist_date_from) {
                if ( ! $order->order_regist_date_from = parse_date($order->order_regist_date_from)) {
                    $error_msg['error_order_regist_date_from'] = 'Fromは無効な日付です';
                } else {
                    if (count(explode('-', $order->order_regist_date_from)) == 2) {
                        $order->order_regist_date_from = $order->order_regist_date_from . '-01';
                    }
                    $valid_date_from = true;
                }
            } else {
                $empty_date_from = true;
            }

            if (isset($order->order_regist_date_to) && $order->order_regist_date_to) {
                if ( ! $order->order_regist_date_to = parse_date($order->order_regist_date_to)) {
                    $error_msg['error_order_regist_date_to'] = 'Toは無効な日付です';
                } else {
                    if (count(explode('-', $order->order_regist_date_to)) == 2) {

                        $listdateform = str_replace("-", "/", $order->order_regist_date_from);
                        $n_count_dateform = count(explode("/",$listdateform)) ;
                        if($n_count_dateform = 3){

                            $listdateto = str_replace("-", "/", $order->order_regist_date_to);
                            $listdatetoarra = explode("/",$listdateto);
                            $daydef = cal_days_in_month(CAL_GREGORIAN, $listdatetoarra[1], $listdatetoarra[0]);
                            $order->order_regist_date_to = $order->order_regist_date_to . '-'.$daydef;
                        }
                        else{
                            $order->order_regist_date_to = $order->order_regist_date_to . '-01';
                        }

                    }

                    $valid_date_to = true;
                }
            }

            if ($valid_date_to) {
                if ($empty_date_from) {
                    $error_msg['error_order_regist_date'] = '範囲が正しくありません。';
                } else {
                    if ($valid_date_from && strtotime($order->order_regist_date_to) < strtotime($order->order_regist_date_from)) {
                        $error_msg['error_order_regist_date'] = '検索範囲が正しくありません。';
                    }
                }
            }

            $empty_date_from = false;
            $valid_date_to = false;
            $valid_date_from = false;
            if (isset($order->approval_date_from) && $order->approval_date_from) {
                if ( ! $order->approval_date_from = parse_date($order->approval_date_from)) {
                    $error_msg['error_approval_date_from'] = 'Fromは無効な日付です';
                } else {
                    if (count(explode('-', $order->approval_date_from)) == 2) {
                        $order->approval_date_from = $order->approval_date_from . '-01';
                    }
                    $valid_date_from = true;
                }
            } else {
                $empty_date_from = true;
            }

            if (isset($order->approval_date_to) && $order->approval_date_to) {
                if ( ! $order->approval_date_to = parse_date($order->approval_date_to)) {
                    $error_msg['error_approval_date_to'] = 'Toは無効な日付です';
                } else {
                    if (count(explode('-', $order->approval_date_to)) == 2) {


                        $listdateform = str_replace("-", "/", $order->approval_date_from);
                        $n_count_dateform = count(explode("/",$listdateform)) ;

                        if($n_count_dateform = 3){

                            $listdateto = str_replace("-", "/", $order->approval_date_to);
                            $listdatetoarra = explode("/",$listdateto);
                            $daydef = cal_days_in_month(CAL_GREGORIAN, $listdatetoarra[1], $listdatetoarra[0]);
                            $order->approval_date_to = $order->approval_date_to . '-'.$daydef;
                        }
                        else{

                        $order->approval_date_to = $order->approval_date_to . '-01';
                        }
                    }

                    $valid_date_to = true;
                }
            }

            if ($valid_date_to) {
                if ($empty_date_from) {
                    $error_msg['error_approval_date'] = '範囲が正しくありません。';
                } else {
                    if ($valid_date_from && strtotime($order->approval_date_to) < strtotime($order->approval_date_from)) {
                        $error_msg['error_approval_date'] = '検索範囲が正しくありません。';
                    }
                }
            }
            $empty_date_from = false;
            $valid_date_to = false;
            $valid_date_from = false;
            if (isset($order->payment_date_from) && $order->payment_date_from) {
                if ( ! $order->payment_date_from = parse_date($order->payment_date_from)) {
                    $error_msg['error_payment_date_from'] = 'Fromは無効な日付です';
                } else {
                    if (count(explode('-', $order->payment_date_from)) == 2) {
                        $order->payment_date_from = $order->payment_date_from . '-01';
                    }
                    $valid_date_from = true;
                }
            } else {
                $empty_date_from = true;
            }
            if (isset($order->payment_date_to) && $order->payment_date_to) {
                if ( ! $order->payment_date_to =  parse_date($order->payment_date_to)) {
                    $error_msg['error_payment_date_to'] = 'Toは無効な日付です';
                } else {
                    if (count(explode('-', $order->payment_date_to)) == 2) {

                        $listdateform = str_replace("-", "/", $order->payment_date_from);
                        $n_count_dateform = count(explode("/",$listdateform)) ;

                        if($n_count_dateform = 3){

                            $listdateto = str_replace("-", "/", $order->payment_date_to);
                            $listdatetoarra = explode("/",$listdateto);
                            $daydef = cal_days_in_month(CAL_GREGORIAN, $listdatetoarra[1], $listdatetoarra[0]);
                            $order->payment_date_to = $order->payment_date_to . '-'.$daydef;
                        }
                        else{
                           $order->payment_date_to = $order->payment_date_to . '-01';
                        }
                    }

                    $valid_date_to = true;
                }
            }
            if ($valid_date_to) {
                if ($empty_date_from) {
                    $error_msg['error_payment_date'] = '範囲が正しくありません。';
                } else {
                    if ($valid_date_from && strtotime($order->payment_date_to) < strtotime($order->payment_date_from)) {
                        $error_msg['error_payment_date'] = '検索範囲が正しくありません。';
                    }
                }
            }

            if (isset($order->j_code_branch_cd_from)) {
                if ((strpos($order->j_code_branch_cd_from, '-') !== FALSE) && (strlen($order->j_code_branch_cd_from) - strpos($order->j_code_branch_cd_from, '-') > 3)) {
                    $error_msg['error_j_code_branch_cd_1'] = str_replace('%field%', 'From', lang('common_msg_search_range_invalid'));
                }
            }

            if (isset($order->j_code_branch_cd_to) && strlen($order->j_code_branch_cd_to) > 0 ) {
            	#7067:Start
            	$standadize_jcode_range = $this->standadize_jcode_range($order->j_code_branch_cd_to, 9, $this->order_model->get_lenght_max_jcode_order());
            	if (!is_null($standadize_jcode_range))
            		$order->j_code_branch_cd_to = $standadize_jcode_range;
            	#7067:End

                if (!isset($order->j_code_branch_cd_from) || (isset($order->j_code_branch_cd_from) && strlen($order->j_code_branch_cd_from) == 0)) {
                    $error_msg['error_j_code_branch_cd_1'] = lang("error_j_code_branch_cd_1");
                }
            }

            if (isset($order->j_code_branch_cd_from) && isset($order->j_code_branch_cd_to)) {
            	#7067:Start
            	$standadize_jcode_range = $this->standadize_jcode_range($order->j_code_branch_cd_from, 0, $this->order_model->get_lenght_max_jcode_order());
            	if (!is_null($standadize_jcode_range))
            		$order->j_code_branch_cd_from = $standadize_jcode_range;
            	#7067:End

                if (strcmp(trim($order->j_code_branch_cd_from), trim($order->j_code_branch_cd_to)) > 0) {
                    $error_msg['error_j_code_branch_cd_2'] = lang("error_j_code_branch_cd_2");
                }
                if ((strpos($order->j_code_branch_cd_to, '-') !== FALSE) && (strlen($order->j_code_branch_cd_to) - strpos($order->j_code_branch_cd_to, '-') > 3)) {
                    $error_msg['error_j_code_branch_cd_2'] = str_replace('%field%', 'To', lang('common_msg_search_range_invalid'));
                }
            }
        }

        if ( ! empty($error_msg)) {
            $error_msg['error_form'] = true;
            die(json_encode($error_msg)); exit();
        }
        $search_result = $this->order_model->search_order($set_logged_division="");

        if ($search_result) {
            if (count($search_result) > OVER_RECORD) {
                $search_result = array();
                $search_result['is_over_record'] = true;
            } else {
                //tmp#6841: Start 2015/09/24
                //Get nearest close date
                $this->load->model('client_model');
                $close_date = $this->client_model->get_nearest_closing_date();
                $closing_date = new DateTime($close_date['close_date']);
                //tmp#6841: End
                foreach ($search_result as &$item) {
                    //tmp#6841: Start 2015/09/24
                    $is_edit = true;
                    $delivery_date = new DateTime($item->od_delivery_date);
                    if ($delivery_date->format('Ym') <= $closing_date->format('Ym')) {
                        $is_edit = false;
                    }
                    $item->isCanEdit = $is_edit;
                    //tmp#6841: End
                    foreach ($order_status_configs as $val) {
                        if ($item->od_order_status == $val['code']) {
                            $item->od_order_status_name = $val['name'];
                            break;
                        }
                    }
                    $item->od_gross_profit_rate_percent = $item->od_gross_profit_rate . '%';
                }
            }
        }
        die(json_encode($search_result));
    }

    public function load_template() {
        $template = $this->input->post('template');

        $this->load->model('order_model');

        switch ($template) {
            case 'add':
                // Get id, name, bu_id of account loggin
                $j_accounts = $this->order_model->get_j_account($this->auth->get_account_id ());

                if ($this->auth->has_permission('order_add_edit_all', 'column')) {
                    $j_accounts = $this->order_model->get_j_account_by_bu_id($j_accounts[0]->bu_id);
                }

                //#7427:Start
                $params = array();
                $params['conditions']['status'] = array(
                CLIENT_STATUS_CL_APPROVED,
                CLIENT_STATUS_CREDIT_EVERYTIME
                );
                //#7427:End

                $clients = $this->order_model->get_clients($params, true);
                $clients = json_encode($clients);

                $this->assign('j_accounts', $j_accounts);
                $this->assign('clients', $clients);
                $this->view('order/add.tpl');
                break;
            case 'detail':
                $this->assign('is_edit', true);
                $this->view('order/detail.tpl');
                break;
            case 'edit':
                // Get id, name, bu_id of account loggin
                $j_accounts = $this->order_model->get_j_account($this->auth->get_account_id ());

                if ($this->auth->has_permission('order_add_edit_all', 'column')) {
                    $j_accounts = $this->order_model->get_j_account_by_bu_id($j_accounts[0]->bu_id);
                }

                //get closest closing date
                $this->load->model('closing_accountant_model');
                $closest_closing_accountant = $this->closing_accountant_model->get_closest_closing_date();

                $this->assign('j_accounts', $j_accounts);
                $this->assign('closest_month_closing_date', date('m', strtotime($closest_closing_accountant[0]['closest_closing_date'])));
                $this->assign('closest_year_closing_date', date('Y', strtotime($closest_closing_accountant[0]['closest_closing_date'])));
                $this->view('order/edit.tpl');
                break;
            case 'approve_reject':
                $this->view('order/approve_reject.tpl');
                break;
            case 'change_report':
                $this->view('order/change_repo_approve_reject.tpl');
                break;
        }
    }

    public function export_order() {
        $this->load->model('order_model');
        $this->order_model->export_order();
    }

    public function save() {
        $this->load->model('order_model');
        $data = json_decode($this->input->raw_input_stream);
        $this->order_model->save($data);
    }

    public function detail() {
        $this->load->model('order_model');
        $this->assign('order_id', $this->input->get('order_id'));
        $this->view('order/detail.tpl');
    }

    public function delete() {
        $this->load->model('order_model');
        $data = json_decode($this->input->raw_input_stream);
        $this->order_model->delete($data->order_id);
    }

    public function get_detail() {
        $this->load->model('order_model');
        $data = json_decode($this->input->raw_input_stream);
        $order_detail = $this->order_model->get_detail($data->order_id);

        //Get nearest close date
        $this->load->model('client_model');
        $close_date = $this->client_model->get_nearest_closing_date();
        $closing_date = new DateTime($close_date['close_date']);
//         $closing_date->add(new DateInterval('P1M'));

            foreach ($order_detail as $key => &$detail) {

            $is_edit = true;
            $delivery_date = new DateTime($detail['od_delivery_date']);
            if ($delivery_date->format('Ym') <= $closing_date->format('Ym')) {
                $is_edit = false;
            }

            if (($detail['o_is_order_input'] == 1)
                    || ($detail['o_is_acceptance_input'] == 1)
                    || ($detail['o_is_sales_slip_input'] == 1)
                    || ($detail['o_is_invoice_issue'] == 1)
//#6956: Start
//                     || ($detail['od_is_ad_sales_slip_input'] == 1)
//                     || ($detail['od_is_ad_invoice_issue'] == 1)
//#6956: End
            ) {
                $is_edit = false;
            }

            if ($data->form == 'change_report') {
                $order_status = array(ORDER_STATUS_UNDETERMINED, ORDER_STATUS_CONFIRMED);
            } else {
                $order_status = array(ORDER_STATUS_UNAPPROVED);
            }

            if (!in_array($detail['od_order_status'], $order_status)) {
                $is_edit = false;
            }

            $detail['isCanEdit'] = $is_edit;
            $detail['o_account_id_old'] = $detail['o_account_id'];
        }
//#9763:Start
        $this->load->model('reconcile_amount_model');
        // get bugyo_next_month_receive (tiền nợ của tháng tiếp theo (table payment_entry))
        $payment_entry_result = $this->reconcile_amount_model->get_payment_entry_follow_client($order_detail[0]['o_client_id']);
        $bugyo_next_month_receive = $payment_entry_result['bugyo_next_month_receive']; //16172654

        $payment_result = $this->reconcile_amount_model->get_amount_payment_follow_client($order_detail[0]['o_client_id']);
        $money_received = 0;
        if (!empty($payment_result)) {
            foreach ($payment_result as $row) {
                $money_received += $row['amount'];
            }
        }
        $client_debt = $bugyo_next_month_receive - $money_received;
        $client_debt = ($client_debt > 0) ? $client_debt : 0;

        $order_detail[0]['c_debt'] = $client_debt;
//#9763:End
        $this->ajax_json($order_detail);
    }

    public function edit() {
        $this->load->model('order_model');
        // $this->load->library('auth');
        // $auth_data = $this->auth->get_auth_data();

        $j_accounts = $this->order_model->get_j_account($this->auth->get_account_id ());
        $this->assign('j_accounts', $j_accounts);
        $this->assign('order_id', $this->input->get('order_id'));
        $this->view('order/edit.tpl');
    }

    /**
     * Approve/Reject order screen
     *
     * @author hoang_minh
     * @since 2015-06-15
     */
    public function approve_reject() {
        $this->load->model('order_model');
        $this->assign('order_id', $this->input->get('order_id'));

        $this->view('order/approve_reject.tpl');
    }

    /**
     * Approving order
     *
     * @author hoang_minh
     * @since 2015-06-15
     */
    public function approve_order() {
        //load library
        $this->load->library('email_template');

        //load model
        $this->load->model('order_model');
        $this->load->model('client_model');
        $this->load->model('account_model');
        $this->load->model('authority_model');
        $this->load->model('authority_model');
        $this->load->model('consumption_tax_model');
        $this->load->model('order_detail_model');

        //init variable
        $post = array();
        $orders = array();

        if ($post = json_decode($this->input->raw_input_stream)) {
            //check order's information
            $orders = $this->order_model->get_detail($post->order_id);

            if (empty($orders)) {
                echo $this->ajax_json(array('result' => false, 'error_msg' => lang(''))); exit();
            }

            //tmp#6841: Start
            //Get nearest close date
            $this->load->model('client_model');
            $close_date = $this->client_model->get_nearest_closing_date();
            $closing_date = new DateTime($close_date['close_date']);

            $order_detail = $this->order_model->get_detail($post->order_id);

            foreach ($order_detail as $key => $detail) {
                $delivery_date = new DateTime($detail['od_delivery_date']);
                if ($delivery_date->format('Ym') <= $closing_date->format('Ym')) {
                    echo $this->ajax_json(array('result' => false, 'error_msg' => '納品日は経理締め済みです。')); exit();
                }
            }
            //tmp#6841: End
            //processing approve
            try {
                $this->order_model->begin_transaction();

                //update order
                $update_data = array(
                        'order_approval_account_id' => $this->auth->get_account_id(),
                        'order_approval_date' => $this->order_model->_db_now(),
                        '`comment`' => isset($post->comment) ? $post->comment : '',
                        'lastup_account_id' => $this->auth->get_account_id(),
                        'lastup_datetime' => $this->order_model->_db_now(),
                );

                $update_conditions = array(
                        'id' => $post->order_id
                );

                if ( ! $this->order_model->update_order(array('update_data' => $update_data, 'update_conditions' => $update_conditions))) {
                    throw new Exception(lang('update_order_fail'));
                }

                //update status for order detail
                $update_data = array(
                        'order_status' => Order_model::UNDETERMINED,
                        'lastup_account_id' => $this->auth->get_account_id(),
                        'lastup_datetime' => $this->order_model->_db_now(),
                );

                $update_conditions = array(
                        'order_id' => $post->order_id,
                        'order_status' => Order_model::UNAPPROVED
                );

                if ( ! $this->order_detail_model->update_order_detail(array('update_data' => $update_data, 'update_conditions' => $update_conditions))) {
                    throw new Exception(lang('update_order_detail_fail'));
                }

                //send mail
                $authority_ids = $this->authority_model->get_authority_by_quick_setting('mail_order_recorded');
                $params = array(
                    'conditions' => array(
                            'authority_ids' => implode(',', $authority_ids)
                    ),
                    'columns' => array('mail_address')
                );

                $send_mail_list = $this->account_model->get_accounts_by_parameters($params);

                //order detail
                $order_detail_mail_content = array();
                $total_cost_total_price = 0;
                $total_gross_profit_amount = 0;

                //get tax rate
                $get_tax_rate_params = array(
                        'columns' => array('`consumption_tax`.rate, `consumption_tax_adaptation`.adaptation_date'),
                        'conditions' => array(
                                'consumption_tax_type_id' => CONSUMPTION_TAX
                        ),
                        'orders' => array(
                                '`consumption_tax_adaptation`.adaptation_date' => 'DESC'
                        )
                );

                $tax_rate_list = $this->consumption_tax_model->get_tax_rate_by_conditions($get_tax_rate_params);
                foreach ($orders as $item) {
                    $order_detail_mail_content[] = $item['od_branch_cd'] . ' / '
                                                 . $item['p_name'] . ' / '
                                                 . $item['od_contents'] . ' / '
                                                 . $item['od_delivery_date'] . ' / '
                                                 . number_format($item['od_amount']) . ' / '
                                                 . number_format($item['od_gross_profit_amount']) . ' / '
                                                 . $item['od_gross_profit_rate'] . '%';

                    switch ($item['od_tax_type']) {
                        case TAX_NON:
                        case TAX_EXCLUSIVE:
//                            $total_cost_total_price += $item['od_amount'];
                            $total_cost_total_price = bcadd($total_cost_total_price, $item['od_amount']);
                            break;

                        case TAX_INCLUDED:
                            $tax_rate = 0;
                            if ($tax_rate_list) {
                                foreach ($tax_rate_list as $txr) {
                                    if (strtotime($item['od_delivery_date']) >= strtotime($txr['adaptation_date'])) {
                                        $tax_rate = $txr['rate'];
                                        break;
                                    }
                                }
                            }

//                            $total_cost_total_price += ($item['od_amount'] / (1 + $tax_rate / 100));
                            $total_cost_total_price = bcadd($total_cost_total_price, bcdiv($item['od_amount'], bcadd(1, bcdiv($tax_rate, 100))));
                            break;

                        default:
                            break;
                    }
                    $total_gross_profit_amount += $item['od_gross_profit_amount'];
                }

                $total_cost_total_price = f_ceil($total_cost_total_price);

                //prepair mail content
                $replace_params = array(
                        'charge_name' => $orders[0]['a_name'],
                        'business_unit_name' => $orders[0]['bu_name'],
                        'department_name' => $orders[0]['d_name'],
                        'j_account_id' => $orders[0]['a_name'],
                        'client_name' => $orders[0]['c_name'],
                        'j_code' => $orders[0]['o_j_code'],
                        'order_detail_mail_content' => implode("\r\n", $order_detail_mail_content),
                        'total_cost_total_price' => number_format($total_cost_total_price),
                        'total_gross_profit_amount' => number_format($total_gross_profit_amount),
                        'total_gross_profit_rate' => ($total_cost_total_price != 0) ? number_format(($total_gross_profit_amount / $total_cost_total_price * 100), 2) : 0
                );

                foreach ($send_mail_list as $item) {
                    if ( ! $this->email_template->send_mail('order_approve_reject', array('system_to' => $item['mail_address']), $replace_params)) {
                        throw new Exception(lang('send_mail_approve_order_fail'));
                    }
                }

                $this->order_model->commit_transaction();
                echo $this->ajax_json(array('result' => true, 'error_msg' => lang('approve_order_success'))); exit();

            } catch (Exception $ex) {

                $this->order_model->rollback_transaction();
                $this->app_logger->error_log($ex->getMessage());

                echo $this->ajax_json(array('result' => false, 'error_msg' => lang('approve_order_fail'))); exit();
            }
        }

        echo $this->ajax_json(array('result' => false, 'error_msg' => lang('approve_order_fail'))); exit();
    }

    /**
     * Reject order
     *
     * @author hoang_minh
     * @since 2015-06-17
     */
    public function reject_order() {
        //load model
        $this->load->model('order_model');
        $this->load->model('order_detail_model');

        //init variable
        $post = array();
        $orders = array();

        if ($post = json_decode($this->input->raw_input_stream)) {
            //check order's information
            $orders = $this->order_model->get_detail($post->order_id);

            if (empty($orders)) {
                echo $this->ajax_json(array('result' => false, 'error_msg' => lang(''))); exit();
            }

            //update order
            $update_data = array(
                    '`comment`' => (isset($post->comment)) ? $post->comment : '',
                    'lastup_account_id' => $this->auth->get_account_id(),
                    'lastup_datetime' => $this->order_model->_db_now(),
            );

            $update_conditions = array(
                    'id' => $post->order_id
            );

            if ( ! $this->order_model->update_order(array('update_data' => $update_data, 'update_conditions' => $update_conditions))) {
                throw new Exception(lang('update_order_fail'));
            }

            //update order detail
            $update_data = array(
                    'order_status' => Order_model::REJECTED,
                    'lastup_account_id' => $this->auth->get_account_id(),
                    'lastup_datetime' => $this->order_model->_db_now(),
            );

            $update_conditions = array(
                    'order_id' => $post->order_id,
                    'order_status' => Order_model::UNAPPROVED
            );

            if ( ! $this->order_detail_model->update_order_detail(array('update_data' => $update_data, 'update_conditions' => $update_conditions))) {
                throw new Exception(lang('update_order_detail_fail'));
            }

            echo $this->ajax_json(array('result' => true, 'error_msg' => '')); exit();
        }

        echo $this->ajax_json(array('result' => false, 'error_msg' => lang('approve_order_fail'))); exit();
    }

    /**
     * change report template
     *
     * @author thanh_trung
     * @since 2015-06-17
     */
    public function change_repo_approve_reject() {
        $this->load->model('order_model');
        $this->load->model('gui_parts_element_model');

        $post = json_decode($this->input->raw_input_stream);
        if (empty($post)) {
            redirect ( '_admin/order/show' );
        }
        // If this called from (after changing report) approve/reject screen
        if(isset($post->type) && $post->type =='approve_reject') {

            // Master information show in top half of screen
            $master_information = $this->order_model->get_master_information($post->order_id);
            // Information from detail table
            $grid_detail_information = $this->order_model->get_detail_information($post->order_id);
            // Information from detail log table
            $grid_detail_log_information = $this->order_model->get_detail_log_information($post->order_id);
            // Mix information from detail and from detail log together then sort...
            $list_grid_detail = array();
            // Add information (not all) from detail to detail log to create the same record
            foreach($grid_detail_information AS $detail) {
                $detail['change_report'] = '-';
                $changed = false;
                foreach($grid_detail_log_information AS $detail_log) {
                    if($detail['od_detail_id'] == $detail_log['od_detail_id']) {
                        $changed = true;
                    }
                }
                if ($detail['od_is_cancel_request']) {
                    $detail['change_report'] = "キャンセル";
                }
                if($changed) {
                    $detail['change_report'] = "変更前";
                }
                $list_grid_detail[] = $detail;

                foreach($grid_detail_log_information AS $detail_log) {
                    // Check every record in order detail log
                    if($detail['od_detail_id'] == $detail_log['od_detail_id']) {

                        $detail_log['od_branch_cd'] = $detail['od_branch_cd'];
                        $detail_log['od_order_status'] = $detail['od_order_status'];
                        $detail_log['od_order_status_name'] = $detail['od_order_status_name'];
                        $detail_log['change_report'] = "変更後";

                        if($detail['od_is_cancel_request'] == 1) {
                            $detail_log['change_report'] = "キャンセル";
                        }

                        $detail_log['od_is_change_report_apply'] = $detail['od_is_change_report_apply'];

                        if($detail_log['od_delivery_date'] != $detail['od_delivery_date']) {
                            $detail_log['od_shift_color_cd'] = $detail['od_shift_color_cd'];
                        }
#7085:Start
                        $detail_log['is_change_price'] = FALSE;
                        if( ($detail_log['od_cost_unit_price']!=$detail['od_cost_unit_price']) ||
                            ($detail_log['od_cost_total_price']!=$detail['od_cost_total_price'])
                            ) {
                            $detail_log['is_change_price'] = TRUE;
                        }
#7085:End


                        $list_grid_detail[] = $detail_log;
                    } // End if($detail['od_detail_id'] == $detail_log['
                } // End foreach($grid_detail_log_information
                // echo '<pre>';print_r($detail_log);die;
            }
            // Create data to send back to screen
            $data_list = array('master_information'=>$master_information, 'list_grid_detail'=>$list_grid_detail);
            return $this->ajax_json($data_list);
        }
        // If this called from view detail screen
        else {
            // Check type of view: changed report view or normal view
            $master_information = $this->order_model->get_master_information($post->order_id);
             $type_view_changed_report = $this->order_model->check_type_of_view_is_changed_report($post->order_id);
            // If normal view:
            if(!$type_view_changed_report) {
                $grid_detail_information = $this->order_model->get_detail_information_view_normally($post->order_id);
                //tmp#6841: Start 2015/09/24
                //Get nearest close date
                $this->load->model('client_model');
                $close_date = $this->client_model->get_nearest_closing_date();
                $closing_date = new DateTime($close_date['close_date']);

                foreach ($grid_detail_information as $key => &$tmp_order_detail) {
                    $is_edit = true;
                    $delivery_date = new DateTime($tmp_order_detail['od_delivery_date']);
                    if ($delivery_date->format('Ym') <= $closing_date->format('Ym')) {
                        $is_edit = false;
                    }

                    $tmp_order_detail['isCanEdit'] = $is_edit;
                }
                //tmp#6841: End
                $data_list = array("type_of_changed_report_view" => $type_view_changed_report,
                                    "master_information" => $master_information,
                                    "list_grid_detail" => $grid_detail_information);
                return $this->ajax_json($data_list);
                // print_r($data_list);
            } else {
                // If changed report view
                $grid_detail_information = $this->order_model->get_detail_information_view_changed_report($post->order_id);
                $grid_detail_log_information = $this->order_model->get_detail_log_information($post->order_id);
                $list_grid_detail = array();
                foreach($grid_detail_information AS $detail) {
                    $detail['change_report'] = '-';
                    $changed = false;
                    foreach($grid_detail_log_information AS $detail_log) {
                        if($detail['od_detail_id'] == $detail_log['od_detail_id']) {
                            $changed = true;
                        }
                    }
                    if($detail['od_is_cancel_request']) {
                        $detail['change_report'] = "キャンセル";
                    }
                    if($changed) {
                        $detail['change_report'] = "変更前";
                    }
                    $list_grid_detail[] = $detail;
                    foreach($grid_detail_log_information AS $detail_log) {

                        if($detail['od_detail_id'] == $detail_log['od_detail_id']) {
                            $detail_log['change_report'] = "変更後";
                            if($detail['od_is_cancel_request'] == 1) {
                                $detail_log['change_report'] = "キャンセル";
                            }
                            $detail_log['od_branch_cd'] = $detail['od_branch_cd'];
                            $detail_log['o_is_ad_sales_slip_input'] = $detail['o_is_ad_sales_slip_input'];
                            $detail_log['o_is_ad_invoice_issue'] = $detail['o_is_ad_invoice_issue'];
                            $detail_log['o_is_order_input'] = $detail['o_is_order_input'];
                            $detail_log['o_is_acceptance_input'] = $detail['o_is_acceptance_input'];
                            $detail_log['o_is_sales_slip_input'] = $detail['o_is_sales_slip_input'];
                            $detail_log['o_is_invoice_issue'] = $detail['o_is_invoice_issue'];
                            $detail_log['od_order_status'] = $detail['od_order_status'];
                            $detail_log['od_order_status_name'] = $detail['od_order_status_name'];
                            $detail_log['od_is_cancel_request'] = $detail['od_is_cancel_request'];
                            $detail_log['o_account_id'] = $detail['o_account_id'];
                            $detail_log['o_is_ad_receive_payment'] = $detail['o_is_ad_receive_payment'];
                            $detail_log['od_is_change_report_apply'] = $detail['od_is_change_report_apply'];

                            if($detail_log['od_delivery_date']!=$detail['od_delivery_date']) {
                                $detail_log['od_shift_color_cd'] = $detail['od_shift_color_cd'];
                            }
#7085:Start
                            $detail_log['is_change_price'] = FALSE;
                            if( ($detail_log['od_cost_unit_price']!=$detail['od_cost_unit_price']) ||
                                ($detail_log['od_cost_total_price']!=$detail['od_cost_total_price'])
                                ) {
                                $detail_log['is_change_price'] = TRUE;
                            }
#7085:End
                            $list_grid_detail[] = $detail_log;
                        } // End if($detail['od_detail_id'] == $detail_log['od_detai
                    } // End foreach($grid_detail_log_information AS $detail
                } // End foreach($grid_detail_information AS $de
                $data_list = array("type_of_changed_report_view" => $type_view_changed_report,
                        "master_information" => $master_information,
                        "list_grid_detail" => $list_grid_detail);
                // echo '<pre>';print_r($data_list);die;
                return $this->ajax_json($data_list);
            }
        }
    }


    /**
     * approve report
     *
     * check order_status = 1 && is_change_report_apply = 1 (has change report)
     * get just orders has change report that exist in order_detail_log
     * -- if exists -> update is_change_report_apply = 0
     * -- approve -> update orders detail has change report in table order_detail_log to order_detail
     * -- send email to client has confirm change report
     *
     * @author thanh_trung
     * @since 2015-06-17
     */
    public function approve_report() {
        $this->load->model('order_model');
        $this->load->library('email_template');
        $this->load->model('account_model');
        $this->load->model('authority_model');

        $post = array();
        $orders = array();

        // check data get from http (js file) and decode to object data.
        if ($post = json_decode($this->input->raw_input_stream)) {

            // Update order detail
            if (!$list_order_detail_update = $this->order_model->get_order_detail_id_and_is_cancel_request($post->order_id)) {
                echo $this->ajax_json(array('result' => false, 'error_msg' => lang('order apply not exist')));
            }

            if (!$order_apply_exits = $this->order_model->check_change_report_apply($post->order_id)) {
                echo $this->ajax_json(array('result' => false, 'error_msg' => lang('order apply not exist')));
            }
            try {

                $this->order_model->begin_transaction();

                //Update comment to order table
                $data = array(
//#8216:Start
                        'total_amount'     => $post->o_cost_total_price,
                		'total_amount_tax' => $post->o_tax_total,
                		'lastup_datetime'  => $this->order_model->_db_now(),
//#8216:End
                        '`comment`' => $post->old_comment . (isset($post->comment) ? "\r\n" . $post->comment : ''),
                        'lastup_account_id' => $this->auth->get_account_id(),
                );

                // condition update order
                $conditions = array( 'id' => $post->order_id, 'j_code' => $post->j_code );

                // update order
                if (!$this->order_model->update_change_report(array('data' => $data, 'conditions' => $conditions))) {
                    throw new Exception(lang('update_order_fail'));
                }

//#6970: Start
                $order_approval_date = new DateTime($order_apply_exits[0]['order_approval_date']);

                //Get business unit detail
                $this->load->model('account_model');
                $business_unit = $this->account_model->get_account_business_unit($order_apply_exits[0]['o_j_account_id']);
//#6970: End

                // Order detail has is_cancel_request = 1 => set order_status = 9
                // else order_status = 1
                foreach($list_order_detail_update as $od) {

                    $data_od_update = array(
                            'is_change_report_apply' => 0,
                            'order_status' => (($od['is_cancel_request'] == 1) ? 9:2),
                            'is_cancel_request' => 0
                    );

                    if ($od['is_cancel_request'] == 1) {
#7625:Start
                        $data_od_update['cancel_datetime'] = $this->order_model->_db_now();
                        $data_od_update['change_report_approval_date'] = $this->order_model->_db_now('Y-m-d');
                        $data_od_update['change_report_approval_account_id'] = $this->auth->get_account_id();

                        // Send mail approve order detail has been cancel.
                        $mail_content = '■ キャンセル前' . "\r\n"
                                            . '商品名　　	:' . $od['p_name'] . "\r\n"
                                            . '内容　　　	:' . $od['contents'] ."\r\n"
                                            . '納品日　　	:' . str_replace("-", "/", $od['delivery_date']) ."\r\n"
                                            . '受注金額	:' . number_format($od['amount']) . "\r\n"
                                            . '粗利額　　	:' . number_format($od['gross_profit_amount']) . "\r\n"
                                            . '粗利率　　	:' . number_format($od['gross_profit_rate'], 2, '.', '') . '%' . "\r\n"
                                            . '----------------------------------------'
                                            . "\r\n";
                        $mail_params = array(
                                        'account_id'    => (isset($order_apply_exits[0]['a_name']) ? $order_apply_exits[0]['a_name'] : ''),
                                        'bu_name'       => (isset($order_apply_exits[0]['bu_name']) ? $order_apply_exits[0]['bu_name'] : ''),
                                        'client_name'   => (isset($order_apply_exits[0]['c_name']) ? $order_apply_exits[0]['c_name'] : ''),
                                        'j_code'        => (isset($order_apply_exits[0]['o_j_code']) ? $order_apply_exits[0]['o_j_code'] : ''),
                                        'branch_cd'     => (isset($od['branch_cd']) ? $od['branch_cd'] : ''),
                                        'mail_content'  => $mail_content
                        );

                        $o_change_report_approval_account_id = $order_apply_exits[0]['o_change_report_approval_account_id'];

                        $send_mail_list = $this->order_model->get_mail_list_authority($order_apply_exits[0]['id'], $o_change_report_approval_account_id);

                        // send email to client
                        $mail_address_list = array();

                        foreach ($send_mail_list as $item) {

                            if (!in_array($item['mail_address'], $mail_address_list)) {
                                if (!$this->email_template->send_mail('change_report_approve_cancel_request', array('system_to' => $item['mail_address']), $mail_params)) {
                                    throw new Exception(lang('send_mail_change_report_fail'));
                                }
                                $mail_address_list[] = $item['mail_address'];
                            }
                        }

                    }
#7625:End
                    $condition_od_update = array('id' => $od['id']);

                    if(!$this->order_model->update_order_detail(array('data' => $data_od_update, 'conditions' => $condition_od_update))) {
                        throw new Exception(lang('approve_order_detail_fail'));
                    }
                }
                // End update order detail
                // Get data of order has been apply in table order_detail_log only (data temp new for order apply)
                if ($orders_apply_log = $this->order_model->get_order_detail_log($post->order_id)) {

                    foreach ($orders_apply_log as $item) {

                        /**
                         * prepare content mail
                         * before change: order_detail
                         * after change: order_detail_log
                         */
                      $mail_content = '■ 変更前 ' . "\r\n"
                                . '商品名	:' . $item->od_name . "\r\n"
                                . '内容　	:' . $item->od_contents ."\r\n"
                                . '納品日	:' . str_replace("-", "/", $item->od_delivery_date) ."\r\n"
                                . '受注金額	:' . number_format($item->od_amount) . "\r\n"
                                . '粗利額	:' . number_format($item->od_gross_profit_amount) . "\r\n"
                                . '粗利率	:' . number_format($item->od_gross_profit_rate, 2, '.', '') . '%' . "\r\n"
                                . '----------------------------------------'
                                . "\r\n\r\n"

                                . '■ 変更後' . "\r\n"
                                . '商品名	:' . $item->ol_name . "\r\n"
                                . '内容　	:' . $item->ol_contents ."\r\n"
                                . '納品日	:' . str_replace("-", "/", $item->ol_delivery_date) ."\r\n"
                                . '受注金額	:' . number_format($item->ol_amount) . "\r\n"
                                . '粗利額	:' . number_format($item->ol_gross_profit_amount) . "\r\n"
                                . '粗利率	:' . number_format($item->ol_gross_profit_rate, 2, '.', '') . '%' . "\r\n"
                                . '----------------------------------------'
                                . "\r\n";

                        // update new data for order_detail from order_detail_log
                        $data       = array();
                        $conditions = array();
                        $data = array(
                                'product_id'          => $item->ol_product_id,
                                'contents'            => $item->ol_contents,
                                'delivery_date'       => $item->ol_delivery_date,
                                'billing_date'        => $item->ol_billing_date,
                                'payment_date'        => $item->ol_payment_date,
                                'quantity'            => $item->ol_quantity,
                                'tax_type'            => $item->ol_tax_type,
                                'unit_price'          => $item->ol_unit_price,
                                'amount'              => $item->ol_amount,
                                'cost_unit_price'     => $item->ol_cost_unit_price,
                                'cost_total_price'    => $item->ol_cost_total_price,
                                'change_report_approval_date' => $this->order_model->_db_now('Y-m-d'),
                                'change_report_approval_account_id'   => $this->auth->get_account_id(),
                                'gross_profit_amount' => $item->ol_gross_profit_amount,
                                'gross_profit_rate'   => $item->ol_gross_profit_rate,
                                'lastup_account_id'   => $this->auth->get_account_id(),
                                'lastup_datetime'     => $this->order_model->_db_now(),
                        );

//#6970: Start
                        //Get current date
                        $current_date = new DateTime(date('Y-m-d'));
//                         if (($order_approval_date < $current_date) && ($item->od_original_delivery_date == '0000-00-00') ) {
                        if (($order_approval_date->format('Ym') < $current_date->format('Ym')) && ($item->od_original_delivery_date == '0000-00-00') ) {
                            //delivery date before change report
                            $old_delivery_date = new DateTime($item->od_delivery_date);

                            //Delivery_date after change report
                            $new_delivery_date = new DateTime($item->ol_delivery_date);

                            //Subtract 1 month
                            $totalization_target_months = $business_unit[0]['totalization_target_months'] - 1;
                            if ($old_delivery_date->format('Y-m') != $new_delivery_date->format('Y-m')) {
                                $current_date->modify($totalization_target_months . ' months');
//#Start: Request edditing form Mr. Kobayashi 2015/12/2
//                                 if (($current_date->format('Y-m') > $old_delivery_date->format('Y-m'))
//                                     || ($current_date->format('Y-m') > $new_delivery_date->format('Y-m'))) {
//                                     $data['original_delivery_date'] = $old_delivery_date->format('Y-m-d');
//                                 }
                                if ((($current_date->format('Y-m') > $old_delivery_date->format('Y-m'))
                                        && ($current_date->format('Y-m') <= $new_delivery_date->format('Y-m')))
                                        || (($current_date->format('Y-m') > $new_delivery_date->format('Y-m'))
                                                && ($current_date->format('Y-m') <= $old_delivery_date->format('Y-m')))) {
                                                   $data['original_delivery_date'] = $old_delivery_date->format('Y-m-d');
                                }
//#End
                            }
                        }
//#6970: End
                        $conditions = array('order_id' => $post->order_id, 'branch_cd' => $item->od_branch_cd);

                        if (!$this->order_model->update_order_detail(array('data' => $data, 'conditions' => $conditions))) {
                            throw new Exception('update order detail error');
                        }

                        // Update disable log = 1 with order_detail_id
                        if (!$this->order_model->update_order_detail_log(array('data' => array('disable' => 1), 'conditions' => array('id' => $item->ol_id)))) {
                            throw new Exception('update order detail log error');
                        }

                        // paramms for template email
                        $mail_params = array(
                                'account_id'     => $order_apply_exits[0]['a_name'],
                                'client_name'    => $order_apply_exits[0]['c_name'],
                                'bu_name'        => $order_apply_exits[0]['bu_name'],
                                'j_code'         => $order_apply_exits[0]['o_j_code'],
                                'branch_cd'      => $item->od_branch_cd,
                                'mail_content'   => $mail_content
                        );

                        $send_mail_list = array();

                        //send mail to all
                        $authority_ids = $this->authority_model->get_authority_by_quick_setting('mail_change_report_approval_all');
                        if (! empty($authority_ids)) {
                            $params = array(
                                    'conditions' => array(
                                            'authority_ids' => implode(',', $authority_ids)
                                    ),
                                    'columns' => array('mail_address')
                            );

                            $send_mail_list = $this->account_model->get_accounts_by_parameters($params);
                        }

                        //send mail to self
                        $authority_ids = $this->authority_model->get_authority_by_quick_setting('mail_change_report_approval_self');
                        if (! empty($authority_ids)) {
                            $params = array(
                                    'conditions' => array(
                                            'authority_ids' => implode(',', $authority_ids),
                                            'ids' => implode(',', array($order_apply_exits[0]['o_j_account_id'], $item->od_change_report_applicant_account_id))
                                    ),
                                    'columns' => array('mail_address')
                            );

                            $send_mail_list = array_merge($send_mail_list, $this->account_model->get_accounts_by_parameters($params));
                        }

                        // send email to client
                        $mail_address_list = array();
                        foreach ($send_mail_list as $item) {
                            if ( ! in_array($item['mail_address'], $mail_address_list)) {
                                if (!$this->email_template->send_mail('change_report', array('system_to' => $item['mail_address']), $mail_params)) {
                                    $this->app_logger->error_log('CAN NOT SEND MAIL' . print_r($mail_params, true));
                                }
                                $mail_address_list[] = $item['mail_address'];
                            }
                        }
                     } //end foreach
                }
                $this->order_model->commit_transaction();
                echo $this->ajax_json(array('result' => true, 'error_msg' => 'success'));
            } catch(Exception $e) {

                $this->order_model->rollback_transaction();
                $this->app_logger->error_log($e->getMessage());
                echo $this->ajax_json(array('result' => false, 'error_msg' => 'error'));
            }
        }
    }

    /**
     * reject report
     *
     * @author thanh_trung
     * @since 2015-06-22
     */
    public function reject_report() {
        $this->load->model('order_model');
        $this->load->model('order_detail_model');

        $post   = array();
        $orders = array();
        $data   = array();

        if ($post = json_decode($this->input->raw_input_stream)) {

            // check order have to order_status = 1 and is_change_report_apply = 1
            $order_apply_exits = $this->order_model->check_change_report_apply($post->order_id);
            if (empty($order_apply_exits)) {
                echo $this->ajax_json(array('result' => false, 'error_msg' => lang('order apply not exist'), 'sql' => $this->db->last_query()));exit;
            }

            $orders_apply_log = $this->order_model->get_order_detail_log($post->order_id);

            try {
                $this->order_model->begin_transaction();

//              Update comment reject rp to order table
                if (!empty($post->reject_comment)) {

                    $data = array(
                        'comment' => $post->old_comment . "\r\n" . $post->reject_comment,
                        'lastup_account_id' => $this->auth->get_account_id(),
                        'lastup_datetime' => $this->order_model->_db_now()
                    );
                    $conditions = array('id' => $post->order_id);

                    if (!$this->order_model->update_reject_change_report(array('data' => $data, 'conditions' => $conditions))) {
                        throw new Exception('update error');
                    }
                }

                // delete records of table order_detail_log follow "order_id, branch_cd"
                foreach ($orders_apply_log as $item) {
                    if (!$this->order_model->update_order_detail_log(array('data' => array('disable' => 1), 'conditions' => array('id' => $item->ol_id)))) {
                        throw new Exception('update error');
                    }
                }

                // update flag order
                $update_data = array(
                        'order_status' => Order_model::UNDETERMINED,
                        'is_change_report_apply' => 0,
                        'is_cancel_request' => 0,
                        'shift_color_cd' => '',

                );
//#6983: Start Add more update condition is_change_report_apply only reject order detail that is request change report
                if (!$this->order_detail_model->update_order_detail(array('update_data' => $update_data, 'update_conditions' => array('order_id' => $post->order_id, 'is_change_report_apply' => 1)))) {
                    throw new Exception('update error');
                }
//#6983: End --> Van_Don
                // commit to db if 2 update success
                $this->order_model->commit_transaction();

                // success result
                echo $this->ajax_json(array('result' => true, 'error_msg' => 'success', 'xxx' => $this->db->last_query())); exit();
            } catch (Exception $e) {
                // rollback update if fail
                $this->order_model->rollback_transaction();

                $this->app_logger->error_log($e->getMessage());
                echo $this->ajax_json(array('result' => false, 'error_msg' => 'error', 'sql' => $this->db->last_query()));exit;
            }
        }

    }

    public function change_report() {
        $this->load->model('order_model');
        $data = json_decode($this->input->raw_input_stream);
        $this->order_model->change_report($data);
    }

    public function print_application() {
        $this->load->model('order_model');
        $this->load->model('account_model');
        $this->load->model('client_model');
        $this->load->library('tcpdf/Tcpdf.php');
        $this->load->library('tcpdf/fpdi.php');
        $this->load->helper('zenrin_pdf_helper');

        $can_edit = 0;
        if ($this->auth->has_permission('order_add_edit_his_only', 'column')) {
            $can_edit = 1; // self edit
        }
        if ($this->auth->has_permission('order_add_edit_all', 'column')) {
            $can_edit = 2; // edit all
        }

        if (!$can_edit) show_404();

        $id = $this->input->get('id');

        $order = $this->order_model->get_order_for_print_report($id);
        if (empty($order) || empty($order['details'])) show_404();

        $can_print = false;
        if ($can_edit == 1) {
            if ($this->auth->get_account_id() == $order['j_account_id']) $can_print = true;
        }

        if ($can_edit == 2) {
            $auth_business = $this->auth->get_business_unit();
            $j_account_business = $this->account_model->get_account_business_unit($order['j_account_id']);
            if ($auth_business[0]['id'] == $j_account_business[0]['id']) $can_print = true;
        }

        if (!$can_print) show_404();

        // $order_details = array();
        // for ($i = 1; $i <= 7; $i++) {
        //     $order_details[] = array (
        //         'product_name' => $i,
        //         'contents' => $i,
        //         'delivery_date' => $i,
        //         'billing_date' => $i,
        //         'payment_date' => $i,
        //         'quantity' => $i,
        //         'tax_type' => $i,
        //         'unit_price' => $i,
        //         'amount' => $i,
        //         'total_amount_without_tax' => $i,
        //         'total_tax_amount' => $i,
        //         'total_amount' => $i,
        //         'tax_name' => $i,
        //     );
        // }
        // $order['details'] = $order_details;

        $fpdi = new FPDI();

        // $fontPathRegular = APPPATH . 'libraries/tcpdf/fonts/MSGOTHIC.ttf' ;
        $regularFont     = 'msgothici';

        // $fontPathBold = APPPATH . 'libraries/tcpdf/fonts/MSGOTHIC.ttf' ;
        // $boldFont     = TCPDF_FONTS::addTTFfont($fontPathBold, '', '', 32);

        $fpdi->SetFont($regularFont, '', 11);
        $fpdi->setPrintHeader(false);
        $fpdi->setPrintFooter(false);
        $fpdi->SetAutoPageBreak(TRUE, 0);

        // initialize row
        $header_footer_total_row    = 12;
        $header_no_footer_total_row = 12;
        $body_footer_total_row      = 12;
        $body_no_footer_total_row   = 12;

        $fpdi->AddPage('L', 'A4');
        $fpdi->setSourceFile(APPPATH . 'config/pdf_template/order/header_footer.pdf');
        $y = 0;

        if (count($order['details']) > $header_footer_total_row) {
            $fpdi->setSourceFile(APPPATH . 'config/pdf_template/order/header.pdf');
            $y = 0;
        }

        $header_id = $fpdi->importPage(1);
        $fpdi->useTemplate($header_id, null, null, null, null, true);

        $page_estimator = page_estimator(count($order['details']), $header_footer_total_row, $header_no_footer_total_row, $body_footer_total_row, $body_no_footer_total_row);

        // for ($i = 1; $i <= $page_estimator['body']; $i++) {
        //     $fpdi->AddPage('L', 'A4');
        //     $fpdi->setSourceFile(APPPATH . 'config/pdf_template/order/template_detail_no_footer.pdf');
        //     $body_id = $fpdi->importPage(1);
        //     $fpdi->useTemplate($body_id, null, null, null, null, true);
        // }

        for ($i = 1; $i <= $page_estimator['footer']; $i++) {
            $fpdi->AddPage('L', 'A4');
            $fpdi->setSourceFile(APPPATH . 'config/pdf_template/order/footer.pdf');
            $footer_id = $fpdi->importPage(1);
            $fpdi->useTemplate($footer_id, null, null, null, null, true);
        }

        $fpdi->setPage(1);
        // title

        $fpdi->setXY(25.5, 17 + $y);
        $fpdi->setFontSize(27);

        $fpdi->cell(273, 5, '申　　込　　書', 0, '', 'C');
        // client_name
//#7581:Start
        $fpdi->setFontSize(20);
        $fpdi->setXY(54, 28.5 + $y);
        $fpdi->cell(163, 15.5, $this->client_model->name_with_title($order['client_name'], $order['corporate_status_before'], $order['corporate_status_after']), 0, '', 'C',false,false,true);
//#7581:End
        // client zipcode
        $fpdi->setXY(58, 44.5 + $y);
        $fpdi->setFontSize(9);
        $fpdi->cell(25, 1, substr_replace($order['zipcode'], '-', 3, 0), 0, '', 'L');
        // address
        $fpdi->setXY(53, 48.5 + $y);
        $fpdi->setFontSize(11);
        $fpdi->cell(176, 8, ($order['address_1st'] . $order['address_2nd']), 0, '', 'L');
        // tel
        $fpdi->setXY(65, 56.5 + $y);
        $fpdi->setFontSize(15);
        $fpdi->setCellPaddings(6);
        $fpdi->cell(70, 7, $order['phone_no'], 0, '', 'L');
        // fax
        $fpdi->setXY(146, 56.5 + $y);
        $fpdi->cell(89, 7, $order['fax_no'], 0, '', 'L');
        // division name
        $fpdi->setFontSize(11);
        $fpdi->setXY(53, 64 + $y);
        $fpdi->cell(179, 8, $order['division_name'], 0, '', 'L');
        // condition
        $fpdi->setFontSize(15);
        $fpdi->setXY(53, 83 + $y);
        $condition = '前受金';
        if (!$order['is_ad_receive_payment']) {
            $condition = $order['closing_date_name'] . '  日締め         ' . $order['payment_month_name'] . '  月         ' . $order['payment_date_name'] . '  日支払         ';
        }
        $fpdi->cell(180, 13.5, $condition, 0, '', 'C');
        // print applicate date
        $fpdi->SetFont($regularFont, 'B', 9);
        $fpdi->setCellPaddings(0);
        $fpdi->setXY(258, 28 + $y);
        $date_ = date('Y-m-d', strtotime($order['lastup_datetime']));
        $date = explode('-', $date_);
        $fpdi->cell(50, 0.5, $date[0] . '年 ' . $date[1] . '月 ' . $date[2] .'日', 0, '', 'C');
        // zenrin biz nexus
        $fpdi->setFontSize(12);
        $fpdi->setXY(239, 34.5 + $y);
        $fpdi->cell(60, 0.5, '株式会社ゼンリンビズネクサス', 0, '', 'C');
        // department zipcode
        $fpdi->setFontSize(9);
        $fpdi->setXY(242.5, 40 + $y);
        $fpdi->cell(45, 0.5, substr_replace($order['department_zipcode'], '-', 3, 0), 0, '', 'L');
        // department address 1
        $fpdi->setXY(239, 44.5 + $y);
        $fpdi->cell(45, 0.5, $order['department_address_1'], 0, '', 'L');
        // department address 2
        $fpdi->setXY(239, 48.5 + $y);
        $fpdi->cell(45, 0.5, $order['department_address_2'], 0, '', 'L');
        // department fax
        $fpdi->setXY(239, 52.5 + $y);
        $fpdi->cell(55, 0.5, 'TEL:' . $order['department_phone_no'] . '  FAX:' . $order['department_fax_no'], 0, '', 'L');
//#7650:Start

        $account_name_length = mb_strlen($order['account_name']);

        if ($account_name_length <= 10)
            $yPos = 97;
        elseif ($account_name_length > 10 && $account_name_length <= 20)
            $yPos = 95;
        else
            $yPos = 93.5;

        //$fpdi->setXY(257, 94.5 + $y);
        $fpdi->setXY(257, $yPos + $y);
//#7650:End
        // MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])
        $fpdi->MultiCell(18, 0.5, $order['account_name'], "0" , 'C', false);

        $fpdi->SetFont($regularFont, '', 12);
        $fpdi->setXY(273, 68 + $y);
        $fpdi->cell(25.5, 7.5, $order['j_code'], 0, '', 'C');
        // branch_cd
        $fpdi->setXY(273, 75 + $y);
        $fpdi->cell(25.5, 7.5, $order['s_code'], 0, '', 'C');

        // initialze break page coordination
        $detail_y = 0;
        $start_detail_coor_y = 117.5;
        $page = 2;
        $row_height = 6;
        if (count($order['details']) > 12) {
            $row_height = 8.8;
        }
        foreach ($order['details'] as $row => $detail) {
            // row number
//#7650:Start
            //$fpdi->setFontSize(9);
            $fpdi->setFontSize(11.5);
//#7650:End
            $content_coor_y = $start_detail_coor_y + $y + $detail_y;
            $fpdi->setXY(26, $content_coor_y);
            // $fpdi->cell(11, $row_height, $content_coor_y, 0, '', 'C');
            $fpdi->cell(11, $row_height, sprintf('%02d', $row + 1), 0, '', 'C');
            // product name
//#8532:Start
            // $fpdi->MultiCell(38, $row_height, $detail['product_name'], 0, 'C', false, 1, 37, $content_coor_y, true, 0, false,'', $row_height, 'M', true);
            $fpdi->cell(38, $row_height, $detail['product_name'], 0, '', 'C', false, '', 1);
//#8532:End
            // content
            $fpdi->MultiCell(81, $row_height, mb_substr($detail['contents'], 0, 30, 'UTF-8'), 0, 'C', false, 1, 75, $content_coor_y, true, 0, false,'', $row_height, 'M', true);
            // delivery date
            $fpdi->setXY(156.5, $content_coor_y);
            $fpdi->cell(22, $row_height, str_replace('-', '/', $detail['delivery_date']), 0, '', 'C');
            // billing date
            $fpdi->setXY(178.5, $content_coor_y);
            $fpdi->cell(21.5, $row_height, str_replace('-', '/', $detail['billing_date']), 0, '', 'C');
            // payment date
//#7650:Start
            //$fpdi->setXY(200, $content_coor_y);
            $fpdi->setXY(200.5, $content_coor_y);
//#7650:End
            $fpdi->cell(21.5, $row_height, str_replace('-', '/', $detail['payment_date']), 0, '', 'C');
            // quantity
//#7650:Start
            $fpdi->setFontSize(13);
//#7650:End
            $fpdi->setXY(222.5, $content_coor_y);
//#8258:Start
//             $fpdi->cell(16, $row_height, number_format($detail['quantity']), 0, '', 'C');
            $fpdi->cell(16, $row_height, number_format($detail['quantity']), 0, '', 'C',false,true,true);
//#8258:End
            // tax name
//#7650:Start
            $fpdi->setFontSize(11.5);
//#7650:End
            $fpdi->setXY(238.5, $content_coor_y);
            $fpdi->cell(10.5, $row_height, $detail['tax_name'], 0, '', 'C');
//#7650:Start
            $fpdi->setFontSize(13);
//#7650:End
            // unit price
            $fpdi->setXY(250.5, $content_coor_y);
            $fpdi->cell(21.5, $row_height, number_format($detail['unit_price'], 2), 0, '', 'R', false,'',1);
            // amount
            $fpdi->setXY(272, $content_coor_y);
            $fpdi->cell(32, $row_height, number_format($detail['amount']), 0, '', 'R');

            if (count($order['details']) > 12) {
                if ($page > 2) {
                    $detail_y += $row_height - 0.75;
                } else {
                    $detail_y += $row_height - 0.4;
                }
            } else {
                $detail_y += $row_height - 0.08;
            }

            if (count($order['details']) > 12 && $row == 11) {
                $break_row = 11;
            } else {
                $break_row = false;
            }
            if ($break_row) {
                $start_detail_coor_y = 60;
                $row_height = 8.8;
                $detail_y = 0;
                $fpdi->setPage($page);
                $page++;
            }
        }

        // initialize footer coordination
        $footer_coor_y = 11;
        if (count($order['details']) > $header_footer_total_row) {
            $footer_coor_y = 11;
        }
        // total amount without tax
        $fpdi->setPage($page_estimator['total']);
//7650:Start
        //$fpdi->setFontSize(9);
        $fpdi->setFontSize(13);
//7650:End

        $remark_y = 189;
        $cost_total_price_y = 178;
        $consumption_tax_y = 189;
        $total_amount_tax_y = 199.5;
        if ($page >= 3) {
            $remark_y = 158;
            $cost_total_price_y = 147;
            $consumption_tax_y =  157;
            $total_amount_tax_y = 169;

            $fpdi->SetFont($regularFont, '', 12);
            $fpdi->setXY(273, 26);
            $fpdi->cell(25.5, 7.5, $order['j_code'], 0, '', 'C');
            // branch_cd
            $fpdi->setXY(273, 33);
            $fpdi->cell(25.5, 7.5, $order['s_code'], 0, '', 'C');
        }

        $fpdi->setXY(271.5, $cost_total_price_y + $y + $footer_coor_y);
        $fpdi->cell(32, 10.8, number_format($order['cost_total_price']), 0, '', 'R');
        // total tax amount
        $fpdi->setXY(271.5, $consumption_tax_y + $y + $footer_coor_y);
        $fpdi->cell(32, 10.8, number_format($order['consumption_tax']), 0, '', 'R');
        // total amount
        $fpdi->setXY(271.5, $total_amount_tax_y + $y + $footer_coor_y);
//#8216:Start
//        $fpdi->cell(32, 10.8, number_format($order['total_amount']), 0, '', 'R');
        $fpdi->cell(32, 10.8, number_format($order['total_amount_tax']), 0, '', 'R');
//#8216:End



        // $fpdi->writeHTMLCell(223, 32, 26, 189.5, nl2br($order['remark']), 0, '', false, '', 'L', true);
        $fpdi->MultiCell(223, 32, $order['remark'], 0, '', false, 1, 26, $remark_y, true, 0, false,'', $row_height, 'T', true);
        // $fpdi->setXY(26, 189);
        // $fpdi->cell(2233, 32, $order['remark']);

        // page number
        if ($page_estimator['total'] > 1) {
            for ($i = 1; $i <= $page_estimator['total']; $i++) {
                $fpdi->setPage($i);
                $fpdi->setFontSize(9);
                $fpdi->SetXY(25, 225);
                $page_numer = $i . '/' . $page_estimator['total'];
                $fpdi->cell(273, 5, $page_numer.' ページ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            }
        }

        $fpdi->Output();
    }

    public function cancel_apply_order() {
        $this->load->model('order_model');
        $data = json_decode($this->input->raw_input_stream);

        // update order status = 9 (order detail) and comment (order)
        try {

            if (!$this->order_model->cancel_apply($data->order_id, $data->comment)) {
                throw new Exception('Cannot update status cancel apply');
            }
            echo $this->ajax_json(array('result' => true, 'error_msg' => 'success'));exit;
        } catch (Exception $e) {
            echo 'Message: '.$e->getMessage();
        }
    }

    public function delete_branch() {
        $this->load->model('order_model');
        $data = json_decode($this->input->raw_input_stream);
        $this->order_model->delete_branch($data->order_id, $data->branch);
    }

    /**
     * Count order which has status = 1 (unavailable)
     */
    public function count_order_unavailable() {
        $input = json_decode($this->input->raw_input_stream);
        $this->load->model('order_model');
        $result = $this->order_model->count_order_unavailable($input->client_id);
        echo $this->ajax_json($result);
        exit;
    }

    /**
     * Delivery Statement
     * @throws Exception
     * @author cong_tien
     */
    public function delivery_statement() {
        $date_from = $date_to = NULL;

        $this->load->model(array('order_acceptance_model', 'closing_accountant_model', 'consumption_tax_model', 'sales_slip_model'));

        if ($this->input->post()) {
            try {
                $date_from = str_replace('-', '/', $this->input->post('date_from'));
                $date_to = str_replace('-', '/', $this->input->post('date_to'));

                if (!$this->_check_date($date_from, $date_to)) {
                    throw new Exception('検索範囲が正しくありません。');
                }

                $this->session->set_flashdata('delivery_statement_date', array('date_from' => $date_from, 'date_to' => $date_to));

                $date_from_s = explode('/', $date_from); // year is element 0, month is element 1
                $date_to_s = explode('/', $date_to);

                $date_search = array();

                $ts = strtotime("{$date_from_s[0]}-{$date_from_s[1]}-01 -1 months");
                $date_search['from'] = date('Y-m', $ts);

                if ($date_to) {
                    $ts = strtotime("{$date_to_s[0]}-{$date_to_s[1]}-01 +1 months");
                    $date_search['to'] = date('Y-m', $ts);
                }

                // get orders for download
                $orders = $this->order_acceptance_model->get_order_for_download($date_search);

//#7117:Start
                $data_csv = array();
//#7117:End

                if (!empty($orders)) {
//#7117:Start
                    /*
                    // CSV header
                    $data_csv = array(
                        array(
                            1 => "受注情報\n受注日",
                            2 => "受注情報\n事業部名",
                            3 => "受注情報\n部署名",
                            4 => "受注情報\n営業担当",
                            5 => "受注情報\nＪコード",
                            6 => "受注情報\n枝",
                            7 => "受注情報\nＳコード",
                            8 => "受注情報\nクライアント社名",
                            9 => "受注情報\n商品名",
                            10 => "受注情報\n内容",
                            11 => "受注情報\n納期",
                            12 => "受注情報\n請求日",
                            13 => "受注情報\n支払日",
                            14 => "受注情報\n数量",
                            15 => "受注情報\n単価",
                            16 => "受注情報\n合計",
                            17 => "受注情報\n原価単価",
                            18 => "受注情報\n原価合計",
                            19 => "受注情報\n粗利合計",
                            20 => "受注情報\n粗利率",
                            21 => "売上伝票\n前受金請求金額（税別）",
                            22 => "売上伝票\n確定請求金額（税別）",
                            23 => "検収原価\n非課税",
                            24 => "検収原価\n税込発送料",
                            25 => "検収原価\n税込切手代",
                            26 => "検収原価\n税込その他",
                            27 => "検収原価\n税別発送料",
                            28 => "検収原価\n税別その他",
                            29 => "検収原価\n確定原価（税別、非課税）",
                            30 => "\n確定粗利合計",
                            31 => "\n確定粗利率"
                        )
                    );
                    */
//#7117:End

                    foreach ($orders as $order) {
                        if ($order['order_detail_tax_type'] == TAX_INCLUDED) {
                            if (!empty($order['consumption_tax_rate'])) {
//                                $consumption_tax_value = 1 + ($order['consumption_tax_rate'] / 100);
                                $consumption_tax_value = bcadd(1, bcdiv($order['consumption_tax_rate'], 100));

                                $order['unit_price'] = round(($order['unit_price'] / $consumption_tax_value), 2, PHP_ROUND_HALF_UP);
//                                $order['amount'] = f_ceil($order['amount'] / $consumption_tax_value);
                                $order['amount'] = f_ceil(bcdiv($order['amount'], $consumption_tax_value));
                            }
                        }

                        $total_profits = $determine_rate = '';
                        if (!empty($order['slip_amount_total_confirmed_invoice'])) {
                            // 30. 確定粗利合計
                            $total_profits = $order['slip_amount_total_confirmed_invoice'] - $order['amount_sum'];
                            // 31. 確定粗利率
                            $determine_rate = ($total_profits / $order['slip_amount_total_confirmed_invoice'] * 100);
                        }

//#7117:Start
                        /*
                        // retain determine_rate 2 decimals
                        if (strpos($determine_rate, '.') !== false) {
                            $determine_rates = explode('.', $determine_rate);
                            $determine_rate = $determine_rates[0] . '.' . substr($determine_rates[1], 0, 2);
                            //-11620.04638
                        }
                        */
                        if (!empty($determine_rate)) {
                            $determine_rate = round($determine_rate, 2);
                        }
//#7117:End

                        $data_csv[] = array(
//#7631:Start
//                            1 => $order['order_regist_date'],
                            1 => $order['order_approval_date'],
//#7631:End
                            2 => $order['business_unit_name'],
                            3 => $order['department_name'],
                            4 => $order['account_name'],
                            5 => $order['j_code'],
                            6 => $order['branch_cd'],
                            7 => $order['s_code'],
                            8 => $order['client_name'],
                            9 => $order['product_name'],
                            10 => $order['contents'],
                            11 => $order['delivery_date'],
                            12 => $order['billing_date'],
                            13 => $order['payment_date'],
                            14 => $order['quantity'],
                            15 => $order['unit_price'],
                            16 => $order['amount'],
                            17 => $order['cost_unit_price'],
                            18 => $order['cost_total_price'],
                            19 => $order['gross_profit_amount'],
                            20 => $order['gross_profit_rate'],
                            21 => $order['slip_amount_total_excluding_tax'],
                            22 => $order['slip_amount_total_confirmed_invoice'],
                            23 => $order['tax_free'],
                            24 => $order['shipping_charge_tax'],
                            25 => $order['stamp_tax'],
                            26 => $order['etc_tax'],
                            27 => $order['shipping_charge'],
                            28 => $order['etc'],
                            29 => $order['amount_sum'],
                            30 => $total_profits,
                            31 => $determine_rate
                        );
                    }
//#7117:Start
                }
                /*
                    // file name
                    $file_name = '納品表_' . date('Ymd') . '.csv';

                    $this->_array_to_csv_download($file_name, $data_csv);
                } else {
                    $this->set_message(lang('common_msg_no_data'), 'warning');
                }
                */
//#7117:End
                // get order acceptances
                $order_acceptances = $this->order_acceptance_model->get_order_acceptances_download($date_search);

                if (!empty($order_acceptances)) {
                    foreach ($order_acceptances as $order_acceptance) {
                        $data_csv[] = array(
                            1 => '',    // regist_date
                            2 => $order_acceptance['business_unit_name'],
                            3 => $order_acceptance['department_name'],
                            4 => $order_acceptance['account_name'],
                            5 => '',    // j_code
                            6 => '',    // branch_cd
                            7 => '',    // s_code
                            8 => $order_acceptance['client_name'],
                            9 => '',    // product_name
                            10 => '',   // content
                            11 => '',   // delivery_date
                            12 => '',   // billing_date
                            13 => '',   // payment_date
                            14 => '',   // quantity
                            15 => '',   // unit_price
                            16 => '',   // amount
                            17 => '',   // cost_unit_price
                            18 => '',   // cost_total_price
                            19 => '',   // gross_profit_amount
                            20 => '',   // gross_profit_rate
                            21 => '',
                            22 => 0,
                            23 => $order_acceptance['tax_free'],
                            24 => $order_acceptance['shipping_charge_tax'],
                            25 => $order_acceptance['stamp_tax'],
                            26 => $order_acceptance['etc_tax'],
                            27 => $order_acceptance['shipping_charge'],
                            28 => $order_acceptance['etc'],
                            29 => $order_acceptance['amount'],
                            30 => (is_numeric($order_acceptance['amount'])) ? -$order_acceptance['amount'] : '',
                            31 => ''
                        );
                    }
                }
//#7117:Start

                if (!empty($data_csv)) {
                    // CSV header
                    $data_csv_header = array(
                        array(
//#7631:Start
//                            1 => "受注情報\n受注日",
                            1 => "受注情報\n受注承認日",
//#7631:End
                            2 => "受注情報\n事業部名",
                            3 => "受注情報\n部署名",
                            4 => "受注情報\n営業担当",
                            5 => "受注情報\nＪコード",
                            6 => "受注情報\n枝",
                            7 => "受注情報\nＳコード",
                            8 => "受注情報\nクライアント社名",
                            9 => "受注情報\n商品名",
                            10 => "受注情報\n内容",
                            11 => "受注情報\n納期",
                            12 => "受注情報\n請求日",
                            13 => "受注情報\n支払日",
                            14 => "受注情報\n数量",
                            15 => "受注情報\n単価",
                            16 => "受注情報\n合計",
                            17 => "受注情報\n原価単価",
                            18 => "受注情報\n原価合計",
                            19 => "受注情報\n粗利合計",
                            20 => "受注情報\n粗利率",
                            21 => "売上伝票\n前受金請求金額（税別）",
                            22 => "売上伝票\n確定請求金額（税別）",
                            23 => "検収原価\n非課税",
                            24 => "検収原価\n税込発送料",
                            25 => "検収原価\n税込切手代",
                            26 => "検収原価\n税込その他",
                            27 => "検収原価\n税別発送料",
                            28 => "検収原価\n税別その他",
                            29 => "検収原価\n確定原価（税別、非課税）",
                            30 => "\n確定粗利合計",
                            31 => "\n確定粗利率"
                        )
                    );

                    $data_csv = array_merge($data_csv_header, $data_csv);

                    // file name
                    $file_name = '納品表_' . date('Ymd') . '.csv';

                    $this->_array_to_csv_download($file_name, $data_csv);
                } else {
                    $this->set_message(lang('common_msg_no_data'), 'warning');
                }
//#7117:End
            } catch (Exception $exc) {
                $this->set_message($exc->getMessage());

                $date_from = ($this->input->post('date_from')) ? $this->input->post('date_from') : '';
                $date_to = ($this->input->post('date_to')) ? $this->input->post('date_to') : '';
            }
        } else {
            // Get closing accountant
            $options = array(
                'fields' => 'id, close_date',
                'conditions' => array('DATE_FORMAT(close_date, "%Y-%m") = ' => date('Y-m', strtotime("-1 months")))
            );

            if ($closing_accountant = $this->closing_accountant_model->get_closing_accountants('first', $options)) {
                $date_from = $date_to = date('Y/m');
            } else {
                $date_from = $date_to = date('Y/m', strtotime("-1 months"));
            }
        }

        $this->assign('date_from', $date_from);
        $this->assign('date_to', $date_to);

        $this->check_message();

        $this->view('order/delivery_statement.tpl');
    }

    private function _check_date($date_from, $date_to) {
        try {
            // empty From
            if (!$date_from) {
                throw new Exception('from is empty');
            }

            // invalid From
            if (strpos($date_from, '/') == false || ($date_to && strpos($date_to, '/') == false)) {
                throw new Exception('invalid date /');
            }

            $date_from_s = explode('/', $date_from); // year is element 0, month is element 1
            $date_to_s = explode('/', $date_to);

            if (count($date_from_s) > 2 || count($date_to_s) > 2) {
                throw new Exception('date not format');
            }

            if (!is_numeric($date_from_s[0]) || !is_numeric($date_from_s[1]) || ($date_to && (!is_numeric($date_to_s[0]) || !is_numeric($date_to_s[1])))) {
                throw new Exception('date not is number');
            }

            $date = new DateTime($date_from_s[0] . '-' . $date_from_s[1] . '-01');
            if (!$date->format('Y/m')) {
                throw new Exception('Part is not format');
            }

            if ($date_to) {
                $date = new DateTime($date_to_s[0] . '-' . $date_to_s[1] . '-01');
                if (!$date->format('Y/m')) {
                    throw new Exception('Part is not format');
                }
            }

            // check date
            if (!checkdate($date_from_s[1], 1, $date_from_s[0]) || ($date_to && !checkdate($date_to_s[1], 1, $date_to_s[0]))) {
                throw new Exception('date incorrect');
            }
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * CSV download
     * @param string $filename
     * @param array $data
     * @author cong_tien
     */
    private function _array_to_csv_download($filename = 'export.csv', $data) {
        $filename = filename_to_urlencode($filename);

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        header('Expires: 0');
        header('Content-Transfer-Encoding: binary');
        header('Pragma: no-cache');
        echo "\xEF\xBB\xBF"; // UTF-8 BOM

        // open the "output" stream
        $f = fopen('php://output', 'w');

        foreach ($data as $line) {
            fputcsv($f, $line);
        }

        fclose($f);
        exit;
    }

    /**
     * Calculate get rate consumption_tax
     * @param date $delivery_date
     * @param array $tax_rates
     * @return int
     * @author cong_tien
     */
    private function _cal_get_consumption_tax($delivery_date, array $tax_rates) {
        foreach ($tax_rates as $tax_rate) {
            if (!empty($tax_rate['adaptation_date']) && (strtotime($delivery_date) >= strtotime($tax_rate['adaptation_date']))) {
                return $tax_rate['rate'];
            }
        }

        return 0;
    }

    /**
     * Ajax check date of Delivery Statement
     * @author cong_tien
     */
    public function ajax_convert_date() {
        if ($this->input->is_ajax_request()) {
            $date_from = str_replace('-', '/', $this->input->post('date_from'));
            $date_to = str_replace('-', '/', $this->input->post('date_to'));

            if (!$this->_check_date($date_from, $date_to)) {
                $this->ajax_json(array('status' => 'error', 'msg' => '検索範囲が正しくありません。'));
            }

            $date_from_s = explode('/', $date_from); // year is element 0, month is element 1
            $date_to_s = explode('/', $date_to);

            $date_from = date('Y/m', strtotime("{$date_from_s[0]}-{$date_from_s[1]}-01"));
            $date_to = (!empty($date_to['date_to'])) ? date('Y/m', strtotime("{$date_to_s[0]}-{$date_to_s[1]}-01")) : '';

            // check to > from ?
            if ((!empty($date_to['date_to'])) && (strtotime("{$date_from_s[0]}-{$date_from_s[1]}-01") > strtotime("{$date_to_s[0]}-{$date_to_s[1]}-01"))) {
                $this->ajax_json(array('status' => 'error', 'msg' => '検索範囲が正しくありません。', 'date' => array('date_from' => $date_from, 'date_to' => $date_to)));
            }

            $this->ajax_json(array('status' => 'success', 'date' => array('date_from' => $date_from, 'date_to' => $date_to)));
        } else {
            show_404();
        }
    }

    /**
     * Ajax approve multiple orders
     *
     * @param json
     * @return json
     *
     * @author hoang_minh
     * @since 2015-08-17
     */
    public function approve_multiple_orders() {
        //load library
        $this->load->library('email_template');

        //load model
        $this->load->model('order_model');
        $this->load->model('client_model');
        $this->load->model('account_model');
        $this->load->model('authority_model');
        $this->load->model('authority_model');
        $this->load->model('consumption_tax_model');
        $this->load->model('order_detail_model');

        //init variable
        $post = array();

        if ($post = json_decode($this->input->raw_input_stream)) {
            $order_ids = $post->order_ids;
            $remark = $post->remark;
            $comment = $post->comment;

            //check order's information
            $orders = $this->order_model->get_detail(implode("','", $order_ids));
            $order_id_from_datas = array();
            $order_datas = array();

            foreach ($orders as $item) {
                if ( ! in_array($item['o_id'], $order_id_from_datas)) {
                    $order_id_from_datas[] = $item['o_id'];
                }
                $order_datas[$item['o_id']][] = $item;
            }

            //order_id to update
            foreach ($order_ids as $item) {
                if ( ! in_array($item, $order_id_from_datas)) {
                    echo $this->ajax_json(array('result' => false, 'error_msg' => lang(''))); exit();
                }
            }

            //processing approve
            try {
                $this->order_model->begin_transaction();

                //update order
                $update_data = array();

                foreach ($order_ids as $item) {
                    $update_data[] = array(
                            'id' => $item,
                            'order_approval_account_id' => $this->auth->get_account_id(),
                            'order_approval_date' => $this->order_model->_db_now(),
                            '`comment`' => isset($post->comment) ? $post->comment : '',
                            'lastup_account_id' => $this->auth->get_account_id(),
                            'lastup_datetime' => $this->order_model->_db_now(),
                    );
                }

                if ( ! $this->order_model->update_multiple_orders(array('update_data' => $update_data, 'update_key' => 'id'))) {
                    throw new Exception(lang('update_order_fail'));
                }

                //update status for order detail
                $update_data = array(
                        'order_status' => Order_model::UNDETERMINED,
                        'lastup_account_id' => $this->auth->get_account_id(),
                        'lastup_datetime' => $this->order_model->_db_now(),
                );

                $update_conditions = array(
                        'order_id' => implode("','", $order_ids),
                        'order_status' => Order_model::UNAPPROVED
                );

                if ( ! $this->order_detail_model->update_multiple_order_details(array('update_data' => $update_data, 'update_conditions' => $update_conditions))) {
                    throw new Exception(lang('update_order_detail_fail'));
                }

                //send mail
                $authority_ids = $this->authority_model->get_authority_by_quick_setting('mail_order_recorded');
                $params = array(
                        'conditions' => array(
                                'authority_ids' => implode(',', $authority_ids)
                        ),
                        'columns' => array('mail_address')
                );

                $send_mail_list = $this->account_model->get_accounts_by_parameters($params);

                //get tax rate
                $get_tax_rate_params = array(
                        'columns' => array('`consumption_tax`.rate, `consumption_tax_adaptation`.adaptation_date'),
                        'conditions' => array(
                                'consumption_tax_type_id' => CONSUMPTION_TAX
                        ),
                        'orders' => array(
                                '`consumption_tax_adaptation`.adaptation_date' => 'DESC'
                        )
                );

                $tax_rate_list = $this->consumption_tax_model->get_tax_rate_by_conditions($get_tax_rate_params);
                foreach ($order_datas as $order_detail_datas) {

                    $order_detail_mail_content = array();
                    $total_cost_total_price = 0;
                    $total_gross_profit_amount = 0;

                    foreach ($order_detail_datas as $item) {
                        $order_detail_mail_content[] = $item['od_branch_cd'] . ' / '
                                                     . $item['p_name'] . ' / '
                                                     . $item['od_contents'] . ' / '
                                                     . $item['od_delivery_date'] . ' / '
                                                     . number_format($item['od_amount']) . ' / '
                                                     . number_format($item['od_gross_profit_amount']) . ' / '
                                                     . $item['od_gross_profit_rate'] . '%';

                        switch ($item['od_tax_type']) {
                            case TAX_NON:
                            case TAX_EXCLUSIVE:
//                                $total_cost_total_price += $item['od_amount'];
                                $total_cost_total_price = bcadd($total_cost_total_price, $item['od_amount']);
                                break;

                            case TAX_INCLUDED:
                                $tax_rate = 0;
                                if ($tax_rate_list) {
                                    foreach ($tax_rate_list as $txr) {
                                        if (strtotime($item['od_delivery_date']) >= strtotime($txr['adaptation_date'])) {
                                            $tax_rate = $txr['rate'];
                                            break;
                                        }
                                    }
                                }

//                                $total_cost_total_price += ($item['od_amount'] / (1 + $tax_rate / 100));
                                $total_cost_total_price = bcadd($total_cost_total_price, bcdiv($item['od_amount'], bcadd(1, bcdiv($tax_rate, 100))));
                                break;

                            default:
                                break;
                        }

                        $total_gross_profit_amount += $item['od_gross_profit_amount'];
                    }

                    $total_cost_total_price = f_ceil($total_cost_total_price);

                    //prepair mail content
                    $replace_params = array(
                            'charge_name' => $order_detail_datas[0]['a_name'],
                            'business_unit_name' => $order_detail_datas[0]['bu_name'],
                            'department_name' => $order_detail_datas[0]['d_name'],
                            'j_account_id' => $order_detail_datas[0]['a_name'],
                            'client_name' => $order_detail_datas[0]['c_name'],
                            'j_code' => $order_detail_datas[0]['o_j_code'],
                            'order_detail_mail_content' => implode("\r\n", $order_detail_mail_content),
                            'total_cost_total_price' => number_format($total_cost_total_price),
                            'total_gross_profit_amount' => number_format($total_gross_profit_amount),
                            'total_gross_profit_rate' => ($total_cost_total_price != 0) ? number_format(($total_gross_profit_amount / $total_cost_total_price * 100), 2) : 0
                    );

                    foreach ($send_mail_list as $item) {
                        if ( ! $this->email_template->send_mail('order_approve_reject', array('system_to' => $item['mail_address']), $replace_params)) {
                            throw new Exception(lang('send_mail_approve_order_fail'));
                        }
                    }
                }

                $this->order_model->commit_transaction();
                echo $this->ajax_json(array('result' => true, 'error_msg' => lang('approve_order_success'))); exit();

            } catch (Exception $ex) {

                $this->order_model->rollback_transaction();
                $this->app_logger->error_log($ex->getMessage());

                echo $this->ajax_json(array('result' => false, 'error_msg' => lang('approve_order_fail'))); exit();
            }

        }

        echo $this->ajax_json(array('result' => false, 'error_msg' => lang('approve_order_fail'))); exit();
    }

    /**
     * Ajax get list clients
     *
     * @param json
     * @return json
     *
     * @author hoang_minh
     * @since 2015-08-24
     */
    public function ajax_get_clients() {

        $this->load->model('order_model');
//#9763:Start
        $this->load->model('closing_accountant_model');
        $this->load->model('reconcile_amount_model');
//#9763:End

        $client_minimum_data = true;

        if ($post = json_decode($this->input->raw_input_stream)) {

            $params = array();
            $params['conditions'] = array();

            if (isset($post->s_code) && ! empty($post->s_code)) {
                $params['conditions']['s_code'] = $post->s_code;
            }

            if (isset($post->client_name) && ! empty($post->client_name)) {
                $params['conditions']['name'] = $post->client_name;
            }

            if (isset($post->client_id) && ! empty($post->client_id)) {
                $params['conditions']['id'] = $post->client_id;
                $client_minimum_data = false;
            }
//#7427:Start
            $params['conditions']['status'] = array(
                    CLIENT_STATUS_CL_APPROVED,
                    CLIENT_STATUS_CREDIT_EVERYTIME
            );
//#7427:End
            $clients = $this->order_model->get_clients($params, $client_minimum_data);
//#9763:Start
            // $closing_month_recent = $this->closing_accountant_model->get_closing_month_recent();
            // $close_date = $closing_month_recent[0]['close_date'];

            // get bugyo_next_month_receive (tiền nợ của tháng tiếp theo (table payment_entry))
            if (isset($clients[0]['id'])) {
                $payment_entry_result = $this->reconcile_amount_model->get_payment_entry_follow_client($clients[0]['id']);
                $bugyo_next_month_receive = $payment_entry_result['bugyo_next_month_receive']; //16172654

                $payment_result = $this->reconcile_amount_model->get_amount_payment_follow_client($clients[0]['id']);
                $money_received = 0;
                if (!empty($payment_result)) {
                    foreach ($payment_result as $row) {
                        $money_received += $row['amount'];
                    }
                }
                $client_debt = $bugyo_next_month_receive - $money_received;
                $client_debt = ($client_debt > 0) ? $client_debt : 0;                
            } else {
                $client_debt = 0;
            }

//#9763:End
            echo $this->ajax_json(array('result' => true, 'data' => $clients, 'client_debt' => $client_debt)); exit();
        }

        echo $this->ajax_json(array('result' => false, 'error_msg' => '')); exit();
    }
}
