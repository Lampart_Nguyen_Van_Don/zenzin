<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @author cam_tien
 *
 */

class Order_acceptance extends ZR_Controller {
    public function import() {
        $this->load->model('account_model');
        $this->load->model('order_acceptance_model');
        $this->load->model('order_model');

        if ($this->input->method() == 'post') {
            $order_acceptances = json_decode($this->input->post('order_acceptances'), true);

//#7090:Start

            $returndata = $this->order_acceptance_model->import_data($order_acceptances);
            // $this->update_flag_status_acc($order_acceptances, IS_INSERT_ORDERACCEPTANCE);
            $status = $returndata['status'];
            if ($status){
                $data_import_oc = array(
                        'listid_import' => $returndata['listid_import']
                );
                $this->session->set_userdata('data_import_oc',$data_import_oc);
                redirect('/_admin/order_acceptance/show?import_refresh=1');
//#7090:Start

            }
            $this->assign('upload_status', $status);
        }

        $this->view('order_acceptance/import.tpl');
    }

    public function download_template() {
        $this->load->helper('download');
        force_download(IMPORT_ORDER_ACCEPTANCE_TEMPLATE_PATH, null);
    }

// use csv file as import file
//#6555:Start
    public function csv_to_array($filename = '', $delimiter = ',', $return_header = false) {
        if (!file_exists($filename) || !is_readable($filename)) return FALSE;

        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
                // ignore empty row
                if (array(null) === $row) continue;

                if($return_header) {
                    return $row;
                } elseif (!$header) {
                    $header = $row;
                } else {
                    $data[] = $row;
                }
            }
            fclose($handle);
        }
        return $data;
    }
//#6555:End

    public function ajax_import_excel() {
        ini_set("memory_limit","1024M");
// use csv file as import file
//#6555:Start
        // setlocale(LC_ALL, 'ja_JP.UTF-8');
//#6555:End
        if ($this->input->is_ajax_request()) {
            $this->load->model('subcontractor_model');
            $this->load->model('shipping_type_model');
            $this->load->model('gui_parts_element_model');
            $this->load->model('order_acceptance_model');
            $this->load->model('consumption_tax_model');
            $this->load->model('account_model');
            $this->load->model('order_acceptance_model');
            $this->load->model('closing_accountant_model');
            $this->load->library('PHPExcel/PHPExcel');

// use csv file as import file
//#6555:Start
            // session_write_close();
            // // if file size > 10M, then $file is empty
            // if (!isset($_FILES['excel_file'])) {
            //     echo json_encode(array(
            //         'status' => false,
            //         'messages' => lang('lbl_over_size'),
            //     ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
            //     return;
            // }

            // $file      = $_FILES['excel_file']['tmp_name'];
            // $file_name = $_FILES['excel_file']['name'];

            // // if file size > 10M, then $file is empty
            // if (empty($file)) {
            //     echo json_encode(array(
            //         'status'   => false,
            //         'messages' => lang('lbl_over_size'),
            //     ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
            //     return;
            // }

            // $arrayZips = array("text/plain");
            // $arrayExtensions = array(".csv");
            // $original_extension = (false === $pos = strrpos($file_name, '.')) ? '' : substr($file_name, $pos);
            // $finfo = new finfo(FILEINFO_MIME_TYPE);
            // $type = $finfo->file($file);
            // $valid_file = true;
            // if (!in_array($type, $arrayZips) || !in_array($original_extension, $arrayExtensions)) {
            //     $valid_file = false;
            // }

            // if (!$valid_file) {
            //     echo json_encode(array(
            //         'status'   => false,
            //         'messages' => lang('lbl_unsupported_file'),
            //     ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
            //     return;
            // }

            // // check valid format
            // $header_name = array('Ｊコード','枝','Ｊ担当営業','Ｊ担当営業ID','手配担当','手配担当ID','クライアント社名','請求元ブレーン','請求元ブレーンコード','作業ブレーン','作業ブレーンコード','発送ブレーン','発送ブレーンコード','発送種別','発送種別コード','セット名','単価','件数','発送日','円口','重量','発送物形状','発送物形状コード','計上月','非課税','税込発送料','税込切手代','税込その他','税別発送料','税別その他');
            // $sheet_datas = $this->csv_to_array($file, ',', true);
            // for ($i = 0; $i < 30; $i++) {
            //     if (!isset($sheet_datas[$i])) {
            //         echo json_encode(array(
            //             'status'   => false,
            //             'messages' => lang('lbl_invalid_format'),
            //         ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
            //         return;
            //     }
            //     if (isset($sheet_datas[$i]) && $sheet_datas[$i] != $header_name[$i]) {
            //         echo json_encode(array(
            //             'status'   => false,
            //             'messages' => lang('lbl_invalid_format'),
            //         ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
            //         return;
            //     }
            // }

            // $sheet_datas = $this->csv_to_array($file);

            // if (empty($sheet_datas)) {
            //     echo json_encode(array(
            //         'status'   => false,
            //         'messages' => lang('lbl_empty_file'),
            //     ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
            //     return;
            // }

            // $datas_import = $this->order_acceptance_model->import_excel($sheet_datas);
            // // $this->assign('datas_import', $datas_import);
            // echo json_encode(array(
            //     'status' => true,
            //     'messages' => $datas_import,
            // ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);die;
//#6555:End


// use php library (PHPExcel) to read excel file on server side. But if the file contain too much record (>3000)
// the library can not handle and maybe crash
// The file have to contain maximum 2000 record for the best result
//#6555:Start
            // session_write_close();
            // // if file size > 10M, then $file is empty
            // if (!isset($_FILES['excel_file'])) {
            //     echo json_encode(array(
            //         'status' => false,
            //         'messages' => lang('lbl_over_size'),
            //     ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
            //     return;
            // }

            // $file      = $_FILES['excel_file']['tmp_name'];
            // $file_name = $_FILES['excel_file']['name'];

            // // if file size > 10M, then $file is empty
            // if (empty($file)) {
            //     echo json_encode(array(
            //         'status'   => false,
            //         'messages' => lang('lbl_over_size'),
            //     ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
            //     return;
            // }

            // $arrayZips = array("application/zip");
            // $arrayExtensions = array(".xlsx");
            // $original_extension = (false === $pos = strrpos($file_name, '.')) ? '' : substr($file_name, $pos);
            // $finfo = new finfo(FILEINFO_MIME_TYPE);
            // $type = $finfo->file($file);
            // $valid_file = true;
            // if (!in_array($type, $arrayZips) || !in_array($original_extension, $arrayExtensions)) {
            //     $valid_file = false;
            // }

            // if (!$valid_file) {
            //     echo json_encode(array(
            //         'status'   => false,
            //         'messages' => lang('lbl_unsupported_file'),
            //     ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
            //     return;
            // }

            // $obj_php_excel = PHPExcel_IOFactory::createReaderForFile($file);
            // $obj_php_excel->setReadDataOnly(false);
            // $obj_reader = $obj_php_excel->load($file);

            // //only load data from the first sheet
            // $sheet_datas = $obj_reader->setActiveSheetIndex(0)->toArray(null,true,false,false);

            // // accept data from column 0 -> 29 (A -> AD)
            // foreach ($sheet_datas as $row => $datas) {
            //     foreach ($datas as $column => $data) {
            //         if ($column >= 30) unset($sheet_datas[$row][$column]);
            //     }
            // }

            // // check valid format
            // $header_name = array('Ｊコード','枝','Ｊ担当営業','Ｊ担当営業ID','手配担当','手配担当ID','クライアント社名','請求元ブレーン','請求元ブレーンコード','作業ブレーン','作業ブレーンコード','発送ブレーン','発送ブレーンコード','発送種別','発送種別コード','セット名','単価','件数','発送日','円口','重量','発送物形状','発送物形状コード','計上月','非課税','税込発送料','税込切手代','税込その他','税別発送料','税別その他');
            // for ($i = 0; $i < 30; $i++) {
            //     if (!isset($sheet_datas[0][$i])) {
            //         echo json_encode(array(
            //             'status'   => false,
            //             'messages' => lang('lbl_invalid_format'),
            //         ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
            //         return;
            //     }
            //     if (isset($sheet_datas[0][$i]) && $sheet_datas[0][$i] != $header_name[$i]) {
            //         echo json_encode(array(
            //             'status'   => false,
            //             'messages' => lang('lbl_invalid_format'),
            //         ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
            //         return;
            //     }

            // }

            // array_shift($sheet_datas);
            // // remove all empty row
            // foreach ($sheet_datas as $row => $datas) {
            //     $remove = true;
            //     foreach ($datas as $column => $data) {
            //         if ($column >= 0 && $column <= 29) {
            //             $data = trim($data);
            //             $sheet_datas[$row][$column] = $data;
            //             if ($data != null) $remove = false;

            //             if ($column == 18) {
            //                 if (is_numeric($data)) {
            //                     $cell = $obj_reader->getActiveSheet()->getCell('S' . ($row+2));
            //                     $sheet_datas[$row][$column] = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($cell->getValue()));
            //                 }
            //             }

            //             if ($column == 23 && is_numeric($data)) {
            //                 $cell = $obj_reader->getActiveSheet()->getCell('X' . ($row+2));
            //                 $sheet_datas[$row][$column] = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($cell->getValue()));
            //             }
            //         }
            //     }

            //     if ($remove) unset($sheet_datas[$row]);
            // }

            // if (empty($sheet_datas)) {
            //     echo json_encode(array(
            //         'status'   => false,
            //         'messages' => lang('lbl_empty_file'),
            //     ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
            //     return;
            // }

            // $datas_import = $this->order_acceptance_model->import_excel($sheet_datas);
            // $this->assign('datas_import', $datas_import);
            // echo json_encode(array(
            //     'status' => true,
            //     'messages' => $datas_import,
            // ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
//#6555:End

// use another library (xlsx.js) to read the file on client side, then parse data to json and send that json to server
// each time send 200 record to server to handle and store the result to a json file type until
// all data have been sent and read that json file and parse to view for render
//#6555:Start
          $sheet_datas = json_decode($this->input->post('datas'));
          $last_data   = $this->input->post('last_data');
          $unique_id   = $this->input->post('unique_id');

          $datas_import = $this->order_acceptance_model->import_excel($sheet_datas);

          $file_name  = FCPATH . 'public/admin/order_acceptance/import/' . $unique_id.'.json';
          $past_data = array();
          if (file_exists($file_name)) {
               $past_data = json_decode(file_get_contents($file_name), true);
           }
           $write_data = array_merge($past_data, $datas_import);

           $fp = fopen($file_name, 'w+');
           fwrite($fp, json_encode($write_data));
           fclose($fp);

           if ($last_data) {
               $this->assign('datas_import', $write_data);
               unlink($file_name);
               $json_files = glob(FCPATH . "public/admin/order_acceptance/import/*.json");
               foreach ($json_files as $file) {
                   if (filemtime($file)) {
                       $modify_time = strtotime(date('Y-m-d', filemtime($file)));
                       if ($modify_time < strtotime(date('Y-m-d'))) {
                           unlink($file);
                       }
                   }
               }

               echo json_encode(array(
                   'status' => true,
                   'messages' => $write_data,
               ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
               die;

           } else {
               echo json_encode(array(
                   'status' => false,
               ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
               die;
           }
//#6555:End
        }
    }

    public function get_closing_date_end() {

        //if (!empty($this->_list_closing_date)) return $this->_list_closing_date;

        $this->load->model('closing_accountant_model');

        $closing_date = $this->closing_accountant_model->get_closing_month_recent();
        if(count($closing_date) >0){
            $key_end = (int)count($closing_date) -1 ;
            $this->_list_closing_date= date('Y-m', strtotime( $closing_date[$key_end]['close_date']));
        }
        if(empty($this->_list_closing_date)){
            return date("Y-m");
        }
        else{

        return $this->_list_closing_date;
        }
    }


    public function get_is_shipment_shape_code_name() {
        //$shipment_shapes = $this->_get_shipment_shape_code();
        //      if ($array !== null) return $array;

        $this->load->model('gui_parts_element_model');

        $shipment_shapes = $this->gui_parts_element_model->get_all_shipment_shape_elements();

        $date_return = array();

        foreach ($shipment_shapes as $shipment) {
            $key = $shipment['code'];
            $date_return[$key] = $shipment['name'];

        }
        return  $date_return;
    }

    public function show() {

//#9748: Start

        // $template_mgnt_DP_business_unit   =  $this->is_template_mgnt_DP_business_unit();

        // if($template_mgnt_DP_business_unit){
        //     if (!$this->get_var('type')) {
        //         redirect('/_admin/error/access_deny');
        //     }
        //     $this->show_template_dp_business_unit();
        // }
        // else{
        //     if ($this->get_var('type')) {
        //         redirect('/_admin/error/access_deny');
        //     }

        $this->load->model('account_model');
        $this->load->model('authority_model');

        $account_business_unit = $this->account_model->get_account_business_unit($this->auth->get_account_id());
        $account_template      = $account_business_unit[0]['finish_work_report_template'];

        if (($account_template == DM_FWR_TEMPLATE_CODE && $this->get_var('type')) || ($account_template == DP_FWR_TEMPLATE_CODE && !$this->get_var('type'))) {
            redirect('/_admin/error/access_deny');
        }

        if ($account_template == DP_FWR_TEMPLATE_CODE) {
            return $this->show_template_dp_business_unit();
        } elseif ($account_template != DM_FWR_TEMPLATE_CODE && $account_template != DP_FWR_TEMPLATE_CODE) {
            if ($this->get_var('type')) return $this->show_template_dp_business_unit();
        }
//#9748: End
        $all_sess ="";
        if(isset($_REQUEST['import_refresh'])){
            $all_sess = $this->session->userdata('data_import_oc');
        }
        else{
            $data_import_oc = array(
                    'listid_import' => ""
            );
            $this->session->set_userdata('data_import_oc',$data_import_oc);
        }


    //#7067:Start
    //         $form_jcode_brand = $this->input->post('from_jcode_branch');
    //         $to_jcode_branch = $this->input->post('to_jcode_branch');

            $jcodeidPOst = "";
            $brandcode = "";
            if($this->input->post('from_jcode_code')){
                $jcodeidPOst = $form_jcode_brand = $this->input->post('from_jcode_code');
                if($this->input->post('from_branch_code')){
                    $form_jcode_brand .= "-".$this->input->post('from_branch_code');
                    $brandcode = $this->input->post('from_branch_code');
                }
            }
            else{
                $form_jcode_brand = $this->input->post('from_jcode_branch');
                if($this->input->post('from_branch_code')){
                    $form_jcode_brand .= "-".$this->input->post('from_branch_code');
                }
            }

            if($this->input->post('to_jcode_code')){

                $to_jcode_branch = $this->input->post('to_jcode_code');
                if($this->input->post('to_branch_code')){
                    $to_jcode_branch .= "-".$this->input->post('to_branch_code');
                }
            }
            else{
                $to_jcode_branch = $this->input->post('to_jcode_branch');
            }

    //#7067:End

            $text_is_acceptance_input = $this->input->post('text_is_acceptance_input');
            $postFormOrder = 0;
            if(isset($form_jcode_brand) && isset($to_jcode_branch)){
                $postFormOrder = 1;
            }

            ini_set("memory_limit","1024M");
            $this->load->model('order_acceptance_model');
            $this->load->model('account_model');
            $this->load->model('business_unit_model');
            $this->load->model('shipping_type_model');
            $this->load->model('subcontractor_model');
            $this->load->model('gui_parts_element_model');
            $this->load->model('consumption_tax_model');

            $this->lang->load('order', $this->config->item('language'));


          //  $account_business_unit = $this->account_model->get_account_option_order_acception($this->auth->get_account_id());

            /**
             * If $is_product_division = 0
             * -> load all business unit & all account
             * If $is_product_division = 1
             * -> load business current and account current in business unit
             * @var $is_product_division, business_unit_list
             */
            $account_business_unit = $this->account_model->get_account_business_unit($this->auth->get_account_id());
            $business_units = $account_business_unit;
            $is_product_division = $account_business_unit[0]['is_product_division'];
            $accounts = array();
            $form_data = array();
            $working_accounts = $this->account_model->get_working_accounts('id, name');

        // $is_product_division = 0;
        if (!$is_product_division) {
//#9695:Start
//            $business_units = $this->business_unit_model->get_product_business_unit_id_name();
//            $accounts = $working_accounts;
            $business_units = $this->business_unit_model->get_business_unit_for_order_acceptance(DM_FWR_TEMPLATE_CODE);
//#9695:End

//#9748:Start
            $form_data['business_unit_id'] = $business_units[0]['id'];
//#9748:End
            $is_load = false;
        } else {
//#9695:Start
//            $accounts = $this->account_model->get_all_account_by_business_unit($business_units[0]['id']);
//#9695:Start
            $form_data['business_unit_id'] = $business_units[0]['id'];
            // $form_data['account_id'] = $this->auth->get_account_id();
            $is_load = true;
        }
//#9695:Start
        $accounts = $this->account_model->get_all_account_by_business_unit($business_units[0]['id']);
//#9695:End

            // initialize data

    //#7090:Start
            $form_data['list_id_oc_imput'] = $all_sess;
    //#7090:End

            $error_messages = array();

            if ($this->input->is_ajax_request()) {
                //tmp#6870: Start
                $postFormOrder = 0;
                $form_data = $this->input->post();
                $form_data['postFormOrder'] = $postFormOrder;

                // $default_business_unit_id = isset($form_data['business_unit_id']) ? $form_data['business_unit_id'] : $business_units[0]['id']; -> unused
                //tmp#6870: End

                // validate form data input
                $error_messages = $this->order_acceptance_model->run_search_condition_validate($form_data);

                if (count($error_messages) > 0) {
                    echo $this->ajax_json(array('status' => 'error', 'mesg' => $error_messages));exit;
                } else {
                    $search_results = $this->order_acceptance_model->search($form_data);
                }

            } else {
                if ($this->input->post()) {
                    $form_data = $this->input->post();
                }
                $form_data['postFormOrder'] = $postFormOrder;

                //tmp#6870: Start
                $form_data['order_acceptance_status'] = array(ORDER_ACCEPTANCE_UNAPPROVED, ORDER_ACCEPTANCE_APPROVED);

                if (isset($text_is_acceptance_input)) {
                    $form_data['is_from_order'] = true;
                }
                //tmp#6870: End

                $search_results = empty($error_messages) ? $this->order_acceptance_model->search($form_data) : array();
            }

            // $direct_subcontractors = array();
            // $subcontractors = $this->subcontractor_model->get_id_code_of_all_subcontractor_and_relation();

            // foreach ($subcontractors as &$subcontractor) {
            //     $direct_subcontractors[] = array(
            //         'parent_id'        => $subcontractor['parent_id'],
            //         'parent_code'      => $subcontractor['parent_code'],
            //         'parent_name'      => $subcontractor['parent_name'],
            //         'parent_abbr_name' => $subcontractor['parent_abbr_name'],
            //     );

            //     $tmp = array(
            //         'child_id'        => $subcontractor['parent_id'],
            //         'child_code'      => $subcontractor['parent_code'],
            //         'child_name'      => $subcontractor['parent_name'],
            //         'child_abbr_name' => $subcontractor['parent_abbr_name'],
            //     );

            //     array_unshift($subcontractor['children'], $tmp);
            // }

            $direct_subcontractors   = array();
            $indirect_subcontractors = array();
//#9695:Start
//        $all_subcontractor = $this->subcontractor_model->get_subcontractors();
        $all_subcontractor = $this->subcontractor_model->get_subcontractors_with_business();
        foreach ($all_subcontractor as $subcontractor) {

//            if ($subcontractor['is_no_add'] == 0) {
            if ($subcontractor['is_no_add'] == 0 && $subcontractor['finish_work_report_template'] == DM_FWR_TEMPLATE_CODE) {
                if ($subcontractor['direct_consignment'] == 1) {
//#7846:Start
//                    $direct_subcontractors[] = $subcontractor;
                    $direct_subcontractors[] = array(
                        'id'        => $subcontractor['id'],
                        'abbr_name' => $subcontractor['abbr_name'],
                    );
                }
//                $indirect_subcontractors[] = $subcontractor;
                $indirect_subcontractors[] = array(
                    'id'        => $subcontractor['id'],
                    'abbr_name' => $subcontractor['abbr_name'],
                );
//#7846:End
//#9695:End
                }
            }

            $auth = $this->auth->get_auth_data();

            $can_approve             = $this->auth->has_permission('acceptance_approve', 'column');

            $can_modify              = $this->auth->has_permission('acceptance_input_change', 'column');
            $order_acceptance_status = $this->input->post('order_acceptance_status');

            $list_shipments_shape_info = $this->get_is_shipment_shape_code_name();
    //         print_r($list_shipments_shape_info);
    //      exit();
            foreach ($search_results as $key => $result) {

                $modify_permission = $can_delete = $can_edit = $can_modify;
                $can_approve_base_on_search_cond = $can_approve;

                // if the search condition is searching closing summary month then user can not approve/unapprove data
                // set permission to false
                if (!empty($order_acceptance_status) && in_array(ORDER_ACCEPTANCE_ACCOUNT_TIGHTEN, $this->input->post('order_acceptance_status'))) {
                    $can_delete = $can_approve_base_on_search_cond = $can_edit = false;
                }

                $search_results[$key]['can_add']      = $modify_permission;
                $search_results[$key]['can_separate'] = $modify_permission;
               // $search_results[$key]['can_approve']  = $can_approve_month;
                $search_results[$key]['can_approve']  = $can_approve_base_on_search_cond;
                $search_results[$key]['can_delete']   = $can_delete;
                $search_results[$key]['can_edit']     = $can_edit;

                $search_results[$key]['is_difference_common'] = !$result['j_code'] && !$result['branch_cd'];
                $shipments_shape_info ="";

                $keycode = $search_results[$key]['shipments_shape_cd'];
                if (isset($list_shipments_shape_info[$keycode])) {
                    $shipments_shape_info = $list_shipments_shape_info[$keycode];
                } else {
                    $shipments_shape_info = "";
                }

                if (is_array($shipments_shape_info) && count($shipments_shape_info) > 0) {
                    $search_results[$key]['shipments_shape_name'] = $shipments_shape_info[0]['name'];
                }
                // $search_results[$key]['account_options']                = $working_accounts;
                // $search_results[$key]['direct_subcontractor_options']   = $direct_subcontractors;
                // $search_results[$key]['subcontractor_options']          = $subcontractors;
                // $search_results[$key]['shipping_type_options']          = $this->shipping_type_model->get_all_can_display();
                // $search_results[$key]['shipments_shape_options']        = $this->gui_parts_element_model->get_all_shipment_shape_elements();
                // $search_results[$key]['consumption_tax']                = $this->consumption_tax_model->get_rate();
            }

    //#7125:Start
            $all_consumption_tax = $this->consumption_tax_model->get_consumption_taxs('all', array(
                'fields' => 'rate, adaptation_date',
                'joins'  => array(
                    'consumption_tax_adaptation' => 'consumption_tax_adaptation.id = consumption_tax.consumption_tax_adaptation_id AND consumption_tax_adaptation.disable = 0'
                ),
                'order' => 'adaptation_date DESC'
            ));
    //#7125:End

    //#7846:Start
            $shipping_types = $this->shipping_type_model->get_all_can_display();
    //#7846:End

            $this->assign('account_options', json_encode ($working_accounts));
            $this->assign('direct_subcontractor_options', json_encode ($direct_subcontractors));
            $this->assign('indirect_subcontractor_options', json_encode ($indirect_subcontractors));
    //#7846:Start
    //        $this->assign('shipping_type_options', json_encode ($this->shipping_type_model->get_all_can_display()));
            $this->assign('shipping_type_options', json_encode ($shipping_types));
    //#7846:End
            $this->assign('shipments_shape_options', json_encode ($this->gui_parts_element_model->get_all_shipment_shape_elements()));
    //#7125:Start
            //$this->assign('consumption_tax', json_encode ($this->consumption_tax_model->get_rate()));
            $this->assign('consumption_tax', json_encode ($all_consumption_tax));
    //#7125:End

    //#7087:Start
            if ($search_results) {
                if (count($search_results) > OVER_RECORD) {

                    $search_results = array();
                    $search_results['is_over_record'] = true;
                    if ($this->input->is_ajax_request()) {
                        $this->ajax_json(array(
                                'status' => 'success',
                                'search_result' => $search_results,
                        ));
                        return;
                    } else {
                        $this->assign('search_results', json_encode($search_results, JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT));
                    }
                }
                else{

                    if ($this->input->is_ajax_request()) {

                        $this->ajax_json(array(
                                'status' => 'success',
                                'search_result' => $search_results,
                        ));
                        return;
                    } else {
                        $this->assign('search_results', json_encode($search_results, JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT));
                    }
                }
            }
            else{
                if ($this->input->is_ajax_request()) {

                    $this->ajax_json(array(
                            'status' => 'success',
                            'search_result' => $search_results,
                    ));
                    return;
                } else {
                    $this->assign('search_results', json_encode($search_results, JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT));
                }
            }

    //#7087:End

            $can_invoices = 'none';
            if ($this->auth->has_permission('all_invoices', 'column')) {
                $can_invoices = 'all';
            } elseif ($this->auth->has_permission('self_invoices', 'column')) {
                $can_invoices = 'self';
            }

    //#7846:Start
    //        $shipping_types = $this->shipping_type_model->get_all_can_display();
    //#7846:End

            $this->assign('business_units', $business_units);
            $this->assign('accounts', $accounts);
            $this->assign('shipping_types', $shipping_types);
            $this->assign('arrange_reps', $working_accounts);
            $this->assign('error_messages', $error_messages);
            $this->assign('can_modify', $can_modify);
            $this->assign('can_approve', $can_approve);
            $this->assign('can_invoices', $can_invoices);
            $auth_data = $this->auth->get_auth_data();
            $this->load->model('order_model');
            $this->load->model('gui_parts_element_model');
            $this->order_model->set_account_follow_bussiness_unit();
            $j_accounts = $this->order_model->get_j_account($auth_data['account_id']);

            if ($this->auth->has_permission('order_add_edit_all', 'column')) {
                $j_accounts = $this->order_model->get_j_account_by_bu_id($j_accounts[0]->bu_id);
            }
            $tax_division = $this->gui_parts_element_model->get_by_gui_parts_column_name('tax_division', 'code,name');
            $order_status_configs = $this->gui_parts_element_model->get_by_gui_parts_column_name('order_status', 'code,name');
            $tax_division = json_encode($tax_division);
            $this->assign('j_accounts', $j_accounts);
            $this->assign('order_model', $this->order_model);
            $this->assign('tax_division', $tax_division);

    //#7067:Start
    //         $nlistcode = strlen($form_jcode_brand);

    //         $jcodeidPOst = "";
    //         $brandcode = "";

    //         if ($nlistcode >= 8) {
    //             $jcodeidPOst = substr($form_jcode_brand, 0, 8);
    //             $brandcode = str_replace($jcodeidPOst, "", $form_jcode_brand);
    //         } else {
    //             $jcodeidPOst = $form_jcode_brand;
    //         }
    //#7067:End


            $post_text_is_acceptance_input = 0 ;
            $check_input_closing_date = false;
            $closing_date =  $this->get_closing_date_end();

            //tmp#6688: Start
            if (isset($text_is_acceptance_input)) {
                $post_text_is_acceptance_input = $text_is_acceptance_input;
                $search_status = array();

                foreach ($search_results as $rowac) {
                    if (strtotime($rowac['summary_month'] . '-01') <= strtotime($closing_date . '-01')) {
                        $check_input_closing_date = true;
                    }

                    // set search status
                    if (!isset($search_status[$rowac['order_acceptance_status']])) {
                        $search_status[] = $rowac['order_acceptance_status'];
                    }
                }

                $this->assign('search_status', $search_status);
            }
            //tmp#6688: End

    //#7067:Start
            $this->assign('form_jcode_brand',$form_jcode_brand);
            $this->assign('to_jcode_branch',$to_jcode_branch);
    //#7067:End

            $this->assign('check_input_closing_date', $check_input_closing_date);
            $this->assign('closingdatefinal', $closing_date);
            $this->assign('jcodeidPOst', $jcodeidPOst);
            $this->assign('brandcode', $brandcode);
            $this->assign('is_load', $is_load);
            $this->assign('postFormOrder',$postFormOrder);
            $this->assign('post_text_is_acceptance_input',$post_text_is_acceptance_input);
            $this->assign('order_acceptance_input_change', json_encode($this->auth->has_permission('acceptance_input_change', 'column')));
            $this->assign('order_acceptance_view', json_encode($this->auth->has_permission('order_acceptance_view', 'column')));
            $this->view('order_acceptance/add.tpl');
        // }

//#9748: End

    }

    /**
     * get all account by business unit id
     * method use in order acceptance search conditions
     * @return string
     */
    public function ajax_get_account_by_business_unit_id() {
        if (!$this->input->is_ajax_request()) show_404();

        $this->load->model('account_model');

        $business_unit_id = $this->input->post('business_unit_id');

        $accounts = $this->account_model->get_all_account_by_business_unit($business_unit_id);
        if (empty($accounts)) {
            echo 0; return;
        }

        $this->ajax_json($accounts);
    }

    /**
     * get all account
     * @return string
     */
    public function ajax_get_all_accounts() {
        if (!$this->input->is_ajax_request()) show_404();

        $this->load->model('account_model');

        $accounts = $this->account_model->get_working_accounts('id, name');;
        if (empty($accounts)) {
            echo 0; return;
        }
        $this->ajax_json($accounts);
    }

    public function approve() {

//          $this->update();
        if ($this->input->method() != 'post') show_404();

        $this->load->model('order_acceptance_model');

        ini_set("memory_limit","1024M");
        $data_post = json_decode($this->input->raw_input_stream, true);
        $app_sel = $data_post['app_sel'];
        if($app_sel){

                           $order_acceptances = $data_post['order_acceptance'];
                               if (!$this->auth->has_permission('order_acceptance/update') && !$this->auth->has_permission('acceptance_approve', 'column')) {
                                return $this->ajax_json(array(
                                        'status'   => false,
                                        'messages' => lang('lbl_edit_error'),
                                ));
                           }

                        $action = 1;
                        $error = array();
                         foreach ($order_acceptances as $key => $order_acceptance) {
                                if (!empty($order_acceptance)) {

                                    if (isset($order_acceptance['is_deleting']) && $order_acceptance['is_deleting']) continue;

                                    if ($order_acceptance['is_change_acceptance_status']) $action = 2;

//#6946:Start
//                                  $validate_result = $this->order_acceptance_model->run_validate($order_acceptance);
//                                  if (!empty($validate_result)) {
//                                      $error[$key] = $validate_result;
//                                  }
//#6946:End

                                    $order_acceptance_save[$key]['id'] = $order_acceptances[$key]['id'];
                                    $order_acceptance_save[$key]['j_code'] = $order_acceptances[$key]['j_code'];
                                    $order_acceptance_save[$key]['branch_cd'] = $order_acceptances[$key]['branch_cd'];
                                    $order_acceptance_save[$key]['order_acceptance_status'] = $order_acceptances[$key]['order_acceptance_status'];

//#7078:Start
                                    if(isset($order_acceptances[$key]['report_is_complete'])){
                                        $order_acceptance_save[$key]['report_is_complete'] = $order_acceptances[$key]['report_is_complete'];
                                    }
                                    $order_acceptance_save[$key]['order_id'] = $order_acceptances[$key]['order_id'];
                                    $order_acceptance_save[$key]['acceptance_approval_date']    = date('Y-m-d H:i:s');
                                    $order_acceptance_save[$key]['acceptance_approval_account_id'] = $this->auth->get_account_id();
                                    $order_acceptance_save[$key]['lastup_datetime'] = date('Y-m-d H:i:s');
//#7078:End
                                }
                        }
                        if (empty($error)) {
                            $success = $this->order_acceptance_model->update_order_acceptance2($order_acceptance_save);
                            if ($success) {
                                Order_acceptance::update_flag_status_acc($order_acceptance_save,2);
                                $messages = lang('lbl_approve_success');
                                $this->ajax_json(array(
                                        'status'   =>  $success ? true : false,
                                        'messages' => $messages,
                                ));
                            } else {
                                $error = lang('lbl_approve_error');
                                $this->ajax_json(array(
                                        'status'   => false,
                                        'messages' => $error,
                                ));
                             }
                             die;
                        }
                        else{
                            $this->ajax_json(array(
                                    'status'   => false,
                                    'messages' => $error,
                            ));
                            die;
                        }
        }
        else{

                    $name_file = $data_post['name_file'];
                    $readfile = $data_post['readfile'];
                    if($readfile){

                        $file = $name_file;
                        $order_acceptances ="";
                        $tongkey = $data_post['tongkey'];
                        $dataupdate= array();
                        $key = 0;
                        for($i = 0;  $i< $tongkey ;$i++){

                            $order_acceptances = json_decode(file_get_contents('public/admin/order_acceptance/json/'.$i."_".$file));
                            if(count($order_acceptances) > 0){
                                foreach ($order_acceptances as $rows){
                                    //foreach ($rows as $item){

                                        $dataupdate[$key]['id']= $rows->id;
                                        $dataupdate[$key]['j_code']= $rows->j_code;
                                        $dataupdate[$key]['branch_cd']= $rows->branch_cd;
                                        $dataupdate[$key]['order_acceptance_status']= $rows->order_acceptance_status;
                                        $dataupdate[$key]['order_id']= $rows->order_id;
//#7078:Start
                                        $dataupdate[$key]['acceptance_approval_date']    = date('Y-m-d H:i:s');
                                        $dataupdate[$key]['acceptance_approval_account_id'] = $this->auth->get_account_id();
                                        $dataupdate[$key]['lastup_datetime'] = date('Y-m-d H:i:s');
//#7078:End
                                    //}
                                        $key ++;
                                }
                                unlink('public/admin/order_acceptance/json/'.$i."_".$file);
                            }
                        }

                        $success = $this->order_acceptance_model->update_order_acceptance2($dataupdate);
                        if ($success) {
                            Order_acceptance::update_flag_status_acc($dataupdate,2);
                            $messages = lang('lbl_approve_success');
                            $this->ajax_json(array(
                                    'status'   =>  $success ? true : false,
                                    'messages' => $messages,
                            ));
                        } else {
                            $error = lang('lbl_approve_error');
                            $this->ajax_json(array(
                                    'status'   => false,
                                    'messages' => $error,
                            ));
                         }
                         die;
                    }
                    else{
                            $order_acceptances = $data_post['order_acceptance'];
                            $key =$data_post['key'];

                            if (!$this->auth->has_permission('order_acceptance/update') && !$this->auth->has_permission('acceptance_approve', 'column')) {
                                return $this->ajax_json(array(
                                        'status'   => false,
                                        'messages' => lang('lbl_edit_error'),
                                ));
                            }

                            // action = 1 for save action, 2 for approve action
                            $action = 1;
                            $error = array();
                            $order_acceptance_save ="";
                            foreach ($order_acceptances as $key => $order_acceptance) {
                                if (!empty($order_acceptance)) {

                                    if (isset($order_acceptance['is_deleting']) && $order_acceptance['is_deleting']) continue;

                                    if ($order_acceptance['is_change_acceptance_status']) $action = 2;


//#6946:Start
//                                  $validate_result = $this->order_acceptance_model->run_validate($order_acceptance);
//                                  if (!empty($validate_result)) {
//                                      $error[$key] = $validate_result;
//                                  }
//#6946:End

                                    $order_acceptance_save[$key]['id'] = $order_acceptances[$key]['id'];
                                    $order_acceptance_save[$key]['j_code'] = $order_acceptances[$key]['j_code'];
                                    $order_acceptance_save[$key]['branch_cd'] = $order_acceptances[$key]['branch_cd'];
                                    $order_acceptance_save[$key]['order_acceptance_status'] = $order_acceptances[$key]['order_acceptance_status'];
//#7078:Start
                                    if(isset($order_acceptances[$key]['report_is_complete'])){
                                        $order_acceptance_save[$key]['report_is_complete'] = $order_acceptances[$key]['report_is_complete'];
                                    }
                                    $order_acceptance_save[$key]['order_id'] = $order_acceptances[$key]['order_id'];
                                    $order_acceptance_save[$key]['acceptance_approval_date']    = date('Y-m-d H:i:s');
                                    $order_acceptance_save[$key]['acceptance_approval_account_id'] = $this->auth->get_account_id();
                                    $order_acceptance_save[$key]['lastup_datetime'] = date('Y-m-d H:i:s');
//#7078:End
                                }
                            }
                            //      print_r($error);

                            //print_r($order_acceptances);
                            if (empty($error)) {

                                $order_acceptances_write ="";
                                $key = $data_post['key'];
                                $file = 'public/admin/order_acceptance/json/'.$key."_".$name_file;
                               // $order_acceptances_write =  @json_decode(file_get_contents($file),true);
                               // $order_acceptances_write[] = $order_acceptance_save;
                                file_put_contents($file, json_encode($order_acceptance_save));
                                $messages = lang('lbl_approve_error');
                                $this->ajax_json(array(
                                                    'status'   => true,
                                                    'messages' => $messages,
                                            ));
                            }
                            else{
                                $this->ajax_json(array(
                                                    'status'   => false,
                                                    'messages' => $error,
                                            ));
                            }
                            die;
                    }
            //      if (empty($error)) {
            //          $success = $this->order_acceptance_model->update_order_acceptance2($order_acceptances);
            //          if ($success) {
            //              if ($action == 1){
            //                  Order_acceptance::update_flag_status_acc($order_acceptances,2);
            //                  $messages = lang('lbl_edit_success');
            //              }
            //              if ($action == 2){
            //                  Order_acceptance::update_flag_status_acc($order_acceptances,2);
            //                  $messages = lang('lbl_approve_success');
            //              }
            //          } else {
            //              if ($action == 1) $messages = lang('lbl_edit_error');
            //              if ($action == 2) $messages = lang('lbl_approve_error');
            //          }

            //          $this->ajax_json(array(
            //                  'status'   => $success ? true : false,
            //                  'messages' => $messages,
            //          ));
            //      } else {
            //          $this->ajax_json(array(
            //                  'status'   => false,
            //                  'messages' => $error,
            //          ));
            //      }


            //      die;
        }
    }
    function  update_flag_status_acc($order_acceptances,$action = 1){

        $this->load->model('order_model');
        $this->load->model('order_detail_model');
        $this->load->model('order_acceptance_model');
            $order_acceptances = array_filter($order_acceptances);

            $listdata = array();
            $order_inputs = array();
            foreach ($order_acceptances as $row_order_cc){

                $jcode = $row_order_cc['j_code'];
                $branch_cd = $row_order_cc['branch_cd'];

                if(($jcode <> "")&&($branch_cd <> "")){

                    $listoderdetail = $this->order_model->get_order_detai_by_jCode_and_BrandCode($jcode,$branch_cd);
                    $listoderdetail = array_filter($listoderdetail);
                    if(count($listoderdetail) > 0 ){
                         foreach ($listoderdetail as $rowdt){

                                $order_input['j_code'] = $jcode;
                                $order_input['branch_cd'] = $branch_cd;
                                $idOderDetail = $listoderdetail[0]->id ;

                                if($action = IS_INSERT_ORDERACCEPTANCE) {  //insert on grid

                                       $data['is_order_input'] = IS_ORDER_INPUT;
                                       $data['id'] = $idOderDetail;

                                       $listoccapprovel = $this->order_acceptance_model->get_all_order_acceptance_brandcode_jcode($jcode,$branch_cd,ORDER_ACCEPTANCE_IS_REPORT_ALREADY);
                                       $listocc_notapprovel = $this->order_acceptance_model->get_all_order_acceptance_brandcode_jcode($jcode,$branch_cd,ORDER_ACCEPTANCE_IS_REPORT_NOT_READY);

                                       $nlistoccapprovel =$listoccapprovel[0]['n_id_oc'];
                                       $nlist_occ_notapprovel = $listocc_notapprovel[0]['n_id_oc'];
                                       $data['id'] = $idOderDetail;


                                       if( ($nlistoccapprovel >= 0 ) && ($nlist_occ_notapprovel == 0)){
                                        $data['is_acceptance_input'] = IS_ACCEPTANCE_INPUT;
                                       }
                                       else{
                                        $data['is_acceptance_input'] = IS_NOT_ACCEPTANCE_INPUT;
                                       }

                                       if(($nlistoccapprovel == 0 ) && ($nlist_occ_notapprovel == 0)){
                                        $data['is_order_input'] = IS_NOT_ORDER_INPUT;
                                        $data['is_acceptance_input'] = IS_NOT_ACCEPTANCE_INPUT;
                                       }
                                }
                                else{
                                        $listoccapprovel = $this->order_acceptance_model->get_all_order_acceptance_brandcode_jcode($jcode,$branch_cd,ORDER_ACCEPTANCE_IS_REPORT_ALREADY);
                                        $listocc_notapprovel = $this->order_acceptance_model->get_all_order_acceptance_brandcode_jcode($jcode,$branch_cd,ORDER_ACCEPTANCE_IS_REPORT_NOT_READY);

                                        $nlistoccapprovel =$listoccapprovel[0]['n_id_oc'];
                                        $nlist_occ_notapprovel = $listocc_notapprovel[0]['n_id_oc'];
                                        $data['id'] = $idOderDetail;


                                        if( ($nlistoccapprovel >= 0 ) && ($nlist_occ_notapprovel == 0)){
                                            $data['is_acceptance_input'] = IS_ACCEPTANCE_INPUT;
                                        }
                                        else{
                                            $data['is_acceptance_input'] = IS_NOT_ACCEPTANCE_INPUT;
                                        }
                                        if(($nlistoccapprovel == 0 ) && ($nlist_occ_notapprovel == 0)){
                                            $data['is_order_input'] = IS_NOT_ORDER_INPUT;
                                        }
                                        if($action == 3){
                                            $data['is_order_input'] = IS_NOT_ORDER_INPUT;
                                            $data['is_acceptance_input'] = IS_NOT_ACCEPTANCE_INPUT;
                                        }
                                }

                                $listdata[] = $data;
                                $order_inputs[] = $order_input;
                         }
                    }
                }
            }
            if(count($order_inputs) >0 ){
                    $this->order_detail_model->update_batch('order_detail', $listdata,'id');
                    $this->order_detail_model->update_status_by_flag($order_inputs);
            }
    }

    public function update() {

        if ($this->input->method() != 'post') show_404();

        $this->load->model('order_acceptance_model');

        ini_set("memory_limit","1024M");

//#9748:Start
//        $order_acceptances = json_decode($this->input->raw_input_stream, true);
//        $order_acceptances = $order_acceptances['order_acceptance'];
        $datas = json_decode($this->input->raw_input_stream, true);
        $order_acceptances = $datas['order_acceptance'];

        $order_acceptance_authority = isset($datas['is_dp']) ? DP_FWR_TEMPLATE_CODE : DM_FWR_TEMPLATE_CODE;
//#9748:End

        for ($i = 0; $i < count($order_acceptances); $i++) {

            if ($order_acceptances[$i]) {

                if (!empty($order_acceptances[$i]['unit_price'])) {

                    $pos = strrpos($order_acceptances[$i]['unit_price'], '.');

                    if (!empty($pos)) {
                        $unit_price_ext = substr($order_acceptances[$i]['unit_price'], $pos);
                        $order_acceptances[$i]['unit_price'] = substr($order_acceptances[$i]['unit_price'], 0, $pos);
                        $order_acceptances[$i]['has_decimal_price'] = true;
                    }

//#7090:Start format weight
                    $pos_decimal_weight = strrpos($order_acceptances[$i]['weight'], '.');
                    if (!empty($pos_decimal_weight)) {
                        $decimal_weight = substr($order_acceptances[$i]['weight'], $pos_decimal_weight);
                        $order_acceptances[$i]['weight'] = substr($order_acceptances[$i]['weight'], 0, $pos_decimal_weight);
                        $order_acceptances[$i]['has_decimal_weight'] = true;
                    }
//#7090:End
                }
            }
        }

        if (!$this->auth->has_permission('order_acceptance/update') && !$this->auth->has_permission('acceptance_approve', 'column')) {
            return $this->ajax_json(array(
                'status'   => false,
                'messages' => lang('lbl_edit_error'),
            ));
        }

        // action = 1 for save action, 2 for approve action
        $action = 1;
        $error = array();
        foreach ($order_acceptances as $key => $order_acceptance) {
            if (!empty($order_acceptance)) {

                if (isset($order_acceptance['is_deleting']) && $order_acceptance['is_deleting']) continue;

                if ($order_acceptance['is_change_acceptance_status']) $action = 2;
//#9748:Start
//                $validate_result = $this->order_acceptance_model->run_validate($order_acceptance,$this->is_template_mgnt_DP_business_unit());
                $validate_result = $this->order_acceptance_model->run_validate($order_acceptance, $order_acceptance_authority);
//#9748:End
                if (!empty($validate_result)) {
//#7126:Start
//                    $error[$key] = $validate_result;
                    $error[$order_acceptance['updating_row']] = $validate_result;
//#7126:End
                }
            }
        }
        if (empty($error)) {
//#7090:Start
            for ($i = 0; $i < count($order_acceptances); $i++) {

                if ($order_acceptances[$i] && !empty($order_acceptances[$i]['unit_price']) && isset($order_acceptances[$i]['has_decimal_price'])) {
                    $order_acceptances[$i]['unit_price'] = $order_acceptances[$i]['unit_price'] . $unit_price_ext;
                }
                if ($order_acceptances[$i] && !empty($order_acceptances[$i]['weight']) && isset($order_acceptances[$i]['has_decimal_weight'])) {
                    $order_acceptances[$i]['weight'] = $order_acceptances[$i]['weight'] . $decimal_weight;
                }

            }

//#7090:End

//#9748: Start
            if($order_acceptance_authority == DP_FWR_TEMPLATE_CODE){
                $success = $this->order_acceptance_model->update_order_acceptance_mgnt_DP($order_acceptances);
            }
            else{
                $success = $this->order_acceptance_model->update_order_acceptance($order_acceptances);
            }
//#9748: End

            if ($success) {
                if ($action == 1){
                    Order_acceptance::update_flag_status_acc($order_acceptances,2);
                    $messages = lang('lbl_edit_success');
                }
                if ($action == 2){
                    Order_acceptance::update_flag_status_acc($order_acceptances,2);
                    $messages = lang('lbl_approve_success');
                }
            } else {
                if ($action == 1) $messages = lang('lbl_edit_error');
                if ($action == 2) $messages = lang('lbl_approve_error');
            }

            $this->ajax_json(array(
                'status'   => $success ? true : false,
                'messages' => $messages,
            ));
        } else {
            $this->ajax_json(array(
                'status'   => false,
                'messages' => $error,
            ));
        }

        die;
    }

    public function insert() {
        if (!$this->input->method() == 'post') show_404();

        $this->load->model('order_acceptance_model');
//#9748:Start
//        $order_acceptances = json_decode($this->input->raw_input_stream, true);
//        $order_acceptances = $order_acceptances['order_acceptance'];
        $datas = json_decode($this->input->raw_input_stream, true);
        $order_acceptances = $datas['order_acceptance'];

        $order_acceptance_authority = isset($datas['is_dp']) ? DP_FWR_TEMPLATE_CODE : DM_FWR_TEMPLATE_CODE;
//#9748:End
        // initialize for $unit_price_ext$
        $unit_price_ext = "";
        // end initialize for $unit_price_ext$
        for ($i = 0; $i < count($order_acceptances); $i++) {

            if ($order_acceptances[$i]) {

                if (!empty($order_acceptances[$i]['unit_price'])) {
                    $pos = strrpos($order_acceptances[$i]['unit_price'], '.');
                    if (!empty($pos)) {
                        $unit_price_ext = substr($order_acceptances[$i]['unit_price'], $pos);
                        $order_acceptances[$i]['unit_price'] = substr($order_acceptances[$i]['unit_price'], 0, $pos);
                        $order_acceptances[$i]['has_decimal_price'] = true;
                    }
                }
//#7090:Start format weight
                if (!empty($order_acceptances[$i]['weight'])) {
                    $pos_decimal_weight = strrpos($order_acceptances[$i]['weight'], '.');
                    if (!empty($pos_decimal_weight)) {
                        $decimal_weight = substr($order_acceptances[$i]['weight'], $pos_decimal_weight);
                        $order_acceptances[$i]['weight'] = substr($order_acceptances[$i]['weight'], 0, $pos_decimal_weight);
                        $order_acceptances[$i]['has_decimal_weight'] = true;
                    }
                }
//#7090:End
            }
        }
        // action = 1 for save action
        // action = 2 for approve
        foreach ($order_acceptances as $key => $order_acceptance) {

            if ($order_acceptance['is_change_acceptance_status'])
                $action = 2;

            if (!empty($order_acceptance)) {

                if (isset($order_acceptance['is_deleting']) && $order_acceptance['is_deleting'])
                    continue;
//#9748:Start
//                $validate_result = $this->order_acceptance_model->run_validate($order_acceptance,$this->is_template_mgnt_DP_business_unit());
                $validate_result = $this->order_acceptance_model->run_validate($order_acceptance, $order_acceptance_authority);
//#9748:End
                if (!empty($validate_result)) {
                    $error[$key] = $validate_result;
                }
            }
        }
        $action = 1;

        if (empty($error)) {
//#7090:Start format weight
            for ($i = 0; $i < count($order_acceptances); $i++) {

                if ($order_acceptances[$i] && !empty($order_acceptances[$i]['unit_price']) && isset($order_acceptances[$i]['has_decimal_price'])) {
                    $order_acceptances[$i]['unit_price'] = $order_acceptances[$i]['unit_price'] . $unit_price_ext;
                }
                if ($order_acceptances[$i] && !empty($order_acceptances[$i]['weight']) && isset($order_acceptances[$i]['has_decimal_weight'])) {
                    $order_acceptances[$i]['weight'] = $order_acceptances[$i]['weight'] . $decimal_weight;
                }
            }
//#7090:End

//#9748: Start
            if($order_acceptance_authority == DP_FWR_TEMPLATE_CODE){

                $success = $this->order_acceptance_model->insert_order_acceptance_mgnt_DP($order_acceptances);

            }
            else{

                $success = $this->order_acceptance_model->insert_order_acceptance($order_acceptances);

            }
//#9748: End


            if ($success) {

                if ($action == 1) $messages = lang('lbl_edit_success');
                if ($action == 2) $messages = lang('lbl_approve_success');

                Order_acceptance::update_flag_status_acc($order_acceptances, IS_INSERT_ORDERACCEPTANCE);
            } else {
                if ($action == 1) $messages = lang('lbl_edit_error');
                if ($action == 2) $messages = lang('lbl_approve_error');
            }

            $this->ajax_json(array(
                'status'   => $success['status'] ? true : false,
                'messages' => $messages,
                'data_id'  => $success['data_id'],
            ));
        } else {
            $this->ajax_json(array(
                'status'   => false,
                'messages' => $error,
            ));
        }

        die;
    }

    public function cancel_acceptance() {

        if (!$this->input->method() == 'post') show_404();
        $this->load->model('order_acceptance_model');
        ini_set("memory_limit","1024M");
        $order_acceptances = json_decode($this->input->raw_input_stream, true);
        $multi_larger =  $order_acceptances['multi_larger'];

        if($multi_larger){
                $name_file = $order_acceptances['name_file'];

                $readfile =  $order_acceptances['readfile'];
                if($readfile){
                    $tongkey = $order_acceptances['tongkey'];
                    $dataupdate= array();
                    $key = 0;
                    for($i = 0;  $i< $tongkey ;$i++){

                        $order_acceptances = json_decode(file_get_contents('public/admin/order_acceptance/json/'.$i."_".$name_file));

                        foreach ($order_acceptances as $rows){
                            //foreach ($rows as $item){

                            $dataupdate[$key]['id']= $rows->id;
                            $dataupdate[$key]['j_code']= $rows->j_code;
                            $dataupdate[$key]['branch_cd']= $rows->branch_cd;
                            $dataupdate[$key]['order_acceptance_status']= $rows->order_acceptance_status;
                            $dataupdate[$key]['order_id']= $rows->order_id;
//#7078:Start
                            $dataupdate[$key]['lastup_account_id'] = $this->auth->get_account_id();
                            $dataupdate[$key]['lastup_datetime'] = date('Y-m-d H:i:s');
//#7078:End
                            //}
                            $key ++;
                        }
                        unlink('public/admin/order_acceptance/json/'.$i."_".$name_file);
                    }
                    $success = $this->order_acceptance_model->update_order_acceptance2($dataupdate,true);
                    if ($success) {
                        Order_acceptance::update_flag_status_acc($dataupdate,2);
                        $messages = lang('lbl_unapprove_success');
                        $this->ajax_json(array(
                                'status'   =>  $success ? true : false,
                                'messages' => $messages,
                        ));
                    } else {
                        $error = lang('lbl_unapprove_error');
                        $this->ajax_json(array(
                                'status'   => false,
                                'messages' => $error,
                        ));
                    }
                    die;

                }
                else{
                        $key_namefile =  $order_acceptances['key'];
                        $order_acceptances = $order_acceptances['order_acceptance'];
        //              $order_acceptances = array_filter($order_acceptances);
                        $datawrite = array();
                        foreach ($order_acceptances as $key => $order_acceptance) {
//#6946:Start
//                          $validate_result = $this->order_acceptance_model->run_validate($order_acceptance);
//#6946:End
                            if (!empty($order_acceptance)) {

                                $datawrite[$key]['id']=$order_acceptance['id'];
                                $datawrite[$key]['j_code']= $order_acceptance['j_code'];
                                $datawrite[$key]['branch_cd']= $order_acceptance['branch_cd'];
                                $datawrite[$key]['order_acceptance_status'] = $order_acceptance['order_acceptance_status'];
                                $datawrite[$key]['order_id']= $order_acceptance['order_id'];
                            }
                        }
                        if (!empty($error)) {
                            $this->ajax_json(array(
                                    'status'   => false,
                                    'messages' => $error,
                            ));
                            return;
                        }
                        $order_acceptances_write ="";
                        $file = 'public/admin/order_acceptance/json/'.$key_namefile."_".$name_file;
                        // $order_acceptances_write =  @json_decode(file_get_contents($file),true);
                        // $order_acceptances_write[] = $order_acceptance_save;
                        file_put_contents($file, json_encode($datawrite));
                        $messages = lang('lbl_unapprove_success');
                        $this->ajax_json(array(
                                'status'   => true,
                                'messages' => $messages,
                        ));
                }

        }
        else{
                $order_acceptances = $order_acceptances['order_acceptance'];
                $order_acceptances = array_filter($order_acceptances);

                foreach ($order_acceptances as $key => $order_acceptance) {
//#6946:Start
//                  $validate_result = $this->order_acceptance_model->run_validate($order_acceptance);
//                  if (!empty($validate_result)) {
//                      $error[$key] = $validate_result;
//                  }
//#6946:End

                }
                if (!empty($error)) {
                    $this->ajax_json(array(
                        'status'   => false,
                        'messages' => $error,
                    ));
                    return;
                }

                $success = $this->order_acceptance_model->cancel_order_acceptance($order_acceptances);
                if($success){
                    Order_acceptance::update_flag_status_acc($order_acceptances, 1);
                }

                $this->ajax_json(array(
                    'status'   => $success ? true : false,
                    'messages' => $success ? lang('lbl_unapprove_success') : lang('lbl_unapprove_error'),
                ));
                die;
        }

    }

    public function delete_acceptance() {
        if (!$this->input->method() == 'post') show_404();

        $this->load->model('order_acceptance_model');

        $order_acceptance = json_decode($this->input->raw_input_stream, true);
        $order_acceptance = $order_acceptance['order_acceptance'];

        $success = $this->order_acceptance_model->delete_order_acceptance($order_acceptance);
        $this->ajax_json(array(
            'status'   => $success ? true : false,
            'messages' => $success ? lang('lbl_edit_success') : lang('lbl_edit_error'),
        ));
        die;
    }

    public function show_order() {
        $this->load->model('order_acceptance_model');
    }

    /**
     * get order detail from j_code and branch_cd
     * method use in order input when user input j_code and branch_cd method will called
     * @return json
     */
    public function ajax_get_order_detail() {
        if (!$this->input->method() == 'post' || !$this->input->raw_input_stream) show_404();
        $this->load->model('order_acceptance_model');
        $this->load->model('consumption_tax_model');
//#9695:Start
        $this->load->model('account_model');
//#9695:End

        $formData = json_decode($this->input->raw_input_stream, true);
//#9695:Start
//        $this->ajax_json($this->order_acceptance_model->get_order_detail($formData['j_code'], $formData['branch_cd']));
//#9748:Start
//        $account_business_unit = $this->account_model->get_account_business_unit($this->auth->get_account_id());
//        $template_mgnt_DP = $this->is_template_mgnt_DP_business_unit();
        $order_acceptance_authority = isset($formData['is_dp']) ? DP_FWR_TEMPLATE_CODE : DM_FWR_TEMPLATE_CODE;

//        if($template_mgnt_DP){
        if($order_acceptance_authority == DP_FWR_TEMPLATE_CODE){
//#9748:End
//#9960:Start
//get tax rate
            $tax_rate_list = $this->get_list_tax_rates();

//get tax rate
 //#9960:End
            $order_detail          = $this->order_acceptance_model->get_order_detail_mgnt_DP($formData['j_code'], $formData['branch_cd']);
//#9960:Start
           if(count($order_detail) >0){
                    if(isset($order_detail->od_tax_type)){
                        if($order_detail->od_tax_type == TAX_INCLUDED){
                            $tax_rate = 0;
                            if ($tax_rate_list) {
                                foreach ($tax_rate_list as $txr) {
                                    if (strtotime($order_detail->od_delivery_date) >= strtotime($txr['adaptation_date'])) {
                                        $tax_rate = $txr['rate'];
                                        break;
                                    }
                                }
                            }
                            $total_cost_total_price = bcdiv($order_detail->od_amount, bcadd(1, bcdiv($tax_rate, 100)));
                        }else{
                            $total_cost_total_price =  $order_detail->od_amount;
                        }
                     }
                     else{

                          $total_cost_total_price =  $order_detail->od_amount;

                     }

                    $total_cost_total_price =bcdiv(floor(bcmul($total_cost_total_price ,100)),100);
                    $order_detail->sales_in_order =  $total_cost_total_price;
           }
//#9960:End
        }
        else{
            $order_detail          = $this->order_acceptance_model->get_order_detail($formData['j_code'], $formData['branch_cd']);
        }
//#9748:Start
//        if ($order_detail && $order_detail->finish_work_report_template == $account_business_unit[0]['finish_work_report_template']) {
        if ($order_detail && $order_detail->finish_work_report_template == $order_acceptance_authority) {
//#9748:End
            return $this->ajax_json($order_detail);
        }
        return $this->ajax_json(null);
//#9695:End
    }

    public function export_csv() {
//#9748: Start
        $order_acceptance_authority = $this->input->post('is_dp') ? DP_FWR_TEMPLATE_CODE : DM_FWR_TEMPLATE_CODE;
        if (!$order_acceptance_authority) {
            redirect('/_admin/error/access_deny');
        }

        if($order_acceptance_authority == DP_FWR_TEMPLATE_CODE){
            $this->export_csv_mgnt_DP();
        }
        else{


            $this->load->model('order_acceptance_model');
            $this->load->model('account_model');
            $this->load->model('subcontractor_model');
            $this->load->model('shipping_type_model');
            $this->load->model('gui_parts_element_model');

            $order_acceptances = $this->order_acceptance_model->search($this->input->post());
            $working_accounts = $this->account_model->get_working_accounts('id, name');
            $subcontractors = $this->subcontractor_model->get_subcontractors();
            $shipping_types = $this->shipping_type_model->get_all();
            $shipments_shapes = $this->gui_parts_element_model->get_all_shipment_shape_elements();

            $header = mb_convert_encoding('発注・検収ステータス','SJIS','UTF-8') . ','
                    . mb_convert_encoding('Ｊコード','SJIS','UTF-8') . ','
                    . mb_convert_encoding('枝','SJIS','UTF-8') . ','
                    . mb_convert_encoding('Ｊ担当営業','SJIS','UTF-8') . ','
                    . mb_convert_encoding('手配担当','SJIS','UTF-8') . ','
                    . mb_convert_encoding('クライアント社名','SJIS','UTF-8') . ','
                    . mb_convert_encoding('請求元ブレーン','SJIS','UTF-8') . ','
                    . mb_convert_encoding('作業ブレーン','SJIS','UTF-8') . ','
                    . mb_convert_encoding('発送ブレーン','SJIS','UTF-8') . ','
                    . mb_convert_encoding('発送種別','SJIS','UTF-8') . ','
                    . mb_convert_encoding('セット名','SJIS','UTF-8') . ','
                    . mb_convert_encoding('単価','SJIS','UTF-8') . ','
                    . mb_convert_encoding('件数','SJIS','UTF-8') . ','
                    . mb_convert_encoding('発送日','SJIS','UTF-8') . ','
                    . mb_convert_encoding('円口','SJIS','UTF-8') . ','
                    . mb_convert_encoding('重量','SJIS','UTF-8') . ','
                    . mb_convert_encoding('発送物形状','SJIS','UTF-8') . ','
                    . mb_convert_encoding('計上月','SJIS','UTF-8') . ','
                    . mb_convert_encoding('非課税','SJIS','UTF-8') . ','
                    . mb_convert_encoding('税込発送料','SJIS','UTF-8') . ','
                    . mb_convert_encoding('税込切手代','SJIS','UTF-8') . ','
                    . mb_convert_encoding('税込その他','SJIS','UTF-8') . ','
                    . mb_convert_encoding('税別発送料','SJIS','UTF-8') . ','
                    . mb_convert_encoding('税別その他','SJIS','UTF-8') . ','
                    . mb_convert_encoding('合計（税別、非課税）','SJIS','UTF-8') . "\r\n";

            $content = '';
            foreach ($order_acceptances as $order_acceptance) {

                $status = lang('checkbox_unapproved');
                if ($order_acceptance['order_acceptance_status'] == ORDER_ACCEPTANCE_APPROVED) {
                    $status = lang('checkbox_approved');
                }
                if ($order_acceptance['order_acceptance_status'] == ORDER_ACCEPTANCE_ACCOUNT_TIGHTEN) {
                    $status = lang('checkbox_account_tighten');
                }

                $content .= mb_convert_encoding($status,'SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding($order_acceptance['j_code'],'SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding($order_acceptance['branch_cd'],'SJIS','UTF-8') . ',';

                $account_name     = '';
                $arrange_rep_name = '';
                foreach ($working_accounts as $account) {
                    if ($account['id'] == $order_acceptance['account_id']) {
                        $account_name = $account['name'];
                    }

                    if ($account['id'] == $order_acceptance['arrange_rep']) {
                        $arrange_rep_name = $account['name'];
                    }
                }

            $source_subcontractor_name   = '';
            $work_subcontractor_name     = '';
            $shipping_subcontractor_name = '';
            foreach ($subcontractors as $subcontractor) {
//#9695:Start
                if ($subcontractor['finish_work_report_template'] == DM_FWR_TEMPLATE_CODE) {
//#9695:End
                    if ($subcontractor['id'] == $order_acceptance['source_subcontractor_id']) {
                        $source_subcontractor_name = $subcontractor['name'];
                    }
                    if ($subcontractor['id'] == $order_acceptance['work_subcontractor_id']) {
                        $work_subcontractor_name = $subcontractor['name'];
                    }
                    if ($subcontractor['id'] == $order_acceptance['shipping_subcontractor_id']) {
                        $shipping_subcontractor_name = $subcontractor['name'];
                    }
                }
            }

                $shipping_type_name = '';
                foreach ($shipping_types as $shipping_type) {
                    if ($shipping_type['code'] == $order_acceptance['shipping_type_cd']) {
                        $shipping_type_name = $shipping_type['name'];
                    }
                }

                $shipments_shape_name = '';
                foreach ($shipments_shapes as $shipment_shape) {
                    if ($shipment_shape['code'] == $order_acceptance['shipments_shape_cd']) {
                        $shipments_shape_name = $shipment_shape['name'];
                    }
                }

    //#9643:Start
                $content .= mb_convert_encoding('"'.$account_name.'"','SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding('"'.$arrange_rep_name.'"','SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding('"'.$order_acceptance['client_name'].'"','SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding('"'.$source_subcontractor_name.'"','SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding('"'.$work_subcontractor_name.'"','SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding('"'.$shipping_subcontractor_name.'"','SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding('"'.$shipping_type_name.'"','SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding('"'.$order_acceptance['set_name'].'"','SJIS','UTF-8') . ',';
    //#9643:End
                $content .= mb_convert_encoding($order_acceptance['unit_price'],'SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding($order_acceptance['quantity'],'SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding($order_acceptance['delivery_date'],'SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding($order_acceptance['fee'],'SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding($order_acceptance['weight'],'SJIS','UTF-8') . ',';
    //#9643:Start
                $content .= mb_convert_encoding('"'.$shipments_shape_name.'"','SJIS','UTF-8') . ',';
    //#9643:End
                $content .= mb_convert_encoding($order_acceptance['summary_month'],'SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding($order_acceptance['tax_free'],'SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding($order_acceptance['shipping_charge_tax'],'SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding($order_acceptance['stamp_tax'],'SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding($order_acceptance['etc_tax'],'SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding($order_acceptance['shipping_charge'],'SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding($order_acceptance['etc'],'SJIS','UTF-8') . ',';
                $content .= mb_convert_encoding($order_acceptance['amount'],'SJIS','UTF-8') . ',';

                $content .= "\r\n";
            }
            $csv = $header . $content;

            $this->load->helper('download');

            $filename = filename_to_urlencode("発注・検収_" . date('Ymd') .'.csv');
            force_download($filename, $csv);
        }
//#9748: End


    }

    public function ajax_get_progress() {
        if (!$this->input->get('unique_id')) show_404();

        session_write_close();
        $unique_id = $this->input->get('unique_id');
//#6555:Start
//        $file_name = FCPATH . 'public/admin/order_acceptance/' . $unique_id . '.json';
        $file_name = FCPATH . 'public/admin/order_acceptance/import/' . $unique_id . '.json';
//#6555:End
        $content   = null;

        do {
            if (file_exists($file_name)) {
                $fp = fopen($file_name, 'rb');
                $content = json_decode(stream_get_contents($fp), true);
                fclose($fp);
            } else {
                $content = array('status' => 0);
                break;
            }
        } while ($content == null);

        if ($content != null && $content['status'] == 100) {
//#6555:Start
//            $fp = fopen($file_name, 'rb');
//            fclose($fp);
//            array_map('unlink', glob(FCPATH . "public/admin/order_acceptance/*.json"));

            unlink($file_name);
            $json_files = glob(FCPATH . "public/admin/order_acceptance/import/*.json");
            foreach ($json_files as $file) {
               if (filemtime($file)) {
                   $modify_time = strtotime(date('Y-m-d', filemtime($file)));
                   if ($modify_time < strtotime(date('Y-m-d'))) {
                       unlink($file);
                   }
               }
            }
//#6555:End
        }

        $this->ajax_json($content);

    }

//#9748: Start
    /**
     * Get DM事業部用  Or DP事業部用  by current account
     * @author : nhu_tham
     * @date:15/2/2015
     *
     */
    function is_template_mgnt_DP_business_unit(){

        $this->load->model('account_model');
        $account_business_unit = $this->account_model->get_account_business_unit($this->auth->get_account_id());
        $finish_work_report_template =   $account_business_unit[0]['finish_work_report_template'];
        if($finish_work_report_template == DP_FWR_TEMPLATE_CODE){
            return true;
        }
        else{
            return false;
        }

    }


    function  show_template_dp_business_unit(){
        $all_sess ="";
        if(isset($_REQUEST['import_refresh'])){
            $all_sess = $this->session->userdata('data_import_oc');
        }
        else{
            $data_import_oc = array(
                    'listid_import' => ""
            );
            $this->session->set_userdata('data_import_oc',$data_import_oc);
        }

        $jcodeidPOst = "";
        $brandcode = "";
        if($this->input->post('from_jcode_code')){
            $jcodeidPOst = $form_jcode_brand = $this->input->post('from_jcode_code');
            if($this->input->post('from_branch_code')){
                $form_jcode_brand .= "-".$this->input->post('from_branch_code');
                $brandcode = $this->input->post('from_branch_code');
            }
        }
        else{
            $form_jcode_brand = $this->input->post('from_jcode_branch');
            if($this->input->post('from_branch_code')){
                $form_jcode_brand .= "-".$this->input->post('from_branch_code');
            }
        }

        if($this->input->post('to_jcode_code')){

            $to_jcode_branch = $this->input->post('to_jcode_code');
            if($this->input->post('to_branch_code')){
                $to_jcode_branch .= "-".$this->input->post('to_branch_code');
            }
        }
        else{
            $to_jcode_branch = $this->input->post('to_jcode_branch');
        }

        //#7067:End

        $text_is_acceptance_input = $this->input->post('text_is_acceptance_input');
        $postFormOrder = 0;
        if(isset($form_jcode_brand) && isset($to_jcode_branch)){
            $postFormOrder = 1;
        }

        ini_set("memory_limit","1024M");
        $this->load->model('order_acceptance_model');
        $this->load->model('account_model');
        $this->load->model('business_unit_model');
        $this->load->model('shipping_type_model');
        $this->load->model('subcontractor_model');
        $this->load->model('gui_parts_element_model');
        $this->load->model('consumption_tax_model');
        $this->lang->load('order', $this->config->item('language'));

        /**
         * If $is_product_division = 0
         * -> load all business unit & all account
         * If $is_product_division = 1
         * -> load business current and account current in business unit
         * @var $is_product_division, business_unit_list
        */
        $account_business_unit = $this->account_model->get_account_business_unit($this->auth->get_account_id());
        $business_units = $account_business_unit;
        $is_product_division = $account_business_unit[0]['is_product_division'];
        $accounts = array();
        $form_data = array();
        $working_accounts = $this->account_model->get_working_accounts('id, name');
        if (!$is_product_division) {
            $business_units = $this->business_unit_model->get_business_unit_for_order_acceptance(DP_FWR_TEMPLATE_CODE);
//#9748:Start
            $form_data['business_unit_id'] = $business_units[0]['id'];
//#9748:End
            $is_load = false;
        } else {

            $form_data['business_unit_id'] = $business_units[0]['id'];
            $is_load = true;
        }

        $accounts = $this->account_model->get_all_account_by_business_unit($business_units[0]['id']);

        $form_data['list_id_oc_imput'] = $all_sess;

        $error_messages = array();

        if ($this->input->is_ajax_request()) {
            $postFormOrder = 0;
            $form_data = $this->input->post();
            $form_data['postFormOrder'] = $postFormOrder;
            // validate form data input
            $error_messages = $this->order_acceptance_model->run_search_condition_validate($form_data);

            if (count($error_messages) > 0) {
                echo $this->ajax_json(array('status' => 'error', 'mesg' => $error_messages));exit;
            } else {
                $search_results = $this->order_acceptance_model->search_mgmt_dp($form_data);
            }

        } else {
            if ($this->input->post()) {
                $form_data = $this->input->post();
            }
            $form_data['postFormOrder'] = $postFormOrder;
            $form_data['order_acceptance_status'] = array(ORDER_ACCEPTANCE_UNAPPROVED, ORDER_ACCEPTANCE_APPROVED);

            if (isset($text_is_acceptance_input)) {
                $form_data['is_from_order'] = true;
            }

            $search_results = empty($error_messages) ? $this->order_acceptance_model->search_mgmt_dp($form_data) : array();
        }
        $direct_subcontractors   = array();
        $indirect_subcontractors = array();
        $all_subcontractor = $this->subcontractor_model->get_subcontractors_with_business();
//         print_r($all_subcontractor);
        foreach ($all_subcontractor as $subcontractor) {
            if ($subcontractor['is_no_add'] == 0 && $subcontractor['finish_work_report_template'] == DP_FWR_TEMPLATE_CODE ) {
                if ($subcontractor['direct_consignment'] == 1) {
                    $direct_subcontractors[] = array(
                            'id'        => $subcontractor['id'],
                            'abbr_name' => $subcontractor['abbr_name'],
                    );
                }
                $indirect_subcontractors[] = array(
                        'id'        => $subcontractor['id'],
                        'abbr_name' => $subcontractor['abbr_name'],
                );
            }
        }

        $auth = $this->auth->get_auth_data();

        $can_approve             = $this->auth->has_permission('acceptance_approve', 'column');

        $can_modify              = $this->auth->has_permission('acceptance_input_change', 'column');
        $order_acceptance_status = $this->input->post('order_acceptance_status');

        $list_shipments_shape_info = $this->get_is_shipment_shape_code_name();

//#9960:Start
//get tax rate
        $tax_rate_list = $this->get_list_tax_rates();
//get tax rate
//#9960:End

        foreach ($search_results as $key => $result) {

//#9960:Start
            if($result['od_tax_type'] == TAX_INCLUDED){
                $tax_rate = 0;
                if ($tax_rate_list) {
                    foreach ($tax_rate_list as $txr) {
                        if (strtotime($result['od_delivery_date']) >= strtotime($txr['adaptation_date'])) {
                            $tax_rate = $txr['rate'];
                            break;
                        }
                    }
                }
                $total_cost_total_price = bcdiv($result['od_amount'], bcadd(1, bcdiv($tax_rate, 100)));
            }else{
                $total_cost_total_price =  $result['od_amount'];
            }
            $total_cost_total_price =bcdiv(floor(bcmul($total_cost_total_price ,100)),100);
//#9960:End

            $modify_permission = $can_delete = $can_edit = $can_modify;
            $can_approve_base_on_search_cond = $can_approve;

            // if the search condition is searching closing summary month then user can not approve/unapprove data
            // set permission to false
            if (!empty($order_acceptance_status) && in_array(ORDER_ACCEPTANCE_ACCOUNT_TIGHTEN, $this->input->post('order_acceptance_status'))) {
                $can_delete = $can_approve_base_on_search_cond = $can_edit = false;
            }

            $search_results[$key]['can_add']      = $modify_permission;
            $search_results[$key]['can_separate'] = $modify_permission;
//#9960:Start
            $search_results[$key]['sales_in_order']  = $total_cost_total_price;
//#9960:End
            $search_results[$key]['can_approve']  = $can_approve_base_on_search_cond;
            $search_results[$key]['can_delete']   = $can_delete;
            $search_results[$key]['can_edit']     = $can_edit;

            $search_results[$key]['is_difference_common'] = !$result['j_code'] && !$result['branch_cd'];
            $shipments_shape_info ="";

            $keycode = $search_results[$key]['shipments_shape_cd'];
            if (isset($list_shipments_shape_info[$keycode])) {
                $shipments_shape_info = $list_shipments_shape_info[$keycode];
            } else {
                $shipments_shape_info = "";
            }

            if (is_array($shipments_shape_info) && count($shipments_shape_info) > 0) {
                $search_results[$key]['shipments_shape_name'] = $shipments_shape_info[0]['name'];
            }
        }

        //#7125:Start
        $all_consumption_tax = $this->consumption_tax_model->get_consumption_taxs('all', array(
                'fields' => 'rate, adaptation_date',
                'joins'  => array(
                        'consumption_tax_adaptation' => 'consumption_tax_adaptation.id = consumption_tax.consumption_tax_adaptation_id AND consumption_tax_adaptation.disable = 0'
                ),
                'order' => 'adaptation_date DESC'
        ));
        //#7125:End

        //#7846:Start
        $shipping_types = $this->shipping_type_model->get_all_can_display();
        //#7846:End

        $this->assign('account_options', json_encode ($working_accounts));
        $this->assign('direct_subcontractor_options', json_encode ($direct_subcontractors));
        $this->assign('indirect_subcontractor_options', json_encode ($indirect_subcontractors));
        //#7846:Start
        //        $this->assign('shipping_type_options', json_encode ($this->shipping_type_model->get_all_can_display()));
        $this->assign('shipping_type_options', json_encode ($shipping_types));
        //#7846:End
        $this->assign('shipments_shape_options', json_encode ($this->gui_parts_element_model->get_all_shipment_shape_elements()));
        //#7125:Start
        //$this->assign('consumption_tax', json_encode ($this->consumption_tax_model->get_rate()));
        $this->assign('consumption_tax', json_encode ($all_consumption_tax));
        //#7125:End

        //#7087:Start
        if ($search_results) {
            if (count($search_results) > OVER_RECORD) {

                $search_results = array();
                $search_results['is_over_record'] = true;
                if ($this->input->is_ajax_request()) {
                    $this->ajax_json(array(
                            'status' => 'success',
                            'search_result' => $search_results,
                    ));
                    return;
                } else {
                    $this->assign('search_results', json_encode($search_results, JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT));
                }
            }
            else{

                if ($this->input->is_ajax_request()) {

                    $this->ajax_json(array(
                            'status' => 'success',
                            'search_result' => $search_results,
                    ));
                    return;
                } else {
                    $this->assign('search_results', json_encode($search_results, JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT));
                }
            }
        }
        else{
            if ($this->input->is_ajax_request()) {

                $this->ajax_json(array(
                        'status' => 'success',
                        'search_result' => $search_results,
                ));
                return;
            } else {
                $this->assign('search_results', json_encode($search_results, JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT));
            }
        }


        $can_invoices = 'none';
        if ($this->auth->has_permission('all_invoices', 'column')) {
            $can_invoices = 'all';
        } elseif ($this->auth->has_permission('self_invoices', 'column')) {
            $can_invoices = 'self';
        }


        $this->assign('business_units', $business_units);
        $this->assign('accounts', $accounts);
        $this->assign('shipping_types', $shipping_types);
        $this->assign('arrange_reps', $working_accounts);
        $this->assign('error_messages', $error_messages);
        $this->assign('can_modify', $can_modify);
        $this->assign('can_approve', $can_approve);
        $this->assign('can_invoices', $can_invoices);
        $auth_data = $this->auth->get_auth_data();
        $this->load->model('order_model');
        $this->load->model('gui_parts_element_model');
        $this->order_model->set_account_follow_bussiness_unit();
        $j_accounts = $this->order_model->get_j_account($auth_data['account_id']);

        if ($this->auth->has_permission('order_add_edit_all', 'column')) {
            $j_accounts = $this->order_model->get_j_account_by_bu_id($j_accounts[0]->bu_id);
        }
        $tax_division = $this->gui_parts_element_model->get_by_gui_parts_column_name('tax_division', 'code,name');
        $order_status_configs = $this->gui_parts_element_model->get_by_gui_parts_column_name('order_status', 'code,name');
        $tax_division = json_encode($tax_division);
        $this->assign('j_accounts', $j_accounts);
        $this->assign('order_model', $this->order_model);
        $this->assign('tax_division', $tax_division);


            $post_text_is_acceptance_input = 0 ;
            $check_input_closing_date = false;
            $closing_date =  $this->get_closing_date_end();
            if (isset($text_is_acceptance_input)) {
                $post_text_is_acceptance_input = $text_is_acceptance_input;
                $search_status = array();

                foreach ($search_results as $rowac) {
                    if (strtotime($rowac['summary_month'] . '-01') <= strtotime($closing_date . '-01')) {
                        $check_input_closing_date = true;
                    }

                    // set search status
                    if (!isset($search_status[$rowac['order_acceptance_status']])) {
                        $search_status[] = $rowac['order_acceptance_status'];
                    }
                }

                $this->assign('search_status', $search_status);
            }
            $this->assign('form_jcode_brand',$form_jcode_brand);
            $this->assign('to_jcode_branch',$to_jcode_branch);
            $this->assign('check_input_closing_date', $check_input_closing_date);
            $this->assign('closingdatefinal', $closing_date);
            $this->assign('jcodeidPOst', $jcodeidPOst);
            $this->assign('brandcode', $brandcode);
            $this->assign('is_load', $is_load);
            $this->assign('postFormOrder',$postFormOrder);
            $this->assign('post_text_is_acceptance_input',$post_text_is_acceptance_input);
            $this->assign('order_acceptance_input_change', json_encode($this->auth->has_permission('acceptance_input_change', 'column')));
            $this->assign('order_acceptance_view', json_encode($this->auth->has_permission('order_acceptance_view', 'column')));
            $this->view('order_acceptance/add_template_mgnt_DP.tpl');

    }

    private function export_csv_mgnt_DP() {
        $this->load->model('order_acceptance_model');
        $this->load->model('account_model');
        $this->load->model('subcontractor_model');
        $this->load->model('shipping_type_model');
        $this->load->model('gui_parts_element_model');

        $order_acceptances = $this->order_acceptance_model->search_mgmt_dp($this->input->post());
        $working_accounts = $this->account_model->get_working_accounts('id, name');
        $subcontractors = $this->subcontractor_model->get_subcontractors();
        $shipping_types = $this->shipping_type_model->get_all();
        $shipments_shapes = $this->gui_parts_element_model->get_all_shipment_shape_elements();

        $header = 	  mb_convert_encoding('発注・検収ステータス','SJIS','UTF-8') . ','
                    . mb_convert_encoding('Ｊコード','SJIS','UTF-8') . ','
                    . mb_convert_encoding('枝','SJIS','UTF-8') . ','
                    . mb_convert_encoding('Ｊ担当営業','SJIS','UTF-8') . ','
                    . mb_convert_encoding('クライアント社名','SJIS','UTF-8') . ','
                    . mb_convert_encoding('商品名','SJIS','UTF-8') . ','
                    . mb_convert_encoding('内容','SJIS','UTF-8') . ','
                    . mb_convert_encoding('請求元ブレーン','SJIS','UTF-8') . ','
                    . mb_convert_encoding('受注時単価','SJIS','UTF-8') . ','
                    . mb_convert_encoding('受注時数量','SJIS','UTF-8') . ','
                    . mb_convert_encoding('受注時売上（税別）','SJIS','UTF-8') . ','
                    . mb_convert_encoding('受注時原価合計（税別）','SJIS','UTF-8') . ','
                    . mb_convert_encoding('確定原価（税別）','SJIS','UTF-8') . ','
                    . mb_convert_encoding('納品完了日','SJIS','UTF-8') . ','
                    . mb_convert_encoding('計上月','SJIS','UTF-8') . "\r\n";

        $content = '';

//#9960:Start
//get tax rate
        $tax_rate_list = $this->get_list_tax_rates();

//get tax rate

//#9960:End
        foreach ($order_acceptances as $order_acceptance) {

            $status = lang('checkbox_unapproved');
            if ($order_acceptance['order_acceptance_status'] == ORDER_ACCEPTANCE_APPROVED) {
                $status = lang('checkbox_approved');
            }
            if ($order_acceptance['order_acceptance_status'] == ORDER_ACCEPTANCE_ACCOUNT_TIGHTEN) {
                $status = lang('checkbox_account_tighten');
            }

            $content .= mb_convert_encoding($status,'SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding($order_acceptance['j_code'],'SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding($order_acceptance['branch_cd'],'SJIS','UTF-8') . ',';

            $account_name     = '';
            $arrange_rep_name = '';
            foreach ($working_accounts as $account) {
                if ($account['id'] == $order_acceptance['account_id']) {
                    $account_name = $account['name'];
                }

                if ($account['id'] == $order_acceptance['arrange_rep']) {
                    $arrange_rep_name = $account['name'];
                }
            }

            $source_subcontractor_name   = '';
            foreach ($subcontractors as $subcontractor) {
                    if ($subcontractor['id'] == $order_acceptance['source_subcontractor_id']) {
                        $source_subcontractor_name = $subcontractor['name'];
                    }
            }

#9960:Start
            if($order_acceptance['od_tax_type'] == TAX_INCLUDED){
                $tax_rate = 0;
                if ($tax_rate_list) {
                    foreach ($tax_rate_list as $txr) {
                        if (strtotime($order_acceptance['od_delivery_date']) >= strtotime($txr['adaptation_date'])) {
                            $tax_rate = $txr['rate'];
                            break;
                        }
                    }
                }
                $total_cost_total_price = bcdiv($order_acceptance['od_amount'], bcadd(1, bcdiv($tax_rate, 100)));
            }else{
                $total_cost_total_price =  $order_acceptance['od_amount'];
            }
            $total_cost_total_price =bcdiv(floor(bcmul($total_cost_total_price ,100)),100);
#9960:End

            //#9643:Start
            $content .= mb_convert_encoding('"'.$account_name.'"','SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding('"'.$order_acceptance['client_name_order'].'"','SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding('"'.$order_acceptance['pro_name'].'"','SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding('"'.$order_acceptance['content_pro'].'"','SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding('"'.$source_subcontractor_name.'"','SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding('"'.$order_acceptance['unit_price_in_order'].'"','SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding('"'.$order_acceptance['quanity_in_order'].'"','SJIS','UTF-8') . ',';
#9960:Start
//            $content .= mb_convert_encoding('"'.$order_acceptance['sales_in_order'].'"','SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding('"'.$total_cost_total_price.'"','SJIS','UTF-8') . ',';
#9960:End
            $content .= mb_convert_encoding('"'.$order_acceptance['total_cost_in_order_excluding_tax'].'"','SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding($order_acceptance['amount'],'SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding($order_acceptance['delivery_date'],'SJIS','UTF-8') . ',';
            $content .= mb_convert_encoding($order_acceptance['summary_month'],'SJIS','UTF-8') . ',';
            $content .= "\r\n";
        }
        $csv = $header . $content;

        $this->load->helper('download');

        $filename = filename_to_urlencode("発注・検収_" . date('Ymd') .'.csv');
        force_download($filename, $csv);

    }

//#9960:Start
    public function get_list_tax_rates(){

        //get tax rate

        $this->load->model('consumption_tax_model');
        $list_shipments_shape_info = $this->get_is_shipment_shape_code_name();
        $get_tax_rate_params = array(
                'columns' => array('`consumption_tax`.rate, `consumption_tax_adaptation`.adaptation_date'),
                'conditions' => array(
                        'consumption_tax_type_id' => CONSUMPTION_TAX
                ),
                'orders' => array(
                        '`consumption_tax_adaptation`.adaptation_date' => 'DESC'
                )
        );

        $tax_rate_list = $this->consumption_tax_model->get_tax_rate_by_conditions($get_tax_rate_params);
        return $tax_rate_list;

        //get tax rate

    }
//#9960:End
//#9748: End
}
