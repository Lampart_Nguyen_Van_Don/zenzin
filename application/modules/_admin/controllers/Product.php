<?php
class Product extends ZR_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('product_model');
    }

    /**
     * show
     * @author Phong
     * @return
     * @created date: 20/5/2015
     */
    public function show() {
        $business_unit = $this->product_model->get_business_unit_of_current_logged_user($this->auth->get_account_id ());
        $list_product = array();
        // If business unit is OK
        if(!empty($business_unit)) {
            $list_product = $this->product_model->set_pager()->get_all_products($business_unit['id']);
            if(!empty($list_product)) {
                $this->assign('list_product', $list_product);
            }
        }
        // Check "add permission" of current logged user
        $add_permission = $this->auth->has_permission("product_registration_change_delete", 'column');
        $this->assign('add_permission', $add_permission);
        $this->view('product/show.tpl');
    }

    /**
     * detail
     * @author Phong
     * @return
     * @created date: 20/5/2015
     */
    public function detail() {
        if (! $this->input->is_ajax_request ()) {
            show_404 ();
        }

        $product = $this->product_model->get_product_by_id($this->input->post('id_product'));
        if(!empty($product)) {
            $this->assign('product', $product);
            // Check "update and delete permission" of current logged user
            $update_delete_permission = $this->auth->has_permission("product_registration_change_delete", 'column');
            $this->assign('update_delete_permission', $update_delete_permission);
        }
        $this->view('product/detail.tpl');
    }

    /**
     * edit
     * @author Phong
     * @return
     * @created date: 20/5/2015
     */
    public function edit() {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        
        if ($this->input->post("to_edit_page")) {
            $id_product = $this->input->post('id_product');
            $product = $this->product_model->get_product_by_id($id_product);
            if (!empty($product)) {
                $this->assign('product_name_maxlenght', Product_model::MAX_LENGHT_NAME);
                $this->assign('product', $product);
            }
            $this->view('product/edit.tpl');
        } else if ($this->input->post("save_edit")) {
            $product_id = $this->input->post("product_id");

            if (!$this->product_model->check_record_exist($product_id)) {
                $result = array(
                    'status' => false,
                    'message' => lang('common_record_not_exist')
                );

                $this->ajax_json($result);
            }

            if ($this->product_model->validate('default')) {
                // update product
                $data_update = array(
                    'name' => trim($this->input->post("name")),
                    'is_no_add' => $this->input->post("product_is_no_add")
                );

                if ($this->product_model->update("product", $data_update, array('id' => $product_id))) {
                    $result = array(
                        'status' => true,
                        'message' => lang('common_edit_successfully')
                    );
                } else {
                    $result = array(
                        'status' => false,
                        'message' => lang('common_could_not_edit')
                    );
                }

                $this->ajax_json($result);
            } else {
                $result = array(
                    'status' => false,
                    'messages' => array(
                        'name' => form_error('name')
                    )
                );

                $this->ajax_json($result);
            }
        }
    }

    /**
     * add
     * @author Phong
     * @return
     * @created date: 20/5/2015
     */
    public function add() {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        
        // Only go to add page (open popup)
        if ($this->input->post('to_add_page')) {
            $this->assign('product_name_maxlenght', Product_model::MAX_LENGHT_NAME);
            
            $this->view('product/add.tpl');
        } else if ($this->input->post('save_add')) {
            if ($this->product_model->validate('default')) {
                // Business unit of current logged user
                $business_unit = $this->product_model->get_business_unit_of_current_logged_user($this->auth->get_account_id());

                // If null product manage code => Error message
                if (empty($business_unit['product_manage_code'])) {
                    $result = array(
                        'status' => false,
                        'messages' => array(
                            'product_manage_code' => sprintf(lang('error_null_product_manage_code'), $business_unit['name'])
                        )
                    );

                    $this->ajax_json($result);
                }
                
                $code_last = 0;
                // get product code last
                if ($product_code_last = $this->product_model->get_product_code_last($business_unit['id'])) {
                    $code_last = substr($product_code_last['code'], -Product_model::CODE_LENGHT);

                    // if code max is delete: decreased 1
                    if ($product_code_last['disable']) {
                        --$code_last;
                    }
                }
                
                $code_new = $business_unit['product_manage_code'] . sprintf("%0" . Product_model::CODE_LENGHT . "d", ++$code_last);
                
                // Data insert
                $data_insert = array(
                    'code' => $code_new,
                    'name' => trim($this->input->post("name")),
                    'is_no_add' => 0,
                    'business_unit_id' => $business_unit['id']
                );
                
                // Insert
                if ($this->product_model->insert("product", $data_insert)) {
                    $result = array(
                        'status' => true,
                        'message' => lang('common_insert_successfully')
                    );
                } else {
                    $result = array(
                        'status' => false,
                        'message' => lang('common_could_not_add')
                    );
                }
                
                $this->ajax_json($result);
            } else {
                $result = array(
                    'status' => false,
                    'messages' => array(
                        'name' => form_error('name')
                    )
                );
                
                $this->ajax_json($result);
            }
        }
    }

    /**
     * delete
     * @return
     * @author Phong
     * @created date: 20/5/2015
     */
    public function delete() {
        if (! $this->input->is_ajax_request ()) {
            show_404 ();
        }
        $product_id = $this->input->post("product_id");
        $this->load->model('product_model');
        // Check order_id exists in order_detail
        if($this->product_model->check_record_exist($product_id)) {
            if(!$this->product_model->check_product_order($product_id)) {
                // Delete process
                $this->product_model->delete("product", array('id'=>$product_id));
                $errors ['error'] = 0;
                echo json_encode ( $errors );
            } else {
                $errors ['error'] = 1;
                $errors ['error_message'] = lang('error_order_product_exists');
                echo json_encode ( $errors );
            }
        } else {
            $errors ['error'] = 1;
            $errors ['error_message'] = lang('common_record_not_exist');
            echo json_encode ( $errors );
        }
    }
}