<?php
class Reconcile_amount extends ZR_Controller {
    public function __construct() {
        parent::__construct ();
        $this->load->model('reconcile_amount_model');
    }

    public function import() {
        $this->check_message();

        // upload excel file
        if ($this->input->is_ajax_request()) {
            session_write_close();
//#9547:Start
//            $excel_datas = $this->reconcile_amount_model->validate_import_data();
            $datas                = $this->reconcile_amount_model->validate_import_data();
            $excel_datas          = $datas['excel_datas'];
            $clients_total_amount = $datas['clients_total_amount'];

//#9547:End
            if (isset($excel_datas['status']) && $excel_datas['status'] == false) {
                echo json_encode($excel_datas ,JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
                return;
            }

            $this->assign('excel_datas', $excel_datas);
//#9547:Start
            $this->assign('clients_total_amount', $clients_total_amount);
//#9547:End
            echo json_encode(array(
                'status' => true,
                'messages' => $this->app_smarty->fetch('reconcile_amount/import_confirm.tpl'),
            ), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
            return;
        }

        // insert to db
        if ($this->input->method() == 'post' && $this->input->post('save_data')) {
            $status = $this->reconcile_amount_model->import_excel($this->input->post('excel_data'));

            if ($status) redirect('/_admin/reconcile_amount/import_clearance');

            $this->assign('upload_status', $status);
        }

        $this->view('reconcile_amount/import.tpl');
    }


     /**
     * clearance list
     * @author: Vo Ly Minh Nhan
     * @created date: 31/July/2015
     */
    public function import_clearance() {
        //getting data
        $pulldown = $this->reconcile_amount_model->get_pulldown_datetime();

        $summary_month = $pulldown['default'];
        $s_code = '';
        if ($this->input->method() == 'post') {
            $summary_month = $this->input->post('summary_month');
            $s_code        = $this->input->post('s_code');
        }

        // $data = $this->reconcile_amount_model->get_clearance_data($summary_month, $s_code, '');

        //assignments
        $this->assign("pulldown", $pulldown['list']);
        $this->assign("summary_month", $summary_month);
        // $this->assign("data", json_encode($data));

        $this->view('reconcile_amount/import_clearance.tpl');
    }

    /**
     * save_upload_to_db
     * @author Vo Ly Minh Nhan
     * @created date: 30/July/2015
     */
    public function get_customer_by_scode() {
        $s_code = $this->input->get("term");
        $exactly = $this->input->get("exactly") ? 1 : 0;

        if ($this->input->is_ajax_request()) {
            $s_code  = $this->input->post('term');
            $exactly = $this->input->post('exactly') ? 1 : 0;
//#9547:Start
            $amount     = $this->input->post('amount');
            $is_confirm = $this->input->post('is_confirm') === "true" ? 1 : 0;
        }

//        echo json_encode((array)$this->reconcile_amount_model->get_client_by_scode($s_code, $exactly), JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);
        $client = $this->reconcile_amount_model->get_client_by_scode($s_code, $exactly);
        if ($client && $is_confirm) {
            $validate_result = $this->reconcile_amount_model->validate_adjust_money($client[0]['id'], $amount);
            if ($validate_result) {
                $client[0]['errors']   = true;
                $client[0]['messages'] = $validate_result;
            }
        }
        echo json_encode($client, JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT);die;
//#9547:End
    }

    /**
     * Use ajax to get reconcile data base on summary_month, s_code and remaining_scrub
     * @return json
     * @author  cam_tien
     */
    public function get_data() {
        if (!$this->input->is_ajax_request()) show_404();

        $summary_month = $this->input->post('summary_month');
        $s_code = $this->input->post('s_code');
        $remaining_scrub = $this->input->post('remaining_scrub') ? 1 : 0;
        $action = $this->input->post('action') ? $this->input->post('action') : false;

        $this->load->model('reconcile_amount_model');

        $valid_summary = $this->reconcile_amount_model->get_pulldown_datetime();
        if (!in_array($summary_month, $valid_summary['list'])) {
            $this->ajax_json(array(
                'can_save' => false,
                'reconcile_datas' => array(),
            ));
        }

        // $summary_month = '2015/07';
        // $s_code = '';
        // $remaining_scrub = 0;
        // $action = false;

        $reconcile_datas = $this->reconcile_amount_model->get_clearance_data($summary_month, $s_code, $remaining_scrub, $action);
        // echo "<pre>";print_r($reconcile_datas);
        $this->ajax_json(array(
            'can_save' => $summary_month == $valid_summary['default'],
            'reconcile_datas' => $reconcile_datas,
        ));
    }

    /**
     * update data after blance
     * @return json
     * @author  cam_tien
     */
    public function update_clearance() {
        if (!$this->input->is_ajax_request()) show_404();
        if (!$this->input->post('data')) show_404();

        $this->load->model('reconcile_amount_model');

        $datas = json_decode($this->input->post('data'), true);

        $errors = $this->reconcile_amount_model->run_update_validate($datas);
        if (!empty($errors)) {
            $this->ajax_json(array(
                'status' => false,
                'messages' => $errors,
            ));
        }

        $results = $this->reconcile_amount_model->update_clearance_data($datas);
        $this->ajax_json($results);
    }

    public function detail() {
        if ((!$id = $this->input->post('id')) || (!is_numeric($id))) {
            echo lang('msg_invalid_id');exit;
        }

        if (!$month = $this->input->post('month')) {
            $month = '';
        } else {
            $month = $this->input->post('month');
        }

        if (!$year = $this->input->post('year')) {
            $year = '';
        } else {
            $year = $this->input->post('year');
        }


        $status_detail = 0;
        $this->load->library( 'session' );
        $this->session->set_userdata( 'id', $id );
        $this->session->set_userdata( 'month_pay', $month );
        $this->session->set_userdata( 'year_pay', $year );
        $this->load->models(array('client_model', 'order_model', 'account_model', 'business_unit_model', 'gui_parts_element_model', 'sales_slip_model','payment_past_data','reconcile_amount_model'));

        $client_s_code =  $this->payment_past_data->get_account_all();

        $past_payment_data =  $this->payment_past_data->get_client_s_code_payment($id);

        $array_s_code_client = array();

        foreach($client_s_code as $account) {
            if($account['id'] != $past_payment_data[0]['id']) {
                $array_s_code_client[$account['id']] = $account['s_code']. ":" . $this->client_model->name_with_title($account['name'], $account['corporate_status_before'], $account['corporate_status_after']);
            }
        }

        $payment_data = $this->payment_past_data->get_client_payment($id,$month,$year);
        $total_money = $this->payment_past_data->get_total_payment($id,$month,$year);

        $max_closing_accountant_date = $this->reconcile_amount_model->_get_max_closing_accountant_date();

        $client_not_balance = $this->reconcile_amount_model->check_client_can_balance($id,$month,$year);

        if((int)$month == date('m', strtotime($max_closing_accountant_date)) && (int)$year == date('Y', strtotime($max_closing_accountant_date))  && !empty($client_not_balance))
        {
           $status_detail = 1;
        }
        else $status_detail = 0;


        $this->assign('id', $id);
        $this->assign('s_code_client', $array_s_code_client);
        $this->assign('payment_data',  $payment_data);
        $this->assign('client_s_code', $past_payment_data);
        $this->assign('total_money',  $total_money);
        $this->assign('status_detail',$status_detail);

        $this->view('reconcile_amount/detail.tpl');
    }

    public function edit_payment() {
        $this->load->library( 'session' );
        $this->load->models(array('client_model', 'order_model', 'account_model', 'business_unit_model', 'gui_parts_element_model', 'sales_slip_model','payment_past_data'));

        $valid_u = true;
        $error_data = array();

        if(!$this->input->post('client_id_u') && !$this->session->userdata('month_pay') && !$this->session->userdata('year_pay')) {
            echo lang('msg_invalid_id');exit;
        }

        $this->load->library('form_validation');
//#7315:Start
//        $this->form_validation->set_rules('input_u_money',lang('lbl_divide_money'),'required|numeric|check_equal_zero');
//#9588:Start
//        $this->form_validation->set_rules('input_u_money',lang('lbl_divide_money'),'required|numeric|check_equal_zero|less_than_equal_to[' . MAX_SIGNED_INT_10 . ']');
        $this->form_validation->set_rules('input_u_money',lang('lbl_divide_money'),'required|numeric|check_equal_zero|less_than_equal_to[' . MAX_SIGNED_INT_10 . ']|greater_than_equal_to['. -(MAX_SIGNED_INT_10 + 1) .']');
        $this->form_validation->set_message('less_than_equal_to', lang('error_amount_out_of_range'));
        $this->form_validation->set_message('greater_than_equal_to', lang('error_amount_out_of_range'));
//#9588:Start
//#7315:End
        $this->form_validation->set_message('check_equal_zero', str_replace('%field%', lang('lbl_divide_money'), lang('error_is_required')));
        $this->form_validation->set_rules('input_u_comment',lang('lbl_divide_before_comment'),'required|trim');

        // if ($this->input->post('input_d_money') < 0) {
        //     $valid_u = false;
        //     $error_data['input_u_money'] = str_replace('%field%', lang('lbl_divide_money'), lang('msg_numeric_unsigned'));
        // }

        $valid_u_total = $this->form_validation->run() && $valid_u;
        if($valid_u_total) {
            $total_money = $this->payment_past_data->get_total_payment($this->input->post('client_id_u'),$this->session->userdata('month_pay'),$this->session->userdata('year_pay'));


                $month = $this->session->userdata('month_pay');
                $year = $this->session->userdata('year_pay');

                $client_id = $this->input->post('client_id_u');
                $input_d_money = round($this->input->post('input_u_money'));
                $input_u_comment = $this->input->post('input_u_comment');

                $int_date      = $year . '-' . $month . '-01';
                $month_pay     = date('Y-m-t',strtotime($int_date));

//#9537:Start
                $to_client = $this->client_model->get_client_by_id($client_id);
                if (!$to_client) {
                    return $this->ajax_json(array(
                        'status'   => false,
                        'messages' => 'to client not exist',
                    ));
                }

                $validate_client_to = $this->reconcile_amount_model->validate_adjust_money($to_client['id'], round($input_d_money));
                if ($validate_client_to) {
                    return $this->ajax_json(array(
                        'status'   => false,
                        's_code'   => $to_client['s_code'],
                        'messages' => $validate_client_to,
                    ));
                }
//#9547:End

                $data_client_edit = array(
                    'client_id' => $client_id,
                    'payment_date' => $month_pay,
                    'account_holder' => '手調整',
                    'amount' => round($input_d_money),
                    'comment' =>  $input_u_comment,
                    'lastup_account_id'  => $this->auth->get_account_id() ,
                    'create_datetime' => date("Y-m-d H:i:s"),
                    'lastup_datetime' => date("Y-m-d H:i:s"),
                    'disable' => 0
                );

                if ($this->payment_past_data->insert_data($data_client_edit)) {
                    $this->reconcile_amount_model->update_payment_entry_after_edit($client_id);
                }
                
                $result = array(
                    'status' => AJAX_SUCCESS,
                    'message'=> lang('common_insert_successfully'),
                    'month' => $month,
                    'year' => $year,
                    'client_id' => $client_id,
                    'input_d_money' => number_format(round($input_d_money)),
                    'input_u_comment' => $input_u_comment,
                    'month_date' => date('Y/m/t',strtotime($int_date)),
                    'account_holder' => '手調整'
                );

        } else {
            $result = array(
                'status' => AJAX_FAIL,
                'errors'  => $this->form_validation->error_array() + $error_data,
                'valid' => $valid_u
            );
        }
        $this->ajax_json($result);
    }

    public function divide_money() {
        $this->load->library( 'session' );
        $this->load->models(array('client_model', 'order_model', 'account_model', 'business_unit_model', 'gui_parts_element_model', 'sales_slip_model','payment_past_data'));

        $valid = true;
        $error_data = array();

        if(!$this->input->post('client_id_own') && !$this->session->userdata('month_pay') && !$this->session->userdata('year_pay')) {
            echo lang('msg_invalid_id');
            exit;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('in_s_code',lang('lbl_s_code'),'required');
//#7315:Start
//        $this->form_validation->set_rules('input_d_money',lang('lbl_divide_money'),'required|numeric|check_equal_zero');
        $this->form_validation->set_rules('input_d_money',lang('lbl_divide_money'),'required|numeric|check_equal_zero|less_than_equal_to[' . MAX_SIGNED_INT_10 . ']');
        $this->form_validation->set_message('less_than_equal_to', lang('error_amount_out_of_range'));
//#7315:End
        $this->form_validation->set_message('check_equal_zero', str_replace('%field%', lang('lbl_divide_money'), lang('error_must_be_positive')));
        $this->form_validation->set_rules('input_b_d_comment',lang('lbl_divide_before_comment'),'required|trim');
        $this->form_validation->set_rules('input_a_d_comment',lang('lbl_divide_after_comment'),'required|trim');

        if(($this->input->post('input_d_money')) < 0) {
            $valid = false;
            $error_data['input_d_money'] = str_replace('%field%', lang('lbl_divide_money'), lang('error_must_be_positive'));
        }

        $valid_total = $this->form_validation->run() && $valid;
        if($valid_total) {
            $total_money = $this->payment_past_data->get_total_payment($this->input->post('client_id_own'),$this->session->userdata('month_pay'),$this->session->userdata('year_pay'));

            if(!empty($total_money)) {
                if($total_money[0]['payment_amount'] <  $this->input->post('input_d_money')) {
                    $result = array(
                        'status' => AJAX_FAIL,
                        'errors' =>'',
                        'message' => lang('msg_status_payment'),
                        'valid' => $valid,
                        'total' => $total_money
                    );
                } else {

                    $month = $this->session->userdata('month_pay');
                    $year = $this->session->userdata('year_pay');

                    $client_id = $this->input->post('client_id_own');
                    $in_s_code = $this->input->post('in_s_code');
                    $input_d_money = $this->input->post('input_d_money');
                    $input_b_d_comment = $this->input->post('input_b_d_comment');
                    $input_a_d_comment = $this->input->post('input_a_d_comment');

                    $int_date = $year . '-' . $month . '-01';
                    $month_pay = date('Y-m-t',strtotime($int_date));

//#9547:Start                    
//                    if (!$this->reconcile_amount_model->check_can_devide_money($client_id, round(-$input_d_money))) {
//                        $result = array(
//                            'status' => AJAX_FAIL,
//                            'errors' =>'',
//                            'message' => lang('error_can_not_blance'),
//                            'valid' => $valid,
//                            'total' => $total_money
//                        );
//                    } else {

                    $from_client = $this->client_model->get_client_by_id($client_id);
                    if (!$from_client) {
                        return $this->ajax_json(array(
                            'status'   => false,
                            'messages' => str_replace('%field%', lang('lbl_client'), lang('error_not_exist')),
                        ));
                    }

                    $to_client = $this->client_model->get_client_by_id($in_s_code);
                    if (!$to_client) {
                        return $this->ajax_json(array(
                            'status'   => false,
                            'messages' => str_replace('%field%', lang('lbl_client'), lang('error_not_exist')),
                        ));
                    }

                    $validate_client_from = $this->reconcile_amount_model->validate_adjust_money($from_client['id'], round(-$input_d_money));
                    if ($validate_client_from) {
                        return $this->ajax_json(array(
                            'status'   => false,
                            's_code'   => $from_client['s_code'],
                            'messages' => $validate_client_from,
                        ));
                    }

                    $validate_client_to = $this->reconcile_amount_model->validate_adjust_money($to_client['id'], round($input_d_money));
                    if ($validate_client_to) {
                        return $this->ajax_json(array(
                            'status'   => false,
                            's_code'   => $to_client['s_code'],
                            'messages' => $validate_client_to,
                        ));
                    }
//#9547:End
                    $data_client_from = array(
                        'client_id' => $client_id,
                        'payment_date' => $month_pay,
                        'account_holder' => '入金分割',
                        'amount' => round(-$input_d_money),
                        'comment' =>  $input_b_d_comment,
                        'lastup_account_id'  => $this->auth->get_account_id(),
                        'create_datetime' => date("Y-m-d H:i:s"),
                        'lastup_datetime' => date("Y-m-d H:i:s"),
                        'disable' => 0
                    );

                    $data_client_to = array(
                        'client_id' => $in_s_code,
                        'payment_date' => $month_pay,
                        'account_holder' => '入金分割',
                        'amount' => round($input_d_money),
                        'comment' =>  $input_a_d_comment,
                        'lastup_account_id'  => $this->auth->get_account_id(),
                        'create_datetime' => date("Y-m-d H:i:s"),
                        'lastup_datetime' => date("Y-m-d H:i:s"),
                        'disable' => 0
                    );

                    

                    if ($this->payment_past_data->insert_data($data_client_from)) {
                        $this->reconcile_amount_model->update_payment_entry_after_edit($client_id);
                    }

                    if ($this->payment_past_data->insert_data($data_client_to)) {
                        $this->reconcile_amount_model->update_payment_entry_after_edit($in_s_code);
                    }

                    $result = array(
                        'status' => AJAX_SUCCESS,
                        'message'=> lang('common_insert_successfully'),
                        'month' => $month,
                        'year' => $year,
                        'client_id' => $client_id,
                        'in_s_code' => $in_s_code,
                        'input_d_money' => number_format(round(-$input_d_money)),
                        'input_b_d_comment' => $input_b_d_comment,
                        'input_a_d_comment' => $input_a_d_comment,
                        'month_date' => date('Y/m/t',strtotime($int_date)),
                        'account_holder' => '入金分割',

                    );
//                    }
//#9547:End
                }
            } else {
                $result = array(
                    'status' => AJAX_FAIL,
                    'errors' =>'',
                    'message' => lang('msg_status_payment'),
                    'valid' => $valid,
                    'total' => $total_money
                );
            }
        } else {
            $result = array(
                'status' => AJAX_FAIL,
                'errors'  => $this->form_validation->error_array() + $error_data,
                'valid' => $valid
            );
        }

        $this->ajax_json($result);
    }

    /**
     * export reconcile data to csv
     * @author  cam_tien
     */
    public function export_csv() {
        if ($this->input->method() != 'post') show_404();
        if (!$this->input->post('summary_month')) show_404();

        $this->load->model('reconcile_amount_model');
        $this->load->helper('download');

        $summary_month  = $this->input->post('summary_month');
        $s_code         = $this->input->post('s_code');
        $remaining_srub = $this->input->post('remaining_srub');
		$filename = filename_to_urlencode("奉行インポートデータ_" . date('Ymd') .'.csv');
        force_download($filename,  $this->reconcile_amount_model->export_csv($summary_month, $s_code, $remaining_srub));
    }

    public function ajax_get_progress() {
        if (!$this->input->get('unique_id')) show_404();

        session_write_close();
        $unique_id = $this->input->get('unique_id');
        $file_name = FCPATH . 'public/admin/reconcile_amount/' . $unique_id . '.json';
        $content   = null;

        do {
            if (file_exists($file_name)) {
                $fp = fopen($file_name, 'rb');
                $content = json_decode(stream_get_contents($fp), true);
                fclose($fp);
            } else {
                $content = array('status' => 0);
                break;
            }
        } while ($content == null);

        if ($content != null && $content['status'] >= 100) {
            $fp = fopen($file_name, 'rb');
            fclose($fp);
            array_map('unlink', glob(FCPATH . "public/admin/reconcile_amount/*.json"));
        }
        
        $this->ajax_json($content);
    }
}