<?php
class Sales extends ZR_Controller
{
    public function show()
    {
        ini_set("memory_limit","1024M");
        $this->load->models(array('sales_slip_model', 'account_model', 'product_model', 'business_unit_model', 'order_model', 'gui_parts_element_model', 'closing_accountant_model'));
        $accounts = $this->account_model->get_account_department_business_unit('', 'is_product_division', false);
        $closing_accountant_date = $this->closing_accountant_model->get_closest_closing_date();
        $closing_accountant_date = new DateTime($closing_accountant_date[0]['closest_closing_date']);
        $default_delivery_date_from = clone $closing_accountant_date;
        $default_delivery_date_from->modify('first day of next month');

        $can_approve = $this->auth->has_permission('sales_approval', 'column');
        if ($this->auth->has_permission('all_invoices', 'column')) {
            $can_create = 'all';
        } elseif ($this->auth->has_permission('self_invoices', 'column')) {
            $can_create = 'self';
        } else {
            $can_create = 'none';
        }

        // Order data
        $this->order_model->set_account_follow_bussiness_unit();
        $tax_division = $this->gui_parts_element_model->get_by_gui_parts_column_name('tax_division', 'code,name');
        $tax_division = json_encode($tax_division);
        $auth_data = $this->auth->get_auth_data();
        $j_accounts = $this->order_model->get_j_account($auth_data['account_id']);
        $bu_id_user_logged = $j_accounts[0]->bu_id;

        $products = $this->order_model->get_products_by_bu_id($j_accounts[0]->bu_id);
        $products = json_encode($products);
        if ($this->auth->has_permission('order_add_edit_his_only', 'column')) {
            $j_accounts = $this->order_model->get_j_account_by_bu_id($j_accounts[0]->bu_id);
        }
        $this->lang->load('order', $this->config->item('language'));
        $this->assign('j_accounts', $j_accounts);
        $this->assign('bu_id_user_logged', $bu_id_user_logged);
        $this->assign('products', $products);
        $this->assign('tax_division', $tax_division);
        $this->assign('can_approve', $can_approve ? 'true' : 'false');
        $this->assign('can_create', $can_create);
        $this->assign('account_data', $accounts);
        $this->assign('login_account_id', $this->auth->get_account_id());
        $this->assign('business_unit_options', map_element($this->business_unit_model->get_product_business_unit_id_name(), 'id', 'name'));
        $this->assign('default_delivery_date_from', $default_delivery_date_from);
        $this->assign('closing_accountant_date', $closing_accountant_date->format('Y-m-d'));
        $this->assign('order_acceptance_input_change', json_encode($this->auth->has_permission('acceptance_input_change', 'column')));
        $this->assign('order_acceptance_view', json_encode($this->auth->has_permission('order_acceptance_view', 'column')));
        $this->view('sales/show.tpl');
    }

    public function get_data()
    {
        $this->load->models(array('sales_slip_model'));

        $post = $this->input->post();

        if (!$this->auth->has_permission('all_invoices', 'column') && $this->auth->has_permission('self_invoices', 'column')) {
            $post['search_j_account'] = $this->auth->get_account_id();
        }


        #7067:Start

//  		$standadize_jcode_range = $this->standadize_jcode_range($post["search_j_code_branch_cd_from"], 0);
//         if (!is_null($standadize_jcode_range))
// 			$post["search_j_code_branch_cd_from"] = $standadize_jcode_range;

//  		$standadize_jcode_range = $this->standadize_jcode_range($post["search_j_code_branch_cd_to"], 9);
//         if (!is_null($standadize_jcode_range))
// 			$post["search_j_code_branch_cd_to"] = $standadize_jcode_range;

        #7067:End

        $search_params = $this->sales_slip_model->search_sales_slip_validate($post);

        if ($search_params[0]) {
//#7087:Start
//            $this->ajax_json($this->sales_slip_model->filter($search_params[1])->get_data());
            $this->ajax_json($this->sales_slip_model->filter($search_params[1])->get_data(false, OVER_RECORD));
//#7087:End
        } else {
            $this->ajax_json(array('error' => $search_params[1]));
        }
    }

    public function apply_approval()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->models(array('sales_slip_model'));

        $data = json_decode($this->input->post('data'), true);

        $apply_ids = array();
        foreach ($data as $row) {
            if ($row['to_be_applied']) {
                $apply_ids[] = $row['sales_slip_id'];
            }
        }

        if ($this->sales_slip_model->apply_approval($apply_ids)) {
            $this->ajax_json(array(
                'status' => AJAX_SUCCESS,
                'message' => lang('common_bulk_successfully_edit')
            ));
        } else {
            $this->ajax_json(array(
                'status' => AJAX_FAIL,
                'message' => $this->sales_slip_model->error ? $this->sales_slip_model->error : lang('common_could_not_edit')
            ));
        }
    }

    public function approve()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->models(array('sales_slip_model'));
        $this->load->model('account_model');

        $data = json_decode($this->input->post('data'), true);

        $approve_ids = array();
        $can_approved = true;

        $arr_info_user_logged = $this->account_model->get_account_department_business_unit($this->auth->get_account_id());
        $business_unit_user_logged = $arr_info_user_logged[0]['business_unit_id'];

        foreach ($data as $row) {
            if ($row['to_be_approved']) {
                $approve_ids[] = $row['sales_slip_id'];
                // Check business unit of j_account_id of sales_slip need approve, just business_unit together with account logged can approve.
                $arr_info_user_sale_slip_detail = $this->account_model->get_account_department_business_unit($row['j_account_id']);
                if ($business_unit_user_logged != $arr_info_user_sale_slip_detail[0]['business_unit_id']) {
                    $can_approved = false;
                }
            }
        }

        if (!$can_approved) {
            $this->ajax_json(array(
                'status' => AJAX_FAIL,
                'message' => '売上伝票承認できません。'
            ));
        }

        if ($this->sales_slip_model->approve($approve_ids)) {
            $this->ajax_json(array(
                'status' => AJAX_SUCCESS,
                'message' => lang('msg_approve_successful')
            ));
        } else {
            $this->ajax_json(array(
                'status' => AJAX_FAIL,
                'message' => $this->sales_slip_model->error ? $this->sales_slip_model->error : lang('common_could_not_edit')
            ));
        }
    }

    /**
     * Approve Sales slip
     * @author cong_tien
     */
    public function approval() {
        if (!$this->input->is_ajax_request() || !$this->input->post('sales_slip_id')) {
            show_404();
        }

        $this->load->models(array('sales_slip_model'));

        $params = array(
            $this->input->post('sales_slip_id') => array(
                'id'        => $this->input->post('sales_slip_id'),
                'comment'   => $this->input->post('sales_slip_comment')
            )
        );

        if ($this->sales_slip_model->approves($params)) {
            $this->ajax_json(array(
                'status'    => AJAX_SUCCESS,
                'message'   => lang('common_edit_sales_successfully')
            ));
        } else {
            $this->ajax_json(array(
                'status'    => AJAX_FAIL,
                'message'   => $this->sales_slip_model->error ? $this->sales_slip_model->error : lang('common_could_not_edit')
            ));
        }
    }

    /**
     * Reject Sales slip
     * @author cong_tien
     */
    public function reject() {
        if (!$this->input->is_ajax_request() || !$this->input->post('sales_slip_id')) {
            show_404();
        }

        $this->load->models(array('sales_slip_model'));

        $params = array(
            'id'        => $this->input->post('sales_slip_id'),
            'comment'   => $this->input->post('sales_slip_comment')
        );

        if ($this->sales_slip_model->reject($params)) {
            $this->ajax_json(array(
                'status'    => AJAX_SUCCESS,
                'message'   => lang('common_reject_successfully')
            ));
        } else {
            $this->ajax_json(array(
                'status'    => AJAX_FAIL,
                'message'   => $this->sales_slip_model->error ? $this->sales_slip_model->error : lang('common_could_not_edit')
            ));
        }
    }

    /**
     * View popup order acceptance
     * @author cong_tien
     */
    public function order_acceptance() {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->models(array('order_acceptance_model'));
        $this->lang->load('order_acceptance', $this->config->item('language'));

        $j_code     = $this->input->post('j_code');
        $branch_cd  = $this->input->post('branch_cd');

        $options = array(
            'fields' => 'order_acceptance.set_name, order_acceptance.weight, order_acceptance.quantity, order_acceptance.delivery_date, order_acceptance.amount,
                        subcontractor.name AS subcontractor_name, shipping_type.name AS shipping_type_name',
            'conditions' => array(
                'order_acceptance_status' => ORDER_ACCEPTANCE_APPROVED,
                'j_code'                  => $j_code,
                'branch_cd'               => $branch_cd
            ),
            'joins' => array(
                'subcontractor' => 'subcontractor.id = order_acceptance.source_subcontractor_id AND subcontractor.disable = 0',
                'shipping_type' => 'shipping_type.id = order_acceptance.shipping_type_cd AND shipping_type.disable = 0'
            )
        );

        $order_acceptances = $this->order_acceptance_model->get_order_acceptances('all', $options);

        $this->assign('j_code', $j_code);
        $this->assign('branch_cd', $branch_cd);
        $this->assign('order_acceptances', $order_acceptances);

        $this->view('sales/order_acceptance.tpl');
    }

    /**
     * Approve Sales slip
     * @author cong_tien
     */
    public function approval_to_pending() {
        if (!$this->input->is_ajax_request() || !$this->input->post('sales_slip_id')) {
            show_404();
        }

        $this->load->models(array('sales_slip_model'));

        $params = array('id' => $this->input->post('sales_slip_id'));

        if ($this->sales_slip_model->approval_to_pending($params)) {
            $this->ajax_json(array(
                'status'    => AJAX_SUCCESS,
                'message'   => lang('common_edit_sales_successfully')
            ));
        } else {
            $this->ajax_json(array(
                'status'    => AJAX_FAIL,
                'message'   => $this->sales_slip_model->error ? $this->sales_slip_model->error : lang('common_could_not_edit')
            ));
        }
    }

    public function add_to_sales_slip()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->models(array('sales_slip_model'));

        $slip_number = $this->input->post('slip_number');
        $detail_ids = $this->input->post('sales_slip_detail_id');

        if ($this->sales_slip_model->add_to_sales_slip($slip_number, $detail_ids)) {
            $this->ajax_json(array(
                'status' => AJAX_SUCCESS,
                'message' => lang('common_edit_successfully')
            ));
        } else {
            $this->ajax_json(array(
                'status' => AJAX_FAIL,
                'message' => $this->sales_slip_model->error ? $this->sales_slip_model->error : lang('common_could_not_edit')
            ));
        }
    }

    public function change()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->models(array('sales_slip_model'));

        $data = json_decode($this->input->post('data'), true);

        $change_list = $errors = array();
        foreach ($data as $row) {
            $change_list[] = $row;
            if ($row['to_be_updated'] && !$row['to_be_deleted']) {
                $row_error = $this->sales_slip_model->validate_ssd($row);
                if (!empty($row_error)) {
                    $errors[$row['j_code'] . $row['branch_cd']] = $row_error;
                }
            }
        }

        if (empty($errors)) {
            if ($this->sales_slip_model->do_update($change_list)) {
                $this->ajax_json(array(
                    'status' => AJAX_SUCCESS,
                    'message' => lang('common_successfully_edit')
                ));
            } else {
                $this->ajax_json(array(
                    'status' => AJAX_FAIL,
                    'message' => $this->sales_slip_model->error ? $this->sales_slip_model->error : lang('common_could_not_edit')
                ));
            }
        } else {
            $this->ajax_json(array(
                'status' => AJAX_FAIL,
                'errors' => $errors
            ));
        }
    }

    /**
     * View sales slip
     * @author van_don
     */
    public function detail() {

        $this->load->model('sales_slip_model');
        $this->load->model('order_model');
        $this->load->model('account_model');

        $sales_data = $this->sales_slip_model->get_sales_slip_info($this->input->post('id'));

        //Get sales slip detail
        $this->load->helper('zenrin_system_helper');
        $sales_slip_detail = sales_slip_export_pdf($this->input->post('id'));

        $is_no_data = true;
        if (!$sales_data || !$sales_slip_detail) {
            $is_no_data = false;
            $this->assign('is_no_data', $is_no_data);
            $this->view('sales/detail.tpl');
            exit;
        }

        $sales_data['billing_date'] = (($sales_data['billing_date'] == '0000-00-00') ? '' :  date('Y/m/d', strtotime($sales_data['billing_date'])));
        $sales_data['payment_date'] = (($sales_data['payment_date'] == '0000-00-00') ? '' :  date('Y/m/d', strtotime($sales_data['payment_date'])));
        $sales_data['zipcode'] = substr_replace($sales_data['zipcode'], '-', 3, 0);
        //Get order_acceptace info
        //$order_acceptance_info = $this->sales_slip_model->get_order_acceptance_info($sales_data['order_id']);

        $is_fix_payment = false;
        // Check accounts from sale slip detail and user login have business unit together => can approve sale slip
        $can_approve_sales_slip = true;

        $arr_info_user_logged = $this->account_model->get_account_department_business_unit($this->auth->get_account_id());
        $business_unit_user_logged = $arr_info_user_logged[0]['business_unit_id'];

        foreach ($sales_slip_detail as $key => &$slip_detail) {

            if (isset($slip_detail['is_show_minus']) || !$slip_detail['is_ad_receive_payment']) {
                $is_fix_payment = true;
            }
            /*
            if ($order_acceptance_info) {
                $slip_detail['delivery_date'] = $order_acceptance_info->delivery_date;
            }*/
//#7045:Start
//             if (!is_null($slip_detail['order_delivery_date']) && ($slip_detail['order_delivery_date'] != '0000-00-00'))
//                 $slip_detail['delivery_date'] = $slip_detail['order_delivery_date'];
//#7045:End
            $slip_detail['delivery_date'] = date('Y/m/d', strtotime($slip_detail['delivery_date']));

            // check business unit of accounts in sale slip
            $arr_info_user_sale_slip_detail = $this->account_model->get_account_department_business_unit($slip_detail['j_account_id']);

            if ($arr_info_user_sale_slip_detail[0]['business_unit_id'] !=  $business_unit_user_logged) {
                $can_approve_sales_slip  = false;
            }

        }
        $sales_data['sales_slip_number'] = (($is_fix_payment) ? 'SC' : 'SA') . $sales_data['sales_slip_number'];
        //Check permisson to edit or approve
        $is_approval = false;
        $is_edit = false;
        $sales_slip_status = array(SALES_STATUS_CREATED, SALES_STATUS_APPROVAL_PENDING);

        if ($this->auth->has_permission('all_invoices', 'column')) {
            if (in_array($sales_data['sales_slip_status'], $sales_slip_status)) {
                $is_edit = true;
            }

            if ($sales_data['sales_slip_status'] == SALES_STATUS_CREATED) {
                $is_approval = true;
            }
        }
        elseif ($this->auth->has_permission('self_invoices', 'column')) {
            if ($this->auth->get_account_id() == $sales_data['j_account_id']) {

                if (in_array($sales_data['sales_slip_status'], $sales_slip_status)) {
                    $is_edit = true;
                }

                if ($sales_data['sales_slip_status'] == SALES_STATUS_CREATED) {
                    $is_approval = true;
                }
            }
        }

        //Check permisson to approve or reject
        $is_edit_approval_reject = false;
        if ($this->auth->has_permission('sales_approval', 'column') && ($sales_data['sales_slip_status'] == SALES_STATUS_APPROVAL_PENDING)) {
            $is_edit_approval_reject = true;
        }



//         const SALES_STATUS_INVOICE_APPROVED = 22;
//         const SALES_STATUS_INVOICE_ISSUED = 23;
//         const SALES_STATUS_INVOICED_DISCARDED = 29;

        //Check permisson to download pdf
        $sales_slip_status = array(SALES_STATUS_INVOICE_APPROVED, SALES_STATUS_INVOICE_ISSUED, SALES_STATUS_INVOICED_DISCARDED);
        $is_pdf_download = false;
        if ($this->auth->has_permission('invoice_approve', 'column')) {
            if (($sales_data['sales_slip_number'])
                    && in_array($sales_data['sales_slip_status'], $sales_slip_status)) {
                $is_pdf_download = true;
            }
        }

        //Get tax type
        $this->load->model('sales_slip_model');
        $tax_type = $this->sales_slip_model->tax_options();

        $this->assign('can_approve_sales_slip', $can_approve_sales_slip);
        $this->assign('is_no_data', $is_no_data);
        $this->assign('tax_type', $tax_type);
        $this->assign('sales_slip_status', $this->sales_slip_model->status_options());
        $this->assign('sales_data', $sales_data);
        $this->assign('sales_slip_detail', $sales_slip_detail);
        $this->assign('is_approval', $is_approval);
        $this->assign('is_edit', $is_edit);
        $this->assign('is_edit_approval_reject', $is_edit_approval_reject);
        $this->assign('is_pdf_download', $is_pdf_download);
        $this->assign('is_fix_payment', $is_fix_payment);

        $this->view('sales/detail.tpl');
    }


    /**
     * Exporting sales slip detail to pdf
     * @author van_don
     */
    public function pdf_export() {

        $this->load->library('tcpdf/Tcpdf.php');
        $this->load->library('tcpdf/fpdi.php');
        $this->load->helper('zenrin_pdf_helper');


        //Check permisson to download pdf

        if (!$this->auth->has_permission('invoice_approve', 'column')) {
            $this->app_logger->error_log("You do not have permission to download pdf file" . __LINE__);
            redirect('/_admin/error/access_deny');
        }

        //Load model
        $this->load->model('sales_slip_model');
        $this->load->model('client_model');

        $sales_slip_id = $this->input->post('sales_slip_id');

        //Get sales slip info
        $sale_slip = $this->sales_slip_model->get_sales_slip_info($sales_slip_id);


        //Check permisson to download pdf
        $sales_slip_status = array(SALES_STATUS_INVOICE_APPROVED, SALES_STATUS_INVOICE_ISSUED, SALES_STATUS_INVOICED_DISCARDED);

        //Check sales slip status to download
        if (!$sale_slip || ($sale_slip['sales_slip_number'] == '') || !in_array($sale_slip['sales_slip_status'], $sales_slip_status)) {
            $this->app_logger->error_log("Can not get sales slip info. " . __LINE__);
            redirect('/_admin/error/access_deny');
        }
        //Get client info
        if (!$client = $this->client_model->get_client_by_id($sale_slip['client_id'])) {
            $this->app_logger->error_log("Can not get client info. " . __LINE__);
            redirect('/_admin/error/access_deny');
        }

        //Get sales slip detail
        $this->load->helper('zenrin_system_helper');
        if (!$sale_slip_detail = sales_slip_export_pdf($sales_slip_id)) {
            $this->app_logger->error_log("Can not get sales slip detail info. " . __LINE__);
            redirect('/_admin/error/access_deny');
        }
        $this->load->model('gui_parts_element_model');
        //Get tax_type
        $tax_type = array();
        if ($tax_division = $this->gui_parts_element_model->get_by_gui_parts_column_name('tax_division', 'code,name')) {
            foreach ($tax_division as $key => $type) {
                $tax_type[$type['code']] = $type['name'];
            }
        }

        //Get department info
        $this->load->model('department_model');
        $department = $this->department_model->get_department_by_account_id($sale_slip_detail[0]['j_account_id']);

        //Start generating to pdf
        $fpdi = new FPDI();
        $regularFont     = 'msgothici';

        $fpdi->SetFont($regularFont, '', 11);
        $fpdi->setPrintHeader(false);
        $fpdi->setPrintFooter(false);
        $fpdi->SetAutoPageBreak(TRUE, 0);

        // initialize row
        $header_footer_total_row    = 23;
        $header_no_footer_total_row = 23;
        $body_footer_total_row      = 31;
        $body_no_footer_total_row   = 34;

        $fpdi->AddPage('L', 'A4');
        $fpdi->setSourceFile(APPPATH . 'config/pdf_template/sales/header_footer.pdf');
        $y = 0;

        $cnt_sales_slip = count($sale_slip_detail);
        if ($cnt_sales_slip > $header_footer_total_row) {
            $fpdi->setSourceFile(APPPATH . 'config/pdf_template/sales/header.pdf');
            $y = 0;
        }

        $header_id = $fpdi->importPage(1);
        $fpdi->useTemplate($header_id, null, null, null, null, true);

        $page_estimator = page_estimator($cnt_sales_slip, $header_footer_total_row, $header_no_footer_total_row, $body_footer_total_row, $body_no_footer_total_row);

        for ($i = 1; $i <= $page_estimator['body']; $i++) {
            $fpdi->AddPage('L', 'A4');
            $fpdi->setSourceFile(APPPATH . 'config/pdf_template/sales/body.pdf');
            $body_id = $fpdi->importPage(1);
            $fpdi->useTemplate($body_id, null, null, null, null, true);
        }

        for ($i = 1; $i <= $page_estimator['footer']; $i++) {
             $fpdi->AddPage('L', 'A4');
             $fpdi->setSourceFile(APPPATH . 'config/pdf_template/sales/footer.pdf');
             $footer_id = $fpdi->importPage(1);
             $fpdi->useTemplate($footer_id, null, null, null, null, true);
        }
        $page = 1;
        $fpdi->setPage($page);

        // s_code
        $fpdi->setXY(48, 14.5 + $y);
        $fpdi->setFontSize(11);
        $fpdi->cell(25, 1, $client['s_code'], 0, '', 'L');


        $invoice_approval_account_date = new DateTime($sale_slip['invoice_approval_account_date']);

        // Set title date
        $fpdi->setXY(175, 16.5 + $y);
        $fpdi->setFontSize(10);
        $fpdi->cell(25, 10, $invoice_approval_account_date->format('Y'), 0, '', 'L');

        $fpdi->setXY(185.7, 16.5 + $y);
        $fpdi->setFontSize(10);
        $fpdi->cell(25, 10, $invoice_approval_account_date->format('m'), 0, '', 'L');

        $fpdi->setXY(192.2, 16.5 + $y);
        $fpdi->setFontSize(10);
        $fpdi->cell(25, 10, $invoice_approval_account_date->format('d'), 0, '', 'L');

        // client zipcode
        $fpdi->setXY(30, 38.7 + $y);
        $fpdi->setFontSize(9);
        $fpdi->cell(25, 1, substr_replace($sale_slip['zipcode'], '-', 3, 0), 0, '', 'L');

        // address
        $fpdi->setXY(24, 41 + $y);
        $fpdi->setFontSize(9);
        $fpdi->cell(176, 8, $sale_slip['address_1st'], 0, '', 'L');
        if ($sale_slip['address_2nd']) {
            // address 2
            $fpdi->setXY(24, 46 + $y);
            $fpdi->setFontSize(9);
            $fpdi->cell(176, 8, $sale_slip['address_2nd'], 0, '', 'L');
        }

        // client_name
        $fpdi->setFontSize(11);
        $fpdi->setXY(24, 46.5 + $y);
        $fpdi->cell(150, 15.5, $sale_slip['client_name'], 0, '', 'L');

        //部署名　担当者　様
        $fpdi->setFontSize(11);
        $fpdi->setXY(24, 51 + $y);
        $fpdi->cell(150, 15.5, $sale_slip['division_name'], 0, '', 'L');

        $fpdi->setFontSize(11);
        $fpdi->setXY(35, 56 + $y);
        $fpdi->cell(150, 15.5, $sale_slip['charge_name'] .'様', 0, '', 'L');

        // tel
        $fpdi->setXY(33, 69 + $y);
        $fpdi->setFontSize(9);
        $fpdi->cell(70, 7, $sale_slip['phone_no'], 0, '', 'L');

        // fax
        $fpdi->setXY(70, 69 + $y);
        $fpdi->setFontSize(9);
        $fpdi->cell(70, 7, $sale_slip['fax_no'], 0, '', 'L');

        //Account info
        // fax
        $fpdi->setXY(143, 57 + $y);
        $fpdi->setFontSize(9);
        $fpdi->cell(25, 1, substr_replace($department['zipcode'], '-', 3, 0), 0, '', 'L');

        //Set address
        $fpdi->setXY(145, 61.2 + $y);
        $fpdi->setFontSize(9);
        $fpdi->cell(25, 1, $department['address_1'], 0, '', 'L');

        $fpdi->setXY(145, 65.8 + $y);
        $fpdi->setFontSize(9);
        $fpdi->cell(25, 1, $department['address_2'], 0, '', 'L');

        //Set tel
        $fpdi->setXY(145, 70.4 + $y);
        $fpdi->setFontSize(9);
        $fpdi->cell(25, 1, $department['phone_no'], 0, '', 'L');

        //Set fax
        $fpdi->setXY(177, 70.4 + $y);
        $fpdi->setFontSize(9);
        $fpdi->cell(25, 1, $department['fax_no'], 0, '', 'L');

        //Set account name
        $fpdi->setXY(163, 74.7 + $y);
        $fpdi->setFontSize(10);
        $fpdi->cell(25, 1, $department['name'], 0, '', 'L');

        // initialze break page coordination
        $detail_y = 0;
        $start_detail_coor_y = 86.2;
//         $page = 2;
        $row_height = 6.5;
        $cnt_row = 0;
        $is_first_page = true;
        $is_fix_payment = false;

        foreach ($sale_slip_detail as $row => $detail) {

            $cnt_row++;
            // row number
            $fpdi->setFontSize(8);
            $content_coor_y = $start_detail_coor_y + $y + $detail_y;

            //Set j_code + branch_code
            $fpdi->setXY(26.5, $content_coor_y);
            $fpdi->cell(18, $row_height + 2.5, $detail['j_code'] . $detail['branch_cd'], 0, '', 'C');

            //Set delivery_date
//#7045:Start
//             if (!is_null($detail['order_delivery_date']) && ($detail['order_delivery_date'] != '0000-00-00'))
//                 $detail['delivery_date'] = $detail['order_delivery_date'];
//#7045:End
            $fpdi->setXY(45, $content_coor_y);
            //$fpdi->cell(18, $row_height + 2.5, (($order_acceptance_info) ? $order_acceptance_info->delivery_date : $detail['delivery_date']), 0, '', 'C');
            $delivery_date = date_create($detail['delivery_date']);
            $fpdi->cell(18, $row_height + 2.5, date_format($delivery_date, "Y/m/d"), 0, '', 'C');

            //Set product_name
            $fpdi->setXY(62, $content_coor_y);
            $fpdi->cell(70, $row_height, $detail['product_name'], 0, '', 'C');
            //Set contents
            $fpdi->setXY(62, $content_coor_y);
            $fpdi->setCellPaddings('', '', 2, '');
            $fpdi->cell(70, $row_height + 6.5, $detail['contents'], 0, '', 'C', false, '', 1);
            $detail_y += $row_height + 0.27;

            if ($detail['is_ad_receive_payment'] != 1) {
                $is_fix_payment = true;
                //Set taxt type
                $fpdi->setXY(125, $content_coor_y);
                $fpdi->cell(18, $row_height + 2.5, $tax_type[$detail['tax_type']], 0, '', 'C');

                //Set quantity
                if ($detail['quantity'] < 0) {
                    $fpdi->writeHTMLCell(17, $row_height + 2.5, 136, $content_coor_y + 2.5, '<font color="red">' . number_format($detail['quantity']) . '</font>',0,0,0, true, 'R', false, '', 1);
                } else {
                    $fpdi->setXY(135, $content_coor_y);
                    $fpdi->cell(17, $row_height + 2.5, number_format($detail['quantity']), 0, '', 'R', false, '', 1);
                }

                //Set amount
                if ($detail['amount'] < 0) {
                    $fpdi->writeHTMLCell(31, $row_height + 2.5, 170, $content_coor_y + 2.5, '<font color="red">' . number_format($detail['amount']) . '</font>',0,0,0, true,'R', false, '', 1);
                } else {
                    $fpdi->setXY(170, $content_coor_y);
                    $fpdi->cell(31, $row_height + 2.5, number_format($detail['amount']), 0, '', 'R', false, '', 1);
                }

                //Set quantity
                if ($detail['unit_price'] < 0) {
                    $fpdi->writeHTMLCell(31, $row_height + 2.5, 144.5, $content_coor_y + 2.5, '<font color="red">' . number_format($detail['unit_price'], 2) . '</font>',0,0,0, true,'R', false, '', 1);
                } else {
                    $fpdi->setXY(153.5, $content_coor_y);
                    $fpdi->cell(21, $row_height + 2.5, number_format($detail['unit_price'], 2), 0, '', 'R', false, '', 1);
                }
            } else {
                if ($detail['is_ad_receive_payment_sales'] == 1) {
                    $minus = '';
                    if (isset($detail['is_show_minus'])) {
                        $is_fix_payment = true;
                        $minus = '-';
                        //Set taxt type
                        $fpdi->setXY(125, $content_coor_y);
                        $fpdi->writeHTMLCell(19, $row_height + 2.5, 125, $content_coor_y + 2.5, '<font color="red">' . $tax_type[$detail['tax_type']] . '</font>',0,0,0, true, 'C');
                        $fpdi->writeHTMLCell(17, $row_height + 2.5, 136, $content_coor_y + 2.5, '<font color="red">' . $minus . number_format($detail['quantity']) . '</font>',0,0,0, true, 'R', false, '', 1);
                        $fpdi->writeHTMLCell(31, $row_height + 2.5, 170, $content_coor_y + 2.5, '<font color="red">' . $minus . number_format($detail['amount']) . '</font>',0,0,0, true,'R', false, '', 1);
                        $fpdi->writeHTMLCell(31, $row_height + 2.5, 144.5, $content_coor_y + 2.5, '<font color="red">' . number_format($detail['unit_price'], 2) . '</font>',0,0,0, true,'R', false, '', 1);

                    } else {
                        //Set taxt type
                        $fpdi->setXY(125, $content_coor_y);
                        $fpdi->cell(18, $row_height + 2.5, $tax_type[$detail['tax_type']], 0, '', 'C');
                        $fpdi->writeHTMLCell(17, $row_height + 2.5, 136, $content_coor_y + 2.5,  number_format($detail['quantity']) ,0,0,0, true, 'R', false, '', 1);
                        $fpdi->writeHTMLCell(31, $row_height + 2.5, 170, $content_coor_y + 2.5,  number_format($detail['amount']) ,0,0,0, true,'R', false, '', 1);
                        $fpdi->writeHTMLCell(31, $row_height + 2.5, 144.5, $content_coor_y + 2.5,  number_format($detail['unit_price'], 2) ,0,0,0, true,'R', false, '', 1);
                    }
                } else {

                    $fpdi->setXY(125, $content_coor_y);
                    $fpdi->cell(18, $row_height + 2.5, $tax_type[$detail['tax_type']], 0, '', 'C');

                    //Set quantity
                    if ($detail['quantity'] < 0) {
                        $fpdi->writeHTMLCell(17, $row_height + 2.5, 136, $content_coor_y + 2.5, '<font color="red">' . number_format($detail['quantity']) . '</font>',0,0,0, true, 'R', false, '', 1);
                    } else {
                        $fpdi->writeHTMLCell(17, $row_height + 2.5, 136, $content_coor_y + 2.5,  number_format($detail['quantity']) ,0,0,0, true, 'R', false, '', 1);
                    }

                    //Set amount
                    if ($detail['amount'] < 0) {
                        $fpdi->writeHTMLCell(31, $row_height + 2.5, 170, $content_coor_y + 2.5, '<font color="red">' . number_format($detail['amount']) . '</font>',0,0,0, true,'R', false, '', 1);
                    } else {
                        $fpdi->writeHTMLCell(31, $row_height + 2.5, 170, $content_coor_y + 2.5,  number_format($detail['amount']) ,0,0,0, true,'R', false, '', 1);
                    }

                    //Set quantity
                    if ($detail['unit_price'] < 0) {
                        $fpdi->writeHTMLCell(31, $row_height + 2.5, 144.5, $content_coor_y + 2.5, '<font color="red">' . number_format($detail['unit_price'], 2) . '</font>',0,0,0, true,'R', false, '', 1);
                    } else {
                        $fpdi->writeHTMLCell(31, $row_height + 2.5, 144.5, $content_coor_y + 2.5,  number_format($detail['unit_price'], 2) ,0,0,0, true,'R', false, '', 1);
                    }
                }
            }

            if ($is_first_page && $cnt_row == 23 && $cnt_sales_slip > 23) {
                $start_detail_coor_y = 19.8;
                $row_height =  6.5;
                $detail_y = 0;
                $page ++;
                $fpdi->setPage($page);
                $cnt_row = 0;
                $is_first_page = false;
           } elseif (!$is_first_page && $cnt_row == $body_no_footer_total_row) {
                $start_detail_coor_y = 19.8;
                $row_height =  6.5;
                $detail_y = 0;
                $page ++;
                $fpdi->setPage($page);
                $cnt_row = 0;
                $is_first_page = false;
            }
        }

        //Set sales slip number
        $fpdi->setXY(176, 12 + $y);
        $fpdi->setPage(1);
        $fpdi->setFontSize(10);
        $fpdi->cell(25, 10, (($is_fix_payment) ? 'SC' : 'SA') . $sale_slip['sales_slip_number'], 0, '', 'R');


        $y_total_amount = 241;
        $y_total_tax = 248;
        $y_total_amount_tax = 255;

        // page number
        if ($page_estimator['total'] > 1) {
            for ($i = 1; $i <= $page_estimator['total']; $i++) {
                $fpdi->setPage($i);
                $fpdi->setFontSize(9);
                $fpdi->SetXY(25, 260);
                $page_numer = $i . '/' . $page_estimator['total'];
                $fpdi->cell(170, 5, $page_numer.' ページ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            }
            $y_total_amount = 229;
            $y_total_tax = 236;
            $y_total_amount_tax = 243;
        }

        //Set footer data

        $fpdi->setPage($page);
        $fpdi->setFontSize(13);

        //Set total
        $fpdi->SetXY(174, $y_total_amount);
        $fpdi->cell(27, 13, number_format($sale_slip['total_amount']), 0, '', 'R', false, '', 1);

        //Set total
        $fpdi->SetXY(174, $y_total_tax);
        $fpdi->cell(27, 13, number_format($sale_slip['total_tax']), 0, '', 'R', false, '', 1);

        //Set total
        $fpdi->SetXY(174, $y_total_amount_tax);
        $fpdi->cell(27, 13, number_format($sale_slip['total_amount_tax']), 0, '', 'R', false, '', 1);

        $file_name = '売上伝票' . $client['name'] . '様' . (($is_fix_payment) ? 'SC' : 'SA') . $sale_slip['sales_slip_number'] . '.pdf';
        $file_name = filename_to_urlencode($file_name);
        $fpdi->Output($file_name, 'D');
    }

    public function add()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->model('sales_slip_model');

        if ($action = $this->input->post('action')) {
            if ($action != 'add'){
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('common_could_not_add')));
            }

//#8149:Start
//          if (!($sales_slip_param = $this->input->post('sales_slip')) || !($sales_slip_detail_param = $this->input->post('sales_slip_detail'))) {
            if (!($sales_slip_param = $this->input->post('sales_slip')) || !($sales_slip_detail_param = $this->input->post('sales_slip_detail')) || !($sales_slip_detail_sequence = $this->input->post('sequence'))) {
//#8149:End
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('common_could_not_add')));
            }
        }

        $sales_slip_detail_ids = $this->input->post('sales_slip_detail_id');

        // check if all sales slip detail exist
        if (!($sales_slip_detail = $this->sales_slip_model->get_sales_slip_detail_by_params(array('sales_slip_detail.id IN' => '("' . implode('", "', $sales_slip_detail_ids) . '")')))
            || (count($sales_slip_detail) != count($sales_slip_detail_ids))
        ) {
            if ($action) {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('common_could_not_add')));
            } else {
                $this->assign('error_message', lang('common_could_not_add'));
                $this->view('sales/_error.tpl');
                return;
            }
        }
        if (!$this->auth->has_permission('all_invoices', 'column')
            && (!$this->auth->has_permission('self_invoices', 'column')
                || ($this->auth->get_account_id()  != $sales_slip_detail[0]['j_account_id'])
            )
        ) {
            $this->app_smarty->display('error/ajax_access_deny.tpl');
            exit;
        }

        // sales slip detail has to belong to ONLY ONE client
        $client_ids = array_unique(get_element($sales_slip_detail, 'client_id'));

        if (count($client_ids) > 1) {
            if ($action) {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('msg_same_s_code')));
            } else {
                $this->assign('error_message', lang('msg_same_s_code'));
                $this->view('sales/_error.tpl');
                return;
            }
        }

        $client_id = $client_ids[0];

        $this->load->model('client_model');
        if (!$client = $this->client_model->get_item_by_id('client', $client_id)) {
            if ($action) {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('common_could_not_add')));
            } else {
                $this->assign('error_message', lang('common_could_not_add'));
                $this->view('sales/_error.tpl');
                return;
            }
        }

        $this->load->model('order_detail_model');

        // process adding data here then return json result and do not load view
        if ($action) {
            $sales_slip_param['client_id'] = $client['id'];
            $sales_slip_param['client_name'] = $client['name'];
            $sales_slip_param['is_ad_receive_payment'] = $sales_slip_detail[0]['order_is_ad_receive_payment'];
//#7620:Start
//            if (!($sales_slip_detail[0]['order_is_ad_receive_payment'] && $sales_slip_detail[0]['is_ad_receive_payment'])) {
            if (!($sales_slip_detail[0]['order_is_ad_receive_payment'])) {
//#7620:End
                $sales_slip_param['billing_closing_date'] = $sales_slip_detail[0]['billing_date'];
                $sales_slip_param['payment_expire_date'] = $sales_slip_detail[0]['payment_date'];
            }

//#8149:Start
//          if (true === ($result = $this->sales_slip_model->insert_sales_slip($sales_slip_param, $sales_slip_detail_param))) {
            if (true === ($result = $this->sales_slip_model->insert_sales_slip($sales_slip_param, $sales_slip_detail_param, $sales_slip_detail_sequence))) {
//#8149:End
                $this->ajax_json(array('status' => AJAX_SUCCESS, 'message' => lang('msg_create_sales_report_successful')));
            } else {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => $result));
            }
        }

        if ($sales_slip_detail[0]['order_is_ad_receive_payment']) {
            $ad_receive_sales_slip_detail_ids = get_element($sales_slip_detail, 'ad_receive_sales_slip_detail_id');

            $extra_sales_slip_detail = $this->sales_slip_model->get_items_by_parameters(
                'sales_slip_detail',
                null,
                array(
                    'ad_receive_sales_slip_detail_id IN ("' . implode('", "', $ad_receive_sales_slip_detail_ids) . '")',
                    'id NOT IN("' . implode('", "', $sales_slip_detail_ids) . '")',
                    'is_ad_receive_payment = 1',
                    'disable = 0'
                )
            );
        }

        // - Default name of product when first add is token from ORDER data
        // - Add tax rate
        $this->load->model('consumption_tax_model');
        foreach ($sales_slip_detail as $key => $detail) {
            $sales_slip_detail[$key]['tax_rate'] = $this->consumption_tax_model->get_tax_rate_by_date($detail['delivery_date']);
        }
        if (isset($extra_sales_slip_detail)) {
            foreach ($extra_sales_slip_detail as $key => $extra_detail) {
                $extra_sales_slip_detail[$key]['tax_rate'] = $this->consumption_tax_model->get_tax_rate_by_date($extra_detail['delivery_date']);
            }
        }

        $sales_slip = array(
            'zipcode' => $sales_slip_detail[0]['zipcode'],
            'address_1st' => $sales_slip_detail[0]['address_1st'],
            'address_2nd' => $sales_slip_detail[0]['address_2nd'],
            'division_name' => $sales_slip_detail[0]['division_name'],
            'charge_name' => $sales_slip_detail[0]['charge_name'],
            'phone_no' => $sales_slip_detail[0]['phone_no'],
            'fax_no' => $sales_slip_detail[0]['fax_no'],
            'billing_date' => $sales_slip_detail[0]['billing_date'],
            'payment_date' => $sales_slip_detail[0]['payment_date'],
//#7620:Start
//            'is_ad_receive_payment' => $sales_slip_detail[0]['order_is_ad_receive_payment'] && $sales_slip_detail[0]['is_ad_receive_payment'],
            'is_ad_receive_payment' => $sales_slip_detail[0]['order_is_ad_receive_payment'],
//#7620:End
        );

        $this->assign('action', $this->method);

        $this->assign('client', $client);
        $this->assign('sales_slip_detail', $sales_slip_detail);
        $this->assign('old_sales_slip_detail', isset($extra_sales_slip_detail) ? $extra_sales_slip_detail : array());

        $this->assign('sales_slip', $sales_slip);

        $this->view('sales/_form.tpl');
    }

    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }

        $this->load->model('sales_slip_model');

        if (!($sales_slip_id = $this->input->post('sales_slip_id'))) {
            $this->ajax_json(array('status' => AJAX_FAIL));
        }

        if ($action = $this->input->post('action')) {
            if (($action != 'edit') && ($action != 'delete')) {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('common_could_not_edit')));
            }
            if ($action == 'edit') {
//#8149:Start
//              if (!($sales_slip_param = $this->input->post('sales_slip')) || !($sales_slip_detail_param = $this->input->post('sales_slip_detail'))) {
                if (!($sales_slip_param = $this->input->post('sales_slip')) || !($sales_slip_detail_param = $this->input->post('sales_slip_detail')) || !($sales_slip_detail_sequence = $this->input->post('sequence'))) {
//#8149:End
                    $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('common_could_not_edit')));
                }
            }
        }

        if (!($sales_slip = $this->sales_slip_model->get_item_by_id('sales_slip', $sales_slip_id))) {
            if ($action == 'edit') {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('common_could_not_edit')));
            } elseif ($action == 'delete') {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('common_could_not_delete')));
            } else {
                $this->assign('error_message', lang('common_could_not_edit'));
                $this->view('sales/_error.tpl');
                return;
            }
        };

        if (!($sales_slip_detail = $this->sales_slip_model->get_items_by_parameters('sales_slip_detail', null, array('sales_slip_id = ' . $sales_slip_id, 'disable = 0'), array('sequence ASC', 'j_code ASC', 'branch_cd ASC', 'create_datetime ASC')))) {
            if ($action == 'edit') {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('common_could_not_edit')));
            } elseif ($action == 'delete') {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('common_could_not_delete')));
            } else {
                $this->assign('error_message', lang('common_could_not_edit'));
                $this->view('sales/_error.tpl');
                return;
            }
        };

        $this->load->model('order_model');

        // list view make sure that ONLY the same "is_ad_receive_payment" can be created in ONE sales slip,
        // so no need to further check and only need property of one "is_ad_receive_payment" here
        if (!$order = $this->order_model->get_items_by_parameters('`order`', array('id', 'j_code', 'is_ad_receive_payment', 'j_account_id'), array('j_code = "'. $sales_slip_detail[0]['j_code'] . '"', 'disable = 0'))) {
            if ($action == 'edit') {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('common_could_not_edit')));
            } elseif ($action == 'delete') {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('common_could_not_delete')));
            } else {
                $this->assign('error_message', lang('common_could_not_edit'));
                $this->view('sales/_error.tpl');
                return;
            }
        };

        $jcode_order_id_map = map_element($order, 'j_code', 'id');

        $this->load->model('consumption_tax_model');
        foreach ($sales_slip_detail as $key => $detail) {
            $sales_slip_detail[$key]['order_id'] = isset($jcode_order_id_map[$detail['j_code']]) ? $jcode_order_id_map[$detail['j_code']] : 0;
            $sales_slip_detail[$key]['tax_rate'] = $this->consumption_tax_model->get_tax_rate_by_date($detail['delivery_date']);
        }

        if (!$this->auth->has_permission('all_invoices', 'column')
            && (!$this->auth->has_permission('self_invoices', 'column')
                || ($this->auth->get_account_id() != $order[0]['j_account_id'])
            )
        ) {
            $this->app_smarty->display('error/ajax_access_deny.tpl');
            exit;
        }

        if (!in_array($sales_slip['sales_slip_status'], array(SALES_STATUS_CREATED, SALES_STATUS_APPROVAL_PENDING))) {
            if ($action == 'edit') {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('common_could_not_edit')));
            } elseif ($action == 'delete') {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('common_could_not_delete')));
            } else {
                $this->assign('error_message', lang('common_could_not_edit'));
                $this->view('sales/_error.tpl');
                return;
            }
        }

        // process adding data here then return json result and do not load view
        if ($action == 'edit') {
            $sales_slip_param['id'] = $sales_slip_id;
            $sales_slip_param['is_ad_receive_payment'] = $order[0]['is_ad_receive_payment'];

//#7620:Start
//            if (!($order[0]['is_ad_receive_payment'] && $sales_slip_detail[0]['is_ad_receive_payment'])) {
            if (!($order[0]['is_ad_receive_payment'])) {
//#7620:End
                $sales_slip_param['billing_closing_date'] = $sales_slip['billing_date'];
                $sales_slip_param['payment_expire_date'] = $sales_slip['payment_date'];
            }

//#8149:Start
//          if (true === ($result = $this->sales_slip_model->update_sales_slip($sales_slip_param, $sales_slip_detail_param, $sales_slip_detail))) {
            if (true === ($result = $this->sales_slip_model->update_sales_slip($sales_slip_param, $sales_slip_detail_param, $sales_slip_detail, $sales_slip_detail_sequence))) {
//#8149:End
                $this->ajax_json(array('status' => AJAX_SUCCESS, 'message' => lang('msg_update_sales_report_successful')));
            } else {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => $result));
            }
        } elseif ($action == 'delete') {
            if (true === ($result = $this->sales_slip_model->delete_sales_slip($sales_slip_id))) {
                $this->ajax_json(array('status' => AJAX_SUCCESS, 'message' => lang('msg_delete_sales_report_successful')));
            } else {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => $result));
            }
        }

        if ($order[0]['is_ad_receive_payment']) {
            $sales_slip_detail_ids = get_element($sales_slip_detail, 'id');
            $ad_receive_sales_slip_detail_ids = get_element($sales_slip_detail, 'ad_receive_sales_slip_detail_id');

            $this->load->model('order_detail_model');
            if (!$extra_sales_slip_detail = $this->order_detail_model->get_items_by_parameters('sales_slip_detail', null, array('ad_receive_sales_slip_detail_id IN ("' . implode('", "', $ad_receive_sales_slip_detail_ids) . '")', 'id NOT IN("' . implode('", "', $sales_slip_detail_ids) . '")', 'is_ad_receive_payment = 1', 'disable = 0'))) {
                $extra_sales_slip_detail = array();
            }

            foreach ($extra_sales_slip_detail as $key => $extra_detail) {
                $extra_sales_slip_detail[$key]['tax_rate'] = $this->consumption_tax_model->get_tax_rate_by_date($extra_detail['delivery_date']);
            }
        }

//#7620:Start
//        $sales_slip['is_ad_receive_payment'] = $order[0]['is_ad_receive_payment'] && $sales_slip_detail[0]['is_ad_receive_payment'];
        $sales_slip['is_ad_receive_payment'] = $order[0]['is_ad_receive_payment'];
//#7620:End

        $this->load->model('client_model');
        if (!$client = $this->client_model->get_item_by_id('client', $sales_slip['client_id'])) {
            if (!$action) {
                $this->assign('error_message', lang('msg_same_s_code'));
                $this->view('sales/_error.tpl');
                return;
            } else {
                $this->ajax_json(array('status' => AJAX_FAIL, 'message' => lang('msg_same_s_code')));
            }
        };

        $this->assign('action', $this->method);

        $this->assign('client', $client);
        $this->assign('sales_slip_detail', $sales_slip_detail);
        $this->assign('old_sales_slip_detail', isset($extra_sales_slip_detail) ? $extra_sales_slip_detail : array());

        $this->assign('sales_slip', $sales_slip);

        $this->view('sales/_form.tpl');
    }

//#7769:Start
//    public function update_payment_billing_date() {
//        $this->load->model('sales_slip_model');
//        if ($this->sales_slip_model->update_payment_billing_date()) {
//            echo 'Success';
//        } else {
//            echo 'Error. No changes saved.';
//        }
//    }
//#7769:End
}