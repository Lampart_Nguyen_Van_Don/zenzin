<?php
class Sales_target extends ZR_Controller
{
    private $_ci;
    public function __construct() {
        parent::__construct ();

        $this->load->model ( 'sales_target_model' );
        $this->_ci = & get_instance ();
        $this->_ci->load->library ( 'tcpdf/tcpdf' );
    }

    public function setting() {
        $this->load->models(array('account_model', 'business_unit_model'));

        $result = $this->auth->has_permission ( "sales_target", 'column' );
        if(!$result) {
            show_404();
            return;
        }
        $login_account_id = $this->auth->get_account_id();
        $this->assign('login_account_id', $login_account_id);

        $business_unit = $this->account_model->get_account_department_business_unit($login_account_id);
        $is_product_division = $this->business_unit_model->is_product_division($business_unit[0]['business_unit_id']);

        $this->assign('business_unit_id', $business_unit[0]['business_unit_id']);
        $this->assign('is_product_division', $is_product_division);

        $business_unit_options = map_element($this->business_unit_model->get_product_business_unit_id_name(), 'id', 'name');
        $this->assign('business_options', $business_unit_options);

        // Date from begins at the first day of current month
        $date_from = date("Y")."-".date("m")."-01";

        // Date to begins at the last day of current month
        $last_day_of_month = cal_days_in_month ( CAL_GREGORIAN, date("m"), date("Y") );
        $date_to = date("Y")."-".date("m")."-".$last_day_of_month;

        if($is_product_division) {
            $data = $this->sales_target_model->search_sales_target($date_from, $date_to, $business_unit[0]['business_unit_id'],  $is_product_division);
        }
        else {
            //if not belong to product divistion, then choose and search 1st option of options of business logic
            $data = $this->sales_target_model->search_sales_target($date_from, $date_to, array_shift(array_keys($business_unit_options)),  $is_product_division);
        }
        //var_dump($data);exit;
        $data_list = json_encode ( array (
                'sales_target' => $data,
                'has_authority' => $result
        ));

        $this->assign("data_list", $data_list);

        $this->view('sales_target/setting.tpl');
    }

    public function get_data()
    {
       if ($post = json_decode ( $this->input->raw_input_stream )) {

            $date_from = $post->datepicker_from;
            $date_result = $this->parse_date_input ( $date_from );
            // If input type: 13/5 => 2013/5
            if ($date_result == "plus2000") {
                $parts = $this->separate_string ( $date_from );
                $date_from = ($parts [0] + 2000) . "-" . $parts [1] . "-01";
            }           // If input type: 2013/05
            else if ($date_result == "2chars") {
                $parts = $this->separate_string ( $date_from );
                $date_from = $parts [0] . "-" . $parts [1] . "-01";
            }           // If input type: 2013/05/23
            else if ($date_result == "3chars") {
                $parts = $this->separate_string ( $date_from );
                $date_from = $parts [0] . "-" . $parts [1] . "-01";
            }

            $post->datepicker_from = $date_from;
            $error_messages = $this->sales_target_model->run_search_condition_validate($post);

            if(!empty($error_messages)) {
                $errors = json_encode ( array (
                    'has_error' => true,
                    'error_messages' => $error_messages
                ));
                echo $errors; exit();
            }
            $this->load->models(array('account_model', 'business_unit_model'));
            $login_account_id = $this->auth->get_account_id();
            $business_unit = $this->account_model->get_account_department_business_unit($login_account_id);
            $is_product_division = $this->business_unit_model->is_product_division($business_unit[0]['business_unit_id']);

            $date_from = $post->datepicker_from;
            $date_to = $post->datepicker_to;
            $business_unit_id = $post->business_unit_id;

            // Check search date_from
            if (parse_date ( $date_from )) {
                $date_from = parse_date ( $date_from );
                $date_from_split = explode ( "-", $date_from );
                $date_from = $date_from_split [0] . "-" . $date_from_split [1] . "-" . "01";
            } else {
                $date_from = "";
            }

            // Check search date_to
            if (parse_date ( $date_from )) {
                $date_to = parse_date ( $date_from );
                $date_to_split = explode ( "-", $date_to );
                $last_day_of_month = cal_days_in_month ( CAL_GREGORIAN, $date_to_split [1], $date_to_split [0] );
                $date_to = $date_to_split [0] . "-" . $date_to_split [1] . "-" . $last_day_of_month;
            } else {
                $date_to = "";
            }

            if($business_unit_id == '0') {
                $data = $this->sales_target_model->search_sales_target($date_from, $date_to, '', $is_product_division);
            }
            else {
                $data = $this->sales_target_model->search_sales_target($date_from, $date_to, $business_unit_id, $is_product_division);
            }

            $result = $this->auth->has_permission ( "sales_target", 'column' );

            $date_from_split = explode ( "-", $date_from );
            $general_format = $date_to_split [0] . "/" . $date_to_split [1];
            $data_list = json_encode ( array (
                    'sales_target' => $data,
                    'general_format' => $general_format,
                    'has_authority' => $result
            ));
            echo $data_list; exit();
        }
    }

    /**
     * 2015-06-01 or 2015/06/01 => return array(2015, 06, 01)
     * @param unknown $raw
     * @return unknown
     */
    public function separate_string($raw) {
        $delimiter = '-';
        if (strpos ( $raw, '/' ) !== false) {
            $delimiter = '/';
        }
        $parts = explode ( $delimiter, $raw );
        if($parts[1] > 0 && $parts[1] <10 && !(strpos ( $parts[1], '0' ) !== false)) {
            $parts[1] = '0'.$parts[1];
        }
        return $parts;
    }

    /**
     * Check numeric and format of date input
     * @param unknown $raw_date
     * @return string|boolean
     */
    public function parse_date_input($raw_date) {
        // $format = 'Y-m-d';
        $delimiter = '-';

        if (strpos ( $raw_date, '/' ) !== false) {
            $delimiter = '/';
        }
        $parts = explode ( $delimiter, $raw_date );
        // print_r($parts);
        if (count ( $parts ) == 2) {
            // Year >=1, month: 1 ->12
            if (is_numeric ( $parts [0] ) & is_numeric ( $parts [1] ) & ($parts[0] > 0) & $parts [1] > 0 & $parts [1] < 13) {
                // Year <100 => + 2000: 13=>2013, 15=>2015
                if ($parts [0] < 100) {
                    return "plus2000";
                } else {
                    return "2chars";
                }
            } else {
                return false;
            }
        } else if (count ( $parts ) == 3) {
            // Year >=1, month: 1 ->12
            if (is_numeric ( $parts [0] ) & is_numeric ( $parts [1] ) & ($parts[0] > 0) & $parts [1] > 0 & $parts [1] < 13) {
                return "3chars";
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**s
     * save_all_grids
     *
     * @author : Thanh
     *         @created date: 18/August/2015
     */
    public function save_all_grids() {
        $all_grids = json_decode ( $this->input->raw_input_stream, true );
        $searchDate = $all_grids ['searchDate'];
        $login_account_id = $this->auth->get_account_id();

        $all_grids = $all_grids ['allGrids'];
        $all_grids_shortage = array ();

        if (! empty ( $all_grids )) {
            foreach ( $all_grids as $key => $value ) {
                $value_sales_target = array ();
                if (count ( $value ['sales_target'] ) > 0) {
                    foreach ( $value ['sales_target'] as $row ) {
                        // if (isset ( $row ['can_change_or_not'] )) {
                            $value_sales_target [] = $row;
                        // }
                    }
                }
                if (! empty ( $value_sales_target )) {
                    $all_grids_shortage [$key] = $value_sales_target;
                }
            }
        } else {
            $errors ['error'] = 2;
            echo json_encode ( $errors );
            exit ();
        }

        if (! empty ( $all_grids_shortage )) {
            if ($this->sales_target_model->save_all_grids ( $all_grids_shortage, $searchDate, $login_account_id )) {
                $errors ['error'] = 0;
                echo json_encode ( $errors );
                exit ();
            } else {
                $errors ['error'] = 1;
                echo json_encode ( $errors );
                exit ();
            }
        } else {
            $errors ['error'] = 2;
            echo json_encode ( $errors );
            exit ();
        }
    }
    /**
     * sales report
     * @author Luan
     * @return
     * @created date: 30/07/2015
     */
     public function report()
    {
        if (!$this->input->is_ajax_request()) {
            //load model
            $this->load->model(array('department_model','business_unit_model','account_model'));

            //init variables
            $data = array();
            $business_unit_id_list = array();
            $this->assign('error', false);

            $business_unit = $this->department_model->get_business_unit_of_current_logged_user($this->auth->get_account_id ());
            $business_unit_options = $this->business_unit_model->get_product_business_unit_id_name();
                $this->assign('business_unit_options', $business_unit_options);
            if(intval($business_unit['is_product_division']) == 0){
                //is product division
                $this->assign('business_unit',$business_unit_options[0]['id']);
            }
            else{
                //is not product division
                $this->assign('business_unit',$business_unit['id']);
            }

            if($this->input->server('REQUEST_METHOD') == 'POST'){

                //post method
                $data = $this->input->post();
                $this->assign('datepicker_from', $this->input->post('datepicker_from'));
                $this->assign('business_unit_id', $this->input->post('business_unit_id'));
                $this->assign('time', $this->input->post('time'));
                $this->assign('business_unit', $this->input->post('business_unit'));
                if($data['business_unit'] == -1){
                    //select all
                    foreach ($business_unit_options as $business_unit_option) {
                        $business_unit_id_list[] = $business_unit_option['id'];
                    }
                }
                else{
                    //select 1 division
                    $business_unit_id_list[] = $data['business_unit'];
                }

            }else{

                //get method
                $business_unit_id_list[] = $business_unit['id'];
                $data = array('time' => date("Y/m"), 'business_unit' => $business_unit['id']);
                $this->assign('time', date("Y/m"));
            }

            if(count($data)) {
                //data is ok
                $list_acounts = array();
                $list_departments = array();
                $list_business_units = array();

                $delimiter = strpos($data['time'], '-') ? '-' : '/';
                if(trim($data['time'])==''){
                    $this->assign('empty_date', true);
                }
                else{
                    $date = explode($delimiter, $data['time']);
                    $isdate = false;
                    if(count($date) == 2){
                        $isdate = $this->check_date_format($date);
                    }
                    if($isdate){
                        //date format is correct
                        if(intval($date[0])<1000){
                            $date[0] = intval($date[0])+2000;
                        }
                        $date[1] = intval($date[1]);
                        $this->assign('time', $date[0].$delimiter.sprintf("%02d", $date[1]));
                        foreach ($business_unit_id_list as $business_unit_id) {
//                             $list_departments = array_merge($list_departments,$this->get_list_departments($business_unit_id,$date));
//                             if($this->get_list_business_units($business_unit_id,$date)) $list_business_units[] = $this->get_list_business_units($business_unit_id,$date);
                            $list_acounts = array_merge($list_acounts,$this->get_list_all_report($business_unit_id,$date));
                        }

                        list($list_departments, $list_business_units) = $this->list_department_business($list_acounts);

                        usort($list_acounts, function($a, $b) {
                            if(($b['target_achieve_percentage'] - $a['target_achieve_percentage']) == 0) {
                                return true;
                            }
                            return $b['target_achieve_percentage'] - $a['target_achieve_percentage'];
                        });
                        usort($list_departments, function($a, $b) {
                             if(($b['target_achieve_percentage'] - $a['target_achieve_percentage']) == 0) {
                                return true;
                            }
                            return $b['target_achieve_percentage'] - $a['target_achieve_percentage'];
                        });
                        usort($list_business_units, function($a, $b) {
                            if(($b['target_achieve_percentage'] - $a['target_achieve_percentage']) == 0) {
                                return true;
                            }
                            return $b['target_achieve_percentage'] - $a['target_achieve_percentage'];
                        });
                        $this->assign('list_departments', array_map("unserialize", array_unique(array_map("serialize", $list_departments))));
                        $this->assign('list_business_units', $list_business_units);
                        $this->assign('list_acounts', array_map("unserialize", array_unique(array_map("serialize", $list_acounts))));
                    }
                    else{
                        $this->assign('error_date_format', true);
                    }
                }

            }

            return $this->view('sales_target/show_report.tpl');
        }
        //load model
        $this->load->model(array('department_model','business_unit_model','account_model'));

        //init variables
        $data = array();
        $business_unit_id_list = array();
        $this->assign('error', false);

        $business_unit = $this->department_model->get_business_unit_of_current_logged_user($this->auth->get_account_id ());
        $business_unit_options = $this->business_unit_model->get_product_business_unit_id_name();
        $this->assign('business_unit_options', $business_unit_options);
        if(intval($business_unit['is_product_division']) == 0){

            //is product division
            $this->assign('is_product_division', 0);
        }
        else{

            //is not product division
            $this->assign('is_product_division', 1);

        }

        if($this->input->server('REQUEST_METHOD') == 'POST'){

            //post method
            $data = $this->input->post();
            $this->assign('time', $this->input->post('time'));
            $this->assign('business_unit', $this->input->post('business_unit'));
            if($data['business_unit'] == -1){
                //select all
                foreach ($business_unit_options as $business_unit_option) {
                    $business_unit_id_list[] = $business_unit_option['id'];
                }
            }
            else{
                //select 1 division
                $business_unit_id_list[] = $data['business_unit'];
            }

        }else{

            //get method
            $this->assign('business_unit',$business_unit['id']);
            $business_unit_id_list[] = $business_unit['id'];
            $data = array('time' => date("Y/m"), 'business_unit' => $business_unit['id']);
            $this->assign('time', date("Y/m"));
        }

        if(count($data)) {
            //data is ok
            $list_acounts = array();
            $list_departments = array();
            $list_business_units = array();

            $delimiter = strpos($data['time'], '-') ? '-' : '/';
            if(trim($data['time'])==''){
                echo 'error_empty_date';
                return false;
            }
            else{
                $date = explode($delimiter, $data['time']);
                $isdate = false;
                if(count($date) == 2){
                    $isdate = $this->check_date_format($date);
                }
                if($isdate){
                    //date format is correct
                    if(intval($date[0])<1000){
                        $date[0] = intval($date[0])+2000;
                    }
                    $date[1] = intval($date[1]);
                    $this->assign('time', $date[0].$delimiter.sprintf("%02d", $date[1]));
                    foreach ($business_unit_id_list as $business_unit_id) {
//                         $list_departments = array_merge($list_departments,$this->get_list_departments($business_unit_id,$date));
//                         if($this->get_list_business_units($business_unit_id,$date)) $list_business_units[] = $this->get_list_business_units($business_unit_id,$date);
                        $list_acounts = array_merge($list_acounts,$this->get_list_all_report($business_unit_id,$date));
                    }

                    list($list_departments, $list_business_units) = $this->list_department_business($list_acounts);

                    usort($list_acounts, function($a, $b) {
                        if(($b['target_achieve_percentage'] - $a['target_achieve_percentage']) == 0) {
                            return true;
                        }
                        return $b['target_achieve_percentage'] - $a['target_achieve_percentage'];
                    });
                    usort($list_departments, function($a, $b) {
                         if(($b['target_achieve_percentage'] - $a['target_achieve_percentage']) == 0) {
                            return true;
                        }
                        return $b['target_achieve_percentage'] - $a['target_achieve_percentage'];
                    });
                    usort($list_business_units, function($a, $b) {
                        if(($b['target_achieve_percentage'] - $a['target_achieve_percentage']) == 0) {
                            return true;
                        }
                        return $b['target_achieve_percentage'] - $a['target_achieve_percentage'];
                    });
                    $this->assign('list_departments', array_map("unserialize", array_unique(array_map("serialize", $list_departments))));
                    $this->assign('list_business_units', $list_business_units);
                    $this->assign('list_acounts', array_map("unserialize", array_unique(array_map("serialize", $list_acounts))));
                }
                else{
                    echo 'error_date_format';
                    return false;
                }
            }
        }


        $this->view('sales_target/report.tpl');
    }

    /**
     *
     * Get Report List
     */
    public function get_list_all_report($business_unit_id,$current_month){
        if(($business_unit_id === null)|| empty($current_month)){
            return false;
        }

        $result = array();
        $this->load->model(array('account_model','order_model','business_unit_model'));
        $business_unit = $this->business_unit_model->get_business_unit_by_id($business_unit_id);
        if(!$business_unit) return $result;
        $accounts = array();
        foreach ($this->account_model->get_sale_target_account_by_business_unit($business_unit_id, $current_month) as $key => $value) {
            $accounts[] = $value;
        }
        $account_reports = $this->get_report_by_account($accounts,$current_month,intval($business_unit['totalization_target_months']));
        foreach($account_reports as $tmp){
            $tmp['business_unit_id']  = $business_unit_id;
            $tmp['business_unit_name']  = $business_unit['name'];
            if(intval($tmp['target_amount'])>0){
//#7617:Start
//                 $tmp['target_achieve_percentage'] = f_floor(($tmp['monthly_report']*100/$tmp['target_amount'])*10)/10;
//                $tmp['target_achieve_percentage'] = f_floor(($tmp['monthly_report']*100/$tmp['target_amount'])*100)/100;
                $tmp['target_achieve_percentage'] = bcdiv(bcmul($tmp['monthly_report'], 100), $tmp['target_amount'], 2);
//#7617:End
            }
            else{
                $tmp['target_achieve_percentage'] = 0;
            }

            $result[] = $tmp;
        }
        return $result;
    }
    /**
     * get list department and business from report list
     * @param array $list_acounts
     * @return array($list_departments,$list_business_units)
     */
    public function list_department_business($list_acounts){
        $list_departments = array();
        $list_business_units = array();
        foreach($list_acounts as $value){
            // department report
            if(!isset($list_departments[$value['department_id']])){
                $list_departments[$value['department_id']] = array(
                        'department_name'=>$value['department_name'],
                        'target_amount'=>'',
                        'target_amount'=>'',
                        'monthly_report'=>'',
                        'last_month_prophit'=>'',
                        'last_month_summary_month'=>'',
                        'cancel'=>'',
                        'summary_month'=>'',
                        'last_month_report'=>'',
                        'sales'=>'',
                        'target_achieve_percentage'=>''
                );
            }
            $list_departments[$value['department_id']]['target_amount'] += isset($value['target_amount'])?$value['target_amount']:'';
            $list_departments[$value['department_id']]['monthly_report'] += isset($value['monthly_report'])?$value['monthly_report']:'';
            $list_departments[$value['department_id']]['last_month_prophit'] += isset($value['last_month_prophit'])?$value['last_month_prophit']:'';
            $list_departments[$value['department_id']]['last_month_summary_month'] += isset($value['last_month_summary_month'])?$value['last_month_summary_month']:'';
            $list_departments[$value['department_id']]['cancel'] += isset($value['cancel'])?$value['cancel']:'';
            $list_departments[$value['department_id']]['summary_month'] += isset($value['summary_month'])?$value['summary_month']:'';
            $list_departments[$value['department_id']]['last_month_report'] += isset($value['last_month_report'])?$value['last_month_report']:'';
            $list_departments[$value['department_id']]['sales'] += isset($value['sales'])?$value['sales']:'';

            // business report
            if(!isset($list_business_units[$value['business_unit_id']])){
                $list_business_units[$value['business_unit_id']] = array('department_name'=>$value['business_unit_name'],
                            'target_amount'=>'',
                            'target_amount'=>'',
                            'monthly_report'=>'',
                            'last_month_prophit'=>'',
                            'last_month_summary_month'=>'',
                            'cancel'=>'',
                            'summary_month'=>'',
                            'last_month_report'=>'',
                            'sales'=>'',
                            'target_achieve_percentage'=>''
                );
            }
            $list_business_units[$value['business_unit_id']]['target_amount'] += isset($value['target_amount'])?$value['target_amount']:'';
            $list_business_units[$value['business_unit_id']]['monthly_report'] += isset($value['monthly_report'])?$value['monthly_report']:'';
            $list_business_units[$value['business_unit_id']]['last_month_prophit'] += isset($value['last_month_prophit'])?$value['last_month_prophit']:'';
            $list_business_units[$value['business_unit_id']]['last_month_summary_month'] += isset($value['last_month_summary_month'])?$value['last_month_summary_month']:'';
            $list_business_units[$value['business_unit_id']]['cancel'] += isset($value['cancel'])?$value['cancel']:'';
            $list_business_units[$value['business_unit_id']]['summary_month'] += isset($value['summary_month'])?$value['summary_month']:'';
            $list_business_units[$value['business_unit_id']]['last_month_report'] += isset($value['last_month_report'])?$value['last_month_report']:'';
            $list_business_units[$value['business_unit_id']]['sales'] += isset($value['sales'])?$value['sales']:'';
        }

        foreach($list_departments as $key => $department_report){
            if(intval($department_report['target_amount'])>0){
//              $list_departments[$key]['target_achieve_percentage'] = f_floor(($department_report['monthly_report']*100/$department_report['target_amount'])*10)/10;
                $list_departments[$key]['target_achieve_percentage'] = bcdiv(bcmul($department_report['monthly_report'], 100), $department_report['target_amount'], 1);
            }
            else{
                $list_departments[$key]['target_achieve_percentage'] = 0;
            }
        }
        foreach($list_business_units as $key => $business_report){
            if(intval($business_report['target_amount'])>0){
                $list_business_units[$key]['target_achieve_percentage'] = f_floor(($business_report['monthly_report']*100/$business_report['target_amount'])*10)/10;
            }
            else{
                $list_business_units[$key]['target_achieve_percentage'] = 0;
            }
        }
        return array($list_departments,$list_business_units);
    }

    /**
     * Get list accounts
     * @author Luan
     * @return
     * @created date: 30/07/2015
     */
//     public function get_list_acounts($business_unit_id,$current_month)
//     {
//         if(($business_unit_id === null)|| empty($current_month)){
//             return false;
//         }

//         $result = array();
//         $this->load->model(array('account_model','order_model','business_unit_model'));
//         $business_unit = $this->business_unit_model->get_business_unit_by_id($business_unit_id);
//         $accounts = array();
//         foreach ($this->account_model->get_sale_target_account_by_business_unit($business_unit_id, $current_month) as $key => $value) {
//             $accounts[] = $value;
//         }
//         $account_reports = $this->get_report_by_account($accounts,$current_month,intval($business_unit['totalization_target_months']));
//         foreach($account_reports as $tmp){
//             if(intval($tmp['target_amount'])>0){
// //#7617:Start
// //                 $tmp['target_achieve_percentage'] = f_floor(($tmp['monthly_report']*100/$tmp['target_amount'])*10)/10;
//                 $tmp['target_achieve_percentage'] = f_floor(($tmp['monthly_report']*100/$tmp['target_amount'])*100)/100;
// //#7617:End
//             }
//             else{
//                 $tmp['target_achieve_percentage'] = 0;
//             }

//             $result[] = $tmp;
//         }
//         return $result;
//     }

     /**
     * Get list departments
     * @author Luan
     * @return
     * @created date: 30/07/2015
     */
//     public function get_list_departments($business_unit_id,$current_month)
//     {
//         if(($business_unit_id === null)|| empty($current_month)){
//             return null;
//         }
//         $result = array();
//         $this->load->model(array('account_model','order_model','department_model','business_unit_model'));
//         $business_unit = $this->business_unit_model->get_business_unit_by_id($business_unit_id);
//         $department_ids = array();
//         foreach ($this->department_model->get_departments_by_business_unit_id($business_unit_id) as $department_key => $department) {
//             $department_ids[] = $department['id'];
//         }

//         $accounts = array();
//         foreach ($this->account_model->get_sale_target_account_by_department($department_ids,$current_month) as $account_key => $account) {
//             $accounts[$account['department_name']][] =  $account;
//         }

//         foreach($accounts as $department_name => $account_ids){
//             $account_reports = $this->get_report_by_account($account_ids,$current_month,intval($business_unit['totalization_target_months']));
//             if(empty($account_reports)){
//                 //move to next department
//                 continue;
//             }
//             $department_report = array('department_name'=>$department_name,
//                     'target_amount'=>'',
//                     'target_amount'=>'',
//                     'monthly_report'=>'',
//                     'last_month_prophit'=>'',
//                     'last_month_summary_month'=>'',
//                     'cancel'=>'',
//                     'summary_month'=>'',
//                     'last_month_report'=>'',
//                     'sales'=>'',
//                     'target_achieve_percentage'=>''
//             );
//             foreach ($account_reports as $key => $value) {
//                 $department_report['target_amount'] += isset($value['target_amount'])?$value['target_amount']:'';
//                 $department_report['monthly_report'] += isset($value['monthly_report'])?$value['monthly_report']:'';
//                 $department_report['last_month_prophit'] += isset($value['last_month_prophit'])?$value['last_month_prophit']:'';
//                 $department_report['last_month_summary_month'] += isset($value['last_month_summary_month'])?$value['last_month_summary_month']:'';
//                 $department_report['cancel'] += isset($value['cancel'])?$value['cancel']:'';
//                 $department_report['summary_month'] += isset($value['summary_month'])?$value['summary_month']:'';
//                 $department_report['last_month_report'] += isset($value['last_month_report'])?$value['last_month_report']:'';
//                 $department_report['sales'] += isset($value['sales'])?$value['sales']:'';
//             }
//             if(intval($department_report['target_amount'])>0){
//                 $department_report['target_achieve_percentage'] = f_floor(($department_report['monthly_report']*100/$department_report['target_amount'])*10)/10;
//             }
//             else{
//                 $department_report['target_achieve_percentage'] = 0;
//             }
//             $result[] = $department_report;
//         }
//         return $result;
//     }

     /**
     * Get list business unit
     * @author Luan
     * @return
     * @created date: 30/07/2015
     */
//     public function get_list_business_units($business_unit_id,$current_month)
//     {
//         if(($business_unit_id === null)|| empty($current_month)){
//             return null;
//         }

//         $result = array();
//         $this->load->model(array('account_model','order_model','business_unit_model'));
//         $business_unit = $this->business_unit_model->get_business_unit_by_id($business_unit_id);
//         $business_name='';
//         $accounts = array();
//         foreach ($this->account_model->get_sale_target_account_by_business_unit($business_unit_id,$current_month) as $key => $value) {
//             $accounts[] = $value;
//         }
//         $account_reports = $this->get_report_by_account($accounts,$current_month,intval($business_unit['totalization_target_months']));
//         if(empty($account_reports)){
//             return null;
//         }
//             $business_report = array('department_name'=>$business_unit['name'],
//                                         'target_amount'=>'',
//                                         'target_amount'=>'',
//                                         'monthly_report'=>'',
//                                         'last_month_prophit'=>'',
//                                         'last_month_summary_month'=>'',
//                                         'cancel'=>'',
//                                         'summary_month'=>'',
//                                         'last_month_report'=>'',
//                                         'sales'=>'',
//                                         'target_achieve_percentage'=>''
//                 );
//             foreach ($account_reports as $key => $value) {
//                 $business_report['target_amount'] += isset($value['target_amount'])?$value['target_amount']:'';
//                 $business_report['monthly_report'] += isset($value['monthly_report'])?$value['monthly_report']:'';
//                 $business_report['last_month_prophit'] += isset($value['last_month_prophit'])?$value['last_month_prophit']:'';
//                 $business_report['last_month_summary_month'] += isset($value['last_month_summary_month'])?$value['last_month_summary_month']:'';
//                 $business_report['cancel'] += isset($value['cancel'])?$value['cancel']:'';
//                 $business_report['summary_month'] += isset($value['summary_month'])?$value['summary_month']:'';
//                 $business_report['last_month_report'] += isset($value['last_month_report'])?$value['last_month_report']:'';
//                 $business_report['sales'] += isset($value['sales'])?$value['sales']:'';
//             }
//             if(intval($business_report['target_amount'])>0){
//                 $business_report['target_achieve_percentage'] = f_floor(($business_report['monthly_report']*100/$business_report['target_amount'])*10)/10;
//             }
//             else{
//                 $business_report['target_achieve_percentage'] = 0;
//             }
//             $result[] = $business_report;
//         return $business_report;
//     }

      /**
     * Get report by account
     * @author Luan
     * @return
     * @created date: 30/07/2015
     */

    public function get_report_by_account($accounts,$current_month,$totalization_target_months)
    {
        //get current month, last month and target month
        if(empty($accounts)) return array();
        $is_single = false;
        if($is_single = !isset($accounts[0]['account_id'])){
            $accounts = array($accounts);
        }
        $account_ids = array();
        foreach($accounts as $account){
            $account_ids[] = $account['account_id'];
        }
        $result = array();
        $datestring= $current_month[0].'-'.$current_month[1].' first day of this month';
        $dt=date_create($datestring);
        $current_month  = explode('-', $dt->format('Y-m-d'));

        $datestring= $current_month[0].'-'.$current_month[1].' last day of last month';
        $dt=date_create($datestring);
        $last_month  = explode('-', $dt->format('Y-m-d'));

        $datestring= ($current_month[0]+intval(($current_month[1]+$totalization_target_months)/12)).'-'.(($current_month[1]+$totalization_target_months)%12).' last day of last month';
        $dt=date_create($datestring);
        $target_month  = explode('-', $dt->format('Y-m-d'));

        //calculate fields
        $last_month_prophit = $this->order_model->get_sale_report_last_month_prophit($account_ids,$last_month);
        $last_month_summary_month = $this->order_model->get_sale_report_last_month_summary_month($account_ids,$last_month,$current_month);
        $cancel = $this->order_model->get_sale_report_cancel($account_ids,$last_month,$current_month,$target_month);
        $ahead_sche = $this->order_model->get_sale_change_delivery_date_ahead($account_ids,$last_month,$current_month);
        $summary_month = $this->order_model->get_sale_report_summary_month($account_ids,$current_month, $target_month);
        $last_month_report = $this->order_model->get_sale_report_last_month_report($account_ids,$last_month, $target_month);
//         $monthly_report  = $last_month_prophit + $last_month_summary_month + $cancel + $summary_month + $last_month_report;
        $sales = $this->order_model->get_sale_report_sales($account_ids,$last_month, $current_month, $target_month);

        foreach ($accounts as $key => $account){
            $cancel_val = 0 - $cancel[$account['account_id']];
//          $monthly_report  = $last_month_prophit[$account['account_id']] + $last_month_summary_month[$account['account_id']] + $cancel_val + $ahead_sche[$account['account_id']] + $summary_month[$account['account_id']] + $last_month_report[$account['account_id']];
//#20151215 Request edit from Mr. Kobayashi
            $monthly_report  = $last_month_prophit[$account['account_id']] + $last_month_summary_month[$account['account_id']] + $cancel_val + $ahead_sche[$account['account_id']] + $summary_month[$account['account_id']] + $last_month_report[$account['account_id']];
//#20151215 End
            //部署名
            $result[$key]['department_id'] = $account['department_id'];
            $result[$key]['department_name'] = $account['department_name'];
            //アカウント名
            $result[$key]['account_name'] = $account['account_name'];
            //目標
            $result[$key]['target_amount'] = $account['target_amount'];
            //月報
            $result[$key]['monthly_report'] = $monthly_report;
            //前月清算アップ
            $result[$key]['last_month_prophit'] = $last_month_prophit[$account['account_id']];
            //前月納品計上分
            $result[$key]['last_month_summary_month'] = $last_month_summary_month[$account['account_id']];
            //キャン/前倒し
            $result[$key]['cancel'] = $cancel_val + $ahead_sche[$account['account_id']];
            //今月計上分
            $result[$key]['summary_month'] = $summary_month[$account['account_id']];
            //前月末時点月報
            $result[$key]['last_month_report'] = $last_month_report[$account['account_id']];
            //売上
            $result[$key]['sales'] = $sales[$account['account_id']];
        }

        return (count($result)>0 && $is_single) ? $result[0]:$result;
    }

      /**
     * Check date format
     * @author Luan
     * @return
     * @created date: 07/08/2015
     */
    function check_date_format($date){
        if(!ctype_digit($date[0])||!ctype_digit($date[1]) || (intval($date[0]) < 1) || (intval($date[0])>2999) || (intval($date[1]) < 1) ||(intval($date[1]) > 12)) return false;
        return true;
    }

    // function sales_target(){
    //    $this->load->models(array('account_model', 'business_unit_model'));

    //     $result = $this->auth->has_permission ( "sales_target", 'column' );

    //     $login_account_id = $this->auth->get_account_id();
    //     $this->assign('login_account_id', $login_account_id);

    //     $business_unit = $this->account_model->get_account_department_business_unit($login_account_id);
    //     $is_product_division = $this->business_unit_model->is_product_division($business_unit[0]['business_unit_id']);

    //     $this->assign('business_unit_id', $business_unit[0]['business_unit_id']);
    //     $this->assign('is_product_division', $is_product_division);

    //     $business_unit_options = map_element($this->business_unit_model->get_product_business_unit_id_name(), 'id', 'name');
    //     $this->assign('business_options', $business_unit_options);

    //     // Date from begins at the first day of current month
    //     $date_from = date("Y")."-".date("m")."-01";

    //     // Date to begins at the last day of current month
    //     $last_day_of_month = cal_days_in_month ( CAL_GREGORIAN, date("m"), date("Y") );
    //     $date_to = date("Y")."-".date("m")."-".$last_day_of_month;

    //     if($is_product_division) {
    //         $data = $this->sales_target_model->search_sales_target($date_from, $date_to, $business_unit[0]['business_unit_id'],  $is_product_division);
    //     }
    //     else {
    //         //if not belong to product divistion, then choose and search 1st option of options of business logic
    //         $data = $this->sales_target_model->search_sales_target($date_from, $date_to, array_shift(array_keys($business_unit_options)),  $is_product_division);
    //     }
    //     //var_dump($data);exit;
    //     $data_list = json_encode ( array (
    //             'sales_target' => $data,
    //             'has_authority' => $result
    //     ));

    //     $this->assign("data_list", $data_list);


    //      //load model
    //     $this->load->model(array('department_model','business_unit_model','account_model'));

    //     //init variables
    //     $data = array();
    //     $business_unit_id_list = array();
    //     $this->assign('error', false);

    //     $business_unit = $this->department_model->get_business_unit_of_current_logged_user($this->auth->get_account_id ());
    //     $business_unit_options = $this->business_unit_model->get_product_business_unit_id_name();
    //         $this->assign('business_unit_options', $business_unit_options);
    //     if(intval($business_unit['is_product_division']) == 0){
    //         //is product division
    //         $this->assign('business_unit',$business_unit_options[0]['id']);
    //     }
    //     else{
    //         //is not product division
    //         $this->assign('business_unit',$business_unit['id']);
    //     }

    //     if($this->input->server('REQUEST_METHOD') == 'POST'){

    //         //post method
    //         $data = $this->input->post();
    //         $this->assign('datepicker_from', $this->input->post('datepicker_from'));
    //         $this->assign('business_unit_id', $this->input->post('business_unit_id'));
    //         $this->assign('time', $this->input->post('time'));
    //         $this->assign('business_unit', $this->input->post('business_unit'));
    //         if($data['business_unit'] == -1){
    //             //select all
    //             foreach ($business_unit_options as $business_unit_option) {
    //                 $business_unit_id_list[] = $business_unit_option['id'];
    //             }
    //         }
    //         else{
    //             //select 1 division
    //             $business_unit_id_list[] = $data['business_unit'];
    //         }

    //     }else{

    //         //get method
    //         $business_unit_id_list[] = $business_unit['id'];
    //         $data = array('time' => date("Y/m"), 'business_unit' => $business_unit['id']);
    //         $this->assign('time', date("Y/m"));
    //     }

    //     if(count($data)) {
    //         //data is ok
    //         $list_acounts = array();
    //         $list_departments = array();
    //         $list_business_units = array();

    //         $delimiter = strpos($data['time'], '-') ? '-' : '/';
    //         if(trim($data['time'])==''){
    //             $this->assign('empty_date', true);
    //         }
    //         else{
    //             $date = explode($delimiter, $data['time']);
    //             $isdate = false;
    //             if(count($date) == 2){
    //                 $isdate = $this->check_date_format($date);
    //             }
    //             if($isdate){
    //                 //date format is correct
    //                 if(intval($date[0])<1000){
    //                     $date[0] = intval($date[0])+2000;
    //                 }
    //                 $date[1] = intval($date[1]);
    //                 $this->assign('time', $date[0].$delimiter.sprintf("%02d", $date[1]));
    //                 foreach ($business_unit_id_list as $business_unit_id) {
    //                     $list_departments = array_merge($list_departments,$this->get_list_departments($business_unit_id,$date));
    //                     if($this->get_list_business_units($business_unit_id,$date)) $list_business_units[] = $this->get_list_business_units($business_unit_id,$date);
    //                     $list_acounts = array_merge($list_acounts,$this->get_list_acounts($business_unit_id,$date));
    //                 }
    //                 usort($list_acounts, function($a, $b) {
    //                     return $b['target_achieve_percentage'] - $a['target_achieve_percentage'];
    //                 });
    //                 usort($list_departments, function($a, $b) {
    //                     return $b['target_achieve_percentage'] - $a['target_achieve_percentage'];
    //                 });
    //                 usort($list_business_units, function($a, $b) {
    //                     return $b['target_achieve_percentage'] - $a['target_achieve_percentage'];
    //                 });
    //                 $this->assign('list_departments', array_map("unserialize", array_unique(array_map("serialize", $list_departments))));
    //                 $this->assign('list_business_units', $list_business_units);
    //                 $this->assign('list_acounts', array_map("unserialize", array_unique(array_map("serialize", $list_acounts))));
    //             }
    //             else{
    //                 $this->assign('error_date_format', true);
    //             }
    //         }

    //     }


    //     $this->view('sales_target/sales_target.tpl');
    // }


}
