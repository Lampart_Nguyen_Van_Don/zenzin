<?php
class Sample extends ZR_Controller {

    // Product
    /**
     *
     * @author Cong_Minh
     */
    public function show_product() {
        $this->view('sample/show_product.tpl');
    }

    /**
     *
     * @author Cong_Minh
     */
    public function get_products_by_conditions() {
        $this->load->model('sample_model');
        $products = $this->sample_model->get_products_by_conditions(file_get_contents("php://input"));
        die(json_encode($products));
    }

    /**
     *
     * @author Cong_Minh
     */
    public function update_list_product() {
        $this->load->model('sample_model');
        $products = $this->sample_model->update_list_product(file_get_contents("php://input"));
    }

    /**
     *
     * @author Cong_Minh
     */
    public function remove_list_product() {
        $this->load->model('sample_model');
        $products = $this->sample_model->remove_list_product(file_get_contents("php://input"));
    }



    // Account
    /**
     *
     * @author Cong_Minh
     */
    public function show_account() {
        $this->view('sample/show_account.tpl');
    }

    /**
     *
     * @author Cong_Minh
     */
    public function get_accounts_by_conditions() {
        $this->load->model('sample_model');
        $accounts = $this->sample_model->get_accounts_by_conditions(file_get_contents("php://input"));
        die(json_encode($accounts));
    }

    /**
     *
     * @author Cong_Minh
     */
    public function get_all_department() {
        $this->load->model('sample_model');
        $departments = $this->sample_model->get_all_department();
        die(json_encode($departments));
    }

    /**
     *
     * @author Cong_Minh
     */
    public function get_all_authority() {
        $this->load->model('sample_model');
        $authorities = $this->sample_model->get_all_authority();
        die(json_encode($authorities));
    }

    /**
     *
     * @author Cong_Minh
     */
    public function save_account() {
        $this->load->model('sample_model');
        $authorities = $this->sample_model->save_account(file_get_contents("php://input"));
    }


    // TMP
    /**
     *
     * @author Cong_Minh
     */
    public function show_tmp() {
        $this->view('sample/show_tmp.tpl');
    }

    /**
     *
     * @author Cong_Minh
     */
    public function get_all_tmp() {
        $this->load->model('sample_model');
        $tmp_records = $this->sample_model->get_all_tmp();
        die(json_encode($tmp_records));
    }


    /*Department*/
    public $search_conditions;
    // Constructor
    public function __construct() {
        parent::__construct ();
        // Load model for department management
        $this->load->model('department_model');
    }

    /**
     * Show all departments
     * @return
     * @author
     */
    public function show_department() {
        // Load business unit model
        $this->load->model('business_unit_model');
        // Get all business unit
        $list_business_unit = $this->business_unit_model->get_business_unit_id_name();
        // Assign all business_units to view
        $this->assign ('bu', $this->_make_option_data($list_business_unit, 'id,name'));
        // List of department to display from searching
        $departments = array();
        // User clicks search
        if($this->input->post()) {
            // print_r($this->input->post());
            $conditions = array('search_business_unit'=>trim($this->input->post('search_business_unit')), 'search_name'=>trim($this->input->post('search_name')));
            $departments = $this->department_model->set_pager ()->get_all_departments ($conditions);
        } else {
            $departments = $this->department_model->set_pager ()->get_all_departments ();
        }
        $this->assign('departments', $departments);
        $this->view('sample/show_department.tpl');
    }
    /**
     * Edit and delete department.
     * From detail screen, user clicks "Edit" to open
     * "Edit" screen or "Delete" to delete current department
     * @author
     * @return
     */
    public function edit_department() {
        // From list department screen, user click "EDIT"
        // print_r($this->input->post());
        // return;


        if ($this->input->post('edit')|| $this->input->post('edit_b')) {
            // echo "<br/>EDIT";
            // Get id of department which to save
            $this->edit_navigation($this->input->post('d_id'));
            // $_POST = array();
        } else if ($this->input->post('delete')) {
            // Check if this exists
            $list = array_filter($this->department_model->check_department_exist($this->input->post('d_id')));
            if (!empty($list)) {
                $this->department_model->delete('department',array('id' => $this->input->post('d_id')));
            }
            // Return to view screen
            redirect_post('/_admin/sample/show_department');
        } else if ($this->input->post('save_edit')) {
            // echo "HERE";
            // return;
            $this->edit_save_progress($this->input->post('d_id'));
        } else if ($this->input->post('back')) {
            // Return to view screen
            // If has search condition => Send the conditions to /show
            if (isset($this->search_conditions)) {
                redirect_data('/_admin/sample/show_department', $this->search_conditions);
            } else {
                redirect('/_admin/sample/show_department');
            }
        }
    }

    /**
     * From list of department screen, user clicks "Edit" to view detail department
     * @author
     * @return
     */
    public function detail_department() {
        // Click edit from list department
        if ($id = $this->input->post('d_id')) {
            // Get item to show in view
            $item = $this->department_model->get_department_by_id($id);
            $this->assign ('d', $item);
            $this->view ('sample/detail_department.tpl');
        }

    }

    /**
     * From list of department screen, user clicks "Copy" to copy department
     * @author
     * @return
     */
    public function copy_department() {
        // When click Copy to show copy screen
        if ($this->input->post('copy')|| $this->input->post('copy_b')) {
            $this->copy_navigation($this->input->post('d_id'));
        } else if ($this->input->post('save_copy')) {
            $this->copy_save_progress($this->input->post('d_id'));
        } else if ($this->input->post('back')) {
            // If has search condition => Send the conditions to /show
            if (isset($this->search_conditions)) {
                redirect_post('/_admin/sample/show_department');
            } else {
                redirect('/_admin/sample/show_department');
            }
        }
    }

    /**
     * Copy navigation => User click copy => Change to page copy
     * @author
     * @return
     */
    public function copy_navigation($id) {
        // Load copy screen with filled data
        // Load business unit model
        $this->load->model('business_unit_model');
        // Get all business unit
        $list_business_unit = $this->business_unit_model->get_business_unit_id_name ();
        $this->assign('bu', $list_business_unit);
        // Get item to show in view
        $item = $this->department_model->get_department_by_id($id);
        $this->assign('d', $item);
        $this->view('sample/copy_department.tpl');
    }

    /**
     * Execute when user click "Save" in copy screen
     * @author
     * @return
     */
    public function copy_save_progress($id) {
        if ($this->department_model->validate('edit_department')) {
            // Fields to update
            // List params
            // Insert department
            $data = array (
                    'business_unit_id' => trim($_POST ['business_unit_id']),
                    'name' => trim($_POST ['name']),
                    'zipcode' => trim($_POST ['zipcode']),
                    'address_1' => trim($_POST ['address_1']),
                    'address_2' => trim($_POST ['address_2']),
                    'phone_no' => trim($_POST ['phone_no']),
                    'fax_no' => trim($_POST ['fax_no']),
                    'lastup_account_id' => $this->auth->get_account_id ()
            );
            $this->department_model->insert('department', $data);
            // Return to view screen
            // If has search condition => Send the conditions to /show
            if (isset($this->search_conditions)) {
                redirect_post('/_admin/sample/show_department');
            } else {
                redirect('/_admin/sample/show_department');
            }
        } else {
            // Validate fail
            $this->load->model('business_unit_model');
            // Get all business unit
            $list_business_unit = $this->business_unit_model->get_business_unit_id_name ();
            $this->assign('bu', $list_business_unit);
            // Get item to show in view
            $item = $this->department_model->get_department_by_id($id);
            $this->assign('d', $item);
            $this->view('sample/copy_department.tpl');
        }
    }

    /**
     * User click "Edit" => Change page
     * @author
     * @return
     */
    public function edit_navigation($id) {
        $item = $this->department_model->get_department_by_id($id);
        // Load business unit model
        $this->load->model('business_unit_model');
        // Get all business unit
        $list_business_unit = $this->business_unit_model->get_business_unit_id_name ();
        $this->assign('bu', $list_business_unit);
        $this->assign('d', $item);
        $_POST = array ();
        // $this->input->post() = array();
        $this->view('sample/edit_department.tpl');
    }

    /**
     * User clicks "Save" => Save to database
     * @author
     * @return
     */
    public function edit_save_progress($id) {
        if ($this->department_model->validate('edit_department')) {
            // Fields to update
            $zipcode = trim($this->input->post('zipcode'));
            $data = array (
                    'business_unit_id' => $this->input->post('business_unit_id'),
                    'name' => $this->input->post('name'),
                    'zipcode' => $zipcode,
                    'address_1' => $this->input->post('address_1'),
                    'address_2' => $this->input->post('address_2'),
                    'phone_no' => $this->input->post('phone_no'),
                    'fax_no' => $this->input->post('fax_no')
            );
            $condition = array (
                    'id' => $id
            );
            // Update department
            $this->department_model->update_department('department', $data, $condition);
            // $_POST = array();
            // Return to view screen
            if (isset($this->search_conditions)) {
                redirect_post('/_admin/sample/show_department');
            } else {
                redirect('/_admin/sample/show_department');
            }
        } else {
            // Validation fail
            $item = $this->department_model->get_department_by_id($id);
            // Load business unit model
            $this->load->model('business_unit_model');
            // Get all business unit
            $list_business_unit = $this->business_unit_model->get_business_unit_id_name ();
            $this->assign('bu', $list_business_unit);
            $this->assign('d', $item);
            // $_POST = array();
            $this->view('sample/edit_department.tpl');
        } // End else validation fail
    }

    /**
     * From list department screen, user click "Add" to add new department
     * Only for navigation, not save
     * @return
     * @author
     */
    public function add_department() {
        // Load business unit model
        $this->load->model('business_unit_model');
        // Get all business unit
        $list_business_unit = $this->business_unit_model->get_business_unit_id_name ();
        $this->assign('bu', $list_business_unit);
        // If user click "Add"
        // if (isset($_POST ['add_new'])) {
        $this->view('sample/add_department.tpl');
    }

    /**
     * Load detail of department by popup (dialogbox) (b)
     * @author
     * @return
     */
    public function load_detail_b_department() {
        // Check search condition
        // $this->check_search_condition ();
        // print_r($this->search_conditions);
        $id = $this->input->post('id');
        $department = $this->department_model->get_department_by_id($id);
        $this->assign("d", $department);
        $this->view('sample/load_detail_b_department.tpl');
    }

    /**
     * Load detail of department by popup (dialogbox) (c)
     * @author
     * @return
     */
    public function load_detail_c_department() {
        $department = $this->department_model->get_department_by_id($this->input->post('id'));
        $this->assign("d", $department);
        $this->view('sample/load_detail_c_department.tpl');
    }

    /**
     * Edit/Copy department by popup (dialogbox) (c): only load page
     * @return
     * @author
     */
    public function edit_c_department() {
        $id = $this->input->post('id');
        // Load business unit model
        $this->load->model('business_unit_model');
        // Get all business unit
        $list_business_unit = $this->business_unit_model->get_business_unit_id_name ();
        $this->assign('bu', $list_business_unit);
        $department = $this->department_model->get_department_by_id($id);
        $this->assign("d", $department);
        if ($this->input->post('type')== "edit") {
            $this->view('sample/edit_c_department.tpl');
        } else {
            $this->view('sample/copy_c_department.tpl');
        }
    }

    /**
     * Save editting/copying department on dialogbox (c)
     * @return
     * @author
     * Use for adding, copying and editting
     */
    public function edit_c_ajax() {
        // Validation
        if ($this->department_model->validate('edit_department')) {
            // Fields to update
            // $zipcode = trim($this->input->post('zipcode1'). $this->input->post('zipcode2'));
            $data = array (
                    'business_unit_id' => $this->input->post('business_unit_id'),
                    'name' => $this->input->post('name'),
                    'zipcode' => $this->input->post('zipcode'),
                    'address_1' => $this->input->post('address_1'),
                    'address_2' => $this->input->post('address_2'),
                    'phone_no' => $this->input->post('phone_no'),
                    'fax_no' => $this->input->post('fax_no'),
                    'lastup_account_id' => $this->auth->get_account_id ()
            );

            // Update department: Save or insert new (copy) or insert new (add screen)
            if($this->input->post('type')) {
                $this->department_model->insert('department', $data);
            } else {
                $condition = array (
                        'id' => $this->input->post('d_id')
                );
                $this->department_model->update_department('department', $data, $condition);
            }
            $errors ['error'] = 0;
            echo json_encode($errors);
        } else {
            $errors ['error'] = 1;
            // $errors['business_unit_id'] = form_error('business_unit_id');
            $errors ['name'] = form_error('name');
            $errors ['zipcode'] = form_error('zipcode');
            //$errors ['zipcode2'] = form_error('zipcode2');
            $errors ['address_1'] = form_error('address_1');
            // $errors['address_2'] = form_error('address_2');
            $errors ['phone_no'] = form_error('phone_no');
            $errors ['fax_no'] = form_error('fax_no');
            echo json_encode($errors);
        }
    }

    /**
     * Delete
     */
    public function delete_department() {
        // Check if this exists
        $list = array_filter($this->department_model->check_department_exist($this->input->post('d_id')));
        if (!empty($list)) {
            $this->department_model->delete("department",$this->input->post('d_id'));
        }
        // Return to view screen
        redirect_post('/_admin/sample/show_department');
    }
    /*End department*/




}