<?php

class Shipping extends ZR_Controller {
    public function show() {
        $this->load->model('shipping_type_model');
        $shipping_types = $this->shipping_type_model->get_all();

        $this->assign('shipping_types', $shipping_types);
        $this->view('shipping/show.tpl');
    }

    public function detail(){
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('shipping_id');

            $this->load->model('shipping_type_model');
            $shipping_type = $this->shipping_type_model->get_by_id($id);

            if (empty($shipping_type)) {
                $this->ajax_json(array(
                                    'error_code' => 400,
                                    'messages' => lang('lbl_invalid_input')
                                ));
                return;
            }

            $this->assign('shipping_type', $shipping_type);
            $this->view('shipping/detail.tpl');
        } else {
            show_404();
        }
    }

    public function edit() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('shipping_id');

            $this->load->model('shipping_type_model');
            $shipping_type = $this->shipping_type_model->get_by_id($id);
            if (empty($shipping_type)) {
                $this->ajax_json(array(
                                    'error_code' => 400,
                                    'messages' => lang('lbl_invalid_input')
                                ));
                return;
            }

            $this->assign('shipping_type', $shipping_type);
            $this->view('shipping/edit.tpl');
        } else {
            show_404();
        }
    }

    public function update() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('shipping_id');

            $this->load->model('shipping_type_model');
            $shipping_type = $this->shipping_type_model->get_by_id($id);
            if (empty($shipping_type)) {
                $status = array(
                            'status'   => false,
                            'messages' => lang('lbl_edit_error'),
                        );
                echo json_encode($status);
            } else {
                if ($this->shipping_type_model->validate('edit')) {


                    $data = array(
                                'name'                 => $this->input->post('name'),
                                'is_remaining_confirm' => $this->input->post('is_remaining_confirm') ? 1 : 0,
                                'is_advance_payment'   => $this->input->post('is_advance_payment') ? 1 : 0,
                                'is_reporting'         => $this->input->post('is_reporting') ? 1 : 0,
                                'is_no_add'            => $this->input->post('is_no_add') ? 1 : 0,
                                'sequence'             => $this->input->post('sequence'),
                            );
                    $data = array_map('trim', $data);

                    $success = $this->shipping_type_model->update_shipping_type($id, $data);

                    $status = array(
                                'status'   => $success ? true : false,
                                'messages' => $success ? lang('lbl_edit_success') : lang('lbl_edit_error'),
                            );
                } else {
                    $status = array(
                                'status'   => false,
                                'messages' => array(
                                                'name'                 => form_error('name'),
                                                'is_remaining_confirm' => form_error('is_remaining_confirm'),
                                                'is_advance_payment'   => form_error('is_advance_payment'),
                                                'is_reporting'         => form_error('is_reporting'),
                                                'is_no_add'            => form_error('is_no_add'),
                                                'sequence'             => form_error('sequence'),
                                            )
                            );
                }

                echo json_encode($status);
            }
        } else {
             show_404();
        }
    }

    public function add() {
        if ($this->input->is_ajax_request()) {

            $this->load->model('shipping_type_model');
            if ($this->shipping_type_model->validate('add')) {
                $data = $this->input->post();
                $data = array_map('trim', $data);

                $success = $this->shipping_type_model->add_new_shipping_type($data);
                $status = array(
                            'status'   => $success ? true : false,
                            'messages' => $success ? lang('lbl_add_success') : lang('lbl_add_error'),
                        );
            } else {
                $status = array(
                            'status'   => false,
                            'messages' => array(
                                            'name'                 => form_error('name'),
                                            'is_remaining_confirm' => form_error('is_remaining_confirm'),
                                            'is_advance_payment'   => form_error('is_advance_payment'),
                                            'is_reporting'         => form_error('is_reporting'),
                                        ),
                        );
            }

            echo json_encode($status);
        } else {
            show_404();
        }

    }

    public function delete() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('shipping_id');

            $this->load->model('shipping_type_model');
            $shipping_type = $this->shipping_type_model->get_by_id($id);
            if (empty($shipping_type)) {
                $status = array(
                            'status'   => false,
                            'messages' => lang('lbl_edit_error'),
                        );
            } else {
                $success = $this->shipping_type_model->delete_shipping_type($shipping_type);

                if ($success === -1) {

                    return $this->ajax_json(array(
                        'status' => false,
                        'messages' => lang('lbl_delete_in_use_error'),
                    ));
                }

                $status = array(
                            'status'   => $success ? true : false,
                            'messages' => $success ? lang('lbl_delete_success') : lang('lbl_delete_error'),
                        );
            }
            $this->ajax_json($status);
        } else {
            show_404();
        }
    }

}