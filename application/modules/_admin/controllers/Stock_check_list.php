<?php

class Stock_check_list extends ZR_Controller {

    public function __construct() {
        parent::__construct();
    }

    private $_fpdi;
    private $_total_page_print = 0;

    public function show() {

        //get last thursday
        $this_thursday = strtotime("thursday this week");
        $last_thursday = strtotime("last Thursday");

        if ($last_thursday == $this_thursday) {
            $last_thursday -= 7 * 24 * 60 * 60;
        }
        $last_thursday = date('Y/m/d', $last_thursday);

        //get this wednesday
        $this_wednesday = date('Y/m/d', strtotime("wednesday this week"));

        //assign value
        $this->assign('last_thursday', $last_thursday);
        $this->assign('this_wednesday', $this_wednesday);
        $this->view('stock_check_list/show.tpl');
    }

    public function ajax_check_valid_input() {
        //load model
        $this->load->model('order_acceptance_model');

        $error_msg = array();
        $delivery_date_from = false;
        $delivery_date_to = false;

        if ($post = $this->input->post()) {
            if (isset($post['delivery_date_from']) && ! empty($post['delivery_date_from'])) {
                if ( ! $delivery_date_from = parse_date($post['delivery_date_from'], true)) {
                    $error_msg['delivery_date_from'] = lang('delivery_date_from_invalid');
                }
            } else {
                $error_msg['delivery_date_from'] = lang('delivery_date_from_required');
            }

            if (isset($post['delivery_date_to']) && ! empty($post['delivery_date_to'])) {
                if ( ! $delivery_date_to = parse_date($post['delivery_date_to'], true)) {
                    $error_msg['delivery_date_to'] = lang('delivery_date_to_invalid');
                }
            } else {
                $error_msg['delivery_date_to'] = lang('delivery_date_to_required');
            }

            if ($delivery_date_from && $delivery_date_to) {
                if (count(explode('-', $delivery_date_from)) == 2) {
                    $delivery_date_from .= '-01';
                }

                if (count(explode('-', $delivery_date_to)) == 2) {
                    $delivery_date_to .= '-31';
                }

                if (strtotime($delivery_date_to) < strtotime($delivery_date_from)) {
                    $error_msg['delivery_date_from'] = lang('delivery_date_range_invalid');
                }
            }

            //get data[$i] to report
            $params = array(
                    'delivery_date_from' => $delivery_date_from,
                    'delivery_date_to' => $delivery_date_to,
            );

            if (empty($error_msg)) {
                if ( ! $order_acceptances = $this->order_acceptance_model->get_remain_order_acceptance($params)) {
                    $error_msg['delivery_date_from'] = lang('no_data_found');
                };
            }

            if (empty($error_msg)) {
                echo $this->ajax_json(array('result' => true, 'error_msg' => array())); exit();
            }
        }

        echo $this->ajax_json(array('result' => false, 'error_msg' => $error_msg)); exit();
    }

    public function print_report() {
        //load model
        $this->load->model('order_acceptance_model');
        $this->load->model('subcontractor_relation_model');
        $this->load->model('subcontractor_model');

        //load library
        $this->load->library('tcpdf/Tcpdf.php');
        $this->load->library('tcpdf/fpdi.php');
        $this->load->helper('zenrin_pdf_helper');

        if ($get = $this->input->get()) {
            $delivery_date_from = false;
            $delivery_date_to = false;

            if ( ! isset($get['dlf']) || empty($get['dlf']) || ! $delivery_date_from = parse_date($get['dlf'], true)) {
                show_404();
            }

            if ( ! isset($get['dlt']) || empty($get['dlt']) || ! $delivery_date_to = parse_date($get['dlt'], true)) {
                show_404();
            }

            if ($delivery_date_from && $delivery_date_to) {
                if (count(explode('-', $delivery_date_from)) == 2) {
                    $delivery_date_from .= '-01';
                }

                if (count(explode('-', $delivery_date_to)) == 2) {
                    $delivery_date_to .= '-31';
                }

                if (strtotime($delivery_date_to) < strtotime($delivery_date_from)) {
                    show_404();
                }
            }

            //get data to report
            $params = array(
                    'delivery_date_from' => $delivery_date_from,
                    'delivery_date_to' => $delivery_date_to,
            );

            if ( ! $order_acceptances = $this->order_acceptance_model->get_remain_order_acceptance($params)) {
                show_404();
            };

            $data_to_report = array();
            $report_keys = array();

            foreach ($order_acceptances as &$item) {
            	$params = array('conditions' => array('parent_subcontractor_id' => $item['work_subcontractor_id']));

            	$subcontractor_relations = $this->subcontractor_relation_model->get_subcontractors_relations_misc("parent_subcontractor_id", $item['work_subcontractor_id']);

            	if (count($subcontractor_relations) > 0) {

            		$the_id = 0;
            		foreach ($subcontractor_relations as $val) {
            			$the_id = $val['parent_subcontractor_id'];
            			break;
            		}

            		if ($the_id > 0) {
            			$params = array('conditions' => array("id" => $the_id));
            			$subcontractor = $this->subcontractor_model->get_subcontractors_by_params($params);
            			if (count($subcontractor) > 0) {
            				$item['source_subcontractor_id'] = $subcontractor[0]['id'];
            				$item['source_subcontractor_name'] = $subcontractor[0]['name'];
            			}
            		}

            	} else {
            		$subcontractor_relations = $this->subcontractor_relation_model->get_subcontractors_relations_misc("child_subcontractor_id", $item['work_subcontractor_id']);
            		if (count($subcontractor_relations) > 0) {
            			$the_id = 0;
            			foreach ($subcontractor_relations as $val) {
            				$the_id = $val['parent_subcontractor_id'];
            				break;
            			}

            			if ($the_id > 0) {
            				$params = array('conditions' => array("id" => $the_id));
            				$subcontractor = $this->subcontractor_model->get_subcontractors_by_params($params);
            				if (count($subcontractor) > 0) {
            					$item['source_subcontractor_id'] = $subcontractor[0]['id'];
            					$item['source_subcontractor_name'] = $subcontractor[0]['name'];
            				}
            			}
            		} else {
            			$params = array('conditions' => array("id" => $item['work_subcontractor_id']));
            			$subcontractor = $this->subcontractor_model->get_subcontractors_by_params($params);
            			if (count($subcontractor) > 0) {
            				$item['source_subcontractor_id'] = $subcontractor[0]['id'];
            				$item['source_subcontractor_name'] = $subcontractor[0]['name'];
            			}
            		}
            	}
            }

            //foreach ($order_acceptances as $item) {
            //tmp#6868, #6781: Start
        	for ($i = 0; $i < count($order_acceptances); $i++) {

				$the_item = $order_acceptances[$i];
				if ($the_item['work_subcontractor_id'] != 0) {
                	$key = $the_item['source_subcontractor_id'] . $the_item['work_subcontractor_id'];

            	    if ( ! in_array($key, $report_keys)) {
                    	$report_keys[] = $key;
                	}

                	$data_to_report[$key][] = $the_item;
				}
            }
            //tmp#6868, #6781: End

            //config library
            $this->_fpdi = new FPDI();
            $this->_fpdi->setPrintHeader(false);
            $this->_fpdi->setPrintFooter(false);
            $this->_fpdi->SetAutoPageBreak(TRUE, 0);

            //print data
            foreach ($data_to_report as $item) {
                $template_configs = $this->_get_template_from_data($item);
                $this->_generate_pdf_data($item, $template_configs);
            }

            $this->_fpdi->Output();
        }

        show_404();
    }

    /**
     * Get template from data
     *
     * @param array $data
     * @return mixed|array
     *
     * @author hoang_minh
     * @since 2015-08-07
     */
    private function _get_template_from_data($data) {
        if (empty($data)) {
            return false;
        }

        //get number pages to generate data
        $number_rows = count($data);
        if ($number_rows < 27) {
            $number_pages = 1;
        } else {
            $number_pages = 2;
            $number_rows -= 54;

            while ($number_rows > 0) {
                $number_rows -= 26;
                $number_pages++;
            }
        }

		//posX: Y position
        //prepare template for data
        $template_configs = array();
        if ($number_pages == 1) {
            $template_configs[0]['path'] = APPPATH . 'config/pdf_template/stock_check_list/header_footer.pdf';
            $template_configs[0]['posX'] = 41;
            $template_configs[0]['page_number'] = 1 + $this->_total_page_print;
            $template_configs[0]['begin'] = 0;
            $template_configs[0]['end'] = count($data);

        } elseif ($number_pages == 2) {
            $template_configs[0]['path'] = APPPATH . 'config/pdf_template/stock_check_list/header.pdf';
            $template_configs[0]['posX'] = 41;
            $template_configs[0]['page_number'] = 1 + $this->_total_page_print;
            $template_configs[0]['begin'] = 0;
            $template_configs[0]['end'] = 26;

            $template_configs[1]['path'] = APPPATH . 'config/pdf_template/stock_check_list/header_footer.pdf';
            $template_configs[1]['posX'] = 41;
            $template_configs[1]['page_number'] = $number_pages + $this->_total_page_print;
            $template_configs[1]['begin'] = 26;
            $template_configs[1]['end'] = count($data);
        } else {
            $template_configs[0]['path'] = APPPATH . 'config/pdf_template/stock_check_list/header.pdf';
            $template_configs[0]['posX'] = 41;
            $template_configs[0]['page_number'] = 1 + $this->_total_page_print;
            $template_configs[0]['begin'] = 0;
            $template_configs[0]['end'] = 26;

            $start = 26;
            for ($i = 1; $i < $number_pages - 1; $i++) {
                $template_configs[$i]['path'] = APPPATH . 'config/pdf_template/stock_check_list/header.pdf';
                $template_configs[$i]['posX'] = 41;
                $template_configs[$i]['page_number'] = $i + 1 + $this->_total_page_print;
                $template_configs[$i]['begin'] = $start;
                $template_configs[$i]['end'] = $start + 26;

                $start += 26;
            }

            $template_configs[$number_pages - 1]['path'] = APPPATH . 'config/pdf_template/stock_check_list/header_footer.pdf';
            $template_configs[$number_pages - 1]['posX'] = 41;
            $template_configs[$number_pages - 1]['page_number'] = $number_pages + $this->_total_page_print;
            $template_configs[$number_pages - 1]['begin'] = $start;
            $template_configs[$number_pages - 1]['end'] = count($data);
        }
        $this->_total_page_print += $number_pages;

        return $template_configs;
    }

    /**
     * Generate pdf data
     *
     * @param array $data
     * @param string $template
     * @param int $begin_pos
     *
     * @author hoang_minh
     * @since 2015-08-06
     */


    private function _generate_pdf_data($data, $template_configs) {

        $page = 1;
        $total_page = count($template_configs);
        $regularFont = 'msgothici';

        foreach ($template_configs as $cfg) {
            $y = $cfg['posX'];

            $this->_fpdi->AddPage('L', 'A4');
            $this->_fpdi->setSourceFile($cfg['path']);

            $header_id = $this->_fpdi->importPage(1);
            $this->_fpdi->useTemplate($header_id, null, null, null, null, true);
            $this->_fpdi->setPage($cfg['page_number']);

            //if ($page == 1) {
                $this->_fpdi->SetFont($regularFont, 'B', 27);
                $this->_fpdi->setXY(57.5, 21.3);
                $this->_fpdi->cell(23.9, 6, $data[0]['source_subcontractor_name'], 0, '', 'L');
            //}

            $this->_fpdi->SetFont($regularFont, 'B', 6);

            //generate data
            for ($i = $cfg['begin']; $i < $cfg['end']; $i++) {

            	$client_name_length = mb_strlen($data[$i]['client_name'], 'UTF-8');
            	$stretch = 0;
            	if ($client_name_length > 28)
            		$stretch = 1;

                //set data
                $this->_fpdi->setXY(13.1, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(23.9, 6, $data[$i]['work_subcontractor_name'], 1, '', 'L');

                $this->_fpdi->setXY(37, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(21.4, 6, $data[$i]['shipping_subcontractor_name'], 1, '', 'L');

                $this->_fpdi->setXY(58.4, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(18.1, 6, $data[$i]['j_account_name'], 1, '', 'L');

                $this->_fpdi->setXY(76.6, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(22.8, 6, $data[$i]['arrange_rep_account_name'], 1, '', 'L');

                $this->_fpdi->setXY(99.4, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(21, 6, $data[$i]['shipping_type_name'], 1, '', 'L');

                $this->_fpdi->setXY(120.4, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                //$this->_fpdi->cell(59.3, 6, $data[$i]['client_name'] . " (" . mb_strlen($data[$i]['client_name'], 'UTF-8') . ")", 1, '', 'L')->SetFontSize(20);
                $this->_fpdi->cell(59.3, 6, $data[$i]['client_name'], 1, '', 'L', false, '', $stretch);

                $this->_fpdi->setXY(179.8, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(15.6, 6, number_format($data[$i]['quantity']), 1, '', 'R');

                $this->_fpdi->setXY(195.4, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(18.1, 6, date('Y/m/d', strtotime($data[$i]['delivery_date'])), 1, '', 'R');

                $this->_fpdi->setXY(213.6, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(17.6, 6, $data[$i]['j_code'], 1, '', 'R');

                $this->_fpdi->setXY(231.2, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(5.7, 6, $data[$i]['branch_cd'], 1, '', 'R');

                $this->_fpdi->setXY(237, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(10.3, 6, '', 1, '', 'L');

                $this->_fpdi->setXY(247.5, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(12.3, 6, '', 1, '', 'L');

                $this->_fpdi->setXY(260, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(12.6, 6, '', 1, '', 'L');

                $this->_fpdi->setXY(272.8, $y);
                $this->_fpdi->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
                $this->_fpdi->cell(12.1, 6, '', 1, '', 'L');

                $y += 6;
            }

            //pagination
            if ($total_page > 1) {
                $this->_fpdi->setXY(145, 198.5);
                $this->_fpdi->cell(16.9, 6, $page . '／' . $total_page . 'ページ', 0, '', 'L');
            }

            //time remark
            if ($page == $total_page) {

                $this->_fpdi->setXY(259, 198.5);
                $this->_fpdi->cell(16.9, 6, date('Y/m/d H:i'), 0, '', 'L');
            }

            $page++;
        }
    }

}
