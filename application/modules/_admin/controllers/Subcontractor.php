<?php
/**
 * Subcontractor's management.
 *
 * @author hoang_minh
 * @since 2015-05-11
 */
class Subcontractor extends ZR_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display list of subcontrators with search or without search action.
     *
     * @author hoang_minh
     * @since 2015-05-11
     */
    public function show() {
        //load model
        $this->load->model('subcontractor_model');
//#9695:Start
        $this->load->model('account_model');
        $this->load->model('business_unit_model');
//#9695:End

        //get configs
        $direct_consignment_configs = $this->subcontractor_model->get_configs('direct_consignment');

        //init variables
        $subcontractors = array();
        $post = array();
        $params = array();
        $params['conditions'] = array();
        $params['orders'] = array('code ' => 'ASC',"id" => "ASC");

        $return_data = array(
                    'search_code' => '',
                    'search_name' => '',
                    'search_abbr_name' => '',
                    'search_direct_consignment' => $direct_consignment_configs[0]['value'],
                    'pg' => '',
                    'back_link' => create_current_url()
        );

        //get searching conditions
        if ($post = $this->input->post()) {

            if (isset($post['search_code']) && $post['search_code'] != '') {
                $params['conditions'] += array('code' => $post['search_code']);
                $return_data['search_code'] = htmlspecialchars($post['search_code']);
            }

            if (isset($post['search_name']) && $post['search_name'] != '') {
                $params['conditions'] += array('name' => $post['search_name']);
                $return_data['search_name'] = htmlspecialchars($post['search_name']);
            }

            if (isset($post['search_abbr_name']) && $post['search_abbr_name'] != '') {
                $params['conditions'] += array('abbr_name' => $post['search_abbr_name']);
                $return_data['search_abbr_name'] = htmlspecialchars($post['search_abbr_name']);
            }

            if (isset($post['search_direct_consignment']) && $post['search_direct_consignment'] != $direct_consignment_configs[0]['value']) {
                $params['conditions'] += array('direct_consignment' => $post['search_direct_consignment']);
                $return_data['search_direct_consignment'] = htmlspecialchars($post['search_direct_consignment']);
            }

            if (isset($post['pg'])) {
                $return_data['pg'] = $post['pg'];
            }
        }
//#9695:Start
        $account_business = $this->account_model->get_account_business_unit($this->auth->get_account_id());
        $business_unit_by_template = $this->business_unit_model->get_all_business_unit_by_template_code($account_business[0]['finish_work_report_template']);
        $params['conditions']['business_unit_id'] = $business_unit_by_template;
//#9695:End

        //get subcontrator
        //print_r($params);
        $subcontractors = $this->subcontractor_model->set_pager()->get_subcontractors_by_params($params);

        //check if user have role add, edit
        $have_add_edit_delete_role = $this->auth->has_permission('brain_add_edit_delete', 'column');

        //assign variables
        $this->assign('subcontractor_configs', $direct_consignment_configs);
        $this->assign('post', $return_data);
        $this->assign('subcontractors', $subcontractors);
        $this->assign('have_add_edit_delete_role', $have_add_edit_delete_role);

        //load view
        $this->view('subcontractor/show.tpl');
    }

    /**
     * Subcontractor detail
     *
     * @author hoang_minh
     * @since 2015-05-11
     */
    public function detail() {
        //load model
        $this->load->model('subcontractor_model');
        $this->load->model('subcontractor_relation_model');

        //init variables
        $post = array();
        $subcontractor = array();
        $post = $this->input->post();

        //get configs
        $is_no_add_configs= $this->subcontractor_model->get_configs('is_no_add');
        $direct_consignment_configs = $this->subcontractor_model->get_configs('direct_consignment');


        if ( ! $post || ! isset($post['id']) || ! isset($post['back_link'])) {
            redirect_post('_admin/subcontractor/show');
        }

        //get subcontractor by subcontractor_id which be posted.
        $params = array(
            'conditions' => array('id' => $post['id'])
        );
        $subcontractor = $this->subcontractor_model->get_subcontractors_by_params($params);

        if ( ! $subcontractor) {
            $this->_set_error(lang('subcontractor_not_exist_error_msg'));
        }

        //get relations of subcontractor
        $relation_key = 'child_subcontractor_id';
        if ($subcontractor[0]['direct_consignment'] == $direct_consignment_configs[1]['value']) {
            $relation_key = 'parent_subcontractor_id';
        }
        $params['conditions'] = array();
        $params['conditions'] += array($relation_key => $subcontractor[0]['id']);
       // $params['']
        $subcontractor_relations = $this->subcontractor_relation_model->get_subcontractors_relations_by_params($params);
        foreach ($subcontractor_relations as &$val) {
            $val['name'] = htmlspecialchars($val['name']);
        }

        //check if user have role add, edit
        $have_add_edit_delete_role = $this->auth->has_permission('brain_add_edit_delete', 'column');

        //assign values
        $this->assign('post', $post);
        $this->assign('subcontractor', $subcontractor[0]);
        $this->assign('is_no_add_configs', $is_no_add_configs);
        $this->assign('direct_consignment_configs', $direct_consignment_configs);
        $this->assign('subcontractor_relations', $subcontractor_relations);
        $this->assign('have_add_edit_delete_role', $have_add_edit_delete_role);

        //load view
        $this->view('subcontractor/dialog_forms/detail.tpl');
    }

    /**
     * Add subcontractor
     *
     * @author hoang_minh
     * @since 2015-05-14
     */
    public function add() {
        //load model
        $this->load->model('subcontractor_model');
        $this->load->model('subcontractor_relation_model');
//#9695:Start
        $this->load->model('account_model');
//#9695:End

        //get configs
        $is_no_add_configs= $this->subcontractor_model->get_configs('is_no_add');
        $direct_consignment_configs = $this->subcontractor_model->get_configs('direct_consignment');

        //init variables
        $post = array();
        $error_msg = array('form' => '');

        if ($post = $this->input->post()) {
            //check if have submit
            if (isset($post['save'])) {

                //validation
                if ($this->subcontractor_model->validate('subcontractor')) {

                    //check if new subcontractor is child and have parent or not.
                    if (($post['direct_consignment'] == $direct_consignment_configs[2]['value'])
                            && ( ! isset($post['new_relations']) || empty($post['new_relations']))) {

                        $error_msg['parent_subcontractor_error_msg'] = '<span class="validation-error">' . lang('parent_subcontractor_is_required') . '</span>';
                        echo $this->ajax_json(array('result' => false, 'error_msg' => $error_msg)); exit();
                    }

                    try {
                        $this->subcontractor_model->begin_transaction();
//#9695:Start
                        $account_business = $this->account_model->get_account_business_unit($this->auth->get_account_id());
                        $account_business = $account_business[0];
//#9695:End
                        //prepair subcontractor data
                        $insert_data = array(
                                            'code' => $post['code'],
                                            'name' => $post['name'],
                                            'abbr_name' => $post['abbr_name'],
                                            'direct_consignment' => $post['direct_consignment'],
                                            'is_no_add' => $post['is_no_add'],
//#9695:Start
                                            'business_unit_id' => $account_business['id'],
//#9695:End
                        );

                        //insert subcontractor
                        if ( ! $this->subcontractor_model->insert_subcontractor($insert_data)) {
                            throw new Exception(lang('add_error_msg'));
                        }
                        $subcontract_id = $this->subcontractor_model->get_insert_id();
                        $insert_data = array();

                        if (isset($post['new_relations'])) {

                            //check exist data
                            foreach ($post['new_relations'] as $val) {
                                $params = array();
                                $params['conditions'] = array('id' => $val);
                                if ( ! $this->subcontractor_model->get_subcontractors_by_params($params)) {
                                    throw new Exception(lang('subcontractor_not_exist_error_msg'));
                                }
                            }

                            //prepair subcontractor relation data
                            $insert_data = array();

                            //if subcontractor type is indirect
                            if ($post['direct_consignment'] == $direct_consignment_configs[2]['value']) {
                                foreach ($post['new_relations'] as $val) {
                                    $insert_data[] = array(
                                                        'parent_subcontractor_id' => $val,
                                                        'child_subcontractor_id'  => $subcontract_id,
                                                        'create_datetime' => $this->subcontractor_model->_db_now(),
                                                        'lastup_datetime' => $this->subcontractor_model->_db_now()
                                    );
                                }

                            //if subcontractor type is direct
                            } elseif ($post['direct_consignment'] == $direct_consignment_configs[1]['value']) {
                                foreach ($post['new_relations'] as $val) {
                                    $insert_data[] = array(
                                                        'parent_subcontractor_id' => $subcontract_id,
                                                        'child_subcontractor_id'  => $val,
                                                        'create_datetime' => $this->subcontractor_model->_db_now(),
                                                        'lastup_datetime' => $this->subcontractor_model->_db_now()
                                    );
                                }
                            }

                            //insert subcontractor relation
                            if ( ! $this->subcontractor_relation_model->insert_subcontractor_relations($insert_data)) {
                                throw new Exception(lang('add_subcontractor_relation_error_msg'));
                            }

                        }

                        $this->subcontractor_model->commit_transaction();
                        echo $this->ajax_json(array('result' => true, 'error_msg' => array('form' => lang('add_success_msg')))); exit();

                    } catch (Esxception $ex) {
                        $this->app_logger->error_log($ex->getMessage());

                        $error_msg['form'] = $ex->getMessage();
                        $this->subcontractor_model->rollback_transaction();
                        echo $this->ajax_json(array('result' => false, 'error_msg' => array('form' => lang('add_error_msg')))); exit();
                    }
                } else {
                    //get error msg
                    $error_msg['form'] = lang('add_error_msg');
                    $error_msg['code'] = form_error('code');
                    $error_msg['name'] = form_error('name');
                    $error_msg['abbr_name'] = form_error('abbr_name');
                    $error_msg['is_no_add'] = form_error('is_no_add');
                    $error_msg['direct_consignment'] = form_error('direct_consignment');

                    //check if new subcontractor is child and have parent or not.
                    if (($post['direct_consignment'] == $direct_consignment_configs[2]['value'])
                            && ( ! isset($post['new_relations']) || empty($post['new_relations']))) {

                        $error_msg['parent_subcontractor_error_msg'] = '<span class="validation-error">' . lang('parent_subcontractor_is_required') . '</span>';
                    }

                    echo $this->ajax_json(array('result' => false, 'error_msg' => $error_msg)); exit();
                }
            } else {

                //init data
                $post['code'] = '';
                $post['name'] = '';
                $post['abbr_name'] = '';
                $post['is_no_add'] = $is_no_add_configs[1]['value'];
                $post['direct_consignment'] = $direct_consignment_configs[2]['value'];
            }
        } else {
            redirect_post('_admin/subcontractor/show');
        }

        //return data
        $hidden_data = array(
                'search_code' => (isset($post['search_code'])) ? $post['search_code'] : '',
                'search_name' => (isset($post['search_name'])) ? $post['search_name'] : '',
                'search_abbr_name' => (isset($post['search_abbr_name'])) ? $post['search_abbr_name'] : '',
                'search_direct_consignment' => (isset($post['search_direct_consignment'])) ? $post['search_direct_consignment'] : $direct_consignment_configs[0]['value'],
                'back_link' => (isset($post['back_link'])) ? $post['back_link'] : '',
        );

        //asign data
        $this->assign('is_no_add_configs', $is_no_add_configs);
        $this->assign('direct_consignment_configs', $direct_consignment_configs);
        $this->assign('error_msg', $error_msg);
        $this->assign('hidden_data', $hidden_data);
        $this->assign('post', $post);

        //load view
        $this->view('subcontractor/dialog_forms/add.tpl');
    }

    /**
     * Edit subcontractor
     *
     * @author hoang_minh
     * @since 2015-05-18
     */
    public function edit() {
        //load model
        $this->load->model('subcontractor_model');
        $this->load->model('subcontractor_relation_model');

        //get configs
        $is_no_add_configs= $this->subcontractor_model->get_configs('is_no_add');
        $direct_consignment_configs = $this->subcontractor_model->get_configs('direct_consignment');

        //init variables
        $post = array();
        $error_msg = array('form' => '');
        $post = $this->input->post();

        if ( ! $post || ! isset($post['id']) || ! isset($post['back_link'])) {
            redirect_post('_admin/subcontractor/show');
        }

        //get subcontractor by subcontractor_id which be deleted.
        $params = array(
                'conditions' => array('id' => $post['id']),
        );
        $subcontractor = $this->subcontractor_model->get_subcontractors_by_params($params);
        if (empty($subcontractor)) {
            $this->_set_error(lang('subcontractor_not_exist_error_msg'));
        }

        //check if have submit
        if (isset($post['save'])) {

            //validation
            if ($this->subcontractor_model->validate('subcontractor', array('id' => $post['id']))) {

                //check if new subcontractor is child and have parent or not.
                if (($post['direct_consignment'] == $direct_consignment_configs[2]['value'])
                   && ( ! isset($post['new_relations']) || empty($post['new_relations']))) {

                   $error_msg['parent_subcontractor_error_msg'] = '<span class="validation-error">' . lang('parent_subcontractor_is_required') . '</span>';
                   echo $this->ajax_json(array('result' => false, 'error_msg' => $error_msg)); exit();
                }

                try {
                    $this->subcontractor_model->begin_transaction();

                    //prepair subcontractor data
                    $update_data = array(
                            'code' => $post['code'],
                            'name' => $post['name'],
                            'abbr_name' => $post['abbr_name'],
                            'direct_consignment' => $post['direct_consignment'],
                            'is_no_add' => $post['is_no_add'],
                            'lastup_account_id' => $this->auth->get_account_id()
                    );

                    $update_conditions = array(
                            'id' => $post['id']
                    );

                    $params = array(
                                'update_data' => $update_data,
                                'update_conditions' => $update_conditions
                    );

                    //update subcontractor
                    if ( ! $this->subcontractor_model->update_subcontractor($params)) {
                        throw new Exception(lang('update_error_msg'));
                    }

                    //if have changed direct consignment
                    if ($subcontractor[0]['direct_consignment'] != $post['direct_consignment']) {

                        //delete all relations
                        $update_conditions = array(
                                'parent_subcontractor_id' => $post['id']
                        );

                        $update_data = array(
                                'lastup_account_id' => $this->auth->get_account_id(),
                                'lastup_datetime' =>  $this->subcontractor_model->_db_now()
                        );

                        $params = array(
                                'update_data' => $update_data,
                                'update_conditions' => $update_conditions
                        );

                        if ( ! $this->subcontractor_relation_model->delete_subcontractor_relations($params)) {
                            throw new Exception(lang('delete_subcontractor_relation_error_msg'));
                        }

                        $update_conditions = array(
                                'child_subcontractor_id' => $post['id']
                        );

                        $params = array(
                                'update_data' => $update_data,
                                'update_conditions' => $update_conditions
                        );

                        if ( ! $this->subcontractor_relation_model->delete_subcontractor_relations($params)) {
                            throw new Exception(lang('delete_subcontractor_relation_error_msg'));
                        }

                        //if have been added new relations
                        $insert_data = array();
                        if (isset($post['new_relations'])) {

                            //check exist data
                            foreach ($post['new_relations'] as $val) {
                                $params = array();
                                $params['conditions'] = array('id' => $val);
                                if ( ! $this->subcontractor_model->get_subcontractors_by_params($params)) {
                                    throw new Exception(lang('subcontractor_not_exist_error_msg'));
                                }
                            }

                            //if is indirect
                            if ($direct_consignment_configs[1]['value'] != $post['direct_consignment']) {
                                foreach ($post['new_relations'] as $val) {
                                    $insert_data[] = array(
                                            'parent_subcontractor_id' => $val,
                                            'child_subcontractor_id'  => $post['id'],
                                            'create_datetime' => $this->subcontractor_model->_db_now(),
                                            'lastup_datetime' => $this->subcontractor_model->_db_now()
                                    );
                                }
                            } else {
                                foreach ($post['new_relations'] as $val) {
                                    $insert_data[] = array(
                                            'child_subcontractor_id' => $val,
                                            'parent_subcontractor_id'  => $post['id'],
                                            'create_datetime' => $this->subcontractor_model->_db_now(),
                                            'lastup_datetime' => $this->subcontractor_model->_db_now()
                                    );
                                }
                            }

                            if ( ! $this->subcontractor_relation_model->insert_subcontractor_relations($insert_data)) {
                                throw new Exception(lang('add_subcontractor_relation_error_msg'));
                            }
                        }
                    } else {

                        if (isset($post['new_relations'])) {

                            //check exist data
                            foreach ($post['new_relations'] as $val) {
                                $params = array();
                                $params['conditions'] = array('id' => $val);
                                if ( ! $this->subcontractor_model->get_subcontractors_by_params($params)) {
                                    throw new Exception(lang('subcontractor_not_exist_error_msg'));
                                }
                            }

                            //init variables
                            $current_relations = array();
                            $old_relations = array();
                            $new_relations = $post['new_relations'];

                            $params = array();
                            $params['conditions'] = array('child_subcontractor_id' => $subcontractor[0]['id']);

                            if ($direct_consignment_configs[1]['value'] == $post['direct_consignment']) {
                                $params['conditions'] = array('parent_subcontractor_id' => $subcontractor[0]['id']);
                            }
                            $current_relations = $this->subcontractor_relation_model->get_subcontractors_relations_by_params($params);

                            //get relations will be deleted, added
                            foreach ($current_relations as $val) {
                                if ( ! in_array($val['id'], $new_relations)) {
                                    $old_relations[] = $val['id'];
                                } else {
                                    $index = array_search($val['id'], $new_relations);
                                    unset($new_relations[$index]);
                                }
                            }

                            //delete old relations
                            if ( ! empty($old_relations)) {
                                $deleted_ids = implode(',', $old_relations);

                                $update_conditions = array();
                                $update_data = array(
                                        'lastup_account_id' => $this->auth->get_account_id(),
                                        'lastup_datetime' => $this->subcontractor_model->_db_now()
                                );


                                if ($direct_consignment_configs[1]['value'] != $post['direct_consignment']) {
                                    $update_conditions += array(
                                            'parent_subcontractor_id' => $deleted_ids
                                    );
                                } else {
                                    $update_conditions += array(
                                            'child_subcontractor_id' => $deleted_ids
                                    );
                                }

                                $params = array(
                                        'update_data' => $update_data,
                                        'update_conditions' => $update_conditions,
                                );

                                //delete relation
                                if ( ! $this->subcontractor_relation_model->delete_subcontractor_relations($params)) {
                                    throw new Exception(lang('delete_subcontractor_relation_error_msg'));
                                }
                            }

                            //add new relations
                            if (! empty($new_relations)) {
                                $insert_data = array();

                                if ($direct_consignment_configs[1]['value'] != $post['direct_consignment']) {
                                    foreach ($new_relations as $val) {
                                        $insert_data[] = array(
                                                'parent_subcontractor_id' => $val,
                                                'child_subcontractor_id'  => $post['id'],
                                                'create_datetime' => $this->subcontractor_model->_db_now(),
                                                'lastup_datetime' => $this->subcontractor_model->_db_now()
                                        );
                                    }
                                } else {
                                    foreach ($new_relations as $val) {
                                        $insert_data[] = array(
                                                'child_subcontractor_id' => $val,
                                                'parent_subcontractor_id'  => $post['id'],
                                                'create_datetime' => $this->subcontractor_model->_db_now(),
                                                'lastup_datetime' => $this->subcontractor_model->_db_now()
                                        );
                                    }
                                }

                                if ( ! $this->subcontractor_relation_model->insert_subcontractor_relations($insert_data)) {
                                    throw new Exception(lang('add_subcontractor_relation_error_msg'));
                                }
                            }
                        } else {
                            if ($direct_consignment_configs[1]['value'] == $post['direct_consignment']) {
                                $update_conditions = array(
                                        'parent_subcontractor_id' => $post['id']
                                );
                            } else {
                                $update_conditions = array(
                                        'child_subcontractor_id' => $post['id']
                                );
                            }
                            //delete relations
                            $update_data = array(
                                    'lastup_account_id' => $this->auth->get_account_id(),
                                    'lastup_datetime' => $this->subcontractor_model->_db_now()
                            );

                            $params = array(
                                    'update_data' => $update_data,
                                    'update_conditions' => $update_conditions
                            );

                            if ( ! $this->subcontractor_relation_model->delete_subcontractor_relations($params)) {
                                throw new Exception(lang('delete_subcontractor_relation_error_msg'));
                            }
                        }
                    }

                    $this->subcontractor_model->commit_transaction();
                    echo $this->ajax_json(array('result' => true, 'error_msg' => array('form' => lang('update_success_msg')))); exit();

                } catch (Exception $ex) {
                    $this->app_logger->error_log($ex->getMessage());

                    $error_msg['form'] = $ex->getMessage();
                    $this->subcontractor_model->rollback_transaction();
                    echo $this->ajax_json(array('result' => false, 'error_msg' => array('form' => lang('update_error_msg')))); exit();
                }

            } else {
                //get error msg
                $error_msg['form'] = lang('update_error_msg');
                $error_msg['code'] = form_error('code');
                $error_msg['name'] = form_error('name');
                $error_msg['abbr_name'] = form_error('abbr_name');
                $error_msg['is_no_add'] = form_error('is_no_add');
                $error_msg['direct_consignment'] = form_error('direct_consignment');

                //check if new subcontractor is child and have parent or not.
                if (($post['direct_consignment'] == $direct_consignment_configs[2]['value'])
                    && ( ! isset($post['new_relations']) || empty($post['new_relations']))) {

                    $error_msg['parent_subcontractor_error_msg'] = '<span class="validation-error">' . lang('parent_subcontractor_is_required') . '</span>';
                }

                echo $this->ajax_json(array('result' => false, 'error_msg' => $error_msg)); exit();
            }
        } else {
            //init data
            $post['code'] =  $subcontractor[0]['code'];
            $post['name'] = $subcontractor[0]['name'];
            $post['abbr_name'] = $subcontractor[0]['abbr_name'];
            $post['is_no_add'] = $subcontractor[0]['is_no_add'];
            $post['direct_consignment'] = $subcontractor[0]['direct_consignment'];
        }

        //return data
        $hidden_data = array(
                'search_code' => (isset($post['search_code'])) ? $post['search_code'] : '',
                'search_name' => (isset($post['search_name'])) ? $post['search_name'] : '',
                'search_abbr_name' => (isset($post['search_abbr_name'])) ? $post['search_abbr_name'] : '',
                'search_direct_consignment' => (isset($post['search_direct_consignment'])) ? $post['search_direct_consignment'] : $direct_consignment_configs[0]['value'],
                'back_link' => (isset($post['back_link'])) ? $post['back_link'] : '',
        );

        //asign data
        $this->assign('is_no_add_configs', $is_no_add_configs);
        $this->assign('direct_consignment_configs', $direct_consignment_configs);
        $this->assign('error_msg', $error_msg);
        $this->assign('hidden_data', $hidden_data);
        $this->assign('post', $post);

        //load view
        $this->view('subcontractor/dialog_forms/edit.tpl');
    }


    /**
     * Delete subcontractor
     *
     * @author hoang_minh
     * @since 2015-05-12
     */
    public function delete() {
        //load model
        $this->load->model('subcontractor_model');
        $this->load->model('subcontractor_relation_model');
        $this->load->model('order_acceptance_model');

        //init variables
        $post = array();
        $subcontractor = array();
        $post = $this->input->post();

        if ( ! $post || ! isset($post['id'])) {
            redirect_post('_admin/subcontractor/show');
        }

        //get subcontractor by subcontractor_id which be deleted.
        $params = array(
                'conditions' => array('id' => $post['id']),
        );

        $subcontractor = $this->subcontractor_model->get_subcontractors_by_params($params);
        if ( ! $subcontractor) {
           echo $this->ajax_json(array('result' => false, 'error_msg' => array('form' => lang('subcontractor_not_exist_error_msg')))); exit();
        }

        //check subcontractor is parent and have child
        $direct_consignment_configs = $this->subcontractor_model->get_configs('direct_consignment');
        $have_chlid_conditions = array(
                'parent_subcontractor_id' => $post['id']
        );

        if (($subcontractor[0]['direct_consignment'] == $direct_consignment_configs[1]['value'])
           && ($this->subcontractor_relation_model->get_subcontractors_relations_by_params(array('conditions' => $have_chlid_conditions)))) {

           echo $this->ajax_json(array('result' => false, 'error_msg' => array('form' => lang('cannot_delete_parent_error_msg')))); exit();
        }

        //check subcontractor is use in order_acceptance
        if ($this->order_acceptance_model->get_order_acceptance_by_conditions(array('conditions' => array('source_subcontractor_id' => $post['id'])))
           || $this->order_acceptance_model->get_order_acceptance_by_conditions(array('conditions' => array('work_subcontractor_id' => $post['id'])))
           || $this->order_acceptance_model->get_order_acceptance_by_conditions(array('conditions' => array('shipping_subcontractor_id' => $post['id'])))) {

               echo $this->ajax_json(array('result' => false, 'error_msg' => array('form' => lang('cannot_delete_error_msg')))); exit();
        }

        try {
            $this->subcontractor_model->begin_transaction();

            //prepairing data
            $params = array();
            $params['update_conditions'] = array('id' => $post['id']);
            $params['update_data'] = array('lastup_account_id' => $this->auth->get_account_id());

            //deleting subcontractor
            if ( ! $this->subcontractor_model->delete_subcontractor($params)) {
                throw new Exception(lang('delete_subcontractor_error_msg'));
            }

            //deleting subcontractor relation with id is parent_subcontractor_id
            $params['update_conditions'] = array('parent_subcontractor_id' => $post['id']);
            $params['update_data'] = array(
                    'lastup_account_id' => $this->auth->get_account_id(),
                    'lastup_datetime' => $this->subcontractor_model->_db_now()
            );

            if ( ! $this->subcontractor_relation_model->delete_subcontractor_relation($params)) {
                throw new Exception(lang('delete_subcontractor_relation_error_msg'));
            }
            //deleting subcontractor relation with id is child_subcontractor_id
            $params['update_conditions'] = array('child_subcontractor_id' => $post['id']);

            if ( ! $this->subcontractor_relation_model->delete_subcontractor_relation($params)) {
                throw new Exception(lang('delete_subcontractor_relation_error_msg'));
            }

            $this->subcontractor_model->commit_transaction();
            echo $this->ajax_json(array('result' => true, 'error_msg' => array())); exit();

        } catch (Exception $ex) {
            $this->app_logger->error_log($ex->getMessage());
            $this->_set_error($ex->getMessage());
            $this->subcontractor_model->rollback_transaction();
            echo $this->ajax_json(array('result' => false, 'error_msg' => array('form' => lang('delete_subcontractor_error_msg')))); exit();
        }
    }

    /**
     * Set error message and direct
     *
     * @author hoang_minh
     * @since 2015-05-12
     */
    private function _set_error($error_msg = '') {

        //set error messages
        $this->assign('error_msg', $error_msg);
        $this->view('subcontractor/dialog_forms/error.tpl');
        exit();
    }

    /**
     * Ajax get all subcontractors
     *
     * @param null
     * @return json
     * @author hoang_minh
     * @since 2015-05-15
     */
    public function ajax_get_subcontractors() {
        //load model
        $this->load->model('subcontractor_model');
//#9695:Start
        $this->load->model('account_model');
        $this->load->model('business_unit_model');
//#9695:End

        if ($post = $this->input->post()) {
            //init variables
            $subcontractors = array();
            $param['conditions'] = $post;
            $param['orders'] = array('code' => 'ASC',"id" => "ASC");
//#9695:Start
            $account_business          = $this->account_model->get_account_business_unit($this->auth->get_account_id());
            $business_unit_by_template = $this->business_unit_model->get_all_business_unit_by_template_code($account_business[0]['finish_work_report_template']);

            $param['conditions']['business_unit_id'] = $business_unit_by_template;
//#9695:End
            //get subcontrator
            $subcontractors = $this->subcontractor_model->get_subcontractors_by_params($param);

            foreach ($subcontractors as &$val) {
                $val['name'] = htmlspecialchars($val['name']);
            }

            echo $this->ajax_json(array('result' => true, 'data' => $subcontractors)); exit();
        }

        echo $this->ajax_json(array('result' => false)); exit();
    }

    /**
     * Ajax get all subcontractor relations
     *
     * @param null
     * @return json
     * @author hoang_minh
     * @since 2015-05-19
     */
    public function ajax_get_subcontractor_relations() {
        //load model
        $this->load->model('subcontractor_relation_model');

        if ($post = $this->input->post()) {

            //init variables
            $subcontractors = array();
            $param = array();
            $param['conditions'] = array();
            $param['orders'] = array('subcontractor.create_datetime' => 'ASC');

            if (isset($post['parent_subcontractor_id'])) {
                $param['conditions'] += array('parent_subcontractor_id' => $post['parent_subcontractor_id']);
            }

            if (isset($post['child_subcontractor_id'])) {
                $param['conditions'] += array('child_subcontractor_id' => $post['child_subcontractor_id']);
            }

            if (isset($post['direct_consignment'])) {
                $param['conditions'] += array('direct_consignment' => $post['direct_consignment']);
            }

            if (isset($post['except_id'])) {
                $param['conditions'] += array('except_id' => $post['except_id']);
            }

            //get subcontrator
            $subcontractors = $this->subcontractor_relation_model->get_subcontractors_relations_by_params($param);
            foreach ($subcontractors as &$val) {
                $val['name'] = htmlspecialchars($val['name']);
            }

            echo $this->ajax_json(array('result' => true, 'data' => $subcontractors)); exit();
        }

        echo $this->ajax_json(array('result' => false)); exit();
    }
}