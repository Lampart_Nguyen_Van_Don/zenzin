<?php
class Tmp extends ZR_Controller {
    public function show() {
        $this->view('tmp_grid/show.tpl');
    }

    public function get_all() {
        $this->load->model('tmp_grid_model');
        $tmp_records = $this->tmp_grid_model->get_all();
        die(json_encode($tmp_records));
    }
}