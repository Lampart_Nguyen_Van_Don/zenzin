<?php

$lang['lbl_search_condition'] = 'Search conditions';
$lang['lbl_business_name']    = 'Business name';
$lang['lbl_department_name']  = 'Department name';
$lang['lbl_name']             = 'Name';
$lang['lbl_name_tooltip']     = '(Like)';
$lang['lbl_action']           = 'Action';
$lang['lbl_view']             = 'View';
$lang['btn_add']              = 'Add';
$lang['btn_export_csv']     = 'Export to CSV';
$lang['lbl_login_id']         = 'Login ID';
$lang['lbl_password']         = 'Password';
$lang['lbl_confirm_password'] = 'Confirm password';
$lang['lbl_email']            = 'Email';
$lang['lbl_resign_flag']      = 'Resign flag';
$lang['lbl_resign']           = 'Resign';
$lang['lbl_authority_name']   = 'Authority';

$lang['btn_search'] = 'Search';
$lang['btn_edit']   = 'Edit';
$lang['btn_copy']   = 'Copy';
$lang['btn_close']  = 'Close';
$lang['btn_back']   = 'Back';
$lang['btn_delete'] = 'Delete';
$lang['btn_save']   = 'Save';

$lang['error_please_select_department'] = '※Please select department';
$lang['error_please_select_authority']  = '※Please select authority';
$lang['error_password_alphabet_number'] = '※Must contain alphabets and number';
$lang['error_password_min_length']      = '※At least 8 characters';
$lang['error_password_not_match']       = '※Password dose not match';
$lang['error_password_required']        = '※Password is required';
$lang['error_email_exist']              = '※Email is exist';
$lang['error_email_wrong']              = '※Wrong Email';
$lang['error_email_required']           = '※Email is required';
$lang['error_name_required']            = '※Name is required';
$lang['error_login_id_required']        = '※Login id is required';
$lang['error_login_id_exist']           = '※Login id has been taken';
$lang['lbl_invalid_input']              = '入力した値が無効です';

$lang['lbl_edit_error']   = 'Edit error';
$lang['lbl_edit_success'] = 'Edit success';
$lang['lbl_add_error']    = 'Add error';
$lang['lbl_add_success']  = 'Add success';

$lang['lbl_disable_own_account'] = 'Can not disable your own account';
$lang['lbl_delete_success']      = 'Disable success';
$lang['lbl_delete_error']        = 'Disable error';
$lang['lbl_delete_in_use_error'] = 'Can not delete because account is in use';