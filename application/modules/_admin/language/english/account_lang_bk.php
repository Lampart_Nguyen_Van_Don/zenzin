<?php
//label
$lang['please_choose'] = "Please choose";

//error message
$lang['name_required'] = "Name is required";
$lang['name_must_greater_than_6_characters'] = "Name is minimum 6 characters";
$lang['department_id_invalid'] = "Department is invalid";
$lang['login_id_required'] = "Login id is required";
$lang['login_id_must_greater_than_8_characters'] = "Login id is minimum 8 characters";
$lang['login_id_uniqued'] = "Login id is exists";
$lang['authority_id_invalid'] = "Authority is invalid";
$lang['mail_address_required'] = "Mail address is required";
$lang['mail_address_invalid'] = "Mail address is invalid";
$lang['mail_address_uniqued'] = "Mail address is exists";
$lang['update_success'] = "Update success";
$lang['update_fail'] = "Update fail";
$lang['add_success'] = "Add success";
$lang['add_fail'] = "Add fail";
$lang['delete_fail'] = "Delete fail";
$lang['delete_success'] = "Delete success";
$lang['not_access'] = "You don't have permission to access";


