<?php
$lang['lbl_authority_name'] = "Name";
$lang['lbl_authority_priority'] = "Priority";
$lang['lbl_features']    = "";
$lang['error_null_authority_name']    = "Authority name is required!";

$lang['msg_could_not_delete']    = "This could not be deleted because of account constrains!";
$lang['msg_insert_fail']    = "This could not be inserted! Please check again!";
$lang['msg_update_fail']    = "This could not be updated! Please check again!";

$lang['au_info']    = "Authority information";
$lang['au_add_new']    = "Add new authority";
$lang['au_edit']    = "Edit authority";
$lang['au_features']    = "Authority features";
$lang['au_setting']    = "Authority setting";
$lang['list_actions']    = "List actions";
$lang['no_authority']    = "There is not any authority.";

$lang['list_authorities']    = "List of authorities";
$lang['lbl_action_name']    = "Action";
$lang['lbl_authority_name'] = "Authority name";
$lang['lbl_action_is_display_menu']    = "Display?";
$lang['lbl_action_is_display_menu_value']    = "Yes";
$lang['lbl_authority_name_table'] = "Authority name";
$lang['lbl_action_group']    = "Action group";
$lang['error_authority_exists'] = 'Authority is duplicated.';


