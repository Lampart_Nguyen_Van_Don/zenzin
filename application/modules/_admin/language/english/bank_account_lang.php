<?php
$lang['lbl_bank_account_show']        = 'Bank account detail';
$lang['lbl_bank_account_edit']        = 'Bank account edit';

$lang['lbl_bank_name'] = 'Bank name';
$lang['lbl_branch_name'] = 'Branch name';
$lang['lbl_account_number'] = 'Account number';
$lang['lbl_account_holder'] = 'Account holder';

$lang['error_empty_bank_name'] = 'Bank name is required!';
$lang['error_empty_branch_name'] = 'Branch name is required!';
$lang['error_empty_account_number'] = 'Account number is required!';
$lang['error_empty_account_holder'] = 'Account holder is required!';

$lang['error_numberic_account_number'] = 'Account number must be a number!';
$lang['error_numberic_account_holder'] = 'Account holder must be a number!';

$lang['bank_name_required'] = 'bank name is required';
$lang['branch_name_required'] = 'branch name is required';
$lang['account_number_required'] = 'account number is required';
$lang['account_holder_required'] = 'account holder is required';