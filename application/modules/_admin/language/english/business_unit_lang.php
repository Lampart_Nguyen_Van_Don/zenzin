<?php
//Title
$lang['lbl_business_unit_list']  = "List of business unit";
$lang['lbl_business_unit_info']  = "Business unit information";
$lang['lbl_business_unit_add']   = "Business unit add";
$lang['lbl_business_unit_edit']  = "Business unit edit";

//Confirm
$lang['message_sure_to_delete'] = 'Are you sure to delete?';

//Header content
$lang['lbl_business_unit_name'] = "Name";
$lang['lbl_bu_name'] = "Name business unit";
$lang['lbl_is_product_div'] = "Status product division";
$lang['lbl_product_div'] = "Product division";
$lang['lbl_target_months'] = "Target month";
$lang['lbl_j_code'] = "Business unit code";
$lang['lbl_bugyo_code'] = "Department code";
$lang['lbl_bugyo_account_code'] = "Account code";
$lang['lbl_bugyo_abstract_string'] = "Abstract string";
$lang['lbl_good_mn_code'] = "Product manage code";
$lang['lbl_work_finish_report_template'] = 'Work finish work report template';
$lang['lbl_template'] = "Template";

// Error
$lang['lbl_invalid_month'] = "Invalid month";
$lang['lbl_invalid_input'] = "Invalid input";
$lang['lbl_invalid_id'] = "Invalid id";
$lang['lbl_update_failed'] = "Update failed";
$lang['lbl_delete_failed'] = "Delete failed";
$lang['lbl_delete_exist'] = "Cannot remove. This business unit has been used in department.";
$lang['lbl_insert_failed'] = "Insert failed";

$lang['txt_bu_name_required'] = "business name is required";
$lang['txt_target_months_required'] = "target month is required";
$lang['txt_j_code_required'] = "j code is required";
$lang['txt_bugyo_account_code_required'] = "bugyo account code is required";
$lang['txt_bugyo_abstract_string_required'] = "bugyo abstract string is required";
$lang['txt_bugyo_department_code_required'] = "bugyo department code is required";
$lang['txt_product_mn_code_required'] = "product manage code is required";
$lang['rtb_template_required'] = "template is required";
$lang['work_finish_report_template'] = "work finish report template is required";
$lang['txt_product_mn_code_length'] = "2 character is required";
$lang['txt_bu_name_unique'] = "business name exists";

// Success
$lang['lbl_update_success'] = "Update success";
$lang['lbl_delete_success'] = "Delete success";
$lang['lbl_insert_success'] = "Insert success";

//Action
$lang['lbl_action_name'] = "Action";
$lang['btn_view'] = "View";
$lang['btn_close'] = "Close";
$lang['btn_save']   = "Save";
$lang['btn_edit']   = "Edit";
$lang['btn_edit']   = "Add";
