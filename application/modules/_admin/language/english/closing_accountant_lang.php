<?php
$lang["lbl_year_month"] = "年月";
$lang["lbl_status"] = "ステータス";
$lang["lbl_status_closed"] = "月次決算後";
$lang["lbl_status_not_closed"] = "月次決算前";
$lang["lbl_btn_close"] = "締め";
$lang["lbl_btn_unclose"] = "締め解除";

$lang["msg_not_close_order_data_not_finished"] = "注文が完了していないため、完了できませんでした。";