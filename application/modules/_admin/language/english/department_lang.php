<?php
$lang['error_null_department_name']         	  = 'Name is required!';
$lang['error_null_zipcode']         		  	  = 'Zipcode is required!';
$lang['error_null_address1']         		  	  = 'Address 1 is required!';
$lang['error_null_tel']         		  	      = 'TEL is required!';
$lang['error_null_fax']         		  	      = 'FAX is required!';
$lang['error_null_business_unit_id']         	  = 'Business unit is required!';

$lang["common_lbl_business_unit"] = "Business unit";
$lang["common_lbl_department_name"] = "Name";
$lang["common_lbl_action"] = "Action";
$lang["common_lbl_copy"] = "Copy";
$lang["common_lbl_department_zipcode"] = "Zipcode";
$lang["common_lbl_department_address1"] = "Address 1";
$lang["common_lbl_department_address2"] = "Address 2";

$lang["common_detail_department"] = "Department information";
$lang["common_listof_department"] = "List of department";

$lang["common_edit_department"] = "Edit department";
$lang["common_copy_department"] = "Copy department";
$lang["common_add_department"] = "Add department";

$lang["common_duplicated_records"] = "Duplicated item. Choose another name or business unit";

$lang["alert_enter_a_hyphen"] = "Please enter a hyphen";
$lang["alert_not_enter_a_hyphen"] = "Please do not enter a hyphen";

$lang["error_zipcode_not_contains_hyphen"] = "Zipcode must not contain hyphen";
$lang["error_TEL_contains_hyphen_and_no"] = "TEL must have hyphen and number";
$lang["error_FAX_contains_hyphen_and_no"] = "FAX must have hyphen and number";

$lang["alert_could_not_delete_account_constraint"] = "Could not delete because of account constraint";
$lang["alert_zipcode_7_chars"] = "Zipcode contains 7 characters";

$lang['alert_name_128_chars_max'] = 'The maximum number of characters is 128';
$lang['alert_zipcode_12_chars_max'] = 'The maximum number of characters is 12';
$lang['alert_address1_max'] = 'The maximum number of characters is 255';
$lang['alert_address2_max'] = 'The maximum number of characters is 255';
$lang['alert_phone_no_max'] = 'The maximum number of characters is 16';
$lang['alert_fax_no_max'] = 'The maximum number of characters is 16';
