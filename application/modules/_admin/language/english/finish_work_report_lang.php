<?php
//label
$lang['lbl_form_title'] = 'Finish work report manager';
$lang['lbl_delivery_date'] = 'Delivery date';
$lang['lbl_j_code_branch_code'] = 'J-code + Branch-code';
$lang['lbl_client_code'] = 'Client code';
$lang['lbl_client_name'] = 'Client name';
$lang['lbl_finish_work_status'] = 'Work status';
$lang['lbl_print'] = 'Print';
$lang['lbl_action'] = 'Action';
$lang['lbl_status'] = 'Status';
$lang['lbl_j_code'] = 'J-code';
$lang['lbl_branch_code'] = 'Branch-code';
$lang['lbl_division_name'] = 'Division name';
$lang['lbl_charge_name'] = 'Charge name';
$lang['lbl_fax'] = 'FAX';
$lang['lbl_set_name'] = 'Set name';
$lang['lbl_remark'] = 'Remark';
$lang['lbl_arrange_rep'] = 'Arrange rep';

$lang['lbl_delivery_date_description'] = '(Search from date to date)';
$lang['lbl_j_code_branch_code_description'] = '(Search from prefix match to prefix match)';
$lang['lbl_client_code_description'] = '(Search prefix match)';
$lang['lbl_client_name_description'] = '(Fuzzy search)';

$lang['lbl_download_link'] = '[DL]';
$lang['title_client_detail'] = 'Client detail';

//button
$lang['btn_search'] = 'Search';
$lang['btn_print'] = 'Print';
//7184:Start
$lang['btn_check_all_application_form_print'] = 'Check ALL Application Form Print';
//7184:End
//option
$lang['all'] = 'All';

//error msg
$lang['delivery_date_from_required'] = 'Delivery date from is required';
$lang['delivery_date_from_invalid'] = 'Delivery date from is invalid';
$lang['delivery_date_to_invalid'] = 'Delivery date to is invalid';
$lang['delivery_date_invalid_range'] = 'Delivery date to is invalid';
$lang['delivery_date_invalid_range_from_less_than_to'] = "Search range is incorrect.";
$lang['no_data'] = 'No data';
$lang['row_has_error'] = 'Row %param% has errors';
$lang['report_fax_no_required'] = 'Report fax no is required';
$lang['report_fax_no_invalid'] = 'Report fax no is invalid';
$lang['system_notification'] = 'Sytem noitification';
$lang['choose_least_work_report'] = 'You must choose least one work report';
$lang['nothing_changed'] = 'Nothing changed';
$lang['msg_enter_hyphen_and_num'] = '%field% please enter hyphen and number';

//success msg
$lang['update_fail'] = 'Update order acceptance fail';
$lang['update_success'] = 'Update order acceptance success';
$lang['lbl_no_data_available'] = "No data";

//7910:Start
$lang['lbl_header_media_name_for_dp'] = 'Media name';
$lang['lbl_header_number_of_print_for_dp'] = 'Number of prints';
$lang['lbl_header_delivery_finish_datefor_dp'] = 'Delivery finish date';
//7910:End
