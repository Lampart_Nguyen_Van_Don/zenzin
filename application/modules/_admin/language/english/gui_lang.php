<?php

$lang['lbl_type']     = 'Type';
$lang['lbl_action']   = 'Action';
$lang['lbl_sequence'] = 'Sequence';
$lang['lbl_name']     = 'Name';
$lang['lbl_is_display']  = 'Is display?';
$lang['lbl_code']     = 'Code';
$lang['btn_display']  = 'Display';
$lang['btn_add_new']  = 'Add new';
$lang['btn_ref']      = 'View';

$lang['btn_edit'] = 'Edit';
$lang['btn_close'] = 'Close';

$lang['lbl_name_of_gui_parts']         = 'Name of "gui_parts"' ;
$lang['lbl_code_of_gui_parts_element'] = 'Code of "gui_parts_element"';
$lang['lbl_name_of_gui_parts_element'] = 'Name of "gui_parts_element"';
$lang['lbl_display_or_not']            = 'Display or not?';
$lang['btn_save']                      = 'Save';
$lang['btn_comeback']                  = 'Comeback';
$lang['btn_delete']                    = 'Delete';
$lang['btn_add']                       = 'Add';

$lang['info_display'] = 'No display';

$lang['lbl_edit_error']     = 'Edit error';
$lang['lbl_edit_success']   = 'Edit success';
$lang['lbl_add_error']      = 'Add error';
$lang['lbl_add_success']    = 'Edit success';
$lang['lbl_delete_success'] = 'Delete success';
$lang['lbl_delete_error']   = 'Delete error';
$lang['lbl_delete_using_error']    = "Cannot delete gui element. It was using";

$lang['lbl_invalid_input']         = 'Invalid input';
$lang['error_name_required']       = 'Name is required';
$lang['error_sequence_required']   = 'Sequence is required';
$lang['error_sequence_max_length'] = 'Maximum 5 characters';
$lang['error_sequence_numeric']    = 'Only accept numeric';
$lang['error_sequence_greater_than'] = "Cannot be greater than";