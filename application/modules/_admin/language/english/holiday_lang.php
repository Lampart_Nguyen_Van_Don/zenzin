<?php

$lang['lbl_established_year'] = 'Established year';
$lang['lbl_year']             = 'Year';
$lang['lbl_establish_day']    = 'Day';
$lang['lbl_monday']           = 'Monday';
$lang['lbl_tuesday']          = 'Tuesday';
$lang['lbl_wednesday']        = 'Wednesday';
$lang['lbl_thursday']         = 'Thursday';
$lang['lbl_friday']           = 'Friday';
$lang['lbl_saturday']         = 'Saturday';
$lang['lbl_sunday']           = 'Sunday';
$lang['lbl_holiday']          = 'Holiday';

$lang['btn_establish_day'] = 'Establish';
$lang['btn_search']        = 'Search';

$lang['error_no_filter_checked']   = 'You must check at least one';
$lang['error_can_not_change']      = 'The date that you selected can not be change.';
$lang['error_can_not_change_current_date'] = 'Cannot change current date.';
$lang['confirm_overwrite_holiday'] = 'All current holiday will be override. Are you want to sure to reset the hohiday?';
$lang['confirm_set_holiday']       = 'Do you want to set this date is holiday?';
$lang['confirm_reject_holiday']    = 'Do you want to reject this holiday?';


$lang['lbl_insert_success'] = 'Success';
$lang['lbl_insert_error']   = 'Error';

$lang['lbl_invalid_input'] = 'Invalid input data';

$lang['warning_change_holiday'] = 'This will reset all stablished day';
