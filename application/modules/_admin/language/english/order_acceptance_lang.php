<?php

$lang['lbl_input_file'] = 'Input file';

$lang['btn_download_template'] = 'Download template';
$lang['btn_browse']            = 'Browse';
$lang['btn_upload']            = 'Upload';
$lang['lbl_loading']           = 'Loading';
$lang['lbl_complete']          = 'Completed';

$lang['lbl_acceptance_condition']      = 'Conditions';
$lang['lbl_approve_status']            = 'Approve status';
$lang['lbl_jcode_branch_cd']           = 'j_code + branch';
$lang['lbl_jcode_branch_tooltip']      = 'From j_code + branch To j_code + branch';
$lang['lbl_account_id']                = 'account id';
$lang['lbl_business_unit_name']        = 'business unit name';
$lang['lbl_arrange_rep']               = 'arrange rep';
$lang['lbl_hidden_search']             = 'search nearly';
$lang['lbl_client_name']               = 'client name';
$lang['lbl_source_subcontractor_id']   = 'source subcontractor id';
$lang['lbl_work_subcontractor_id']     = 'work subcontractor id';
$lang['lbl_shipping_subcontractor_id'] = 'shipping subcontractor id';
$lang['lbl_shipping_type_cd']          = 'shipping type cd';
$lang['lbl_delivery_date']             = 'delivery date';
$lang['lbl_delivery_date_tooltip']     = 'From delivery date To delivery date';
$lang['lbl_summary_month']             = 'month summary';
$lang['lbl_summary_month_tooltip']     = 'From summary month To summary month';

$lang['btn_export_csv']             = 'export csv';
$lang['btn_approve_all_acceptance'] = 'approve all acceptance';
$lang['btn_cancel_all_acceptance']  = 'cancel all acceptance';

$lang['lbl_approve_all']             = 'approve all';
$lang['lbl_action']                  = 'action';
$lang['btn_separate']                = 'separate';
$lang['lbl_edit']                    = 'edit';
$lang['lbl_order_acceptance_status'] = 'order acceptance status';
$lang['lbl_diffence_common']         = 'diffence common';
$lang['lbl_jcode']                   = 'j_code';
$lang['lbl_branch']                  = 'branch_cd';
$lang['lbl_set_name']                = 'set name';
$lang['lbl_unit_price']              = 'unit price';
$lang['lbl_quantity']                = 'quantity';
$lang['lbl_fee']                     = 'fee';
$lang['lbl_weight']                  = 'weight';
$lang['lbl_shipment_shape_cd']       = 'shipment shape cd';
$lang['lbl_free_tax']                = 'tax free';
$lang['lbl_shipping_charge_tax']     = 'shipping charge tax';
$lang['lbl_stamp_tax']               = 'stamp tax';
$lang['lbl_etc_tax']                 = 'etc tax';
$lang['lbl_shipping_charge']         = 'shipping charge';
$lang['lbl_etc']                     = 'etc';
$lang['lbl_total_amount']            = 'total amount';

$lang['checkbox_unapproved']      = 'unapproved';
$lang['checkbox_approved']        = 'approved';
$lang['checkbox_account_tighten'] = 'monthly closing';

$lang['lbl_edit_error']     = 'Update error';
$lang['lbl_edit_success']   = 'Update success';

$lang['lbl_confirm_delete'] = 'Are you sure to delete this row ?';

$lang['btn-check-all-clickk']       =  " Check all ";

    /////////////////////////
   //                     //
  // IMPORT EXCEL ERROR  //
 //                     //
/////////////////////////
$lang['lbl_please_select_file'] = 'Please select file';
$lang['lbl_unsupported_file']   = 'Unsupported file';
$lang['lbl_empty_file']         = "Empty file";
$lang['lbl_row']                = 'Row: ';

$lang['error_jcode_branch_is_not_exist']           = 'jcode branch is not exist';
$lang['error_branch_only_two_characters']          = 'branch only contain 2 letters';
$lang['error_source_subcontractor_can_not_use']    = 'source subcontractor is not exist';
$lang['error_work_subcontractor_can_not_use']      = 'work subcontractor is not exist';
$lang['error_shipping_subcontractor_can_not_use']  = 'shipping subcontractor is not exist';
$lang['error_shipping_type_can_not_use']           = 'shipping type subcontractor is not exist';
$lang['error_unit_price_must_be_number']           = 'unit price must be number';
$lang['error_unit_price_can_not_be_negative']      = 'unit price can not be negative';
$lang['error_quantity_must_be_number']             = 'quantity must be number';
$lang['error_quantity_can_not_be_negative']        = 'quantity can not be negative';
$lang['error_delivery_date_invalid']               = 'delivery date is invalid';
$lang['error_fee_must_be_number']                  = 'fee must be number';
$lang['error_fee_can_not_be_negative']             = 'fee can not be negative';
$lang['error_fee_max_length']                 	   = 'fee only contain 9 numbers';
$lang['error_weight_must_be_number']               = 'weight must be number';
$lang['error_weight_can_not_be_negative']          = 'weight can not be negative';
$lang['error_weight_max_length']              	   = 'weight can not more then 4 numbers';
$lang['error_summary_month_has_not_close']         = 'summary month has not close';

$lang['error_shipments_shape_id_is_not_exist']    = 'shippment shape id is not exist';
$lang['error_account_id_is_not_exist']            = 'account id is not exist';
$lang['error_arrange_rep_is_not_exist']           = 'arrange rep is not exist';
$lang['error_tax_free_must_be_number']            = 'tax free must be number';
$lang['error_tax_free_max_length']				  = 'tax free only contain 9 numbers';
$lang['error_stamp_tax_max_length']				  = 'stamp tax only contain 9 numbers';

$lang['error_shipping_charge_tax_must_be_number'] = 'shipping charge tax must be number';
$lang['error_shipping_charge_tax_max_length'] 	  = 'shipping charge tax only contain 9 number';
$lang['error_shipping_charge_must_be_number']     = 'shipping charge must be number';

$lang['error_shipping_charge_max_length']         = 'shipping charge only contain 9 numbers';

$lang['error_summary_month_insert_is_required']   = "Summary month insert is required";
$lang['error_shipping_stamp_tax_must_be_number']  = 'stamp tax must be number';
$lang['error_etc_tax_must_be_number']             = 'etc tax must be number';
$lang['error_etc_must_be_number']                 = 'etc must be number';
$lang['error_etc_tax_max_length']				  = 'etc tax max only contain 9 numbers';
$lang['error_etc_max_length']                     = 'etc only contain 9 numbers';

$lang['lbl_import_error']      = 'Import error';
$lang['lbl_import_success']    = 'Import success';

$lang['export_csv_file_name'] = "発注・検収_" . date('Ymd') .'.csv';


    ////////////////////////////
   //                        //
  // INPUT ORDER ACCEPTANCE //
 //                        //
////////////////////////////

$lang['error_j_code_max_length']                     = 'j code only can contain maximum 10 letters';
$lang['error_branch_cd_max_length']                  = 'branch cd only can contain maximum 2 letters';
$lang['error_account_id_is_required']                = 'account id is required';
$lang['error_arrange_rep_is_required']               = 'arrange rep is required';
$lang['error_client_name_is_required']               = 'client name is required';
$lang['error_source_subcontractor_id_is_required']   = 'source subcontractor id is required';
$lang['error_work_subcontractor_id_is_required']     = 'work subcontractor id is required';
$lang['error_shipping_subcontractor_id_is_required'] = 'shipping subcontractor is required';
$lang['error_shipping_type_cd_is_required']          = 'shipping type cd is required';
$lang['error_set_name_is_required']                  = 'set name is required';
$lang['error_unit_price_is_required']                = 'unit price is required';
$lang['error_quantity_max_length']                   = 'unit price only contain maximum 9 numbers。';
$lang['error_quantity_is_required']                  = 'quantity is required';
//#8258:Start
$lang['error_quantity_max_length']                   = 'quantiy only can contain maximum 7 letters';
//#8258:End
$lang['error_delivery_date_is_required']             = 'delivery date is required';
$lang['error_fee_is_required']                       = 'fee is required';
$lang['error_weight_is_required']                    = 'weight is required';
$lang['error_shipments_shape_cd_is_required']        = 'shipments shape cd is required';
$lang['error_delivery_date_form_is_required']        = 'form delivery date is required';
$lang['error_summary_month_form_is_required']        = 'form summary month date is required';
$lang['error_total_amount_max_length']               = 'amount tax cannnot greater 10 numbers';

$lang['lbl_no_data_available'] = "No data available";

$lang['lbl_confirm_close_windows'] = "If you close or reload windows, you may lose your recently changed data";
$lang['error_nothing_changed'] = 'Nothing changed';
//#9695:Start
$lang['error_account_business']                   = 'ERROR BUSINESS UNIT';
$lang['error_subcontractor_business']             = 'ERROR SUBCONTRACTOR BUSINESS UNIT';
//#9695:End
//#9748: Start
$lang['lbl_delivery_finish_date'] = "Delivery Finish Date";
$lang['lbl_product_name']         = 'Product Name';
$lang['lbl_content_order'] ='Content';
$lang['lbl_unit_price_in_order'] ='Unit Price';
$lang['lbl_quanity_in_order'] ='Quanity';
$lang['lbl_total_sales_in_order'] = 'Total sales in Order';
$lang['lbl_total_cost_in_order_excluding_tax'] = 'Total cost（Excluding Tax）';
$lang['lbl_fixed_cost_excluding_tax'] = "Fixed cost（Excluding Tax）";

$lang['error_fixed_cost_excluding_tax'] = "Fixed cost（Excluding Tax） is reqired";
$lang['error_delivery_finish_date_required'] = "Delivery Finish Date is required";
//#9748: End
