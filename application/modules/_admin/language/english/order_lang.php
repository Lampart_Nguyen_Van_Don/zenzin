<?php

//form title
$lang['approve_reject_form_title'] = 'Order Approve/Reject';
$lang['title_client_detail'] = 'Client detail';

//label
$lang['lbl_s_business_unit'] = 's business_unit';
$lang['lbl_j_code'] = 'J-code';
$lang['lbl_s_code'] = 'S-code';
$lang['lbl_name'] = 'Client name';
$lang['lbl_client_name'] = 'Client company name';
$lang['lbl_client_name_furigana'] = 'Client name furigana';
$lang['lbl_client_department_name'] = 'Client department name';
$lang['lbl_name_kana'] = 'Client name (kana)';
$lang['lbl_zipcode'] = 'Zipcode';
$lang['lbl_address'] = 'Address';
$lang['lbl_phone_fax'] = 'TEL / FAX';
$lang['lbl_phone'] = 'Phone';
$lang['lbl_fax'] = 'FAX';
$lang['lbl_devision_name'] = 'Devision name';
$lang['lbl_charge_name'] = 'Charge name';
$lang['lbl_payment_term'] = 'Payment term';
$lang['lbl_account_id'] = 'Account';
$lang['lbl_cost_total_price'] = 'Cost total price';
$lang['lbl_consumption_tax'] = 'Consumption tax';
$lang['lbl_tax_total'] = 'Total Tax';
$lang['lbl_gross_profit_amount'] = 'Gross profit amount';
$lang['lbl_remark'] = 'Remark';
$lang['lbl_comment'] = 'Order approval/reject comment';
$lang['lbl_comment_change_report'] = 'Change report comment';
$lang['lbl_branch_cd'] = 'Branch cd';
$lang['lbl_order_status'] = 'Order status';
$lang['lbl_status_detail'] = 'Status detail';
$lang['lbl_product_name'] = 'Product name';
$lang['lbl_content'] = 'Contents';
$lang['lbl_delivery_date'] = 'Delivery date';
$lang['lbl_billing_date'] = 'Billing date';
$lang['lbl_payment_date'] = 'Payment date';
$lang['lbl_quantity'] = 'Quantity';
$lang['lbl_tax_type'] = 'Tax type';
$lang['lbl_unit_price'] = 'Unit price';
$lang['lbl_amount'] = 'Amount';
$lang['lbl_order_form_input_date'] = 'Order form input date';
$lang['lbl_order_regist_date'] = 'Order regist date';
$lang['lbl_order_detail_cost_unit_price'] = 'Cost unit price';
$lang['lbl_order_detail_cost_total_price'] = 'Cost total price';
$lang['lbl_gross_profit_amount'] = 'Gross profit amount';
$lang['lbl_order_detail_gross_profit_rate'] = 'Gross profit rate';
$lang['lbl_change_report'] = 'Change report';
$lang['lbl_is_change_report_apply'] = 'Is change report apply';
$lang['lbl_detail_contents'] = 'Detail content';
$lang['lbl_comment_report'] = 'Change report comment';
$lang['lbl_comment_report_approve_reject'] = 'Approval/rejection comment';
$lang['lbl_is_ad_receive_payment_sales_input'] = 'Is ad receive payment sales input';
$lang['lbl_is_ad_receive_payment'] = 'Is ad receive payment';
$lang['lbl_is_order_input'] = 'Is order input';
$lang['lbl_is_acceptance_input'] = 'Is acceptance input';
$lang['lbl_is_sales_slip_input'] = 'Is sales slip input';
$lang['lbl_is_invoice_issue'] = 'Is invoice issue';
$lang['lbl_is_ad_sales_slip_input'] = 'Is admin sales slip input';
$lang['lbl_is_ad_invoice_issue'] = 'Is admin invoice issue';
$lang['lbl_applier_info'] = 'Info applier';
$lang['lbl_closing_date'] = 'Closing date';
$lang['lbl_payment_month_cd'] = 'Payment month cd';
$lang['lbl_payment_day_cd'] = 'Payment day cd';
$lang['lbl_credit_infor'] = 'Credit information';
$lang['lbl_credit_infor_ad_receive'] = 'Credit information payment received';
$lang['lbl_credit_limit'] = 'Credit limits(Credit limits - account payment)';
$lang['lbl_receive_payment'] = 'Receive payment';
$lang['lbl_j_code_branch_code'] = 'J code + branch code';
$lang['lbl_order_date_approve'] = 'Order date approve';
$lang['lbl_j_business_unit'] = 'j business unit';
$lang['lbl_approve_report'] = 'Approve report';
$lang['lbl_reject_report'] = 'Reject report';
$lang['lbl_is_ad_sale_slip_input'] = 'Is ad sale slip input';

// Add form
$lang['lbl_search_client'] = 'Search client';

// Order status
$lang['lbl_is_ad_sale_slip_input'] = 'is ad sale slip';
$lang['lbl_is_ad_invoice_issue'] = 'is ad invoice issue';
$lang['lbl_is_order'] = 'is order';
$lang['lbl_is_acceptance'] = 'is acceptance';
$lang['lbl_is_sales_slip'] = 'is sales slip';
$lang['lbl_is_invoice_issue'] = 'is invoice issue';


//button
$lang['btn_approve'] = 'Approve';
$lang['btn_reject'] = 'Reject';
$lang['lbl_action'] = 'Action';
$lang['lbl_change_report'] = 'Change report';
$lang['lbl_copy_branch'] = 'Copy';
$lang['lbl_copy_order'] = 'Copy order';
$lang['lbl_order_approve_reject'] = 'Order approve/reject';
$lang['lbl_order_change_report_approve_reject'] = 'Change report approve/reject';
$lang['lbl_delete_branch'] = 'Delete';
$lang['lbl_add_branch'] = 'Add';
$lang['lbl_add_order'] = 'Add new order';
$lang['lbl_add_order_save'] = 'Save order';
$lang['lbl_approve_order'] = 'Approve order';
$lang['lbl_print'] = 'Print';
$lang['lbl_csv_download'] = 'CSV download';
$lang['lbl_save'] = 'Save';
$lang['lbl_edit'] = 'Edit';
$lang['lbl_search'] = 'Search';
$lang['lbl_show'] = 'Show';
$lang['lbl_close'] = 'Close';
$lang['lbl_check_approve_more'] = 'Approve';
$lang['lbl_delete_order'] = 'Delete';
$lang['lbl_cancel_branch'] = 'Cancel branch';
$lang['lbl_cancel_branch_release'] = 'Cancel release';
$lang['lbl_regist_cancel'] = 'Registration cancel';
$lang['lbl_filter'] = 'Search';
$lang['lbl_client_change'] = 'Change client';

$lang['msg_enter_hyphen_and_num'] = '%field% please enter hyphen and number';

//error message
$lang['update_order_fail'] = 'Update order fail';
$lang['update_order_detail_fail'] = 'Update order detail fail';
$lang['update_client_fail'] = 'Update client fail';
$lang['send_mail_approve_order_fail'] = 'Send mail approve order fail';
$lang['send_mail_change_report_fail'] = 'Send mail change report fail';
$lang['approve_order_fail'] = 'Approve order fail';
$lang['approve_order_detail_fail'] = 'Update order detail fail';
$lang['approve_order_success'] = 'Approve order success';
$lang['edit_nothing_change'] = 'Not changed';
$lang['change_report_nothing_change'] = 'Not changed';

$lang['lbl_hide_search'] = 'Hide search';
$lang['lbl_open_search'] = 'Open search';