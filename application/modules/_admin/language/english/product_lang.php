<?php

$lang['code']= 'Code';
$lang['business_unit']= 'Business unit';
$lang['name_product']= 'Name of product';
$lang['is_no_add']= 'Is tax or not';

$lang['message_sure_to_delete'] = 'Are you sure to delete?';;
$lang['error_code_null']= 'Code is required!';
$lang['error_code_max_length']= 'The code can not exceed 5 characters in length!';
$lang['error_code_unique']= 'Code is unique!';
$lang['error_business_unit_null']= 'Business unit is required!';
$lang['error_business_unique']= 'Business unit is unique!';
$lang['error_name_product_null']= 'The name of product is required!';
$lang['error_name_product_limit_character']= 'The product length name can not greater 128 characters!';
$lang['error_is_no_add_null']= 'Is tax is required!';
$lang['error_order_product_exists'] = 'Order product has been exists!';
$lang['error_null_product_manage_code'] = 'This business unit <<>> has null product manage code!';

$lang['edit_product']= 'Edit product';
$lang['is_no_add']= 'Is add?';
$lang['is_no_add_in_edit']= 'Is add?';
$lang['product_information']= 'Product information';
$lang['add_new_product']= 'Add new product';
$lang['list_of_product']= 'List of product';

$lang['is_no_add_0']= 'Yes';
$lang['is_no_add_1']= 'No';
$lang['is_no_add_flag']= 'Is disabled adding?';