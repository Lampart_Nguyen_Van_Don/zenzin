<?php
$lang['msg_import_successfully']     = 'Import successfully!';
$lang['msg_import_fail']     = 'Import fail! Please try again later!';
$lang['msg_only_xls_xlsx']     = 'Only file xls or xlsx can be uploaded!';

$lang['lbl_check']     = 'File check';
$lang['lbl_datetime']     = 'Date';
$lang['lbl_source_acc']     = 'Account source';
$lang['lbl_amount']     = 'Amount';
$lang['lbl_s_code']     = 'S_Code';
$lang['lbl_client_name']     = 'Client name';
$lang['lbl_action']     = 'Action';
$lang['lbl_import_ng']     = 'Client NG';
$lang['lbl_confirm_client']     = 'Need confirm';

$lang['lbl_file_import']     = 'File import';
$lang['lbl_import_and_process']     = 'Import and process';

$lang['btn_import']     = 'import';
$lang['btn_cancel']     = 'cancel';

$lang['lbl_invalid_date'] = "Invalid datetime";
$lang['lbl_invalid_client'] = "Invalid client";
$lang['lbl_invalid_amount'] = "Invalid amount";
$lang['lbl_amount_less_than_zero'] = "Amount less than zero";

$lang['lbl_input_file'] = 'Input file';

$lang['btn_download_template'] = 'Download template';
$lang['btn_browse']            = 'Browse';
$lang['btn_upload']            = 'Upload';
$lang['lbl_loading']           = 'Loading';
$lang['lbl_complete']          = 'Completed';

