<?php

$lang['lbl_search'] = "Search";
$lang['lbl_search_from_to'] = "Date";
$lang['lbl_search_division'] = "Business name";

$lang['lbl_search_from_to_description'] = "対象年月";
$lang['lbl_print_button_description'] = "前渡金繰越計算表一括印刷";

$lang['name'] = "Name";
$lang['department'] = "Department name";
$lang['target'] = "Target amount";

$lang['error_from_date_invalid'] = "Date field is invalid date";

$lang['target'] = '営業目標設定';
$lang['report'] = '営業月報';

