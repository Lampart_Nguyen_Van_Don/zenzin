<?php

$lang['code']= 'Code';
$lang['business_unit']= 'Business unit';
$lang['name_product']= 'Name product';
$lang['is_no_add']= 'Is tax or not';

$lang['message_sure_to_delete'] = 'Are you sure to delete?';;
$lang['error_code_null']= 'Code is required!';
$lang['error_code_max_length']= 'The Code can not exceed 5 characters in length!';
$lang['error_code_unique']= 'Code is unique!';
$lang['error_business_unit_null']= 'Business unit is required!';
$lang['error_business_unique']= 'Business unit is unique!';
$lang['error_name_product_null']= 'The Name product is required!';
$lang['error_is_no_add_null']= 'Is tax is required!';

// Department
$lang['error_null_department_name']         	  = 'Name is required!';
$lang['error_null_zipcode']         		  	  = 'Zipcode is required!';
$lang['error_null_address1']         		  	  = 'Address 1 is required!';
$lang['error_null_tel']         		  	      = 'TEL is required!';
$lang['error_null_fax']         		  	      = 'FAX is required!';

$lang["common_lbl_business_unit"] = "Business unit";
$lang["common_lbl_department_name"] = "Name";
$lang["common_lbl_action"] = "Action";
$lang["common_lbl_copy"] = "Copy";
$lang["common_lbl_department_zipcode"] = "Zipcode";
$lang["common_lbl_department_address1"] = "Address 1";
$lang["common_lbl_department_address2"] = "Address 2";

$lang["common_detail_department"] = "Department information";
$lang["common_listof_department"] = "List of department";

$lang["common_edit_department"] = "Edit department";
$lang["common_copy_department"] = "Copy department";
$lang["common_add_department"] = "Add department";