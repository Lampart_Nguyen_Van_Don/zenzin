<?php

$lang['lbl_business']        = 'Business name';
$lang['lbl_action']          = 'Action';
$lang['lbl_sequence']        = 'Sequence';
$lang['lbl_code']            = 'Code';
$lang['lbl_name']            = 'Name';
$lang['lbl_is_remaining_confirm'] = 'Remaining confirm';
$lang['lbl_is_advance_payment']     = 'Advance payment';
$lang['lbl_reporting']       = 'Reporting';
$lang['lbl_is_display']      = 'Is display';
$lang['btn_add']             = 'Add new';
$lang['lbl_view']            = 'View';

$lang['lbl_is_remaining_confirm_flag'] = 'Remaining confirm flag';
$lang['lbl_is_advance_payment_flag']   = 'Advance payment flag';
$lang['lbl_reporting_flag']            = 'Reporting flag';
$lang['lbl_is_display_flag']           = 'Is display flag';
$lang['btn_edit']                      = 'Edit';
$lang['btn_close']                     = '閉じる';
$lang['lbl_is_no_add']                = 'No display';

$lang['lbl_edit_error']     = 'Edit error';
$lang['lbl_edit_success']   = 'Edit success';
$lang['lbl_add_error']      = 'Add error';
$lang['lbl_add_success']    = 'Edit success';
$lang['lbl_delete_success'] = 'Delete successful';
$lang['lbl_delete_error']   = 'Delete error';

$lang['btn_save']   = 'Save';
$lang['btn_back']   = 'Comeback';
$lang['btn_delete'] = 'Delete';

$lang['error_code_required']       = 'Code is required';
$lang['error_name_required']       = 'Name is required';
$lang['error_name_max_length']     = 'Maximum 128 characters';
$lang['error_sequence_required']   = 'Sequence is required';
$lang['error_sequence_max_length'] = 'Maximum 5 characters';
$lang['error_sequence_numeric']    = 'Only accept numeric';

$lang['lbl_invalid_input'] = 'Invalid input';
$lang['lbl_invalid_input_minus'] = "Invalid input";

$lang['lbl_delete_in_use_error'] = 'In used in order acceptance';