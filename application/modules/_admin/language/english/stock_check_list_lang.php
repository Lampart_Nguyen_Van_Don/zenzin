<?php

//label
$lang['lbl_delivery_date'] = 'Delivery date';
$lang['lbl_delivery_date_description'] = 'Input range of delivery date';

//button
$lang['lbl_print'] = 'Print';

//error msg
$lang['delivery_date_from_required'] = 'Delivery date from is required';
$lang['delivery_date_to_required'] = 'Delivery date to is required';
$lang['delivery_date_from_invalid'] = 'Delivery date from is invalid';
$lang['delivery_date_to_invalid'] = 'Delivery date to is invalid';
$lang['delivery_date_range_invalid'] = 'Delivery date range is invalid';
$lang['no_data_found'] = 'No data found';
