<?php

//label
$lang['lbl_form_title']         = 'Subcontractor manager';
$lang['lbl_detail_form_title']  = 'Subcontractor informations';
$lang['lbl_add_form_title']     = 'Add Subcontractor';
$lang['lbl_edit_form_title']     = 'Edit Subcontractor';
$lang['lbl_code']               = 'Code';
$lang['lbl_name']               = 'Name';
$lang['lbl_abbr_name']          = 'Abbr Name';
$lang['lbl_subcontractor_type'] = 'Subcontractor type';
$lang['lbl_direct_consignment'] = 'Direct consignment';
$lang['lbl_is_no_add']         = 'Is no add';
$lang['lbl_is_no_add_checkbox'] = 'No add';
$lang['lbl_action']             = 'Action';
$lang['lnk_edit']               = 'Edit';
$lang['lbl_parent_subcontractor_relation']  = 'Parent subcontractor relations';
$lang['lbl_parent_subcontractor']     = 'Parent subcontractors';
$lang['lbl_choosen_parent_subcontractor']  = 'Choosen subcontractors';
$lang['lbl_child_subcontractor_relation']  = 'Child subcontractor relations';
$lang['lbl_child_subcontractor']     = 'Child subcontractors';
$lang['lbl_choosen_child_subcontractor']  = 'Choosen subcontractors';
$lang['code'] = 'Code';

//options
$lang['default_consignment'] = 'All';
$lang['direct_consignment'] = 'Direct';
$lang['indirect_consignment'] = 'Indirect';
$lang['is_no_add'] = 'Is no add';
$lang['is_no_add_symbol'] = 'yes';
$lang['is_add_symbol'] = 'no';
//button
$lang['btn_view']   = 'View';
$lang['btn_add']    = 'Add';
$lang['btn_search'] = 'Search';
$lang['btn_close']  = 'Close';
$lang['btn_delete']  = 'Delete';
$lang['btn_save']  = 'Save';

//description
$lang['description_for_code'] = '(Subcontractor code)';
$lang['description_for_name'] = '(Subcontractor name)';
$lang['description_for_abbr_name'] = '(Subcontractor abbr name)';

//validation
$lang['require'] = '%label% is required';
$lang['min_character'] = '%label% is minium %min% characters';
$lang['max_character'] = '%label% is maximum %max% characters';
$lang['min_max_character'] = '%label% is minium %min% and maximum %max% characters';
$lang['[0-9A-Z]'] = '%label% only have numbers [0-9] and uppercase [A-Z] characters';
$lang['min_max_[0-9A-Z]'] = '%label% is minium %min% and maximum %max% characters and only have numbers 0-9 and uppercase A-Z characters';
$lang['unique'] = '%label% is uniqued';

//message
$lang['error_title'] = 'Error';
$lang['no_data'] = 'No data';
$lang['subcontractor_not_exist_error_msg'] = 'The subcontractor is not exists.';
$lang['system_error_msg'] = 'System error, please try again';
$lang['add_error_msg'] = 'Add subcontractor fail';
$lang['add_success_msg'] = 'Add subcontractor success';
$lang['update_error_msg'] = 'Update subcontractor fail';
$lang['update_success_msg'] = 'Update subcontractor success';
$lang['add_subcontractor_relation_error_msg'] = 'Add subcontractor relations fail';
$lang['update_subcontractor_relation_error_msg'] = 'Update subcontractor relations fail';
$lang['delete_subcontractor_error_msg'] = 'Delete subcontractor fail';
$lang['delete_subcontractor_relation_error_msg'] = 'Delete subcontractor relations fail';
$lang['cannot_delete_error_msg'] = "Cannot delete subcontractor, it's using";
$lang['error_attr_name_max_length'] = " ";
$lang['cannot_delete_parent_error_msg'] = "Cannot delete subcontractor, it's have chlid subcontractors";
$lang['parent_subcontractor_is_required'] = "Parent subcontractor is required";