<?php

$lang['lbl_search_condition'] = '検索条件';
$lang['lbl_business_name']    = '事業部名';
$lang['lbl_department_name']  = '部署名';
$lang['lbl_name']             = '氏名';
$lang['lbl_name_tooltip']     = '(あいまい検索)';
$lang['lbl_action']           = '処理';
$lang['lbl_view']             = '参照';
$lang['btn_add']              = '追加';
$lang['btn_export_csv']       = 'CSVダウンロード';
$lang['lbl_login_id']         = 'ログインID';
$lang['lbl_password']         = 'パスワード';
$lang['lbl_confirm_password'] = 'パスワード確認';
$lang['lbl_email']            = 'メールアドレス';
$lang['lbl_resign_flag']      = '退職フラグ';
$lang['lbl_resign']           = '退職';
$lang['lbl_authority_name']   = '権限名';

$lang['btn_search'] = '検索';
$lang['btn_edit']   = '編集';
$lang['btn_copy']   = 'アカウントコピー作成';
$lang['btn_close']  = '閉じる';
$lang['btn_back']   = '一覧へ戻る';
$lang['btn_delete'] = '削除';
$lang['btn_save']   = '保存';

$lang['error_please_select_department'] = '部署名を選択して下さい';
$lang['error_please_select_authority']  = '権限名を選択して下さい。';
$lang['error_password_alphabet_number'] = '大文字、小文字、数字をすべて使用して設定する必要があります。';
$lang['error_password_min_length']      = 'パスワードは8文字以上で設定して下さい。';
$lang['error_password_not_match']       = '入力されたパスワードが一致していません。もう1度入力して下さい。';
$lang['error_password_required']        = 'パスワードを入力して下さい。';
$lang['error_email_exist']              = '※メールアドレスは既に存在しています。';
$lang['error_email_wrong']              = 'メールアドレスの形式になっていません。<p>半角英数字であること、ドメインが正しい事をご確認ください。</p>';
$lang['error_email_required']           = 'メールアドレスを入力して下さい。';
$lang['error_name_required']            = '氏名を入力して下さい。';
$lang['error_login_id_required']        = 'ログインIDを入力して下さい。';
$lang['error_login_id_exist']           = '※ログインIDは既に存在しています。';
$lang['lbl_invalid_input']              = '※入力した値が無効です';

$lang['lbl_edit_error']   = 'データ更新に失敗しました。';
$lang['lbl_edit_success'] = '更新しました';
$lang['lbl_add_error']    = 'データ追加に失敗しました。';
$lang['lbl_add_success']  = '追加しました';

$lang['lbl_disable_own_account'] = 'アカウント削除できません。';
$lang['lbl_delete_success']      = 'アカウント削除できました。';
$lang['lbl_delete_error']        = 'アカウント削除失敗しました。';
$lang['lbl_delete_in_use_error'] = 'このアカウントは現在使用されています。';
