<?php
$lang['lbl_search_from_to'] = "発送年月";
$lang['lbl_search_subcontract_code'] = "ブレーンコード";
$lang['lbl_search_subcontract_name'] = "ブレーン名";
$lang['lbl_search_subcontract_abbr_name'] = " ブレーン略称";

$lang['lbl_search_from_to_description'] = "(From必須　Fromのみ指定の場合、指定年月以降を検索　From To指定で範囲指定検索）";
$lang['lbl_search_subcontract_code_description'] = "(前方一致検索）";
$lang['lbl_search_subcontract_name_description'] = "(あいまい検索）";
$lang['lbl_search_subcontract_abbr_name_description'] = "(あいまい検索）";
$lang['lbl_print_button_description'] = "前渡金繰越計算表一括印刷";
