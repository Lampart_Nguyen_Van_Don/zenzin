<?php
$lang['err_equal_old_password'] = "現在のパスワードと一つ前に使用していたパスワードは設定できません。";
$lang['err_can_not_change_password'] = 'There was an error, the password can not change!';
$lang['lbl_new_password'] = '新パスワード';
$lang['lbl_confirm_password'] = 'パスワード確認';
$lang['lbl_id'] = 'ログインID';
$lang['lbl_password'] = 'パスワード';
$lang['lbl_change_password'] = 'パスワード変更';
$lang['lbl_login'] = 'ログイン';