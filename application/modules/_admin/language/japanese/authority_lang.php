<?php
$lang['lbl_authority_name'] = "名称";
$lang['lbl_authority_priority'] = "優先";
$lang['lbl_features']    = "機能一覧";
$lang['error_null_authority_name']    = "名称管理は必須項目です。";

$lang['msg_could_not_delete']    = "この権限は現在使用されています。";
$lang['msg_insert_fail']    = "データ追加に失敗しました。";
$lang['msg_update_fail']    = "データ更新に失敗しました。";

$lang['au_info']    = "権限情報";
$lang['au_add_new']    = "新規権限追加";
$lang['au_edit']    = "権限編集";
$lang['au_features']    = "権限編集";
$lang['au_setting']    = "権限設定";
$lang['list_actions']    = "アクション一覧";
$lang['no_authority']    = "権限がありません。";

$lang['list_authorities']    = "権限一覧";
$lang['lbl_action_name']    = "アクション";
//#7568:Start
$lang['lbl_action_group_name']= " アクション グループ";
//#7568:End
$lang['lbl_authority_name'] = "権限名";
$lang['lbl_action_is_display_menu']    = "メニュー表示";
$lang['lbl_action_is_display_menu_value']    = "表示";
$lang['lbl_authority_name_table'] = "権限名";
$lang['lbl_action_group']    = "アクショングループ";
$lang['error_authority_exists'] = '入力された権限名称は既に使用されています。';
