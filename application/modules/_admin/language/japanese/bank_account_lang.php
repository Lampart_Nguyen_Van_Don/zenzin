<?php
$lang['lbl_bank_account_show']        = '銀行口座詳細';
$lang['lbl_bank_account_edit']        = '振込口座編集';

$lang['lbl_bank_name'] = '金融機関名';
$lang['lbl_branch_name'] = '支店名';
$lang['lbl_account_number'] = '口座番号';
$lang['lbl_account_holder'] = '口座名義';

$lang['error_empty_bank_name'] = 'Bank name is required!';
$lang['error_empty_branch_name'] = 'Branch name is required!';
$lang['error_empty_account_number'] = 'Account number is required!';
$lang['error_empty_account_holder'] = 'Account holder is required!';

$lang['error_numberic_account_number'] = 'Account number must be a number!';
$lang['error_numberic_account_holder'] = 'Account holder must be a number!';

$lang['bank_name_required'] = '金融機関名を入力して下さい。';
$lang['branch_name_required'] = '支店名を入力して下さい。';
$lang['account_number_required'] = '口座番号を入力して下さい。';
$lang['account_holder_required'] = '口座名義を入力して下さい。';