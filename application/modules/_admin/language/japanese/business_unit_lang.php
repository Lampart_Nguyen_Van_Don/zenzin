<?php
//Title
$lang['lbl_business_unit_list'] = "一覧ビジネスユニット";
$lang['lbl_business_unit_info'] = "ビジネスユニット情報";
$lang['lbl_business_unit_add']  = "ビジネスユニットの追加";
$lang['lbl_business_unit_edit'] = "ビジネスユニットの編集";

//Confirm
$lang['message_sure_to_delete'] = 'あなたは、削除してもよろしいです?';

//Header content
$lang['lbl_business_unit_name'] = "事業部名";
$lang['lbl_bu_name'] = "事業部名";
$lang['lbl_is_product_div'] = "生産部門フラグ";
$lang['lbl_product_div'] = "生産部門";
$lang['lbl_target_months'] = "月報集計対象月数";
$lang['lbl_j_code'] = "Ｊコード用事業部コード";
$lang['lbl_bugyo_code'] = "奉行用-売上用部門コード";
$lang['lbl_bugyo_account_code'] = "奉行用-売上用勘定科目コード";
$lang['lbl_bugyo_abstract_string'] = "奉行用-売上用摘要文字列";
$lang['lbl_good_mn_code'] = "商品管理用事業部コード";
$lang['lbl_work_finish_report_template'] = '作業完了報告フォーマット';
$lang['lbl_template'] = "申込書備考テンプレート";

//Error
$lang['lbl_invalid_month'] = "12ヶ月以内で設定して下さい。";
$lang['lbl_invalid_input'] = '入力した値が無効です';
$lang['lbl_invalid_id'] = "無効な ID";
$lang['lbl_update_failed'] = "データ追加に失敗しました。";
$lang['lbl_delete_failed'] = "エラーの削除";
$lang['lbl_delete_exist'] = "この事業部は現在使用されています。";
$lang['lbl_insert_failed'] = "挿入に失敗しました";

$lang['txt_bu_name_required'] = "事業部名を入力して下さい。";
$lang['txt_target_months_required'] = "月報集計対象月数を入力して下さい。";
$lang['txt_j_code_required'] = "Ｊコード用事業部コードを入力して下さい。";
$lang['txt_bugyo_account_code_required'] = "奉行用-売上用勘定科目コードを入力して下さい。";
$lang['txt_bugyo_abstract_string_required'] = "奉行用-売上用摘要文字列を入力して下さい。";
$lang['txt_bugyo_department_code_required'] = "奉行用-売上用部門コードを入力して下さい。";
$lang['txt_product_mn_code_required'] = "商品管理用事業部コードを入力して下さい。";
$lang['rtb_template_required'] = "申込書備考テンプレートを入力して下さい。";
$lang['txt_product_mn_code_length'] = "2桁で設定して下さい。";
$lang['work_finish_report_template'] = '作業完了報告フォーマットを入力して下さい。';
$lang['txt_bu_name_unique'] = "入力された事業部名は既に使用されています。";

// Success
$lang['lbl_update_success'] = "更新は完了しました。";
$lang['lbl_delete_success'] = "成功の削除";
$lang['lbl_insert_success'] = "成功を挿入";

//Action
$lang['lbl_action_name'] = "処理";
$lang['btn_view'] = "見ます";
$lang['btn_close'] = "閉じる";
$lang['btn_save']   = "保存";
$lang['btn_edit']   = "編集";
$lang['btn_add']   = "追加";