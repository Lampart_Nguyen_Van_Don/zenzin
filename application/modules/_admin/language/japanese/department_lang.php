<?php
$lang['error_null_department_name']               = '部署名を入力してください。';
$lang['error_null_zipcode']                         = '郵便番号を入力してください。';
$lang['error_null_address1']                         = '住所1を入力してください。';
$lang['error_null_tel']                             = 'TELを入力してください。';
$lang['error_null_fax']                             = 'FAXを入力してください。';
$lang['error_null_business_unit_id']               = '事業部名を入力してください。';

$lang["common_lbl_business_unit"] = "事業部名";
$lang["common_lbl_department_name"] = "部署名";
$lang["common_lbl_action"] = "処理";
$lang["common_lbl_copy"] = "部署コピー作成";
$lang["common_lbl_department_zipcode"] = "郵便番号";
$lang["common_lbl_department_address1"] = "住所1";
$lang["common_lbl_department_address2"] = "住所2";

$lang["common_detail_department"] = "部署情報";
$lang["common_listof_department"] = "部署一覧";

$lang["common_edit_department"] = "部署編集";
$lang["common_copy_department"] = "部署コピー";
$lang["common_add_department"] = "部署追加";

$lang["common_duplicated_records"] = "<<business_unit>>-<<department>>が既に存在しています。";

$lang["alert_enter_a_hyphen"] = "ハイフンを入力してください。";
$lang["alert_not_enter_a_hyphen"] = "ハイフンは入力しないでください。";

$lang["error_zipcode_not_contains_hyphen"] = "郵便番号は数値のみで入力してください。";
$lang["error_TEL_contains_hyphen_and_no"] = "電話番号はハイフンと数値で入力してください。";
$lang["error_FAX_contains_hyphen_and_no"] = "FAXはハイフンと数値で入力してください。";

$lang["alert_could_not_delete_account_constraint"] = "この部署は現在使用されています。";
$lang["alert_zipcode_7_chars"] = "入力桁数が足りません。";

$lang['alert_name_128_chars_max'] = '部署名は 128文字を超えてはいけません。';
$lang['alert_zipcode_12_chars_max'] = '郵便番号は 12文字を超えてはいけません。';
$lang['alert_address1_max'] = '住所1は 255文字を超えてはいけません。';
$lang['alert_address2_max'] = '住所2は 255文字を超えてはいけません。';
$lang['alert_phone_no_max'] = 'TELは 16文字を超えてはいけません。';
$lang['alert_fax_no_max'] = 'FAXは 16文字を超えてはいけません。';
