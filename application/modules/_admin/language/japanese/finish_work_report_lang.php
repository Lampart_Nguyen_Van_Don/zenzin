<?php
//label
$lang['lbl_form_title'] = 'Finish work report manager';
$lang['lbl_delivery_date'] = '発送日';
$lang['lbl_j_code_branch_code'] = 'Ｊコード＋枝';
$lang['lbl_client_code'] = 'Ｓコード';
$lang['lbl_client_name'] = 'クライアント社名';
$lang['lbl_finish_work_status'] = '作業完了報告状況';
$lang['lbl_print'] = '印刷';
$lang['lbl_action'] = '処理';
$lang['lbl_status'] = '状況';
$lang['lbl_j_code'] = 'Ｊコード';
$lang['lbl_branch_code'] = '枝';
$lang['lbl_division_name'] = '部署名';
$lang['lbl_charge_name'] = 'お客様担当者';
$lang['lbl_fax'] = 'FAX';
$lang['lbl_set_name'] = '内容';
$lang['lbl_remark'] = '備考';
$lang['lbl_arrange_rep'] = '手配担当';
// * 6849 - delete
$lang['lbl_delivery_date_description'] = '(From必須　Fromのみ指定の場合、指定日のみ検索　From・To指定で範囲指定検索）';
$lang['lbl_j_code_branch_code_description'] = '(Fromのみ指定の場合、前方一致検索　From・To指定で前方一致の範囲指定検索）';
$lang['lbl_client_code_description'] = '(前方一致検索）';
$lang['lbl_client_name_description'] = '(あいまい検索）';
$lang['approve_reject_form_title'] = '受注承認/却下';
$lang['lbl_download_link'] = '[DL]';
$lang['lbl_s_business_unit'] = 'Ｓ担当営業';
$lang['lbl_client_name_furigana'] = 'クライアント社名（フリガナ）';
$lang['lbl_client_department_name'] = 'クライアント部署名';
$lang['lbl_address'] = '住所';
$lang['lbl_comment_change_report'] = '変レポコメント';
$lang['lbl_status_detail'] = 'ステータス詳細';
$lang['lbl_order_form_input_date'] = '申込書右上日付';
$lang['lbl_is_change_report_apply'] = '変レポ申請中';
$lang['lbl_comment_report'] = '変レポコメント';
$lang['lbl_comment_report_approve_reject'] = '変レポ承認/却下コメント';
//button
$lang['btn_search'] = '検索';
$lang['btn_print'] = '一括作業完了報告印刷';
//7184:Start
$lang['btn_check_all_application_form_print'] = '一括作業完了報告印刷チェック';
//7184:End
//option
$lang['all'] = '全て';

//error msg
$lang['delivery_date_from_required'] = '発送日Fromを入力して下さい。';
//7203:Start
//$lang['delivery_date_from_invalid'] = '発送日が不正です。';
//$lang['delivery_date_from_invalid'] = '納品日Fromが無効です。';
//7220:Start
//$lang['delivery_date_from_invalid'] = '納品日Fromが無効です。';
$lang['delivery_date_from_invalid'] = '発送日Fromが無効です。';
//7220:End
//$lang['delivery_date_to_invalid'] = '発送日TOを入力して下さい。';
//7220:Start
//$lang['delivery_date_to_invalid'] = '納品日Toが無効です。';
$lang['delivery_date_to_invalid'] = '発送日Toが無効です。';
//7220:End
//7203:End
//7203:Start
$lang['delivery_date_invalid_range_from_less_than_to'] = '検索範囲が正しくありません。';
//7220:Start
//$lang['delivery_date_invalid_range'] = '納品日Fromは必須項目です。';
$lang['delivery_date_invalid_range'] = '発送日Fromは必須項目です。';
//7220:End
//7203:End
$lang['no_data'] = 'データ無し';
$lang['row_has_error'] = '行％param％：エラー発生';
$lang['report_fax_no_required'] = 'FAX番号が必要です。';
$lang['report_fax_no_invalid'] = 'FAX番号が無効です。';
$lang['system_notification'] = 'システム通知';
$lang['choose_least_work_report'] = '印刷データをチェックしてください。';
$lang['nothing_changed'] = '変更はありません。';
$lang['msg_enter_hyphen_and_num'] = '%field%はハイフンと数値で入力してください。';
$lang['j_code_error'] = '検索範囲が正しくありません。';

//success msg
$lang['update_fail'] = '作業完了報告の更新は失敗しました。';
$lang['update_success'] = '作業完了報告の更新は成功しました。';
$lang['lbl_no_data_available'] = "データがありません。";

//label
$lang['lbl_comment'] = 'コメント';
$lang['lbl_s_code'] = 'Ｓコード';
$lang['lbl_name'] = '社名';
$lang['lbl_name_kana'] = '社名（フリガナ）';
$lang['lbl_zipcode'] = '郵便番号';
$lang['lbl_phone_fax'] = 'TEL / FAX';
$lang['lbl_phone'] = 'TEL';
$lang['lbl_fax'] = 'FAX';
$lang['lbl_devision_name'] = '部署名';
$lang['lbl_charge_name'] = 'お客様担当者';
$lang['lbl_payment_term'] = '支払条件';
$lang['lbl_account_id'] = 'Ｊ担当営業';
$lang['lbl_payment_term'] = '支払条件';
$lang['lbl_order_regist_date'] = '受注登録日';
$lang['lbl_cost_total_price'] = '税別合計';
$lang['lbl_consumption_tax'] = '消費税';
$lang['lbl_tax_total'] = '税込合計';
$lang['lbl_gross_profit_amount'] = '粗利合計';

//header text for popup
$lang['lbl_branch_cd'] = '枝';
$lang['lbl_order_status'] = '受注ステータス';
$lang['lbl_change_report'] = '変レポ';
$lang['lbl_product_name'] = '商品名';
$lang['lbl_detail_contents'] = '内容';
//7220:Start
//$lang['lbl_delivery_date'] = '納品日';
$lang['lbl_delivery_date'] = '発送日';
//7220:End
$lang['lbl_billing_date'] = '請求日';
$lang['lbl_payment_date'] = '支払日';
$lang['lbl_quantity'] = '数量';
$lang['lbl_tax_type'] = '税';
$lang['lbl_unit_price'] = '単価';
$lang['lbl_amount'] = '合計';
$lang['lbl_order_detail_cost_unit_price'] = '原価税別単価';
$lang['lbl_order_detail_cost_total_price'] = '原価税別合計';
$lang['lbl_gross_profit_amount'] = '粗利合計';
$lang['lbl_order_detail_gross_profit_rate'] = '粗利率';
$lang['lbl_j_code_branch_code'] = 'Ｊコード＋枝';
$lang['lbl_is_ad_sales_slip_input'] = '前売伝';
$lang['lbl_is_ad_invoice_issue'] = '前請求';
$lang['lbl_is_order_input'] = '発注';
$lang['lbl_is_acceptance_input'] = '検収';
$lang['lbl_is_sales_slip_input'] = '売伝';
$lang['lbl_is_invoice_issue'] = '請求';
$lang['lbl_content'] = '内容';
$lang['lbl_copy_branch'] = '枝コピー';

$lang['lbl_applier_info'] = '申込者情報';
$lang['lbl_closing_date'] = '締め日';
$lang['lbl_payment_month_cd'] = '支払月';
$lang['lbl_payment_day_cd'] = '支払日';
$lang['lbl_credit_infor'] = '与信情報';
$lang['lbl_credit_infor_ad_receive'] = '与信情報 前受金';
$lang['lbl_credit_limit'] = '与信限度額（与信限度額-売掛金）';
$lang['lbl_receive_payment'] = '前受金';
$lang['lbl_j_code_branch_code'] = 'Ｊコード＋枝';
$lang['lbl_order_date_approve'] = '受注承認日';
$lang['lbl_j_business_unit'] = '事業部';
$lang['lbl_approve_report'] = '変レポ承認';
$lang['lbl_reject_report'] = '却下';
$lang['lbl_is_ad_sale_slip_input'] = '前売上';
//Button
$lang['lbl_delete_branch'] = '枝削除';
$lang['lbl_cancel_branch'] = '枝キャンセル';
$lang['lbl_show'] = '参照';
$lang['lbl_check_approve_more'] = '一括承認';
$lang['lbl_cancel_branch_release'] = 'キャンセル解除';
$lang['btn_approve'] = '受注承認';
$lang['btn_reject'] = '却下';
$lang['lbl_action'] = '処理';
$lang['lbl_copy_branch'] = '枝コピー';
$lang['lbl_copy_order'] = '申込書コピー作成';
$lang['lbl_order_approve_reject'] = '受注承認/却下';
$lang['lbl_order_change_report_approve_reject'] = '変レポ承認/却下';

$lang['lbl_add_branch'] = '追加';
$lang['lbl_add_order'] = '新規申込書作成';
$lang['lbl_add_order_save'] = '申込書作成';
$lang['lbl_approve_order'] = '一括受注承認';
//7220:Start
//$lang['lbl_print'] = '申込書印刷';
//$lang['lbl_print'] = '作業完了報告印刷';
$lang['lbl_print'] = '印刷';
//7220:End
$lang['lbl_csv_download'] = 'CSVダウンロード';
$lang['lbl_save'] = '保存';
$lang['lbl_edit'] = '編集';
$lang['lbl_search'] = '検索';
$lang['lbl_show'] = '参照';
$lang['lbl_close'] = '閉じる';
$lang['lbl_check_approve_more'] = '一括承認';
$lang['lbl_delete_order'] = '削除';

$lang['lbl_cancel_branch_release'] = 'キャンセル解除';
$lang['lbl_regist_cancel'] = '申込キャンセル';
$lang['lbl_filter'] = '絞込';
$lang['lbl_client_change'] = '決定';
//Message
$lang['change_report_nothing_change'] = '変レポ内容に変更がありません。';
$lang['edit_nothing_change'] = '変更はありません。';
//FWR:Start
$lang['lbl_header_media_name_for_dp'] = '媒体名';
$lang['lbl_header_number_of_print_for_dp'] = '掲載部数';
$lang['lbl_header_delivery_finish_datefor_dp'] = '納品完了日';
//FWR:End
