<?php

$lang['lbl_type']     = '種別';
$lang['lbl_action']   = '処理';
$lang['lbl_sequence'] = 'ソート順';
$lang['lbl_name']     = '名称';
$lang['lbl_is_display']  = '新規入力';
$lang['lbl_code']     = 'コード';
$lang['btn_display']  = '表示';
$lang['btn_add_new']  = '追加';
$lang['btn_ref']      = '参照';

$lang['btn_edit'] = '編集';
$lang['btn_close'] = '閉じる';

$lang['lbl_name_of_gui_parts']         = '種別' ;
$lang['lbl_code_of_gui_parts_element'] = 'コード';
$lang['lbl_name_of_gui_parts_element'] = '名称';
$lang['lbl_display_or_not']            = '新規入力不可フラグ';
$lang['btn_save']                      = '保存';
$lang['btn_comeback']                  = '一覧へ戻る';
$lang['btn_delete']                    = '削除';
$lang['btn_add']                       = '追加';

$lang['info_display'] = '新規入力不可';

$lang['lbl_edit_error']     = 'データ更新に失敗しました。';
$lang['lbl_edit_success']   = '更新しました';
$lang['lbl_add_error']      = 'データ追加に失敗しました。';
$lang['lbl_add_success']    = '追加しました';
$lang['lbl_delete_success'] = '削除は成功しました';
$lang['lbl_delete_error']   = '削除は失敗しました';
$lang['lbl_delete_using_error']    = "このコード・名称 は現在使用されています。";

$lang['lbl_invalid_input']         = 'ソート順を入力してください。';
$lang['error_name_required']       = '名称を入力してください。';
$lang['error_sequence_required']   = 'ソート順が必須です';
$lang['error_sequence_max_length'] = 'ソート順は最大5桁'; //シーケンスは最大5桁
$lang['error_sequence_numeric']    = 'シーケンスは数値みに入力可です';
$lang['error_sequence_greater_than'] = "より小さい数しか入力できません";