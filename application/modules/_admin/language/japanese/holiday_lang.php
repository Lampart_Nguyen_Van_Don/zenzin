<?php

$lang['lbl_established_year'] = '設定済み';
$lang['lbl_year']             = '対象年';
$lang['lbl_establish_day']    = '定休日';
$lang['lbl_monday']           = '月';
$lang['lbl_tuesday']          = '火';
$lang['lbl_wednesday']        = '水';
$lang['lbl_thursday']         = '木';
$lang['lbl_friday']           = '金';
$lang['lbl_saturday']         = '土';
$lang['lbl_sunday']           = '日';
$lang['lbl_holiday']          = '祝日';

$lang['btn_establish_day'] = '休日一括設定';
$lang['btn_search']        = '表示';

$lang['error_no_filter_checked']   = '休日を設定してください。';
$lang['error_can_not_change']      = '過去の日付は設定変更ができません。';
$lang['error_can_not_change_current_date'] = '現在の日付は設定変更ができません。';
$lang['confirm_overwrite_holiday'] = '休日を設定します。よろしいですか？';
$lang['confirm_set_holiday']       = '営業日から休日に変更しますがよろしいですか？';
$lang['confirm_reject_holiday']    = '休日から営業日に変更しますがよろしいですか？';


$lang['lbl_insert_success'] = '休日設定が完了しました。';
$lang['lbl_insert_error']   = '休日設定は失敗しました。';

$lang['lbl_invalid_input'] = '入力した値が無効です';

$lang['warning_change_holiday'] = '個別で設定した休日もリセットされます。';
