<?php

$lang['lbl_sales_slip_status']         = '請求書ステータス';
$lang['lbl_status_approval_pending']   = '請求書発行承認申請中';
$lang['lbl_status_approved']           = '請求書発行承認済み';
$lang['lbl_status_issued']             = '請求書発行済み';
$lang['lbl_status_discarded']          = '請求書発行破棄';
$lang['lbl_status_sales_slip_created'] = '請求書発行却下';

$lang['lbl_j_account_id'] = 'Ｊ担当営業';
$lang['lbl_s_account_id'] = 'Ｓ担当営業';
$lang['lbl_business_id']  = '事業部';

$lang['lbl_sales_slip_number'] = '請求書番号';
$lang['lbl_client_name']       = 'クライアント社名';
$lang['lbl_delivery_date'] = '納品日';
$lang['lbl_billing_date']      = '請求日';
$lang['lbl_payment_date']      = '支払日';
$lang['lbl_s_code']            = 'Ｓコード';
$lang['lbl_name_kana']         = 'クライアント社名（フリガナ）';
$lang['lbl_division_name']     = 'クライアント部署名';

$lang['lbl_sales_slip_number_tooltip'] = '(数字部分のみ入力、完全一致検索）';
$lang['lbl_approximate_search']        = '(あいまい検索）';
$lang['lbl_date_tooltip']              = '(Fromのみ指定の場合、指定日のみ検索　From・To指定で範囲指定検索　年月or年月日指定）';
$lang['lbl_s_code_tooltip']            = '(前方一致検索）';

$lang['lbl_approve_and_invoice'] = '一括承認＆請求書発行';

$lang['error_billing_date_invalid'] = '請求日は日付フォーマットが無効です。';
$lang['error_payment_date_invalid'] = '支払日は日付フォーマットが無効です。';
$lang['areyousure'] =  "破棄してよろしいですか？";

  //////////
 // GRID //
//////////

$lang['lbl_approve_all']      = '一括承認';
$lang['lbl_charge_name']      = 'お客様担当者';
$lang['lbl_cost_total_price'] = '税別合計';
$lang['lbl_consumption_tax']  = '消費税';
$lang['lbl_tax_total']        = '税込合計';
$lang['lbl_income_month']     = '入金年月';

$lang['lbl_please_select_approve_print'] = '請求書発行対象はありません。';
$lang['lbl_invoice_approval_account_date'] = '初回印刷日';

  ////////////
 // ERRORS //
////////////

$lang['error_delivery_date_invalid'] = '納品日は日付フォーマットが無効です。';
$lang['error_delivery_date_from_bigger_to'] = '検索範囲が正しくありません。';
$lang['error_is_settlement_approve'] = '締め済みのため、承認できません。';
$lang['error_is_settlement_approve_reject'] = '締め済みのため、承認/却下できません。';
$lang['error_is_settlement_reject'] = '締め済みのため、却下できません。';
$lang['error_is_settlement_discard'] = '締め済みのため、破棄できません。';

//#6930:Start
$lang['error_check_sales_lip_not_receive_payment'] = "確定分の請求書があるため、破棄できません。";
//#6930:End

  ////////////
 // DETAIL //
////////////
$lang['btn_print_invoice']    = '請求書印刷';
$lang['btn_download_invoice'] = '請求書PDFダウンロード';
$lang['btn_approval_reject']  = '請求書承認/却下';
$lang['btn_discard']          = '請求書破棄';

$lang['lbl_zipcode']             = '郵便番号';
$lang['lbl_address_1']           = '住所１';
$lang['lbl_address_2']           = '住所２';
$lang['lbl_sort_division_name']  = '部署';
$lang['lbl_bill_closing_date']   = '請求締め日';
$lang['lbl_payment_expire_date'] = 'お支払い期限日';
$lang['lbl_j_code']              = 'Ｊコード';
$lang['lbl_branch_cd']           = '枝';
$lang['lbl_delivery_date_2']     = '納品日';
$lang['lbl_product_name']        = '商品名';
$lang['lbl_contents']            = '内容';
$lang['lbl_tax_type']            = '税';
$lang['lbl_quantity']            = '数量';
$lang['lbl_unit_price']          = '単価';
$lang['lbl_total']               = '金額';

$lang['lbl_edit_error']     = 'データ更新に失敗しました。';
$lang['lbl_edit_success']   = '更新しました';

$lang['error_invalid_input'] = '入力した値が無効です';

  /////////////////////
 // APPROVAL REJECT //
/////////////////////

$lang['lbl_approval_reject_comment'] = '承認/却下コメント';
$lang['btn_approve_invoice']         = '請求書発行承認';
$lang['btn_reject_invoice']          = '請求書発行承認却下';
//#7434:Start
$lang['common_lbl_exportcsv'] 		 = "CSVダウンロード";
//#7434:End

//#9787:Start
$lang['lbl_reject_success']           = '却下しました。';
$lang['send_mail_change_report_fail'] = 'メール変更通知が失敗送ります';
//#9787:End