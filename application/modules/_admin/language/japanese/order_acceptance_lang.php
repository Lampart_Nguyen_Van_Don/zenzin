<?php

$lang['lbl_input_file'] = '発注入力ファイル';

$lang['btn_download_template'] = '発注入力用テンプレートダウンロード';
$lang['btn_browse']            = '選択';
$lang['btn_upload']            = 'データ読み込み＆チェック';
$lang['lbl_loading']           = '読み込み中';
$lang['lbl_complete']          = '完了しました。';
$lang['btn_import']            = 'インポート';
$lang['btn_cancel']            = 'キャンセル';
$lang['lbl_file_limit_size']   = '※最大容量10MB';

$lang['lbl_acceptance_condition']      = '検索条件';
$lang['lbl_approve_status']            = '承認状態';
$lang['lbl_jcode_branch_cd']           = 'Ｊコード＋枝';
$lang['lbl_jcode_branch_tooltip']      = '(Fromのみ指定の場合、前方一致検索　From・To指定で前方一致の範囲指定検索）';
$lang['lbl_account_id']                = 'Ｊ担当営業';
$lang['lbl_business_unit_name']        = '事業部';
$lang['lbl_arrange_rep']               = '手配担当';
$lang['lbl_hidden_search']             = '(あいまい検索）';
$lang['lbl_client_name']               = 'クライアント社名';
$lang['lbl_source_subcontractor_id']   = '請求元ブレーン';
$lang['lbl_work_subcontractor_id']     = '作業ブレーン';
$lang['lbl_shipping_subcontractor_id'] = '発送ブレーン';
$lang['lbl_shipping_type_cd']          = '発送種別';
$lang['lbl_delivery_date_acceptance']  = '発送日';
//#7156:Start
// $lang['lbl_delivery_date_tooltip']     = '(Fromのみ指定の場合、指定日のみ検索　From・To指定で範囲指定検索　年月or年月日指定）';
$lang['lbl_delivery_date_tooltip']     = '(Fromのみ指定の場合、指定日以降検索　From・To指定で範囲指定検索　年月or年月日指定）';
//#7156:End
$lang['lbl_summary_month']             = '計上月';
//#7156:Start
//$lang['lbl_summary_month_tooltip']     = '(Fromのみ指定の場合、指定年月のみ検索　From・To指定で範囲指定検索）';
$lang['lbl_summary_month_tooltip']     = '(Fromのみ指定の場合、指定年月以降検索　From・To指定で範囲指定検索)';
//#7156:End
$lang['lbl_order_acceptance_amount']   = '合計(税別)';

$lang['btn_export_csv']             = 'CSVダウンロード';
$lang['btn_approve_all_acceptance'] = '一括検収承認';
$lang['btn_cancel_all_acceptance']  = '一括検収承認解除';
$lang['btn_go_to_import']           = '発注入力インポート';
$lang['btn-check-all-click']       =  " 一括検収承認チェック";

$lang['lbl_approve_all']             = '一括検収承認';
$lang['lbl_action']                  = '処理';
$lang['btn_separate']                = '分割';
$lang['lbl_edit_acceptance']         = '変更';
$lang['lbl_order_acceptance_status'] = '発注・検収ステータス';
$lang['lbl_diffence_common']         = '個別・共通';
$lang['lbl_jcode']                   = 'Ｊコード';
$lang['lbl_branch']                  = '枝';
$lang['lbl_set_name']                = 'セット名';
$lang['lbl_unit_price']              = '単価';
$lang['lbl_quantity_acceptance']     = '件数';
$lang['lbl_fee']                     = '円口';
$lang['lbl_weight']                  = '重量';
$lang['lbl_shipment_shape_cd']       = '発送物形状';
$lang['lbl_free_tax']                = '非課税';
$lang['lbl_shipping_charge_tax']     = '税込発送料';
$lang['lbl_stamp_tax']               = '税込切手代';
$lang['lbl_etc_tax']                 = '税込その他';
$lang['lbl_shipping_charge']         = '税別発送料';
$lang['lbl_etc']                     = '税別その他';
$lang['lbl_total_amount']            = '合計(税別、非課税)';

$lang['checkbox_unapproved']      = '未承認';
$lang['checkbox_approved']        = '検収承認済み';
$lang['checkbox_account_tighten'] = '経理締め済み';

$lang['lbl_edit_error']     = 'データ更新に失敗しました。';
$lang['lbl_edit_success']   = '保存しました。';

$lang['lbl_approve_success'] = '選択した発注・原価情報を承認しました。';
$lang['lbl_approve_error']   = '一括検収承認は失敗しました。';

$lang['lbl_unapprove_success'] = '一括検収承認解除しました';
$lang['lbl_unapprove_error']   = '一括検収承認解除は失敗しました。';

//$lang['lbl_confirm_delete'] = 'あなたは必ず、この行を削除します111';
$lang['lbl_confirm_delete'] = '行目を削除します。よろしいですか？';


    /////////////////////////
   //                     //
  // IMPORT EXCEL ERROR  //
 //                     //
/////////////////////////
$lang['lbl_please_select_file']          = 'アップロードファイルを選択してください。';
$lang['lbl_unsupported_file']            = 'ファイル種類が無効です。';
$lang['lbl_empty_file']                  = 'ファイルのデータがありません。';
$lang['lbl_invalid_format']              = '発注入力用テンプレートファイルが無効です。';
$lang['lbl_over_size']                   = 'インポートファイル容量は最大１０ＭＢで選択してください。';
$lang['lbl_row']                         = '行目';
$lang['lbl_source_subcontractor_code']   = '請求元ブレーンコード';
$lang['lbl_work_subcontractor_code']     = '作業ブレーンコード';
$lang['lbl_shipping_subcontractor_code'] = '発送ブレーンコード';
$lang['lbl_shipping_type_code']          = '発送種別コード';


$lang['error_jcode_branch_is_not_exist']           = 'Jコード及び枝が存在していません。';
$lang['error_empty_account_id']                    = 'Ｊ担当営業ＩＤは必須項目です。';
$lang['error_empty_client_name']                   = 'クライアント社名は必須項目です。';
$lang['error_branch_only_two_characters']          = '枝2文字が含まれています。';
$lang['error_source_subcontractor_can_not_use']    = '請求元ブレーンコードを使用できません。';
$lang['error_empty_source_subcontractor']          = '請求元ブレーンは必須項目です。';
$lang['error_work_subcontractor_can_not_use']      = '作業ブレーンコードを使用できません。';
$lang['error_shipping_subcontractor_can_not_use']  = '発送ブレーンコードを使用できません。';
$lang['error_shipping_type_can_not_use']           = '発送種別コードを使用できません。';
$lang['error_empty_shipping_type']                 = '発送種別コードは必須項目です。';
$lang['error_unit_price_must_be_number']           = '単価は数値で入力してください。';
$lang['error_unit_price_can_not_be_negative']      = '単価はマイナスしてはいけないです。';
$lang['error_quantity_must_be_number']             = '件数は数値で入力してください。';
$lang['error_quantity_can_not_be_negative']        = '件数はマイナスしてはいけないです。';
$lang['error_quantity_must_be_integer']            = '件数は整数で入力してください。';
$lang['error_delivery_date_invalid']               = '発送日が無効です。';
$lang['error_fee_must_be_number']                  = '円口は数値で入力してください。';
$lang['error_fee_can_not_be_negative']             = '円口はマイナスしてはいけないです。';
$lang['error_fee_must_be_integer']                 = '円口は整数で入力してください。';
$lang['error_fee_max_length']                 	   = '円口は１０桁以上の入力ができません。管理部に問い合わせてください。';
$lang['error_weight_must_be_number']               = '重量は数値で入力してください。';
$lang['error_weight_can_not_be_negative']          = '重量はマイナスしてはいけないです。';
$lang['error_weight_must_be_integer']              = '重量は整数で入力してください。';
$lang['error_weight_max_length']              	   = '重量は4桁以上の入力ができません。管理部に問い合わせてください。';
$lang['error_summary_month_has_not_close']         = '計上月が無効です。';
$lang['error_is_not_exist']                        = '%field%が存在していません。';
$lang['error_account_resigned']                    = '%field%は退職しました。';
$lang['error_total_amount_max_length']             = '合計(税別、非課税)は１０桁以上の入力ができません。管理部に問い合わせてください。';

$lang['error_shipments_shape_id_is_not_exist']    = '発送物形状コードが存在していません。';
$lang['error_account_id_is_not_exist']            = 'Ｊ担当営業ＩＤが存在していません。';
$lang['error_arrange_rep_is_not_exist']           = '手配担当IDが存在していません。';

$lang['error_tax_free_must_be_number']            = '非課税は数値で入力してください。';
$lang['error_tax_free_must_be_integer']           = '非課税は整数で入力してください。';
$lang['error_tax_free_max_length']                = '非課税は１０桁以上の入力ができません。管理部に問い合わせてください。';

$lang['error_shipping_charge_tax_must_be_number'] = '税込発送料は数値で入力してください。';
$lang['error_shipping_charge_tax_must_be_integer']= '税込発送料は整数で入力してください。';
$lang['error_shipping_charge_tax_max_length']	  = '税込発送料は１０桁以上の入力ができません。管理部に問い合わせてください。';

$lang['error_shipping_stamp_tax_must_be_number']  = '税込切手代は数値で入力してください。';
$lang['error_stamp_tax_must_be_integer']          = '税込切手代は整数で入力してください。';
$lang['error_stamp_tax_max_length']               = '税込切手代は１０桁以上の入力ができません。管理部に問い合わせてください。';

$lang['error_etc_tax_must_be_number']             = '税込その他は数値で入力してください。';
$lang['error_etc_tax_must_be_integer']            = '税込その他は整数で入力してください。';
$lang['error_etc_tax_max_length']				  = '税込その他は１０桁以上の入力ができません。管理部に問い合わせてください。';

$lang['error_shipping_charge_must_be_number']     = '税別発送料は数値で入力してください。';
$lang['error_shipping_charge_must_be_integer']    = '税別発送料は整数で入力してください。';
$lang['error_shipping_charge_max_length']         = '税別発送料は１０桁以上の入力ができません。管理部に問い合わせてください。';

$lang['error_etc_must_be_number']                 = '税別その他は数値で入力してください。';
$lang['error_etc_must_be_integer']                = '税別その他は整数で入力してください。';
$lang['error_etc_max_length']                     = '税別その他は１０桁以上の入力ができません。管理部に問い合わせてください。';

$lang['error_max_size']                           = 'インポートファイル容量は最大１０ＭＢで選択してください。';
$lang['error_finalization']                       = '締め済みのため、インポートできません。';
//#9695:Start
$lang['error_account_business']                   = 'ERROR BUSINESS UNIT';
$lang['error_subcontractor_business']             = 'ERROR SUBCONTRACTOR BUSINESS UNIT';
//#9695:End
$lang['lbl_import_error']      = 'インポートに失敗しました。';
$lang['lbl_import_success']    = 'インポートしました';


$lang['export_csv_file_name'] = "発注・検収_" . date('Ymd') .'.csv';

    ////////////////////////////
   //                        //
  // INPUT ORDER ACCEPTANCE //
 //                        //
////////////////////////////

$lang['error_j_code_max_length']                     = 'Ｊコードは最大128桁です。';
$lang['error_j_code_is_required']                    = 'Ｊコードは必須項目です。';
$lang['error_branch_cd_max_length']                  = '枝は最大128桁です。';
$lang['error_branch_cd_is_required']                 = '枝は必須項目です。';
$lang['error_account_id_is_required']                = 'Ｊ担当営業は必須項目です。';
$lang['error_arrange_rep_is_required']               = '手配担当は必須項目です。';
$lang['error_client_name_is_required']               = 'クライアント社名は必須項目です。';
$lang['error_source_subcontractor_id_is_required']   = '請求元ブレーンは必須項目です。';
$lang['error_work_subcontractor_id_is_required']     = '作業ブレーンは必須項目です。';
$lang['error_shipping_subcontractor_id_is_required'] = '発送ブレーンは必須項目です。';
$lang['error_shipping_type_cd_is_required']          = '発送種別コードは必須項目です。';
$lang['error_set_name_is_required']                  = 'セット名は必須項目です。';
$lang['error_unit_price_is_required']                = '単価は必須項目です。';
$lang['error_unit_price_max_length']                 = '単価は１０桁以上の入力ができません。管理部に問い合わせてください。';
$lang['error_quantity_is_required']                  = '件数は必須項目です。';
//#8258:Start
$lang['error_quantity_max_length']                   = '件数は8桁以上の入力ができません。管理部に問い合わせてください。';
//#8258:End
$lang['error_delivery_date_is_required']             = '発送日は必須項目です。';
$lang['error_fee_is_required']                       = '円口は必須項目です。';
$lang['error_weight_is_required']                    = '重量は必須項目です。';
$lang['error_shipments_shape_cd_is_required']        = '発送物形状コードは必須項目です。';
$lang['error_j_code_branch_cd_search_area']          = '検索範囲が正しくありません。';
$lang['error_delivery_date_search_area']             = '検索範囲が正しくありません。';
$lang['error_summary_month_search_area']             = '検索範囲が正しくありません。';
$lang['error_delivery_date_form_is_required']        = '発送日Fromは必須項目です。';

$lang['error_delivery_date_form_is_invalue']        = '発送日は無効です。';

$lang['error_summary_month_form_is_required']       = '計上月Fromは必須項目です。';

$lang['error_summary_month_insert_is_required']     = "計上月は必須項目です。";

$lang['lbl_no_data_available'] = "データがありません。";

$lang['lbl_confirm_close_windows'] = "画面を閉じたりリロードしたりしたら、変更されたデータを失います。";

$lang['lbl_nothing_change'] = '変更がありません。';
$lang['lbl_please_select_unapprove'] = '検収承認解除対象はありません。';
$lang['lbl_please_select_approve'] = '検収承認対象はありません。';
// #7090:start
$lang['lbl_error_format_summary_month'] = "計上月が無効です。日付型yyyy/mmとしてやり直してください。";
// #7090:End

//#7774:Start
$lang['error_delivery_date_required'] = "計上月は必須項目です。";
//#7774:End

//#9748: Start
$lang['lbl_delivery_finish_date'] = "納品完了日";
$lang['lbl_product_name']               = '商品名';
$lang['lbl_content_order'] ='内容';
$lang['lbl_unit_price_in_order'] ='受注時単価';
$lang['lbl_quanity_in_order'] ='受注時数量';
$lang['lbl_total_sales_in_order'] = '受注時売上（税別）';
$lang['lbl_total_cost_in_order_excluding_tax'] = '受注時原価合計（税別）';
$lang['lbl_fixed_cost_excluding_tax'] = "確定原価（税別）";

$lang['error_fixed_cost_excluding_tax'] = "確定原価（税別）は必須項目です。";
$lang['error_delivery_finish_date_required'] = "納品完了日は必須項目です。";

$lang['error_max_length_amount'] = "確定原価（税別）は１０桁以上の入力ができません。管理部に問い合わせてください。";
$lang['error_fixed_cost_excluding_tax_must_be_number']                 = '確定原価（税別） は数値で入力してください。';
$lang['error_delivery_finish_date_invalid']               = '納品完了日が無効です。';
//#9748: End