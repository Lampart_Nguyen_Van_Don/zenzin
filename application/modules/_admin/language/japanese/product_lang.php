<?php

$lang['code']= '受注商品コード';
$lang['business_unit']= '事業部名';
$lang['name_product']= '商品名';
// $lang['is_no_add']= '課税/非課税';

$lang['message_sure_to_delete'] = '削除します。よろしいですか？';
$lang['error_code_null']= '受注商品コードは必須項目です。';
$lang['error_code_max_length']= '受注商品コードは 5文字を超えてはいけません。';
$lang['error_code_unique']= '受注商品コードは既に使用されています';
$lang['error_business_unit_null']= '事業部名は必須項目です。';
$lang['error_business_unique']= '事業部名は既に使用されています';
$lang['error_name_product_null']= '受注商品名を入力してください。';
$lang['error_name_product_limit_character']= '商品名 欄は %s文字を超えてはいけません。';
$lang['error_is_no_add_null']= '課税/非課税は必須項目です。';
$lang['error_order_product_exists'] = 'この受注商品は現在存在しています。';
$lang['error_null_product_manage_code'] = '%sの商品管理用事業部コードを設定してください。';

$lang['edit_product']= '受注商品編集';
$lang['is_no_add']= '新規入力';
$lang['is_no_add_in_edit']= '新規入力不可フラグ';
$lang['product_information']= '受注商品情報';
$lang['add_new_product']= '受注商品新規追加';
$lang['list_of_product']= '受注商品一覧';

$lang['is_no_add_0']= '可';
$lang['is_no_add_1']= '不可';
$lang['is_no_add_flag']= '新規入力不可';