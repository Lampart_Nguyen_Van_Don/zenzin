<?php
$lang['msg_import_successfully']     = 'Import successfully!';
$lang['msg_import_fail']     = 'Import fail! Please try again later!';
$lang['msg_only_xls_xlsx']     = 'Only file xls or xlsx can be uploaded!';

$lang['lbl_check']     = 'チェック結果';
$lang['lbl_datetime']     = '日付';
$lang['lbl_source_acc']     = '振込元';
$lang['lbl_amount']     = '入金額';
$lang['lbl_s_code']     = 'Ｓコード';
$lang['lbl_client_name']     = 'クライアント名';
$lang['lbl_action']     = '処理';
$lang['lbl_import_ng']     = 'クライアント取得NG';
$lang['lbl_confirm_client']     = 'クライアント決定';

$lang['lbl_file_import']     = '入金情報ファイル';
$lang['lbl_import_and_process']     = 'データ読み込み＆チェック';

$lang['btn_import']     = 'インポート';
$lang['btn_cancel']     = 'キャンセル';

$lang['lbl_amount_less_than_zero'] = "Amount less than zero";



/* start volyminhnhan@gmail.com modification*/
$lang['msg_err_s_code']     = 'Please input customer s-code - you can translate this in language file!';
$lang['btn_download_template'] = '発注入力用テンプレートダウンロード';
$lang['btn_browse']            = '選択';
$lang['btn_upload']            = 'データ読み込み＆チェック';
$lang['lbl_loading']           = '読み込む中';
$lang['lbl_complete']          = '完成しました';
$lang['btn_import']            = 'インポート';
$lang['btn_cancel']            = 'キャンセル';
$lang['lbl_file_limit_size']   = '※最大容量10MB';
/* end volyminhnhan@gmail.com modification*/


  ///////////////
 // CLEARANCE //
///////////////
$lang['lbl_summary_month']                = '計上年月';
$lang['lbl_remaining_scrubbing']          = '消し込み残';
$lang['lbl_remaining_scrubbing_show']     = '消し込み残のみ表示';
$lang['lbl_s_code_tooltip']               = '(前方一致検索）';
$lang['lbl_previous_month']               = '月以前入金予定分';
$lang['lbl_current_month']                = '月入金予定分';
$lang['lbl_next_month']                   = '月以降入金予定分';
$lang['lbl_sales_slip_number']            = '請求書No';
$lang['lbl_sales_slip_amount']            = '請求額';
$lang['lbl_previous_receive_payment']     = '前受金';
$lang['lbl_current_receive_payment']      = '月入金';
$lang['lbl_etc']                          = 'その他入金<br>（手入力）';
$lang['lbl_fee']                          = '手数料<br>（手入力）';
$lang['lbl_repayment']                    = '返金<br>（手入力）';
$lang['lbl_miscellaneous_loss']           = '雑損失<br>（手入力）';
$lang['lbl_miscellaneous_income']         = '雑収入<br>（手入力）';
$lang['lbl_offset_next_month_receive']    = '入金消し込み<br>翌月売掛金';
$lang['lbl_offset_next_month_ad_receive'] = '入金消し込み<br>翌月前受金';
$lang['lbl_bugyo_next_month_receive']     = '勘定奉行<br>翌月売掛金';
$lang['lbl_bugyo_next_month_ad_receive']  = '勘定奉行<br>翌月前受金';
$lang['lbl_debt']                         = '売掛金';


$lang['lbl_total_previous_receive_payment']     = '月末前受金';
$lang['lbl_total_current_receive_payment']      = '月入金';
$lang['lbl_total_offset_next_month_receive']    = '月末入金消し込み<br>売掛金';
$lang['lbl_total_offset_next_month_ad_receive'] = '月末入金消し込み<br>前受金';
$lang['lbl_total_bugyo_next_month_receive']     = '月末勘定奉行<br>売掛金';
$lang['lbl_total_bugyo_next_month_ad_receive']  = '月末勘定奉行<br>前受金';
//#9696:Start
$lang['lbl_total_etc']                          = '月その他入金';
$lang['lbl_total_fee']                          = '月手数料';
$lang['lbl_total_repayment']                    = '月返金';
$lang['lbl_total_miscellaneous_loss']           = '月雑損失';
$lang['lbl_total_miscellaneous_income']         = '月雑収入';
//#9696:End

$lang['btn_show_etc']                  = 'その他入金欄 表示';
$lang['btn_show_miscellaneous_loss']   = '雑損失欄 表示';
$lang['btn_show_miscellaneous_income'] = '雑収入欄 表示';
$lang['btn_download_csv']              = '勘定奉行用CSVダウンロード';
$lang['btn_show_1'] = '当月入金＝当月入金予定分 消し込み';
$lang['btn_show_2'] = '前受金＋当月入金＝当月入金予定分＋前月以前売掛金 消し込み';
$lang['btn_show_3'] = '当月入金＝当月入金予定分＋前月以前売掛金 消し込み';
$lang['btn_import'] = '入金情報のインポート';


$lang['lbl_edit_error']       = 'データ更新に失敗しました。';
$lang['lbl_edit_success']     = '更新しました';
$lang['error_can_not_blance'] = '請求額は前受金より大きいため、消し込み不可です。';

  ////////////
 // ERRORS //
////////////
$lang['error_is_integer'] = '%field%は整数で入力してください。';

  ////////////
 // DETAIL //
////////////

$lang['lbl_action']                = '処理';
$lang['lbl_client']                = 'クライアント';
$lang['lbl_status']                = 'ステータス';
$lang['lbl_s_code']                = 'Ｓコード';
$lang['lbl_name']                  = 'クライアント名'; //'社名';
$lang['lbl_date_return_money']     = '入金日';
$lang['lbl_name_bank']             = '振込元';
$lang['lbl_money_recived']         = '入金額';
$lang['lbl_comment']               = '備考';
$lang['lbl_total_money_recived']   = '入金額合計';
$lang['common_msg_no_data']        = 'データがありません。';
$lang['btn_order_manage']          = '申込書作成';
$lang['lbl_divide_money_recived']  = "入金分割";
$lang['lbl_total_money']           = '入金額合計';
$lang['lbl_divide_money']          = '金額';
$lang['lbl_divide_before_comment'] = '分割元備考';
$lang['lbl_divide_after_comment']  = '分割先備考';
$lang['lbl_b_devide']              = '入金分割';
$lang['msg_invalid_id']            = '無効なID';
$lang['msg_numeric_unsigned']      = '%field%はマイナスしてはいけないです。';
$lang['msg_status_payment']        = "入金額を入金額合計以下入力してください。";
$lang['lbl_u_comment']             = '備考';
$lang['lbl_u_update']              = '調整';
$lang['error_is_required']         = "%field%を入力して下さい。";
$lang['error_must_be_positive']    = '正の%field%を入力してください。';
$lang['error_9_length']            = '%field%は10桁以上の入力ができません。管理部に問い合わせてください。';
$lang['error_can_not_be_negative'] = '%field%はマイナスしてはいけないです。';


  ////////////
 // IMPORT //
////////////
$lang['lbl_please_select_file'] = 'アップロードファイルを選択してください。';
$lang['lbl_unsupported_file']   = 'ファイル種類が無効です。';
$lang['lbl_empty_file']         = 'ファイルのデータがありません。';
$lang['lbl_invalid_format']     = '入金情報インポートファイルが無効です。';
$lang['lbl_over_size']          = 'インポートファイル容量は最大１０ＭＢで選択してください。';

$lang['error_invalid_date']               = "日付が無効です。";
$lang['error_invalid_bank']               = "振込元が存在していません。";
$lang['error_amount_must_be_number']      = "入金額は数値で入力してください。";
$lang['error_amount_can_not_be_negative'] = "入金額はマイナスしてはいけないです。";
$lang['error_s_code_is_required']         = "Ｓコードを入力してください。";
$lang['error_s_code_is_not_exist']        = "Ｓコードが存在していません。";
$lang['error_amount_out_of_range']        = "金額値がオーバーしています。";
//#9694:Start
$lang['error_please_select_s_code']       = 'Ｓコードを選択してください。';
//#9694:End

$lang['lbl_import_error']      = 'インポートに失敗しました。';
$lang['lbl_import_success']    = 'インポートしました';

//#9547:Start
$lang['lbl_out_of_range']        = '%field%がオーバーしています。';
$lang['lbl_current_month_mount'] = '当月入金額';
$lang['error_not_exist']         = '%field%が存在していません。';
$lang['error_system_limit_range']       = 'システムで処理できる金額上限をオーバーしました。';
//#9547:End



