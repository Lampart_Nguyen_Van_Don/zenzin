<?php
//Search
$lang['lbl_status'] = '売上伝票ステータス';
$lang['lbl_adv_payment'] = '前受金案件';
$lang['lbl_sales_representative'] = '担当営業';
$lang['lbl_slip_number'] = '売上伝票番号';
$lang['lbl_company_name'] = 'クライアント社名';
$lang['lbl_code'] = 'コード';
$lang['lbl_branch'] = '枝';
$lang['lbl_delivery_date'] = '納品日';
$lang['lbl_order_regist_date'] = '受注登録日';
$lang['lbl_order_approval_date'] = '受注承認日';
$lang['lbl_billing_date'] = '請求日';
$lang['lbl_payment_date'] = '支払日';
$lang['lbl_kana'] = '（フリガナ）';
$lang['lbl_division'] = 'クライアント部署名';
$lang['lbl_business_unit'] = '事業部';
$lang['lbl_ni'] = 'に';
$lang['lbl_zipcode'] = '郵便番号';
$lang['lbl_address_1'] = '住所';
$lang['lbl_address_2'] = '住所２';
$lang['lbl_department'] = '部署';
$lang['lbl_customer'] = 'お客様担当者';
$lang['lbl_tel'] = 'TEL';
$lang['lbl_fax'] = 'FAX';
$lang['lbl_closing_date'] = '締め日';
$lang['lbl_payment_date'] = '支払日';
$lang['lbl_bill_closing_date']   = '請求締め日';
$lang['lbl_payment_expire_date'] = 'お支払い期限日';
$lang['lbl_total_untax_amount'] = '税別合計';
$lang['lbl_total_tax'] = '消費税';
$lang['lbl_total_tax_amount'] = '税込合計';
$lang['lbl_move'] = '移動';

//Table
$lang['lbl_bulk_approval'] = '一括承認';
$lang['lbl_batch_apply'] = '一括申請';
$lang['lbl_slip_create'] = '売伝作成';
$lang['lbl_action'] = '処理';
$lang['lbl_edit'] = '変更';
$lang['lbl_apply_client'] = '申込クライアント';
$lang['lbl_product'] = '商品名';
$lang['lbl_contents'] = '内容';
$lang['lbl_quantity'] = '数量';
$lang['lbl_tax'] = '税';
$lang['lbl_unit_price'] = '単価';
$lang['lbl_money_amount'] = '金額';
$lang['lbl_total'] = '合計';
$lang['lbl_invoice_date'] = '請求日';
$lang['lbl_order_quantity'] = '受注数量';
$lang['lbl_gross_unit_price'] = '原価税別単価';
$lang['lbl_total_amount'] = '確定原価税別合計';
$lang['lbl_adv_pay'] = '前受金';
$lang['lbl_confirmed_pay'] = '確定';
$lang['lbl_invoice_address'] = '請求書宛名';

//Search legend
$lang['lbl_num_exact'] = '(数字部分のみ入力、完全一致検索）';
$lang['lbl_like_search'] = '(あいまい検索）';
$lang['lbl_prefix_search'] = '(前方一致検索）';
$lang['lbl_date_prefix'] = '(Fromのみ指定の場合、前方一致検索　From・To指定で前方一致の範囲指定検索）';
$lang['lbl_date_specified'] = '(Fromのみ指定の場合、指定日のみ検索　From・To指定で範囲指定検索　年月or年月日指定）';

$lang['btn_create_sales'] = '売上伝票作成';
$lang['btn_delete_sales'] = '売上伝票削除';
$lang['btn_bulk_apply'] = '一括申請';
$lang['btn_approval_select_all'] = '一括承認選択';
$lang['btn_bulk_sales_approval'] = '一括売上伝票承認';
$lang['btn_unearned_split'] = '前受分割';
$lang['btn_confirm_split'] = '確定分割';
$lang['btn_view_slip'] = '売伝参照';
$lang['btn_spec_delete'] = '明細削除';
$lang['btn_undo'] = '戻す';
$lang['btn_exclude'] = '除外';
$lang['btn_save']   = '保存';

$lang['msg_delivery_date_within_same_month'] = '納品日の変更は月内で入力してください。';
$lang['msg_delivery_date_invalid'] = '納品日に入力されている値が不適切です。';
$lang['msg_delivery_date_empty'] = '納品日が未入力です。';
$lang['msg_same_s_code'] = '売上伝票は同一のＳコードを追加してください。';
$lang['msg_same_payment_billing_date'] = '売上伝票は請求日と支払日が同一の受注情報を追加してください。';
$lang['msg_same_delivery_date'] = '売上伝票は納品年月が同一の受注情報を追加してください。';
$lang['msg_not_same_parent_slip_number'] = '前受金分の売上伝票は一致していませんので確定分をまとめられません。';
$lang['msg_mix_adv_payment_type'] = '売上明細に前受金分以外が混じていますので売上伝票作成できません。';
$lang['msg_confirm_amount_not_included_all_adv_amount'] = '確定請求に漏れがあります。全て含めてください。';
$lang['msg_empty_edit'] = '変更はありません。';
$lang['msg_empty_create_sales_slip'] = '売上伝票を作成する明細を選んでください。';
$lang['msg_no_sales_slip_number'] = '売上伝票番号を追加してください。';
$lang['msg_empty_add_to_sales_slip'] = '追加したい明細にチェックを入れてください。';
$lang['msg_empty_bulk_apply'] = '一括申請対象はありません。';
$lang['msg_empty_bulk_approve'] = '一括承認対象はありません。';
$lang['msg_slip_number_not_exists'] = '売上伝票番号が存在していません。';
$lang['msg_sales_slip_status_cant_add'] = '売上伝票ステータスが「売上伝票作成中」か「売上伝票承認申請中」のみ追加可能です。';
$lang['msg_positive_number'] = '%field%マイナス数値を入力できません。';
$lang['msg_ss_add_edit_payment_billing_date_validate'] = '請求締め日＜お支払い期限日＜納品日で設定してください。';
$lang['msg_create_sales_report_successful'] = '売上伝票を作成しました。';
$lang['msg_update_sales_report_successful'] = '変更を保存しました。';
$lang['msg_delete_sales_report_successful'] = '売上伝票を削除しました。';
$lang['msg_approve_successful'] = '選択した売上伝票を承認しました。';
$lang['msg_sales_slip_accountant_is_closed'] = '経理締めが終わりましたので保存できません。ページを更新して、最新の情報をご確認ください。';
$lang['msg_num_max_length'] = '%field%は%length%桁以上の入力ができません。管理部に問い合わせてください。';
$lang['msg_str_max_length'] = '%field%入力可能文字数は%length%文字です。';
//#6900:Start
$lang['msg_order_is_change_report'] = '変レポ中なので入力できません';
//#6900:End
//#7769:Start
$lang['msg_sales_closing_date_different'] = '他の売上明細の締日と異なります。一度売上伝票を削除して、再作成して下さい。';
//#7769:End
//#10119:Start
$lang['msg_tax_type_invalid'] = '選択した税区分は無効です。';
//#10119:End

//Start view language
$lang['lbl_sales_slip_number'] = '売上伝票番号';
$lang['lbl_sales_slip_status'] = '売上伝票ステータス';
$lang['lbl_j_account_id'] = 'Ｊ担当営業';
$lang['lbl_s_account_id'] = 'Ｓ担当営業';
$lang['lbl_j_code'] = 'Ｊコード';
$lang['lbl_s_code'] = 'Ｓコード';
$lang['lbl_contract'] = 'お客様担当者';
//$lang['lbl_billing_date'] = '請求締め日';
//$lang['lbl_payment_date'] = 'お支払い期限日';
$lang['lbl_download_pdf'] = '売上伝票PDFダウンロード';
$lang['lbl_sales_slip_approval_rejection'] = '売上伝票承認/却下';
$lang['lbl_sales_slip_approval'] = '売上伝票承認';
$lang['lbl_sales_slip_rejection'] = '売上伝票承認却下';
$lang['lbl_sales_slip_comment'] = '承認/却下コメント';
$lang['btn_sales_slip_approval'] = '売上伝票承認申請';
$lang['btn_sales_slip_pdf_download'] = '売上伝票PDFダウンロード';
$lang['lbl_amount_total'] = '税別合計';
$lang['lbl_tax_total']    = '消費税';
$lang['lbl_amount_tax_total']    = '税込合計';
$lang['lbl_yen'] = '円';
$lang['lbl_no_data_available'] = "データがありません。";
//End view language

// Order acceptance
$lang['lbl_order_acceptance_view'] = '検収情報参照';
$lang['lbl_total_taxes_not_determine'] = '確定原価税別合計';
