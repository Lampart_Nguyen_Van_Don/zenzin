<?php
$lang['lbl_search'] = "表示";
$lang['lbl_search_from_to'] = "対象年月";
$lang['lbl_search_division'] = "事業部名";

$lang['lbl_search_from_to_description'] = "対象年月";
$lang['lbl_print_button_description'] = "前渡金繰越計算表一括印刷";

$lang['name'] = "氏名";
$lang['department'] = "部署";
$lang['target'] = "目標粗利額";

$lang['error_from_date_invalid'] = "対象年月は無効な日付です";

//Sales Report
$lang['lbl_target_date']         = '対象年月';
$lang['lbl_department_name']     = '事業部名';
$lang['lbl_by_account']         = 'アカウント別';
$lang['lbl_by_department']         = '部署別';
$lang['lbl_by_business']         = '事業部別';

$lang['col_department_name']             = '部署名';
$lang['col_account_name']                 = '部署名';
$lang['col_business_unit_name']         = '事業部';
$lang['col_reponsible_employee_name']     = '担当社員名';
$lang['col_target']                     = '目標';
$lang['col_monthly_report']             = '月報';
$lang['col_achivement_rate']             = '達成率(%)';
$lang['col_last_month_profit']             = '前月精算アップ';
$lang['col_last_month_summary']         = '前月納品計上分';
$lang['col_cancel']                     = 'キャン/前倒し';
$lang['col_current_month_summary']         = '今月計上分';
$lang['col_last_month_report']             = '前月末時点月報';
$lang['col_sales']                         = '売上';

$lang['error_message_time_format']         	= '対象年月は無効な日付です';
$lang['error_empty_date']         			= '対象年月が必要です';

$lang['target'] = '営業目標設定';
$lang['report'] = '営業月報';