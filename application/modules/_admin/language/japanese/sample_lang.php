<?php

$lang['code']= '受注商品コード';
$lang['business_unit']= '事業部名';
$lang['name_product']= '受注商品名';
$lang['is_no_add']= '課税/非課税';

$lang['message_sure_to_delete'] = '削除します。よろしいですか？';
$lang['error_code_null']= '受注商品コードは必須項目です。';
$lang['error_code_max_length']= '受注商品コードは 5 文字を超えてはいけません。';
$lang['error_code_unique']= '受注商品コードは既に使用されています';
$lang['error_business_unit_null']= '事業部名は必須項目です。';
$lang['error_business_unique']= '事業部名は既に使用されています';
$lang['error_name_product_null']= '受注商品名は必須項目です。';
$lang['error_is_no_add_null']= '課税/非課税は必須項目です。';

// Deparment
$lang['error_null_department_name']         	  = '名称は必須項目です。';
$lang['error_null_zipcode']         		  	  = '郵便番号が無効です。';
$lang['error_null_address1']         		  	  = '住所1は必須項目です。';
$lang['error_null_tel']         		  	      = 'TELは必須項目です。';
$lang['error_null_fax']         		  	      = 'FAXは必須項目です。';

$lang["common_lbl_business_unit"] = "郵便番号";
$lang["common_lbl_department_name"] = "名称";
$lang["common_lbl_action"] = "処理";
$lang["common_lbl_copy"] = "コピー";
$lang["common_lbl_department_zipcode"] = "郵便番号";
$lang["common_lbl_department_address1"] = "住所1";
$lang["common_lbl_department_address2"] = "住所2";

$lang["common_detail_department"] = "部署情報";
$lang["common_listof_department"] = "部署一覧";

$lang["common_edit_department"] = "部署編集";
$lang["common_copy_department"] = "部署コピー";
$lang["common_add_department"] = "部署追加";