<?php

$lang['lbl_business']        = '事業部名';
$lang['lbl_action']          = '処理	';
$lang['lbl_sequence']        = 'ソート順';
$lang['lbl_code']            = '発送種別コード';
$lang['lbl_name']            = '発送種別名';
$lang['lbl_is_remaining_confirm'] = '残部確認対象';
$lang['lbl_is_advance_payment']     = '前渡金対象';
$lang['lbl_reporting']       = '作業完了報告対象';
$lang['lbl_is_display']      = '新規入力';
$lang['btn_add']             = '追加	';
$lang['lbl_view']            = '参照';

$lang['lbl_is_remaining_confirm_flag'] = '残部確認対象フラグ';
$lang['lbl_is_advance_payment_flag']   = '前渡金対象フラグ';
$lang['lbl_reporting_flag']            = '作業完了報告対象フラグ';
$lang['lbl_is_display_flag']           = '新規入力不可フラグ';
$lang['btn_edit']                      = '編集';
$lang['btn_close']                     = '閉じる';
$lang['lbl_is_no_add']                = '新規入力不可';

$lang['lbl_edit_error']     = 'データ更新に失敗しました。';
$lang['lbl_edit_success']   = '更新しました';
$lang['lbl_add_error']      = 'データ追加に失敗しました。';
$lang['lbl_add_success']    = '追加しました';
$lang['lbl_delete_success'] = '削除は成功しました';
$lang['lbl_delete_error']   = '削除は失敗しました';

$lang['btn_save']   = '保存';
$lang['btn_back']   = '一覧へ戻る';
$lang['btn_delete'] = '削除';

$lang['error_code_required']       = '発送種別コードが必須項目です';
$lang['error_name_required']       = '発送種別名を入力してください。';
$lang['error_name_max_length']     = '発送種別名は最大128桁';
$lang['error_sequence_required']   = 'ソート順を入力してください。';
$lang['error_sequence_max_length'] = 'ソート順は最大5桁';
$lang['error_sequence_numeric']    = '数値を入力して下さい。';

$lang['lbl_invalid_input'] = '入力した値が無効です';
$lang['lbl_invalid_input_minus'] = "ソート順は最大5桁";

$lang['lbl_delete_in_use_error'] = 'この発送種別は現在使用されています。';