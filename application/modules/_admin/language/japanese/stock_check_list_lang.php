<?php

//label
$lang['lbl_delivery_date'] = '発送日';
$lang['lbl_delivery_date_description'] = '(From・To必須　範囲指定検索）';

//button
$lang['lbl_print'] = '印刷';

//error msg
$lang['delivery_date_from_required'] = '発送日Fromは必須項目です。';
$lang['delivery_date_to_required'] = '発送日Toは必須項目です。';
$lang['delivery_date_from_invalid'] = '発送日Fromが無効です。';
$lang['delivery_date_to_invalid'] = '発送日Toが無効です。';
$lang['delivery_date_range_invalid'] = '検索範囲が正しくありません。';
$lang['no_data_found'] = 'データがありません。';
