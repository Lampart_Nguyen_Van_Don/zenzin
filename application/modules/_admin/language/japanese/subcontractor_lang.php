<?php

//label
$lang['lbl_form_title']         = '検索条件';
$lang['lbl_detail_form_title']  = 'ブレーン情報';
$lang['lbl_add_form_title']     = 'ブレーン追加';
$lang['lbl_edit_form_title']     = 'ブレーン編集';
$lang['lbl_code']               = 'ブレーンコード';
$lang['lbl_name']               = 'ブレーン名';
$lang['lbl_abbr_name']          = 'ブレーン略称';
$lang['lbl_subcontractor_type'] = '請求元ブレーンフラグ';
$lang['lbl_direct_consignment'] = '請求元ブレーン';
$lang['lbl_is_no_add']         = '新規入力不可フラグ';
$lang['lbl_is_no_add_checkbox'] = '新規入力不可';
$lang['lbl_action']             = '処理';
$lang['lnk_edit']               = '編集';
$lang['lbl_parent_subcontractor_relation']  = '親ブレーン';
$lang['lbl_parent_subcontractor']     = '請求元ブレーン';
$lang['lbl_choosen_parent_subcontractor']  = '選択済み親ブレーン';
$lang['lbl_child_subcontractor_relation']  = '子ブレーン';
$lang['lbl_child_subcontractor']     = '再委託先ブレーン';
$lang['lbl_choosen_child_subcontractor']  = '選択済み子ブレーン';
$lang['code'] = 'コード';

//options
$lang['default_consignment'] = '全て';
$lang['direct_consignment'] = ' 請求元ブレーン';
$lang['indirect_consignment'] = '再委託先';
$lang['is_no_add'] = '新規入力';
$lang['is_no_add_symbol'] = '不可';
$lang['is_add_symbol'] = '可';
//button
$lang['btn_view']    = '参照';
$lang['btn_add']    = '追加';
$lang['btn_search'] = '検索';
$lang['btn_close']  = '閉じる';
$lang['btn_delete']  = '削除';
$lang['btn_save']  = '保存';

//description
$lang['description_for_code'] = '(前方一致検索）';
$lang['description_for_name'] = '(あいまい検索）';
$lang['description_for_abbr_name'] = '(あいまい検索）';

//validation
$lang['require'] = '%label%は必須項目です。';
$lang['min_character'] = '%label% is minium %min% characters';
$lang['max_character'] = '%label%は%max%文字を超えてはいけません。';
$lang['min_max_character'] = '%label% は半角英数字%min%～%max%桁、英字は大文字のみで入力してください。(※記号は不可です)';
$lang['[0-9A-Z]'] = '%label% must only have numbers [0-9] and uppercase [A-Z] characters。';
$lang['min_max_[0-9A-Z]'] = '%label% は半角英数字%min%～%max%桁、英字は大文字のみで入力してください。(※記号は不可です)';
$lang['unique'] = '%label% は既に存在しています。';
$lang['invalid_data'] = '%label% が無効です';
//message
$lang['error_title'] = 'エラー';
$lang['no_data'] = 'データ無し';
$lang['subcontractor_not_exist_error_msg'] = 'ブレーンが存在しません。';
$lang['system_error_msg'] = 'System error, please try again';
$lang['add_error_msg'] = 'ブレーン追加は失敗しました。';
$lang['add_success_msg'] = 'ブレーンを追加しました。';
$lang['update_error_msg'] = 'ブレーン追加は失敗しました。';
$lang['update_success_msg'] = 'ブレーン更新は成功しました。';
$lang['add_subcontractor_relation_error_msg'] = 'ブレーン紐付追加は失敗しました。';
$lang['update_subcontractor_relation_error_msg'] = 'ブレーン紐付更新は失敗しました。';
$lang['delete_subcontractor_error_msg'] = 'ブレーンを削除できませんでした。';
$lang['delete_subcontractor_relation_error_msg'] = 'ブレーン紐付で削除できませんでした。';
$lang['cannot_delete_error_msg'] = "このブレーンは現在使用されています。";
$lang['error_attr_name_max_length'] = " ブレーン略称は64文字を超えてはいけません。 ";
$lang['cannot_delete_parent_error_msg'] = "子ブレーンが存在していますので削除できません。";
$lang['parent_subcontractor_is_required'] = "親ブレーンは必須項目です。";