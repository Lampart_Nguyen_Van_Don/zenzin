{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

<div class="cell content slide-right">
    <div class="content-change-password">
        <h1 class="page-title">{lang('lbl_change_password')}</h1>

        <form action="" method="post">
            <div class="form">
                {if isset($err_msg)}
                    <span class="error-message">{$err_msg}</span>
                {/if}

                <table class="table-simple">
                    <tr>
                        <td>{lang('lbl_new_password')}<span class="txt-red">※</span></td>
                        <td>
                            <input type="password" style="display: none" /><!--Off remember password of browser-->
                            <input name="new_password" type="password" size="30" value="{if isset($form.new_password)}{$form.new_password}{/if}" autocomplete="off" />
                            {form_error('new_password')}
                        </td>
                    </tr>
                    <tr>
                        <td>{lang('lbl_confirm_password')}<span class="txt-red">※</span></td>
                        <td>
                            <input name="confirm_password" type="password" size="30" value="{if isset($form.confirm_password)}{$form.confirm_password}{/if}" autocomplete="off" />
                            {form_error('confirm_password')}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="caution">パスワードの注意事項
                                <p>・大小英字と数字を必ず混在させてください</p>
                                <p>例）aBc012xyZ</p>
                                <p>・桁数は8桁以上としてください</p>
                                <p>・現在のパスワードと、一つ前に使用していたパスワードは使用できません</p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <a href="/_admin/home" class="btn-small btn-green btn-close">{lang('common_lbl_cancel')}</a>
                            <button type="submit" class="btn-small btn-green btn-save">{lang('lbl_change_password')}</button>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </div>
</div>

{if isset($change_password_success) && $change_password_success}
    <div id="dialog_change_password" title="{lang('lbl_change_password')}">
        パスワード変更完了しました。システムは自動的にログアウトします。
        <br/>
        再度ログインして、利用してください。
    </div>

    {literal}
        <script type="text/javascript">
            $(document).ready(function() {
                $("#dialog_change_password").dialog({
                    height: 'auto', 
                    width: 500, 
                    modal: true,
                    buttons: {
                        "OK": {
                            click: function () {
                                $(this).dialog("close");
                            },
                            text: 'OK',
                            'class': 'close-dialog'
                        }
                    },
                    close: function() {
                        window.location.href = '/_admin/home';
                    },
                    open: function() {
                        $('.ui-dialog-buttonpane').addClass('button-dialog');
                    }
                });
            });
        </script>
    {/literal}
{/if}

{include file = "inc/footer.tpl"}