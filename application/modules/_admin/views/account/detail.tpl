<div>
    <table class="table-theme">
        <tr>
            <td>{lang('lbl_department_name')}</td>
            <td>
                <input type="text" value="{html_escape($account.business_name|cat:$account.department_name)}" disabled>
            </td>
        </tr>
        <tr>
            <td>{lang('lbl_name')}</td>
            <td><input type="text" value="{html_escape($account.name)}" disabled></td>
        </tr>
        <tr>
            <td>{lang('lbl_login_id')}</td>
            <td><input type="text" value="{$account.login_id}" disabled></td>
        </tr>
        <tr>
            <td>{lang('lbl_email')}</td>
            <td><input type="text" value="{$account.mail_address}" disabled></td>
        </tr>
        <tr>
            <td>{lang('lbl_resign_flag')}</td>
            <td>
                <label>
                    <input type="checkbox" value="1" {if $account.resignation}{'checked'}{/if} disabled> {lang('lbl_resign')}
                </label>
            </td>
        </tr>
        <tr>
            <td>{lang('lbl_authority_name')}</td>
            <td><input type="text" value="{$account.authority_name}" disabled></td>
        </tr>
        <tr>
            <td colspan="2">
                {if $auth_accessible}
                <button type="button" class="btn-small btn-green btn-edit edit-account" data-account-id="{$account.id}">{lang('btn_edit')}</button>
                <button type="button" class="btn-small btn-green copy-account" data-account-id="{$account.id}">{lang('btn_copy')}</button>
                {/if}
                <button type="button" class="btn-small btn-green btn-close close-dialog">{lang('btn_close')}</button>
            </td>
        </tr>
    </table>
</div>

{literal}
<script>
    $(".edit-account").on('click', function(event) {
        account_id = $(this).data('account-id');
        $.ajax({
            url: '/_admin/account/ajax_edit',
            type: 'POST',
            data: {account_id: account_id},
        })
        .done(function(data) {
            if (typeof data == 'object' && data.error_code == 400) {
                $("#dialog-edit").html('<div class="error-message">'+data.messages+'</div>');
                $("#dialog-edit").dialog({
                    title: 'アカウント管理',
                    width: 'auto',
                    height: 'auto',
                    modal : true,
                });
                return;
            }

            $("#dialog-edit").html(data);
            $("#dialog-edit").dialog({
                title: 'アカウント管理',
                width: 'auto',
                height: 'auto',
                modal: true,
                beforeClose: function( event, ui ) {
                    var close = close_confirm();
                    return close;
                },
//                 close: function() {
//                     $(window).unbind('beforeunload');
//                 }
            })
        })
    });

    $(".copy-account").on('click', function(event) {
        account_id = $(this).data('account-id');
        $.ajax({
            url: '/_admin/account/ajax_edit',
            type: 'POST',
            data: {account_id: account_id, is_copy: 1},
        })
        .done(function(data) {
            if (typeof data == 'object' && data.error_code == 400) {
                $("#dialog-edit").html('<div class="error-message">'+data.messages+'</div>');
                $("#dialog-edit").dialog({
                    title: 'アカウント管理',
                    width: 'auto',
                    height: 'auto',
                    modal : true,
                });
                return;
            }

            $("#dialog-edit").html(data);
            $("#dialog-edit").dialog({
                title: 'アカウント管理',
                width: 'auto',
                height: 'auto',
                modal: true,
                beforeClose: function( event, ui ) {
                    var close = close_confirm();
                    return close;
                },
            })
        })
    });

</script>
{/literal}