<div>
    <p class="messages"></p>
    {form_open(null, "id='edit-form'")}
        {if !$is_copy}
        <input type="hidden" name="account_id" value="{$account.id}">
        {/if}
        <table class="table-theme">
            <tr>
                <td>{lang('lbl_department_name')}<span class="red-symbol">※</span></td>
                <td>
                    <select name="department_id">
                        {foreach $departments as $department}
                        <option value="{$department.department_id}" {if $department.department_id == $account.department_id}{'selected'}{/if}>{html_escape($department.business_unit_name)}{html_escape($department.department_name)}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_name')}<span class="red-symbol">※</span></td>
                <td>
                    <input name="name" type="text" value="{html_escape($account.name)}">
                    <p id="edit-name-error"></p>
                </td>
            </tr>
            <tr>
                <td>
                    {lang('lbl_login_id')}
                    {if $is_copy}<span class="red-symbol">※</span>{/if}
                </td>
                <td>
                    <input name="login_id" type="text" value="{$account.login_id}" {if !$is_copy}{'disabled'}{/if}>
                    {if $is_copy}
                    <p id="edit-login-id-error"></p>
                    {/if}
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_email')}<span class="red-symbol">※</span></td>
                <td>
                    <input name="mail_address" type="text" value="{$account.mail_address}">
                    <p id="edit-mail-address-error"></p>
                </td>
            </tr>
            <tr>
                <td>
                    {lang('lbl_password')}
                    {if $is_copy}<span class="red-symbol">※</span>{/if}
                </td>
                <td>
                    <input name="password" type="password">
                    <p id="edit-password-error"></p>
                </td>
            </tr>
            <tr>
                <td>
                    {lang('lbl_confirm_password')}
                    {if $is_copy}<span class="red-symbol">※</span>{/if}
                </td>
                <td>
                    <input name="confirm_password" type="password">
                    <p id="edit-confirm-password-error"></p>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_resign_flag')}</td>
                <td>
                    <label>
                        <input name="resignation" type="checkbox" value="1" {if $account.resignation}{'checked'}{/if}> {lang('lbl_resign')}
                        <p id="edit-resignation-error"></p>
                    </label>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_authority_name')}<span class="red-symbol">※</span></td>
                <td>
                    <select name="authority_id" {if $this->auth->get_account_id() == $account.id && !$is_copy}{'disabled'}{/if}>
                        {foreach $authorities as $authority}
                        <option value="{$authority.id}" {if $authority.id == $account.authority_id}{'selected'}{/if}>{$authority.name}</option>
                        {/foreach}
                    </select>
                    {if $this->auth->get_account_id() == $account.id}
                    <input name="authority_id" type="hidden" value="{$account.authority_id}">
                    {/if}
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button type="button" class="btn-small btn-green btn-save {if !$is_copy}{'update-account'}{else}{'copy-add-account'}{/if}">{if !$is_copy}{lang('btn_save')}{else}{lang('btn_add')}{/if}</button>
                    <button type="button" class="btn-small btn-green btn-close close-dialog">{lang('btn_close')}</button>
                    {if $this->auth->get_account_id() != $account.id && !$is_copy}
                    <button type="button" class="btn-small btn-green btn-delete delete-account" data-account-id="{$account.id}">{lang('btn_delete')}</button>
                    {/if}
                </td>
            </tr>
        </table>
    {form_close()}
</div>
<script>
    is_copy = {if $is_copy} true {else} false {/if};
</script>
{literal}
<script>
    $(".update-account").on('click', function(event) {
        $.ajax({
            url: '/_admin/account/ajax_update',
            type: 'POST',
            data: $("#edit-form").serialize(),
        })
        .done(function(data) {
            $(".messages").html("");
            if (typeof data.error_code !== 'undefined' && data.error_code == 400) {
                $("#dialog-edit").html('<div class="error-message">'+data.messages+'</div>');
                $("#dialog-edit").dialog({
                    title: 'アカウント管理',
                    width: 'auto',
                    height: 'auto',
                    modal : true,
                });
                return;
            }

            if (data.status) {
                form_changed = false;
                form_submitted = true;
                $(".messages").html("<div class='success-message blink_me'>"+data.messages+"</div><br><br>");
                $(".messages").addClass('alert-success');
                $(".ui-dialog-titlebar-close").addClass('back-to-show');
                $(".btn-close").addClass('back-to-show');
                $("#edit-name-error, #edit-password-error, #edit-confirm-password-error, #edit-mail-address-error, #edit-resignation-error").html('');
            } else {
                if (typeof data.messages == 'object') {
                    $("#edit-name-error").html(data.messages.name);
                    $("#edit-password-error").html(data.messages.password);
                    $("#edit-confirm-password-error").html(data.messages.confirm_password);
                    $("#edit-mail-address-error").html(data.messages.mail_address);
                    $("#edit-resignation-error").html(data.messages.resignation);
                } else {
                    $(".messages").text("<div class='error-message'>"+data.messages+"</div>");
                    $(".messages").addClass('alert-error');
                }
            }

        });
    });

    $(".copy-add-account").on('click', function(event) {
        $.ajax({
            url: '/_admin/account/ajax_add',
            type: 'POST',
            data: $("#edit-form").serialize(),
        })
        .done(function(data) {
            $(".messages").html("");
            if (typeof data.error_code !== 'undefined' && data.error_code == 400) {

                $("#dialog-edit").html('<div class="error-message">'+data.messages+'</div>');
                $("#dialog-edit").dialog({
                    title: 'アカウント管理',
                    width: 'auto',
                    height: 'auto',
                    modal : true,
                });
                return;
            }

            if (data.status) {
                form_changed = false;
                form_submitted = true;
                $(".messages").html("<div class='success-message blink_me'>"+data.messages+"</div><br><br>");
                $(".messages").addClass('alert-success');
                $(".btn-close").addClass('back-to-show');
                $(".ui-dialog-titlebar-close").addClass('back-to-show');
                $("#edit-login-id-error, #edit-name-error, #edit-password-error, #edit-confirm-password-error, #edit-mail-address-error, #edit-resignation-error").html('');
            } else {
                if (typeof data.messages == 'object') {
                    $("#edit-login-id-error").html(data.messages.login_id);
                    $("#edit-name-error").html(data.messages.name);
                    $("#edit-password-error").html(data.messages.password);
                    $("#edit-confirm-password-error").html(data.messages.confirm_password);
                    $("#edit-mail-address-error").html(data.messages.mail_address);
                    $("#edit-resignation-error").html(data.messages.resignation);
                } else {
                    $(".messages").html("<div class='error-message'>"+data.messages+"</div>");
                    $(".messages").addClass('alert-error');
                }
            }
        });
    });

    $(".delete-account").on('click', function(event) {
        account_id = $(this).data('account-id');

        $("#dialog-confirm").html('アカウントを削除しますか?').dialog({
            title: '確認',
            width: 'auto',
            height: 'auto',
            modal : true,
            buttons: {
                "はい": function() {
                     $.ajax({
                        url: '/_admin/account/ajax_delete',
                        type: 'POST',
                        data: {account_id: account_id},
                    })
                    .done(function(data) {
                        $(".messages").html("");
                        if (!data.status) {
                            $(".messages").html('<div class="error-message">'+data.messages+'</div>');
                            $("#dialog-confirm").dialog('close');
                            return;
                        }

                        dom =   "<form method='post' action='/_admin/account/show'>"+
                                    "<input type='hidden' name='pg' value='"+$("#page").val()+"'>"+
                                    "<input type='hidden' name='business_unit_id' value='"+$("#cbx-business-unit").val()+"'>"+
                                    "<input type='hidden' name='department_id' value='"+$("#cbx-department").val()+"'>"+
                                    "<input type='hidden' name='name' value='"+$("#name").val()+"'>"+
                                "</form>";
                        $(dom).appendTo('body').submit();
                    })
                },
                "いいえ": function () {
                    $(this).dialog('close');
                }
            }
        });
    });


    bind_edit_event = function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            if (is_copy) {
                $('.copy-add-account').trigger('click');
            } else {
                $('.update-account').trigger('click');
            }
            return false;
        }
    }

    $(window).bind('keydown', bind_edit_event);

    exit_confirm();
</script>
{/literal}
