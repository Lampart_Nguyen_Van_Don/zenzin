{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>
        <div class="search-container">
            {form_open("_admin/account/show", "class=search_form")}
                <table class="table-simple">
                    <tr>
                        <th>{lang('lbl_business_name')}</th>
                        <td>
                            <select id="cbx-business-unit" name="business_unit_id" style="min-width: 150px;" class="max-width">
                                <option value="-1">{lang('common_lbl_all')}</option>
                                {foreach $business_units as $business_unit}
                                <option value="{$business_unit.id}" {if isset($form.business_unit_id) && $business_unit.id == $form.business_unit_id}{'selected'}{/if}>{html_escape($business_unit.name)}</option>
                                {/foreach}
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_department_name')}</th>
                        <td>
                            <select id="cbx-department" name="department_id" style="min-width: 150px;" class="max-width">
                                {if !isset($departments) || empty($departments)}
                                <option value="-1">{lang('common_lbl_all')}</option>
                                {else}
                                {foreach $departments as $department}
                                <option value="{$department.id}" {if isset($form.department_id) && $department.id == $form.department_id}{'selected'}{/if}>{html_escape($department.name)}</option>
                                {/foreach}
                                {/if}
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_name')}</th>
                        <td>
                            <input id="name" type="text" name="name" value="{if isset($form.name)}{$form.name}{/if}">
                            <p class="search-tooltip">{lang('lbl_name_tooltip')}</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button type="submit" class="btn-small btn-green btn-search">{lang('btn_search')}</button>
                        </td>
                    </tr>
                </table>
            {form_close()}
        </div>
        <br>
        <h1></h1>

            <table class="table-theme odd">
                <thead>
                    <tr>
                        <th>{lang('lbl_action')}</th>
                        <th>{lang('lbl_department_name')}</th>
                        <th>{lang('lbl_name')}</th>
                    </tr>
                </thead>
                <tbody>
                    {if empty($accounts)}
                    <tr><td colspan="3">{lang('common_lbl_no_data')}</td></tr>
                    {else}
                    {foreach $accounts as $account}
                    <tr>
                        <td><button type="button" class="btn-small btn-green btn-view view-account" data-account-id="{$account.id}">{lang('lbl_view')}</button></td>
                        <td>{html_escape($account.business_name)}{html_escape($account.department_name)}</td>
                        <td>{html_escape($account.name)}</td>
                    </tr>
                    {/foreach}
                    {/if}

                    {if $auth_accessible}
                    <tr>
                        <td colspan="3">
                            <button class="btn-small btn-green btn-add">{lang('btn_add')}</button>
                            <button class="btn-small btn-green btn-export">{lang('btn_export_csv')}</button>
                        </td>
                    </tr>
                    {/if}
                </tbody>
            </table>

            <input id="page" type="hidden" value="{if isset($form.pg)}{$form.pg}{else}{1}{/if}">
            {if $pagination}
                {$pagination.links}
            {/if}
    </div>
<div id="dialog-edit" style="display:none"></div>
<div id="dialog-add" style="display:none">
    <p class="messages"></p>
    {form_open(null, "id='add-form'")}
        <table class="table-theme">
            <tr>
                <td>{lang('lbl_department_name')}<span class="red-symbol">※</span></td>
                <td>
                    <select id="add-cbx-department-id" name="department_id">
                        <option value="">{lang('common_please_select')}</option>
                        {foreach $business_departments as $department}
                        <option value="{$department.department_id}">{html_escape($department.business_unit_name)}{html_escape($department.department_name)}</option>
                        {/foreach}
                    </select>
                    <p id="add-department-error"></p>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_name')}<span class="red-symbol">※</span></td>
                <td>
                    <input name="name" type="text" value="">
                    <p id="add-name-error"></p>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_login_id')}<span class="red-symbol">※</span></td>
                <td>
                    <input name="login_id" type="text" value="">
                    <p id="add-login-id-error"></p>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_email')}<span class="red-symbol">※</span></td>
                <td>
                    <input name="mail_address" type="text" value="">
                    <p id="add-mail-address-error"></p>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_password')}<span class="red-symbol">※</span></td>
                <td>
                    <input name="password" type="password">
                    <p id="add-password-error"></p>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_confirm_password')}<span class="red-symbol">※</span></td>
                <td>
                    <input name="confirm_password" type="password">
                    <p id="add-confirm-password-error"></p>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_resign_flag')}</td>
                <td>
                    <label>
                        <input name="resignation" type="checkbox" value="1"> {lang('lbl_resign')}
                        <p id="add-resignation-error"></p>
                    </label>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_authority_name')}<span class="red-symbol">※</span></td>
                <td>
                    <select id="add-cbx-authority-id" name="authority_id">
                        <option value="">{lang('common_please_select')}</option>
                        {foreach $authorities as $authority}
                        <option value="{$authority.id}">{$authority.name}</option>
                        {/foreach}
                    </select>
                    <p id="add-authority-error"></p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button type="button" class="btn-small btn-green btn-save add-account">{lang('btn_add')}</button>
                    <button type="button" class="btn-small btn-green btn-close close-dialog">{lang('btn_close')}</button>
                </td>
            </tr>
        </table>
    {form_close()}
</div>
<div id="dialog-confirm" style="display:none"></div>
</div>

<script>
    label = {
        select_all: "{lang('common_lbl_all')}"
    };
</script>
{literal}
<script>
    $(document).ready(function() {
        $(".search_form :input").change(function() {
            $(window).unbind('beforeunload');
        });
    });
    $(".btn-search").on('click', function() {
        form_changed = false;
        form_submitted = true;
        $(".search_form").submit();
    });

    // initialize data
    var escapeHTML = function(string) {
        htmlEscaper = /[&<>"'\/]/g;

        htmlEscapes = {
                        '&': '&amp;',
                        '<': '&lt;',
                        '>': '&gt;',
                        '"': '&quot;',
                        "'": '&#x27;',
                        '/': '&#x2F;'
                    };

        return string.replace(htmlEscaper, function(match) {
            return htmlEscapes[match];
        });
    };

    $("#cbx-business-unit").on('change', function(event) {
        $("#cbx-department").html('');

        business_unit_id = $(this).val();

        if (business_unit_id == -1) {
            $("#cbx-department").append("<option value='-1'>" + label.select_all + "</option>");
//             return;
        } else {

            $("#cbx-department").attr('disabled', 'disabled');

            $.ajax({
                url: '/_admin/account/ajax_get_department',
                type: 'POST',
                data: {business_unit_id: business_unit_id}
            })
            .done(function(data) {
                $("#cbx-department").removeAttr('disabled');
                if (data == 0) {
                    dom = "<option value='-1'>"+label.select_all+"</option>";
                    $("#cbx-department").append(dom);
                    return;
                }

                dom = "";
                dom = "<option value='-1'>"+label.select_all+"</option>";
                $.each(data, function(index, department) {
                    dom += "<option value='" + department.id + "'>" + escapeHTML(department.name) + "</option>";
                });
                $("#cbx-department").append(dom);

            })
        }
    });

    $(".view-account").on('click', function(event) {
        account_id = $(this).data('account-id');
        $.ajax({
            url: '/_admin/account/ajax_get_detail',
            type: 'POST',
            data: {account_id: account_id}
        })
        .done(function(data) {

            if (typeof data == 'object' && data.error_code == 400) {
                $("#dialog-edit").html('<div class="error-message">'+data.messages+'</div>');
                $("#dialog-edit").dialog({
                    title: 'アカウント管理',
                    width: 'auto',
                    height: 'auto',
                    modal : true
                });
                return;
            }

            $("#dialog-edit").html(data);
            $("#dialog-edit").dialog({
                title: 'アカウント管理',
                width: 'auto',
                height: 'auto',
                modal : true,
                open: function() {
                    $(window).unbind('keydown', bind_add_event);
                },
                close: function() {
                    if (typeof bind_edit_event != 'undefined') {
                        $(window).unbind('keydown', bind_edit_event);
                    }

                    if (typeof bind_add_event != 'undefined') {
                        $(window).bind('keydown', bind_add_event);
                    }
                }
            });
        })
    });

    $(".btn-add").on('click', function(event) {
        $(".messages, #add-department-error, #add-name-error, #add-login-id-error, #add-mail-address-error, #add-password-error, #add-confirm-password-error, #add-resignation-error, #add-authority-error").html('');
        // RESET ALL INPUT DATA
        $("#add-form input, #add-form select").each(function(index, element) {
            switch ($(this).prop('tagName')) {
                case 'SELECT':
                    $(this).children().removeAttr('selected');
                    break;
                case 'INPUT':
                    if ($(this).attr('type') == 'checkbox') {
                        $(this).removeAttr('checked');
                    } else {
                        $(this).val('');
                    }
                    break;
            }
        });
        $("#dialog-add").dialog({
            title: 'アカウント管理',
            width: 'auto',
            height: 'auto',
            modal : true,
            beforeClose: function( event, ui ) {
                var close = close_confirm();
                return close;
            }
//             close: function() {
//                 $(window).unbind('beforeunload');
//             }
        });
    });

    $(".add-account").on('click', function(event) {
        $.ajax({
            url: '/_admin/account/ajax_add',
            type: 'POST',
            data: $("#add-form").serialize()
        })
        .done(function(data) {
            $(".messages").html("");
            if (typeof data.error_code !== 'undefined' && data.error_code == 400) {
                $("#dialog-add").html('<div class="error-message">'+data.messages+'</div>');
                $("#dialog-add").dialog({
                    title: 'アカウント管理',
                    width: 'auto',
                    height: 'auto',
                    modal : true
                });
                return;
            }

            if (data.status) {
                form_changed = false;
                form_submitted = true;
                $(".messages").html("<div class='success-message blink_me'>"+data.messages+"</div><br><br>");
                $(".messages").addClass('alert-success');
                $(".btn-close").addClass('back-to-show');
                $(".ui-dialog-titlebar-close").addClass('back-to-show');
                $("#add-login-id-error, #add-name-error, #add-password-error, #add-confirm-password-error, #add-mail-address-error, #add-resignation-error, #add-department-error, #add-authority-error").html('');
                // RESET ALL INPUT DATA
                $("#add-form input, #add-form select").each(function(index, element) {
                    switch ($(this).prop('tagName')) {
                        case 'SELECT':
                            $(this).children().removeAttr('selected');
                            break;
                        case 'INPUT':
                            if ($(this).attr('type') == 'checkbox') {
                                $(this).removeAttr('checked');
                            } else {
                                $(this).val('');
                            }
                            break;
                    }
                });
            } else {
                if (typeof data.messages == 'object') {
                    $("#add-login-id-error").html(data.messages.login_id);
                    $("#add-name-error").html(data.messages.name);
                    $("#add-password-error").html(data.messages.password);
                    $("#add-confirm-password-error").html(data.messages.confirm_password);
                    $("#add-mail-address-error").html(data.messages.mail_address);
                    $("#add-resignation-error").html(data.messages.resignation);
                    $("#add-department-error").html(data.messages.department);
                    $("#add-authority-error").html(data.messages.authority);
                } else {
                    $(".messages").html("<div class='error-message'>"+data.messages+"</div>");
                    $(".messages").addClass('alert-error');
                }
            }
        });

    })

    $(".btn-export").on('click', function(event) {
        dom =   "<form method='post' action='/_admin/account/export_csv' target='_blank'>"+
                    "<input type='hidden' name='department_id' value='"+$("#cbx-department").val()+"'>"+
                    "<input type='hidden' name='name' value='"+$("#name").val()+"'>"+
                "</form>";
        $(dom).appendTo('body').submit();
    });

    $("body").on('click', '.close-dialog', function(event) {
        $(".ui-dialog-content").dialog("close");
    });

    $('body').on('click', '.back-to-show', function(event) {
        if (!form_changed && $(".success-message").length > 0) {
            dom =   "<form method='post' action='/_admin/account/show'>"+
                        "<input type='hidden' name='pg' value='"+$("#page").val()+"'>"+
                        "<input type='hidden' name='business_unit_id' value='"+$("#cbx-business-unit").val()+"'>"+
                        "<input type='hidden' name='department_id' value='"+$("#cbx-department").val()+"'>"+
                        "<input type='hidden' name='name' value='"+$("#name").val()+"'>"+
                    "</form>";
            $(dom).appendTo('body').submit();
        }
    });
 // ENTER => SEARCH
    $(".table-simple").keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
         {
            e.preventDefault();
            if($("select[name='business_unit_id']").is(':focus')
                    | $("select[name='department_id']").is(':focus')
                    | $("input[name='name']").is(':focus')
            ) {
                // alert(133);
                $(".search_form").submit();
            }
         }
    }
    );

    var bind_add_event = function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            if ($("#dialog-add").is(":visible")) {
                $('.add-account').trigger("click");
            }
            return false;
        }
    }

    $(window).bind('keydown', bind_add_event);

    exit_confirm();
</script>
{/literal}

{include file = "inc/footer.tpl"}