<div>
    <div>
        <div id="message"></div>
        <hr class="blank-10px" />
        {if isset($account_title)}
        <table class="table-theme">
            <tr>
                <td width="">勘定科目名</td>
                <td>{htmlspecialchars ($account_title.name)}</td>
            </tr>
            <tr>
                <td>勘定科目コード</td>
                <td>{htmlspecialchars ($account_title.calculate_code)}</td>
            </tr>
            <tr>
                <td>補助科目コード</td>
                <td>{htmlspecialchars ($account_title.support_code)}</td>
            </tr>
            <tr>
                <td>摘要文字列</td>
                <td>{htmlspecialchars ($account_title.remarks)}</td>
            </tr>
            <tr>
                <td>部門コード</td>
                <td>{htmlspecialchars ($account_title.bugyo_dp_code)}</td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="float: left;">
                        <button name="{$account_title.id}" class = 'btn-small btn-green btn-edit' value="{$account_title.id}">{lang('common_lbl_edit')}</button>
                    </div>
                    <div style="float: left;">
                        <button name="close_detail_btn" id="close_detail_btn" class="btn-small btn-green btn-close" value="Close">{lang('common_lbl_close')}</button>
                    </div>
                </td>
            </tr>
        </table>
        {else}
            <div class="error-message">{lang('common_record_not_exist')}</div>
        {/if}
        <hr class="blank-10px" />
    </div>
</div>

{literal}
<script>
$(document).ready(function() {
    // Close detail c dialog box
    $(document).on('click', '#close_detail_btn', function() {
            $("#dialog_view_edit").dialog("close");
     });
    // Click edit in dialogbox
    $(".btn-edit").on('click', function() {
        $.ajax({
            url: "/_admin/account_title/edit",
            type: "POST",
            data: {
                id: this.value,
                type: "edit",
                redirect_form_post:redirect_data()
            }
        })
        .done(function(data) {
            $("#dialog_view_edit").html(data);
            $("#dialog_view_edit").dialog({
                title: "勘定奉行科目編集",
                width: 'auto',
                height: 'auto',
                modal: true,
                beforeClose: function( event, ui ) {
                    var close = close_confirm();
                    return close;
                },
                close: function() {
                    $(document).unbind('keydown', bind_edit_event);
                }
            })
            $('#dialog_view_edit').dialog("open");
        });
     });
}); // Close $(document).ready(function() {
</script>
{/literal}
