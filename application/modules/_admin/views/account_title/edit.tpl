<div>
    <div>
        <div id="message"></div>
        <hr class="blank-10px" />
        {if isset($account_title)}
        <table class="table-theme">
            <tr>
                <td width="">勘定科目名</td>
                <td> {htmlspecialchars ($account_title.name)}
                <!-- <input type="text" name="name" id="name" value="{htmlspecialchars ($account_title.name)}"/> -->
                <input type="hidden" name="id" id="id" value="{htmlspecialchars ($account_title.id)}"/>
                <div id="err_name"></div>
                </td>
            </tr>
            <tr>
                <td>勘定科目コード<span class="red-symbol">※</span></td>
                <td><input type="text" name="calculate_code" id="calculate_code" value="{htmlspecialchars ($account_title.calculate_code)}"/>
                <div id="err_calculate_code"></div>
                </td>
            </tr>
            <tr>
                <td>補助科目コード</td>
                <td><input type="text" name="support_code" id="support_code" value="{htmlspecialchars ($account_title.support_code)}"/>
                <div id="err_support_code"></div>
                </td>
            </tr>
            <tr>
                <td>摘要文字列<span class="red-symbol">※</span></td>
                <td><input type="text" name="remarks" id="remarks" value="{htmlspecialchars ($account_title.remarks)}"/>
                <div id="err_remarks"></div>
                </td>
            </tr>
            <tr>
                <td>部門コード<span class="red-symbol">※</span></td>
                <td><input type="text" name="bugyo_dp_code" id="bugyo_dp_code" value="{htmlspecialchars ($account_title.bugyo_dp_code)}"/>
                <div id="err_bugyo_dp_code"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="float: left;">
                        <button name="{$account_title.id}" class = 'btn-small btn-green btn-edit' value="{$account_title.id}">{lang('common_lbl_save')}</button>
                    </div>
                    <div style="float: left;">
                        <form method="POST" action="/_admin/account_title/show" id="close_form" class="close_form">
                                {make_search_hidden(FALSE)}
                                <button name="close_detail_btn" type="submit" id="close_detail_btn" class="btn-small btn-green btn-close" value="Close">{lang('common_lbl_close')}</button>
                        </form>
                    </div>
                </td>
            </tr>
        </table>
        {else}
            <div class="error-message">{lang('common_record_not_exist')}</div>
        {/if}
        <hr class="blank-10px" />
    </div>
</div>
{literal}
<script>
    $(".close_form").submit(function(){
        $("#dialog_view_edit").dialog("close");
        if (form_submitted && !form_changed) {
            return true;
        } else {
             return false;
        }
    });

    /* $(document).bind('keydown', function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            alert(99999);
            $('.btn-edit').click();
            return false;
        }
    }); */

    // Save to DB
    $(".btn-edit").on('click', function() {
        // alert($("#name").val()+"/"+$("#calculate_code").val());
        url = "/_admin/account_title/edit/";
        $.post( url,
                {
                  name            : $("#name").val(),
                  calculate_code  : $("#calculate_code").val(),
                  support_code    : $("#support_code").val(),
                  remarks         : $("#remarks").val(),
                  bugyo_dp_code   : $("#bugyo_dp_code").val(),
                  id_item         : $("#id").val(),
                  type            : "edit_save_data"
                },
                function( data ) {
                    // If has error messages
                    if (data["error"] == 1) {
                        $(".validation-error").remove();
                        $(".success-message").remove();
                        $("#err_message").remove();
                        $("#err_name").html(data["name"]);
                        $("#err_calculate_code").html(data["calculate_code"]);
                        $("#err_support_code").html(data["support_code"]);
                        $("#err_remarks").html(data["remarks"]);
                        $("#err_bugyo_dp_code").html(data["bugyo_dp_code"]);
                    } else if(data["error"] == 2) {
                        $("#message").html("<div class='warning-message blink_me'>"+"只今、更新できません。再度行ってください。"+"</div>");
                    }
                    else {
                        form_changed = false;
                        form_submitted = true;
                        $(".validation-error").remove();
                        $("#message").html("<div class='success-message blink_me'>"+"更新が完了しました。"+"</div>");
                    }
                },
                "json");
    });

    var bind_edit_event = function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            $('.btn-edit').trigger('click');
            return false;
        }
    };

    $(document).bind('keydown', bind_edit_event);
    exit_confirm();
</script>
{/literal}
