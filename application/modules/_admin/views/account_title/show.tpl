{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<!-- Department -->
<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>
        <!-- Keep pagination -->
        {make_search_hidden()}
        <hr class="blank-10px" />
        <table class="table-theme odd">
            <tr>
                <th>処理</th>
                <th>勘定科目名</th>
                <th>勘定科目コード</th>
                <th>補助科目コード</th>
                <th>摘要文字列</th>
                <th>部門コード</th>
            </tr>
            {if isset($account_titles)}
                {foreach from=$account_titles item=d}
                <tr>
                    <td>
                        <!-- <input type="hidden" value="{$d.id}" name="d_id" /> -->
                        <button class="btn-small btn-green btn-view" value="{$d.id}" name="">{lang('common_lbl_view')}</button>
                    </td>
                    <td>{htmlspecialchars ($d.name)}</td>
                    <td>{htmlspecialchars ($d.calculate_code)}</td>
                    <td>{htmlspecialchars ($d.support_code)}</td>
                    <td>{htmlspecialchars ($d.remarks)}</td>
                    <td>{htmlspecialchars ($d.bugyo_dp_code)}</td>
                </tr>
                {/foreach}
            {else}
                <tr>
                    <td colspan="6">{lang('common_lbl_no_data')}</td>
                </tr>
            {/if}
            <!-- <tr>
                <td colspan="6">
                    <button name="add" id="add" value="add" class="btn-small btn-green btn-add">{lang('common_btn_add')}</button>
                </td>
            </tr> -->
        </table>
        <hr class="blank-10px" />
        {if $pagination} {$pagination.links} {/if}
    </div>
</div>
<div id="dialog_view_edit"></div>
<div id="delete_confirm"></div>
<!-- e/content -->

{literal}
<script>
    $(document).ready(function() {
        $(function() {
            $("#dialog_view_edit").dialog({
                autoOpen : false,
                width : "auto",
                height : "auto",
                modal : true,
                //resizable: false,
                beforeClose: function( event, ui ) {
                    var close = close_confirm();
                    return close;
                },
                buttons : {
                },
                close : function() {
                }
            });
        });

        // Load detail
        $(document).on("click", ".btn-view", function() {
            $('#dialog_view_edit').load("/_admin/account_title/detail", {
                id : this.value,
                redirect_form_post: redirect_data()
            }, function() {
                $('#dialog_view_edit').dialog('option', 'title', "勘定奉行科目参照");
                $("#dialog_view_edit").dialog( "option", "height", "auto");
                $("#dialog_view_edit").dialog( "option", "width", "auto" );
                $('#dialog_view_edit').dialog("open");
            });
        }); // Close $(document).on("click", "#detail_c", function() {

        // Close C when click outside
        $("body").on('click', '.close-dialog', function(event) {
            $(".ui-dialog-content").dialog("close");
            $("#dialog-edit-content").html('');
        });

        $("body").on('click', '.ui-dialog-titlebar-close', function(event) {
            if (form_submitted && !form_changed) {
                $(".close_form").submit();
            }
        });
}); // Close $(document).ready(function() {
</script>
{/literal}
{include file = "inc/footer.tpl"}
