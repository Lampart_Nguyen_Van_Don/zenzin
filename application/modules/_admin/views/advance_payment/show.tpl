{include file = "inc/header.tpl"}
<link rel="styleSheet" href="/public/admin/sample/main.css" />
<script
    src="/public/admin/js/angular.js"></script>
<!-- #7846:Start -->
<!-- <script
    src="/public/admin/js/angular-touch.js"></script>
<script
    src="/public/admin/js/angular-animate.js"></script> -->
<script
    src="/public/admin/js/angular-sanitize.min.js"></script>
<!-- <script src="/public/admin/js/ui-grid-unstable.js"></script> -->
<script src="/public/admin/js/ui-grid-unstable-ver3.0.7.js"></script>
<!-- <link rel="styleSheet" href="/public/admin/css/ui-grid-unstable.css" /> -->
<link rel="styleSheet" href="/public/admin/css/ui-grid-unstable-ver3.0.7.css" />
<!-- #7846:End -->
<script type="text/javascript">
var data_list = {$data_list};
var add_click = 0;
var delete_click = 0;
</script>
<style rel="styleSheet">
.ui-grid-pager-panel {
    visibility: hidden;
}
.grid {
    margin-bottom: 5px;
    height: 575px !important;
}

.ui-grid-viewport{
    height: 543px !important;
}
button.btn-small {
    margin-bottom: 20px;
}

.ui-grid-header-cell-wrapper{
    height: 31px;
    padding-top: 6px;
}
.ui-grid-header-cell-wrapper .ui-grid-cell-contents{
    padding-left: 5px;
}
.ui-grid-cell-contents{
padding:0px;
padding-left: 5px;
padding-right: 5px;
}
.btn-small {
 height: 27px;
}
.grid .ui-grid-cell .ng-scope{
    height: 27px!important;
    padding-top: 6px;
    margin-top: 0px;
}
.grid .ui-grid-cell  button {
     margin-top: 1px;
}
.grid .ui-grid-cell .ui-grid-cell-contents-hidden{
 padding-top: 0px!important;
 float:right;

  height: 0px !important;
}

.ui-grid-cell.ng-scope .scope-new
{
padding-top: 0px!important;

}

 .ui-grid-cell .ng-scope:last-child{
height: auto !important;
padding-top: 0px!important;

}
 .ui-grid-cell .ng-scope:first-child{
 padding-top: 6px!important;
margin-top: 2px;
 }

.grid .ui-grid-cell button.ng-scope:last-child
 {
     height: 27px!important;
       padding-top: 6px !important;
       margin-top: 2px;
 }
div.ui-grid-cell input[type="checkbox"] {
    margin: 0px 0 0 6px;
    width: auto;
}
.ui-dialog-titlebar-close .ui-button-text {
    height: 5px !important;
}
.fixed-dialog {
    position: fixed;
}
.back_ground_color {
    background-color: #B0B0B0 !important;
}
.checkbox_change_css {
    text-align: center;
    vertical-align: middle;
    margin-left: -5px;
}
</style>
<script>
// jsGlobal variable
label = {
        action: lang['lbl_action'],
        change: lang["lbl_change"],
        j_code: lang['lbl_j_code'],
        branch_cd: lang['lbl_branch_cd'],
        client_name: lang['lbl_client_name'],
        unit_price: lang['lbl_unit_price'],
        quantity: lang['lbl_quantity'],
        delivery_date: lang['lbl_delivery_date'],
        total_amount: lang["lbl_total_amount"],
        adjusted_amount: lang["lbl_adjusted_amount"],
        accumulative: lang["lbl_accumulative"],
        confirm: lang["lbl_confirm"]
    };
    btn = {
        add_new: lang['button_add'],
        delete_row: lang['button_delete_ad_pay']
    };
</script>
<script src="/public/admin/advance_payment/advance_payment.js"></script>
<!-- <script src="/public/admin/advance_payment/jspdf.debug.js"></script>
<script src="/public/admin/advance_payment/jspdf.plugin.autotable.js"></script> -->
{include file = "inc/top-header.tpl"} {include file = "inc/nav-left.tpl"}
<div class="cell content slide-right">
    <div ng-controller="adPayCtrl">
        <!--#6972:Start-->
        <div ui-i18n="{literal}{{lang}}{/literal}"></div>
        <!--#6972:End-->
        
        <h1>{$title}</h1>
        <!-- <table border="1" class="tbl_search_advance_payment">
            <tr>
                <td width="120px" style="vertical-align: top;">
                                <div >{lang('lbl_search_from_to')}</div>
                                <div style="padding-top: 16px;">{lang('lbl_search_subcontract_code')}</div>
                                <div style="padding-top: 14px;">{lang('lbl_search_subcontract_name')}</div>
                                <div style="padding-top: 15px;">{lang('lbl_search_subcontract_abbr_name')}</div>
                </td>
                <td>
                    ng-model="search.datepicker_to"
                    <input type="text" name="datepicker_from" ng-model="search.datepicker_from" />～<input type="text" name="datepicker_to" ng-model="search.datepicker_to" /><br/>
                    <div class="legend">{lang('lbl_search_from_to_description')}</div>
                    <div id="error_date_msg"></div>
                    <input type="text" name="subcontractor_code" ng-model="search.subcontractor_code" value="{if isset($form.subcontractor_code)}{$form.subcontractor_code}{/if}"/><br/>
                    <div class="legend">{lang('lbl_search_subcontract_code_description')}</div>
                    <input type="text" name="subcontractor_name" ng-model="search.subcontractor_name" value="{if isset($form.subcontractor_name)}{$form.subcontractor_name}{/if}"/><br/>
                    <div class="legend">{lang('lbl_search_subcontract_name_description')}</div>
                    <input type="text" name="subcontractor_abbr_name" ng-model="search.subcontractor_abbr_name" value="{if isset($form.subcontractor_abbr_name)}{$form.subcontractor_abbr_name}{/if}"/><br/>
                    <div class="legend">{lang('lbl_search_subcontract_abbr_name_description')}</div>
                </td>
            </tr>
        </table> -->
        <table class="table-simple">
            <tbody><tr>
                <th><div >{lang('lbl_search_from_to')}</div></th>
                <td>
                    <input type="text" name="datepicker_from" ng-model="search.datepicker_from" />～<input type="text" name="datepicker_to" ng-model="search.datepicker_to" /><br/>
                    <div class="legend">{lang('lbl_search_from_to_description')}</div>
                    <div id="error_date_msg"></div>
                </td>
            </tr>

            <tr>
                <th><div>{lang('lbl_search_subcontract_code')}</div></th>
                <td>
                    <input type="text" name="subcontractor_code" ng-model="search.subcontractor_code" value="{if isset($form.subcontractor_code)}{$form.subcontractor_code}{/if}"/><br/>
                    <div class="legend">{lang('lbl_search_subcontract_code_description')}</div>
                </td>
            </tr>

            <tr>
                <th><div>{lang('lbl_search_subcontract_name')}</div></th>
                <td>
                    <input type="text" name="subcontractor_name" ng-model="search.subcontractor_name" value="{if isset($form.subcontractor_name)}{$form.subcontractor_name}{/if}"/><br/>
                    <div class="legend">{lang('lbl_search_subcontract_name_description')}</div>
                </td>
            </tr>

            <tr>
                <th><div>{lang('lbl_search_subcontract_abbr_name')}</div></th>
                <td>
                    <input type="text" name="subcontractor_abbr_name" ng-model="search.subcontractor_abbr_name" value="{if isset($form.subcontractor_abbr_name)}{$form.subcontractor_abbr_name}{/if}"/><br/>
                    <div class="legend">{lang('lbl_search_subcontract_abbr_name_description')}</div>
                </td>
            </tr>
        </tbody>
        </table>
        <br/>
        <div>
            <button type="submit" name="search" id="btn_search" ng-click="filter(search)" class="btn-small btn-green btn-search" value="search">{lang('common_lbl_search')}</button>
            <button type="submit" name="print" class="btn-small btn-green" value="print" ng-click="printPDF(rows)">{lang('lbl_print_button_description')}</button>
        </div>
        <form action="/_admin/advance_payment/print_pdf_report" method="post" id="printPDFForm" style="display:none;" target="_blank">
            <!-- tmp#6942: Start 2015/09/30 (Fix the first time after adding new row, could not print pdf) -->
            <input type="text" name="PDFData"/>
            <!-- tmp#6942: End  -->
        </form>

        <div ng-if="displayTable" ng-repeat="row in rows">
            {literal}

            <div>ブレーンコード:   <span ng-bind="row.code"></span> &nbsp;&nbsp;&nbsp;  	ブレーン名: <span ng-bind="row.name"></span></div>
            <div>対象年月: <span ng-bind="search_time_frame"></span></div>
            {/literal}
            <div ui-grid="row.gridOption" class="grid" ui-grid-pinning ui-grid-resize-columns ui-grid-pagination ui-grid-edit ui-grid-row-edit>
            </div>
            <button ng-if="displayBtnSave" class='btn-small btn-green btn-save' ng-click="saveGrid(rows, row.gridOption.data, row)">{lang('common_lbl_save')}</button>
            <br/>
            <a id="bottom"></a>
        </div>
        <div ng-show="displayNoData">
            <br/>{lang('common_lbl_no_data')}
        </div>
    </div>
</div>
<div id="confirm-delete" style="display:none">{lang('message_sure_to_delete')}</div>
<div id="dialog_form_messages" title="messages"></div>
<!-- e/content -->

{literal}
<!-- <script>
$(window).bind('beforeunload', function(){
    // Trigger when form_changed return true then show confirm navigation, else dont show
    if (add_click!=delete_click)
        return 'このページから移動しますか？ 入力したデータは保存されません。';
});
</script> -->
{/literal}

{include file = "inc/footer.tpl"}