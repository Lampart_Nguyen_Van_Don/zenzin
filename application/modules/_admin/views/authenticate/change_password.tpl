{include file = "inc/header.tpl"}
<link href="/public/admin/css/login.css" rel="stylesheet" media="all"/>
<div class="login-wrapper">
<form action="" method="post">
        <div class="login-form">
            <div class="logo"><img src="/public/admin/img/client-logo.png" alt="" /></div>
            {if isset($err_msg)}
                <span class="validation-error">{$err_msg}</span>
            {/if}
            <table>
                <tr>
                    <td>{lang('lbl_new_password')}<span class="txt-red">※</span></td>
                    <td>
                        <input name="new_password" type="password" size="30" value="{if isset($form.new_password)}{$form.new_password}{/if}" autocomplete="off" />
                        {form_error('new_password')}
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_confirm_password')}<span class="txt-red">※</span></td>
                    <td>
                        <input name="confirm_password" type="password" size="30" value="{if isset($form.confirm_password)}{$form.confirm_password}{/if}" autocomplete="off" />
                        {form_error('confirm_password')}
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    <div class="caution">パスワードの注意事項
                    <p>・大小英字と数字を必ず混在させてください</p>
                    <p>例）aBc012xyZ</p>
                    <p>・桁数は8桁以上としてください</p>
                    <p>・現在のパスワードと、一つ前に使用していたパスワードは使用できません</p>
                    </div>
                    </td>
                </tr>
                <tr>
                    <td><a href="/_admin/authenticate/logout"><span class="logout"></span>{lang('common_lbl_logout')}</a></td>
                    <td><input type="submit" value="{lang('lbl_change_password')}" /></td>
                </tr>
            </table>
        </div>
    </form>
</div>
{include file = "inc/footer.tpl"}