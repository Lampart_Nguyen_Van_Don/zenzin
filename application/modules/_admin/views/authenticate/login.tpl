{include file = "inc/header.tpl"}
<link href="/public/admin/css/login.css" rel="stylesheet" media="all"/>
<div class="login-wrapper">
<form action="" method="post">
        <div class="login-form">
            <div class="logo"><img src="/public/admin/img/client-logo.png" alt="" /></div>
            {if isset($err_msg)}
                <span class="validation-error">{$err_msg}</span>
            {/if}
            <table>
                <tr>
                    <td>{lang('lbl_id')}</td>
                    <td>
                        <input name="login_id" type="text" size="30" value="{if isset($form.login_id)}{$form.login_id}{/if}" autocomplete="off" />
                        {form_error('login_id')}
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_password')}</td>
                    <td>
                        <input name="password" type="password" size="30" autocomplete="off"/>
                        {form_error('password')}
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="{lang('lbl_login')}" /></td>
                </tr>
            </table>
        </div>
    </form>
</div>
{include file = "inc/footer.tpl"}