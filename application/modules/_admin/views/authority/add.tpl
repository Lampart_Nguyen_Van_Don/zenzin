<div>
    <div>
        <div id="message" class="alert-success"></div>
        <table class="table-simple">
                <tbody>
                <tr>
                    <th width="11%">{lang("lbl_authority_name")}<span class="red-symbol">※</span></th>
                    <td><input type="text" value="" id = "authority_name" name="authority_name"/>
                    <br/>{form_error('authority_name')}
                    </td>
                </tr>
                <tr>
                    <th>{lang("list_actions")}</th>
                    <td>
                     <div id="list_functions">
                        {foreach from = $list_groups item = g}
                            <h2>{htmlspecialchars ($g.name)}</h2>
                            {assign var="row" value=0}
                            <table id="tbl_checkbox" class="tbl_checkbox">
                            <tr>
                            {if isset($quick_settings[$g.id])}
                            {foreach from = $quick_settings[$g.id] item = f}
                                <!-- Group 1 loop -->
                                    {assign var=row value=$row + 1}
                                    <td>
                                        <input type="checkbox" name="function[]" id="{$f.id}" value="{$f.id}" />
                                        <label for="{$f.id}">{htmlspecialchars ($f.name)}</label>
                                     </td>
                                    {if ($row == 4)}
                                      </tr><tr>
                                    {assign var=row value=0}{/if}
                                <!-- End group 1 loop -->
                            {/foreach}
                            {/if}
                            </tr>
                            </table>
                        {/foreach}
                    </div>
                    </td>
                </tr>
                <tr >
                    <td colspan="2">
                                <div class="float_left">
                                    <button name="save_add" style="" type="submit"
                                        id="save_add" value="save_add" class="btn-small btn-green btn-save">{lang('common_lbl_add')}</button>
                                </div>
                                <div class="float_left">
                                    <form method="POST" id="back_form" class="close_form" action = "/_admin/authority/show">
                                     {make_search_hidden(FALSE)}
                                        <button name="back" type="submit" id="" value="back"
                                            class="btn-small btn-green btn-close">{lang('common_lbl_close')}</button>
                                    </form>
                                </div>
                    </td>
                </tr>
            </tbody></table>
    </div>
</div>
{literal}
<script>

$('#detail_authority').animate({scrollTop: 0}, 100);

$('body').keydown(function(e) {
    if (e.ctrlKey && (e.which == 83)) {
        $('.btn-save').click();
        return false;
    }
});

$('.btn-save').on('click', function() {
        var authority_name = $("#authority_name").val();
        var functions = $("input[name='function\\[\\]']:checked")
              .map(function(){return $(this).val();}).get();
        url = "/_admin/authority/add/";
        $.post( url,
                {
                    save_add: "save_add",
                    functions: functions,
                    authority_name: authority_name,
                    redirect_form_post:redirect_data()
                },
                function( data ) {

                    $('#detail_authority').animate({scrollTop: 0}, 100);

                    // If has error messages
                    if (data["error"] == 1) {
                        $("#message").html("");
                        // Remove old error messages
                        $(".validation-error").remove();
                        $( "#message" ).empty();
                        // Show error messages
                        $("#authority_name").after(data["authority_name"]);
                    } else if(data["error"] == 2) {
                        // If transaction fail: Insert authority or its functions fails
                        $(".validation-error").remove();
                        $("#message").html(data["error_name"]);
                    } else {
                        form_changed = false;
                        form_submitted = true;
                        $("#message").html("<div class='success-message blink_me'>"+$( "#common_insert_successfully" ).val()+"</div>");
                        $("#authority_name").val('');
                        $(".validation-error").remove();
                    }
                },
                "json");
    }); // Close $(document).on('click', '#save_add', function() {

    $(".close_form").submit(function(){
        $("#detail_authority").dialog("close");
        if (form_submitted && !form_changed) {
            return true;
        } else {
             return false;
        }
    });
//});
exit_confirm();
</script>
{/literal}