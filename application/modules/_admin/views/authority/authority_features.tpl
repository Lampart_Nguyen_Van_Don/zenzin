{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

<script src="/public/admin/js/angular.js"></script>
<script src="/public/admin/js/angular-touch.js"></script>
<script src="/public/admin/js/angular-animate.js"></script>
<script src="/public/admin/js/angular-grid.js"></script>
<script src="/public/admin/js/csv.js"></script>
<script src="/public/admin/js/pdfmake.js"></script>
<script src="/public/admin/js/vfs_fonts.js"></script>
<script src="/public/admin/js/ui-grid-unstable.js"></script>
<script src="/public/admin/js/jquery.ba-resize.min.js"></script>
<link rel="stylesheet" href="/public/admin/css/ui-grid-unstable.css" type="text/css">
<link rel="stylesheet" href="/public/admin/css/order_acceptance.css" type="text/css">
<link rel="stylesheet" href="/public/admin/css/angular-grid.css" />
<link rel="stylesheet" href="/public/admin/css/theme-fresh.css" />

<script type="text/javascript">
    //init global variables
    var jsGlobals = [];
    var column_au = [];
    jsGlobals.push('{lang("lbl_action_group")}');
    jsGlobals.push('{lang("lbl_action_name")}');
    jsGlobals.push('{lang("lbl_authority_name")}');

    {foreach from=$list_authorities item = au}
        var name = "{html_escape(htmlspecialchars_decode($au.name))}";
        jsGlobals.push(name);
        column_au.push("id_{$au.id}");
    {/foreach}
</script>

<style type="text/css">
    .cell-light {
        background-color: #1A69AB !important;
        color: white;
    }
    .ui-grid-cell-contents {
        white-space: normal !important;
        font-size: 11px !important;
    }
    .ui-grid-cell {
        font-size: 11px !important;
    }
    .ui-grid-header-cell-wrapper {
        font-size:11px !important;
    }
</style>
<script src="/public/admin/authority/app.js"></script>
<div class="cell content slide-right">
    <div ng-controller="MainCtrl">
        <h1 class="page-title">{$title}</h1>
        {$breadcrumb}<br /><br />
        <form method="POST" action="/_admin/authority/authority_features">
            <table class="table-simple">
                <tr>
                    <th><div class="lbl_search">{lang('lbl_action_group')}</div></th>
                <td>
                    {if isset($form.action_group_id)}
                        {html_options ng-model="search.action_group_id" name="action_group_id" options=$list_group_actions selected=$form.action_group_id}
                    {else}
                        {html_options ng-model="search.action_group_id" name="action_group_id" options=$list_group_actions}
                    {/if}
                </td>
                <td colspan="2">
                    <div align="center">
                        <button type="button" ng-click="filter(search)" class="btn-small btn-green btn-search" name="search" value="search">{lang('common_lbl_search')}</button>
                    </div>
                </td>
                </tr>
            </table>
        </form>
        {if isset($form.action_group_id)}
            <input type="hidden" id="search_group_action_id" value="{$form.action_group_id}"/>
        {/if}
        <hr class="blank-10px" />
        <h1></h1>
        <div ag-grid="gridOptions" style="height: 400px;" external-scopes="states" ui-grid-edit ui-grid-row-edit class="ag-fresh authority-features">
            <div class="watermark" ng-class="{literal}{'show-watermark' : showWatermark}{/literal}">{lang('lbl_no_data_available')}</div>
        </div>
    </div>
</div>
<div id="detail_action"></div>
<input type="hidden" id="au_features" value="{lang('au_features')}"/>
<input type="hidden" id="au_setting" value="{lang('au_setting')}"/>
<input type="hidden" id="common_edit_successfully" value="{lang('common_edit_successfully')}"/>

{literal}
    <script>
        $(document).ready(function() {

            $(function() {
                $("#detail_action").dialog({
                    autoOpen : false,
                    width : "auto",
                    height : "auto",
                    modal : true,
                    open: function(){
                        $('.ui-dialog-titlebar-close').bind('click',function(){
                            if (form_submitted) {
                                $(".close_form").submit();
                            }
                        });

                    },
                    beforeClose: function( event, ui ) {
                        var close = close_confirm();
                        return close;
                    },
                    buttons : {},
                    close : function() {
                    }
                });
            });

            // Click to view detail function
            $(document).on("click", "#view_detail", function() {
                $('#detail_action').load("/_admin/authority/authority_setting", {
                    quick_setting_id: $(this).data('seq'),
                    group_action_id: $("#search_group_action_id").val(),
                    navigate: "navigate"
                }, function() {
                    $('#detail_action').dialog('option', 'title', $('#au_setting').val());
                    $("#detail_action").dialog( "option", "height", "auto" );
                    $("#detail_action").dialog( "option", "width", "auto" );
                    $('#detail_action').dialog("open");
                });
            }); // End detail function

        });
    </script>
{/literal}
{include file = "inc/footer.tpl"}