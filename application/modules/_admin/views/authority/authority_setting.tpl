<div>
    <div>
        <div id="message" class="alert-success"></div>
        {if isset($quick_setting)}
<!-- #7568:Start !-->
        	 {lang("lbl_action_group_name")}:{$nameaction}<br/>
<!-- #7568:End !-->
             {lang('lbl_action_name')}：{$quick_setting.name}
        <div id="list_authorites" style="border: 1px solid black; padding: 10px; margin-top: 5px; margin-bottom: 5px;">
            <table>
                <tr>
                {if isset($list_authorities)}
                    {assign var="row" value=0}
                    {foreach from = $list_authorities item = au}
                        {assign var=row value=$row + 1}
                        <td>
                            <input type="checkbox" name="function[]" id="{$au.id}" value="{$au.id}" {if isset($authorities[$au.id])} checked{/if}/>
                            <label for="{$au.id}">{$au.name}</label>
                        </td>
                        {if ($row == 4)}
                        </tr><tr>
                        {assign var=row value=0}{/if}
                    {/foreach}
                {/if}
                </tr>
            </table>
        </div>

         <div style="float: left; margin-left:36%;">
             <button name="save_edit" style="" type="submit"
                 id="save_edit" value="{$quick_setting.id}" class="btn-small btn-green btn-save">{lang('common_lbl_save')}</button>
         </div>
         <div style="float: left;">
             <form method="POST" class="close_form" action = "/_admin/authority/authority_features">
                 {if isset($group_action_id)}
                    <input type="hidden" name="action_group_id" value="{$group_action_id}"/>
                 {/if}
                 <button name="back" type="button" id="" value="back" class="btn-small btn-green btn-close">{lang('common_lbl_close')}</button>
             </form>
         </div>
        {else}
            <div class="error-message">{lang('common_record_not_exist')}</div>
        {/if}
    </div>
</div>
{literal}
<script>
    $(".close_form").on("click", function(){
        $("#detail_action").dialog("close");
        $('.btn-search').click();
    });

    $(document).bind('keydown', function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            $('.btn-save').click();
            return false;
        }
    });
    // User clicks save
    $(document).on('click', '#save_edit', function() {
        var functions = $("input[name='function\\[\\]']:checked")
              .map(function(){return $(this).val();}).get();
        var url = "/_admin/authority/authority_setting/";
        $.post( url,
                {
                    quick_setting_id: this.value,
                    functions: functions,
                    save_edit_setting: "save_edit_setting"
                },
                function( data ) {
                    // If has error messages
                    if (data["error"] == 1) {
                        // Remove old error messages
                        $(".validation-error").remove();
                        // Show error messages
                        $("#message").html(data["error_name"]);
                    } else {
                        form_changed = false;
                        form_submitted = true;
                        $("#message").html("<div class='success-message blink_me'>"+$( "#common_edit_successfully" ).val()+"</div>");
                        $(".validation-error").remove();
                    }
                },
                "json");
    });
    exit_confirm();
//});
</script>
{/literal}