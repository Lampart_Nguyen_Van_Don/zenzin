{literal}
<script>

    $('#detail_authority').animate({scrollTop: 0}, 100);

    // Close dialog
    $('#close_dialog').on('click', function() {
               $("#detail_authority").dialog("close");
               return false;
    });

    // Click edit in dialogbox c
    $('#to_edit_page').on('click', function() {
         $('#detail_authority').load("/_admin/authority/edit", {
             authority_id: this.value,
             to_edit_page: "to_edit_page",
             redirect_form_post:redirect_data()
         }, function() {
             $("#dialog_form_c").dialog( "option", "height", "auto" );
             $("#dialog_form_c").dialog( "option", "width", "600" );
             $('#dialog_form_c').dialog('option', 'title', $('#au_edit').val());
             $('#dialog_form_c').dialog("open");
         });
     });
</script>
{/literal}
<div>
    <div>
    <div id="message" style="color:red;"></div>
    {if isset($authority)}
        <table class="table-simple">
                <tbody>
                <tr>
                    <th width="11%">{lang("lbl_authority_name")}</th>
                    <td>{htmlspecialchars ($authority.name)}</td>
                </tr>
                <tr>
                    <th>{lang("list_actions")}</th>
                    <td>
                    <div id="list_functions">
                    {if isset($list_groups)}
                        {foreach from = $list_groups item = g}
                            <h2>{htmlspecialchars ($g.name)}</h2>
                            {assign var="row" value=0}
                            <table id="tbl_checkbox" class="tbl_checkbox">
                                <tr>
                                {if isset($quick_settings[$g.id])}
                                    {foreach from = $quick_settings[$g.id] item = f}
                                             {assign var=row value=$row + 1}
                                             <td class="a-top">
                                             {if isset($action_authorities[$f.id][$authority.id])}
                                                 <input type="checkbox" disabled name="function[]" id="{$f.id}" value="{$f.id}" checked/>
                                                 <label for="{$f.id}">{$f.name|htmlspecialchars}</label>
                                             {else}
                                                 <input type="checkbox" disabled name="function[]" id="{$f.id}" value="{$f.id}" />
                                                 <label for="{$f.id}">{$f.name|htmlspecialchars}</label>
                                             {/if}
                                              </td>
                                             {if ($row == 3)}
                                              </tr><tr>
                                             {assign var=row value=0}
                                             {/if}
                                    {/foreach}
                                {/if}
                               </tr>
                            </table>
                        {/foreach}
                    {/if}
                    </div>
                    </td>
                </tr>
                <tr >
                    <td colspan="2">
                        <div class="float_left">
                            <input type="hidden" value="{$authority.id}" name="a_id" />
                            <button name="to_edit_page"
                                id="to_edit_page" value="{$authority.id}" class="btn-small btn-green btn-edit">{lang('common_lbl_edit')}</button>
                        </div>

                        <div class="float_left">

                            <button name="close_dialog" id="close_dialog"
                               class="btn-small btn-green btn-close" value="Close">{lang('common_lbl_close')}
                            </button>
                        </div>
                    </td>
                </tr>
            </tbody>
           </table>
            {else}
                <div class="error-message">{lang('common_record_not_exist')}</div>
            {/if}
   </div>
</div>
