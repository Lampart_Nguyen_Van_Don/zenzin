<div>
    <div>
        <div id="message" style="color:red;"></div>
        {if isset($authority)}
        <table class="table-simple">
                <tbody>
                <tr>
                    <th width="11%">{lang("lbl_authority_name")}<span class="red-symbol">※</span></th>
                    <td><input type="text" value="{htmlspecialchars ($authority.name)}" id = "authority_name" name="authority_name"/>
                    <br/><div id="err_authority_name"></div>
                    </td>
                </tr>
                <tr>
                    <th>{lang("list_actions")}</th>
                    <td>
                     <div id="list_functions">
                         {if isset($list_groups)}
                            {foreach from = $list_groups item = g}
                                <h2>{htmlspecialchars ($g.name)}</h2>
                                {assign var="row" value=0}
                                <table id="tbl_checkbox" class="tbl_checkbox">
                                <tr>
                                {if isset($quick_settings[$g.id])}
                                    {foreach from = $quick_settings[$g.id] item = f}
                                             {assign var=row value=$row + 1}
                                             <td>
                                             {if isset($action_authorities[$f.id][$authority.id])}
                                                 <input type="checkbox" name="function[]" id="{$f.id}" value="{$f.id}" checked/>
                                                 <label for="{$f.id}">{$f.name|htmlspecialchars}</label>
                                             {else}
                                                 <input type="checkbox" name="function[]" id="{$f.id}" value="{$f.id}" />
                                                 <label for="{$f.id}">{$f.name|htmlspecialchars}</label>
                                             {/if}
                                              </td>
                                             {if ($row == 4)}
                                              </tr><tr>
                                             {assign var=row value=0}
                                             {/if}
                                    {/foreach}
                                {/if}
                                </tr>
                                </table>
                            {/foreach}
                        {/if}
                    </div>
                    </td>
                </tr>
                <tr >
                    <td colspan="2">
                                <div class="float_left">
                                    <button name="save_edit" style="" type="submit"
                                        id="save_edit" value="{$authority.id}" class="btn-small btn-green btn-save">{lang('common_lbl_save')}</button>
                                </div>
                                <div class="float_left">
                                    <form method="POST" class="close_form" action = "/_admin/authority/show">
                                    {make_search_hidden(FALSE)}
                                        <button name="back" type="submit" id="" value="back"
                                            class="btn-small btn-green btn-close">{lang('common_lbl_close')}</button>
                                    </form>
                                </div>
                                <div class="float_left">
                                    <form method="POST" id="delete_form" action = "/_admin/authority/show">
                                        {make_search_hidden(FALSE)}</form>
                                        <button name="delete_authority" id="delete_authority"
                                            class="btn-small btn-green btn-delete" value="{$authority.id}">{lang('common_lbl_delete')}</button>

                                </div>
                    </td>
                </tr>
            </tbody></table>
            {else}
                <div class="error-message">{lang('common_record_not_exist')}</div>
            {/if}
    </div>
</div>
{literal}
<script>

    $('#detail_authority').animate({scrollTop: 0}, 100);

    $(document).bind('keydown', function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            $('.btn-save').click();
            return false;
        }
    });

    $('.btn-save').on('click', function() {
        var authority_name = $("#authority_name").val();
        var functions = $("input[name='function\\[\\]']:checked")
              .map(function(){return $(this).val();}).get();
        url = "/_admin/authority/edit/";
        $.post( url,
                {
                    authority_id: this.value,
                    save_edit_page: "save_edit_page",
                    functions: functions,
                    authority_name: authority_name
                },
                function( data ) {

                    $('#detail_authority').animate({scrollTop: 0}, 100);

                    // If has error messages
                    if (data["error"] == 1) {
                        // Remove old error messages
                        $(".validation-error").remove();
                        $( "#message" ).empty();
                        // Show error messages
                        // $("#authority_name").after(data["authority_name"]);
                        $("#err_authority_name").html(data["authority_name"]);
                    } else if(data["error"] == 2) {
                        // If transaction fail: Update authority or its functions fails
                        $(".validation-error").remove();
                        $("#message").html(data["error_name"]);
                    } else {
                        form_changed = false;
                        form_submitted = true;
                        //$("#message").html("Editted successfully!");
                        $("#message").html("<div class='success-message blink_me'>"+$( "#common_edit_successfully" ).val()+"</div>");
                        $(".validation-error").remove();
                    }
                },
                "json");
    }); // End $(document).on('click', '#save_edit', function() {
    // Close form
    $(".close_form").submit(function(){
        $("#detail_authority").dialog("close");
        if (form_submitted && !form_changed) {
            return true;
        } else {
             return false;
        }
    });
    exit_confirm();
</script>
{/literal}