{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>
        {$breadcrumb}<br />

        <hr class="blank-10px" />
        <table class="table-theme odd">
            <tr>
                <th>{lang("common_lbl_action")}</th>
                <th>{lang("common_lbl_sequence")}</th>
                <th>{lang("lbl_authority_name")}</th>
            </tr>
            {if isset($authorities)}
                {foreach from=$authorities item = authority}
            <tr>
                <td>
                    <button name="detail" type="submit" value="{$authority.id}" class="btn-small btn-green btn-view">{lang('common_lbl_view')}</button>
                </td>
                <td>
                    <input name="sequence[{$authority.id}]" type="text" maxlength="5" value={$authority.sequence} class="sequence {$authority.id} {$authority.sequence}"/>
                </td>
                <td>{htmlspecialchars ($authority.name)}</td>
            </tr>
            {/foreach} {else}
            <tr>
                <th colspan="2">{lang('txt_no_data')}</th>
            </tr>
            {/if}
            <tr>
                <td colspan="6">
                    <div class="align-button">
                        {form_sequence()}
                        <button name="add_authority" type="submit" id="add_authority" value="{$authority.id}" class="btn-small btn-green btn-add">{lang('common_btn_add')}</button>
                    </div>
            </tr>
        </table>
        <hr class="blank-10px" />
        {if $pagination} {$pagination.links} {/if}
    </div>
    {make_search_hidden()}
    <div id="detail_authority" title="{lang('au_info')}"></div>
    <div id="delete_confirm"></div>
</div>
<!-- e/content -->
<input type="hidden" id="common_edit_successfully" value="{lang('common_edit_successfully')}"/>
<input type="hidden" id="common_insert_successfully" value="{lang('common_insert_successfully')}"/>
<input type="hidden" id="au_info" value="{lang('au_info')}"/>
<input type="hidden" id="au_add_new" value="{lang('au_add_new')}"/>
<input type="hidden" id="au_edit" value="{lang('au_edit')}"/>
<input type="hidden" id="au_edit" value="{lang('au_edit')}"/>

{literal}
<script>
    $(document).ready(function() {
        $(function() {
            // Load as type B
            $("#detail_authority").dialog({
                autoOpen : false,
                width : "auto",
                height : "600",
                modal : true,
                beforeClose: function( event, ui ) {
                    var close = close_confirm();
                    return close;
                },
                buttons : {},
                close : function() {
                }
            });
        });

        // Close dialog when click outside
        $("body").on('click', '.close-dialog', function(event) {
            $(".ui-dialog-content").dialog("close");
            $("#dialog-edit-content").html('');
            $('#detail_authority').html("");
        });

        $("body").on('click', '.ui-dialog-titlebar-close', function(event) {
            if (form_submitted && !form_changed) {
                $(".close_form").submit();
            }
        });

       // Load dialogbox when click detail
        $('.btn-view').on("click", function() {
            $('#detail_authority').load("/_admin/authority/detail", {
                id : this.value,
                redirect_form_post: redirect_data()
            }, function() {
                $("#detail_authority").dialog({
                    title: '権限参照',
                    width: 'auto',
                    height: '600',
                    modal : true
                });
                $('#detail_authority').dialog('open');
            });
        });

     // Load dialogbox when click add
        $('#add_authority').on("click", function() {
            // alert(this.value);
            $('#detail_authority').load("/_admin/authority/add", {
                go_to_add_page: "go_to_add_page",
                redirect_form_post: redirect_data()
            }, function() {
                $("#detail_authority").dialog({
                    title: $('#au_add_new').val(),
                    width: 'auto',
                    height: '600',
                    modal : true
                });
                $('#detail_authority').dialog('open');
            });
        });

         // Close dialog
        $('#close_dialog').on('click', function() {
            $("#detail_authority").dialog("close");
            return false;
        });

        // User click delete to delete authority
        $(document).on('click', '#delete_authority', function(event) {
            event.preventDefault();
            var id = $(this).val();
            confirm_dialog(lang['message_confirm_delete'], {
                'delete' : function() {
                    // Close dialog
                    $(this).dialog('close');
                    deleteAuthority(id);
                }
            });
        });

        function deleteAuthority(id) {
            url = "/_admin/authority/delete/";
            $.post( url,
                    {
                    authority_id: id
                    },
                    function( data ) {
                        // If has error messages
                        if (data["error"] == 1) {
                            $('#detail_authority').animate({scrollTop: 0}, 100);
                            // Remove old error messages
                            $(".validation-error").remove();
                            $("#message").html('<div class="error-message">'+data["error_name"]+'</div>');

                        } else {
                            // Submit form to load all data again:
                            $("#delete_form").submit();
                            $("#detail_authority").dialog("close");
                        }
                    },
                    "json");
        }
    });
</script>
{/literal}

{include file = "inc/footer.tpl"}
