<style>
	.table-theme input {
		width: 99%;
	}
	.table-theme .first_col {
		width: 100px;
	}
</style>
<div>
    <div>
        <hr class="blank-10px" />
        <div id="message" style="color:red;"></div>
        <input type="hidden" name="b_id" id="b_id" value="{$item.id}" />
        <table class="table-theme" width="100%">
            <tr>
                <td class="first_col">{lang('lbl_bank_name')}<span class="txt-red"> ※</span></td>
                <td>
                    <input type="text" name="bank_name" id="bank_name" value="{html_escape(htmlspecialchars_decode($item.financial_institution_name))}">
                </td>
            </tr>
            <tr>
                <td class="first_col">{lang('lbl_branch_name')}<span class="txt-red"> ※</span></td>
                <td><input type="text" name="branch_name" id="branch_name" value="{html_escape(htmlspecialchars_decode($item.branch_name))}"></td>
            </tr>
            <tr>
                <td class="first_col">{lang('lbl_account_number')}<span class="txt-red"> ※</span></td>
                <td><input type="text" name="account_number" id="account_number" value="{html_escape(htmlspecialchars_decode($item.account_number))}"></td>
            </tr>
            <tr>
                <td class="first_col">{lang('lbl_account_holder')}<span class="txt-red"> ※</span></td>
                <td><input type="text" name="account_holder" id="account_holder" value="{html_escape(htmlspecialchars_decode($item.account_holder))}"></td>
            </tr>

            <tr>
                <td colspan=2>
                    <div style="float: left;">
                        <button name="save_edit" value="Save" value="Save" class = 'btn-small btn-green btn-save'>{lang('common_lbl_save')}</button>
                    </div>
                    <div style="float: left;">
                        <form method="POST" action="/_admin/bank_account/show" class="close_form">
                            <button name="close_edit" type="submit" id = "close_edit" class="btn-small btn-green btn-close" value="Close">{lang('common_lbl_close')}</button>
                        </form>
                    </div>
                </td>
            </tr>

        </table>
    </div>
</div>
<input type="hidden" id="common_edit_successfully" value="{lang('common_edit_successfully')}"/>

{literal}
    <script>
        $(".close_form").submit(function(){
            $("#dialog_form").dialog("close");
            if (form_submitted && !form_changed) {
                return true;
            } else {
                 return false;
            }
        });


        $(document).bind('keydown', function(e) {
            if (e.ctrlKey && (e.which == 83)) {
                e.preventDefault();
                $('.btn-save').click();
                return false;
            }
        });

        $(".btn-save").on('click', function() {
            $.post('/_admin/bank_account/edit/',
                {
                    name: this.name,
                    b_id: $("#b_id").val(),
                    bank_name: $("#bank_name").val(),
                    branch_name: $("#branch_name").val(),
                    account_number: $("#account_number").val(),
                    account_holder: $("#account_holder").val()
                },
                function(data) {
                    var name = 'bank_name';
                    if (data['error'] == 1) {
                        $('.validation-error').remove();
                        $('#bank_name').after('<p>' + data.bank_name + '</p>');
                        $('#branch_name').after('<p>' + data.branch_name + '</p>');
                        $('#account_number').after('<p>'+ data.account_number + '</p>');
                        $('#account_holder').after('<p>'+ data.account_holder + '</p>');
                            
                        $("#dialog_form").dialog({
                            height: 'auto', 
                            width: 'auto', 
                            modal: true,
                            position: {my: "center", at: "center", of: window}
                        });
                    }
                    else if (data['error'] == 0) {
                        form_changed = false;
                        form_submitted = true;
                        $(".validation-error").remove();
                        $("#message").html('<span class="success-message blink_me">'+$("#common_edit_successfully").val()+'</span>');
                    }
                },
            "json");
        });

    // Trigger if on change in form input
    exit_confirm();
    </script>
{/literal}