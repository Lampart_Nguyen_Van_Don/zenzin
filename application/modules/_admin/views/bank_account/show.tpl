{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

{literal}
<script>
    $(document).ready(function() {
        // Load dialog
        $(function() {
        	var the_width = ($(".table-simple tr:first-child td").width() + 200) + "px";
            $("#dialog_form").dialog({
                autoOpen : false,
                width : the_width, //373,
                height : 370,
                modal : true,
                beforeClose: function( event, ui ) {
                    var close = close_confirm();
                    return close;
                },
                open: function(){
                    jQuery('.ui-widget-overlay').bind('click',function(){
                        jQuery('#dialog').dialog('close');
                    })

                },
                buttons : {
                },
                close : function() {
                }
            });

        });

        /*
        * Using Ajax method
        * Event #click, call function:  load data from url bank_account/edit to #dialog_form
        * After load success, call dialog to show
        */
        $(".btn-edit").on("click", function() {
            $.ajax({
                type: "POST",
                url: "/_admin/bank_account/edit",
                data: {
                    id : this.value,
                    name: this.name
                }
            })
            .done(function(data) {
            	var the_width = ($(".table-simple tr:first-child td").width() + 200) + "px";
                $("#dialog_form").dialog({
                    title: $( "#lbl_bank_account_edit" ).val(),
                    width: the_width,
                    height: 'auto',
                    modal: true,
                    position: {my: "center", at: "center", of: window}
                });
                $('#dialog_form').html(data);
                $('#dialog_form').dialog("open");
            });
        });

        $("body").on('click', '.ui-dialog-titlebar-close', function(event) {
            if (form_submitted && !form_changed) {
                $(".close_form").submit();
            }
        });

    });
</script>
{/literal}
<div class="cell content">
    <div>
        <h1 class="page-title">{$title}</h1>
        <hr class="blank-10px" />
        <table class="table-simple">
            <tr>
                <th>{lang('lbl_bank_name')}</th>
                <td>{$item.financial_institution_name}</td>
            </tr>
            <tr>
                <th>{lang('lbl_branch_name')}</th>
                <td>{$item.branch_name}</td>
            </tr>
            <tr>
                <th>{lang('lbl_account_number')}</th>
                <td>{$item.account_number}</td>
            </tr>
            <tr>
                <th>{lang('lbl_account_holder')}</th>
                <td>{$item.account_holder}</td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="float: left;">
                        <input type="hidden" value="{$item.id}" name="item_id">
                        <button name="edit" id="edit" class='btn-small btn-green btn-edit' value="{$item.id}">{lang('common_lbl_edit')}</button>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<!-- Store messages -->
<input type="hidden" id="edit_successfully" value="{lang('edit_successfully')}"/>
<input type="hidden" id="lbl_bank_account_edit" value="{lang('lbl_bank_account_edit')}"/>
<input type="hidden" id="ref_financial_institution_name" value="{$item.financial_institution_name}"/>
<!-- End store messages -->

<div id="dialog_form" title="{lang('lbl_bank_account_edit')}"></div>

{include file = "inc/footer.tpl"}