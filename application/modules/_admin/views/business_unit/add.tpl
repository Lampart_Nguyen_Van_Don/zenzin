    <div>
        <div>
            <hr class="blank-10px" />
            <div id="message" style="color:red;"></div>
            {*hidden id here*}
            {form_open("/_admin/business_unit/show", "id='form_add' class='form_data'")}
            <table class="table-theme">
                <tr>
                    <td style="width:241px;">{lang('lbl_bu_name')}<span class="txt-red"> ※</span></td>
                    <td><input type="text" name="txt_bu_name" id="txt_bu_name" value=""></td>
                </tr>
                <tr>
                    <td>{lang('lbl_is_product_div')}</td>
                    <td>
                    <label>
                        {form_checkbox([
                            'name' => 'txt_is_product_div',
                            'id' => 'is_product_div',
                            'value' => 1,
                            'checked' => 0,
                            'onchange' => 'isChecked(this);'
                        ])} <span class="product_div">{lang('lbl_product_div')}</span>
                    </label>
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_target_months')}<span class="txt-red not-show"> ※</span></td>
                    <td>
                        <input type="text" name="txt_target_months" size="1" maxlength="2" id="txt_target_months" value="" disabled> <span id='lbl_month'>{lang('common_lbl_months')}</span>
                        <div class="legend"> (半角数字2桁まで)</div>
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_j_code')}<span class="txt-red not-show"> ※</span></td>
                    <td>
                        <input type="text" name="txt_j_code" size="2" maxlength="1" id="txt_j_code" value="" disabled>
                        <div class="legend"> (半角英数字1桁)</div>
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_bugyo_account_code')}<span class="txt-red not-show"> ※</span></td>
                    <td>
                        <input type="text" name="txt_bugyo_account_code" size="2" maxlength="4" id="txt_bugyo_account_code" value="" disabled>
                        <div class="legend"> (半角数字4桁まで)</div>
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_bugyo_abstract_string')}<span class="txt-red not-show"> ※</span></td>
                    <td>
                        <input type="text" name="txt_bugyo_abstract_string" maxlength="20" id="txt_bugyo_abstract_string" value="" disabled>
                        <div class="legend"> (全角20桁まで)</div>
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_bugyo_code')}<span class="txt-red not-show"> ※</span></td>
                    <td>
                        <input type="text" name="txt_bugyo_department_code" size="2" maxlength="2" id="txt_bugyo_department_code" value="" disabled>
                        <div class="legend"> (半角数字2桁)</div>
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_good_mn_code')}<span class="txt-red not-show"> ※</span></td>
                    <td>
                        <input type="text" name="txt_product_mn_code" size="2" maxlength="2" id="txt_product_mn_code" value="" disabled>
                        <div class="legend"> (半角数字2桁)</div>
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_work_finish_report_template')}<span class="txt-red not-show"> ※</span></td>
                    <td>
                        <select name="work_finish_report_template" id="work_finish_report_template" disabled>
                            <option value="">{lang('common_please_select')}</option>
                            {if isset($business_unit_template)}
                            {foreach from=$business_unit_template item=bu_temp}
                                <option value="{$bu_temp.code}">{htmlspecialchars ($bu_temp.name)}</option>
                            {/foreach}
                            {/if}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_template')}<span class="txt-red not-show"> ※</span></td>
                    <td>
                        <textarea name="rtb_template" id="rtb_template" rows="4" cols="50" disabled></textarea>
                        <div class="legend"> (改行入力可)</div>
                    </td>
                </tr>
                <tr>
                    <td colspan=2>
                        <button name="save_add" id="save_add" type="button" value="Save" class='btn-small btn-green btn-save'>{lang('btn_add')}</button>
                        <input type="hidden" name="pg" id="pg" value={$pg} />
                        <form method="POST" action="/_admin/business_unit/show" id="close_form">
                            <button name="close_edit" type="button" id = "close_edit" class="btn-small btn-green btn-close" value="Close">{lang('common_lbl_close')}</button>
                        </form>
                    </td>
                </tr>
            </table>
            {form_close()}
        </div>
    </div>
{literal}
<script>
$("#form_add").submit(function() {
    form_changed = false;
    if( !$('#message').is(':empty') ) {
        $("#dialog_form").dialog("close");
        $("#close_form").submit();
    } else {
         $("#dialog_form").dialog("close");
         return false;
    }
});

$("#save_add").on('click', function(e) {

    form_data = $("#form_add").serialize();
    form_data += '&btn_name=save_add';
    url = '/_admin/business_unit/add/';
    $.post(url, form_data, function(data) {
            // Have error
            if (!data.status) {
                // reset errors before
                $('.validation-error').remove();
                // check situation have is_product_division
                if (data.errors && data.is_product_division) {
                    $('#txt_bu_name').after('<p>' + data.errors.txt_bu_name + '</p>');
                    $('#lbl_month').after('<p>' + data.errors.txt_target_months + '</p>');
                    $('#txt_j_code').after('<p>' + data.errors.txt_j_code + '</p>');
                    $('#txt_bugyo_department_code').after('<p>' + data.errors.txt_bugyo_department_code + '</p>');
                    $('#txt_bugyo_account_code').after('<p>' + data.errors.txt_bugyo_account_code + '</p>');
                    $('#txt_bugyo_abstract_string').after('<p>' + data.errors.txt_bugyo_abstract_string + '</p>');
                    $('#txt_product_mn_code').after('<p>' + data.errors.txt_product_mn_code + '</p>');
                    $('#rtb_template').after('<p>' + data.errors.rtb_template + '</p>');
                    $('#work_finish_report_template').after('<p>' + data.errors.work_finish_report_template + '</p>');
                } else { // not choose is_product_division
                    $('#txt_bu_name').after('<p>' + data.errors.txt_bu_name + '</p>');
                }
            }
            else {
                form_changed = false;
                form_submitted = true;
                // after add success, reset input bu name
                $('#txt_bu_name, #txt_target_months, #lbl_month, #txt_j_code, #txt_bugyo_department_code, #txt_bugyo_account_code, #txt_bugyo_abstract_string, #txt_product_mn_code, #rtb_template').val('').prop('disabled', true);
                $('#txt_bu_name').prop('disabled', false);
                $('#is_product_div').prop('checked', false);  
                $(".txt-red").slice(1,8).addClass('not-show');
                // reset messages validation before
                $('.validation-error').remove();
                // show message success
                $("#message").html('<span class="success-message blink_me">' + data.message + '</span>');
                // add class back-to-show to trigger event reload form
                $(".btn-close").addClass('back-to-show');
                $(".ui-dialog-titlebar-close").addClass('back-to-show');
            }
        },
    "json")

});

function isChecked(obj) {
    if ($(obj).prop('checked')) { // have checked is division
        // add asterisk to 4 label
        $(".txt-red").slice(1,9).removeClass('not-show');
        $("input:text,textarea,select").slice(1,9).prop('disabled', false);

    } else {
        $(".txt-red").slice(1,9).addClass('not-show');
        // remove data in inputs
        $("input:text,textarea,select").slice(1,9).val('');
        $("input:text,textarea,select").slice(1,9).prop('disabled', true);
    }
}


var bind_add_event = function(e) {
    if (e.ctrlKey && (e.which == 83)) {
        e.preventDefault();
        $("#save_add").trigger('click');
        return false;
    }
};

$(document).bind('keydown', bind_add_event);

exit_confirm();

</script>
{/literal}