    <div>
        <div>
            <hr class="blank-10px" />
            <div id="message"></div>
            {*hidden id here*}
            {if isset($business_unit)}
            <table class="table-theme">
                <tr>
                    <td>{lang('lbl_bu_name')}</td>
                    <td>{htmlspecialchars ($business_unit.name)}</td>
                </tr>
                <tr>
                    <td>{lang('lbl_is_product_div')}</td>
                    <td>
                    {form_checkbox([
                                'name' => 'txt_is_product_div',
                                'value' => $business_unit.is_product_division,
                                'checked' => $business_unit.is_product_division,
                                'disabled' => ""

                            ])} <span class="product_div">{lang('lbl_product_div')}</span>
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_target_months')}</td>
                    <td>{if $business_unit.is_product_division}{$business_unit.totalization_target_months}{/if}</td>
                </tr>
                <tr>
                    <td>{lang('lbl_j_code')}</td>
                    <td>{if $business_unit.is_product_division}{$business_unit.j_code}{/if}</td>
                </tr>
                <tr>
                    <td>{lang('lbl_bugyo_account_code')}</td>
                    <td>{if $business_unit.is_product_division}{$business_unit.bugyo_account_code}{/if}</td>
                </tr>
                <tr>
                    <td>{lang('lbl_bugyo_abstract_string')}</td>
                    <td>{if $business_unit.is_product_division}{htmlspecialchars ($business_unit.bugyo_abstract_string)}{/if}</td>
                </tr>
                <tr>
                    <td>{lang('lbl_bugyo_code')}</td>
                    <td>{if $business_unit.is_product_division}{$business_unit.bugyo_department_code}{/if}</td>
                </tr>
                <tr>
                    <td>{lang('lbl_good_mn_code')}</td>
                    <td>{if $business_unit.is_product_division}{$business_unit.product_manage_code}{/if}</td>
                </tr>
                <tr>
                    <td>{lang('lbl_work_finish_report_template')}</td>
                    <td>{if $business_unit.is_product_division && isset($business_unit.finish_work_report_template_name)} {htmlspecialchars ($business_unit.finish_work_report_template_name)} {/if}</td>
                </tr>
                <tr>
                    <td>{lang('lbl_template')}</td>
                    <td>{if $business_unit.is_product_division}{$business_unit.template|nl2br|truncate:30}{/if}</td>
                </tr>
                <tr>
                    <td colspan=2>
                        <div class="float_left">
                            <button type="button" class='btn-small btn-green btn-edit' data-business-id="{$business_unit.id}">{lang('common_lbl_edit')}</button>
                        </div>
                        <div class="float_left">
                            <button type="submit" class="btn-small btn-green btn-close">{lang('common_lbl_close')}</button>
                        </div>
                    </td>
                </tr>
            </table>
            {else}
                <div class="error-message">{lang('common_record_not_exist')}</div>
            {/if}
        </div>
    </div>
{literal}
<script>
$(".btn-edit").on('click', function() {
    form_changed = false;
    id = $(this).data('business-id');
    $('#dialog_form').load("/_admin/business_unit/edit", {
        id: id
        }, function(data) {
            if (!data) {
                $("#dialog_form").html('<span class="error-message">'+ 'invalid id' +'</span>');
            }
            $("#dialog_form").dialog({
                title: '事業部編集',
                width: 'auto',
                height: 'auto',
                modal : true,
                close: function() {
                    $(document).unbind('keydown', bind_edit_event);
                }
            });
        });
});

</script>
{/literal}