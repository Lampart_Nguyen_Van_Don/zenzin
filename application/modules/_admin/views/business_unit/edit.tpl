    <div>
        <div>
            <hr class="blank-10px" />
            <div id="message" style="color:red;"></div>
            {*hidden id here*}
            {form_open("/_admin/business_unit/show", "id='form_edit' class='form_data'")}
            <table class="table-theme">
                <tr>
                    <td style="width:241px;">{lang('lbl_bu_name')}<span class="txt-red"> ※</span></td>
                    <td><input type="text" name="txt_bu_name" id="txt_bu_name" value="{$business_unit.name|htmlspecialchars}"></td>
                </tr>
                <tr>
                    <td>{lang('lbl_is_product_div')}</td>
                    <td>
                    <label>
                    {form_checkbox([
                                'name' => 'txt_is_product_div',
                                'id' => 'txt_is_product_div',
                                'value' => 1,
                                'checked' => $business_unit.is_product_division,
                                'onchange' => 'isChecked(this);'
                            ])} <span class="product_div">{lang('lbl_product_div')}</span>
                    </label>
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_target_months')}<span class="txt-red {if $business_unit.is_product_division}{else}not-show{/if}"> ※</span></td>
                    <td>
                        <input type="text" name="txt_target_months" size="1" maxlength="2" id="txt_target_months" value="{if $business_unit.totalization_target_months != 0 && $business_unit.is_product_division}{$business_unit.totalization_target_months}{/if}" {if $business_unit.is_product_division}{else}disabled{/if}> <span id='lbl_month'>{lang('common_lbl_months')}</span>
                        <div class="legend"> (半角数字2桁まで)</div>
                    </td>
                </tr>

                <tr>
                    <td>{lang('lbl_j_code')}<span class="txt-red {if $business_unit.is_product_division}{else}not-show{/if}"> ※</span></td>
                    <td>
                        <input type="text" name="txt_j_code" size="2" maxlength="1" id="txt_j_code" value="{if $business_unit.is_product_division}{$business_unit.j_code}{/if}" {if !$business_unit.is_product_division}disabled{/if}>
                        <div class="legend"> (半角英数字1桁)</div>
                    </td>
                </tr>

                <tr>
                    <td>{lang('lbl_bugyo_account_code')}<span class="txt-red {if $business_unit.is_product_division}{else}not-show{/if}"> ※</span></td>
                    <td>
                        <input type="text" name="txt_bugyo_account_code" size="3" maxlength="4" id="txt_bugyo_account_code" value="{if $business_unit.is_product_division}{$business_unit.bugyo_account_code}{/if}" {if !$business_unit.is_product_division}disabled{/if}>
                        <div class="legend"> (半角数字4桁まで)</div>
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_bugyo_abstract_string')}<span class="txt-red {if $business_unit.is_product_division}{else}not-show{/if}"> ※</span></td>
                    <td>
                        <input type="text" name="txt_bugyo_abstract_string" maxlength="20" id="txt_bugyo_abstract_string" value="{if $business_unit.is_product_division}{htmlspecialchars($business_unit.bugyo_abstract_string)}{/if}" {if !$business_unit.is_product_division}disabled{/if}>
                        <div class="legend"> (全角20桁まで)</div>
                    </td>
                </tr>

                <tr>
                    <td>{lang('lbl_bugyo_code')}<span class="txt-red {if $business_unit.is_product_division}{else}not-show{/if}"> ※</span></td>
                    <td>
                        <input type="text" name="txt_bugyo_department_code" size="2" maxlength="2" id="txt_bugyo_department_code" value="{if $business_unit.bugyo_department_code != 0 && $business_unit.is_product_division}{$business_unit.bugyo_department_code}{/if}" {if $business_unit.is_product_division}{else}disabled{/if}>
                        <div class="legend"> (半角数字2桁)</div>
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_good_mn_code')}<span class="txt-red {if $business_unit.is_product_division}{else}not-show{/if}"> ※</span></td>
                    <td>
                        <input type="text" name="txt_product_mn_code" size="2" maxlength="2" id="txt_product_mn_code" value="{if $business_unit.product_manage_code != 0 && $business_unit.is_product_division}{$business_unit.product_manage_code}{/if}" {if $business_unit.is_product_division}{else}disabled{/if}>
                        <div class="legend"> (半角数字2桁)</div>
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_work_finish_report_template')}<span class="txt-red {if $business_unit.is_product_division}{else}not-show{/if}"> ※</span></td>
                    <td>
                        <select name="work_finish_report_template" id="work_finish_report_template" {if $business_unit.is_product_division}{else}disabled{/if} >
                            <option value="">{lang('common_please_select')}</option>
                            {if isset($business_unit_template)}
                            {foreach from=$business_unit_template item=bu_temp}
                                <option value="{$bu_temp.code}" {if $business_unit.finish_work_report_template==$bu_temp.code}selected{/if}>{htmlspecialchars ($bu_temp.name)}</option>
                            {/foreach}
                            {/if}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_template')}<span class="txt-red {if $business_unit.is_product_division}{else}not-show{/if}"> ※</span></td>
                    <td>
                        <textarea name="rtb_template" id="rtb_template" rows="4" cols="50" {if $business_unit.is_product_division}{else}disabled{/if}>{if $business_unit.template && $business_unit.is_product_division}{$business_unit.template}{/if}</textarea>
                        <div class="legend"> (改行入力可)</div>
                    </td>
                </tr>
                <tr>
                    <td colspan=2>
                            <button name="save_edit"  type="button" value="{$business_unit.id}" class = 'btn-small btn-green btn-save'>{lang('btn_save')}</button>
                            <button name="close_edit" type="button" id="close_edit" class="btn-small btn-green btn-close" value="Close">{lang('common_lbl_close')}</button>
                            <button name="btn_delete" type="button" value="Delete" data-business-id={$business_unit.id} class="btn-small btn-green btn-delete">{lang('common_lbl_delete')}</button>
                        </div >

                    </td>
                </tr>
            </table>
            {form_close()}
            <form method="POST" id="form_delete" action="/_admin/business_unit/show">
            </form>
        </div>
    </div>
{literal}
<script>
    $("#form_edit").submit(function() {
        if( !$('#message').is(':empty') ) {
            $("#dialog_form").dialog("close");
            return true;
        } else {
            $("#dialog_form").html('');
            $("#dialog_form").dialog("close");
            return false;
        }
    });


    var bind_edit_event = function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            $('.btn-save').click();
            return false;
        }
    };

    $(document).bind('keydown', bind_edit_event);

    $(".btn-save").on('click', function() {
        form_data = $("#form_edit").serialize();
        form_data += '&btn_name=save_edit&id='+this.value;

        $.post('/_admin/business_unit/edit/', form_data, function(data) {
            if (!data.status) {
                if (data.is_product_division) {
                    $("#message").html('');
                    $('.validation-error').remove();
                    $('#txt_bu_name').after('<p>' + data.errors.txt_bu_name + '</p>');
                    $('#lbl_month').after('<p>' + data.errors.txt_target_months + '</p>');
                    $('#txt_j_code').after('<p>' + data.errors.txt_j_code + '</p>');
                    $('#txt_bugyo_department_code').after('<p>' + data.errors.txt_bugyo_department_code + '</p>');
                    $('#txt_bugyo_account_code').after('<p>' + data.errors.txt_bugyo_account_code + '</p>');
                    $('#txt_bugyo_abstract_string').after('<p>' + data.errors.txt_bugyo_abstract_string + '</p>');
                    $('#txt_product_mn_code').after('<p>' + data.errors.txt_product_mn_code + '</p>');
                    $('#work_finish_report_template').after('<p>' + data.errors.finish_work_report_template + '</p>');
                    $('#rtb_template').after('<p>' + data.errors.rtb_template + '</p>');
                } else {
                    $("#message").html('');
                    $('.validation-error').remove();
                    $('#txt_bu_name').after('<p>' + data.errors.txt_bu_name + '</p>');
                }
            } else {
                form_changed = false;
                form_clean = $(".form_data").serialize();
                $('.validation-error').remove();
                $(".btn-close").addClass('back-to-show');
                $(".ui-dialog-titlebar-close").addClass('back-to-show');
                $("#message").html('<span class="success-message blink_me">' + data.message + '</span>');
                    
                $("#txt_target_months, #txt_j_code, #txt_bugyo_account_code, #txt_bugyo_abstract_string, #txt_bugyo_department_code, #txt_product_mn_code, #rtb_template").removeData();
            }
        })

    });

    $(".btn-delete").on('click', function(event) {
        event.preventDefault();
        business_unit_id = $(this).data('business-id');
        confirm_dialog(lang['message_confirm_delete'], {
            'delete' : function() {
                // Close dialog
                $(this).dialog('close');
                $.ajax({
                    url: '/_admin/business_unit/delete',
                    type: 'POST',
                    data: {business_unit_id: business_unit_id}
                })
                .done(function(data) {
                    if (!data.status) {
                        $("#message").html('<span class="error-message">'+ data.message +'</span>');
                        $("#dialog_form").dialog({
                            width:'auto',
                            height: 'auto',
                            modal: true
                        });
                        return; //stop here
                    }

                    dom = "<input type='hidden' name='pg' value='"+$("#page").val()+"'>";
                    $(dom).appendTo('#form_delete');
                    $("#form_delete").submit();
                })
            }
        });
    });

    function isChecked(obj) {
        if ($(obj).prop('checked')) {
            // add asterisk to 4 label
            $(".txt-red").slice(1,9).removeClass('not-show');
            $("input:text,textarea,select").slice(1,9).prop('disabled', false);

            $("#txt_target_months").val($("#txt_target_months").data("old-data"));
            $("#txt_j_code").val($("#txt_j_code").data("old-data"));
            $("#txt_bugyo_account_code").val($("#txt_bugyo_account_code").data("old-data"));
            $("#txt_bugyo_abstract_string").val($("#txt_bugyo_abstract_string").data("old-data"));
            $("#txt_bugyo_department_code").val($("#txt_bugyo_department_code").data("old-data"));
            $("#txt_product_mn_code").val($("#txt_product_mn_code").data("old-data"));
            $("#rtb_template").val($("#rtb_template").data("old-data"));
        } else {
            $(".txt-red").slice(1,9).addClass('not-show');
            $("input:text,textarea,select").slice(1,9).prop('disabled', true);

            $("#txt_target_months").data('old-data',$("#txt_target_months").val()).val('');
            $("#txt_j_code").data('old-data',$("#txt_j_code").val()).val('');
            $("#txt_bugyo_account_code").data('old-data',$("#txt_bugyo_account_code").val()).val('');
            $("#txt_bugyo_abstract_string").data('old-data',$("#txt_bugyo_abstract_string").val()).val('');
            $("#txt_bugyo_department_code").data('old-data',$("#txt_bugyo_department_code").val()).val('');
            $("#txt_product_mn_code").data('old-data',$("#txt_product_mn_code").val()).val('');
            $("#rtb_template").data('old-data',$("#rtb_template").val()).val('');
        }
    }
exit_confirm();
</script>
{/literal}