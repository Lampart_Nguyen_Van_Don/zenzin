{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

{make_search_hidden()}
<div class="cell content slide-right" >
    <div>
    {* Content table business unit view *}
    <h1 class="page-title">{$title}</h1>
    <table class="table-theme odd">
        {if isset($business_units)} {* Have content *}
        <tr>
            <th>{lang('lbl_action_name')}</th>
            <th>{lang('lbl_business_unit_name')}</th>
        </tr>
        {foreach from = $business_units item = business_unit}
        <tr>
            <td><a class="btn-small btn-green btn-view view-business" data-business-id={$business_unit.id}>{lang('common_lbl_view')}</a></td>
            <td>{htmlspecialchars ($business_unit.name|truncate:20)}</td>
        </tr>
        {/foreach}
        {else}{* No content to show *}
        <tr>
            <td colspan="3">{lang('common_lbl_no_data')}</td>
        </tr>
        {/if}
        {* button add new *}
        <tr>
            <td colspan="2"><a class="btn-small btn-green btn-add add-business">{lang('common_btn_add')}</a></td>
            <input id="page" name="pg" type="hidden" value="{if isset($form.pg)}{$form.pg}{else}{1}{/if}">
        </tr>

    </table>
    <hr class="blank-10px" />
    {if $pagination} {$pagination.links} {/if}
    </div>
    {*Store message*}
    <input type="hidden" id="lbl_business_unit_info" value="{lang('lbl_business_unit_info')}"></input>
    <input type="hidden" id="lbl_business_unit_add" value="{lang('lbl_business_unit_add')}"></input>
    <input type="hidden" id="lbl_business_unit_edit" value="{lang('lbl_business_unit_edit')}"></input>
</div>

<div id="dialog_form" title="{lang('lbl_business_unit_info')}"></div>

{literal}
<script>
    $(document).ready(function() {

        $(function() {
            $("#dialog_form").dialog({
                autoOpen : false,
                modal : true,
                open: function(){
                    jQuery('.ui-widget-overlay').bind('click',function(){
                        jQuery('#dialog').dialog('close');
                    })
                },

                beforeClose: function( event, ui ) {
                    var close = close_confirm();
                    return close;
                },

                buttons : {
                },
                close : function() {

                }
            });
        });

        $(".view-business").on('click', function() {
            form_changed = false;
            business_unit_id = $(this).data('business-id');
            $.ajax({
                type: "POST",
                url: "/_admin/business_unit/detail",
                data: {
                    business_unit_id: business_unit_id,
                    redirect_form_post:redirect_data()
                }
            })
            .done(function(data) {

                if (data.error_code == 400) {
                    $("#dialog_form").dialog({
                        width: "auto",
                        height: "auto",
                        modal: true
                    });
                    $('#dialog_form').dialog('open');
                    return;
                }

                //Load data to dialog
                $('#dialog_form').html(data);
                $('#dialog_form').dialog({
                    title: '事業部参照',
                    width: 'auto',
                    height: 'auto',
                    modal: true
                });
                $('#dialog_form').dialog('open');
             })


        });

        $(".add-business").on('click', function() {
            form_changed = false;
        // Ajax load view add
            $.ajax({
                type: "POST",
                url: "/_admin/business_unit/add",
                data: {
                    pg : $("#page").val(),
                    redirect_form_post:redirect_data()
                }
            })
            .done(function(data) {
                $("#dialog_form").dialog({
                    title: '事業部追加',
                    width: 'auto',
                    height: 'auto',
                    modal : true,
                    close: function() {
                        $(document).unbind('keydown', bind_add_event);
                    }
                });
                $('#dialog_form').html(data);
                $('#dialog_form').dialog('open');

            });
        });

        $("body").on('click', '.btn-close', function(event) {
            $(".ui-dialog-content").dialog("close");
        });

        $("body").on('click', '.close-dialog', function(event) {
            $(".ui-dialog-content").dialog("close");
        });

        // Trigger this class and add form submit with param 'pg' to keep page status current
        $('body').on('click', '.back-to-show', function(event) {
            if (!form_changed && $(".success-message").length > 0) {
                dom =   "<form id='form_reload' method='post' action='/_admin/business_unit/show'>"+
                            "<input type='hidden' name='pg' value='"+$("#page").val()+"'>"+
                        "</form>";
                $(dom).appendTo('body').submit();
            }
        });
    });
</script>
{/literal}
{include file = "inc/footer.tpl"}