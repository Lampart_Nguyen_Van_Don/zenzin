<link href="/public/admin/client/main.css" rel="stylesheet">
<style>
.ui-dialog-content a {
    color: #fff;
}
#input-table td:last-child {
    min-width: 750px;
}
</style>

<div class="cl_content">
    <div>
        <div class="message"></div>

        <hr class="blank-10px" />

        {form_open('', ['id' => 'apply_form'], ['is_reapp' => $is_reapp])}
        {if $is_reapp}
            {form_hidden(['id' => $client->id])}
        {/if}

        <table id="input-table" class="table-simple">
            {if $is_reapp}
                {if $client->status == $smarty.const.CLIENT_STATUS_CL_APPROVED || $client->status == $smarty.const.CLIENT_STATUS_CL_APPROVAL_REJECTED}
                    <tr>
                        <td>{lang('lbl_last_comment')}</td>
                        <td class="pre-wrap">{htmlspecialchars($client->comment)}</td>
                    </tr>
                {/if}
            {/if}
            <tr>
                <td>{lang('lbl_name')} <span class="red-symbol">※</span></td>
                <td>
                    <span style="width: 27%; float:left;">
                        {form_dropdown([
                            'name' => 'corporate_status_before',
                            'class' => 'company_title prefix',
                            'items' => ['' => lang('common_select')] + $client->company_title_options(),
                            'value' => set_value('corporate_status_before', $client->corporate_status_before)
                        ])}
                        <div class="error"></div>
                    </span>
                    <span style="width: 42%; float:left;">
                        {form_input([
                            'id'   => 'name',
                            'name' => 'name',
                            'class' => 'xs',
                            'value' => set_value('name', $client->name, false)
                        ])}
                        <div class="error"></div>
                    </span>
                    <span style="width: 27%; float:left;">
                        {form_dropdown([
                            'name' => 'corporate_status_after',
                            'class' => 'company_title suffix',
                            'items' => ['' => lang('common_select')] + $client->company_title_options(),
                            'value' => set_value('corporate_status_after', $client->corporate_status_after)
                        ])}
                    </span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_name_kana')} <span class="red-symbol">※</span></td>
                <td>
                    <span style="width: 27%; float:left;">&nbsp;</span>
                    <span style="width: 42%; float:left;">
                        {form_input([
                            'id'   => 'name_kana',
                            'name' => 'name_kana',
                            'class' => 'xs',
                            'value' => set_value('name_kana', $client->name_kana, false)
                        ])}
                        <div class="legend">{lang('lbl_kana_auto')}</div>
                        <div class="error"></div>
                    </span>
                    <span style="width: 27%; float:left;">&nbsp;</span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_zipcode')} <span class="red-symbol">※</span></td>
                <td>
                    {form_input([
                        'name' => 'zipcode',
                        'class' => 'ss',
                        'maxlength' => '7',
                        'value' => set_value('zipcode', $client->zipcode, false)
                    ])}
                    <div class="legend">{lang('lbl_no_hyphen')}</div>
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_address')}１ <span class="red-symbol">※</span></td>
                <td>
                    {form_input([
                        'name' => 'address_1st',
                        'class' => 'xxs',
                        'value' => set_value('address_1st', $client->address_1st, false)
                    ])}
                    <div class="legend">{lang('lbl_enter_2byte')}</div>
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_address')}２</td>
                <td>
                    {form_input([
                        'name' => 'address_2nd',
                        'class' => 'xxs',
                        'value' => set_value('address_2nd', $client->address_2nd, false)
                    ])}
                    <div class="legend">{lang('lbl_enter_2byte')}</div>
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_representative_name')} <span class="red-symbol">※</span></td>
                <td>
                    {form_input([
                        'name' => 'representative_name',
                        'value' => set_value('representative_name', $client->representative_name, false)
                    ])}
                    {lang('lbl_mr')}
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_tel')} <span class="red-symbol">※</span></td>
                <td>
                    {form_input([
                        'name' => 'phone_no',
                        'value' => set_value('phone_no', $client->phone_no, false)
                    ])}
                    <div class="legend">{lang('lbl_enter_hyphen')}</div>
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_fax')}</td>
                <td>
                    {form_input([
                        'name' => 'fax_no',
                        'value' => set_value('fax_no', $client->fax_no, false)
                    ])}
                    <div class="legend">{lang('lbl_enter_hyphen')}</div>
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_business_category')} <span class="red-symbol">※</span></td>
                <td>
                    {form_dropdown([
                        'name' => 'business_category',
                        'items' => ['' => lang('common_select')] + $client->business_category_options(),
                        'value' => set_value('business_category', $client->business_category)
                    ])}
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_division')}</td>
                <td>
                    {form_input([
                        'name' => 'division_name',
                        'class' => 'xxs',
                        'value' => set_value('division_name', $client->division_name, false)
                    ])}
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_customer_contact')} <span class="red-symbol">※</span></td>
                <td>
                    {form_input([
                        'name' => 'charge_name',
                        'value' => set_value('charge_name', $client->charge_name, false)
                    ])}
                    {lang('lbl_mr')}
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_mail_address')}</td>
                <td>
                    {form_input([
                        'name' => 'mail_address',
                        'class' => 'xxs',
                        'value' => set_value('mail_address', $client->mail_address, false)
                    ])}
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_payment_terms')} <span class="red-symbol">※</span></td>
                <td>
                    <span style="width: 28%; float:left;">
                        {lang('lbl_closing_date')}&nbsp;
                        {form_dropdown([
                            'name' => 'closing_date',
                            'items' => ['' => lang('common_select')] + $client->payment_closing_date_options(),
                            'value' => set_value('closing_date', $client->closing_date)
                        ])} {lang('common_lbl_date')}
                        <div class="error"></div>
                    </span>

                    <span style="width: 28%; float:left;">
                        {lang('lbl_payment_month')}
                        {form_dropdown([
                            'name' => 'payment_month_cd',
                            'items' => ['' => lang('common_select')] + $client->payment_month_options(),
                            'value' => set_value('payment_month_cd', $client->payment_month_cd)
                        ])} {lang('common_lbl_month')}
                        <div class="error"></div>
                    </span>

                    <span style="width: 28%; float:left;">
                        {lang('lbl_payment_date')}
                        {form_dropdown([
                            'name' => 'payment_day_cd',
                            'items' => ['' => lang('common_select')] + $client->payment_date_options(),
                            'value' => set_value('payment_day_cd', $client->payment_day_cd)
                        ])} {lang('common_lbl_date')}
                        <div class="error"></div>
                    </span>

                    <span style="width: 10%; float:left; margin-top: 3px">
                        <label>
                            {form_checkbox([
                                'name'    => 'is_ad_receive_payment_request',
                                'value'   => 1,
                                'checked' => set_checkbox('is_ad_receive_payment_request', 1, $client->is_ad_receive_payment_request == 1),
                                'style'   => 'vertical-align: middle'
                            ])}
                            {lang('lbl_advance_payment')}
                        </label>
                    </span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_sales_representative')}</td>
                <td>
                    <div style="display: inline-block; margin-right: 10px;">
                        {lang('lbl_sales_staff')}１<span class="red-symbol">※</span>
                        {form_dropdown([
                            'name' => 's_account_id_1st',
                            'items' => ['' => lang('common_select')] + $accounts,
                            'value' => set_value('s_account_id_1st', $client->s_account_id_1st)
                        ])}
                        <div class="error"></div>
                    </div>

                    <div style="display: inline-block; vertical-align: top;">
                        {lang('lbl_sales_staff')}２
                        {form_dropdown([
                            'name' => 's_account_id_2nd',
                            'items' => ['' => lang('common_select')] + $accounts,
                            'value' => set_value('s_account_id_2nd', $client->s_account_id_2nd)
                        ])}
                        <div class="error"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>{lang(lbl_first_delivery)} <span class="red-symbol">※</span></td>
                <td>
                    <span style="width: 18%; float:left;">
                        {lang('lbl_delivery_month')}:&nbsp;
                        {form_input([
                            'name' => 'delivery_month_cd',
                            'class' => 'xts',
                            'value' => set_value('delivery_month_cd', $client->delivery_month_cd, false)
                        ])}
                        {lang('common_lbl_month')}
                        <div class="error"></div>
                    </span>
                    <span style="width: 28%; float:left;">
                        {lang('lbl_delivery_amount')}:&nbsp;
                        {form_input([
                            'name' => 'delivery_amount',
                            'class' => 'ts delivery_amount',
                            'value' => set_value('delivery_amount', $client->delivery_amount, false)
                        ])}
                        {lang('common_lbl_yen')}
                        <div class="error"></div>
                    </span>
                    <span style="width: 26%; float:left;">
                        {lang('lbl_profit_amount')}:&nbsp;
                        {form_input([
                            'name' => 'gross_profit_amount',
                            'class' => 'ts profit_amount',
                            'value' => set_value('gross_profit_amount', $client->gross_profit_amount, false)
                        ])}
                        {lang('common_lbl_yen')}
                        <div class="error"></div>
                    </span>
                    <span class="a-right" style="width: 27%; float:left;">
                        {lang('lbl_profit_rate')}:&nbsp;
                        {form_input([
                            'name' => 'gross_profit_rate',
                            'class' => 'ts profit_rate',
                            'value' => set_value('gross_profit_rate', $client->gross_profit_rate, false),
                            'disabled' => 'disabled'
                        ])}％
                        <div class="error"></div>
                    </span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_year_total')} <span class="red-symbol">※</span></td>
                <td>
                    <span style="width: 18%; float:left;">&nbsp;</span>
                    <span style="width: 28%; float:left;">
                        {lang('lbl_delivery_amount')}:&nbsp;
                        {form_input([
                            'name' => 'total_delivery_amount',
                            'class' => 'ts delivery_amount',
                            'value' => set_value('total_delivery_amount', $client->total_delivery_amount, false)
                        ])}
                        {lang('common_lbl_yen')}
                        <div class="error"></div>
                    </span>
                    <span style="width: 26%; float:left;">
                        {lang('lbl_profit_amount')}:&nbsp;
                        {form_input([
                            'name' => 'total_gross_profit_amount',
                            'class' => 'ts profit_amount',
                            'value' => set_value('total_gross_profit_amount', $client->total_gross_profit_amount, false)
                        ])}
                        {lang('common_lbl_yen')}
                        <div class="error"></div>
                    </span>
                    <span class="a-right" style="width: 27%; float:left;">
                        {lang('lbl_profit_rate')}:&nbsp;
                        {form_input([
                            'name' => 'total_gross_profit_rate',
                            'class' => 'ts profit_rate',
                            'value' => set_value('total_gross_profit_rate', $client->total_gross_profit_rate, false),
                            'disabled' => 'disabled'
                        ])}％
                        <div class="error"></div>
                    </span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_remark')}（{lang('lbl_background_info')}） <span class="red-symbol">※</span></td>
                <td>
                    {form_textarea([
                        'name' => 'remark',
                        'value' => set_value('remark', $client->remark, false),
                        'rows' => 3
                    ])}
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    {if !$is_reapp}
                        <button name="action" class="btn-small btn-green" value="apply">{lang('btn_new_application')}</button>
                    {else}
                        <button name="action" class="btn-small btn-green" value="reapply">{lang('btn_reapplication')}</button>
                    {/if}
                    <a href="javascript:void(0)" class="btn-small btn-green btn-close">{lang('btn_back_to_list')}</a>
                    {if $is_reapp}
                        <button name="action" class="btn-small btn-green" value="delete">{lang('common_lbl_delete')}</button>
                    {/if}
                </td>
            </tr>
        </table>
        {form_close()}
    </div>
</div>
{*<script src="/public/admin/js/auto_ruby.jquery.js"></script>*}
<script src="/public/admin/client/function.js"></script>
<script>
    registered = false;

    company_title_handler();

//    convert_kana_handler();

    auto_calculate_profit();

    adv_receive_handler();

    {if !$is_reapp}
        bind_save_key($('button[value=apply]'));
    {else}
        bind_save_key($('button[value=reapply]'));
    {/if}

    $('button[name=action]').click(function(e) {
        e.preventDefault();
        var action = $(this).val();
        var id = $('[name=id]').val();

        $('.error').html('');
        $('.message').removeClass('caution').removeClass('success-message');

        if (action == 'delete') {
            $.post('edit', { id: id, action: 'check_order' }, function(result) {
                if (result.status) {
                    confirm_dialog(lang['message_confirm_delete'], {
                        'delete' : function() {
                            do_action('/_admin/client/apply_retry_delete', action, $('#apply_form').serialize());
                            $(this).dialog('close');
                        }
                    });
                } else {
                    $('.message').addClass('caution').text(result.message);
                }
            });
        } else {
            do_action('/_admin/client/apply_retry_delete', action, $('#apply_form').serialize());
        }
    });

    exit_confirm();
</script>
