<link href="/public/admin/client/main.css" rel="stylesheet">
<style>
.ui-dialog-content a {
    color: #fff;
}
#view-table td:last-child {
    min-width: 600px;
}
</style>

<div class="cl_content">
    <div>
        <div class="message"></div>

        {form_open('', ['id' => 'credit_form'], ['id' => $client.id])}

        <table id="view-table" class="table-simple">
            <tr>
                <td>{lang('lbl_name')}</td>
                <td>{htmlspecialchars($client.name)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_name_kana')}</td>
                <td>{htmlspecialchars($client.name_kana)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_zipcode')}</td>
                <td>{substr($client.zipcode, 0, 3)}-{substr($client.zipcode, 3)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_address')}１</td>
                <td>{htmlspecialchars($client.address_1st)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_address')}２</td>
                <td>{htmlspecialchars($client.address_2nd)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_representative_name')}</td>
                <td>{htmlspecialchars($client.representative_name)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_tel')}/{lang('lbl_fax')}</td>
                <td>
                    <span>{lang('lbl_tel')}: {$client.phone_no}</span>
                    <span class="ml10">{lang('lbl_fax')}: {$client.fax_no}</span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_business_category')}</td>
                <td>
                    {if isset($business_category_options[$client.business_category])}
                        {$business_category_options[$client.business_category]}
                    {/if}
                </td>
            </tr>

            <tr>
                <td>{lang('lbl_payment_terms')}</td>
                <td>
                    {if !$client.is_ad_receive_payment_request}
                        <span>
                            {lang('lbl_closing_date')}:&nbsp;
                            {if isset($payment_closing_date_options[$client.closing_date])}
                                {$payment_closing_date_options[$client.closing_date]}{lang('common_lbl_date')}
                            {/if}
                        </span>
                        <span class="ml10">
                            {lang('lbl_payment_month')}:&nbsp;
                            {if isset($payment_month_options[$client.payment_month_cd])}
                                {$payment_month_options[$client.payment_month_cd]}{lang('common_lbl_month')}
                            {/if}
                        </span>
                        <span class="ml10">
                            {lang('lbl_payment_date')}:&nbsp;
                            {if isset($payment_date_options[$client.payment_day_cd])}
                                {$payment_date_options[$client.payment_day_cd]}{lang('common_lbl_date')}
                            {/if}
                        </span>
                    {else}
                        <span>{lang('lbl_advance_payment')}</span>
                    {/if}
                </td>
            </tr>

            <tr>
                <td>{lang('lbl_sales_representative')}</td>
                <td>
                    {strip}
                        {if isset($accounts[$client.s_account_id_1st])}
                            {htmlspecialchars($accounts[$client.s_account_id_1st]['name'])}
                        {/if}
                        {if isset($accounts[$client.s_account_id_2nd])}
                            , {htmlspecialchars($accounts[$client.s_account_id_2nd]['name'])}
                        {/if}
                    {/strip}
                </td>
            </tr>

            <tr>
                <td>{lang('lbl_first_month_delivery')}</td>
                <td style="min-width: 500px">
                    <span style="width: 22%; float:left;">{lang('lbl_delivery_month')}: {$client.delivery_month_cd}{lang('common_lbl_month')}</span>
                    <span style="width: 27%; float:left;">{lang('lbl_delivery_amount')}: {number_format($client.delivery_amount)}{lang('common_lbl_yen')}</span>
                    <span style="width: 23%; float:left;">{lang('lbl_profit_amount')}: {number_format($client.gross_profit_amount)}{lang('common_lbl_yen')}</span>
                    <span class="a-right" style="width: 25%; float:left;">{lang('lbl_profit_rate')}: {$client.gross_profit_rate}％</span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_year_total')}</td>
                <td>
                    <span style="width: 22%; float:left;">&nbsp;</span>
                    <span style="width: 27%; float:left;">{lang('lbl_delivery_amount')}: {number_format($client.total_delivery_amount)}{lang('common_lbl_yen')}</span>
                    <span style="width: 25%; float:left;">{lang('lbl_profit_amount')}: {number_format($client.total_gross_profit_amount)}{lang('common_lbl_yen')}</span>
                    <span class="a-right" style="width: 23%; float:left;">{lang('lbl_profit_rate')}: {$client.total_gross_profit_rate}％</span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_remark')}（{lang('lbl_background_info')}）</td>
                <td>
                    <span class="pre-wrap">{htmlspecialchars($client.remark)}</span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_credit_result')}</td>
                <td>
                    <div>{lang('lbl_credit_level')}: {if isset($credit_level_options[$client.credit_level])}{$credit_level_options[$client.credit_level]}{/if}</div>
                    <div>
                        {lang('lbl_credit_amount')}: {number_format($client.credit_amount)}{lang('common_lbl_yen')}
                        {if $client.is_ad_receive_payment}
                            <span class="ml10">{lang('lbl_advance_payment')}</span>
                        {/if}
                    </div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_comment')}</td>
                <td>
                    <span class="pre-wrap">{htmlspecialchars($client.comment)}</span>
                </td>
            </tr>

            <tr>
                <td>{lang('lbl_comment')}</td>
                <td>
                    {form_textarea([
                        'name' => 'comment',
                        'value' => set_value('comment', $client.comment, false),
                        'rows' => 3
                    ])}
                    <div class="error"></div>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <button type="submit" name="action" class="btn-small btn-green" value="approve">{lang('btn_approve')}</button>
                    <button type="submit" name="action" class="btn-small btn-green" value="reject">{lang('btn_reject')}</button>
                </td>
            </tr>

        </table>
        {form_close()}
    </div>
</div>

<script src="/public/admin/client/function.js"></script>
<script>
    bind_save_key($('button[value=approve]'));

    $('[name=action]').click(function(e) {
        e.preventDefault();
        do_action('/_admin/client/approve_reject', $(this).val(), $('#credit_form').serialize());
    });

    exit_confirm();
</script>