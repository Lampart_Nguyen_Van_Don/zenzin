{$accounts = index_element($account_data, 'id')}

<link href="/public/admin/client/main.css" rel="stylesheet">
<style>
.ui-dialog-content a {
    color: #fff;
}
#view-table td:last-child {
    min-width: 600px;
}
</style>

<div class="cl_content">
    <div>

        <div class="result_message"></div>

        <table id="view-table" class="table-simple">
            <tr>
                <td>{lang('lbl_s_code')}</td>
                <td>
                    {htmlspecialchars($client.s_code)}
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_name')}</td>
                <td>
                    {htmlspecialchars($this->client_model->name_with_title($client.name, $client.corporate_status_before, $client.corporate_status_after))}
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_name_kana')}</td>
                <td>{htmlspecialchars($client.name_kana)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_zipcode')}</td>
                <td>{substr($client.zipcode, 0, 3)}-{substr($client.zipcode, 3)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_address')}１</td>
                <td>{htmlspecialchars($client.address_1st)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_address')}２</td>
                <td>{htmlspecialchars($client.address_2nd)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_representative_name')}</td>
                <td>{htmlspecialchars($client.representative_name)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_tel')}/{lang('lbl_fax')}</td>
                <td>
                    <span>{lang('lbl_tel')}: {$client.phone_no}</span>
                    <span class="ml10">{lang('lbl_fax')}: {$client.fax_no}</span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_business_category')}</td>
                <td>
                    {if isset($business_category_options[$client.business_category])}
                        {$business_category_options[$client.business_category]}
                    {/if}
                </td>
            </tr>

            <tr>
                <td>{lang('lbl_payment_terms')}</td>
                <td>
                    {if !$client.is_ad_receive_payment_request}
                        <span>
                            {lang('lbl_closing_date')}:&nbsp;
                            {if isset($payment_closing_date_options[$client.closing_date])}
                                {$payment_closing_date_options[$client.closing_date]}{lang('common_lbl_date')}
                            {/if}
                        </span>
                        <span class="ml10">
                            {lang('lbl_payment_month')}:&nbsp;
                            {if isset($payment_month_options[$client.payment_month_cd])}
                                {$payment_month_options[$client.payment_month_cd]}{lang('common_lbl_month')}
                            {/if}
                        </span>
                        <span class="ml10">
                            {lang('lbl_payment_date')}:&nbsp;
                            {if isset($payment_date_options[$client.payment_day_cd])}
                                {$payment_date_options[$client.payment_day_cd]}{lang('common_lbl_date')}
                            {/if}
                        </span>
                    {else}
                        <span>{lang('lbl_advance_payment')}</span>
                    {/if}
                </td>
            </tr>

            <tr>
                <td>{lang('lbl_sales_representative')}</td>
                <td>
                    {strip}
                        {if isset($accounts[$client.s_account_id_1st])}
                            {htmlspecialchars($accounts[$client.s_account_id_1st]['name'])}
                        {/if}
                        {if isset($accounts[$client.s_account_id_2nd])}
                            , {htmlspecialchars($accounts[$client.s_account_id_2nd]['name'])}
                        {/if}
                    {/strip}
                </td>
            </tr>

            <tr>
                <td>{lang('lbl_first_month_delivery')}</td>
                <td style="min-width: 500px">
                    <span style="width: 22%; float:left;">{lang('lbl_delivery_month')}: {$client.delivery_month_cd}{lang('common_lbl_month')}</span>
                    <span style="width: 27%; float:left;">{lang('lbl_delivery_amount')}: {number_format($client.delivery_amount)}{lang('common_lbl_yen')}</span>
                    <span style="width: 23%; float:left;">{lang('lbl_profit_amount')}: {number_format($client.gross_profit_amount)}{lang('common_lbl_yen')}</span>
                    <span class="a-right" style="width: 25%; float:left;">{lang('lbl_profit_rate')}: {$client.gross_profit_rate}％</span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_year_total')}</td>
                <td>
                    <span style="width: 22%; float:left;">&nbsp;</span>
                    <span style="width: 27%; float:left;">{lang('lbl_delivery_amount')}: {number_format($client.total_delivery_amount)}{lang('common_lbl_yen')}</span>
                    <span style="width: 25%; float:left;">{lang('lbl_profit_amount')}: {number_format($client.total_gross_profit_amount)}{lang('common_lbl_yen')}</span>
                    <span class="a-right" style="width: 23%; float:left;">{lang('lbl_profit_rate')}: {$client.total_gross_profit_rate}％</span>
                </td>
            </tr>
            <tr>
                <td><div style="min-width: 200px;">{lang('lbl_remark')}（{lang('lbl_background_info')}）</div></td>
                <td>
                	<div class="longtext_limit">
                    	<span class="pre-wrap">{htmlspecialchars($client.remark)}</span>
                    </div>
                </td>
            </tr>

            {if !in_array($client.status, [$smarty.const.CLIENT_STATUS_CREDIT_WAITING, $smarty.const.CLIENT_STATUS_TRADING_NG]) }
                <tr>
                    <td>{lang('lbl_credit_result')}</td>
                    <td>
                        <div>{lang('lbl_credit_level')}: {if isset($credit_level_options[$client.credit_level])}{$this->client_model->credit_level_options($client.credit_level)}{/if}</div>
                        <div>
                            {lang('lbl_credit_limit')}: {number_format($client.credit_amount)}{lang('common_lbl_yen')}
                            {if $client.is_ad_receive_payment}
                                <span class="ml10">{lang('lbl_advance_payment')}</span>
                            {/if}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>{lang('lbl_comment')}</td>
                    <td>
                        <span class="pre-wrap">{htmlspecialchars($client.comment)}</span>
                    </td>
                </tr>
            {/if}
        </table>

        <hr class="blank-10px" />

        {if $this->auth->has_permission('client/apply_retry_delete')
        && in_array($client.status, [$smarty.const.CLIENT_STATUS_CREDIT_WAITING]) && (isset($is_edit) && $is_edit)}
            <a href="javascript:void(0)" class="btn-small btn-green btn-reapp">{lang('btn_reapp_delete')}</a>
        {/if}

        {if ($this->auth->has_permission('client/input_credit') || $this->auth->has_permission('client/apply_retry_delete'))
        && (!in_array($client.status, [$smarty.const.CLIENT_STATUS_CREDIT_WAITING,$smarty.const.CLIENT_STATUS_CREDIT_EXPIRED, $smarty.const.CLIENT_STATUS_CL_APPROVAL_REJECTED, $smarty.const.CLIENT_STATUS_TRADING_NG]))
		&& (isset($is_edit) && $is_edit)
        }
            <a href="javascript:void(0)" class="btn-small btn-green btn-edit">{lang('common_lbl_edit')}</a>
        {/if}

        {if $this->auth->has_permission('client/apply_retry_delete') && (isset($is_edit) && $is_edit)}
            <a href="javascript:void(0)" class="btn-small btn-green btn-reapp copy">{lang('btn_client_copy')}</a>
        {/if}

        {if $this->auth->has_permission('client/input_credit')
        && $client.status == $smarty.const.CLIENT_STATUS_CREDIT_WAITING && (isset($is_edit) && $is_edit)}
            <a href="javascript:void(0)" class="btn-small btn-green btn-credit">{lang('btn_input_credit_result')}</a>
        {/if}

        {if $this->auth->has_permission('client/apply_approval')
        && $client.status == $smarty.const.CLIENT_STATUS_CREDIT_ALREADY && (isset($is_edit) && $is_edit)}
            <a href="javascript:void(0)" class="btn-small btn-green btn-apply-approval">{lang('btn_cl_app_approval')}</a>
        {/if}

        {if $this->auth->has_permission('client/approve_reject')
        && $client.status == $smarty.const.CLIENT_STATUS_CL_APPROVAL_PENDING && $can_approve_client && (isset($is_edit) && $is_edit)}
            <a href="javascript:void(0)" class="btn-small btn-green btn-approve">{lang('btn_cl_approval_reject')}</a>
        {/if}

        <a href="javascript:void(0)" class="btn-small btn-green btn-close">{lang('common_lbl_close')}</a>

    </div>
</div>
<div id="dialog_2"></div>

<script>
//$('#dialog').dialog({
//    autoOpen : false,
//    width : 600,
//    height : 800,
//    modal : true,
//    open: function(){
//        jQuery('.ui-widget-overlay').bind('click',function(){
//            //jQuery('#dialog').dialog('close');
//        })
//    },
//    close: function() {
//        form_changed = false;
//    }
//});

{if isset($dialog_selector)}
    var cl_dialog = $('{$dialog_selector}');
{else}
    var cl_dialog = $('#dialog');
{/if}

$('.cl_content .btn-reapp').click(function() {
    var copy = 0;
    if ($(this).hasClass('copy')) {
        copy = 1;
    }
    cl_dialog.load('/_admin/client/apply_retry_delete', {
         id: {$client.id},
         copy: copy
    }, function() {
        if (!copy) {
            cl_dialog.dialog('option', 'title', '{lang('btn_reapp_delete')}');
        } else {
            cl_dialog.dialog('option', 'title', '{lang('btn_new_application')}');
        }
        cl_dialog.dialog( "option", "width", 'auto' );
        cl_dialog.dialog( "option", "height", 'auto' );
        cl_dialog.dialog("open");
    });
});
$('.cl_content .btn-credit').click(function() {
    cl_dialog.load('/_admin/client/input_credit', {
         id: {$client.id},
         gui_parts: {json_encode([
                  'payment_closing_date_options' => $payment_closing_date_options,
                  'payment_date_options' => $payment_date_options,
                  'payment_month_options' => $payment_month_options,
                  'business_category_options' => $business_category_options
          ])},
    }, function() {
        cl_dialog.dialog('option', 'title', '{lang('btn_input_credit_result')}');
        cl_dialog.dialog( "option", "width", 'auto' );
        cl_dialog.dialog( "option", "height", 'auto' );
        cl_dialog.dialog("open");
    });
});
$('.cl_content .btn-approve').click(function() {
    cl_dialog.load('/_admin/client/approve_reject', {
         id: {$client.id},
         gui_parts: {json_encode([
                  'payment_closing_date_options' => $payment_closing_date_options,
                  'payment_date_options' => $payment_date_options,
                  'payment_month_options' => $payment_month_options,
                  'credit_level_options' => $credit_level_options,
                  'business_category_options' => $business_category_options
          ])},
    }, function() {
        cl_dialog.dialog('option', 'title', '{lang('btn_cl_approval_reject')}');
        cl_dialog.dialog( "option", "width", 'auto' );
        cl_dialog.dialog( "option", "height", 'auto' );
        cl_dialog.dialog("open");
    });
});
$('.cl_content .btn-edit').click(function() {
    cl_dialog.load('/_admin/client/edit', {
         id: {$client.id},
    }, function() {
        cl_dialog.dialog('option', 'title', '{lang('common_lbl_edit')}');
        cl_dialog.dialog( "option", "width", 'auto' );
        cl_dialog.dialog( "option", "height", 'auto' );
        cl_dialog.dialog("open");
    });
});
$('.cl_content .btn-apply-approval').click(function() {
    confirm_dialog(lang['message_confirm_apply'], {
        'ok' : function() {
            $.post('apply_approval', { id: {$client.id} }, function(result) {
                if (result.status) {
                    $('#search_form').submit();
                } else {
                    $('.result_message').text(result.message);
                    $('.result_message').addClass('caution');
                }
            });
            $(this).dialog('close');
        }
    });
});
cl_dialog.on('click', '.btn-close', function() {
    cl_dialog.dialog('close');
});
</script>