{$disable_input_credit = ''}
{$disabled_change_sales = ''}
{if !$this->auth->has_permission('client/input_credit')}
    {$disable_input_credit = 'disabled'}
{/if}
{if !$this->auth->has_permission('change_sales_staff', 'column')}
    {$disabled_change_sales = 'disabled'}
{/if}
<link href="/public/admin/client/main.css" rel="stylesheet">
<style>
.ui-dialog-content a {
    color: #fff;
}
#input-table td:last-child {
    min-width: 750px;
}
</style>

<div class="cl_content">
    <div>
        <div class="message"></div>

        <hr class="blank-10px" />

        {form_open('', ['id' => 'edit_form'], ['id' => $client->id])}

        <table id="input-table" class="table-simple">
            <tr>
                <td>{lang('lbl_name')} <span class="red-symbol">※</span></td>
                <td>
                    <span style="width: 27%; float:left;">
                        {form_dropdown([
                            'name' => 'corporate_status_before',
                            'class' => 'company_title prefix',
                            'items' => ['' => lang('common_select')] + $client->company_title_options(),
                            'value' => set_value('corporate_status_before', $client->corporate_status_before),
                            $disable_input_credit => $disable_input_credit
                        ])}
                        <div class="error"></div>
                    </span>
                    <span style="width: 42%; float:left;">
                        {form_input([
                            'id'   => 'name',
                            'name' => 'name',
                            'class' => 'xs',
                            'value' => set_value('name', $client->name, false),
                            $disable_input_credit => $disable_input_credit
                        ])}
                        <div class="error"></div>
                    </span>
                    <span style="width: 27%; float:left;">
                        {form_dropdown([
                            'name' => 'corporate_status_after',
                            'class' => 'company_title suffix',
                            'items' => ['' => lang('common_select')] + $client->company_title_options(),
                            'value' => set_value('corporate_status_after', $client->corporate_status_after),
                            $disable_input_credit => $disable_input_credit
                        ])}
                    </span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_name_kana')} <span class="red-symbol">※</span></td>
                <td>
                    <span style="width: 27%; float:left;">&nbsp;</span>
                    <span style="width: 42%; float:left;">
                        {form_input([
                            'id'   => 'name_kana',
                            'name' => 'name_kana',
                            'class' => 'xs',
                            'value' => set_value('name_kana', $client->name_kana, false),
                            $disable_input_credit => $disable_input_credit
                        ])}
                        <div class="legend">{lang('lbl_kana_auto')}</div>
                        <div class="error"></div>
                    </span>
                    <span style="width: 27%; float:left;">&nbsp;</span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_zipcode')} <span class="red-symbol">※</span></td>
                <td>
                    {form_input([
                        'name' => 'zipcode',
                        'class' => 'ss',
                        'maxlength' => '7',
                        'value' => set_value('zipcode', $client->zipcode, false)
                    ])}
                    <div class="legend">{lang('lbl_no_hyphen')}</div>
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_address')}１ <span class="red-symbol">※</span></td>
                <td>
                    {form_input([
                        'name' => 'address_1st',
                        'class' => 'xxs',
                        'value' => set_value('address_1st', $client->address_1st, false)
                    ])}
                    <div class="legend">{lang('lbl_enter_2byte')}</div>
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_address')}２</td>
                <td>
                    {form_input([
                        'name' => 'address_2nd',
                        'class' => 'xxs',
                        'value' => set_value('address_2nd', $client->address_2nd, false)
                    ])}
                    <div class="legend">{lang('lbl_enter_2byte')}</div>
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_representative_name')} <span class="red-symbol">※</span></td>
                <td>
                    {form_input([
                        'name' => 'representative_name',
                        'value' => set_value('representative_name', $client->representative_name, false)
                    ])}
                    {lang('lbl_mr')}
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_tel')} <span class="red-symbol">※</span></td>
                <td>
                    {form_input([
                        'name' => 'phone_no',
                        'value' => set_value('phone_no', $client->phone_no, false)
                    ])}
                    <div class="legend">{lang('lbl_enter_hyphen')}</div>
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_fax')}</td>
                <td>
                    {form_input([
                        'name' => 'fax_no',
                        'value' => set_value('fax_no', $client->fax_no, false)
                    ])}
                    <div class="legend">{lang('lbl_enter_hyphen')}</div>
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_business_category')} <span class="red-symbol">※</span></td>
                <td>
                    {form_dropdown([
                        'name' => 'business_category',
                        'items' => ['' => lang('common_select')] + $client->business_category_options(),
                        'value' => set_value('business_category', $client->business_category)
                    ])}
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_division')}</td>
                <td>
                    {form_input([
                        'name' => 'division_name',
                        'class' => 'xxs',
                        'value' => set_value('division_name', $client->division_name, false)
                    ])}
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_customer_contact')} <span class="red-symbol">※</span></td>
                <td>
                    {form_input([
                        'name' => 'charge_name',
                        'value' => set_value('charge_name', $client->charge_name, false)
                    ])}
                    {lang('lbl_mr')}
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_mail_address')}</td>
                <td>
                    {form_input([
                        'name' => 'mail_address',
                        'class' => 'xxs',
                        'value' => set_value('mail_address', $client->mail_address, false)
                    ])}
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_payment_terms')} <span class="red-symbol">※</span></td>
                <td>
                    <span style="width: 28%; float:left;">
                        {lang('lbl_closing_date')}&nbsp;
                        {form_dropdown([
                            'name' => 'closing_date',
                            'items' => ['' => lang('common_select')] + $client->payment_closing_date_options(),
                            'value' => set_value('closing_date', $client->closing_date),
                            $disable_input_credit => $disable_input_credit
                        ])} {lang('common_lbl_date')}
                        <div class="error"></div>
                    </span>

                    <span style="width: 28%; float:left;">
                        {lang('lbl_payment_month')}
                        {form_dropdown([
                            'name' => 'payment_month_cd',
                            'items' => ['' => lang('common_select')] + $client->payment_month_options(),
                            'value' => set_value('payment_month_cd', $client->payment_month_cd),
                            $disable_input_credit => $disable_input_credit
                        ])} {lang('common_lbl_month')}
                        <div class="error"></div>
                    </span>

                    <span style="width: 28%; float:left;">
                        {lang('lbl_payment_date')}
                        {form_dropdown([
                            'name' => 'payment_day_cd',
                            'items' => ['' => lang('common_select')] + $client->payment_date_options(),
                            'value' => set_value('payment_day_cd', $client->payment_day_cd),
                            $disable_input_credit => $disable_input_credit
                        ])} {lang('common_lbl_date')}
                        <div class="error"></div>
                    </span>

                    <span style="width: 10%; float:left; margin-top: 3px">
                        <label>
                            {form_checkbox([
                                'name'    => 'is_ad_receive_payment_request',
                                'value'   => 1,
                                'checked' => set_checkbox('is_ad_receive_payment_request', 1, $client->is_ad_receive_payment_request == 1),
                                'style'   => 'vertical-align: middle',
                                $disable_input_credit => $disable_input_credit
                            ])}
                            {lang('lbl_advance_payment')}
                        </label>
                    </span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_sales_representative')}</td>
                <td>
                    <div style="display: inline-block; margin-right: 10px;">
                        {lang('lbl_sales_staff')}１<span class="red-symbol">※</span>
                        {form_dropdown([
                            'name' => 's_account_id_1st',
                            'items' => ['' => lang('common_select')] + $accounts,
                            'value' => set_value('s_account_id_1st', $client->s_account_id_1st),
                            $disabled_change_sales => $disabled_change_sales
                        ])}
                        <div class="error"></div>
                    </div>

                    <div style="display: inline-block; vertical-align: top;">
                        {lang('lbl_sales_staff')}２
                        {form_dropdown([
                            'name' => 's_account_id_2nd',
                            'items' => ['' => lang('common_select')] + $accounts,
                            'value' => set_value('s_account_id_2nd', $client->s_account_id_2nd),
                            $disabled_change_sales => $disabled_change_sales
                        ])}
                        <div class="error"></div>
                    </div>
                </td>
            </tr>

            <tr>
                <td>{lang('lbl_credit_info')}</td>
                <td id="credit-block">
                    <div class="mb10">
                        <label>
                            {form_checkbox([
                                'name'    => 'update_credit',
                                'value'   => 1,
                                $disable_input_credit => $disable_input_credit
                            ])}
                            {lang('lbl_update_recredit_date')}
                        </label>
                    </div>
                    <div class="mb10">
                        {lang('lbl_credit_level')}
                        {form_dropdown([
                            'name' => 'credit_level',
                            'items' => ['' => lang('common_select')] + $this->client_model->credit_level_options(),
                            'value' => set_value('credit_level', $client->credit_level),
                            'disabled' => 'disabled'
                        ])}
                        <div class="error"></div>
                    </div>
                    <div class="mb10">
                        <span style="width: 25%; float:left;">
                            <label>
                                {form_checkbox([
                                    'name'    => 'is_ad_receive_payment',
                                    'value'   => 1,
                                    'checked' => set_checkbox('is_ad_receive_payment', 1, $client->is_ad_receive_payment == 1),
                                    'disabled' => 'disabled'
                                ])}
                                {lang('lbl_advance_payment')}
                                <div class="error"></div>
                            </label>
                        </span>

                        <span style="width: 30%; float:left;">
                            {lang('lbl_credit_amount')}
                            {form_input([
                                'name' => 'credit_amount',
                                'class' => 'ss',
                                'value' => set_value('credit_amount', number_format($client->credit_amount)),
                                'disabled' => 'disabled'
                            ])}
                            {lang('common_lbl_yen')}
                            <div class="error"></div>
                        </span>
                    </div>
                    <div style="clear: both">{lang('lbl_comment')}</div>
                    <div>
                        {form_textarea([
                            'name' => 'comment',
                            'value' => set_value('comment', $client->comment, false),
                            'rows' => 3,
                            'disabled' => 'disabled'
                        ])}
                        <div class="error"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_payment_account')}</td>
                <td>
                    {if empty($bank_accounts)}
                        <div>
                            {form_input([
                                'name' => 'bank_account[]',
                                'class' => '',
                                $disable_input_credit => $disable_input_credit
                            ])}
                            {if $disable_input_credit == ''}
                                <a href="javascript:void(0)" class="btn-small btn-green btn-add-bank-acc">{lang('common_lbl_add')}</a>
                                <a href="javascript:void(0)" class="btn-small btn-green btn-remove-bank-acc">{lang('common_lbl_delete')}</a>
                            {/if}
                        </div>
                    {else}
                        {foreach $bank_accounts as $name}
                            <div style="margin-bottom:5px">
                                {form_input([
                                    'name' => 'bank_account[]',
                                    'class' => '',
                                    'value' => set_value('bank_account[]', $name),
                                    $disable_input_credit => $disable_input_credit
                                ])}
                                {if $disable_input_credit == ''}
                                    <a href="javascript:void(0)" class="btn-small btn-green btn-add-bank-acc">{lang('common_lbl_add')}</a>
                                    <a href="javascript:void(0)" class="btn-small btn-green btn-remove-bank-acc">{lang('common_lbl_delete')}</a>
                                {/if}
                            </div>
                        {/foreach}
                    {/if}
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <button name="action" class="btn-small btn-green" value="edit">{lang('common_lbl_save')}</button>
                    <a href="javascript:void(0)" class="btn-small btn-green btn-close">{lang('btn_back_to_list')}</a>
                    <button name="action" class="btn-small btn-green" value="delete">{lang('common_lbl_delete')}</button>
                </td>
            </tr>
        </table>
        {form_close()}
    </div>
</div>
{*<script src="/public/admin/js/auto_ruby.jquery.js"></script>*}
<!--<script src="/public/admin/client/function.js"></script>-->
<script>
    company_title_handler();

//    convert_kana_handler();

    credit_input_handler();

    adv_receive_handler('{$disable_input_credit}');

    bind_save_key($('button[value=edit]'));
//#6869:Start
    $('[name=credit_amount]').inputNumberFormat();
//#6869:End
    $('[name=update_credit]').change(function() {
        var is_disable = !this.checked;
        $('[name=credit_level]').prop('disabled', is_disable);
        $('[name=is_ad_receive_payment]').prop('disabled', is_disable);
        $('[name=credit_amount]').prop('disabled', is_disable);
        $('[name=comment]').prop('disabled', is_disable);

        if (this.checked) {
            $('[name=credit_level]').trigger('change');
        }
    });

    $('#edit_form').on('click', '.btn-add-bank-acc', function() {
        var input_template = '<div style="margin-top:5px">'
                        +        '<input type="text" name="bank_account[]">'
                        +        ' <a href="javascript:void(0)" class="btn-small btn-green btn-add-bank-acc">{lang('common_lbl_add')}</a>'
                        +        ' <a href="javascript:void(0)" class="btn-small btn-green btn-remove-bank-acc">{lang('common_lbl_delete')}</a>'
                        +    '</div>';
        $(this).parents('td').append(input_template);
    });

    $('#edit_form').on('click', '.btn-remove-bank-acc', function() {
        if ($(this).parents('td').children('div').length > 1) {
            $(this).parent('div').remove();
        }
    });

    $('button[name=action]').click(function(e) {
        e.preventDefault();
        var action = $(this).val();
        var id = $('[name=id]').val();

        $('.error').html('');
        $('.message').removeClass('caution').removeClass('success-message');

        if (action == 'delete') {
            $.post('/_admin/client/edit', { id: id, action: 'check_order' }, function(result) {
                if (result.status) {
                    confirm_dialog(lang['message_confirm_delete'], {
                        'delete' : function() {
                            do_action('/_admin/client/edit', action, $('#edit_form').serialize());
                            $(this).dialog('close');
                        }
                    });
                } else {
                    $('.message').addClass('caution').text(result.message);
                }
            });
        } else {
//#6869:Start
            var credit_amount = $('[name=credit_amount]').val();
            $('[name=credit_amount]').val(credit_amount.replace(/,/g, ''));
            var data = $('#edit_form').serialize();
            $('[name=credit_amount]').val(credit_amount);
//#6869:End
            // Because disabled input will not be posted, append it to data variable
            $('#credit-block input:disabled').each(function() {
                if ($(this).attr('type') == 'checkbox') {
                    if ($(this).prop('checked')) {
                        data += '&' + $(this).attr('name') + '=' + $(this).val();
                    }
                } else {
                    data += '&' + $(this).attr('name') + '=' + $(this).val();
                }
            });
//#7158:Start
//            do_action('/_admin/client/edit', action, data, id);
            do_action('/_admin/client/edit', action, data, id, "#"+cl_dialog.attr('id'));
//#7158:End
        }
    });

    exit_confirm();
</script>