<link href="/public/admin/client/main.css" rel="stylesheet">
<style>
.ui-dialog-content a {
    color: #fff;
}
#view-table td:last-child {
    min-width: 600px;
}
</style>

<div class="cl_content">
    <div>
        <div class="message"></div>

        {form_open('', ['id' => 'credit_form'], ['id' => $client.id])}

        <table id="view-table" class="table-simple">
            <tr>
                <td>{lang('lbl_name')}</td>
                <td>{htmlspecialchars($this->client_model->name_with_title($client.name, $client.corporate_status_before, $client.corporate_status_after))}</td>
            </tr>
            <tr>
                <td>{lang('lbl_name_kana')}</td>
                <td>{htmlspecialchars($client.name_kana)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_zipcode')}</td>
                <td>{substr($client.zipcode, 0, 3)}-{substr($client.zipcode, 3)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_address')}１</td>
                <td>{htmlspecialchars($client.address_1st)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_address')}２</td>
                <td>{htmlspecialchars($client.address_2nd)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_representative_name')}</td>
                <td>{htmlspecialchars($client.representative_name)}</td>
            </tr>
            <tr>
                <td>{lang('lbl_tel')}/{lang('lbl_fax')}</td>
                <td>
                    <span>{lang('lbl_tel')}: {$client.phone_no}</span>
                    <span class="ml10">{lang('lbl_fax')}: {$client.fax_no}</span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_business_category')}</td>
                <td>
                    {if isset($business_category_options[$client.business_category])}
                        {$business_category_options[$client.business_category]}
                    {/if}
                </td>
            </tr>

            <tr>
                <td>{lang('lbl_payment_terms')}</td>
                <td>
                    {if !$client.is_ad_receive_payment_request}
                        <span>
                            {lang('lbl_closing_date')}:&nbsp;
                            {if isset($payment_closing_date_options[$client.closing_date])}
                                {$payment_closing_date_options[$client.closing_date]}{lang('common_lbl_date')}
                            {/if}
                        </span>
                        <span class="ml10">
                            {lang('lbl_payment_month')}:&nbsp;
                            {if isset($payment_month_options[$client.payment_month_cd])}
                                {$payment_month_options[$client.payment_month_cd]}{lang('common_lbl_month')}
                            {/if}
                        </span>
                        <span class="ml10">
                            {lang('lbl_payment_date')}:&nbsp;
                            {if isset($payment_date_options[$client.payment_day_cd])}
                                {$payment_date_options[$client.payment_day_cd]}{lang('common_lbl_date')}
                            {/if}
                        </span>
                    {else}
                        <span>{lang('lbl_advance_payment')}</span>
                    {/if}
                </td>
            </tr>

            <tr>
                <td>{lang('lbl_sales_representative')}</td>
                <td>
                    {strip}
                        {if isset($accounts[$client.s_account_id_1st])}
                            {htmlspecialchars($accounts[$client.s_account_id_1st]['name'])}
                        {/if}
                        {if isset($accounts[$client.s_account_id_2nd])}
                            , {htmlspecialchars($accounts[$client.s_account_id_2nd]['name'])}
                        {/if}
                    {/strip}
                </td>
            </tr>

            <tr>
                <td>{lang('lbl_first_month_delivery')}</td>
                <td style="min-width: 500px">
                    <span style="width: 22%; float:left;">{lang('lbl_delivery_month')}: {$client.delivery_month_cd}{lang('common_lbl_month')}</span>
                    <span style="width: 27%; float:left;">{lang('lbl_delivery_amount')}: {number_format($client.delivery_amount)}{lang('common_lbl_yen')}</span>
                    <span style="width: 23%; float:left;">{lang('lbl_profit_amount')}: {number_format($client.gross_profit_amount)}{lang('common_lbl_yen')}</span>
                    <span class="a-right" style="width: 25%; float:left;">{lang('lbl_profit_rate')}: {$client.gross_profit_rate}％</span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_year_total')}</td>
                <td>
                    <span style="width: 22%; float:left;">&nbsp;</span>
                    <span style="width: 27%; float:left;">{lang('lbl_delivery_amount')}: {number_format($client.total_delivery_amount)}{lang('common_lbl_yen')}</span>
                    <span style="width: 25%; float:left;">{lang('lbl_profit_amount')}: {number_format($client.total_gross_profit_amount)}{lang('common_lbl_yen')}</span>
                    <span class="a-right" style="width: 23%; float:left;">{lang('lbl_profit_rate')}: {$client.total_gross_profit_rate}％</span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_remark')}（{lang('lbl_background_info')}）</td>
                <td>
                    <span class="pre-wrap">{htmlspecialchars($client.remark)}</span>
                </td>
            </tr>

            <tr>
                <td>{lang('lbl_credit_level')} <span class="red-symbol">※</span></td>
                <td>
                    {form_dropdown([
                        'name' => 'credit_level',
                        'items' => ['' => lang('common_select')] + $credit_level_options
                    ])}
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_advance_payment')}/{lang('lbl_credit_amount')} <span class="red-symbol">※</span></td>
                <td>
                    <span style="width: 25%; float:left;">
                        <label>
                            {$disabled_is_ad_receive_payment = 'false'}
                            {$credit_amount_value = set_value('credit_amount')}
                            {if $client.is_ad_receive_payment_request == 1}
                                {$disabled_is_ad_receive_payment = 'true'}
                                {$credit_amount_value = 0}
                            {/if}
                            {form_checkbox([
                                'name'    => 'is_ad_receive_payment',
                                'value'   => 1,
                                'checked' => set_checkbox('is_ad_receive_payment', 1, $client.is_ad_receive_payment_request == 1),
                                'disabled' => ($disabled_is_ad_receive_payment)
                            ])}
                            {lang('lbl_advance_payment')}
                            <div class="error"></div>
                        </label>
                    </span>

                    <span style="width: 50%; float:left;">
                        {lang('lbl_credit_amount')}
                        {form_input([
                            'name' => 'credit_amount',
                            'class' => 'ss',
                            'value' => $credit_amount_value,
                            'disabled' => $disabled_is_ad_receive_payment
                        ])}
                        {lang('common_lbl_yen')}
                        <div class="error"></div>
                    </span>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_comment')} </td>
                <td>
                    {form_textarea([
                        'name' => 'comment',
                        'value' => set_value('comment', '', false),
                        'rows' => 3
                    ])}
                    <div class="error"></div>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <button type="submit" name="action" class="btn-small btn-green" value="submit">{lang('btn_credit_input')}</button>
                </td>
            </tr>

        </table>
        {form_close()}
    </div>
</div>

<!--<script src="/public/admin/client/function.js"></script>-->
<script>
    credit_input_handler({$disabled_is_ad_receive_payment});

    bind_save_key($('button[value=submit]'));
//#6869:Start
    $('[name=credit_amount]').inputNumberFormat();
//#6869:End
    $('[name=action]').click(function(e) {
        e.preventDefault();

//#6869:Start
        var credit_amount = $('[name=credit_amount]').val();
        $('[name=credit_amount]').val(credit_amount.replace(/,/g, ''));
        var data = $('#credit_form').serialize();
        $('[name=credit_amount]').val(credit_amount);
//#6869:End

        // Because disabled input will not be posted, append it to data variable
        $('#credit_form input:disabled').each(function() {
            if ($(this).attr('type') == 'checkbox') {
                if ($(this).prop('checked')) {
                    data += '&' + $(this).attr('name') + '=' + $(this).val();
                }
            } else {
                data += '&' + $(this).attr('name') + '=' + $(this).val();
            }
        });

        do_action('/_admin/client/input_credit', $(this).val(), data);
    });

    exit_confirm();
</script>