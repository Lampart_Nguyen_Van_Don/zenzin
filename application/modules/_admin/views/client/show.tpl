{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

{$account_options = map_element($account_data, 'id', 'name')}
{$account_options_group_by_business = map_element($account_data, 'id', 'name', 'business_unit_id')}
{$accounts = index_element($account_data, 'id')}

<script>
    var common_lbl_view = '{lang('common_lbl_view')}';
    var btn_order_manage = '{lang('btn_order_manage')}';
    var title_client_detail = '{lang('title_client_detail')}';
    var business_default = {if $accounts.{$login_account_id}.is_product_division}{$accounts.{$login_account_id}.business_unit_id}{else}''{/if};
    var client_status_name = {$client_status_name};
    var company_title = {$company_title};
    var is_edit_all = {$is_edit_all};
    var is_edit_only = {$is_edit_only};
    var is_add_order = {json_encode($this->auth->has_permission('order/add'))};
    var login_account_id = {$login_account_id};
    var accounts = {json_encode($accounts)};
    var status_is_edit = {json_encode([$smarty.const.CLIENT_STATUS_CL_APPROVED, $smarty.const.CLIENT_STATUS_CREDIT_EVERYTIME, $smarty.const.CLIENT_STATUS_RE_CREDIT_WAITING])};
    var is_load = {$is_load};
    var call_ajax_get_client = null;
</script>

<style>
#client-table td {
    min-width: 75px;
}
.search-status-row {
    width: 100%;
    min-width: 550px;
    max-width: 650px;
}
.search-status {
    display: inline-block;
    width: 30%;
}
.order-dialog .ui-widget-content{
        border: none !important;
    }
    .order-dialog .ui-dialog-buttonpane {
        text-align: center !important;
        padding: 0px !important;
    }

    .order-dialog .ui-dialog-buttonset {
        float: none !important;
    }

    .cell-template-data {
        width: 100%;
        height: 100%;
        line-height: 30px;
    }

    input.ng-scope {
        line-height: 30px !important;
    }

    select.ng-scope {
        height: 30px !important;
    }
</style>
<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>

        {form_open('', ['id' => 'search_form'])}
        <div style="display: inline-block">
            <table id="search_toggle" class="table-simple" style="width: 100%">
                <tr>
                    <td id="search_toggle" style="padding-left: 10px; border-bottom: none">
                        <a href="javascript:void(0)">{lang('lbl_hide_search')}</a>
                    </td>
                </tr>
            </table>
            <div id="search_body" style="display: inline-block">
                <table class="table-simple">
                    <tbody>
                    <tr>
                        <th>
                            <div>{lang('lbl_client')}</div>
                            <div>{lang('lbl_status')}</div>
                        </th>
                        <td>
                            <div class="search-status-row">
                            {$count = 1}
                            {$status_options = $model->status_options()}
                            {foreach $status_options as $k => $v}
                                <div class="search-status">
                                    <label>
                                    {form_checkbox([
                                        'name'    => 'search_status[]',
                                        'value'   => $k,
                                        'checked' => set_checkbox('search_status[]', $k)
                                    ])}
                                    {$v}
                                    </label>
                                </div>
                                {if $count++ % 3 == 0}
                                    </div><div class="search-status-row">
                                {/if}
                            {/foreach}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_s_code')}</th>
                        <td>{form_input([
                                'name' => 'search_s_code',
                                'value' => set_value('search_s_code', '', false)
                            ])}
                            <div class="legend">{lang('lbl_prefix_search')}</div>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_name')}</th>
                        <td>{form_input([
                                'name' => 'search_name',
                                'value' => set_value('search_name', '', false),
                                'class' => 'xs'
                            ])}
                            <div class="legend">{lang('lbl_like_search')}</div>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_name_kana')}</th>
                        <td>{form_input([
                                'name' => 'search_name_kana',
                                'value' => set_value('search_name_kana', '', false),
                                'class' => 'xs'
                            ])}
                            <div class="legend">{lang('lbl_like_search')}</div>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_division')}</th>
                        <td>{form_input([
                                'name' => 'search_division_name',
                                'value' => set_value('search_division_name', '', false)
                            ])}
                            <div class="legend">{lang('lbl_like_search')}</div>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_tel')}</th>
                        <td>{form_input([
                                'name' => 'search_phone_no',
                                'value' => set_value('search_phone_no', '', false)
                            ])}
                            <div class="legend">{lang('lbl_like_search')}</div>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_zipcode')}</th>
                        <td>{form_input([
                                'name' => 'search_zipcode',
                                'class' => 'ss',
                                'maxlength' => '7',
                                'value' => set_value('search_zipcode')
                            ])}
                            <div class="legend">{lang('lbl_exact_search')}</div>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_sales_representative')}</th>
                        <td>
                            <div style="display: inline-block; margin-right: 10px;">
                                {if $accounts.{$login_account_id}.is_product_division}
                                    {$default_bu = $accounts.{$login_account_id}.business_unit_id}
                                {else}
                                    {$default_bu = ''}
                                {/if}
                                {lang('lbl_department')}&nbsp;
                                {form_dropdown([
                                    'name' => 'search_business',
                                    'items' => [0 => lang('common_lbl_all')] + $business_unit_options,
                                    'value' => set_value('search_business', $default_bu)
                                ])}
                            </div>

                            <div style="display: inline-block;">
                                {lang('lbl_sales_staff')}

                                {if isset($account_options_group_by_business[set_value('search_business', $default_bu)])}
                                    {$account_items = [0 => lang('common_lbl_all')] + $account_options_group_by_business[set_value('search_business', $default_bu)]}
                                {elseif set_value('search_business', $default_bu) == 0}
                                    {$account_items = [0 => lang('common_lbl_all')] + $account_options}
                                {else}
                                    {$account_items = [0 => lang('common_lbl_all')]}
                                {/if}

                                {form_dropdown([
                                    'name' => 'search_account',
                                    'data-parent' => 'search_business',
                                    'data-parent-null-action' => 'load_all',
                                    'items' => $account_items,
                                    'value' => set_value('search_account'),
                                    'data-group-items' => htmlspecialchars(json_encode($account_options_group_by_business))
                                ])}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_payment_terms')}</th>
                        <td>
                            {lang('lbl_closing_date')}&nbsp;
                            {form_dropdown([
                                'name' => 'search_closing_date',
                                'items' => ['' => lang('common_lbl_all')] + $model->payment_closing_date_options(),
                                'value' => set_value('search_closing_date')
                            ])} {lang('common_lbl_date')}

                            <span class="ml10">
                                {lang('lbl_payment_month')}
                                {form_dropdown([
                                    'name' => 'search_payment_month_cd',
                                    'items' => ['' => lang('common_lbl_all')] + $model->payment_month_options(),
                                    'value' => set_value('search_payment_month_cd')
                                ])} {lang('common_lbl_month')}
                            </span>

                            <span class="ml10">
                                {lang('lbl_payment_date')}
                                {form_dropdown([
                                    'name' => 'search_payment_day_cd',
                                    'items' => ['' => lang('common_lbl_all')] + $model->payment_date_options(),
                                    'value' => set_value('search_payment_day_cd')
                                ])} {lang('common_lbl_date')}
                            </span>

                            <span class="ml10">
                                <label>
                                    {form_checkbox([
                                        'name' => 'search_is_ad_receive_payment_request',
                                        'value' => 1,
                                        'checked' => set_checkbox('search_is_ad_receive_payment_request', 1)
                                    ])}
                                    {lang('lbl_advance_payment')}
                                </label>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_s_code_issue_date')}</th>
                        <td>
                            {form_input([
                                'name' => 'search_issue_s_code_date_from',
                                'class' => 'ss',
                                'value' => set_value('search_issue_s_code_date_from', '', false)
                            ])}
                            ~
                            {form_input([
                                'name' => 'search_issue_s_code_date_to',
                                'class' => 'ss',
                                'value' => set_value('search_issue_s_code_date_to', '', false)
                            ])}
                            {lang('lbl_search_range')}
                            <div class="legend">{lang('lbl_search_range_instruction')}</div>
                            {foreach $date_errors as $error}
                                <div class="validation-error">{$error}</div>
                            {/foreach}
                        </td>
                    </tr>
                    <tr>
                        <td class="a-center" colspan="2">
                            <button type="submit" class="btn-small btn-green btn-search">{lang('common_lbl_search')}</button>
                        </td>
                    </tr>
                </tbody>
                </table>
            </div>
        </div>

        <hr class="blank-10px" />

        {form_close()}

        <hr class="blank-10px" />
        <h1></h1>
        <hr class="blank-10px" />

        <div class="client-buttons" style="margin-bottom: 10px">
            {if $this->auth->has_permission('client/apply_retry_delete')}
                <a href="javascript:void(0)" class="btn-small btn-green btn-add">{lang('btn_new_application')}</a>
            {/if}
            {if $this->auth->has_permission('client/export_csv_full')}
                <a href="javascript:void(0)" class="btn-small btn-green export_csv full">{lang('btn_download_csv')}</a>
            {/if}
            {if $this->auth->has_permission('client/export_csv')}
                <a href="javascript:void(0)" class="btn-small btn-green export_csv">{lang('btn_download_csv_in_charge_account')}</a>
            {/if}
        </div>

        <div class="" style="overflow-x: auto; width: 100%">
{*#7364:Start*}
        {if empty($clients)}
        <table class="table-theme odd client-table">
            <thead>
                <tr>
                    <th>{lang('lbl_action')}</th>
                    <th>
                        <div>{lang('lbl_client')}</div>
                        <div>{lang('lbl_status')}</div>
                    </th>
                    <th>{lang('lbl_s_code')}</th>
                    <th>{lang('lbl_name')}</th>
                    <th>{lang('lbl_division')}</th>
                    <th>{lang('lbl_charge_name')}</th>
                    <th>{lang('lbl_zipcode')}</th>
                    <th>{lang('lbl_address')}１</th>
                    <th>{lang('lbl_address')}２</th>
                    <th>{lang('lbl_sales_representative')}１</th>
                    <th>{*{lang('lbl_credit_result')}-*}{lang('lbl_credit_amount')}</th>
                </tr>
            </thead>
            <tbody>
                <tr><td class="a-center" colspan="11">{lang('common_msg_no_data')}</td></tr>
            </tbody>
        </table>
        {/if}
{*#7364:End*}
        <table id="client-table" class="table-theme odd client-table">
{*#7364:Start*}
        {if $clients}
            <thead>
                <tr>
                    <th>{lang('lbl_action')}</th>
                    <th>
                        <div>{lang('lbl_client')}</div>
                        <div>{lang('lbl_status')}</div>
                    </th>
                    <th>{lang('lbl_s_code')}</th>
                    <th>{lang('lbl_name')}</th>
                    <th>{lang('lbl_division')}</th>
                    <th>{lang('lbl_charge_name')}</th>
                    <th>{lang('lbl_zipcode')}</th>
                    <th>{lang('lbl_address')}１</th>
                    <th>{lang('lbl_address')}２</th>
                    <th>{lang('lbl_sales_representative')}１</th>
                    <th>{*{lang('lbl_credit_result')}-*}{lang('lbl_credit_amount')}</th>
                </tr>
            </thead>
        {/if}
{*#7364:End*}
            <tbody>
                {foreach $clients as $client}
{*#7364:Start*}
                {*<tr data-id="{$client.id}" {if $client.status == $smarty.const.CLIENT_STATUS_CL_APPROVAL_REJECTED} class="danger"{elseif $client.status == $smarty.const.CLIENT_STATUS_TRADING_NG} class="gray"{/if}>*}
                <tr data-id="{$client.id}" class="full-size-row{if $client.status == $smarty.const.CLIENT_STATUS_CL_APPROVAL_REJECTED} danger{elseif $client.status == $smarty.const.CLIENT_STATUS_TRADING_NG} gray{/if}">
{*#7364:End*}
                    <td style="white-space: nowrap;">
                        <a href="javascript:void(0)" class="btn-small btn-green btn-view view">{lang('common_lbl_view')}</a>
{*#7427:Start*}
                        {*if in_array($client.status, [$smarty.const.CLIENT_STATUS_CL_APPROVED, $smarty.const.CLIENT_STATUS_CREDIT_EVERYTIME, $smarty.const.CLIENT_STATUS_RE_CREDIT_WAITING])*}
                        {if in_array($client.status, [$smarty.const.CLIENT_STATUS_CL_APPROVED, $smarty.const.CLIENT_STATUS_CREDIT_EVERYTIME])}
{*#7427:End*}
{*#8354:Start*}
                            {*{$in_charge_bu = []}*}
                            {*{if isset($accounts.{$client.s_account_id_1st})}*}
                                {*{$in_charge_bu[] = $accounts.{$client.s_account_id_1st}.business_unit_id}*}
                            {*{/if}*}
                            {*{if isset($accounts.{$client.s_account_id_2nd})}*}
                                {*{$in_charge_bu[] = $accounts.{$client.s_account_id_2nd}.business_unit_id}*}
                            {*{/if}*}
                            {*{if ($is_edit_all && in_array($accounts.{$login_account_id}.business_unit_id, $in_charge_bu))*}
                                {*|| ($is_edit_only && in_array($login_account_id, [$client.s_account_id_1st, $client.s_account_id_2nd]))}*}
                            {if $this->auth->has_permission('order/add')}
{*#8354:End*}
                                <a href="javascript:void(0)" ng-click="showAddByClient({$client.id})" class="btn-small btn-green">{lang('btn_order_manage')}</a>
                            {/if}
                        {/if}
                    </td>
                    <td>
                        {$model->status_options($client.status)}
                        {*htmlspecialchars($client.gui_part_element_name)*}
                    </td>
                    <td>{htmlspecialchars($client.s_code)}</td>
                    <td style="min-width: 400px">
                        <a class="view">
                            {htmlspecialchars($this->client_model->name_with_title($client.name, $client.corporate_status_before, $client.corporate_status_after))}
                        </a>
                    </td>
                    <td>{htmlspecialchars($client.division_name)}</td>
                    <td>{htmlspecialchars($client.charge_name)}</td>
                    <td>{substr($client.zipcode, 0, 3)}-{substr($client.zipcode, 3)}</td>
                    <td style="max-width: 100px">{htmlspecialchars($client.address_1st)}</td>
                    <td style="max-width: 100px">{htmlspecialchars($client.address_2nd)}</td>
                    <td>
                        {*if isset($accounts[$client.s_account_id_1st])}
                            {htmlspecialchars($accounts[$client.s_account_id_1st]['name'])}
                        {/if*}
                        {htmlspecialchars($client.acc_1_name)}
                    </td>
                    <td class="a-right">{number_format($client.credit_amount)}</td>
                </tr>
                {/foreach}
            </tbody>
        </table>
        </div>

        <div class="ajax-loading"><img src="{base_url('/public/admin/img/ajax-loader.gif')}" alt="Ajax loading" /></div>

        <div class="client-buttons" style="margin-top: 10px">
            {if $this->auth->has_permission('client/apply_retry_delete')}
                <a href="javascript:void(0)" class="btn-small btn-green btn-add">{lang('btn_new_application')}</a>
            {/if}
            {if $this->auth->has_permission('client/export_csv_full')}
                <a href="javascript:void(0)" class="btn-small btn-green export_csv full">{lang('btn_download_csv')}</a>
            {/if}
            {if $this->auth->has_permission('client/export_csv')}
                <a href="javascript:void(0)" class="btn-small btn-green export_csv">{lang('btn_download_csv_in_charge_account')}</a>
            {/if}
        </div>
    </div>
</div>

{form_open('', [
    'id' => 'client_form'
])}
{*form_hidden('client_data', json_encode($clients))*}
{form_hidden('search_data', json_encode($data_search))}
{form_hidden('business_default', $default_bu)}
{form_close()}

<div id="dialog"></div>
<div id="dialogAdd">
    {*$clients = $od_clients*}
    {$clients = null}
    {include file="order/add.tpl"}
</div>
<div id="message_alert"></div>
<!-- Dialog Box which shows error messages -->
<div id="dialog-errors"></div>
<script>
var modified = false;

//#7846:Start
// var app = angular.module('app', ['ngTouch', 'ngSanitize', 'ui.grid', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.pagination', 'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.cellNav']);
var app = angular.module('app', ['ngSanitize', 'ui.grid', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.pagination', 'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.cellNav']);
//#7846:End
app.filter('mapTaxType', function() {
    return function(input) {
        if (!input) {
            return '';
        } else {
            for (i = 0; i < tax_division.length; i++) {
                if (tax_division[i].code == input) {
                    return tax_division[i].name;
                }
            }
        }
    };
});
var products = {$products};
var tax_division = {$tax_division};
var my_account = {$this->order_model->my_account};
var j_accounts = {json_encode($j_accounts)};
var jsGlobals = [
    lbl_branch_cd = '{lang("lbl_branch_cd")}',
    lbl_order_status = '{lang("lbl_order_status")}',
    lbl_change_report = '{lang("lbl_change_report")}',
    lbl_product_name = '{lang("lbl_product_name")}',//3
    lbl_detail_contents = '{lang("lbl_detail_contents")}',
    lbl_delivery_date = '{lang("lbl_delivery_date")}',//5
    lbl_billing_date = '{lang("lbl_billing_date")}',
    lbl_payment_date = '{lang("lbl_payment_date")}',//7
    lbl_quantity = '{lang("lbl_quantity")}',
    lbl_tax_type = '{lang("lbl_tax_type")}',
    lbl_unit_price = '{lang("lbl_unit_price")}',
    lbl_amount = '{lang("lbl_amount")}',//11
    lbl_order_detail_cost_unit_price = '{lang("lbl_order_detail_cost_unit_price")}',
    lbl_order_detail_cost_total_price = '{lang("lbl_order_detail_cost_total_price")}',
    lbl_gross_profit_amount = '{lang("lbl_gross_profit_amount")}',
    lbl_order_detail_gross_profit_rate = '{lang("lbl_order_detail_gross_profit_rate")}',
    lbl_j_code = '{lang("lbl_j_code")}',
    lbl_s_code = '{lang("lbl_s_code")}',
    lbl_account_id = '{lang("lbl_account_id")}',
    lbl_is_ad_sales_slip_input = '{lang("lbl_is_ad_sales_slip_input")}',
    lbl_is_ad_invoice_issue = '{lang("lbl_is_ad_invoice_issue")}',
    lbl_is_order_input = '{lang("lbl_is_order_input")}',
    lbl_is_acceptance_input = '{lang("lbl_is_acceptance_input")}',
    lbl_is_sales_slip_input = '{lang("lbl_is_sales_slip_input")}',
    lbl_is_invoice_issue = '{lang("lbl_is_invoice_issue")}',
    lbl_name = '{lang("lbl_name")}',
    lbl_content = '{lang("lbl_content")}',
    lbl_action = '{lang("lbl_action")}',//27
    lbl_change_report = '{lang("lbl_change_report")}',
    lbl_copy_branch = '{lang("lbl_copy_branch")}',
    lbl_delete_branch = '{lang("lbl_delete_branch")}',
    lbl_cancel_branch = '{lang("lbl_cancel_branch")}',
    lbl_show = '{lang("lbl_show")}',
    lbl_check_approve_more = '{lang("lbl_check_approve_more")}'
];

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

$(document).ready(function() {
    var csv_url = 'export_csv';
    var full_csv_url = 'export_csv_full';
    var data = $('[name=client_data]').val();
    var $dialog = $('#dialog');

    $dialog.dialog({
        autoOpen : false,
        modal : true,
        open: function(){
//             jQuery('.ui-widget-overlay').bind('click',function(){
//                 jQuery('#dialog').dialog('close');
//             })
        },
        beforeClose: function( event, ui ) {
            var close = close_confirm();
            return close;
        },
        close : function() {
            form_changed = false;
//             $(window).unbind('beforeunload');

            if (modified) {
                $('#search_form').submit();
            }
            if (typeof cl_functions_loaded !== 'undefined' && cl_functions_loaded) {
                unbind_save_key();
            }
        }
//        height: 'auto',
//        width: 'auto'
    });

    $('.btn-search').on('click', function() {
        form_changed = false;
        form_submitted = true;
        $("#search_form").submit();
    });

    $('.export_csv').click(function() {
        if ($(this).hasClass('full')) {
            $('#client_form').attr('action', full_csv_url);
        } else {
            $('#client_form').attr('action', csv_url);
        }
        $('#client_form').submit();
    });

    $('.client-buttons .btn-add').click(function() {
        $dialog.load('/_admin/client/apply_retry_delete', {
             redirect_form_post: redirect_data()
        }, function() {
            $dialog.dialog('option', 'title', '{lang('btn_new_application')}');
            $dialog.dialog('option', 'width', 'auto');     //1000
            $dialog.dialog('option', 'height', 'auto');    //600
            $dialog.dialog("open");
        });
    });

    $("#dialogAdd").dialog({
        autoOpen : false,
        width : "auto",
        height : "auto",
        modal : true,
        buttons : {
        },
        beforeClose: function( event, ui ) {
            return close_confirm();
        },
        close : function() {
            if ($("#dialog-errors").hasClass('ui-dialog-content')) {
                $("#dialog-errors").dialog('destroy');
            }
        }
    });
    exit_confirm();

    $('body').on('click', '.btn-close', function() {
        $(this).parents('.ui-dialog-content').dialog('close');
//        $('.ui-dialog-content').dialog('close');
    });

    lbl_open_search = '{lang('lbl_open_search')}';
    lbl_hide_search = '{lang('lbl_hide_search')}';

    if (loadData('cl_showSearch') == 0) {
        hideSearch();
    } else {
        showSearch();
    }

    $('#search_toggle').click(function () {
        if ($('#search_body').is(':visible')) {
            hideSearch(true);
        } else {
            showSearch(true);
        }
    });

    // fixed thead
    $('table#client-table').floatThead({
{*#7364:Start*}
        position: 'absolute',
{*#7364:End*}
        top: 61,
        zIndex: 1,
{*#7364:Start*}
        getSizingRow: function($table){ // this is only called when using IE
            return $table.find('tbody tr.full-size-row:first>*');
{*#7364:End*}
        }
    });

    $(document).ready(function() {
        $(window).scrollTop(0);
    });
});
</script>
<script src="/public/admin/order/add.js"></script>

{include file = "inc/footer.tpl"}
