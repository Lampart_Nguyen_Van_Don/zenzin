{include file = "inc/header.tpl"} {include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<style>
.order-dialog .ui-widget-content {
    border: none !important;
}

.order-dialog .ui-dialog-buttonpane {
    text-align: center !important;
    padding: 0px !important;
}

.order-dialog .ui-dialog-buttonset {
    float: none !important;
}
</style>
<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>
        <div>{if isset($message)}{$message}{/if}</div>
        <hr class="blank-10px" />
        <div style="height: auto; width: 250px;">
            <table class="table-simple">
                <tr>
                    <th width="100px">{lang('lbl_year_month')}</th>
                    <td>{$prev_month}</td>
                </tr>
                <tr id="row_status">
                    <th>{lang('lbl_status')}</th>
                    <td>
                        <div id="status">{if $is_closed} {lang('lbl_status_closed')}
                            {else} {lang('lbl_status_not_closed')} {/if}</div>
                    </td>
                </tr>
                <!-- #7133: Start 12/Oct/2015 -->
                {if $show_take_action}
                    {if $is_closed}
                        {if $unclose_btn}
                            <tr>
                                <td colspan="2" align="center"><input type="hidden"
                                    name="take_action_type"
                                    value="unclose">
                                    <button name="take_action" class="btn-small btn-green m-height">
                                        {lang('lbl_btn_unclose')} </button></td>
                            </tr>
                        {/if}
                    {else}
                        <tr>
                            <td colspan="2" align="center"><input type="hidden"
                                name="take_action_type"
                                value="close">
                                <button name="take_action" class="btn-small btn-green m-height">
                                    {lang('lbl_btn_close')}</button></td>
                        </tr>
                    {/if}
                {/if}
                <!-- #7133: End 12/Oct/2015 -->
            </table>
        </div>
    </div>
</div>
<div id="dialog-messages" style="display: none"></div>
<div id="dialog_form_messages" style="display: none"></div>
{include file = "inc/footer.tpl"}
