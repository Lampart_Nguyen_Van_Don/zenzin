    <div>
        <div>
            <div id="message" class="alert-success"></div>
            <div id="message_error" class="alert-error"></div>
            <table class="table-theme">
                    <tr>
                        <td width="">{lang('common_lbl_business_unit')}<span class="red-symbol">※</span></td>
                        <td>
                            <select name="business_unit_id" id="business_unit_id">
                                <option value="">{lang('common_please_select')}</option>
                                {if isset($bu)} {foreach from=$bu item=b}
                                    <option value="{$b.id}">{htmlspecialchars ($b.name)}</option>
                                {/foreach} {/if}
                            </select>
                            <br/><div id="err_business_unit_id"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>{lang('common_lbl_department_name')}<span class="red-symbol">※</span></td>
                        <td>
                            <input type="text" value="{if isset($form.name)}{htmlspecialchars ($form.name)}{/if}" name="name" id="name"/>
                            <br/><div id="err_name"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>{lang('common_lbl_department_zipcode')}<span class="red-symbol">※</span></td>
                        <td>
                        <input type="text" value="{if isset($form.zipcode)}{htmlspecialchars ($form.zipcode)}{/if}" name="zipcode" id="zipcode"/>
                        <br/><div class="legend">{lang('alert_not_enter_a_hyphen')}</div>
                        <div id="err_zipcode"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>{lang('common_lbl_department_address1')}<span class="red-symbol">※</span></td>
                        <td>
                            <input type="text" value="{if isset($form.address_1)}{htmlspecialchars ($form.address_1)}{/if}" name="address_1" id="address_1"/>
                            <br/><div id="err_address_1"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>{lang('common_lbl_department_address2')}</td>
                        <td>
                            <input type="text" value="{if isset($form.address_2)}{htmlspecialchars ($form.address_2)}{/if}" name="address_2" id="address_2"/>
                            <br/><div id="err_address_2"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>TEL<span class="red-symbol">※</span></td>
                        <td>
                            <input type="text" value="{if isset($form.phone_no)}{htmlspecialchars ($form.phone_no)}{/if}" name="phone_no" id="phone_no"/>
                            <br/><div class="legend">{lang('alert_enter_a_hyphen')}</div>
                            <div id="err_phone_no"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>FAX<span class="red-symbol">※</span></td>
                        <td>
                            <input type="text" value="{if isset($form.fax_no)}{htmlspecialchars ($form.fax_no)}{/if}" name="fax_no" id="fax_no"/>
                            <br/><div class="legend">{lang('alert_enter_a_hyphen')}</div>
                            <div id="err_fax_no"></div>
                        </td>
                    </tr>
                <tr>
                    <td colspan="2">
                            <div class="float_left">
                                <button name="save_add" id="save_add" value="Save" class = 'btn-small btn-green btn-save'>{lang('common_lbl_add')}</button>
                            </div>
                            <div class="float_left">
                                <form method="POST" action="/_admin/department/show" class="close_form">
                                    {make_search_hidden(FALSE)}
                                    <button name="close_copy_c" type="submit" id = "close_copy_c" class="btn-small btn-green btn-close"
                                    value="Close">{lang('common_lbl_close')}</button>
                                </form>
                            </div>
                    </td>
                </tr>
            </table>
            <!-- </form> -->
            <hr class="blank-10px" />
        </div>
    </div>
<script>
    $(".close_form").submit(function(){
        $("#dialog_form_c").dialog("close");
        if (form_submitted && !form_changed) {
            return true;
        } else {
             return false;
        }
    });

    $("#save_add").on('click', function() {
        $.post( "/_admin/department/edit_ajax/",
                {
                  business_unit_id: $("#business_unit_id").val(),
                  name            : $("#name").val(),
                  zipcode         : $("#zipcode").val(),
                  address_1       : $("#address_1").val(),
                  address_2       : $("#address_2").val(),
                  phone_no        : $("#phone_no").val(),
                  fax_no          : $("#fax_no").val(),
                  type              : "add"
                },
                function( data ) {
                    if (data["error"] == 1) {
                        $(".validation-error").remove();
                        $(".success-message").remove();
                        $(".error-message").remove();
                        $("#err_business_unit_id").html(data["business_unit_id"]);
                        $("#err_name").html(data["name"]);
                        $("#err_zipcode").html(data["zipcode"]);
                        $("#err_address_1").html(data["address_1"]);
                        $("#err_address_2").html(data["address_2"]);
                        $("#err_phone_no").html(data["phone_no"]);
                        $("#err_fax_no").html(data["fax_no"]);
                    } else if(data["error"]==2) {
                        $(".success-message").remove();
                        $("#message_error").html("<div class='error-message blink_me'>"+$( "#common_duplicated_records" ).val()+"</div>");
                    } else {
                        form_changed = false;
                        form_submitted = true;
                        $(".validation-error").remove();
                        $(".error-message").remove();
                        $("#message").html("<div class='success-message blink_me'>"+$( "#common_insert_successfully" ).val()+"</div>");
                        // Reset form input value
                        $('select[name="business_unit_id"]').find('option:first').prop('selected', true);
                        $('input[name="name"]').val('');
                        $('input[name="zipcode"]').val('');
                        $('input[name="address_1"]').val('');
                        $('input[name="address_2"]').val('');
                        $('input[name="phone_no"]').val('');
                        $('input[name="fax_no"]').val('');
                    }
                },
                "json");
    });
    exit_confirm();

    var bind_add_event = function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            $('.btn-save').trigger('click');
            return false;
        }
    };

    $(document).bind('keydown', bind_add_event);

</script>