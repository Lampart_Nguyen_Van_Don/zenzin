<div>
    <div>
        <div id="message"></div>
        <hr class="blank-10px" />
        {if isset($d)}
        <table class="table-theme">
            <tr>
                <td width="">{lang('common_lbl_business_unit')}</td>
                <td><input type="hidden" name="did" value="{$d.id}" />
                    {htmlspecialchars ($d.business_unit_name)}</td>
            </tr>
            <tr>
                <td>{lang('common_lbl_department_name')}</td>
                <td>{htmlspecialchars ($d.name)}</td>
            </tr>
            <tr>
                <td>{lang('common_lbl_department_zipcode')}</td>
                <td>{htmlspecialchars ($d.zipcode)}</td>
            </tr>
            <tr>
                <td>{lang('common_lbl_department_address1')}</td>
                <td>{htmlspecialchars ($d.address_1)}</td>
            </tr>
            <tr>
                <td>{lang('common_lbl_department_address2')}</td>
                <td>{htmlspecialchars ($d.address_2)}</td>
            </tr>
            <tr>
                <td>TEL</td>
                <td>{htmlspecialchars ($d.phone_no)}</td>
            </tr>
            <tr>
                <td>FAX</td>
                <td>{htmlspecialchars ($d.fax_no)}</td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="float: left;">
                        <button name="btn_edit" class = 'btn-small btn-green btn-edit' value="{$d.id}">{lang('common_lbl_edit')}</button>
                    </div>
                     <div style="float: left;">
                        <button name="btn_copy" class = 'btn-small btn-green btn-edit' value="{$d.id}">{lang('common_lbl_copy')}</button>
                    </div>
                    <div style="float: left;">
                        <button name="close_load_detail_c" id="close_load_detail_c" class="btn-small btn-green btn-close" value="Close">{lang('common_lbl_close')}</button>
                    </div>
                </td>
            </tr>
        </table>
        {else}
            <div class="error-message">{lang('common_record_not_exist')}</div>
        {/if}
        <hr class="blank-10px" />
    </div>
</div>

{literal}
<script>
$(document).ready(function() {
    // Close detail c dialog box
    $(document).on('click', '#close_load_detail_c', function() {
            $("#dialog_form_c").dialog("close");
     });
    // Click edit in dialogbox
    $(".btn-edit").on('click', function() {


        var _title =  this.name == "btn_edit"? "部署編集" : "部署コピー";

        $.ajax({
            url: "/_admin/department/edit",
            type: "POST",
            data: {
                id: this.value,
                type: this.name,
                redirect_form_post:redirect_data()
            }
        })
        .done(function(data) {
            $("#dialog_form_c").html(data);
            $("#dialog_form_c").dialog({
                title: _title,
                width: 'auto',
                height: 'auto',
                modal: true,
                beforeClose: function( event, ui ) {
                    var close = close_confirm();
                    return close;
                },
                close: function() {
                    $(document).unbind('keydown', bind_edit_event);
                }
            })

            $('#dialog_form_c').dialog("open");
        });
     }); // $(".btn-edit").on('click', function() {
}); // Close $(document).ready(function() {
</script>
{/literal}
