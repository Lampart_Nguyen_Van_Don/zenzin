    <div>
        <div>
            <div id="message" class="alert-success"></div>
            <div id="err_message" class="alert-error"></div>
            {if isset($d)}
            <table class="table-theme">
                <tr>
                    <td width="">{lang('common_lbl_business_unit')}<span class="red-symbol">※</span></td>
                    <td><input type="hidden" name="d_id" id="d_id" value="{$d.id}" />
                        <select name="business_unit_id" id="business_unit_id" class="max-width">
                            <option value="">{lang('common_please_select')}</option>
                            {if isset($bu)}
                                {foreach from=$bu item=b}
                                    <option value="{$b.id}" {if ($b.id==$d.business_unit_id)}selected{/if}>{htmlspecialchars ($b.name)}</option>
                                {/foreach}
                            {/if}
                        </select>
                        <br/><div id="err_business_unit_id"></div>
                    </td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_name')}<span class="red-symbol">※</span></td>
                    <td><input type="text" value="{if isset($form.name)}{$form.name}{else}{htmlspecialchars ($d.name)}{/if}" name="name" id="name" />
                    <br/><div id="err_name"></div>
                    </td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_zipcode')}<span class="red-symbol">※</span></td>
                    <td><input type="text"
                        value="{if isset($form.zipcode1)}{$form.zipcode1}{else}{htmlspecialchars ($d.zipcode)}{/if}"
                        name="zipcode" id="zipcode" />
                        <br/><div class="legend">{lang('alert_not_enter_a_hyphen')}</div>
                        <div id="err_zipcode"></div>
                        </td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_address1')}<span class="red-symbol">※</span></td>
                    <td><input type="text"
                        value="{if isset($form.address_1)}{$form.address_1}{else}{htmlspecialchars ($d.address_1)}{/if}"
                        name="address_1" id="address_1" /> <br /><div id="err_address_1"></div></td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_address2')}</td>
                    <td><input type="text"
                        value="{if isset($form.address_2)}{$form.address_2}{else}{htmlspecialchars ($d.address_2)}{/if}"
                        name="address_2" id="address_2" /><br /><div id="err_address_2"></div></td>
                </tr>
                <tr>
                    <td>TEL<span class="red-symbol">※</span></td>
                    <td><input type="text"
                        value="{if isset($form.phone_no)}{$form.phone_no}{else}{htmlspecialchars ($d.phone_no)}{/if}"
                        name="phone_no" id="phone_no" />
                        <br/><div class="legend">{lang('alert_enter_a_hyphen')}</div>
                        <div id="err_phone_no"></div></td>
                </tr>
                <tr>
                    <td>FAX<span class="red-symbol">※</span></td>
                    <td><input type="text"
                        value="{if isset($form.fax_no)}{$form.fax_no}{else}{htmlspecialchars ($d.fax_no)}{/if}"
                        name="fax_no" id="fax_no" />
                        <br/><div class="legend">{lang('alert_enter_a_hyphen')}</div>
                        <div id="err_fax_no"></div></td>
                </tr>
                <tr>
                    <td colspan=2>
                            <div class="float_left">
                                <button class = 'btn-small btn-green btn-save' type="button">
                                    {if isset($type)}
                                        {lang('common_btn_add')}
                                    {else}
                                        {lang('common_lbl_save')}
                                    {/if}
                                </button>
                            </div>
                            <div class="float_left">
                                <form method="POST" action="/_admin/department/show" id="close_form" class="close_form">
                                    {make_search_hidden(FALSE)}
                                    <button name="close_edit_c" type="submit" id = "close_edit_c" class="btn-small btn-green btn-close" value="Close">{lang('common_lbl_close')}</button>
                                </form>
                            </div>
                            <div class="float_left">
                            <form method="POST" id="form_delete" action="/_admin/department/show">
                                {make_search_hidden(FALSE)}
                            </form>
                            {if !isset($type)}
                            <button id="delete_department" value="{$d.id}" class="btn-small btn-green btn-delete">{lang('common_lbl_delete')}</button>
                            {/if}
                        </div>
                    </td>
                </tr>
            </table>
            {else}
                <div class="error-message">{lang('common_record_not_exist')}</div>
            {/if}
            <hr class="blank-10px" />
            <input type="hidden" name="type" id="type"
            value="{if isset($type)}{$type}{/if}"/>
        </div>
    </div>
{literal}
<script>
    $(".close_form").submit(function(){
        $("#dialog_form_c").dialog("close");
        if (form_submitted && !form_changed) {
            return true;
        } else {
             return false;
        }
    });

    $(document).bind('keydown', function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            $('.btn-save').click();
            return false;
        }
    });

    // Save to DB
    $(".btn-save").on('click', function() {
        url = "/_admin/department/edit_ajax/";
        $.post( url,
                {
                  d_id            : $("#d_id").val(),
                  business_unit_id: $("#business_unit_id").val(),
                  name            : $("#name").val(),
                  zipcode         : $("#zipcode").val(),
                  address_1       : $("#address_1").val(),
                  address_2       : $("#address_2").val(),
                  phone_no        : $("#phone_no").val(),
                  fax_no          : $("#fax_no").val(),
                  type            : $("#type").val()
                },
                function( data ) {
                    // If has error messages
                    if (data["error"] == 1) {
                        $(".validation-error").remove();
                        $(".success-message").remove();
                        $("#err_message").remove();
                        $("#err_business_unit_id").html(data["business_unit_id"]);
                        $("#err_name").html(data["name"]);
                        $("#err_zipcode").html(data["zipcode"]);
                        $("#err_address_1").html(data["address_1"]);
                        $("#err_address_2").html(data["address_2"]);
                        $("#err_phone_no").html(data["phone_no"]);
                        $("#err_fax_no").html(data["fax_no"]);
                    } else if (data["error"] == 2) {
                        $(".validation-error").remove();
                        $(".success-message").remove();
                        $("#message").html("<div class='error-message blink_me'>"+data['msg_error']+"</div>");
                    } else {
                        form_changed = false;
                        form_submitted = true;
                        $(".validation-error").remove();
                        if(data['type']=="edit") {
                            $("#message").html("<div class='success-message blink_me'>"+$( "#common_edit_successfully" ).val()+"</div>");
                        } else {
                            $("#message").html("<div class='success-message blink_me'>追加が完了しました。</div>");
                        }
                    }
                },
                "json");
    });

    var bind_edit_event = function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            $('.btn-save').trigger('click');
            return false;
        }
    };

    $(document).bind('keydown', bind_edit_event);

    exit_confirm();
</script>
{/literal}
