<!-- Show list of departmenta -->
{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<!-- Department -->
<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>
        <!-- Keep pagination -->
        {make_search_hidden()}
        <hr class="blank-10px" />
        <table class="table-theme odd">
            <tr>
                <th>{lang('common_lbl_action')}</th>
                <th>{lang('common_lbl_business_unit')}</th>
                <th>{lang('common_lbl_department_name')}</th>
            </tr>
            {if isset($departments)}
                {foreach from=$departments item=d}
                <tr>
                    <td>
                        <!-- <input type="hidden" value="{$d.department_id}" name="d_id" /> -->
                        <button class="btn-small btn-green btn-view" value="{$d.department_id}" name="">{lang('common_lbl_view')}</button>
                    </td>
                    <td>{htmlspecialchars ($d.business_unit_name)}</td>
                    <td>{htmlspecialchars ($d.department_name)}</td>
                </tr>
                {/foreach}
            {else}
                <tr>
                    <td colspan="3">{lang('common_lbl_no_data')}</td>
                </tr>
            {/if}
            <tr>
                <td colspan="3">
                    <!-- {form_add_button()} -->
                    <button name="add" id="add" value="add" class="btn-small btn-green btn-add">{lang('common_btn_add')}</button>
                </td>
            </tr>
        </table>
        <hr class="blank-10px" />
        {if $pagination} {$pagination.links} {/if}
    </div>
</div>
<!-- Store messages -->
<input type="hidden" id="common_copy_department" value="{lang('common_copy_department')}"/>
<input type="hidden" id="common_add_department" value="{lang('common_add_department')}"/>
<input type="hidden" id="common_edit_department" value="{lang('common_edit_department')}"/>
<input type="hidden" id="detail_department" value="{lang('common_detail_department')}"/>
<input type="hidden" id="common_edit_successfully" value="{lang('common_edit_successfully')}"/>
<input type="hidden" id="common_insert_successfully" value="{lang('common_insert_successfully')}"/>
<input type="hidden" id="common_duplicated_records" value="{lang('common_duplicated_records')}"/>
<!-- End store messages -->
<div id="dialog_form_c" title="{lang('common_detail_department')}"></div>
<div id="delete_confirm"></div>
<!-- e/content -->

{literal}
<script>
    $(document).ready(function() {
        $(function() {
            $("#dialog_form_c").dialog({
                autoOpen : false,
                width : "auto",
                height : "auto",
                modal : true,
                beforeClose: function( event, ui ) {
                    var close = close_confirm();
                    return close;
                },
                buttons : {
                },
                close : function() {
                }
            });
        });

        // Load detail
        $(document).on("click", ".btn-view", function() {
            $('#dialog_form_c').load("/_admin/department/detail", {
                id : this.value,
                redirect_form_post: redirect_data()
            }, function() {
                $('#dialog_form_c').dialog('option', 'title', $( "#detail_department" ).val());
                $("#dialog_form_c").dialog( "option", "height", "auto");
                $("#dialog_form_c").dialog( "option", "width", "auto" );
                $('#dialog_form_c').dialog("open");
            });
        }); // Close $(document).on("click", "#detail_c", function() {
        // Load add
        $(document).on("click", "#add", function() {
            $('#dialog_form_c').load("/_admin/department/add", {
                id : this.name,
                redirect_form_post: redirect_data()
            }, function() {
                $("#dialog_form_c").dialog({
                    title: $('#common_add_department').val(),
                    width: 'auto',
                    height: 'auto',
                    modal : true,
                    close : function() {
                        $(document).unbind('keydown', bind_add_event);
                    }
                });
                $('#dialog_form_c').dialog('open');
            });
        }); // Close $(document).on("click", "#add", function() {

        // Delete department
        $(document).on('click', '#delete_department', function(event) {
            event.preventDefault();
            var id = $(this).val();


            confirm_dialog(lang['message_confirm_delete'], {
                'delete' : function() {
                    // Close dialog
                    $(this).dialog('close');
                    deleteDepartment(id);
                }
            });


            /*$("#delete_confirm").html(lang['message_confirm_delete']).dialog({
                title: lang['title_confirm'],
                width: 'auto',
                height: 'auto',
                modal : true,
                buttons: {
                    "Yes": function() {
                        $(this).dialog('close');
                        deleteDepartment(id);
                    },
                    "No": function () {
                        $(this).dialog('close');
                    }
                },
                close : function () {
                    // alert("close");
                    $(this).dialog('close');
                }
            }); // Close $("#delete_confirm").html(lang['message_confirm_delete']).dialog({
            */





        }); // Close $(document).on('click', '#delete_department', function(event) {
        function deleteDepartment(id) {
            url = "/_admin/department/delete/";
            $.post( url,
                    {
                        d_id: id
                    },
                    function( data ) {
                        // If has error messages
                        if (data["error"] == 1) {
                            // Remove old error messages
                            $(".validation-error").remove();
                            $(".success-message").remove();
                            $("#err_message").html('<div class="error-message">'+data["error_name"]+'</div>');
                        } else {
                            // Submit form to load all data again:
                            $("#form_delete").submit();
                            $("#dialog_form_c").dialog("close");
                        }
                    },
                    "json");
        } // Close function deleteDepartment(id) {

        // Close C when click outside
        $("body").on('click', '.close-dialog', function(event) {
            $(".ui-dialog-content").dialog("close");
            $("#dialog-edit-content").html('');
        });

        $("body").on('click', '.ui-dialog-titlebar-close', function(event) {
            if (form_submitted && !form_changed) {
                $(".close_form").submit();
            }
        });
}); // Close $(document).ready(function() {
</script>
{/literal}
{include file = "inc/footer.tpl"}
