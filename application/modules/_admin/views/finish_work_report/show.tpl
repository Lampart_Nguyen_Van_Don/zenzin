{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

    <script src="/public/admin/js/angular.js"></script>
<!-- #7846:Start -->
<!--     <script src="/public/admin/js/angular-touch.js"></script> -->
<!--     <script src="/public/admin/js/angular-animate.js"></script> -->
    <script src="/public/admin/js/angular-sanitize.min.js"></script>
    <script src="/public/admin/js/csv.js"></script>
    <script src="/public/admin/js/pdfmake.js"></script>
    <script src="/public/admin/js/vfs_fonts.js"></script>
<!--     <script src="/public/admin/js/ui-grid-unstable.js"></script> -->
    <script src="/public/admin/js/ui-grid-unstable-ver3.0.7.js"></script>

<!--     <link rel="stylesheet" href="/public/admin/css/ui-grid-unstable.css" type="text/css"> -->
    <link rel="stylesheet" href="/public/admin/css/ui-grid-unstable-ver3.0.7.css" type="text/css">
<!-- #7846:End -->
    <link rel="stylesheet" href="/public/admin/css/order_acceptance.css" type="text/css">
    <link rel="stylesheet" href="/public/admin/order/main.css" rel="stylesheet" media="all"/>
     <script type="text/javascript">

        var account_list = {$order_model->list_account_follow_bussiness_unit};
        var search_result = {$search_result};
        var my_account = {$order_model->my_account};
        var can_approve = {$can_approve};
        var can_edit = {$can_edit};
        var can_invoices = '{$can_invoices}';
        var can_acceptance = {$can_acceptance};
        var tax_division = {$tax_division};
        var j_accounts = {json_encode($j_accounts)};
        var list_order_status = {json_encode($order_model->list_order_status)};
        var can_invoice_issue = {$can_invoice_issue};
        // Check order acceptance permission
        // 発注・検収-発注検収力・変更
        var order_acceptance_input_change = {$order_acceptance_input_change};
        // 発注・検収-一覧・参照
        var order_acceptance_view = {$order_acceptance_view};
//7910:Start
        var header_text = '{json_encode($header_text)}';
        var hide_column = '{json_encode($hide_column)}';
        var show_column = '{json_encode($show_column)}';
        var business_unit_name = '{$business_unit_name}';
//7910:End
        var UNAPPROVED = 1;
        var UNDETERMINED = 2;
        var CONFIRMED = 3;
        var CANCELLED = 4;
        var REJECTED = 9;
        var can_approve = {$can_approve};
        var can_edit = {$can_edit};

       var fwr_jsGlobals = [
                         '{lang("lbl_print")}',
                         '{lang("lbl_action")}',
                         '{lang("lbl_status")}',
                         '{lang("lbl_j_code")}',
                         '{lang("lbl_branch_code")}',
                         '{lang("lbl_client_name")}',
//7910:Start
                         //'{lang("lbl_division_name")}',
                         '{if isset($header_text["lbl_division_name"])} {lang($header_text["lbl_division_name"])} {else} {lang("lbl_division_name")} {/if}',
//7910:End
                         '{lang("lbl_charge_name")}',
                         '{lang("lbl_fax")}',
//7910:Start
                         //'{lang("lbl_delivery_date")}',
                         '{if isset($header_text["lbl_delivery_date"])} {lang($header_text["lbl_delivery_date"])} {else} {lang("lbl_delivery_date")} {/if}',
//7910:End
                         '{lang("lbl_set_name")}',
                         '{lang("lbl_remark")}',
                         '{lang("system_notification")}',
                         '{lang("choose_least_work_report")}',
                         '{lang("nothing_changed")}',
                         '{lang("report_fax_no_required")}',
                         '{lang("report_fax_no_invalid")}'
//7910:Start
                         ,'{lang("lbl_header_number_of_print_for_dp")}'
//7910:End
        ];
    var jsGlobals = [
            // 0
            lbl_branch_cd = '{lang("lbl_branch_cd")}',
            // 1
            lbl_order_status = '{lang("lbl_order_status")}',
            // 2
            lbl_change_report = '{lang("lbl_change_report")}',
            // 3
            lbl_product_name = '{lang("lbl_product_name")}',//3
            // 4
            lbl_detail_contents = '{lang("lbl_detail_contents")}',
            // 5
            lbl_delivery_date = '{lang("lbl_delivery_date")}',//5
            // 6
            lbl_billing_date = '{lang("lbl_billing_date")}',
            // 7
            lbl_payment_date = '{lang("lbl_payment_date")}',//7
            // 8
            lbl_quantity = '{lang("lbl_quantity")}',
            // 9
            lbl_tax_type = '{lang("lbl_tax_type")}',
            // 10
            lbl_unit_price = '{lang("lbl_unit_price")}',
            // 11
            lbl_amount = '{lang("lbl_amount")}',//11
            // 12
            lbl_order_detail_cost_unit_price = '{lang("lbl_order_detail_cost_unit_price")}',
            // 13
            lbl_order_detail_cost_total_price = '{lang("lbl_order_detail_cost_total_price")}',
            // 14
            lbl_gross_profit_amount = '{lang("lbl_gross_profit_amount")}',
            // 15
            lbl_order_detail_gross_profit_rate = '{lang("lbl_order_detail_gross_profit_rate")}',
            // 16
            lbl_j_code = '{lang("lbl_j_code")}',
            // 17
            lbl_s_code = '{lang("lbl_s_code")}',
            // 18
            lbl_account_id = '{lang("lbl_account_id")}',
            // 19
            lbl_is_ad_sales_slip_input = '{lang("lbl_is_ad_sales_slip_input")}',
            // 20
            lbl_is_ad_invoice_issue = '{lang("lbl_is_ad_invoice_issue")}',
            // 21
            lbl_is_order_input = '{lang("lbl_is_order_input")}',
            // 22
            lbl_is_acceptance_input = '{lang("lbl_is_acceptance_input")}',
            // 23
            lbl_is_sales_slip_input = '{lang("lbl_is_sales_slip_input")}',
            // 24
            lbl_is_invoice_issue = '{lang("lbl_is_invoice_issue")}',
            // 25
            lbl_name = '{lang("lbl_name")}',
            // 26
            lbl_content = '{lang("lbl_content")}',
            // 27
            lbl_action = '{lang("lbl_action")}',//27
            // 28
            lbl_change_report = '{lang("lbl_change_report")}',
            // 29
            lbl_copy_branch = '{lang("lbl_copy_branch")}',
            // 30
            lbl_delete_branch = '{lang("lbl_delete_branch")}',
            // 31
            lbl_cancel_branch = '{lang("lbl_cancel_branch")}',
            // 32
            lbl_show = '{lang("lbl_show")}',
            // 33
            lbl_check_approve_more = '{lang("lbl_check_approve_more")}',
            // 34
            lbl_cancel_branch_release = '{lang("lbl_cancel_branch_release")}',
            // 35
            edit_nothing_change_msg = '{lang("edit_nothing_change")}',
            // 36
            change_report_nothing_change_msg = '{lang("change_report_nothing_change")}'
            // 37
//7910:Start
            ,change_report_nothing_change_msg = '{lang("change_report_nothing_change")}'
//7910:End
        ];
     </script>
    <style type="text/css">
        /*
        .grid {
                  width: 99%;
                  height: 470px;
              }
        */
        .grid-align {
            text-align: center;
        }

        .no-data {
            width: 100%;
            padding: 5px 7px;
            /*background: #f3f3f3;*/
        }

       .order-dialog .ui-widget-content{
            border: none !important;
        }
        .order-dialog .ui-dialog-buttonpane {
            text-align: center !important;
            padding: 0px !important;
        }

        .order-dialog .ui-dialog-buttonset {
            float: none !important;
        }

        .cell-template-data {
            width: 100%;
            height: 100%;
            line-height: 30px;
        }

        input.ng-scope {
            line-height: 30px !important;
        }

        select.ng-scope {
            height: 30px !important;
        }
    </style>

    <div id="dialog-errors" style="display:none"></div>

    <div class="cell content slide-right">
        <div ng-controller="MainCtrl">
        <div ui-i18n="{literal}{{lang}}{/literal}"></div>
            <h1>{$title}</h1>
            <!-- Begin #search -->
            <div>
                <form action="" method="post">
                    <table class="table-simple">
                        <tr>
                            <th>{if $business_unit_name != "DP"} {lang(lbl_delivery_date)} {else} {"納品完了日"} {/if}</th>
                            <td>
                                <input type="text" id="delivery_date_from" ng-model="search.delivery_date_from" name="delivery_date_from"/>～
                                <input type="text" ng-model="search.delivery_date_to" name="delivery_date_to"/><br/>
                                <div class="legend">{lang('lbl_delivery_date_description')}</div>
                                <span id="delivery_date_error_msg" class="validation-error" style="display: none"></span>
                            </td>
                        </tr>

                        <tr>
                            <th>{lang('lbl_j_code_branch_code')}</th>
                            <td>
                                <input type="text" ng-model="search.j_code_branch_code_from" name="j_code_branch_code_from"/>～
                                <input type="text" ng-model="search.j_code_branch_code_to" name="j_code_branch_code_to"/><br/>
                                <div class="legend">{lang('lbl_j_code_branch_code_description')}</div>
                                <span id="j_code_error_msg" class="validation-error" style="display: none"></span>
                            </td>
                        </tr>

                        <tr>
                            <th>{lang('lbl_client_code')}</th>
                            <td>
                                <input type="text" ng-model="search.s_code" name="s_code"/><br/>
                                <div class="legend">{lang('lbl_client_code_description')}</div>
                            </td>
                        </tr>

                        <tr>
                            <th>{lang('lbl_client_name')}</th>
                            <td>
                                <input type="text" ng-model="search.client_name" name="client_name"/><br/>
                                <div class="legend">{lang('lbl_client_name_description')}</div>
                            </td>
                        </tr>

                        <tr {if $business_unit_name == "DP"} {"style='display:none'"} {/if}'>
                            <th>{lang('lbl_arrange_rep')}</th>
                            <td>
                                <select name="arrange_rep" ng-model="search.arrange_rep">
                                    <option value="0">＝＝すべて＝＝</option>

                                    {foreach from=$accounts item=val}
                                        <option value="{$val.id}">{$val.name}</option>
                                    {/foreach}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <th>{lang('lbl_finish_work_status')}</th>
                            <td>
                                {if $finish_work_status_configs}
                                    {$i = 0}
                                    {foreach from=$finish_work_status_configs item=val}
                                        <input type="radio" id="report_is_complete_{$i}" name="report_is_complete" ng-model="search.report_is_complete" title="{$val.name}" value="{$val.code}"/><label for="report_is_complete_{$i}">{$val.name} </label>&nbsp;
                                        {$i = $i + 1}
                                    {/foreach}
                                {/if}
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2"><button type="button" id="submit" ng-click="filter(search)" style="margin: 0 0 0 240px" class="btn-small btn-green btn-search">{lang('btn_search')}</button></td>
                        </tr>
                    </table>
                </form>
            </div>
            <!-- End #search -->

            <br/>
            <hr class="blank-10px"/>

            <!-- Begin #list -->
            <div class="error-message" style="display: none"></div>
            <div class="success-message" style="display: none"></div>
            <div id="message"></div>
            <!--7184:Start-->
            <button type="button" ng-click="checkalllist()" class="btn-small btn-green" >{lang('btn_check_all_application_form_print')}</button>
            <!--7184:Start-->
            <button type="button" class="btn-small btn-green btn-download btn-download-print" >{lang('btn_print')}</button>
            <br/>
            <div style="margin: 10px 0 0 0"></div>
<!-- #7589:Start -->
<!--             <div ui-grid="gridOptions" external-scopes="states" ui-grid-pinning ui-grid-resize-columns ui-grid-edit ui-grid-row-edit ui-grid-cellnav class="finish-work-report-grid"> -->
            <div ui-grid="gridOptions" external-scopes="states" ui-grid-pinning ui-grid-resize-columns ui-grid-edit ui-grid-row-edit ui-grid-cellnav class="finish-work-report-grid grid-viewport-height">
<!-- #7589:Start -->
                <div class="watermark" ng-class="{literal}{'show-watermark' : showWatermark}{/literal}">{lang('lbl_no_data_available')}</div>
            </div>
            <div id="dialog">
            {include file="$detail.tpl"}
            </div>
            <div id="clientDialog"></div>
<!--7221:Start-->
            <button type="button" ng-click="checkalllist()" class="btn-small btn-green" >{lang('btn_check_all_application_form_print')}</button>
<!--7221:End-->
            <button type="button" class="btn-small btn-green btn-download btn-download-print" >{lang('btn_print')}</button>
            <div id="frm_print"></div>
            <!-- End #list -->
        </div>
    </div>
    <script>
        close_confirm();
        $('form input').keydown(function(e) {
            if (e.keyCode == 13) {
                $('button#submit').click();
            }
        });
    </script>
   <script type="text/javascript">
        //init global variables
    clientDialog = $('#clientDialog');
    clientDialog.dialog({
        autoOpen : false,
        title: '{lang('title_client_detail')}',
        height: 'auto',
        width: 'auto',
        modal: true,
        beforeClose: function( event, ui ) {
            return close_confirm();
        },
        close : function() {
            form_changed = false;
            if (typeof cl_functions_loaded !== 'undefined' && cl_functions_loaded) {
                unbind_save_key();
            }
        }
    });

    $("#dialog").dialog({
        autoOpen : false,
        width : "auto",
        height : "auto",
        modal : true,
        buttons : {
        },
        close : function() {
        }
    });

    exit_confirm();
    </script>
    <script src="/public/admin/finish_work_report/app.js"></script>
    <script src="/public/admin/order/detail.js"></script>

{include file = "inc/footer.tpl"}
