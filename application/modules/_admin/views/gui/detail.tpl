<div>
    <p>{lang('lbl_type')}: {$element.gui_parts_name}</p>
    <table class="table-theme">
        <tr>
            <td>{lang('lbl_code_of_gui_parts_element')}</td>
            <td><input type="text" value="{$element.code}" disabled></td>
        </tr>
        <tr>
            <td>{lang('lbl_name_of_gui_parts_element')}</td>
            <td><input type="text" value="{html_escape($element.name)}" disabled></td>
        </tr>
        <tr>
            <td>{lang('lbl_display_or_not')}</td>
            <td>
                <label>
                    <input name="is_no_add" type="checkbox" value="1" {if $element.is_no_add == 1} {'checked'}{/if} disabled> {lang('info_display')}
                </label>
            </td>
        </tr>
        <tr>
            <td>{lang('lbl_sequence')}</td>
            <td><input type="text" value="{$element.sequence}" disabled></td>
        </tr>
        <tr>
            <td colspan="2">
                <button type="button" data-element-id="{$element.id}" class="btn-small btn-green btn-edit">{lang('btn_edit')}</button>
                <button type="button" class="btn-small btn-green btn-close close-dialog">{lang('btn_close')}</button>
            </td>
        </tr>
    </table>
</div>

{literal}
<script>
    $(".btn-edit").on('click', function(event) {
        event.preventDefault();
        element_id = $(this).data('element-id');

        $.ajax({
            url: '/_admin/gui/edit',
            type: 'POST',
            data: {element_id: element_id},
        })
        .done(function(data) {
            if (typeof data == 'object' && data.error_code == 400) {
                $("#dialog-edit-content").html('<div class="error-message">'+data.messages+'</div>');
                $("#dialog-edit-content").dialog({
                    width: 'auto',
                    height: 'auto',
                    modal : true,
                });
                return;
            }
            $("#dialog-edit-content").html(data);
            $("#dialog-edit-content").dialog({
                title: 'コード・名称編集',
                height: 'auto',
                width: 'auto',
                beforeClose: function( event, ui ) {
                    var close = close_confirm();
                    return close;
                },
//                 close: function() {
//                     $(window).unbind('beforeunload');
//                 }
            });
        })
    });

</script>
{/literal}