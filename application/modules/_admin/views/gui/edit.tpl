<div>
    <p class="messages"></p>
    <p>{lang('lbl_type')}: {$element.gui_parts_name}</p>
    {form_open(null, "id='edit_form'")}
        <input type="hidden" name="element_id" value="{$element.id}">
        <table class="table-theme">
            <tr>
                <td>{lang('lbl_code_of_gui_parts_element')}</td>
                <td><input type="text" value="{$element.code}" disabled></td>
            </tr>
            <tr>
                <td>{lang('lbl_name_of_gui_parts_element')} <span class="red-symbol">※</span></td>
                <td>
                    <input type="text" name="name" id="code_name" value="{html_escape($element.name)}" {if !$gui_part.is_change_name_flg}{'disabled'}{/if}>
                    <p id="edit-name-error"></p>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_display_or_not')}</td>
                <td>
                    <label>
                        <input name="is_no_add" type="checkbox" value="1" {if $element.is_no_add == 1} {'checked'}{/if} {if !$gui_part.is_change_new_add_flg}{'disabled'}{/if}> {lang('info_display')}
                    </label>
                    <p id="edit-no-display-error"></p>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_sequence')} <span class="red-symbol">※</span></td>
                <td>
                    <input name="sequence" type="text" value="{$element.sequence}" maxlength="5" {if !$gui_part.is_change_sort_flg}{'disabled'}{/if}>
                    <p id="edit-sequence-error"></p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button type="button" class="btn-small btn-green btn-save btn-update-element">{lang('btn_save')}</button>
                    <button type="button" class="btn-small btn-green btn-close close-dialog">{lang('btn_close')}</button>

                    {if $gui_part.is_delete_flg}
                    <button type="button" class="btn-small btn-green btn-delete delete" data-element-id="{$element.id}">{lang('btn_delete')}</button>
                    {/if}
                </td>
            </tr>
        </table>
    {form_close()}
</div>

{literal}
<script>
    $(document).bind('keydown', function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            $('.btn-update-element').click();
            return false;
        }
    });

    $(".btn-update-element").on('click', function(event) {

    	var f = $("#edit_form");
    	var disabled = f.find(':input:disabled').removeAttr('disabled');
    	var form_data = f.serialize();
        //form_data = $("#edit_form").serialize();
        disabled.attr('disabled','disabled');
        $.ajax({
            url: '/_admin/gui/update',
            type: 'POST',
            data: form_data,
        })
        .done(function(data) {
            data =$.parseJSON(data);
            if (data.status) {
                form_changed = false;
                form_submitted = true;
                $(".messages").html("<div class='success-message blink_me'>"+data.messages+"</div>");
                $(".messages").addClass('alert-success');
                $(".btn-close").addClass('back-to-show');
                $(".ui-dialog-titlebar-close").addClass('back-to-show');
                $("#edit-name-error, #edit-no-display-error, #edit-sequence-error").html('');
            } else {
                if (typeof data.messages == 'object') {
                    $(".messages").html('');
                    $("#edit-name-error").html(data.messages.name);
                    $("#edit-no-display-error").html(data.messages.is_no_add);
                    $("#edit-sequence-error").html(data.messages.sequence);
                } else {
                    $(".messages").html("<div class='error-message blink_me'>"+data.messages+"</div>");
                    $(".messages").addClass('alert-error');
                }
            }
        })
    });

    $(".btn-delete").on('click', function(event) {
        element_id = $(this).data('element-id');

        $("#dialog-confirm").html($("#code_name").val()+'を削除します。よろしいですか？').dialog({
            title: '確認',
            width: 'auto',
            height: 'auto',
            modal : true,
            buttons: {
                "はい": function() {
                     $.ajax({
                        url: '/_admin/gui/delete',
                        type: 'POST',
                        data: {element_id: element_id},
                    })
                    .done(function(data) {
                        if (!data.status) {
                            $(".messages").html('<div class="error-message">'+data.messages+'</div>');
                            $("#dialog-confirm").dialog('close');
                            return;
                        }
                        dom =   "<form method='post' action='/_admin/gui/show'>"+
                                    "<input type='hidden' name='pg' value='"+$("#page").val()+"'>"+
                                    "<input type='hidden' name='type' value='"+$("#type").val()+"'>"+
                                "</form>";
                        $(dom).appendTo('body').submit();
                    })
                },
                "いいえ": function () {
                    $(this).dialog('close');
                }
            }
        });

    });

    exit_confirm();
</script>
{/literal}
