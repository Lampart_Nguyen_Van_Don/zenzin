{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>
        <div>
            {form_open("_admin/gui/show", "class=search_form")}
            <table class="table-simple">
            <tr>
                <th>
                    <label for="gui_part_search">{lang('lbl_type')} <span class="red-symbol">※</span></label>
                </th>
                <td>
                    <select name="type" id="gui_part_search">
                        {$default_gui_part = null}
                        {if !empty($gui_parts)}
                            {$default_gui_part = $gui_parts[0]}
                        {/if}
                        {foreach $gui_parts as $gui_part}
                            {if isset($form.type) && $gui_part.id == $form.type}
                                {$default_gui_part = $gui_part}
                            {/if}
                            <option value="{$gui_part.id}" {if isset($form.type) && $gui_part.id == $form.type}{'selected'}{/if}>{$gui_part.name}</option>
                        {/foreach}
                    </select>
                 </td>
                <td colspan="2">
                    <div align="center">
                        <button type="submit" class="btn-small btn-view btn-green btn-search">{lang('btn_display')}</button>
                    </div>
                 </td>
             </tr>
            </table>
            {form_close()}
        </div>
        <br>
        <h1></h1>

        <table class="table-theme odd">
            <thead>
                <tr>
                    <th>{lang('lbl_action')}</th>
                    <th>{lang('lbl_sequence')}</th>
                    <th>{lang('lbl_code')}</th>
                    <th>{lang('lbl_name')}</th>
                    <th>{lang('lbl_is_display')}</th>
                </tr>
            </thead>
            <tbody>
                {if !empty($elements)}
                {foreach $elements as $element}
                <tr>
                    <td>
                        <a data-element-id="{$element.id}" class="btn-small btn-green btn-view view-element">{lang('btn_ref')}</a>
                    </td>
                    <td>{$element.sequence}</td>
                    <td>{$element.code}</td>
                    <td>{html_escape($element.name)}</td>
                    <td align="center">{$element.new_input}</td>
                </tr>
                {/foreach}
                {else}
                <tr><td colspan="5">No data</td></tr>
                {/if}

                {if $default_gui_part.is_add_flg}
                <tr>
                    <td colspan="5">
                        <button type="button" data-gui-parts-id="{if isset($form.type)}{$form.type}{else}{if !empty($gui_parts)}{$gui_parts[0]['id']}{/if}{/if}" class="btn-small btn-green btn-add">{lang('btn_add_new')}</button>
                    </td>
                </tr>
                {/if}
            </tbody>
        </table>

        <input type="hidden" id="type" value="{if isset($form.type)}{$form.type}{else}{if !empty($gui_parts)}{$gui_parts[0]['id']}{/if}{/if}">
        <input type="hidden" id="page" value="{if isset($form.pg)}{$form.pg}{else}{1}{/if}">
    </div>
</div>
<div id="dialog-edit-content"></div>
<div id="dialog-add-content" style="display:none">
    <p class="messages"></p>
    <p>{lang('lbl_type')}: {$default_gui_part.name}</p>
    <form id="add_form" method="post">
        <input id="gui_parts_id" type="hidden" name="gui_parts_id" value="{$default_gui_part.id}">
        <table class="gui_parts_element_detail">
            <tr>
                <td>{lang('lbl_name')}<span class="red-symbol">※</span></td>
                <td>
                    <input name="name" type="text" value="">
                    <p id="add-name-error"></p>
                </td>
            </tr>
        </table>
        <br>
        <button type="button" class="btn-small btn-green btn-save btn-add-element" id="save_add">{lang('btn_add')}</button>
        <button type="button" class="btn-small btn-green btn-close close-dialog">{lang('btn_close')}</button>
    </form>
</div>
<div id="dialog-confirm" style="display:none"></div>
{literal}
<script>
    $(document).ready(function() {
        $(".search_form :input").change(function() {
            $(window).unbind('beforeunload');
        });
    });
    // ENTER => SEARCH
    $(".table-simple").keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
         {
            e.preventDefault();
            if($("select[id='gui_part_search']").is(':focus')) {
                $(".search_form").submit();
            }
         }
    }
    );

    $(".btn-search").on('click', function() {
        form_changed = false;
        form_submitted = true;
        $(".search_form").submit();
    });

    $(".view-element").on('click', function(event) {
        element_id = $(this).data('element-id');
        $.ajax({
            url: '/_admin/gui/detail',
            type: 'POST',
            data: {element_id: element_id}
        })
        .done(function(data) {
            if (typeof data == 'object' && data.error_code == 400) {
                $("#dialog-edit-content").html('<div class="error-message">'+data.messages+'</div>');
                $("#dialog-edit-content").dialog({
                    width: 'auto',
                    height: 'auto',
                    modal : true
                });
                return;
            }

            $("#dialog-edit-content").html(data);
            $("#dialog-edit-content").dialog({
                title: 'コード・名称参照',
                height: 'auto',
                width: 'auto',
                modal: true
            });
        })
    });

    $(".btn-add").on('click', function(event) {
        $("input[name='name']").val('');
        $(".messages").html('');
        $(".validation-error").remove();
        $("#dialog-add-content").show();
        $("#dialog-add-content").dialog({
            height: 'auto',
            width: 'auto',
            title: 'コード・名称追加',
            modal: true,
            beforeClose: function( event, ui ) {
                var close = close_confirm();
                return close;
            },
            close: function() {
                $(window).unbind('beforeunload');
            }
        });
    });

    $(".btn-add-element").on('click', function(event) {
        $.ajax({
            url: '/_admin/gui/add',
            type: 'POST',
            data: $("#add_form").serialize()
        })
        .done(function(data) {
            data = $.parseJSON(data);

            $('.success-message').remove();
            if (data.status) {
                form_changed = false;
                form_submitted = true;
                $(".messages").html("<div class='success-message blink_me'>"+data.messages+"</div>");
                $(".messages").addClass('alert-success');
                $(".btn-close").addClass('back-to-show');
                $(".ui-dialog-titlebar-close").addClass('back-to-show');
                $("#add-name-error").html('');
                $('input[name="name"]').val('');
            } else {
                if (typeof data.messages == 'object') {
                    $(".messages").html('');
                    $("#add-name-error").html(data.messages.name);
                } else {
                    $(".messages").html('');
                    $(".messages").html("<div class='error-message'>"+data.messages+"</div>");
                    $(".messages").addClass('alert-error');
                }
            }
        })
    });

    $("body").on('click', '.close-dialog', function(event) {
        $(".ui-dialog-content").dialog("close");
    });

    $('body').on('click', '.back-to-show', function(event) {
        if (!form_changed) {
            dom =   "<form method='post' action='/_admin/gui/show'>"+
                    "<input type='hidden' name='type' value='"+$("#type").val()+"'>"+
                "</form>";
            $(dom).appendTo('body').submit();
        }
    });
    var bind_add_event = function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            if ($("#dialog-add-content").is(":visible")) {
                $("#save_add").trigger('click');
            }
            return false;
        }
    };

    $(document).bind('keydown', bind_add_event);
    exit_confirm();
    </script>
{/literal}
{include file = "inc/footer.tpl"}
