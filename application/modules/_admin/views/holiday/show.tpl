{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>
        {if isset($message)}{$message}<br><br>{/if}

        <p>
            {lang('lbl_established_year')}:
            {$tmp_year = array()}
            {foreach $established_years as $year}
            {$tmp_year[] = $year.year}
            {/foreach}
            {implode(',', $tmp_year)}
        </p>

        <div class="calendar-top-filter">
            <div class="calendar-search-wrapper">
                {form_open()}
                    <table class="table-simple" border="1">
                        <tr>
                            <th width="70px">{lang('lbl_year')}<span class="red-symbol">※</span></th>
                            <td width="70px">
                                <select name="year">
                                    {foreach $list_of_years as $year}
                                    <option value="{$year}" {if $year == $year_filter}{'selected'}{/if}>{$year}</option>
                                    {/foreach}
                                </select>
                            </td>
                            <td><button type="submit" class="btn-small btn-green btn-search">{lang('btn_search')}</button></td>
                        </tr>
                    </table>

                {form_close()}
            </div>

            <div class="calendar-check-day-wrapper">
                <form id="update-holiday" method="post" action="/_admin/holiday/ajax_update">
                    <input id="year-filter" type="hidden" name="year" value="{$year_filter}">
                    <table class="table-simple" border="1">
                        <tr>
                            <th height="50px" align="center">{lang('lbl_establish_day')}</th>
                            <td width="400px">
                                <label>
                                    <input name="weekdays[]" type="checkbox" value="{$smarty.const.DAY_OF_WEEK_MON}"> {lang('lbl_monday')}
                                </label>
                                <label>
                                    <input name="weekdays[]" type="checkbox" value="{$smarty.const.DAY_OF_WEEK_TUE}"> {lang('lbl_tuesday')}
                                </label>
                                <label>
                                    <input name="weekdays[]" type="checkbox" value="{$smarty.const.DAY_OF_WEEK_WED}"> {lang('lbl_wednesday')}
                                </label>
                                <label>
                                    <input name="weekdays[]" type="checkbox" value="{$smarty.const.DAY_OF_WEEK_THU}"> {lang('lbl_thursday')}
                                </label>
                                <label>
                                    <input name="weekdays[]" type="checkbox" value="{$smarty.const.DAY_OF_WEEK_FRI}"> {lang('lbl_friday')}
                                </label>
                                <label>
                                    <input name="weekdays[]" type="checkbox" value="{$smarty.const.DAY_OF_WEEK_SAT}"> {lang('lbl_saturday')}
                                </label>
                                <label>
                                    <input name="weekdays[]" type="checkbox" value="{$smarty.const.DAY_OF_WEEK_SUN}"> {lang('lbl_sunday')}
                                </label>
                                <label>
                                    <input name="weekdays[]" type="checkbox" value="{$smarty.const.HOLIDAY}"> {lang('lbl_holiday')}
                                </label>
                            </td>
                        </tr>
                    </table>
                    <div align="center">
                        <button type="submit" class="btn-small btn-green btn-establish-day" target="_blank">{lang('btn_establish_day')}</button>
                        <span class="warning">{lang('warning_change_holiday')}</span>
                    </div>
                </form>
            </div>
        </div>
        <h1></h1>
        {foreach $calendars as $calendar}
        {$calendar}
        {/foreach}
    </div>
</div>
<div id="dialog-confirm" style="display: none"></div>

{literal}
<script>
    //<------------------------------------------------------------ CLICK ENTER TO SEARCH ----------------------------------------------------------->
    $(".table-simple").keypress(function (e) {
           var key = e.which;
           if(key == 13)  // the enter key code
            {
               e.preventDefault();
               if($("select[name='year']").is(':focus')) {
                   $('.btn-search').click();
               }
               if($("input[name='weekdays[]']").is(':focus')) {
                   $('.btn-establish-day').click();
               }

            }
    }
    );
    $(document).bind('keydown', function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            $('.btn-establish-day').click();
            return false;
        }
    });

    $(".btn-establish-day").on('click', function(event) {
        event.preventDefault();

        is_checked = $("#update-holiday input[type=checkbox]:checked").length ? true : false;
        if (!is_checked) {
            $("#dialog-confirm").html('休日を設定してください。').dialog({
                title: '確認',
                width: 'auto',
                height: 110,
                modal: true,
                buttons: []
            });
            return;
        }

        $("#dialog-confirm").html('休日を設定します。よろしいですか？').dialog({
            title: '確認',
            width: 'auto',
            height: 'auto',
            modal: true,
            buttons: {
                "はい": function () {
                    $.ajax({
                        url: '/_admin/holiday/ajax_update',
                        type: 'POST',
                        data: $("#update-holiday").serialize()
                    })
                    .done(function(data) {
                        dom =   "<form method='post' action='/_admin/holiday/show'>"+
                                    "<input type='hidden' name='year' value='"+$("#year-filter").val()+"'>"+
                                "</form>";
                        $(dom).appendTo('body').submit();
                    })
                    $(this).dialog('close');
                },
                "いいえ": function () {
                    $(this).dialog('close');
                }
            }
        });
    });

    $(".date").on('click', function(event) {
        element = $(this);
        date_object = new Date($(this).data('date'));
        today_object = new Date();
        element.parent().parent().parent().parent().addClass('checked');

        //JUST COMPARE DATE
        date_object.setHours(0,0,0,0);
        today_object.setHours(0,0,0,0);

        if (date_object.getTime() < today_object.getTime()) {
            $("#dialog-confirm").html('過去の日付は設定変更ができません。').dialog({
                title: '確認',
                width: 'auto',
                height: 110,
                modal: true,
                buttons: []
            });
            return;
        }

        if (date_object.getTime() == today_object.getTime()) {
            $("#dialog-confirm").html('現在の日付は設定変更ができません。').dialog({
                title: '確認',
                width: 'auto',
                height: 110,
                modal: true,
                buttons: []
            });
            return;
        }

        if ($(this).hasClass('holiday-day')) {
            $("#dialog-confirm").html('休日から営業日に変更しますがよろしいですか？');
        } else {
            $("#dialog-confirm").html('営業日から休日に変更しますがよろしいですか？');
        }

        $("#dialog-confirm").dialog({
            title: '確認',
            width: 'auto',
            height: 'auto',
            modal: true,
            buttons: {
                "はい": function () {
                   $.ajax({
                        url: '/_admin/holiday/ajax_set_day',
                        type: 'POST',
                        data: {date: element.data('date')}
                    })
                    .done(function(data) {
                        if (data.status) {
                            if (element.hasClass('holiday-day')) {
                                element.removeClass('holiday-day');
                                element.css('font-weight', 'normal');
                            } else {
                                element.addClass('holiday-day');
                                element.css('font-weight', 'bold');

                                $.each($('table.holiday-calendar').find('span.date'), function(index, value) {
                                    var date_obj = new Date($(value).data('date'));
                                    var year     = date_obj.getFullYear();
                                    var month    = (date_obj.getMonth() + 1).toString().replace(/^\d$/,'0$&');
                                    var day      =  date_obj.getDate().toString().replace(/^\d$/,'0$&');
                                    var strDate  = year + '-' + month + '-' + day;
                                    
                                    if (data.holiday_list.indexOf(strDate) != -1) {
                                        $(value).addClass('holiday-day').css('font-weight', 'bold');
                                    }
                                });

                                $.each($('table.holiday-calendar').find('b.date'), function(index, value) {
                                    var date_obj = new Date($(value).data('date'));
                                    var year     = date_obj.getFullYear();
                                    var month    = (date_obj.getMonth() + 1).toString().replace(/^\d$/,'0$&');
                                    var day      =  date_obj.getDate().toString().replace(/^\d$/,'0$&');
                                    var strDate  = year + '-' + month + '-' + day;
                                    
                                    if (data.holiday_list.indexOf(strDate) != -1) {
                                        $(value).addClass('holiday-day').css('font-weight', 'bold');
                                    }
                                });
                            }
                        }

                        $('table.checked').removeClass('checked');
                        $("#dialog-confirm").html(data.messages).dialog({
                            title: '確認',
                            width: 'auto',
                            height: 110,
                            modal: true,
                            buttons: []
                        });
                    })
                },
                "いいえ": function () {
                    $(this).dialog('close');
                }
            }
        });
    });

    // $("body").on('click', '.ui-widget-overlay', function(event) {
    //     $(".ui-dialog-content").dialog("close");
    // });
</script>
{/literal}

{include file = "inc/footer.tpl"}