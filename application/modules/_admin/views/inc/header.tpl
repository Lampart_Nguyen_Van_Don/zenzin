<!DOCTYPE html>
<html ng-app="app" id="ja" lang="ja">
<head>
<meta charset="utf-8">
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" href="/public/admin/img/favicon.ico" type="image/x-icon">
<link rel="icon" href="/public/admin/img/favicon.ico" type="image/x-icon">
<title>ZENRIN BIZ NEXUS販売管理システム</title>
<script src="/public/common/js/languages/lang_{config_item('language')}.js"></script>
{$head}
</head>
<body>