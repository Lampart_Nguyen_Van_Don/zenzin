<!-- s/wrapper -->
<div class="wrapper">
    <!-- s/slide-left-col -->
    <div class="slide-left-col">
        <div>
        {if $menu_group}
        {foreach from = $menu_group item = menu}
            {if isset($menu_action_group[$menu.id])}
                <h3 {if isset($current_group.menu_group_id) &&  ($current_group.menu_group_id == $menu.id)}class="on"{/if}>{$menu.name}</h3>
                <ul {if isset($current_group.menu_group_id) &&  ($current_group.menu_group_id == $menu.id)}class="open"{/if}>
                {foreach from = $menu_action_group[$menu.id] key=k item=group}
                    <li {if isset($current_group.action_group_id) && ($current_group.action_group_id == $k)} class="action-active" {/if}><a href="/{$module}/{$menu_action_group[$menu.id][$k][1]['method_name']}">{if isset($action_group_name[$k])}{$action_group_name[$k]}{/if}</a></li>
                {/foreach}
                </ul>
            {/if}
        {/foreach}
        {/if}
        </div>
    </div>
<!-- e/slide-left-col -->