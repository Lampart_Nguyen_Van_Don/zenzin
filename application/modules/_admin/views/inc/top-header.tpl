<!-- s/heaer -->
<div class="header">
    <div>
        <div class="head-btn-nav">
            <a id="slide-left-btn"><span class="genericon genericon-hierarchy"></span></a>
        </div>
        <div class="logo">
            <a href="/_admin"><img src="/public/admin/img/client-logo.png" alt="" height="40" /></a>
        </div>
        <div class="title">
            <!--#7069:Start-->
            <span class="account-name">{$account_name}</span>
            <a href="/_admin/account/change_password" class="head-btn-logout"><span class="genericon genericon-key"></span>{lang('common_lbl_change_password')}</a>
            <!--#7069:End-->
            <a href="/_admin/logout" class="head-btn-logout"><span class="genericon genericon-reply"></span>{lang('common_lbl_logout')}</a>
        </div>
    </div>
</div>
<!-- e/heaer -->