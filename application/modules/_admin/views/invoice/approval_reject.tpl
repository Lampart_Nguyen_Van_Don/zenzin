<style rel="styleSheet">

		.table-small {
		    width: 100%;
		}
</style>

<table class="table-simple" width="100%">
<tr>
<td>
    <div>
        <table class="table-simple table-small">
            <tr>
                <th width="140">{lang('lbl_sales_slip_number')}</th>
                <td>
                    {if $sales_slip.is_ad_receive_payment}AR{else}BN{/if}{$sales_slip.sales_slip_number}
                </td>
            </tr>
            <tr>
                <th>{lang('lbl_sales_slip_status')}</th>
                <td>
                    {if $sales_slip.sales_slip_status == $smarty.const.SALES_STATUS_INVOICE_APPROVAL_PENDING}{lang('lbl_status_approval_pending')}{/if}
                    {if $sales_slip.sales_slip_status == $smarty.const.SALES_STATUS_INVOICE_APPROVED}{lang('lbl_status_approved')}{/if}
                    {if $sales_slip.sales_slip_status == $smarty.const.SALES_STATUS_INVOICE_ISSUED}{lang('lbl_status_issued')}{/if}
                    {if $sales_slip.sales_slip_status == $smarty.const.SALES_STATUS_INVOICED_DISCARDED}{lang('lbl_status_discarded')}{/if}
                </td>
            </tr>
            <tr>
                <th>{lang('lbl_j_account_id')}</th>
                <td>{$sales_slip.account_name}</td>
            </tr>
            <tr>
                <th>{lang('lbl_s_account_id')}</th>
                <td>
                    {if $sales_slip.s_account_name_1st} {$sales_slip.s_account_name_1st}
                    {else} {$sales_slip.s_account_name_2st}
                    {/if}
                </td>
            </tr>
            <tr>
                <th>{lang('lbl_s_code')}</th>
                <td>{$sales_slip.s_code}</td>
            </tr>
            <tr>
                <th>{lang('lbl_zipcode')}</th>
                <td>{$sales_slip.zipcode}</td>
            </tr>
            <tr>
                <th>{lang('lbl_address_1')}</th>
                <td>{$sales_slip.address_1st}</td>
            </tr>
            <tr>
                <th>{lang('lbl_address_2')}</th>
                <td>{$sales_slip.address_2nd}</td>
            </tr>
            <tr>
                <th>{lang('lbl_client_name')}</th>
                <td>{$sales_slip.client_name}</td>
            </tr>
            <tr>
                <th>{lang('lbl_sort_division_name')}</th>
                <td>{$sales_slip.division_name}</td>
            </tr>
            <tr>
                <th>{lang('lbl_charge_name')}</th>
                <td>{$sales_slip.charge_name}</td>
            </tr>
            <tr>
                <th>TEL</th>
                <td>{$sales_slip.phone_no}</td>
            </tr>
            <tr>
                <th>FAX</th>
                <td>{$sales_slip.fax_no}</td>
            </tr>
            <tr>
                <th>{lang('lbl_bill_closing_date')}</th>
                <td>{date('Y/m/d', strtotime($sales_slip.billing_date))}</td>
            </tr>
            <tr>
                <th>{lang('lbl_payment_expire_date')}</th>
                <td>{date('Y/m/d', strtotime($sales_slip.payment_date))}</td>
            </tr>
        </table>
        <br class="br-10px">
        <div class="summary-wrapper">
            <span>{lang('lbl_cost_total_price')}: {number_format($sales_slip.total_amount)}円</span>
            <span>{lang('lbl_consumption_tax')}: {number_format($sales_slip.total_tax)}円</span>
            <span>{lang('lbl_tax_total')}: {number_format($sales_slip.total_amount_tax)}円</span>
        </div>
        <br class="br-10px">
        <table class="table-simple" width="100%">
            <tr>
                <th>{lang('lbl_j_code')}</th>
                <th>{lang('lbl_branch_cd')}</th>
                <th>{lang('lbl_delivery_date_2')}</th>
                <th>{lang('lbl_product_name')}</th>
                <th>{lang('lbl_contents')}</th>
                <th>{lang('lbl_tax_type')}</th>
                <th>{lang('lbl_quantity')}</th>
                <th>{lang('lbl_unit_price')}</th>
                <th>{lang('lbl_total')}</th>
            </tr>
            {foreach $sales_slip.details as $detail}
            <tr>
                <td>{$detail.j_code}</td>
                <td>{$detail.branch_cd}</td>
                <td>{str_replace('-', '/', $detail.delivery_date)}</td>
                <td>{$detail.product_name}</td>
                <td>{$detail.contents}</td>
                <td align="center" {if isset($detail.is_show_minus)}style="color:red"{/if}>
                    {if $detail.tax_type == $smarty.const.TAX_EXCLUSIVE}外{/if}
                    {if $detail.tax_type == $smarty.const.TAX_INCLUDED}内{/if}
                    {if $detail.tax_type == $smarty.const.TAX_NON}非{/if}
                </td>
                <td align="center" {if isset($detail.is_show_minus)}style="color:red"{/if}>
                	<!--6840: Start
                    {if isset($detail.is_show_minus)} {number_format(0 - $detail.quantity)}
                    {else} {$detail.quantity}
                    {/if}
                    6840: End -->
                    {if isset($detail.is_show_minus)} {number_format(0 - $detail.quantity)}
                    {else} {number_format($detail.quantity)}
                    {/if}
                </td>
                <!--6840: Start
                <td align="right" {if isset($detail.is_show_minus)}style="color:red"{/if}>{number_format($detail.unit_price)}</td>
                6840: End -->
                <td align="right" {if isset($detail.is_show_minus)}style="color:red"{/if}>{number_format($detail.unit_price, 2)}</td>
                <td align="right" {if isset($detail.is_show_minus)}style="color:red"{/if}>
                    {if isset($detail.is_show_minus)} {number_format(0 - $detail.amount)}
                    {else} {number_format($detail.amount)}
                    {/if}
                </td>
            </tr>
            {/foreach}
        </table>
        <br class="br-10px">
        <form id="approve_invoice" method="post" action="/_admin/invoice/approval_reject">
            <input type="hidden" name="sales_slip_id" value="{$sales_slip.id}">
            {lang('lbl_approval_reject_comment')}
            <textarea name="comment" cols="30" rows="7" style="display:block; width:100%"></textarea>
            <br class="br-10px">
            <button type="submit" name="approve" value="1" class="btn-small btn-green approve-invoice">{lang('btn_approve_invoice')}</button>
            <button type="submit" name="reject" value="1" class="btn-small btn-green reject-invoice">{lang('btn_reject_invoice')}</button>
            <button type="button" class="btn-small btn-green btn-close">{lang('common_lbl_close')}</button>
        </form>
    </div>
</td>
</tr>
</table>
{*#7036:Start*}
<script>
    var is_submitted = false;
    $('#approve_invoice').submit(function (e) {
        if (!is_submitted) {
            is_submitted = true;
            return true;
        } else {
            e.preventDefault();
            return false;
        }
    });
</script>
{*#7036:End*}