<p class="messages"></p>

<table class="table-simple" width="100%">
    <tr>
        <th width="140">{lang('lbl_sales_slip_number')}</th>
        <td>{if $sales_slip.is_ad_receive_payment}AR{else}BN{/if}{$sales_slip.sales_slip_number}</td>

        <th width="140">{lang('lbl_sales_slip_status')}</th>
        <td>
            {if $sales_slip.sales_slip_status == $smarty.const.SALES_STATUS_INVOICE_APPROVAL_PENDING}{lang('lbl_status_approval_pending')}{/if}
            {if $sales_slip.sales_slip_status == $smarty.const.SALES_STATUS_INVOICE_APPROVED}{lang('lbl_status_approved')}{/if}
            {if $sales_slip.sales_slip_status == $smarty.const.SALES_STATUS_INVOICE_ISSUED}{lang('lbl_status_issued')}{/if}
            {if $sales_slip.sales_slip_status == $smarty.const.SALES_STATUS_INVOICED_DISCARDED}{lang('lbl_status_discarded')}{/if}
        </td>
    </tr>

    <tr>
        <th>{lang('lbl_j_account_id')}</th>
        <td>{$sales_slip.account_name}</td>

        <th>{lang('lbl_s_account_id')}</th>
        <td>
            {if $sales_slip.s_account_name_1st}
                {$sales_slip.s_account_name_1st}
            {else}
                {$sales_slip.s_account_name_2nd}
            {/if}
        </td>
    </tr>

    <tr>
        <th>{lang('lbl_s_code')}</th>
        <td>{$sales_slip.s_code}</td>

        <th>{lang('lbl_client_name')}</th>
        <td>{$sales_slip.client_name}</td>
    </tr>

    <tr>
        <th>{lang('lbl_zipcode')}</th>
<!-- #9250:Start -->
        <td colspan="3">{substr_replace($sales_slip.zipcode, '-', 3, 0)}</td>
<!-- #9250:End -->
    </tr>

    <tr>
        <th>{lang('lbl_address')}</th>
        <td>{$sales_slip.address_1st}</td>

        <td colspan="2">{$sales_slip.address_2nd}</td>
    </tr>

    <tr>
        <th>{lang('lbl_sort_division_name')}</th>
        <td>{$sales_slip.division_name}</td>

        <th>{lang('lbl_charge_name')}</th>
        <td>{$sales_slip.charge_name}</td>
    </tr>

    <tr>
        <th>TEL</th>
        <td>{$sales_slip.phone_no}</td>

        <th>FAX</th>
        <td>{$sales_slip.fax_no}</td>
    </tr>

    <tr>
        <th>{lang('lbl_bill_closing_date')}</th>
        <td>{date('Y/m/d', strtotime($sales_slip.billing_date))}</td>

        <th>{lang('lbl_payment_expire_date')}</th>
        <td>{date('Y/m/d', strtotime($sales_slip.payment_date))}</td>
    </tr>

</table>
<br class="br-10px">
<div class="summary-wrapper">
    <span>{lang('lbl_cost_total_price')}: {number_format($sales_slip.total_amount)}円</span>
    <span>{lang('lbl_consumption_tax')}: {number_format($sales_slip.total_tax)}円</span>
    <span>{lang('lbl_tax_total')}: {number_format($sales_slip.total_amount_tax)}円</span>
</div>
<br class="br-10px">

<table class="table-simple" width="100%">
    <tr>
        <th>{lang('lbl_j_code')}</th>
        <th>{lang('lbl_branch_cd')}</th>
        <th>{lang('lbl_delivery_date_2')}</th>
        <th>{lang('lbl_product_name')}</th>
        <th>{lang('lbl_contents')}</th>
        <th>{lang('lbl_tax_type')}</th>
        <th>{lang('lbl_quantity')}</th>
        <th>{lang('lbl_unit_price')}</th>
        <th>{lang('lbl_total')}</th>
    </tr>
    {foreach $sales_slip.details as $detail}
    <tr>
        <td>{$detail.j_code}</td>
        <td>{$detail.branch_cd}</td>
        <td>{str_replace('-', '/', $detail.delivery_date)}</td>
        <td>{$detail.product_name}</td>
        <td>{$detail.contents}</td>
        <td align="center" {if isset($detail.is_show_minus)}style="color:red"{/if}>
            {if $detail.tax_type == $smarty.const.TAX_EXCLUSIVE}外{/if}
            {if $detail.tax_type == $smarty.const.TAX_INCLUDED}内{/if}
            {if $detail.tax_type == $smarty.const.TAX_NON}非{/if}
        </td>
        <td align="center" {if isset($detail.is_show_minus) || $detail.quantity < 0}style="color:red"{/if}>
        	<!--6840: Start
            {if isset($detail.is_show_minus)} {number_format(0 - $detail.quantity)}
            {else} {$detail.quantity}
            {/if}
            6840: End -->
            {if isset($detail.is_show_minus)} {number_format(0 - $detail.quantity)}
            {else} {number_format($detail.quantity)}
            {/if}
        </td>
        <!--6840: Start
        <td align="right" {if isset($detail.is_show_minus)}style="color:red"{/if}>{number_format($detail.unit_price)}</td>
        6840: End -->
        <td align="right" {if isset($detail.is_show_minus) || $detail.unit_price < 0}style="color:red"{/if}>{number_format($detail.unit_price, 2)}</td>
        <td align="right" {if isset($detail.is_show_minus) || $detail.amount < 0}style="color:red"{/if}>
            {if isset($detail.is_show_minus)} {number_format(0 - $detail.amount)}
            {else} {number_format($detail.amount)}
            {/if}
        </td>
    </tr>
    {/foreach}
    <tr>
        <td colspan="9" class="none-border action-panel">
            <button class="btn-small btn-green print-invoice" data-sales-slip-id="{$sales_slip.id}">{lang('btn_print_invoice')}</button>
            <button class="btn-small btn-green download-invoice" data-sales-slip-id="{$sales_slip.id}">{lang('btn_download_invoice')}</button>

            <!--6846: Start-->
            {if $this->auth->has_permission('invoice/ajax_discard_invoice') && !$is_settlement}
                <!--6846: End-->
                {if $sales_slip.sales_slip_status == $smarty.const.SALES_STATUS_INVOICE_APPROVAL_PENDING}
                    <button class="btn-small btn-green approval-reject-invoice" data-sales-slip-id="{$sales_slip.id}">{lang('btn_approval_reject')}</button>
                {/if}
            {/if}

            <!--6846: Start-->
            {if $this->auth->has_permission('invoice/ajax_discard_invoice_sale') && !$is_settlement}
                <!--6846: End-->
				  <!-- #8064:End -->
               		 {if ($sales_slip.sales_slip_status == $smarty.const.SALES_STATUS_INVOICE_ISSUED || $sales_slip.sales_slip_status == $smarty.const.SALES_STATUS_INVOICE_APPROVED)  && ($isdiscardIinvoice == 1)}
				  <!-- #8064:End -->
                       <button class="btn-small btn-green discard-invoice" data-sales-slip-id="{$sales_slip.id}">{lang('btn_discard')}</button>
                    {/if}
            {/if}

            <button class="btn-small btn-green btn-close">{lang('common_lbl_close')}</button>
        </td>
    </tr>
</table>

{literal}
<script>
    $(".print-invoice").on('click', function(event) {

      event.preventDefault();
      dom = '<form action="/_admin/invoice/approve_and_print" method="POST" target="_blank">' +
		     '<input type="hidden" name="sales_slip_ids" value="' + $(this).data('sales-slip-id') + '"/>' +
		    '<input type="hidden" name="is_print2" value="1"/>' +
		   '</form>';
		    $(dom).appendTo('body').submit();
		});
    $(".download-invoice").on('click', function(event) {
        event.preventDefault();
        dom = '<form action="/_admin/invoice/approve_and_print" method="POST">' +
                    '<input type="hidden" name="sales_slip_ids" value="' + $(this).data('sales-slip-id') + '"/>' +
                    '<input type="hidden" name="is_download" value="1"/>' +
            '</form>';

        $(dom).appendTo('body').submit();
    });

    $(".approval-reject-invoice").on('click', function(event) {
        event.preventDefault();

        $.post("/_admin/invoice/approval_reject", {
            sales_slip_id: $(this).data('sales-slip-id')
        },
      	function(data) {
            $("#dialog-detail").html("");
            $("#dialog-detail").dialog('close');
            $("#loaddatadetail").html(data);
            $("#loaddatadetail").dialog({
            title: '請求書承認/却下',
            width: 'auto',
            height: 'auto',
            modal: true,
            position: { my: "center", at: "center", of: window }
        });
    });

       return false;
      // dom = '<form action="/_admin/invoice/approval_reject" method="POST">' +
         //         '<input type="hidden" name="sales_slip_id" value="' + $(this).data('sales-slip-id') + '"/>' +
      //    '</form>';

    //  $(dom).appendTo('body').submit();
   });
</script>
<style type="text/css">
.table-simple th{
    min-width: 50px;
}
.none-border{
    border: none!important;
}
.action-panel{
    min-width: 500px;
}
</style>
{/literal}
