{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<style rel="styleSheet">
 #cbx-s-account-id{
  width: 200px;
 }
 .grid1439178440020 .ui-grid-coluiGrid-006 {
    min-width: 186px;
    max-width: 180px;
}
#cbx-j-account-id{
 width: 200px;
}
.error-color {
    color: red;
}
#7434:Start
.bt_search2{
height: 27px;
}
#7434:End
</style>

<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>
        <div class="search-wrapper" style="display: inline-block">
            <table class="table-simple" style="width: 100%">
                <tr id="search-toggle">
                    <td style="padding-left: 10px; border-bottom: none"><a href="javascript:void(0)">{lang('common_lbl_hide_search')}</a></td>
                </tr>
            </table>
            <form id="search-form" action="/_admin/invoice/show" method="post">
                <table class="table-search-invoice table-simple">
                    <tr>
                        <th>{lang('lbl_sales_slip_status')}</th>
                        <td>
                            <label>
                                <input type="checkbox" id="filter_status_{$smarty.const.SALES_STATUS_INVOICE_APPROVAL_PENDING}" name="sales_slip_status[]" value="{$smarty.const.SALES_STATUS_INVOICE_APPROVAL_PENDING}" {if isset($form.sales_slip_status) && in_array($smarty.const.SALES_STATUS_INVOICE_APPROVAL_PENDING, $form.sales_slip_status)}{'checked'}{/if}> {lang('lbl_status_approval_pending')}
                            </label>
                            <label>
                                <input type="checkbox" id="filter_status_{$smarty.const.SALES_STATUS_INVOICE_APPROVED}" name="sales_slip_status[]" value="{$smarty.const.SALES_STATUS_INVOICE_APPROVED}" {if isset($form.sales_slip_status) && in_array($smarty.const.SALES_STATUS_INVOICE_APPROVED, $form.sales_slip_status)}{'checked'}{/if}> {lang('lbl_status_approved')}
                            </label>
                            <label>
                                <input type="checkbox" id="filter_status_{$smarty.const.SALES_STATUS_INVOICE_ISSUED}" name="sales_slip_status[]" value="{$smarty.const.SALES_STATUS_INVOICE_ISSUED}" {if isset($form.sales_slip_status) && in_array($smarty.const.SALES_STATUS_INVOICE_ISSUED, $form.sales_slip_status)}{'checked'}{/if}> {lang('lbl_status_issued')}
                            </label>
                            <label>
                                <input type="checkbox" id="filter_status_{$smarty.const.SALES_STATUS_INVOICED_DISCARDED}" name="sales_slip_status[]" value="{$smarty.const.SALES_STATUS_INVOICED_DISCARDED}" {if isset($form.sales_slip_status) && in_array($smarty.const.SALES_STATUS_INVOICED_DISCARDED, $form.sales_slip_status)}{'checked'}{/if}> {lang('lbl_status_discarded')}
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_j_account_id')}</th>
                        <td>
                            <div class="peer-cbx">
                                {lang('lbl_business_id')}
                                <select name="j_account_business_unit_id" class="max-width" {if $is_self_invoices==1 && $is_product_division == 1 } disabled="disabled"  {/if}  id="cbx-business-unit-id-for-j-account">
                                    {if (count($j_account_business_units)) != 1}
                                        <option value="-1">{lang('common_lbl_all')}</option>
                                    {/if}

                                    {foreach $j_account_business_units as $business_unit}
                                        <option value="{$business_unit.id}"
                                            {if isset($form.j_account_business_unit_id) && $business_unit.id == $form.j_account_business_unit_id} {'selected'}
                                            {else}
{*#6688:Start*}
                                                {if $business_unit.id == $default_j_account_business_unit}{if $is_product_division  && !$is_submit_default}{'selected'}{/if}{/if}
{*#6688:End*}
                                        {/if}>{$business_unit.name}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="peer-cbx">
                                {lang('lbl_j_account_id')}
                                <select name="j_account_id" id="cbx-j-account-id" class="max-width" {if $is_self_invoices==1 && $is_product_division == 1 } disabled="disabled"  {/if}  >
                                    {if (count($j_accounts)) != 1}
                                        <option    value="-1">{lang('common_lbl_all')}</option>
                                    {/if}
                                    {foreach $j_accounts as $account}
                                    <option value="{$account.id}" {if isset($form.j_account_id) && $account.id == $form.j_account_id}
                                                      {if $is_self_invoices == 1}    {'selected'}{/if}
                                                {else}
                                                {if !isset($form.j_account_id) && $account.id == $default_j_account}
                                                        {if $is_self_invoices == 1}    {'selected'} {/if}
                                                {/if}
                                                {/if} >{$account.name}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_sales_slip_number')}</th>
                        <td>
                            <input type="text" name="sales_slip_number" value="{if isset($form.sales_slip_number)}{$form.sales_slip_number}{/if}">
                            <p class="search-tooltip">{lang('lbl_sales_slip_number_tooltip')}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_client_name')}</th>
                        <td>
                            <input type="text" name="client_name" value="{if isset($form.client_name)}{$form.client_name}{/if}">
                            <p class="search-tooltip">{lang('lbl_approximate_search')}</p>
                        </td>
                    </tr>
                    <tr>
                            <th>{lang('lbl_delivery_date')}</th>
                            <td>
                                <input type="text" value="{$closedaydevilyday}" name="delivery_date_from"> ～
                                <input type="text" value="{$delivery_date_to}" name="delivery_date_to">
                                <p class="search-tooltip">{lang('lbl_date_tooltip')}</p>

<!--#6688:Start-->
                                {if !empty($error_messages['delivery_date'])}
                                <span class="validation-error" id="delivery_date_error">{$error_messages['delivery_date']}</span>
                                {else}
<!--#6688:End-->
                                <span class="validation-error" id="delivery_date_error" style="display: none"></span>
<!--#6688:Start-->
                                {/if}
<!--#6688:End-->
                            </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_billing_date')}</th>
                        <td>
                            <input type="text" name="from_billing_date" value="{if isset($form.from_billing_date)}{$form.from_billing_date}{/if}"> ～
                            <input type="text" name="to_billing_date" value="{if isset($form.to_billing_date)}{$form.to_billing_date}{/if}">
                            <p class="search-tooltip">{lang('lbl_date_tooltip')}</p>
                            <span class="validation-error" id="billing_date_error" style="display: none"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_payment_date')}</th>
                        <td>
                            <input type="text" name="from_payment_date" value="{if isset($form.from_payment_date)}{$form.from_payment_date}{/if}"> ～
                            <input type="text" name="to_payment_date" value="{if isset($form.to_payment_date)}{$form.to_payment_date}{/if}">
                            <p class="search-tooltip">{lang('lbl_date_tooltip')}</p>
                            <span class="validation-error" id="payment_date_error" style="display: none"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_s_code')}</th>
                        <td>
                            <input type="text" name="s_code" value="{if isset($form.s_code)}{$form.s_code}{/if}">
                            <p class="search-tooltip">{lang('lbl_s_code_tooltip')}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_name_kana')}</th>
                        <td>
                            <input type="text" name="name_kana" value="{if isset($form.name_kana)}{$form.name_kana}{/if}">
                            <p class="search-tooltip">{lang('lbl_approximate_search')}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_division_name')}</th>
                        <td>
                            <input type="text" name="division_name" value="{if isset($form.division_name)}{$form.division_name}{/if}">
                            <p class="search-tooltip">{lang('lbl_approximate_search')}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_s_account_id')}</th>
                        <td>
                            <div class="peer-cbx">
                                {lang('lbl_business_id')}
                                <select name="s_account_business_unit_id" id="cbx-business-unit-id-for-s-account">
                                    <option value="-1">{lang('common_lbl_all')}</option>
                                    {foreach $s_account_business_units as $business_unit}
                                    <option value="{$business_unit.id}" {if isset($form.s_account_business_unit_id) && $business_unit.id == $form.s_account_business_unit_id}{'selected'}{/if}>{$business_unit.name}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <div class="peer-cbx">
                                {lang('lbl_s_account_id')}
                                <select name="s_account_id" id="cbx-s-account-id">
                                    <option value="-1">{lang('common_lbl_all')}</option>
                                    {foreach $s_accounts as $s_account}
                                    <option value="{$s_account.id}" {if isset($form.s_account_id) && ($s_account.id == $form.s_account_id)}{'selected'}{/if}>{$s_account.name}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button type="button" class="btn-small btn-green btn-search bt_search2" ng-click="search()">{lang('common_lbl_search')}</button>

                           <input type="hidden" value="0" name="loadpage" id="loadpage" />

                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <hr class="blank-10px">
        <h1 style="border-color: rgb(68, 147, 208);"></h1>
        <hr class="blank-10px">
<!-- #9787:Start -->
        {if $this->session->flashdata('reject_result')}
            {$reject_result = $this->session->flashdata('reject_result')}
            <div class="{if $reject_result.status}success-message{else}error-message{/if}">{$reject_result.message}</div>    
        {/if}
<!-- #9787:End -->
        <div ng-controller="MainCtrl">
            <div ui-i18n="{literal}{{lang}}{/literal}"></div>
            {if $approve_permission}
            <button class="btn-small btn-green" ng-click="approve_and_print()">{lang('lbl_approve_and_invoice')}</button>
<!--#7434:Start !-->
                             <button type="button" onclick="export_csv()" class="btn-small btn-green"  >{lang("common_lbl_exportcsv")}</button>
<!--#7434:End !-->
            {/if}
            <br class="br-10px">

            <div class="grid gridStyle invoice-grid" ui-grid="gridOptions" ui-grid-pinning ui-grid-resize-columns ui-grid-pagination ui-grid-edit ui-grid-row-edit ui-grid-exporter>
                <div class="watermark" ng-class="{literal}{'show-watermark' : showWatermark}{/literal}">{lang('common_lbl_no_data')}</div>
                <div class="watermark" ng-class="{literal}{'show-watermark' : is_over_record}{/literal}">{lang('common_msg_over_record')}</div>
            </div>
            <div id="dialog-detail" style="display:none" compile="sales_slip_detail"></div>
            <div id="loaddatadetail" style="display:none" compile="sales_slip_detail_rej"></div>
<!--#7578:Start !-->
			{if $approve_permission}
            		<button class="btn-small btn-green" ng-click="approve_and_print()">{lang('lbl_approve_and_invoice')}</button>
             		<button type="button" onclick="export_csv()" class="btn-small btn-green"  >{lang("common_lbl_exportcsv")}</button>
            {/if}
<!--#7578:End !-->

        </div>
    </div>
</div>
<div id="sales_slips_result" style="display:none">{$sales_slips}</div>
<!-- #7461: Start !-->
<!-- div id="dialog-show-client" style="display:none"></div> -->

 <div id="clientDialog"></div>
<div id="dialog-show-client" style="display:none"></div>
<div id="dialog-errors" style="display:none"></div>
<!-- #7461: End !-->

<div id="dialog-messages" style="display:none"></div>
<div id="dialogdiscard" style="display:none">{lang('areyousure')}</div>
<script>

//#7461:Start
    clientDialog = $('#clientDialog');
    clientDialog.dialog({
        autoOpen : false,
        title: '{lang('title_client_detail')}',
        height: 'auto',
        width: 'auto',
        modal: true,
        beforeClose: function( event, ui ) {
            return close_confirm();
        },
        close : function() {
            form_changed = false;
            if (typeof cl_functions_loaded !== 'undefined' && cl_functions_loaded) {
                unbind_save_key();
            }
        }
    });

//#7461:End

    // 6846: Start
    var closing_month = '{$closedaydevilyday}';
    // 6846: End

    //#6688:Start
    var is_submit_default = {$is_submit_default};
    //#6688:End

    label = {
        approve_all: "{lang('lbl_approve_all')}",
        action: "{lang('common_lbl_action')}",
        sales_slip_status: "{lang('lbl_sales_slip_status')}",
        sales_slip_number: "{lang('lbl_sales_slip_number')}",
        j_account_id: "{lang('lbl_j_account_id')}",
        s_code: "{lang('lbl_s_code')}",
        client_name: "{lang('lbl_client_name')}",
        charge_name: "{lang('lbl_charge_name')}",
        billing_date: "{lang('lbl_billing_date')}",
        payment_date: "{lang('lbl_payment_date')}",
        cost_total_price: "{lang('lbl_cost_total_price')}",
        consumption_tax: "{lang('lbl_consumption_tax')}",
        tax_total: "{lang('lbl_tax_total')}",
        income_month: "{lang('lbl_income_month')}",
        check_date: "{lang('lbl_income_month')}",
        delivery_date: "{lang('lbl_delivery_date')}",
        view: "{lang('common_lbl_view')}",
        select_all: "{lang('common_lbl_all')}",
        open_search: "{lang('common_lbl_open_search')}",
        hide_search: "{lang('common_lbl_hide_search')}",
        status_approval_pending: "{lang('lbl_status_approval_pending')}",
        status_approved: "{lang('lbl_status_approved')}",
        status_issued: "{lang('lbl_status_issued')}",
        status_discarded: "{lang('lbl_status_discarded')}",
        status_sales_slip_created: "{lang('lbl_status_sales_slip_created')}",
        please_select_approve_print: "{lang('lbl_please_select_approve_print')}",
//#8213:Start
        lbl_invoice_approval_account_date: "{lang('lbl_invoice_approval_account_date')}"
//#8213:End

    };

    ss_status = {
        ss_no_input: {$smarty.const.SALES_STATUS_NO_INPUT},
        ss_created: {$smarty.const.SALES_STATUS_CREATED},
        inv_approval_pending: {$smarty.const.SALES_STATUS_INVOICE_APPROVAL_PENDING},
        inv_approved: {$smarty.const.SALES_STATUS_INVOICE_APPROVED},
        inv_issued: {$smarty.const.SALES_STATUS_INVOICE_ISSUED},
        inv_discarded: {$smarty.const.SALES_STATUS_INVOICED_DISCARDED}
    };
    approve_permission = {if $approve_permission} true {else} false {/if};
</script>

{literal}
<script>

    $("#cbx-business-unit-id-for-j-account, #cbx-business-unit-id-for-s-account").on('change', function(event) {
       var  business_unit_id = $(this).val();
        append_target_id = '#cbx-j-account-id';

        if (event.target.id == 'cbx-business-unit-id-for-s-account') {
            append_target_id = '#cbx-s-account-id';
        }


        url = '/_admin/invoice/ajax_get_account_by_business_unit_id_invoice';
        if (business_unit_id == -1) {
            url = '/_admin/invoice/ajax_get_all_accounts_invoice';
        }

        $.ajax({
            url: url,
            type: 'POST',
            data: {business_unit_id: business_unit_id}
        })
        .done(function(data) {
            if (data == 0) {
                 dom = "<option value='-1'>" + label.select_all + "</option>";
                 $(append_target_id).html(dom);
                return false;
            }

            dom = "<option value='-1'>" + label.select_all + "</option>";
            $.each(data, function(index, val) {
                 dom += "<option value='" + val.id + "'>" + val.name + "</option>";
            });
            $(append_target_id).html(dom);
        })

    });

    $("#search-toggle").on('click', function(event) {
        if ($('#search-form').is(':visible')) {
            $('#search-toggle').parent().parent().css('width', $('#search-form').width());
            $('#search-toggle td').css('border-bottom', '1px solid #ddd');
            $('#search-toggle a').text(label.open_search);
        } else {
            $('#search-toggle').css('width', '100%');
            $('#search-toggle td').css('border-bottom', 'none');
            $('#search-toggle a').text(label.hide_search);
        }

        $("#search-form").slideToggle('slow');
    });

    $('body').on('click', '.btn-close', function() {
        $(this).parents('.ui-dialog-content').dialog('close');
    });
</script>
{/literal}

{include file = "inc/footer.tpl"}