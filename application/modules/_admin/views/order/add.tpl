<script type="text/javascript">
    var clients = null;
    {if $clients}
        clients = {$clients};
    {/if};
</script>

<style>
#add_grid select{
    height: 30px;
}
#add_grid input{
    height: 30px;
}
/*#6959:Start*/
#add_grid .grid {
    width: 100% !important
}
/*#6959:End*/
/*
-- #6901 Add -S !
*/
#dialog_error_check_change_report{
    display: none;
}
/* #6901 Add -E !*/
</style>

<div ng-controller="AddCtrl">
    <!--#6972:Start-->
    <div ui-i18n="{literal}{{lang}}{/literal}"></div>
    <!--#6972:End-->
    
    {lang('lbl_search_condition')}
    <div ng-bind-html="error_success"></div>
    <table class="table-simple"  style="width: 100%;">
        {if $clients}
            <tr ng-show="show_search">
                <td>{lang('lbl_search_client')}</td>
                <td>
                    <table class="tbl_client">
                        <tr>
                            <td>{lang('lbl_s_code')}</td>
                            <td>
                                <input type="tel" ng-model="search_s_code">
                                <button type="button" class="btn-small btn-green btn-search" ng-click="searchClients()">{lang('lbl_filter')}</button>
                                <div class="legend">(前方一致検索）</div>
                            </td>
                        </tr>
                        <tr>
                            <td>{lang('lbl_name')}</td>
                            <td>
                                <input type="text" ng-model="search_client_name">
                                <button type="button" class="btn-small btn-green btn-search" ng-click="searchClients()">{lang('lbl_filter')}</button>
                                <div class="legend">(あいまい検索）</div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <div ng-show="operators.length">
                                    {*<select ng-model="client.id" ng-init="client.id = options[0]">*}
                                    <select ng-model="client.id" ng-init="client.id = operators[0].id" ng-options="operator.id as operator.s_code + ' ' + operator.name + ' ' + operator.division_name + ' ' + operator.charge_name for operator in operators">
                                    {*<!-- ng-selected="{literal}{{operator.value == client.id}}{/literal}" -->*}
                                        {*<option*}
                                                {*ng-repeat="operator in operators"*}
                                                {*value="{literal}{{operator.id}}{/literal}">*}
                                          {*{literal}{{operator.s_code + ' ' + operator.name + ' ' + operator.division_name + ' ' + operator.charge_name}}{/literal}*}
                                        {*</option>*}
                                    </select>
                                    <button class="btn-small btn-green" ng-click="clientChange()">{lang('lbl_client_change')}</button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <div ng-show="operators.length==0">データがありません</div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        {/if}

        <tr>
            <td>{lang('lbl_applier_info')}</td>
            <td>
                <table class="tbl_client">
                    <tr>
                        <td>{lang('lbl_s_code')}</td>
                        <td>
                            <div id="s_code">{literal}{{client.s_code}}{/literal}</div>
                            <!-- <span class="red small" ng-show="client.credit_level=='3'">都度与信が必要であるこ</span> -->
                        </td>
                        <td>{lang('lbl_name')}</td>
                        <td>
                            <div id="client_name">{literal}{{client.name}}{/literal}</div>
                        </td>
                    </tr>
                    <!--
                    <tr>
                        <td>{lang('lbl_name')}</td>
                        <td>
                            <div id="client_name">{literal}{{client.name}}{/literal}</div>
                        </td>
                    </tr>
                    -->
                    <tr>
                        <td colspan="3" width: 250px;>{lang('lbl_client_name_furigana')}</td>
                        <td >
                            <div id="client_name_furigana">{literal}{{client.name_kana}}{/literal}</div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px;">{lang('lbl_zipcode')}<span class="red"> ※</span></td>
                        <td>
                            <input type="tel" maxlength="7" name="zip_code" id="zip_code" ng-model="client.zipcode">
                        </td>
                        <td colspan="2">
                            <div class="small red">ハイフンは入力しないでください</div>
                        </td>
                    </tr>
                     <tr>
                        <td>{lang('lbl_address')}1<span class="red"> ※</span></td>
                        <td colspan="3">
                            <input type="text" name="address_1st" id="address_1st" size="59" ng-model="client.address_1st">
                        </td>
                    </tr>
                   <tr>
                        <td>{lang('lbl_address')}2<span class="red"></span></td>
                        <td colspan="3">
                            <input type="text" name="address_2nd" id="address_2nd" size="59" ng-model="client.address_2nd">
                        </td>
                    </tr>
                    <tr>
                        <td>TEL<span class="red"> ※</span></td>
                        <td>
                            <input type="tel" name="tel" id="tel" ng-model="client.phone_no">
                        </td>
                        <td align="right" style="width: 123px;">FAX</td>
                        <td>
                            <div>
                                <input type="tel" name="fax" id="fax" ng-model="client.fax_no">
                            </div>
                        </td>
                        <td>
                            <div class="small red">ハイフンを入力してください</div>
                        </td>
                    </tr>
                    <tr>
                        <td>{lang('lbl_devision_name')}</td>
                        <td>
                            <input type="text" name="division_name" id="division_name" ng-model="client.division_name">
                        </td>
                        <td align="right">{lang('lbl_charge_name')} <span class="red"> ※</span></td>
                        <td>
                            <input type="text" name="charge_name" id="charge_name" ng-model="client.charge_name">
                        </td>
                    </tr>
                    <tr>
                        <td>{lang('lbl_payment_term')}</td>
                        <td colspan="3">
                            <div id="payment_terms" ng-show="client.is_ad_receive_payment=='0'">
                                {lang('lbl_closing_date')}： <span id="closing_date_name"></span>
                                {lang('lbl_payment_month_cd')}：<span id="payment_month_cd_name"></span>
                                {lang('lbl_payment_day_cd')}： <span id="payment_day_cd_name"></span>
                            </div>
                            <div id="payment_terms" ng-show="client.is_ad_receive_payment=='1'">
                                {lang('lbl_receive_payment')}
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="tbl_client">
                    <tr>
                        <td width="85%">
                            <div class="left-title" ng-show="client.is_ad_receive_payment=='1'">{lang('lbl_credit_infor_ad_receive')}</div>
                            <div class="left-title" ng-show="client.is_ad_receive_payment=='0'">{literal}与信限度額（{{client.credit_amount_show}}）{/literal}</div>
<!-- #9763:Start -->
                            <div ng-show="client.is_ad_receive_payment=='0'">{literal}売掛金（{{client.client_debt}}）{/literal}</div>
<!-- #9763: -->
                        </td>
                        <td>
                            <input id="checkbox_money_id" type="checkbox" ng-change="setDeliveryDate()"  ng-click="compute_credit()" ng-checked="client.is_ad_receive_payment" ng-model="client.is_ad_receive_payment" ng-disabled="client.is_ad_receive_payment_init==1" ng-true-value="1" ng-false-value="0">
                            <label for="checkbox_money_id">{lang('lbl_receive_payment')}</label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>{lang('lbl_account_id')}<span class="red"> ※</span></td>
            <td>
                {if (count($j_accounts) == 1)}
                    <select ng-model="client.j_account" name="j_account" id='j_account' disabled>
                        {html_options values=$j_accounts[0]->id output=$j_accounts[0]->name}
                    </select>
                {else}
                    <select ng-model="client.j_account" name="j_account" id='j_account'>
                        <!-- <option value="0">＝＝すべて＝＝</option> -->
                        {foreach from=$j_accounts item=a}
                            {html_options values=$a->id output=$a->name}
                        {/foreach}
                    </select>
                {/if}
            </td>
        </tr>
    </table>
        
    <div style="margin: 10px 0 0;">
        {lang('lbl_cost_total_price')}：{literal}{{client.cost_total_price | number:0}}{/literal}円   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {lang('lbl_consumption_tax')}：{literal}{{client.consumption_tax | number:0}}{/literal}円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {lang('lbl_tax_total')}：{literal}{{client.tax_total | number:0}}{/literal}円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {lang('lbl_gross_profit_amount')}：{literal}{{client.gross_profit_total | number:0}}{/literal}円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
        
    <!------------------------------------------ GRID -------------------------------------------->
    <div id="add_grid" style="margin: 10px 0px 10px 0px; font-size:12px;">
        <div ui-grid="gridOptionsAdd" ui-grid-pinning ui-grid-resize-columns ui-grid-edit ui-grid-row-edit ui-grid-cellnav ui-grid-selection class="grid"></div>
    </div>
    <!------------------------------------------ END GRID -------------------------------------------->
    
    <div style="margin-bottom: 10px">
        <button class="btn-small btn-green btn-add" type="button" ng-disabled="is_credit_over" ng-class="is_credit_over ? 'btn-gray' : 'btn-green'" id="addData" ng-click="addData()" ng-show="client.s_code">{lang('lbl_add_branch')}</button>
    </div>
    
    <table class="table-simple"  style="width: 100%;">
        <tr>
            <td width="140">{lang('lbl_remark')}<span class="red"> ※</span></td>
            <td>
                <textarea name="remark" id="remark" ng-model="client.remark" rows="7" cols="" style="width: 100%"></textarea>
            </td>
        </tr>
    </table>
    <br>
    <button class="btn-small btn-save" ng-click="saveData()" ng-disabled="(is_submitted || is_credit_over) ?  true : false" ng-class="(is_submitted || is_credit_over) ? 'btn-gray' : 'btn-green'">{lang('lbl_add_order_save')}</button>
    <button class="btn-small btn-green btn-close" ng-click="back()">{lang('common_lbl_close')}</button>
    <div style="color: red;" ng-bind-html="errors"></div>
</div>
