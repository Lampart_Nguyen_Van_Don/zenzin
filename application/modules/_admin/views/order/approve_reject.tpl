<script type="text/javascript">
    //init global variables
    var jApproveRejects = [
                    '{lang("lbl_branch_cd")}',
                    '{lang("lbl_order_status")}',
                    '{lang("lbl_product_name")}',
                    '{lang("lbl_content")}',
                    '{lang("lbl_delivery_date")}',
                    '{lang("lbl_billing_date")}',
                    '{lang("lbl_payment_date")}',
                    '{lang("lbl_quantity")}',
                    '{lang("lbl_tax_type")}',
                    '{lang("lbl_unit_price")}',
                    '{lang("lbl_amount")}',
                    '{lang("lbl_order_detail_cost_unit_price")}',
                    '{lang("lbl_order_detail_cost_total_price")}',
                    '{lang("lbl_gross_profit_amount")}',
                    '{lang("lbl_order_detail_gross_profit_rate")}',
    ];
</script>

<div ng-controller="approveRejectCtrl">
    <br/>
    <div ng-bind-html="error_success"></div>
    <div id="form_error_msg_reject"></div>
    <table class="table-simple">
        <tr>
            <td>{lang('lbl_j_code')}</td>
            <td>{literal}{{order.o_j_code}}{/literal}</td>
        </tr>
        <tr>
            <td>{lang('lbl_s_code')}</td>
            <td>{literal}{{order.c_s_code}}{/literal}</td>
        </tr>
        <tr>
            <td>{lang('lbl_name')}</td>
            <td>{literal}{{order.c_name}}{/literal}</td>
        </tr>
        <tr>
            <td>{lang('lbl_name_kana')}</td>
            <td>{literal}{{order.c_name_kana}}{/literal}</td>
        </tr>
        <tr>
            <td>{lang('lbl_zipcode')}</td>
            <td>{literal}{{order.c_zipcode | limitTo:3:0}}-{{order.c_zipcode | limitTo:4:3}}{/literal}</td>
        </tr>
        <tr>
            <td>{lang('lbl_address')}1</td>
            <td>{literal}{{order.o_address_1st}}{/literal}</td>
        </tr>
       <tr>
            <td>{lang('lbl_address')}2</td>
            <td>{literal}{{order.o_address_2nd}}{/literal}</td>
        </tr>
        <tr>
            <td>{lang('lbl_phone_fax')}</td>
            <td>{lang('lbl_phone')}: {literal}{{order.o_phone_no}}{/literal}&nbsp;&nbsp;&nbsp;{lang('lbl_fax')}: {literal}{{order.o_fax_no}}{/literal}</td>
        </tr>
        <tr>
            <td>{lang('lbl_devision_name')}</td>
            <td>{literal}{{order.o_division_name}}{/literal}</td>
        </tr>
        <tr>
            <td>{lang('lbl_charge_name')}</td>
            <td>{literal}{{order.o_charge_name}}{/literal}</td>
        </tr>
        <tr>
            <td>{lang('lbl_payment_term')}</td>
            <td>{literal}{{order.payment_terms}}{/literal}</td>
        </tr>
        <tr>
            <td>{lang('lbl_account_id')}</td>
            <td>{literal}{{order.a_name}}{/literal}</td>
        </tr>
        <tr>
            <td colspan="2">
                                            税別合計：{literal}{{order.o_cost_total_price | number:0}}{/literal}円   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            消費税：{literal}{{order.o_consumption_tax | number:0}}{/literal}円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            税込合計：{literal}{{order.o_tax_total | number:0}}{/literal}円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            粗利合計：{literal}{{order.o_gross_profit_total | number:0}}{/literal}円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <div style="margin: 10px 0px 10px 0px;">
                    <div ui-grid="gridOptionsDetailApproveReject" ui-grid-resize-columns class="grid"></div>
                </div>
            </td>
        </tr>

        <tr>
            <td>{lang('lbl_remark')}</td>
            <td><textarea rows="3" cols="100" id="remark" disabled>{literal}{{order.o_remark}}{/literal}</textarea></td>
        </tr>
        <tr>
            <td>{lang('lbl_comment')}</td>
            <td><textarea rows="3" cols="100" ng-model="comment" id="comment"></textarea></td>
        </tr>
        <tr>
           <td colspan="2">
              <button type="button" ng-disabled="(is_submitted || !isCanEdit) ?  true : false" ng-class="(is_submitted || !isCanEdit) ? 'btn-gray' : 'btn-green'" ng-click="approve(order.o_id)" class="btn-small">{lang('btn_approve')}</button>
              <button type="button" ng-disabled="(is_submitted || !isCanEdit) ?  true : false" ng-class="(is_submitted || !isCanEdit) ? 'btn-gray' : 'btn-green'" ng-click="reject(order.o_id)" class="btn-small">{lang('btn_reject')}</button>
              <button type="button" ng-click="close()" class="btn-small btn-green btn-close">{lang('lbl_close')}</button>
          </td>
       </tr>
    </table>
</div>
