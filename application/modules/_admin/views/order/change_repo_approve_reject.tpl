<div ng-controller="ChangeRepoCtrl">
    <div ng-bind-html="error_success"></div>
    <div>
        <table class="table-simple" style="width: 100%">
            <tr>
                <th width="140">{lang('lbl_j_code')}</th>
                <td>{literal}{{order.o_j_code}}{/literal}</td>

                <th width="140">{lang('lbl_s_code')}</th>
                <td>{literal}{{order.c_s_code}}{/literal}</td>
            </tr>

            <tr>
                <th>{lang('lbl_name')}</th>
                <td>{literal}{{order.c_name}}{/literal}</td>

                <th>{lang('lbl_zipcode')}</th>
                <td>{literal}{{order.c_zipcode | limitTo:3:0}}-{{order.c_zipcode | limitTo:4:3}}{/literal}</td>
            </tr>

            <tr>
                <th>{lang('lbl_address')}</th>
                <td>{literal}{{order.o_address_1st}}{/literal}</td>

                <td colspan="2">{literal}{{order.o_address_2nd}}{/literal}</td>
            </tr>

            <tr>
                <th>{lang('lbl_phone')}</th>
                <td>{literal}{{order.o_phone_no}}{/literal}</td>

                <th>{lang('lbl_fax')}</th>
                <td>{literal}{{order.o_fax_no}}{/literal}</td>
            </tr>

            <tr>
                <th>{lang('lbl_devision_name')}</th>
                <td>{literal}{{order.o_division_name}}{/literal}</td>

                <th>{lang('lbl_charge_name')}</th>
                <td>{literal}{{order.o_charge_name}}{/literal}</td>
            </tr>

            <tr>
                <th>{lang('lbl_payment_term')}</th>
                <td colspan="3">{literal}{{order.payment_terms}}{/literal}</td>
            </tr>

            <tr>
                <th>{lang('lbl_account_id')}</th>
                <td colspan="3">{literal}{{order.a_name}}{/literal}</td>
            </tr>
        </table>
    </div>
    <br>
    <div>
        <div ui-grid="gridChangeReportOptions" ui-grid-pinning ui-grid-resize-columns ui-grid-edit ui-grid-row-edit ui-grid-cellnav class="grid grid-order"></div>
    </div>
    <br>
    <div>
        <table class="table-simple" style="width:100%;">
            <tr>
                <td width="20%">{lang('lbl_comment_report')}</td>
                <td><textarea name="comment" id="comment" cols="90" rows="5" disabled>{literal}{{order.o_comment}}{/literal}</textarea></td>
            </tr>
        </table>
    </div>
    <br>
    <hr>
    <br>
    <div>
        <table class="table-simple" style="width:100%;">
            <tr>
                <td width="20%">{lang('lbl_comment_report_approve_reject')}</td>
                <td><textarea name="report_comment" id="report_comment" ng-model="report_comment" cols="90" rows="5"></textarea></td>
            </tr>
        </table>
    </div>
    <br>
    <button type="button" ng-disabled="is_submitted" ng-class="is_submitted ? 'btn-gray' : 'btn-green'" ng-click="approve_report()" class="btn-small">{lang('lbl_approve_report')}</button>
    <button type="button" ng-disabled="is_submitted" ng-class="is_submitted ? 'btn-gray' : 'btn-green'" ng-click="reject_report()" class="btn-small">{lang('lbl_reject_report')}</button>
    <button type="button" ng-click="close()" class="btn-small btn-green btn-close">{lang('lbl_close')}</button>
</div>