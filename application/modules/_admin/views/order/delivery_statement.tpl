{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

<link href="/public/admin/order/main.css" rel="stylesheet" media="all"/>
<link href="/public/admin/css/order_acceptance.css" rel="stylesheet" media="all"/>

<div class="cell content slide-right">
    <div>
        <h1 style="border-color: rgb(68, 147, 208);">{$title}</h1>
<!-- #7255:Start -->
<!-- #7117:Start -->
<!--        <div class="import-container"> -->
       <div class="import-container">
<!--         <div> -->
<!-- #7117:End -->
<!-- #7255:End -->
            {form_open('', 'id="searchform"')}

            <div id="error-container">
                {if isset($message)}
                    {$message}
                {/if}
            </div>

            <table class="table-export table-simple">
                <tr>
<!-- #7255:Start -->
<!--                    <td width="100"><label>計上年月</label></td> -->
                    <th><label>計上年月</label></th>
<!-- #7255:End -->
                    <td>
                        <input type="text" name="date_from" value="{$date_from}"> ～
                        <input type="text" name="date_to" value="{$date_to}">
<!-- #7255:Start -->
                        <button type="submit" class="btn-small btn-green btn-downloads" ng-click="search()">ダウンロード</button>
<!-- #7255:End -->
                        <div class="legend">(From必須　Fromのみ指定の場合、指定年月以降を検索　From・To指定で範囲指定検索)</div>
                    </td>
<!-- #7255:Start -->
<!--                    <td>
                        <button type="submit" class="btn-small btn-green btn-downloads" ng-click="search()">ダウンロード</button>
                    </td>
-->
<!-- #7255:End -->
                </tr>
            </table>

            {form_close()}
        </div>
    </div>
</div>

{include file = "inc/footer.tpl"}