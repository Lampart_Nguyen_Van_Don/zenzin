<!--#7427:Start-->
<script type="text/javascript">
    var CLIENT_STATUS_RE_CREDIT_WAITING = {$smarty.const.CLIENT_STATUS_RE_CREDIT_WAITING};
</script>
<!--#7427:End-->
<div ng-controller="DetailCtrl">
    <!--#6972:Start-->
    <div ui-i18n="{literal}{{lang}}{/literal}"></div>
    <!--#6972:End-->

    <table class="table-simple table-equal" style="width: 100%;">
        <tr>
            <th width="140">{lang('lbl_comment')}</th>
            <!--#7085:Start-->
<!--#7636:Start-->
            <!--<td colspan="3" style="white-space: pre-wrap">{literal}{{order.o_comment}}{/literal}</td>-->
			<td colspan="6" style="white-space: pre-wrap">{literal}{{order.o_comment}}{/literal}</td>
<!--#7636:End-->
            <!--#7085:End-->
        </tr>

        <tr>
<!--#7636:Start-->
			<!--
            <th>{lang('lbl_j_code')}</th>
            <td>{literal}{{order.o_j_code}}{/literal}</td>
            -->
            <th width="140">{lang('lbl_j_code')}</th>
            <td width="400">{literal}{{order.o_j_code}}{/literal}</td>
<!--#7636:End-->
            <th width="140">{lang('lbl_s_code')}</th>
<!--#7636:Start-->
            <!--<td>{literal}{{order.c_s_code}}{/literal}</td>-->
			<td colspan="4">{literal}{{order.c_s_code}}{/literal}</td>
<!--#7636:End-->
        </tr>

        <tr>
<!--#7636:Start-->
			<!--
            <th>{lang('lbl_name')}</th>
            -->
            <th width="140">{lang('lbl_name')}</th>
            <td>{literal}{{order.c_name}}{/literal}</td>
<!--#7636:End-->
<!--#7636:Start-->
			<!--
            <th>{lang('lbl_zipcode')}</th>
            <td>{literal}{{order.c_zipcode | limitTo:3:0 }}-{{order.c_zipcode | limitTo:4:3 }}{/literal}</td>
            -->
            <th width="140">{lang('lbl_zipcode')}</th>
            <td colspan="4">{literal}{{order.c_zipcode | limitTo:3:0 }}-{{order.c_zipcode | limitTo:4:3 }}{/literal}</td>
<!--#7636:End-->
        </tr>

        <tr>
<!--#7636:Start-->
            <!--<th>{lang('lbl_address')}</th>-->
			<th width="140">{lang('lbl_address')}</th>
<!--#7636:End-->
            <td>{literal}{{order.o_address_1st}}{/literal}</td>
<!--#7636:Start-->
            <!--<td colspan="2">{literal}{{order.o_address_2nd}}{/literal}</td>-->
            <td colspan="5">{literal}{{order.o_address_2nd}}{/literal}</td>
<!--#7636:End-->
        </tr>

        <tr>
<!--#7636:Start-->
            <!--<th>{lang('lbl_phone')}</th>-->
            <th width="140">{lang('lbl_phone')}</th>
<!--#7636:End-->
            <td>{literal}{{order.o_phone_no}}{/literal}</td>
<!--#7636:Start-->
			<!--
            <th>{lang('lbl_fax')}</th>
            <td>{literal}{{order.o_fax_no}}{/literal}</td>
            -->
            <th width="140">{lang('lbl_fax')}</th>
            <td colspan="4">{literal}{{order.o_fax_no}}{/literal}</td>
<!--#7636:End-->
        </tr>

        <tr>
<!--#7636:Start-->
            <!--<th>{lang('lbl_devision_name')}</th>-->
			<th width="140">{lang('lbl_devision_name')}</th>
<!--#7636:End-->
            <td>{literal}{{order.o_division_name}}{/literal}</td>
<!--#7636:Start-->
			<!--
            <th>{lang('lbl_charge_name')}</th>
            <td>{literal}{{order.o_charge_name}}{/literal}</td>
            -->
            <th width="140">{lang('lbl_charge_name')}</th>
            <td colspan="4">{literal}{{order.o_charge_name}}{/literal}</td>
<!--#7636:End-->
        </tr>

        <tr>
<!--#7636:Start-->
			<!--
            <th>{lang('lbl_payment_term')}</th>
            <td colspan="3">{literal}{{order.payment_terms}}{/literal}</td>
            -->
            <th width="140">{lang('lbl_payment_term')}</th>
            <td colspan="6">{literal}{{order.payment_terms}}{/literal}</td>
<!--#7636:End-->
        </tr>

        <tr>
<!--#7636:Start-->
			<!--
            <th>{lang('lbl_account_id')}</th>
            <td>{literal}{{order.a_name}}{/literal}</td>
            -->
            <th width="140">{lang('lbl_account_id')}</th>
            <td width="400">{literal}{{order.a_name}}{/literal}</td>

<!--#7636:End-->
<!--#7636:Start-->
			<!--
            <th>{lang('lbl_order_regist_date')}</th>
            <td>{literal}{{order.o_order_regist_date}}{/literal}</td>
            -->
            <th width="140">{lang('lbl_order_regist_date')}</th>
            <td width="100">{literal}{{order.o_order_regist_date}}{/literal}</td>
			<th width="140">{lang('lbl_order_date_approve')}</th>
            <td width="100">{literal}{{order.o_order_approval_date}}{/literal}</td>

<!--#7636:End-->
        </tr>
    </table>

    <div style="margin: 10px 0 0;">
        {lang('lbl_cost_total_price')}：{literal}{{order.o_cost_total_price | number:0}}{/literal}円   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {lang('lbl_consumption_tax')}：{literal}{{order.o_consumption_tax | number:0}}{/literal}円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {lang('lbl_tax_total')}：{literal}{{order.o_tax_total | number:0}}{/literal}円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {lang('lbl_gross_profit_amount')}：{literal}{{order.o_gross_profit_total | number:0}}{/literal}円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div style="margin: 10px 0;">
        <div ui-grid="gridOptionsDetail" ui-grid-pinning ui-grid-resize-columns ui-grid-edit ui-grid-row-edit ui-grid-cellnav ui-grid-selection class="grid grid-order"></div>
    </div>

    <table class="table-simple" style="width:100%;">
        <tr>
            <td width="140"><div class="min_label_length">{lang('lbl_remark')}</div></td>
            <td colspan="3"><div class="longtext_limit"><p style="white-space:pre-wrap;">{literal}{{order.o_remark}}{/literal}</p></div></td>
        </tr>
    </table>

    <div style="margin: 10px 0 0;">
        {if (isset($is_edit) && $is_edit) }
            <a href="/_admin/order/print_application?id={literal}{{order.o_id}}{/literal}" class="btn-small btn-green e-top" target="_blank" ng-show="is_show_print">{lang('lbl_print')}</a>
            <button type="button" ng-click="loadTemplate('edit', order.o_id, form='edit')" class="btn-small btn-green btn-edit" ng-show="(is_show_edit && isCanEdit) ? true : false">{lang('lbl_edit')}</button>
            <button type="button" ng-click="loadTemplate('edit', order.o_id, form='change_report')" class="btn-small btn-green" ng-show="is_show_change_report">{lang('lbl_change_report')}</button>
            <button type="button" ng-click="copy()" class="btn-small btn-green" ng-show="is_show_copy_order">{lang('lbl_copy_order')}</button>
            <button type="button" ng-click="approveReject()" class="btn-small btn-green" ng-show="(is_show_approve && isCanEdit) ? true : false">{lang('lbl_order_approve_reject')}</button>
            <button type="button" ng-click="changeReportApproveReject()" class="btn-small btn-green" ng-show="is_show_change_report_approve">{lang('lbl_order_change_report_approve_reject')}</button>
        {/if}
        <button type="button" ng-click="close()" class="btn-small btn-green btn-close">{lang('lbl_close')}</button>
    </div>

    <input type="hidden" name="dialog_approve_reject_title" value="{lang('approve_reject_form_title')}"/>
</div>
