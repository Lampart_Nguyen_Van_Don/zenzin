<script type="text/javascript">
    {*var products = {$products};*}
    var closest_month_closing_date = {$closest_month_closing_date};
    var closest_year_closing_date = {$closest_year_closing_date};
</script>
<style>
    #edit_grid select{
        height: 30px;
    }
    #edit_grid input{
        height: 30px;
    }
    #address_1st, #address_2nd {
        width: 350px;
    }
</style>
<div ng-controller="EditCtrl">
    {lang('lbl_search_condition')}
    <div ng-bind-html="error_success"></div>
    <table class="table-simple"  style="width: 100%;">
        <tr>
            <th nowrap>{lang('lbl_comment')}</th>
            <td ng-bind-html="order.o_comment | nl2br" colspan="3"></td>
        </tr>

        <tr>
            <th nowrap>{lang('lbl_j_code')}</th>
            <td>{literal}{{order.o_j_code}}{/literal}</td>

            <th nowrap>{lang('lbl_order_regist_date')}</th>
            <td>{literal}{{order.o_order_regist_date}}{/literal}</td>
        </tr>

        <tr>
            <th nowrap>{lang('lbl_order_form_input_date')}</th>
            <td colspan="3"><input type="text" ng-model="order.o_order_form_input_date" ng-blur="parse_date()" /></td>
        </tr>

        <tr>
            <th nowrap>{lang('lbl_s_code')}</th>
            <td>{literal}{{order.c_s_code}}{/literal}</td>

            <th nowrap>{lang('lbl_name')}</th>
            <td><div id="client_name">{literal}{{order.c_name}}{/literal}</div></td>
        </tr>

        <tr>
            <th nowrap>{lang('lbl_zipcode')}<span class="red"> ※</span></th>
            <td colspan="3">
                <input type="tel" name="zip_code" id="zip_code" ng-model="order.o_zipcode" maxlength="7" />
                <div class="small red" style="display: inline-block;">ハイフンは入力しないでください</div>
            </td>
        </tr>

        <tr>
            <th nowrap>{lang('lbl_address')}1<span class="red"> ※</span></th>
            <td><input type="text" name="address_1st" id="address_1st" ng-model="order.o_address_1st" /></td>

            <th>{lang('lbl_address')}2<span class="red"></span></th>
            <td><input type="text" name="address_2nd" id="address_2nd" ng-model="order.o_address_2nd" /></td>
        </tr>

        <tr>
            <th>TEL<span class="red"> ※</span></th>
            <td><input type="tel" name="tel" id="tel" ng-model="order.o_phone_no"></td>

            <th>FAX</th>
            <td>
                <input type="tel" name="fax" id="fax" ng-model="order.o_fax_no">
                <div class="small red" style="display: inline-block;">ハイフンを入力してください</div>
            </td>
        </tr>

        <tr>
            <th nowrap>{lang('lbl_devision_name')}</th>
            <td><input type="text" name="division_name" id="division_name" ng-model="order.o_division_name" /></td>

            <th nowrap>{lang('lbl_charge_name')}<span class="red"> ※</span></th>
            <td><input type="text" name="charge_name" id="charge_name" ng-model="order.o_charge_name" /></td>
        </tr>

        <tr>
            <th nowrap>{lang('lbl_payment_term')}</th>
            <td colspan="3">
                <div id="payment_terms" ng-show="order.o_is_ad_receive_payment=='0'">
                    {lang('lbl_closing_date')}： <span id="e_closing_date_name"></span>
                    {lang('lbl_payment_month_cd')}：<span id="e_payment_month_cd_name"></span>
                    {lang('lbl_payment_day_cd')}： <span id="e_payment_day_cd_name"></span>
                </div>
                <div id="payment_terms" ng-show="order.o_is_ad_receive_payment=='1'">
                    {lang('lbl_receive_payment')}
                </div>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <div class="left-title" ng-show="order.o_is_ad_receive_payment=='0'">{literal}与信限度額（{{order.credit_amount_show}}）{/literal}</div>
<!-- #9763:Start -->
                <div ng-show="order.o_is_ad_receive_payment=='0'">{literal}売掛金（{{order.c_debt}}）{/literal}</div>
<!-- #9763:End -->
                <div class="left-title" ng-show="order.o_is_ad_receive_payment=='1'">{lang('lbl_credit_infor_ad_receive')}</div>
            </td>

            <td colspan="2">
                <input type="checkbox" id="cbx_money_id" ng-change="setDeliveryDate()" ng-click="compute_credit()" ng-checked="order.o_is_ad_receive_payment" ng-model="order.o_is_ad_receive_payment" ng-disabled="order.c_is_ad_receive_payment==1" ng-true-value="1" ng-false-value="0">
                <label for="cbx_money_id">{lang('lbl_receive_payment')}</label>
                <div id="alert_ad_receive_payment" ng-show="order.o_is_ad_receive_payment_init==1"><span class="red small">前受金であること</span></div>
            </td>
        </tr>

        <tr>
            <th>{lang('lbl_account_id')}<span class="red"> ※</span></th>
            <td colspan="3">
                {if (count($j_accounts) == 1)}
                    <select ng-model="order.o_account_id" name="j_account" id='j_account' disabled>
                        {html_options values=$j_accounts[0]->id output=$j_accounts[0]->name}
                    </select>
                {else}
                    <select ng-model="order.o_account_id" name="j_account" id='j_account'>
                        <!-- <option value="0">＝＝すべて＝＝</option> -->
                        {foreach from=$j_accounts item=a}
                            {html_options values=$a->id output=$a->name}
                        {/foreach}
                    </select>
                {/if}
            </td>
        </tr>
    </table>

    <div style="margin: 10px 0 0;">
        {lang('lbl_cost_total_price')}：{literal}{{order.o_cost_total_price | number:0}}{/literal}円   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {lang('lbl_consumption_tax')}：{literal}{{order.o_consumption_tax | number:0}}{/literal}円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {lang('lbl_tax_total')}：{literal}{{order.o_tax_total | number:0}}{/literal}円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {lang('lbl_gross_profit_amount')}：{literal}{{order.o_gross_profit_total | number:0}}{/literal}円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div id="edit_grid" style="margin: 10px 0px 10px 0px; font-size:12px;">
        <div ui-grid="gridOptionsEdit" ui-grid-pinning ui-grid-resize-columns ui-grid-edit ui-grid-row-edit ui-grid-cellnav ui-grid-selection class="grid"></div>
    </div>

    <div ng-show="isShowAddButton">
        <button class="btn-small btn-green btn-add" ng-disabled="(is_credit_over || !isCanEdit) ?  true : false"  ng-class="(is_credit_over || !isCanEdit) ? 'btn-gray' : 'btn-green'" type="button" id="addData" ng-show="order.od_order_status==1" ng-click="addData()">{lang('lbl_add_branch')}</button>
    </div>

    <table class="table-simple" style="width:100%;">
        <tr>
            <td><div class="min_label_length">{lang('lbl_remark')}<span class="red"> ※</span></div></td>
            <td><textarea name="remark" id="remark" ng-model="order.o_remark" rows="7" cols="85%"></textarea></td>
        </tr>

        <tr ng-show="waiting_change_report">
            <td>{lang('lbl_comment_report')}<span class="red"> ※</span></td>
            <td><textarea name="comment" id="comment" ng-model="order.o_new_comment" rows="7" cols="85%"></textarea></td>
        </tr>
    </table>

    <br>

    <div ng-show="isShowEditButtonSection">
        <!-- <a href="/_admin/order/print_application?id={literal}{{order.o_id}}{/literal}" class="btn-small btn-green" target="_blank">{lang('lbl_print')}</a> -->
        <button type="button" ng-disabled="(is_submitted || is_credit_over || !isCanEdit) ?  true : false" ng-class="(is_submitted || is_credit_over || !isCanEdit) ? 'btn-gray' : 'btn-green'" ng-click="changeReport(false)" ng-class="is_submitted ? 'btn-gray' : 'btn-green'" class="btn-small btn-save">{lang('lbl_save')}</button>
        <button type="button" ng-click="back()" class="btn-small btn-green btn-close">{lang('lbl_close')}</button>
        <button type="button" ng-disabled="(is_submitted || !isCanEdit) ?  true : false" ng-click="deleteOrder()" ng-class="(is_credit_over || !isCanEdit) ? 'btn-gray' : 'btn-green'" class="btn-small btn-delete">{lang('lbl_delete_order')}</button>
    </div>

    <div ng-show="isShowChangeReportButtonSection">
        <!-- <a href="/_admin/order/print_application?id={literal}{{order.o_id}}{/literal}" class="btn-small btn-green" target="_blank">{lang('lbl_print')}</a> -->
        <button type="button" ng-disabled="is_submitted" ng-click="changeReport(true)" ng-class="is_submitted ? 'btn-gray' : 'btn-green'" class="btn-small">{lang('lbl_change_report')}</button>
        <button type="button" ng-click="back()" class="btn-small btn-green btn-close">{lang('lbl_close')}</button>
        <!-- <button type="button" ng-disabled="is_submitted" ng-click="cancelApplyOrder()" ng-class="is_submitted ? 'btn-gray' : 'btn-green'" class="btn-small">{lang('lbl_regist_cancel')}</button> -->
    </div>
<!--  <div style="color: red;" ng-bind-html="errors"></div> -->
</div>
