<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        @page {
            font-family: meriyo;
            font-size: 12px;
        }

        .header {
            text-align: center;
            width: 100%;
            font-size: 20px;
        }
        .title {
            background-color: #f1f1f1;
        }
        .left {
            width: 68%;
        }
        .right {
            padding-left: 40px;
            width: 28%;
        }
        table {
            width: 100%;
        }
        .row {
            width: 100%;
        }
    </style>
    </head>
    <body>
        <?php foreach ($order_details as $items):?>
            <div class="header">申　　込　　書</div>
            <table class="row">
                <tr>
                    <td class="left">
                        <table border="1">
                            <tr>
                                <td class="title">フ　リ　ガ　ナ</td>
                                <td><?php echo $order->c_name_kana;?></td>
                            </tr>
                            <tr>
                                <td class="title">社　名</td>
                                <td><?php echo $order->c_name;?></td>
                            </tr>
                            <tr>
                                <td class="title">フ　リ　ガ　ナ</td>
                                <td>〒</td>
                            </tr>
                            <tr>
                                <td class="title">所在地</td>
                                <td><?php echo $order->o_address;?></td>
                            </tr>
                            <tr>
                                <td class="title">連絡先</td>
                                <td>
                                    <div>TEL：　<?php echo $order->o_phone_no;?></div>
                                    <div>FAX：　<?php echo $order->o_fax_no;?></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="title">部署名</td>
                                <td><?php echo $order->o_division_name;?></td>
                            </tr>
                            <tr>
                                <td class="title">担当者</td>
                                <td>（役職 ：　　　　　　　　　　　）　　　　　　　　　　　　　       　　　　出力無　　      ㊞</td>
                            </tr>
                            <tr>
                                <td class="title">お支払い<br>条件</td>
                                <td>
                                <?php
                                if($order->o_is_ad_receive_payment == 1) {
                                    echo "前受金";
                                }
                                else {
                                    echo "締め日：" . $order->c_closing_date
                                        . "　支払月：" . $order->c_payment_month_cd
                                        . "　支払日：" . $order->c_payment_day_cd;
                                }
                                ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="right">
                        <div><?php echo $order->o_order_form_input_date;?></div>
                        <div>株式会社ゼンリンビズネクサス</div>
                        <div>〒101-0065</div>
                        <div>東京都千代田区西神田1-1-1</div>
                        <div>オフィス21ビル2階</div>
                        <div>FAX：03－3233－1732</div>
                        <div>DM事業部 TEL：03-5577-1990</div>
                        <div>DP事業部 TEL：03-5577-1991</div>
                        <table>
                            <tr>
                                <td class="title">弊社使用欄</td>
                            </tr>
                            <tr>
                                <td>Jコード: <?php echo $order->c_s_code;?></td>
                            </tr>
                            <tr>
                                <td>Sコード: <?php echo $order->o_j_code;?></td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td class="title">弊社使用欄</td>
                                <td class="title">承認</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td colspan="2">○ 申込内容</td></tr>
                <tr>
                    <td>
                        <table border="1">
                            <tr>
                                <td class="title">明細番号</td>
                                <td class="title">商　　品　　名</td>
                                <td class="title">内　　容</td>
                                <td class="title">納品日</td>
                                <td class="title">請求日</td>
                                <td class="title">支払日</td>
                                <td class="title">数　量</td>
                                <td class="title">税別単価</td>
                                <td class="title">税別合計</td>
                            </tr>
                            <?php
                                $total = 0;
                                $no = 0;
                            ?>
                            <?php foreach ($items as $item):?>
                                <?php $no++;?>
                                <tr>
                                    <td><?php echo sprintf("%02d", $no);?></td>
                                    <td><?php echo $item->od_branch_cd;?></td>
                                    <td><?php echo $item->od_contents;?></td>
                                    <td><?php echo $item->od_delivery_date;?></td>
                                    <td><?php echo $item->od_billing_date;?></td>
                                    <td><?php echo $item->od_payment_date;?></td>
                                    <td><?php echo $item->od_quantity;?></td>
                                    <td><?php echo $item->od_cost_unit_price;?></td>
                                    <td><?php echo $item->od_cost_total_price;?></td>
                                </tr>
                                <?php $total = $total + $item->od_cost_total_price * $item->od_quantity;?>
                            <?php endforeach;?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="title">合計</td>
                                <td><?php echo $total;?></td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border="1">
                            <tr>
                                <td>原価税別単価</td>
                                <td>原価税別合計</td>
                                <td>粗利合計</td>
                                <td>粗利率</td>
                            </tr>
                            <?php foreach ($items as $item):?>
                            <tr>
                                <td><?php echo $item->od_cost_unit_price;?></td>
                                <td><?php echo $item->od_cost_total_price;?></td>
                                <td><?php echo $item->od_gross_profit_amount;?></td>
                                <td><?php echo $item->od_gross_profit_rate;?></td>
                            </tr>
                            <?php endforeach;?>
                        </table>
                    </td>
                </tr>
            </table>
        <?php endforeach;?>
    </body>
</html>