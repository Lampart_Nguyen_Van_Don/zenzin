{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<link href="/public/admin/order/main.css" rel="stylesheet" media="all"/>
<!-- #7846:Start -->
<!-- <link rel="styleSheet" href="/public/admin/css/ui-grid-unstable.css"/> -->
<link rel="styleSheet" href="/public/admin/css/ui-grid-unstable-ver3.0.7.css"/>
<style id="shift_color_style"></style>
<script src="/public/admin/js/angular.js"></script>
<!-- <script src="/public/admin/js/angular-touch.js"></script> -->
<!-- <script src="/public/admin/js/angular-animate.js"></script> -->
<script src="/public/admin/js/angular-sanitize.min.js"></script>
<!-- <script src="/public/admin/js/ui-grid-unstable.js"></script> -->
<script src="/public/admin/js/ui-grid-unstable-ver3.0.7.js"></script>
<!-- #7846:End -->
<script type="text/javascript">
//#7058:Start
var global_css = [];
//#7058:End
var account_list = {$order_model->list_account_follow_bussiness_unit};
var search_result = {$search_result};
var my_account = {$order_model->my_account};
var can_approve = {$can_approve};
var can_edit = {$can_edit};
var can_invoices = '{$can_invoices}';
//#7066: Start
var is_sales_view = '{$is_sales_view}';
//#7066: End
var can_acceptance = {$can_acceptance};
var tax_division = {$tax_division};
var j_accounts = {json_encode($j_accounts)};
var j_accounts_ext = {json_encode($j_accounts_ext)};
var bu_id_user_logged = {$bu_id_user_logged};

var list_order_status = {json_encode($order_model->list_order_status)};
var can_invoice_issue = {$can_invoice_issue};
// Check order acceptance permission
// 発注・検収-発注検収力・変更
var order_acceptance_input_change = {$order_acceptance_input_change};
// 発注・検収-一覧・参照
var order_acceptance_view = {$order_acceptance_view};

var products = {$products};

var UNAPPROVED = 1;
var UNDETERMINED = 2;
var CONFIRMED = 3;
var CANCELLED = 4;
var REJECTED = 9;

var jsGlobals = [
    // 0
    lbl_branch_cd = '{lang("lbl_branch_cd")}',
    // 1
    lbl_order_status = '{lang("lbl_order_status")}',
    // 2
    lbl_change_report = '{lang("lbl_change_report")}',
    // 3
    lbl_product_name = '{lang("lbl_product_name")}',//3
    // 4
    lbl_detail_contents = '{lang("lbl_detail_contents")}',
    // 5
    lbl_delivery_date = '{lang("lbl_delivery_date")}',//5
    // 6
    lbl_billing_date = '{lang("lbl_billing_date")}',
    // 7
    lbl_payment_date = '{lang("lbl_payment_date")}',//7
    // 8
    lbl_quantity = '{lang("lbl_quantity")}',
    // 9
    lbl_tax_type = '{lang("lbl_tax_type")}',
    // 10
    lbl_unit_price = '{lang("lbl_unit_price")}',
    // 11
    lbl_amount = '{lang("lbl_amount")}',//11
    // 12
    lbl_order_detail_cost_unit_price = '{lang("lbl_order_detail_cost_unit_price")}',
    // 13
    lbl_order_detail_cost_total_price = '{lang("lbl_order_detail_cost_total_price")}',
    // 14
    lbl_gross_profit_amount = '{lang("lbl_gross_profit_amount")}',
    // 15
    lbl_order_detail_gross_profit_rate = '{lang("lbl_order_detail_gross_profit_rate")}',
    // 16
    lbl_j_code = '{lang("lbl_j_code")}',
    // 17
    lbl_s_code = '{lang("lbl_s_code")}',
    // 18
    lbl_account_id = '{lang("lbl_account_id")}',
    // 19
    lbl_is_ad_sales_slip_input = '{lang("lbl_is_ad_sales_slip_input")}',
    // 20
    lbl_is_ad_invoice_issue = '{lang("lbl_is_ad_invoice_issue")}',
    // 21
    lbl_is_order_input = '{lang("lbl_is_order_input")}',
    // 22
    lbl_is_acceptance_input = '{lang("lbl_is_acceptance_input")}',
    // 23
    lbl_is_sales_slip_input = '{lang("lbl_is_sales_slip_input")}',
    // 24
    lbl_is_invoice_issue = '{lang("lbl_is_invoice_issue")}',
    // 25
    lbl_name = '{lang("lbl_name")}',
    // 26
    lbl_content = '{lang("lbl_content")}',
    // 27
    lbl_action = '{lang("lbl_action")}',//27
    // 28
    lbl_change_report = '{lang("lbl_change_report")}',
    // 29
    lbl_copy_branch = '{lang("lbl_copy_branch")}',
    // 30
    lbl_delete_branch = '{lang("lbl_delete_branch")}',
    // 31
    lbl_cancel_branch = '{lang("lbl_cancel_branch")}',
    // 32
    lbl_show = '{lang("lbl_show")}',
    // 33
    lbl_check_approve_more = '{lang("lbl_check_approve_more")}',
    // 34
    lbl_cancel_branch_release = '{lang("lbl_cancel_branch_release")}',
    // 35
    edit_nothing_change_msg = '{lang("edit_nothing_change")}',
    // 36
    change_report_nothing_change_msg = '{lang("change_report_nothing_change")}'
];
</script>
<style id="shift_color_style"></style>
<style>
    .order-dialog .ui-widget-content{
        border: none !important;
    }
    .order-dialog .ui-dialog-buttonpane {
        text-align: center !important;
        padding: 0px !important;
    }

    .order-dialog .ui-dialog-buttonset {
        float: none !important;
    }

    .cell-template-data {
        width: 100%;
        height: 100%;
        line-height: 30px;
    }

    input.ng-scope {
        line-height: 30px !important;
    }

    select.ng-scope {
        height: 30px !important;
    }

    #dialog_error_check_change_report{
        display: none;
    }
/*
#8222:Start
*/
    .loadgif img{
	 width: 200px;
	        width: 315px;
	    background-color: white;
	    border: 6px solid #4493D0;
	}

	.both-blind{
	       position: fixed;
	    z-index: 99999;
	    width: 100%;
	    height: 100%;
	     display: none;
	}
/*
#8222:End
*/

/*#7846:Start*/
    .ui-grid-cell:focus {
        outline: none !important;
    }
/*#7846:End*/
</style>

<!--#8222:Start-->
<div class="loadgif both-blind"></div>
<!--#8222:End-->

<div class="cell content slide-right ">
    <div ng-controller="MainCtrl">
        <div ui-i18n="{literal}{{lang}}{/literal}">
        <h1 style="border-color: rgb(68, 147, 208);">{$title}</h1>
        <div class="search-container">
            {lang('lbl_search_condition')}
            {form_open('', 'id="searchform"')}
            <div style="display: inline-block">
                <table id="search_toggle" class="table-simple" style="width: 100%">
                    <tr>
                        <td id="search_toggle" style="padding-left: 10px; border-bottom: none">
                            <a href="javascript:void(0)">{lang('lbl_hide_search')}</a>
                        </td>
                    </tr>
                </table>

                <div id="search_body" style="display: inline-block">
                    <table class="table-simple">
                        <tr>
                            <th>{lang('lbl_order_status')}</th>
                            <td>
                                {$i = 0}
                                {foreach from=$order_model->list_order_status item=item}
                                    <div class="group-control" ng-init="order_temp = {}; order_temp.order_status_arr = []">
<!--#7644:Start-->
                                       <!--<input type="checkbox" id="order_status_{$i}" name="order_status[{$item->code}]" ng-model="order_temp.order_status_arr[{$item->code}]" ng-checked="{$item->code}==2" ng-init="order_temp.order_status_arr[2] = true"> <label for="order_status_{$i}">{$item->name}</label>-->
                                       <input type="checkbox" id="order_status_{$i}" name="order_status[{$item->code}]" ng-model="order_temp.order_status_arr[{$item->code}]" ng-checked="{$item->code}==1" ng-init="order_temp.order_status_arr[1] = true"> <label for="order_status_{$i}">{$item->name}</label>
<!--#7644:End-->
                                    </div>
                                    {$i = $i + 1}
                                {/foreach}
                            </td>
                        </tr>
                        <tr>
                            <th>{lang('lbl_status_detail')}</th>
                            <td>
                                <div class="group-control">
                                    <div class="group-control">
                                        {lang('lbl_is_ad_sale_slip_input')}
                                    </div>
                                    <div class="group-control">
                                        <input type="checkbox" id="is_ad_sales_slip_input_1" name="is_ad_sales_slip_input[]" value="0"  ng-model="order_temp.is_ad_sales_slip_input_arr[0]"> <label for="is_ad_sales_slip_input_1">未</label><br>
                                        <input type="checkbox" id="is_ad_sales_slip_input_2" name="is_ad_sales_slip_input[]" value="1" ng-model="order_temp.is_ad_sales_slip_input_arr[1]"> <label for="is_ad_sales_slip_input_2">済</label>
                                    </div>
                                </div>
                                <div class="group-control">
                                    <div class="group-control">
                                        {lang('lbl_is_ad_invoice_issue')}
                                    </div>
                                    <div class="group-control">
                                        <input type="checkbox" id="is_ad_invoice_issue_1" name="is_ad_invoice_issue[]" value="0" ng-model="order_temp.is_ad_invoice_issue_arr[0]"> <label for="is_ad_invoice_issue_1">未</label><br>
                                        <input type="checkbox" id="is_ad_invoice_issue_2" name="is_ad_invoice_issue[]" value="1" ng-model="order_temp.is_ad_invoice_issue_arr[1]"> <label for="is_ad_invoice_issue_2">済</label>
                                    </div>
                                </div>
                                <div class="group-control">
                                    <div class="group-control">
                                        {lang('lbl_is_order')}
                                    </div>
                                    <div class="group-control">
                                        <input type="checkbox" id="is_order_input_1" name="is_order_input[]" value="0" ng-model="order_temp.is_order_input_arr[0]"> <label for="is_order_input_1">未</label><br>
                                        <input type="checkbox" id="is_order_input_2" name="is_order_input[]" value="1" ng-model="order_temp.is_order_input_arr[1]"> <label for="is_order_input_2">済</label>
                                    </div>
                                </div>
                                <div class="group-control">
                                    <div class="group-control">
                                        {lang('lbl_is_acceptance')}
                                    </div>
                                    <div class="group-control">
                                        <input type="checkbox" id="is_acceptance_input_1" name="is_acceptance_input[]" value="0" ng-model="order_temp.is_acceptance_input_arr[0]"> <label for="is_acceptance_input_1">未</label><br>
                                        <input type="checkbox" id="is_acceptance_input_2" name="is_acceptance_input[]" value="1" ng-model="order_temp.is_acceptance_input_arr[1]"> <label for="is_acceptance_input_2">済</label>
                                    </div>
                                </div>
                                <div class="group-control">
                                    <div class="group-control">
                                        {lang('lbl_is_sales_slip')}
                                    </div>
                                    <div class="group-control">
                                        <input type="checkbox" id="is_sales_slip_input_1" name="is_sales_slip_input[]" value="0" ng-model="order_temp.is_sales_slip_input_arr[0]"> <label for="is_sales_slip_input_1">未</label><br>
                                        <input type="checkbox" id="is_sales_slip_input_2" name="is_sales_slip_input[]" value="1" ng-model="order_temp.is_sales_slip_input_arr[1]"> <label for="is_sales_slip_input_2">済</label>
                                    </div>
                                </div>
                                <div class="group-control">
                                    <div class="group-control">
                                        {lang('lbl_is_invoice_issue')}
                                    </div>
                                    <div class="group-control">
                                        <input type="checkbox" id="is_invoice_issue_1" name="is_invoice_issue[]" value="0" ng-model="order_temp.is_invoice_issue_arr[0]"> <label for="is_invoice_issue_1">未</label><br>
                                        <input type="checkbox" id="is_invoice_issue_2" name="is_invoice_issue[]" value="1" ng-model="order_temp.is_invoice_issue_arr[1]"> <label for="is_invoice_issue_2">済</label>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>{lang('lbl_is_ad_receive_payment')}</th>
                            <td>
                                <input type="checkbox" id="is_ad_receive_payment" name="is_ad_receive_payment" value="1" ng-model="order_temp.is_ad_receive_payment"> <label for="is_ad_receive_payment">{lang('lbl_is_ad_receive_payment')}</label>
                            </td>
                        </tr>
                        <tr>
                            <th>{lang('lbl_is_change_report_apply')}</th>
                            <td>
                                <input type="checkbox" id="is_change_report_apply" name="is_change_report_apply" value="1" ng-model="order_temp.is_change_report_apply"> <label for="is_change_report_apply">{lang('lbl_is_change_report_apply')}</label>
                            </td>
                        </tr>
                        <tr>
                            <th>{lang('lbl_account_id')}</th>
                            <td>
                                <div class="group-control">
                                    {lang('lbl_j_business_unit')}
                                    <select ng-model="order.j_business_unit" id='j_business_unit' name='j_business_unit' class="max-width">
                                        <option value="0">＝＝すべて＝＝</option>
                                        {foreach from=$list_business_unit item=b}
                                            {html_options values=$b['id'] output=$b['name']}
                                        {/foreach}
                                    </select>
                                </div>
                                <div class="group-control">
                                    {lang('lbl_account_id')}
                                    <select id='j_account' name='j_account' class="max-width">
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>{lang('lbl_client_name')}</th>
                            <td>
                                <input type="text" ng-model="order.client_name" name="client_name">
                                <div class="legend">(あいまい検索）</div>
                            </td>
                        </tr>
                        <tr>
                            <th>{lang('lbl_j_code_branch_code')}</th>
                            <td>
                                <input type="text" ng-model="order.j_code_branch_cd_from" name="j_code_branch_cd_from">~
                                <input type="text" ng-model="order.j_code_branch_cd_to" name="j_code_branch_cd_to">
                                <div class="legend">(Fromのみ指定の場合、前方一致検索　From・To指定で前方一致の範囲指定検索）</div>
                                <span class="validation-error" id="j_code_branch_cd_1" style="display: none"></span>
                                <span class="validation-error" id="j_code_branch_cd_2" style="display: none"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>{lang('lbl_delivery_date')}</th>
                            <td>
                                <input type="text" ng-model="order.delivery_date_from" name="delivery_date_from">~
                                <input type="text" ng-model="order.delivery_date_to" name="delivery_date_to">
                                <div class="legend">(Fromのみ指定の場合、指定日のみ検索　From・To指定で範囲指定検索　年月or年月日指定）</div>
                                <span class="validation-error" id="delivery_date_from" style="display: none"></span>
                                <span class="validation-error" id="delivery_date_to" style="display: none"></span>
                                <span class="validation-error" id="delivery_date" style="display: none"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>{lang('lbl_order_regist_date')}</th>
                            <td>
                                <input type="text" ng-model="order.order_regist_date_from" name="order_regist_date_from">~
                                <input type="text" ng-model="order.order_regist_date_to" name="order_regist_date_to">
                                <div class="legend">(Fromのみ指定の場合、指定日のみ検索　From・To指定で範囲指定検索　年月or年月日指定）</div>
                                <span class="validation-error" id="order_regist_date_from" style="display: none"></span>
                                <span class="validation-error" id="order_regist_date_to" style="display: none"></span>
                                <span class="validation-error" id="order_regist_date" style="display: none"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>{lang('lbl_order_date_approve')}</th>
                            <td>
                                <input type="text" ng-model="order.approval_date_from" name="approval_date_from">~
                                <input type="text" ng-model="order.approval_date_to" name="approval_date_to">
                                <div class="legend">(Fromのみ指定の場合、指定日のみ検索　From・To指定で範囲指定検索　年月or年月日指定）</div>
                                <span class="validation-error" id="approval_date_from" style="display: none"></span>
                                <span class="validation-error" id="approval_date_to" style="display: none"></span>
                                <span class="validation-error" id="approval_date" style="display: none"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>{lang('lbl_payment_day_cd')}</th>
                            <td>
                                <input type="text" ng-model="order.payment_date_from" name="payment_date_from">~
                                <input type="text" ng-model="order.payment_date_to" name="payment_date_to">
                                <div class="legend">(Fromのみ指定の場合、指定日のみ検索　From・To指定で範囲指定検索　年月or年月日指定）</div>
                                <span class="validation-error" id="payment_date_from" style="display: none"></span>
                                <span class="validation-error" id="payment_date_to" style="display: none"></span>
                                <span class="validation-error" id="payment_date" style="display: none"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>{lang('lbl_s_code')}</th>
                            <td>
                                <input type="text" ng-model="order.s_code" name="s_code">
                                <div class="legend">(前方一致検索）</div>
                            </td>
                        </tr>
                        <tr>
                            <th>{lang('lbl_client_name_furigana')}</th>
                            <td>
                                <input type="text" ng-model="order.client_name_furigana" name="client_name_furigana">
                                <div class="legend">(あいまい検索）</div>
                            </td>
                        </tr>
                        <tr>
                            <th>{lang('lbl_client_department_name')}</th>
                            <td>
                                <input type="text" ng-model="order.client_department_name" name="client_department_name">
                                <div class="legend">(あいまい検索）</div>
                            </td>
                        </tr>
                         <tr>
                            <th>{lang('lbl_s_business_unit')}</th>
                            <td>
                                <div class="group-control">
                                    {lang('lbl_j_business_unit')}
                                    <select ng-model="order.s_business_unit" id='s_business_unit' name='s_business_unit' class="m_width">
                                        <option value="0">＝＝すべて＝＝</option>
                                        {foreach from=$list_business_unit item=b}
                                            {html_options values=$b['id'] output=$b['name']}
                                        {/foreach}
                                    </select>
                                </div>
                                <div class="group-control">
                                    {lang('lbl_s_business_unit')}
                                    <select id='s_account' name='s_account' class="m_width">
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="a-center"><button type="button" class="btn-small btn-green btn-search" ng-click="search()">{lang('lbl_search')}</button></td>
                        </tr>
                    </table>
                    </div>
                </div>
                 <hr class="blank-10px" />
            {form_close()}
        </div>

        <hr class="blank-10px" />
        <h1 style="border-color: rgb(68, 147, 208);"></h1>
        <hr class="blank-10px" />

        {if $can_add}
           <button type="button" ng-click="setTaxRate();loadTemplate('add');" class="btn-small btn-green btn-add txt-center-main">{lang('lbl_add_order')}</button>
        {/if}
        {if $can_approve=='true'}
           <button type="button" ng-click="approve()" ng-disabled="(is_submitted) ? true : false" ng-class="(is_submitted) ? 'btn-gray' : 'btn-green'" class="btn-small btn-green txt-center-main">{lang('lbl_approve_order')}</button>
        {/if}
        {if $can_export}
           <button type="button" class="btn-small btn-green txt-center-main export_order">{lang('lbl_csv_download')}</button>
        {/if}
        <br>
        <div style="margin: 10px 0 0 0"></div>
        <div id="form_error_msg"></div>
        <div style="width: 100%">
            <!-- <div class="red" ng-bind-html="errors"></div>
            <button class="btn-small btn-green" type="button" ng-click="save()">{lang('common_lbl_save')}</button>
            <button class="btn-small btn-green" type="button" id="removeRows" ng-click="removeRows()">{lang('common_lbl_delete')}</button>
            <button class="btn-small btn-green" type="button" id="addData" ng-click="addData()">{lang('common_lbl_add')}</button>
            <br>
            <br> -->
<!-- #7589:Start -->
<!--             <div ui-grid="gridOptions" ui-grid-pinning ui-grid-resize-columns ui-grid-edit ui-grid-row-edit ui-grid-cellnav class="order-grid"> -->
            <div ui-grid="gridOptions" ui-grid-pinning ui-grid-resize-columns ui-grid-edit ui-grid-row-edit ui-grid-cellnav class="order-grid grid-viewport-height">
<!-- #7589:End -->
                <div class="watermark" ng-class="{literal}{'show-watermark' : showWatermark}{/literal}">{lang('common_lbl_no_data')}</div>
                <div class="watermark" ng-class="{literal}{'show-watermark' : is_over_record}{/literal}">{lang('common_msg_over_record')}</div>
            </div>
            <div id="dialogAdd">
            {*{include file="$add.tpl"}*}
            </div>
            <div id="dialog">
            {*{include file="$detail.tpl"}*}
            </div>
            <div id="dialogEdit">
            {*{include file="$edit.tpl"}*}
            </div>
            <div id="dialog_approve_reject">
            {*{include file="$approve_reject.tpl"}*}
            </div>
            <div id="dialog_change_report">
            {*{include file="$change_report.tpl"}*}
            </div>
            <!--  #6901 -Add -S -->
            <div id="dialog_error_check_change_report">{lang('error-change-report-order-detail')}</div>
           <!--  #6901 -Add -E -->
            <div id="clientDialog"></div>
            <div id="dialog-errors" style="display:none"></div>
        </div>
<!-- #7577:Start -->
        {if $can_add}
           <button type="button" ng-click="setTaxRate();loadTemplate('add');" class="btn-small btn-green btn-add txt-center-main">{lang('lbl_add_order')}</button>
        {/if}
        {if $can_approve=='true'}
<!-- #8222:Start -->
           <button type="button" ng-click="approve()" ng-disabled="(is_submitted) ? true : false" ng-class="(is_submitted) ? 'btn-gray' : 'btn-green'"  class="btn-small btn-green txt-center-main">{lang('lbl_approve_order')}</button>
<!-- #8222:End -->
        {/if}
        {if $can_export}
           <button type="button" class="btn-small btn-green txt-center-main export_order">{lang('lbl_csv_download')}</button>
        {/if}
<!-- #7577:End -->
    </div>
</div>
<div id="message_alert"></div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#searchform :input").change(function() {
            $(window).unbind('beforeunload');
        });
    });

    $("#dialog").dialog({
        autoOpen : false,
        width : "auto",
        height : "auto",
        modal : true,
        buttons : {
        },
        close : function() {
        }
    });

    $("#dialogAdd").dialog({
        autoOpen : false,
        width : "auto",
        height : "auto",
        modal : true,
        buttons : {
        },
        beforeClose: function( event, ui ) {
            return close_confirm();
        },
        close : function() {
            if ($("#dialog-errors").hasClass('ui-dialog-content')) {
                $("#dialog-errors").dialog('destroy');
            }
        }
    });

    $("#dialogEdit").dialog({
        autoOpen : false,
        width : "auto",
        height : "auto",
        modal : true,
        buttons : {
        },
        beforeClose: function( event, ui ) {
            return close_confirm();
        },
        close : function() {
            if ($("#dialog-errors").hasClass('ui-dialog-content')) {
                $("#dialog-errors").dialog('destroy');
            }
        }
    });

    $("#dialog_approve_reject").dialog({
        autoOpen : false,
        width : "auto",
        height : "auto",
        modal : true,
        buttons : {
        },
        beforeClose: function( event, ui ) {
            return close_confirm();
        },
        close : function() {
        }
    });

    $("#dialog_change_report").dialog({
        autoOpen : false,
        width : "auto",
        height : "auto",
        modal : true,
        buttons : {
        },
        beforeClose: function( event, ui ) {
            return close_confirm();
        },
        close : function() {
        }
    });

    clientDialog = $('#clientDialog');
    clientDialog.dialog({
        autoOpen : false,
        title: '{lang('title_client_detail')}',
        height: 'auto',
        width: 'auto',
        modal: true,
        beforeClose: function( event, ui ) {
            return close_confirm();
        },
        close : function() {
            form_changed = false;
            if (typeof cl_functions_loaded !== 'undefined' && cl_functions_loaded) {
                unbind_save_key();
            }
        }
    });

    lbl_open_search = '{lang('lbl_open_search')}';
    lbl_hide_search = '{lang('lbl_hide_search')}';

    if (loadData('cl_showSearchOrder') == 0) {
        hideSearch();
    } else {
        showSearch();
    }

    $('#search_toggle').click(function () {
        if ($('#search_body').is(':visible')) {
            hideSearch(true);
        } else {
            showSearch(true);
        }
    });

    exit_confirm();
</script>

<script src="/public/admin/order/main.js"></script>
<script src="/public/admin/order/add.js"></script>
<script src="/public/admin/order/detail.js"></script>
<script src="/public/admin/order/edit.js"></script>
<script src="/public/admin/order/approve_reject.js"></script>
<script src="/public/admin/order/change_repo.js"></script>

{include file = "inc/footer.tpl"}
