{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<!-- #7846:Start -->
<link rel="stylesheet" href="/public/admin/css/ui-grid-unstable-ver3.0.7.css">
<script type="text/javascript" src="/public/admin/js/angular.js"></script>
<script type="text/javascript" src="/public/admin/js/angular-sanitize.min.js"></script>
<script type="text/javascript" src="/public/admin/js/ui-grid-unstable-ver3.0.7.js"></script>
<script type="text/javascript" src="/public/admin/order_acceptance/app_mgmt_dp.js"></script>
<script type="text/javascript" src="/public/admin/order/detail.js"></script>
<script type="text/javascript" src="/public/admin/client/function.js"></script>
<!-- #7846:End -->
<script type="text/javascript">
var glb_account_options = {$account_options};
var glb_direct_subcontractor_options = {$direct_subcontractor_options};
var glb_indirect_subcontractor_options = {$indirect_subcontractor_options};
var glb_shipping_type_options = {$shipping_type_options};
var glb_shipments_shape_options = {$shipments_shape_options};
var glb_consumption_tax = {$consumption_tax};
//#7167:Start
var glb_arrange_rep_options = {$account_options};
//#7167:End


</script>

<!-- #7167:Start -->
{literal}
<script type="text/javascript">
glb_arrange_rep_options.splice(0, 0, {
    id: 0,
    name: '',
});

glb_shipping_type_options.splice(0, 0, {
    code: 0,
    create_datetime: null,
    disable: 0,
    id: 0,
    is_advance_payment: null,
    is_no_add: 0,
    is_remaining_confirm: null,
    is_reporting: null,
    lastup_account_id: null,
    lastup_datetime: null,
    name: '',
    sequence: null,
});

glb_indirect_subcontractor_options.splice(0, 0, {
    abbr_name: '',
    code: 0,
    direct_consignment: '',
    id: 0,
    is_no_add: 0,
    name: ''
});

glb_shipments_shape_options.splice(0, 0, {
    code: 0,
    id: 0,
    name: ''
});
</script>
{/literal}
<!-- #7167:Start -->

<style type="text/css">
/*#7167:Start*/
.order-acceptance-grid .ui-grid-cell select option:first-child[value="?"],
.order-acceptance-grid .ui-grid-cell select option:first-child:not([value]) {
    display: none !important;
}
/*#7167:End*/

.loadgif{
    position: fixed;
    z-index: 99999;
    top: 59px;
    right: -1%;
    display: none;

}

.loadgif img{
 width: 200px;
        width: 315px;
    background-color: white;
    border: 6px solid #4493D0;
}

.both-blind{
       position: fixed;
    z-index: 99999;
    width: 100%;
    height: 100%;
     display: none;
}

 /* #6784 - Modify -S */
 /*#7846:Start*/
/*.ui-grid-cell-focus{
    border: 2px solid #5292f7;
    height: 26px;
    margin-right: 0px;
    margin-left: 2px;
}*/
.ui-grid-cell-focus{
    border: 2px solid #5292f7;
    height: inherit;
    margin-right: 0px;
}

.center-checkbox-wrapper.ui-grid-cell-focus,
.action-btn-wrapper.ui-grid-cell-focus{
    height: 26px;
    margin-left: 2px;
}
/*#7846:End*/
 /* #6784 - Modify -E */
</style>
<div class="loadgif both-blind"></div>
<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>
        <div>
            {form_open('', ['id' => 'search-form'])}
                <table class="table-simple table-input-order-acceptance" border="1">
                    <tr>
                        <th>{lang('lbl_approve_status')}</th>
                        <td>
                            <label class="checkbox-label">
                                <input type="checkbox" name="order_acceptance_status[]" value="1" {if (isset($form.order_acceptance_status) && in_array(1, $form.order_acceptance_status)) || (isset($search_status) && in_array(1, $search_status))}{'checked'}{elseif !isset($form.order_acceptance_status) && !isset($search_status)}{'checked'}{/if} /> {lang('checkbox_unapproved')}
                            </label>
                            <label class="checkbox-label">
                                <input type="checkbox" name="order_acceptance_status[]" value="2" {if (isset($form.order_acceptance_status) && in_array(2, $form.order_acceptance_status)) || (isset($search_status) && in_array(2, $search_status))}{'checked'}{elseif !isset($form.order_acceptance_status) && !isset($search_status)}{'checked'}{/if} /> {lang('checkbox_approved')}
                            </label>
                            <label class="checkbox-label">
                                <input type="checkbox" name="order_acceptance_status[]" value="3" {if (isset($form.order_acceptance_status) && in_array(3, $form.order_acceptance_status)) || (isset($post_text_is_acceptance_input) && ($check_input_closing_date == true))}{'checked'}{/if} /> {lang('checkbox_account_tighten')}
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_jcode_branch_cd')}</th>
                        <td>
<!-- #7067:Start -->
                             <input type="text" name="from_jcode_branch" value="{if isset($form_jcode_brand)}{$form_jcode_brand}{/if}" /> ～
                            <input type="text" name="to_jcode_branch" value="{if isset($to_jcode_branch)}{$to_jcode_branch}{/if}" />
<!-- #7067:End -->
                            <p class="order-acceptance-tooltip">{lang('lbl_jcode_branch_tooltip')}</p>
                            <span class="validation-error" id="jcode_branch_error" style="display: none"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_account_id')}</th>
                        <td>
                            {lang('lbl_business_unit_name')}
                                                        <select id="cbx-business-unit" name="business_unit_id" class="max-width">
                                {if count($business_units) != 1}
                                <option value="-1">{lang('common_lbl_all')}</option>
                                {/if}
<!-- #9695:Start -->
                                {$flag = false}
<!-- #9695:End -->
                                {foreach $business_units as $business_unit}
<!--                                 <option value="{html_escape($business_unit.id)}" {if isset($form.business_unit_id) && $business_unit.id == $form.business_unit_id}{'selected'}{/if}>
                                {html_escape($business_unit.name)}
                                </option> -->
<!-- #9695:Start -->
                                <option value="{html_escape($business_unit.id)}" {if isset($form.business_unit_id) && $business_unit.id == $form.business_unit_id}{'selected'}{elseif !$flag}{'selected'}{/if}>
                                {html_escape($business_unit.name)}
                                </option>
                                {$flag = true}
<!-- #9695:End -->
                                {/foreach}
                            </select>

                            {lang('lbl_account_id')}
                            <select style="min-width: 150px;" id="cbx-account" name="account_id" class="max-width">
                                <option value="-1">{lang('common_lbl_all')}</option>
                                {foreach $accounts as $account}
                                <option value="{html_escape($account.id)}" {if isset($form.account_id) && $account.id == $form.account_id}{'selected'}{/if}>{html_escape($account.name)}</option>
                                {/foreach}
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_client_name')}</th>
                        <td>
                            <input type="text" name="client_name" value="{if isset($form.client_name)}{$form.client_name}{/if}" />
                            <p class="order-acceptance-tooltip">{lang('lbl_hidden_search')}</p>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_source_subcontractor_id')}</th>
                        <td>
                            <input type="text" name="source_subcontractor_name" value="{if isset($form.source_subcontractor_name)}{$form.source_subcontractor_name}{/if}" />
                            <p class="order-acceptance-tooltip">{lang('lbl_hidden_search')}</p>
                        </td>
                    </tr>



                    <tr>
                        <th>{lang('lbl_delivery_finish_date')}</th>
                        <td>
                            <input type="text" name="from_delivery_date" value="{if isset($form.from_delivery_date)}{$form.from_delivery_date}{/if}" /> ～
                            <input type="text" name="to_delivery_date" value="{if isset($form.to_delivery_date)}{$form.to_delivery_date}{/if}" />
                            <p class="order-acceptance-tooltip">{lang('lbl_delivery_date_tooltip')}</p>
                            <span class="validation-error" id="delivery_date_error" style="display: none"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_summary_month')}</th>
                        <td>
                            <input type="text" name="from_summary_month" value="{if isset($form.from_summary_month)}{$form.from_summary_month}{/if}" /> ～
                            <input type="text" name="to_summary_month" value="{if isset($form.to_summary_month)}{$form.to_summary_month}{/if}" />
                            <p class="order-acceptance-tooltip">{lang('lbl_summary_month_tooltip')}</p>
                            <span class="validation-error" id="summary_month_error" style="display: none"></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="a-center">
                                <button type="button" ng-click="search()" class="btn-small btn-green btn-search m-height">{lang('common_lbl_search')} </button>
                                <input type="hidden" value="{$postFormOrder}" name="postFormOrder" id="postFormOrder">
                                <input type="hidden" value="{$jcodeidPOst}" name="jcodeidPOst" id="jcodeidPOst">
                                <input type="hidden" value="{$brandcode}" name="brandcode" id="brandcode">
                        </td>
                    </tr>
                </table>
                <br class="br-10px" />
            {form_close()}
        </div>

        <!-- GRID -->
        <div ng-controller="MainCtrl" class="order-acceptance-grid-wrapper div-bt-action-group">
            <div ui-i18n="{literal}{{lang}}{/literal}">
            {if $this->auth->has_permission('acceptance_input_change', 'column')}
            <button class="btn-small btn-green btn-save m-height" ng-click="save()">{lang('common_lbl_save')}</button>
            {/if}
            <button class="btn-small btn-green m-height" ng-click="export()">{lang('btn_export_csv')}</button>
             {if $this->auth->has_permission('acceptance_approve', 'column')}
            <button type="button" ng-click="checkalllist()" class="btn-small btn-green m-height">{lang('btn-check-all-click')}</button>
             {/if}
            {if $this->auth->has_permission('acceptance_approve', 'column')}
            <!--#6946:Start !-->
            <button class="btn-small btn-green m-height" ng-click="approved_all()">{lang('btn_approve_all_acceptance')}</button>
             <!--#6946:End !-->
            <button class="btn-small btn-green m-height" ng-click="cancel_acceptance()">{lang('btn_cancel_all_acceptance')}</button>
            {/if}



            <br class="br-10px clear"  />
            <div class="messages" style="margin-top:10px;" ng-bind-html="error_messages"></div>
            <br class="br-10px" />
             <div  class="loadgif"><img src="/public/admin/img/processing_approvel.gif" alt="" /></div>
             <!-- #6784-S -->
<!-- #7589:Start -->
<!--             <div class="order-acceptance-grid" class="gridStyle" ui-grid="gridOptions" external-scopes="states" ui-grid-pinning ui-grid-resize-columns ui-grid-pagination ui-grid-edit ui-grid-row-edit ui-grid-exporter ui-grid-cellnav> -->
<!-- #7846:Start -->
<!--             <div class="order-acceptance-grid grid-viewport-height" class="gridStyle" ui-grid="gridOptions" external-scopes="states" ui-grid-pinning ui-grid-resize-columns ui-grid-pagination ui-grid-edit ui-grid-row-edit ui-grid-exporter ui-grid-cellnav> -->
            <div class="order-acceptance-grid grid-viewport-height" class="gridStyle" ui-grid="gridOptions" external-scopes="states" ui-grid-pinning ui-grid-resize-columns ui-grid-edit ui-grid-row-edit ui-grid-cellnav>
<!-- #7846:End -->
<!-- #7589:End -->
            <!-- #6784-E -->
                <div class="watermark" ng-class="{literal}{'show-watermark' : showWatermark}{/literal}">{lang('lbl_no_data_available')}</div>
                <!-- #7087-Start -->
                <div class="watermark" ng-class="{literal}{'show-watermark' : is_over_record}{/literal}">{lang('common_msg_over_record')}</div>
                <!-- #7087-End -->
            </div>

            <div id="dialog">
            {include file="{$smarty.const.APPPATH}modules/_admin/views/order/detail.tpl"}
            </div>

            {if $this->auth->has_permission('acceptance_input_change', 'column')}
            <button class="btn-small btn-green btn-save m-height" ng-click="save()">{lang('common_lbl_save')}</button>
            {/if}
            <button class="btn-small btn-green m-height" ng-click="export()">{lang('btn_export_csv')}</button>
            {if $this->auth->has_permission('acceptance_approve', 'column')}
            <button type="button" ng-click="checkalllist()" class="btn-small btn-green m-height">{lang('btn-check-all-click')}</button>
             {/if}
            {if $this->auth->has_permission('acceptance_approve', 'column')}
             <!--#6946:Start !-->
            <button class="btn-small btn-green m-height" ng-click="approved_all()">{lang('btn_approve_all_acceptance')}</button>
            <!--#6946:End !-->
            <button class="btn-small btn-green m-height" ng-click="cancel_acceptance()">{lang('btn_cancel_all_acceptance')}</button>

            {/if}


        </div>
        <!-- END GRID -->
    </div>

</div>
<div id="confirm-delete" style="display:none">{lang('lbl_confirm_delete')}</div>
<!-- #7461: Start !-->
<!--  <div id="dialog-show-client" style="display:none">{lang('lbl_confirm_delete')}</div> -->
<!-- #7461: End !-->
<div id="dialog-messages" style="display:none"></div>
<div id="dialog-errors" style="display:none"></div>
<div id="search_result" style="display:none">{if isset($search_results)}{$search_results}{/if}</div>

<!-- #7461: Start !-->
 <div id="clientDialog"></div>
<!-- #7461: End !-->

<script>

//#7461:Start
    clientDialog = $('#clientDialog');
    clientDialog.dialog({
        autoOpen : false,
        title: '{lang('title_client_detail')}',
        height: 'auto',
        width: 'auto',
        modal: true,
        beforeClose: function( event, ui ) {
            return close_confirm();
        },
        close : function() {
            form_changed = false;
            if (typeof cl_functions_loaded !== 'undefined' && cl_functions_loaded) {
                unbind_save_key();
            }
        }
    });

//#7461:End

    oa_can_modify  = {if $can_modify}true{else}false{/if};
    oa_can_approve = {if $can_approve}true{else}false{/if};
    post_is_acceptance_input = {$post_text_is_acceptance_input};
//#7042:Start
    is_auth_change = {if $this->auth->has_permission('acceptance_input_change', 'column')}true{else}false{/if};
//#7042:End
    select_all = "{lang('common_lbl_all')}";
    label = {
        approve_all: "{lang('lbl_approve_all')}",
        action: "{lang('lbl_action')}",
        edit: "{lang('lbl_edit_acceptance')}",
        order_acceptance_status: "{lang('lbl_order_acceptance_status')}",
        jcode: "{lang('lbl_jcode')}",
        branch: "{lang('lbl_branch')}",
        account_id: "{lang('lbl_account_id')}",
        arrange_rep: "{lang('lbl_arrange_rep')}",
        client_name: "{lang('lbl_client_name')}",
        product_name:"{lang('lbl_product_name')}",
        content_order:"{lang('lbl_content_order')}",
        source_subcontractor_id: "{lang('lbl_source_subcontractor_id')}",
        unit_price_in_order:"{lang('lbl_unit_price_in_order')}",
        quanity_in_order:"{lang('lbl_quanity_in_order')}",
        sales_in_order:"{lang('lbl_total_sales_in_order')}",
        total_cost_in_order_excluding_tax:"{lang('lbl_total_cost_in_order_excluding_tax')}",
        delivery_date: "{lang('lbl_delivery_finish_date')}",
        summary_month: "{lang('lbl_summary_month')}",
        amount: "{lang('lbl_fixed_cost_excluding_tax')}",
        export_csv_file_name: "{lang('export_csv_file_name')}",
        unapproved: "{lang('checkbox_unapproved')}",
        approved: "{lang('checkbox_approved')}",
        account_tighten: "{lang('checkbox_account_tighten')}",
        confirm_close_windows: "{lang('lbl_confirm_close_windows')}",
        update_success: "{lang('lbl_edit_success')}",
        nothing_change: "{lang('lbl_nothing_change')}",
        please_select_unapprove: "{lang('lbl_please_select_unapprove')}",
        please_select_approve: "{lang('lbl_please_select_approve')}",
        row: "{lang('lbl_row')}"
    };

    btn = {
        add: "{lang('common_btn_add')}",
        separate: "{lang('btn_separate')}",
        delete: "{lang('common_lbl_delete')}",
        undo: "{lang('common_lbl_undo')}",
    };

    order_acceptance_status_val = {
        unapproved: {$smarty.const.ORDER_ACCEPTANCE_UNAPPROVED},
        approved: {$smarty.const.ORDER_ACCEPTANCE_APPROVED},
        account_tighten: {$smarty.const.ORDER_ACCEPTANCE_ACCOUNT_TIGHTEN}
    };
    var closingdateend = '{$closingdatefinal}';
    var can_edit = false;
    var can_approve = false;
    var can_invoices = '{$can_invoices}';
    var can_acceptance = true;
    var can_invoice_issue = true;
    var j_accounts = {json_encode($j_accounts)};
    var list_order_status = {json_encode($order_model->list_order_status)};
    var my_account = {$order_model->my_account};
    var tax_division = {$tax_division};
    var UNAPPROVED = 1;
    var UNDETERMINED = 2;
    var CONFIRMED = 3;
    var CANCELLED = 4;
    var REJECTED = 9;
    // Check order acceptance permission
    // 発注・検収-発注検収力・変更
    var order_acceptance_input_change = {$order_acceptance_input_change};
    // 発注・検収-一覧・参照
    var order_acceptance_view = {$order_acceptance_view};


    var jsGlobals = [
        lbl_branch_cd = '{lang("lbl_branch_cd")}',
        lbl_order_status = '{lang("lbl_order_status")}',
        lbl_change_report = '{lang("lbl_change_report")}',
        lbl_product_name = '{lang("lbl_product_name")}',//3
        lbl_detail_contents = '{lang("lbl_detail_contents")}',
        lbl_delivery_date = '{lang("lbl_delivery_date")}',//5
        lbl_billing_date = '{lang("lbl_billing_date")}',
        lbl_payment_date = '{lang("lbl_payment_date")}',//7
        lbl_quantity = '{lang("lbl_quantity")}',
        lbl_tax_type = '{lang("lbl_tax_type")}',
        lbl_unit_price = '{lang("lbl_unit_price")}',
        lbl_amount = '{lang("lbl_amount")}',//11
        lbl_order_detail_cost_unit_price = '{lang("lbl_order_detail_cost_unit_price")}',
        lbl_order_detail_cost_total_price = '{lang("lbl_order_detail_cost_total_price")}',
        lbl_gross_profit_amount = '{lang("lbl_gross_profit_amount")}',
        lbl_order_detail_gross_profit_rate = '{lang("lbl_order_detail_gross_profit_rate")}',
        lbl_j_code = '{lang("lbl_j_code")}',
        lbl_s_code = '{lang("lbl_s_code")}',
        lbl_account_id = '{lang("lbl_account_id")}',
        lbl_is_ad_sales_slip_input = '{lang("lbl_is_ad_sales_slip_input")}',
        lbl_is_ad_invoice_issue = '{lang("lbl_is_ad_invoice_issue")}',
        lbl_is_order_input = '{lang("lbl_is_order_input")}',
        lbl_is_acceptance_input = '{lang("lbl_is_acceptance_input")}',
        lbl_is_sales_slip_input = '{lang("lbl_is_sales_slip_input")}',
        lbl_is_invoice_issue = '{lang("lbl_is_invoice_issue")}',
        lbl_name = '{lang("lbl_name")}',
        lbl_content = '{lang("lbl_content")}',
        lbl_action = '{lang("lbl_action")}',//27
        lbl_change_report = '{lang("lbl_change_report")}',
        lbl_copy_branch = '{lang("lbl_copy_branch")}',
        lbl_delete_branch = '{lang("lbl_delete_branch")}',
        lbl_cancel_branch = '{lang("lbl_cancel_branch")}',
        lbl_show = '{lang("lbl_show")}',
        lbl_check_approve_more = '{lang("lbl_check_approve_more")}',
        lbl_cancel_branch_release = '{lang("lbl_cancel_branch_release")}'
    ];

</script>

{literal}
<script>
    $("#cbx-business-unit").on('change', function(event) {
        business_unit_id = $(this).val();

        if (business_unit_id != -1) {
            $('#cbx-account').prop('disabled', 'disabled');
            $.ajax({
                url: '/_admin/order_acceptance/ajax_get_account_by_business_unit_id',
                type: 'POST',
                data: {business_unit_id: business_unit_id}
            })
            .done(function(data) {

                if (data == 0) {
                    //                     window.location.reload(true);return;
                    dom = "<option value='-1'>" + select_all + "</option>";
                    $("#cbx-account").html(dom);
                    $('#cbx-account').prop('disabled', false);
                    return;
                }

                dom = "<option value='-1'>" + select_all + "</option>";
                $.each(data, function(index, val) {
                     dom += "<option value='" + val.id + "'>" + val.name + "</option>";
                });
                $("#cbx-account").html(dom);
                $('#cbx-account').prop('disabled', false);
            })
        } else {
            $.ajax({
                url: '/_admin/order_acceptance/ajax_get_all_accounts',
                type: 'POST'
            })
            .done(function(data) {
                if (data == 0) {
//                  window.location.reload(true);return;
                 dom = "<option value='-1'>" + select_all + "</option>";
                 $("#cbx-account").html(dom);
                 $('#cbx-account').prop('disabled', false);
                 return;
             }

             dom = "<option value='-1'>" + select_all + "</option>";
             $.each(data, function(index, val) {
                  dom += "<option value='" + val.id + "'>" + val.name + "</option>";
             });
             $("#cbx-account").html(dom);
             $('#cbx-account').prop('disabled', false);
            })
        }
    });

    $("#dialog").dialog({
        autoOpen : false,
        width : "auto",
        height : "auto",
        modal : true,

        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#dialog').dialog('close');
            })
        },
        buttons : {
        },
        close : function() {
        }
    });

    $('body').on('click', '.btn-close', function() {
        $(this).parents('.ui-dialog-content').dialog('close');
    });


</script>
{/literal}

{include file = "inc/footer.tpl"}