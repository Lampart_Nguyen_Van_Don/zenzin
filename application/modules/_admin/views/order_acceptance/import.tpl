{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

<script src="/public/admin/js/jquery-fixed-thead.js"></script>
<!-- #7846:Start -->
<script src="/public/admin/js/jszip.js"></script>
<script src="/public/admin/js/xlsx.js"></script>
<!-- #7846:End -->

<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>
        <div class="import-container">
            <form id="form-download" action="/_admin/order_acceptance/download_template">
                <button type="submit" class="btn-small btn-green btn-download">{lang('btn_download_template')}</button>
            </form>
            <br>
            <form id="form-import" action="/_admin/order_acceptance/ajax_import_excel" method="post" enctype="multipart/form-data">
                <input id="upload-unique-id" name="unique_id" type="hidden">
                <div id="error-container">
                    {if isset($upload_status)}
                        <div class='error-message'>{lang('lbl_import_error')}</div>
                    {/if}
                </div>
                <table class="table-import">
                    <tr>
                        <td width="120">{lang('lbl_input_file')}</td>
                        <td width="700"><div id="file-path"></div></td>
                        <td width="190" align="center">
                            <div class="input-wrapper">
                                {lang('btn_browse')}
                                <!-- #6555:Start -->
                                <input id="browse-file" class="file-input" type="file" name="excel_file" accept=".xlsx"/>
                                <!-- use csv file as import file -->
                                <!-- <input id="browse-file" class="file-input" type="file" name="excel_file" accept=".csv"/> -->
                                <!-- #6555:End -->
                            </div>
                            <span class="red-symbol">{lang('lbl_file_limit_size')}</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="import-propress-bar">
                                <div class="progress-label"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            <button type="submit" class="btn-small btn-green" id="upload-file">{lang('btn_upload')}</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

<div id="dialog-import-result" style="display:none"></div>
<!-- #6555:Start -->
<!-- use for csv -->
<style rel="styleSheet">
.table-simple th,.table-simple th, .table-simple td {
    font-size: 12px;
}

.ui-dialog .ui-dialog-content {
    margin: .5em 1em;
    overflow: hidden;
    padding: 0
}
</style>

<div id="table-container" style="display:none">
    <table class="table-simple table-header-fixed" border="1" width="3470" id="table_header_fixed">
        <thead>
            <tr>
                <th width="245">チェック結果</th>
                <th width="72">Ｊコード</th>
                <th width="35">枝</th>
                <th width="100">Ｊ担当営業</th>
                <th width="100">Ｊ担当営業ID</th>
                <th width="90">手配担当</th>
                <th width="100">手配担当ID</th>
                <th width="150">クライアント社名</th>
                <th width="110">請求元ブレーン</th>
                <th width="160">請求元ブレーンコード</th>
                <th width="130">作業ブレーン</th>
                <th width="150">作業ブレーンコード</th>
                <th width="100">発送ブレーン</th>
                <th width="150">発送ブレーンコード</th>
                <th width="100">発送種別</th>
                <th width="110">発送種別コード</th>
                <th width="160">セット名</th>
                <th width="60">単価</th>
                <th width="60">件数</th>
                <th width="75">発送日</th>
                <th width="50">円口</th>
                <th width="50">重量</th>
                <th width="100">発送物形状</th>
                <th width="150">発送物形状コード</th>
                <th width="75">計上月</th>
                <th width="72">非課税</th>
                <th width="72">税込発送料</th>
                <th width="72">税込切手代</th>
                <th width="72">税込その他</th>
                <th width="72">税別発送料</th >
                <th width="72">税別その他</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<!-- #6555:End -->

<script>
    label = {
        please_select_file: "{lang('lbl_please_select_file')}",
        unsupported_file_type: "{lang('lbl_unsupported_file')}",
        loading: "{lang('lbl_loading')}",
        completed: "{lang('lbl_complete')}",
        max_size: "{lang('error_max_size')}",
        empty: "{lang('lbl_empty_file')}",
        invalid_format: "{lang('lbl_invalid_format')}",
// use csv file as import file
//#6555:Start
        cancel: "{lang('btn_cancel')}",
        import: "{lang('btn_import')}",
//#6555:End
    };
</script>

{literal}
<script>
    $(".import-propress-bar").toggle();
    $(".import-propress-bar").progressbar({
        value: false,
        disabled: true,
        change: function() {
            $(".progress-label").text($(".import-propress-bar").progressbar( "value" ) + "%");
        },
    });


    $("#browse-file").on('change', function(event) {
        path = $("#browse-file").val();
        pos = path.lastIndexOf('\\');
        $("#file-path").text(path.substring(pos+1));
    });

    $('.table-header-fixed').fixed_thead({
        maxHeight: 400
    });

// use solution import csv file
//#6555:Start
    // var progress_status = false;
    // var xhr;

    // $('#form-import').submit(function(event) {
    //     event.preventDefault();

    //     progress_status = false;
    //     handleUploadUI(true, false);

    //     $(".import-propress-bar").progressbar({
    //         value: 0
    //     });

    //     file = $("#browse-file").val();
    //     if (!file) {
    //         handleUploadUI(false, false, label.please_select_file);
    //         return false; 
    //     }

    //     var unique_id = generate_unique_id();
    //     $("#upload-unique-id").val(unique_id);

    //     $(this).ajaxSubmit({
    //         target:   '#dialog-import-result',
    //         beforeSubmit: before_submit,
    //         success: after_success,
    //         uploadProgress: on_progress,
    //         resetForm: true,
    //         // replaceTarget: true
    //     });
    //     if (xhr) xhr.abort();

    //     var get_progress = function() {
    //         xhr = $.getJSON("/_admin/order_acceptance/ajax_get_progress", {unique_id: unique_id}, function(data) {
    //             var cur_progress = $(".import-propress-bar").progressbar("value");
    //             if (data.status <= 100 && data.status > 0 && cur_progress != 100) {
    //                 var status = data.status >= 100 ? 99 : data.status;
    //                 $(".import-propress-bar").progressbar({
    //                     value: status
    //                 });
    //             }
    //             if (!progress_status) get_progress();
    //         });
    //     }
        
    //     get_progress();             
    // });

    // var generate_unique_id = function() {;
    //     rand = Math.floor(Math.random() * 26) + Date.now();
    //     return rand;
    // };

    // var before_submit = function() {
    //     $('#dialog-import-result').html('');
    //     if (window.File && window.FileReader && window.FileList && window.Blob) {
    //         file_type = $('#browse-file')[0].files[0].type;
    //         file_size = $('#browse-file')[0].files[0].size;

    //         // > 10MB
    //         if(file_size > 10485760) {
    //             handleUploadUI(false, false, label.max_size);
    //             return false;
    //         }

    //     } else {
    //         handleUploadUI(false, false);
    //         return false;
    //     }

    //     handleUploadUI(true, true);
    // };

    // var after_success = function(){
    //     try {
    //         progress_status = true;
    //         xhr.abort();
    //         $("#file-path").text('');
    //         upload_result = $.parseJSON($("#dialog-import-result").text());
    //         if (typeof upload_result == 'object' && !upload_result.status) {
    //             handleUploadUI(false, true, upload_result.messages);
    //         } else {
    //             $("#table-container").dialog({
    //                 title: '確認',
    //                 width: '80%',
    //                 height: 'auto',
    //                 modal: true,
    //                 maxHeight: 500,
    //                 position: {
    //                     my: "center", 
    //                     at: "center", 
    //                     of: window
    //                 },
    //                 show: {
    //                     duration: 100,
    //                     delay: 100,
    //                 },
    //                 open: function(event, ui) {
    //                     $(".import-propress-bar").progressbar({
    //                         value: 100
    //                     });
    //                 },
    //                 beforeClose: function( event, ui ) {
    //                     $("#table-container table tbody").html('');
    //                     $("#dialog-import-result").html('');
    //                     handleUploadUI(false, true);
    //                 }
    //             });

    //             if ($("#table-container").dialog('isOpen')) {
    //                 var dom = '';
    //                 var has_error = false;
    //                 $.each(upload_result.messages, function(index, data) {
    //                     dom += '<tr><td style="font-size:12px">' +
    //                                 '<div style="width:245px;">';
    //                     if (data.errors) {
    //                         $.each(data.errors, function(e_index, error) {
    //                             dom += '<p style="color:red">'+error+'</p>';
    //                         })
    //                         has_error = true;
    //                     } else {
    //                         dom += 'OK';
    //                     }
                                            
    //                     dom += '</div></td>';
                        
    //                     dom += '<td><div style="width:72px;">' + to_str(data.j_code) + '</div></td>';

    //                     dom += '<td><div style="width:35px;">' + to_str(data.branch_cd) + '</div></td>';

    //                     dom += '<td><div style="width:100px;">' + to_str(data.login_name) + '</div></td>';

    //                     dom += '<td><div style="width:100px;">' + to_str(data.login_id) + '</div></td>';

    //                     dom += '<td><div style="width:90px;">' + to_str(data.arrange_rep_name) + '</div></td>';

    //                     dom += '<td><div style="width:100px;">' + to_str(data.arrange_rep_login_id) + '</div></td>';

    //                     dom += '<td><div style="width:150px;">' + to_str(data.client_name) + '</div></td>';

    //                     dom += '<td><div style="width:110px;">' + to_str(data.abbr_name) + '</div></td>';

    //                     dom += '<td><div style="width:160px;">' + to_str(data.source_subcontractor_code) + '</div></td>';

    //                     dom += '<td><div style="width:130px;">' + to_str(data.work_subcontractor_name) + '</div></td>';

    //                     dom += '<td><div style="width:150px;">' + to_str(data.work_subcontractor_code) + '</div></td>';

    //                     dom += '<td><div style="width:100px;">' + to_str(data.shipping_subcontractor_name) + '</div></td>';

    //                     dom += '<td><div style="width:150px;">' + to_str(data.shipping_subcontractor_code) + '</div></td>';

    //                     dom += '<td><div style="width:100px;">' + to_str(data.shipping_type_name) + '</div></td>';

    //                     dom += '<td><div style="width:110px;">' + to_str(data.shipping_type_cd) + '</div></td>';

    //                     dom += '<td><div style="width:160px;">' + to_str(data.set_name) + '</div></td>';

    //                     dom += '<td><div style="width:60px;">' + addCommas(data.unit_price) + '</div></td>';

    //                     dom += '<td><div style="width:60px;">' + addCommas(data.quantity) + '</div></td>';

    //                     dom += '<td><div style="width:75px;">' + convert_delimiter(data.delivery_date) + '</div></td>';

    //                     dom += '<td><div style="width:50px;">' + addCommas(data.fee) + '</div></td>';

    //                     dom += '<td><div style="width:50px;">' + addCommas(data.weight) + '</div></td>';

    //                     dom += '<td><div style="width:100px;">' + to_str(data.shipments_shape_name) + '</div></td>';

    //                     dom += '<td><div style="width:150px;">' + to_str(data.shipments_shape_cd) + '</div></td>';

    //                     dom += '<td><div style="width:75px;">' + convert_delimiter(data.summary_month) + '</div></td>';

    //                     dom += '<td><div style="width:72px;">' + addCommas(data.tax_free) + '</div></td>';

    //                     dom += '<td><div style="width:72px;">' + addCommas(data.shipping_charge_tax) + '</div></td>';

    //                     dom += '<td><div style="width:72px;">' + addCommas(data.stamp_tax) + '</div></td>';

    //                     dom += '<td><div style="width:72px;">' + addCommas(data.etc_tax) + '</div></td>';

    //                     dom += '<td><div style="width:72px;">' + addCommas(data.shipping_charge) + '</div></td>';

    //                     dom += '<td><div style="width:72px;">' + addCommas(data.etc) + '</div></td>';

    //                     dom + '</tr>';

    //                     // if (index == 100) return;
    //                 });
                    
    //                 if (has_error) {
    //                     dom += '<tr><td colspan="32"><button type="button" class="btn-small btn-green btn-close" >'+label.cancel+'</button></td></tr>';
    //                 } else {
    //                     dom += '<tr><td colspan="32">' +
    //                                 '<form method="post">' +
    //                                     '<textarea name="order_acceptances" style="display:none">'+ JSON.stringify(upload_result.messages) +'</textarea>' +
    //                                     '<button type="submit" class="btn-small btn-green">'+ label.import +'</button>' +
    //                                     '<button type="button" class="btn-small btn-green btn-close">'+label.cancel+'</button>' +
    //                                 '</form>'+
    //                             '</td></tr>';
    //                 }
                    
    //                 $("#table-container table tbody").append(dom);
    //             }

    //         }
    //     } catch (error) {
    //         console.log(error);
    //     }
        
        
    // };

    // var on_progress = function(event, position, total, percentComplete) {
    // }

//#6555:End

// use php library (PHPExcel) to read excel file on server side. But if the file contain too much record (>3000)
// the library can not handle and maybe crash
// The file have to contain maximum 2000 record for the best result
//#6555:Start
    // var progress_status = false;
    // var xhr;
    // $('#form-import').submit(function(event) {
    //     event.preventDefault();

    //     progress_status = false;
    //     handleUploadUI(true, false);

    //     $(".import-propress-bar").progressbar({
    //         value: 0
    //     });

    //     file = $("#browse-file").val();
    //     if (!file) {
    //         handleUploadUI(false, false, label.please_select_file);
    //         return false; 
    //     }

    //     var unique_id = generate_unique_id();
    //     $("#upload-unique-id").val(unique_id);

    //     $(this).ajaxSubmit({
    //         target:   '#dialog-import-result',
    //         beforeSubmit: before_submit,
    //         success: after_success,
    //         uploadProgress: on_progress,
    //         resetForm: true,
    //         // replaceTarget: true
    //     });
    //     if (xhr) xhr.abort();

    //     var get_progress = function() {
    //         xhr = $.getJSON("/_admin/order_acceptance/ajax_get_progress", {unique_id: unique_id}, function(data) {
    //             var cur_progress = $(".import-propress-bar").progressbar("value");
    //             if (data.status <= 100 && data.status > 0 && cur_progress != 100) {
    //                 var status = data.status >= 100 ? 99 : data.status;
    //                 $(".import-propress-bar").progressbar({
    //                     value: status
    //                 });
    //             }
    //             if (!progress_status) get_progress();
    //         });
    //     }
        
    //     get_progress();             
    // });

    // var generate_unique_id = function() {;
    //     rand = Math.floor(Math.random() * 26) + Date.now();
    //     return rand;
    // };

    // var before_submit = function() {
    //     $('#dialog-import-result').html('');
    //     if (window.File && window.FileReader && window.FileList && window.Blob) {
    //         file_type = $('#browse-file')[0].files[0].type;
    //         file_size = $('#browse-file')[0].files[0].size;

    //         // > 10MB
    //         if(file_size > 10485760) {
    //             handleUploadUI(false, false, label.max_size);
    //             return false;
    //         }

    //     } else {
    //         handleUploadUI(false, false);
    //         return false;
    //     }

    //     handleUploadUI(true, true);
    // };

    // var after_success = function(){
    //     try {
    //         progress_status = true;
    //         xhr.abort();
    //         $("#file-path").text('');
    //         upload_result = $.parseJSON($("#dialog-import-result").text());
    //         if (typeof upload_result == 'object' && !upload_result.status) {
    //             handleUploadUI(false, true, upload_result.messages);
    //         } else {
    //             $("#table-container").dialog({
    //                 title: '確認',
    //                 width: '80%',
    //                 height: 'auto',
    //                 modal: true,
    //                 maxHeight: 500,
    //                 position: {
    //                     my: "center", 
    //                     at: "center", 
    //                     of: window
    //                 },
    //                 show: {
    //                     duration: 100,
    //                     delay: 100,
    //                 },
    //                 open: function(event, ui) {
    //                     $(".import-propress-bar").progressbar({
    //                         value: 100
    //                     });
    //                 },
    //                 beforeClose: function( event, ui ) {
    //                     $("#table-container table tbody").html('');
    //                     $("#dialog-import-result").html('');
    //                     handleUploadUI(false, true);
    //                 }
    //             });

    //             if ($("#table-container").dialog('isOpen')) {
    //                 var dom = '';
    //                 var has_error = false;
    //                 $.each(upload_result.messages, function(index, data) {
    //                     dom += '<tr><td style="font-size:12px">' +
    //                                 '<div style="width:245px;">';
    //                     if (data.errors) {
    //                         $.each(data.errors, function(e_index, error) {
    //                             dom += '<p style="color:red">'+error+'</p>';
    //                         })
    //                         has_error = true;
    //                     } else {
    //                         dom += 'OK';
    //                     }
                                            
    //                     dom += '</div></td>';
                        
    //                     dom += '<td><div style="width:72px;">' + to_str(data.j_code) + '</div></td>';

    //                     dom += '<td><div style="width:35px;">' + to_str(data.branch_cd) + '</div></td>';

    //                     dom += '<td><div style="width:100px;">' + to_str(data.login_name) + '</div></td>';

    //                     dom += '<td><div style="width:100px;">' + to_str(data.login_id) + '</div></td>';

    //                     dom += '<td><div style="width:90px;">' + to_str(data.arrange_rep_name) + '</div></td>';

    //                     dom += '<td><div style="width:100px;">' + to_str(data.arrange_rep_login_id) + '</div></td>';

    //                     dom += '<td><div style="width:150px;">' + to_str(data.client_name) + '</div></td>';

    //                     dom += '<td><div style="width:110px;">' + to_str(data.abbr_name) + '</div></td>';

    //                     dom += '<td><div style="width:160px;">' + to_str(data.source_subcontractor_code) + '</div></td>';

    //                     dom += '<td><div style="width:130px;">' + to_str(data.work_subcontractor_name) + '</div></td>';

    //                     dom += '<td><div style="width:150px;">' + to_str(data.work_subcontractor_code) + '</div></td>';

    //                     dom += '<td><div style="width:100px;">' + to_str(data.shipping_subcontractor_name) + '</div></td>';

    //                     dom += '<td><div style="width:150px;">' + to_str(data.shipping_subcontractor_code) + '</div></td>';

    //                     dom += '<td><div style="width:100px;">' + to_str(data.shipping_type_name) + '</div></td>';

    //                     dom += '<td><div style="width:110px;">' + to_str(data.shipping_type_cd) + '</div></td>';

    //                     dom += '<td><div style="width:160px;">' + to_str(data.set_name) + '</div></td>';

    //                     dom += '<td><div style="width:60px;">' + addCommas(data.unit_price) + '</div></td>';

    //                     dom += '<td><div style="width:60px;">' + addCommas(data.quantity) + '</div></td>';

    //                     dom += '<td><div style="width:75px;">' + convert_delimiter(data.delivery_date) + '</div></td>';

    //                     dom += '<td><div style="width:50px;">' + addCommas(data.fee) + '</div></td>';

    //                     dom += '<td><div style="width:50px;">' + addCommas(data.weight) + '</div></td>';

    //                     dom += '<td><div style="width:100px;">' + to_str(data.shipments_shape_name) + '</div></td>';

    //                     dom += '<td><div style="width:150px;">' + to_str(data.shipments_shape_cd) + '</div></td>';

    //                     dom += '<td><div style="width:75px;">' + convert_delimiter(data.summary_month) + '</div></td>';

    //                     dom += '<td><div style="width:72px;">' + addCommas(data.tax_free) + '</div></td>';

    //                     dom += '<td><div style="width:72px;">' + addCommas(data.shipping_charge_tax) + '</div></td>';

    //                     dom += '<td><div style="width:72px;">' + addCommas(data.stamp_tax) + '</div></td>';

    //                     dom += '<td><div style="width:72px;">' + addCommas(data.etc_tax) + '</div></td>';

    //                     dom += '<td><div style="width:72px;">' + addCommas(data.shipping_charge) + '</div></td>';

    //                     dom += '<td><div style="width:72px;">' + addCommas(data.etc) + '</div></td>';

    //                     dom + '</tr>';

    //                     // if (index == 100) return;
    //                 });
                    
    //                 if (has_error) {
    //                     dom += '<tr><td colspan="32"><button type="button" class="btn-small btn-green btn-close" >'+label.cancel+'</button></td></tr>';
    //                 } else {
    //                     dom += '<tr><td colspan="32">' +
    //                                 '<form method="post">' +
    //                                     '<textarea name="order_acceptances" style="display:none">'+ JSON.stringify(upload_result.messages) +'</textarea>' +
    //                                     '<button type="submit" class="btn-small btn-green">'+ label.import +'</button>' +
    //                                     '<button type="button" class="btn-small btn-green btn-close">'+label.cancel+'</button>' +
    //                                 '</form>'+
    //                             '</td></tr>';
    //                 }
                    
    //                 $("#table-container table tbody").append(dom);
    //             }
    //         }
    //     } catch (error) {
    //         console.log(error);
    //     }
        
        
    // };

    // var on_progress = function(event, position, total, percentComplete) {
    // }
//#6555:End

// use another library (xlsx.js) to read the file on client side, then parse data to json and send that json to server
// each time send 200 record to server to handle and store the result to a json file type until
// all data have been sent and read that json file and parse to view for render
//#6555:Start
    $('#form-import').submit(function(event) {
       event.preventDefault();
       handleUploadUI(true, false);

       $(".import-propress-bar").progressbar({
           value: 0
       });

       file = $("#browse-file").val();
       if (!file) {
           handleUploadUI(false, false, label.please_select_file);
           return;
       }

       // > 10MB
       if($('#browse-file')[0].files[0].size > 10485760) {
           handleUploadUI(false, false, label.max_size);
           return;
       }
           
       switch($('#browse-file')[0].files[0].type) {
           case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
               break;
           case 'application/vnd.ms-excel':
               break;
           default:
               handleUploadUI(false, false, label.unsupported_file_type)
               return;
       }

       var unique_id = generate_unique_id();
       $("#upload-unique-id").val(unique_id);

       var handle_progress = function(excel_datas) {
           if ($.isEmptyObject(excel_datas)) {
               handleUploadUI(false,false, label.empty);
               return;
           }

           var i;
           var header = ['Ｊコード','枝','Ｊ担当営業','Ｊ担当営業ID','手配担当','手配担当ID','クライアント社名','請求元ブレーン','請求元ブレーンコード','作業ブレーン','作業ブレーンコード','発送ブレーン','発送ブレーンコード','発送種別','発送種別コード','セット名','単価','件数','発送日','円口','重量','発送物形状','発送物形状コード','計上月','非課税','税込発送料','税込切手代','税込その他','税別発送料','税別その他'];
           for (i = 0; i < 30; i++) {
               if (excel_datas[0][i] != header[i]) {
                   handleUploadUI(false, false, label.invalid_format);
                   return;
               }                
           }

           if (excel_datas.length == 1) {
               handleUploadUI(false, false, label.empty);
               return;
           }

           var total_sent = 1;
           var last_data  = 0;
           var chunk      = 200;
           var block      = [];
           if (excel_datas.length > 1) {
               do {
                   var send_data = [];
                   var i = total_sent;
                   for (i; i <= chunk; i++) {
                       if (excel_datas[i]) {
                           var k = 0;
                           for (k; k < 30; k++) {
                               if (excel_datas[i][k].trim() != "") {
                                   send_data.push(excel_datas[i]);
                                   break;
                               }
                           }
                           total_sent++;                            
                       }

                       if (total_sent >= excel_datas.length) {
                           last_data = 1;
                       }
                   }

                   chunk += 200;
                   if (send_data.length > 0) {
                       block.push(send_data);    
                   }

               } while (!last_data);
               

               if (block.length == 0) {
                   handleUploadUI(false, false, label.empty);
                   return;
               }

               
               handleUploadUI(true, true);

               var i = 0;
               var last_data = block.length > 1 ? 0 : 1;
               var step = Math.floor((100 / block.length)*100)/100;
               var send = function(block, i, last_data) {
                   $.ajax({
                       url: '/_admin/order_acceptance/ajax_import_excel',
                       type: 'POST',
                       data: {datas: JSON.stringify(block[i]), unique_id: unique_id, last_data: last_data},
                   })
                   .done(function(data) {
                       var cur_progress = $(".import-propress-bar").progressbar("value");
                       if (step <= 100) {
                           var progress = (Math.floor((cur_progress + step)*100)/100) < 100 ? (Math.floor((cur_progress + step)*100)/100) : 99;
                           $(".import-propress-bar").progressbar({
                               value: progress,
                           });
                       }

                       try {
                           data = $.parseJSON(data);
                           if (!data.status) {
                               if (i <= block.length - 2) {
                                   i++;
                                   last_data = i == block.length - 1 ? 1 : 0;
                                   send(block, i, last_data);
                               }
                           } else {
                               $(".import-propress-bar").progressbar({
                                   value: 100,
                               });

                                $("#table-container").dialog({
                                    title: '確認',
                                    width: '80%',
                                    height: 'auto',
                                    modal: true,
                                    maxHeight: 500,
                                    position: {
                                        my: "center", 
                                        at: "center", 
                                        of: window
                                    },
                                    show: {
                                        duration: 100,
                                        delay: 100,
                                    },
                                    open: function(event, ui) {
                                        $(".import-propress-bar").progressbar({
                                            value: 100
                                        });
                                    },
                                    beforeClose: function( event, ui ) {
                                        $("#table-container table tbody").html('');
                                        $("#dialog-import-result").html('');
                                        handleUploadUI(false, true);
                                    }
                                });

                                if ($("#table-container").dialog('isOpen')) {
                                    var dom = '';
                                    var has_error = false;
                                    $.each(data.messages, function(index, data) {
                                        dom += '<tr><td style="font-size:12px">' +
                                                    '<div style="width:245px;">';
                                        if (data.errors) {
                                            $.each(data.errors, function(e_index, error) {
                                                dom += '<p style="color:red">'+error+'</p>';
                                            })
                                            has_error = true;
                                        } else {
                                            dom += 'OK';
                                        }
                                                            
                                        dom += '</div></td>';
                                        
                                        dom += '<td><div style="width:72px;">' + to_str(data.j_code) + '</div></td>';

                                        dom += '<td><div style="width:35px;">' + to_str(data.branch_cd) + '</div></td>';

                                        dom += '<td><div style="width:100px;">' + to_str(data.login_name) + '</div></td>';

                                        dom += '<td><div style="width:100px;">' + to_str(data.login_id) + '</div></td>';

                                        dom += '<td><div style="width:90px;">' + to_str(data.arrange_rep_name) + '</div></td>';

                                        dom += '<td><div style="width:100px;">' + to_str(data.arrange_rep_login_id) + '</div></td>';

                                        dom += '<td><div style="width:150px;">' + to_str(data.client_name) + '</div></td>';

                                        dom += '<td><div style="width:110px;">' + to_str(data.abbr_name) + '</div></td>';

                                        dom += '<td><div style="width:160px;">' + to_str(data.source_subcontractor_code) + '</div></td>';

                                        dom += '<td><div style="width:130px;">' + to_str(data.work_subcontractor_name) + '</div></td>';

                                        dom += '<td><div style="width:150px;">' + to_str(data.work_subcontractor_code) + '</div></td>';

                                        dom += '<td><div style="width:100px;">' + to_str(data.shipping_subcontractor_name) + '</div></td>';

                                        dom += '<td><div style="width:150px;">' + to_str(data.shipping_subcontractor_code) + '</div></td>';

                                        dom += '<td><div style="width:100px;">' + to_str(data.shipping_type_name) + '</div></td>';

                                        dom += '<td><div style="width:110px;">' + to_str(data.shipping_type_cd) + '</div></td>';

                                        dom += '<td><div style="width:160px;">' + to_str(data.set_name) + '</div></td>';

                                        dom += '<td><div style="width:60px;">' + addCommas(data.unit_price) + '</div></td>';

                                        dom += '<td><div style="width:60px;">' + addCommas(data.quantity) + '</div></td>';

                                        dom += '<td><div style="width:75px;">' + convert_delimiter(data.delivery_date) + '</div></td>';

                                        dom += '<td><div style="width:50px;">' + addCommas(data.fee) + '</div></td>';

                                        dom += '<td><div style="width:50px;">' + addCommas(formatWeight(data.weight)) + '</div></td>';

                                        dom += '<td><div style="width:100px;">' + to_str(data.shipments_shape_name) + '</div></td>';

                                        dom += '<td><div style="width:150px;">' + to_str(data.shipments_shape_cd) + '</div></td>';

                                        dom += '<td><div style="width:75px;">' + convert_delimiter(data.summary_month) + '</div></td>';

                                        dom += '<td><div style="width:72px;">' + addCommas(data.tax_free) + '</div></td>';

                                        dom += '<td><div style="width:72px;">' + addCommas(data.shipping_charge_tax) + '</div></td>';

                                        dom += '<td><div style="width:72px;">' + addCommas(data.stamp_tax) + '</div></td>';

                                        dom += '<td><div style="width:72px;">' + addCommas(data.etc_tax) + '</div></td>';

                                        dom += '<td><div style="width:72px;">' + addCommas(data.shipping_charge) + '</div></td>';

                                        dom += '<td><div style="width:72px;">' + addCommas(data.etc) + '</div></td>';

                                        dom + '</tr>';

                                        // if (index == 100) return;
                                    });
                                    
                                    if (has_error) {
                                        dom += '<tr><td colspan="32"><button type="button" class="btn-small btn-green btn-close" >'+label.cancel+'</button></td></tr>';
                                    } else {
                                        dom += '<tr><td colspan="32">' +
                                                    '<form method="post">' +
                                                        '<textarea name="order_acceptances" style="display:none">'+ JSON.stringify(data.messages) +'</textarea>' +
                                                        '<button type="submit" class="btn-small btn-green">'+ label.import +'</button>' +
                                                        '<button type="button" class="btn-small btn-green btn-close">'+label.cancel+'</button>' +
                                                    '</form>'+
                                                '</td></tr>';
                                    }
                                    
                                    $("#table-container table tbody").append(dom);
                                }
                           }
                       } catch (error) {
                           console.log(error);
                       }
                   });
               }

               send(block, i, last_data);
           }
           
       }
       excel_datas = handleFile(event, handle_progress);
       
       return;   
   });

   var X = XLSX;

   function fixdata(data) {
       var o = "", l = 0, w = 10240;
       for(; l<data.byteLength/w; ++l) o+=String.fromCharCode.apply(null,new Uint8Array(data.slice(l*w,l*w+w)));
       o+=String.fromCharCode.apply(null, new Uint8Array(data.slice(l*w)));
       return o;
   }

   function to_json(workbook) {
       var result = {};
       workbook.SheetNames.forEach(function(sheetName) {
           var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName], {header:1});
           $.each(roa, function(row, row_data) {
               for (var column = 0; column <= 29; column++) {
                   if (!row_data[column]) row_data[column] = '';
               }
           });
           
           if(roa.length > 0){
               result = roa;
           }
           return;
       });
       return result;
   }


   function process_wb(wb) {
       var output = "";
       // output = JSON.stringify(to_json(wb), 2, 2);
       output = to_json(wb);
       return output;
   }

   var xlf = document.getElementById('browse-file');
   
   function handleFile(e, callback) {
       try {
           var files = e.target.files;
           var f = $('#browse-file')[0].files[0];
           var reader = new FileReader();
           var name = f.name;
           var output = {};
           reader.onload = function(e, output) {
               var data = e.target.result;
               var wb;
               var arr = fixdata(data);

               wb = X.read(btoa(arr), {type: 'base64'});
               output = process_wb(wb);

               callback(output);
           };
           
           reader.readAsArrayBuffer(f);
       } catch (error) {
           console.log(error);
       }    
   }

//#6555:End

   var handleUploadUI = function(is_disable, is_toggle_progress, messages) {
       if (messages) {
           $('#error-container').html("<div class='error-message'>" + messages + "</div>");
       } else {
           $('#error-container').html('');
       }

       if (is_toggle_progress) $(".import-propress-bar").toggle();
       
       if (is_disable) {
           $(".btn-download").attr('disabled', 'disabled');
           $("#upload-file").attr('disabled', 'disabled');
       } else {
           $(".btn-download").removeAttr('disabled');
           $("#upload-file").removeAttr('disabled');
           $("#browse-file").val('');
           $("#file-path").text('');
       }

   }

    var generate_unique_id = function() {;
        rand = Math.floor(Math.random() * 26) + Date.now();
        return rand;
    };

    var to_str = function(data) {
        if (data) return data.toString();
        return '';
    }

    var convert_delimiter = function(data)  {
        if (data) return data.replace(/-/g, '/');
        return '';
    }

    $(document).on('click', '.btn-close', function() {
        $("#table-container").dialog('close');
    });
    
</script>
{/literal}

{include file = "inc/footer.tpl"}