<style rel="styleSheet">
.table-simple th,.table-simple th, .table-simple td {
    font-size: 12px;
}
</style>
<div id="table-container">
    <table class="table-simple table-header-fixed" border="1" width="3470" id="table_header_fixed">
        <thead><tr>
                <th width="245">チェック結果</th>
                <th width="72">Ｊコード</th>
                <th width="35">枝</th>
                <th width="100">Ｊ担当営業</th>
                <th width="100">Ｊ担当営業ID</th>
                <th width="90">手配担当</th>
                <th width="100">手配担当ID</th>
                <th width="150">クライアント社名</th>
                <th width="110">請求元ブレーン</th>
                <th width="160">請求元ブレーンコード</th>
                <th width="130">作業ブレーン</th>
                <th width="150">作業ブレーンコード</th>
                <th width="100">発送ブレーン</th>
                <th width="150">発送ブレーンコード</th>
                <th width="100">発送種別</th>
                <th width="110">発送種別コード</th>
                <th width="160">セット名</th>
                <th width="60">単価</th>
                <th width="60">件数</th>
                <th width="75">発送日</th>
                <th width="50">円口</th>
                <th width="50">重量</th>
                <th width="100">発送物形状</th>
                <th width="150">発送物形状コード</th>
                <th width="75">計上月</th>
                <th width="72">非課税</th>
                <th width="72">税込発送料</th>
                <th width="72">税込切手代</th>
                <th width="72">税込その他</th>
                <th width="72">税別発送料</th >
                <th >税別その他</th>
            </tr>
          </thead>
          <tbody>
        {$has_error = false}
        {foreach $datas_import as $data}
            {if isset($data['errors'])}{$has_error = true}{/if}
            <tr>
                <td style="font-size:12px">
                    <div style="width:245px;">{if isset($data['errors'])}
                        {foreach $data['errors'] as $error}
                        <p style="color:red">{$error}</p>
                        {/foreach}
                    {else}
                        OK
                    {/if}
                    </div>
                </td>
                <td>
                    <div style="width:72px;">
                        {if isset($data['j_code'])}
                            {$data['j_code']}
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width:35px;">
                        {if isset($data['branch_cd'])}
                            {$data['branch_cd']}
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width:100px;">{$data['login_name']}</div>
                </td>
                <td>
                    <div style="width:100px;">{$data['login_id']}</div>
                </td>
                <td>
                    <div style="width:90px;">{$data['arrange_rep_name']}</div>
                </td>
                <td>
                    <div style="width: 100px;">{$data['arrange_rep_login_id']}</div>
                </td>
                <td>
                    <div style="width: 150px;">{$data['client_name']}</div>
                </td>
                <td>
                    <div style="width: 100px;">
                        {if isset($data['abbr_name'])}
                            {$data['abbr_name']}
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width: 160px;">
                        {if isset($data['source_subcontractor_code'])}
                            {$data['source_subcontractor_code']}
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width: 130px;">
                        {if isset($data['work_subcontractor_name'])}
                            {$data['work_subcontractor_name']}
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width: 150px;">
                        {if isset($data['work_subcontractor_code'])}
                            {$data['work_subcontractor_code']}
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width: 100px;">
                        {if isset($data['shipping_subcontractor_name'])}
                            {$data['shipping_subcontractor_name']}
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width: 150px;">
                        {if isset($data['shipping_subcontractor_code'])}
                            {$data['shipping_subcontractor_code']}
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width: 100px;">
                        {if isset($data['shipping_type_name'])}
                            {$data['shipping_type_name']}
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width: 110px;">
                        {if $data['shipping_type_cd']}
                            {$data['shipping_type_cd']}
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width:160px;">
                        {if $data['set_name']}
                            {$data['set_name']}
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width:60px;">
                        {if $data['unit_price']}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {if is_numeric($data['unit_price'])}
                            <!-- tmp#6893 End -->
                                {number_format($data['unit_price'], 2, '.', ',')}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {else}
                                {$data['unit_price']}
                            {/if}
                            <!-- tmp#6893 End -->
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width:60px;">
                        {if $data['quantity']}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {if is_numeric($data['quantity'])}
                            <!-- tmp#6893 End -->
                                {number_format($data['quantity'])}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {else}
                                {$data['quantity']}
                            {/if}
                            <!-- tmp#6893 End -->
                        {/if}
                    </div>
                </td>
                <td><div style="width: 75px;">{str_replace('-', '/', $data['delivery_date'])}</div></td>
                <td>
                    <div style="width:50px;">
                        {if $data['fee']}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {if is_numeric($data['fee'])}
                            <!-- tmp#6893 End -->
                                {number_format($data['fee'])}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {else}
                                {$data['fee']}
                            {/if}
                            <!-- tmp#6893 End -->
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width:50px;">
                        {if $data['weight']}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {if is_numeric($data['weight'])}
                            <!-- tmp#6893 End -->
                                {number_format($data['weight'])}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {else}
                                {$data['weight']}
                            {/if}
                            <!-- tmp#6893 End -->
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width:100px;">
                        {if isset($data['shipments_shape_name'])}
                            {$data['shipments_shape_name']}
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width:150px;">
                        {if $data['shipments_shape_cd']}
                            {$data['shipments_shape_cd']}
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width: 75px;">
                        {if $data['summary_month']}
                            {str_replace('-', '/', $data['summary_month'])}
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width: 72px;">
                        {if $data['tax_free']}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {if is_numeric($data['tax_free'])}
                            <!-- tmp#6893 End -->
                                {number_format($data['tax_free'])}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {else}
                                {$data['tax_free']}
                            {/if}
                            <!-- tmp#6893 End -->
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width: 72px;">
                        {if $data['shipping_charge_tax']}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {if is_numeric($data['shipping_charge_tax'])}
                            <!-- tmp#6893 End -->
                                {number_format($data['shipping_charge_tax'])}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {else}
                                {$data['shipping_charge_tax']}
                            {/if}
                            <!-- tmp#6893 End -->
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width: 72px;">
                        {if $data['stamp_tax']}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {if is_numeric($data['stamp_tax'])}
                            <!-- tmp#6893 End -->
                                {number_format($data['stamp_tax'])}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {else}
                                {$data['stamp_tax']}
                            {/if}
                            <!-- tmp#6893 End -->
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width: 72px;">
                        {if $data['etc_tax']}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {if is_numeric($data['etc_tax'])}
                            <!-- tmp#6893 End -->
                                {number_format($data['etc_tax'])}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {else}
                                {$data['etc_tax']}
                            {/if}
                            <!-- tmp#6893 End -->
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width: 72px;">
                        {if $data['shipping_charge']}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {if is_numeric($data['shipping_charge'])}
                            <!-- tmp#6893 End -->
                                {number_format($data['shipping_charge'])}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {else}
                                {$data['shipping_charge']}
                            {/if}
                            <!-- tmp#6893 End -->
                        {/if}
                    </div>
                </td>
                <td>
                    <div style="width: 72px;">
                        {if $data['etc']}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {if is_numeric($data['etc'])}
                            <!-- tmp#6893 End -->
                                {number_format($data['etc'])}
                            <!-- tmp#6893 Start:2015/09/28 -->
                            {else}
                                {$data['etc']}
                            {/if}
                            <!-- tmp#6893 End -->
                        {/if}
                    </div>
                </td>
            </tr>
        {/foreach}
            <tr>
                <td colspan="32">
                    <form method="post">
                        {if !$has_error}
                        <textarea name="order_acceptances" style="display:none">{json_encode($datas_import)}</textarea>
                        <button type="submit" class="btn-small btn-green">{lang('btn_import')}</button>
                        {/if}
                        <button type="button" class="btn-small btn-green btn-close" onclick="$(this).dialog('close')">{lang('btn_cancel')}</button>
                    </form>

                </td>
            </tr>
            </tbody>
    </table>
</div>

{literal}
    <script>
        $(".btn-close").on('click', function(event) {
            $("#dialog-import-result").html("");
            $("#dialog-import-result").dialog("close");
        });

        // fixed thead
        $('.table-header-fixed').fixed_thead({
            maxHeight: 400
        });
    </script>
{/literal}

<style>
.ui-dialog .ui-dialog-content {
    margin: .5em 1em;
    overflow: hidden;
    padding: 0
}
</style>
