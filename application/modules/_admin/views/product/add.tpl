<div>
    <div id="message" class="validation-message"></div>
    
    <table class="table-simple">
        <tr>
            <td>{lang('name_product')}<span class="red-symbol">※</span></td>
            <td>
                <input type="text" name="product_name" id="product_name" maxlength="{$product_name_maxlenght}"/> <br />
                <div id="err_product_name" class="validation-message"></div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="float: left;">
                    <input type="hidden" value="" name="a_id" />
                    <button name="save_add" id="save_add" value=""
                        class="btn-small btn-green btn-save">{lang('common_btn_add')}</button>
                </div>
                <div style="float: left;">
                    <form method="POST" action="/_admin/product/show" id="close_form">
                        {make_search_hidden(FALSE)}
                        <button type="submit" class="btn-small btn-green btn-close"
                            value="Close">{lang('common_lbl_close')}</button>
                    </form>
                </div>
            </td>
        </tr>
    </table>
    {make_search_hidden()}
</div>
    
{literal}
    <script>
        $('#save_add').on('click', function() {
            $.post("/_admin/product/add/", {
                    save_add: "save_add",
                    name: $("#product_name").val(),
                    redirect_form_post: redirect_data()
                }, 
                function( data ) {
                    $(".validation-message").empty();

                    if (!data.status) {
                        if (data.message) {
                            $("#message").html("<div class='error-message blink_me'>" + data.message + "</div>");
                        } else if (data.messages.name) {
                            $("#err_product_name").html(data.messages.name);
                        } else if (data.messages.product_manage_code) {
                            $("#message").html("<div class='error-message blink_me'>" + data.messages.product_manage_code + "</div>");
                        }
                    } else {
                        form_changed = false;
                        form_submitted = true;

                        $("#message").html("<div class='success-message blink_me'>" + data.message + "</div>");

                        // Reset data
                        $('#detail_product input').val('');
                    }
                },
            "json");
        });

        $("#close_form").submit(function(){
            $("#detail_product").dialog("close");
                
            // if not change on form then submit
            if (form_submitted && !form_changed) {
                return true;
            } else {
                return false;
            }
        });

        var bind_add_event = function(e) {
            if (e.ctrlKey && (e.which == 83)) {
                e.preventDefault();
                $("#save_add").trigger('click');
                return false;
            }
        };
        $(document).bind('keydown', bind_add_event);

        exit_confirm();
    </script>
{/literal}
