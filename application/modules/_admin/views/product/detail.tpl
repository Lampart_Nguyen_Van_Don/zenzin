<div>
    <hr class="blank-10px" />
    <div id="message" class="alert-error"></div>
    {if isset($product)}
    <table class="table-simple">
        <tr>
            <td>{lang('code')}</td>
            <td>{$product.code}</td>
        </tr>
        <tr>
            <td>{lang('name_product')}</td>
            <td>{htmlspecialchars ($product.name)}</td>
        </tr>
        <tr>
            <td>{lang('is_no_add_in_edit')}</td>
            <td>{if ($product.is_no_add)} <input type="checkbox" disabled="disabled" checked> {lang('is_no_add_flag')} {else}
                <input disabled="disabled" type="checkbox" > {lang('is_no_add_flag')}{/if}</td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="float_left">
                    <input type="hidden" value="" name="a_id" />
                    {if $update_delete_permission == true}
                    <button name="to_edit_page" id="to_edit_page" value="{$product.id}"
                        class="btn-small btn-green btn-edit">{lang('common_lbl_edit')}</button>
                    {/if}
                </div>
                <div class="float_left">
                    <button name="close_dialog" id="close_dialog"
                        class="btn-small btn-green btn-close" value="Close">{lang('common_lbl_close')}
                    </button>
                </div>
            </td>
        </tr>
    </table>
    {else}
    <div class="error-message">{lang('common_record_not_exist')}</div>
    {/if} {make_search_hidden()}
</div>
{literal}
<script>
     $(".btn-edit").on('click', function() {
        $.ajax({
            type: "POST",
            url: "/_admin/product/edit",
            data: {
                  id_product: this.value,
                  to_edit_page: "to_edit_page",
                  redirect_form_post:redirect_data()
            }
        })
        .done(function(data) {
            $("#detail_product").dialog({
                title: $( "#edit_product" ).val(),
                width: 'auto',
                height: 'auto',
                modal : true,
            });
            $("#detail_product").html(data);
            $('#detail_product').dialog("open");
        });
      });
</script>
{/literal}
