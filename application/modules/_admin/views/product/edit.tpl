<div>
    <div id="message" class="validation-message"></div>
    
    {if isset($product)}
        <table class="table-simple">
            <tr>
                <td>{lang('code')}</td>
                <td>{$product.code}</td>
            </tr>
            <tr>
                <td>{lang('name_product')}<span class="red-symbol">※</span></td>
                <td>
                    <input type="text" name="product_name" id="product_name"
                           value="{htmlspecialchars ($product.name)}" maxlength="{$product_name_maxlenght}" /> <br />
                    <div id="err_product_name" class="validation-message"></div>
                </td>
            </tr>
            <tr>
                <td>{lang('is_no_add_in_edit')}</td>
                <td>{if ($product.is_no_add)} <input type="checkbox"
                    name="product_is_no_add" id="product_is_no_add" value="1" checked />
                    <label for="product_is_no_add">{lang('is_no_add_flag')}</label>
                    {else} <input type="checkbox" name="product_is_no_add"
                    id="product_is_no_add" value="0" /> <label for="product_is_no_add">{lang('is_no_add_flag')}</label>
                    {/if}
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="float_left">
                        <input type="hidden" value="" name="a_id" />
                        <button name="save_edit" id="save_edit" value="{$product.id}"
                            class="btn-small btn-green btn-save">{lang('common_lbl_save')}</button>
                    </div>
                    <div style="float: left;">
                        <form method="POST" action="/_admin/product/show" id="close_form">
                            {make_search_hidden(FALSE)}
                            <button type="submit" class="btn-small btn-green btn-close"
                                value="Close">{lang('common_lbl_close')}</button>
                        </form>
                    </div>
                    <div class="float_left">
                        <form method="POST" id="delete_form" action="/_admin/product/show">
                            {make_search_hidden(FALSE)}</form>
                        <button name="delete_product" id="delete_product" type="submit"
                            class="btn-small btn-green btn-delete" value="{$product.id}">{lang('common_lbl_delete')}</button>
                    </div>
                </td>
            </tr>
        </table>
    {else}
        <div class="error-message">{lang('common_record_not_exist')}</div>
    {/if} {make_search_hidden()}
</div>
        
{literal}
    <script>
        $(".btn-save").on('click', function() {
            $.post("/_admin/product/edit/", {
                product_id: this.value,
                save_edit: "save_edit",
                name: $("#product_name").val(),
                product_is_no_add: $("#product_is_no_add").val(),
                redirect_form_post: redirect_data()
            },
            function( data ) {
                $(".validation-message").empty();

                if (!data.status) {
                    if (data.message) {
                        $("#message").html("<div class='error-message blink_me'>" + data.message + "</div>");
                    } else if (data.messages.name) {
                        $("#err_product_name").html(data.messages.name);
                    } else if (data.messages.product_manage_code) {
                        $("#message").html("<div class='error-message blink_me'>" + data.messages.product_manage_code + "</div>");
                    }
                } else {
                    form_changed = false;
                    form_submitted = true;

                    $("#message").html("<div class='success-message blink_me'>" + data.message + "</div>");
                }
            },
            "json");
        });

        $(document).on('click', '#product_is_no_add', function() {
            $(this).attr('value', this.checked ? 1 : 0);
        });

        $("#close_form").submit(function(){
            $("#detail_product").dialog("close");
            if( form_submitted && !form_changed ) {
                return true;
            } else {
                return false;
            }
        });

        var bind_add_event = function(e) {
            if (e.ctrlKey && (e.which == 83)) {
                e.preventDefault();
                $("#save_edit").trigger('click');
                return false;
            }
        };
        $(document).bind('keydown', bind_add_event);

        exit_confirm();
    </script>
{/literal}
