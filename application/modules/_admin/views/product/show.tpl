<!-- Show list of products -->
{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>
        <hr class="blank-10px" />
        <table class="table-theme odd">
            {if isset($list_product)}
            <tr>
                <th>{lang('common_lbl_action')}</th>
                <th>{lang('code')}</th>
                <th>{lang('name_product')}</th>
                <th>{lang('is_no_add')}</th>
            </tr>
            {foreach from=$list_product item=p}
            <tr>
                <td>
                    <button name="detail" type="submit" id="detail" value="{$p.id}"
                            class="btn-small btn-green btn-view">{lang('common_lbl_view')}</button>
                </td>
                <td>{$p.code}</td>
                <td>
                    {htmlspecialchars ($p.name)}
                    <!-- <a href="javascript:void()" data-transition="none" data-seq="{$p.id}" id="view_detail"></a>
                     -->
                </td>
                <td>
                    {$p.is_no_add_name}
                </td>
            </tr>
            {/foreach} {else}
            <tr>
                <th>{lang('common_lbl_action')}</th>
                <th>{lang('code')}</th>
                <th>
                    {lang('name_product')}
                </th>
                <th>{lang('is_no_add')}</th>
            </tr>
            <tr>
                <td colspan="5">{lang('common_lbl_no_data')}</td>
            </tr>
            {/if}
            {if $add_permission == true}
            <tr>
                <td colspan="4">
                    <button name="add" id="add" value="add" class="btn-small btn-green btn-add">{lang('common_btn_add')}</button>
                </td>
            </tr>
            {/if}
        </table>
        {make_search_hidden()}
        <hr class="blank-10px" />
        {if $pagination} {$pagination.links} {/if}
    </div>
    <div id="detail_product" class="dialog_form"></div>
    <div id="delete_confirm"></div>
    <!-- Message -->
    <input type="hidden" id="common_insert_successfully" value="{lang('common_insert_successfully')}"/>
    <input type="hidden" id="edit_product" value="{lang('edit_product')}"/>
    <input type="hidden" id="common_edit_successfully" value="{lang('common_edit_successfully')}"/>
    <input type="hidden" id="product_information" value="{lang('product_information')}"/>
    <input type="hidden" id="add_new_product" value="{lang('add_new_product')}"/>
    <input type="hidden" id="message_sure_to_delete" value="{lang('message_sure_to_delete')}"/>
</div>
{literal}
<script>
$(document).ready(function() {
    $(function() {
        $("#detail_product").dialog({
            autoOpen : false,
            width : "auto",
            height : "auto",
            modal : true,
            open: function(){
                jQuery('.ui-widget-overlay').bind('click',function(){
                    jQuery('#dialog').dialog('close');
                })
            },
            beforeClose: function( event, ui ) {
                var close = close_confirm();
                return close;
            },
            buttons : {},
            close : function() {
            }
        });
    });

    // Close dialog when click outside
    $("body").on('click', '.close-dialog', function(event) {
        $(".ui-dialog-content").dialog("close");
        $("#dialog-edit-content").html('');
    });

    $("body").on('click', '.ui-dialog-titlebar-close', function(event) {
        if( form_submitted && !form_changed) {
            $("#close_form").submit();
        }
    });
    // Load dialogbox when click button detail
    $(document).on("click", "#detail", function() {
        var id = this.value;
        show_detail_product(id);
    });

    // Load dialogbox when click name of the product => detail
    $(document).on("click", "#view_detail", function() {
        var id = $(this).data('seq');
        show_detail_product(id);
    });

    // Show dialogbox detail product when click button or link name
    function show_detail_product(id) {
        $('#detail_product').load("/_admin/product/detail", {
            id_product : id,
            redirect_form_post: redirect_data()
        }, function() {
            $('#detail_product').dialog('option', 'title', $('#product_information').val());
            $("#detail_product").dialog( "option", "height", "auto" );
            $("#detail_product").dialog( "option", "width", "auto" );
            $('#detail_product').dialog("open");
        });
    }

     // Load dialogbox when click add
    $(document).on("click", "#add", function() {
        $('#detail_product').load("/_admin/product/add", {
            id_product : this.value,
            to_add_page: "to_add_page",
            redirect_form_post: redirect_data()
        }, function() {
            $('#detail_product').dialog('option', 'title', $('#add_new_product').val());
            $("#detail_product").dialog( "option", "height", "auto" );
            $("#detail_product").dialog( "option", "width", "auto" );
            $('#detail_product').dialog("open");
        });
    });

     // Close dialog
    $(document).on('click', '#close_dialog', function() {
               $("#detail_product").dialog("close");
               return false;
    });

     // Click delete
    $(document).on('click', '#delete_product', function(event) {
        event.preventDefault();
        var id = $(this).val();
        $("#delete_confirm").html(lang['message_confirm_delete']).dialog({
            title: lang['title_confirm'],
            width: 'auto',
            height: 'auto',
            modal : true,
            buttons: {
                "はい": function() {
                    $(this).dialog('close');
                    delete_product(id);
                },
                "いいえ": function () {
                    $(this).dialog('close');
                }
            }
        });
    });

    function delete_product(id) {
        url = "/_admin/product/delete/";
        $.post( url,
                {
                    product_id: id
                },
                function( data ) {
                    // If has error messages
                    if (data["error"] == 1) {
                        $("#message").html('<div class="error-message">'+data["error_message"]+'</div>');
                    } else {
                        $("#delete_form").submit();
                        $("#detail_product").dialog("close");
                        // window.location.reload();
                    }
                },
                "json");
    }
});
</script>
{/literal}
{include file = "inc/footer.tpl"}