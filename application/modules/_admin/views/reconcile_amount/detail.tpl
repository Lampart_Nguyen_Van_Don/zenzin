


<style>
#client-table td {
    min-width: 75px;
}
#dialog-detail{
  min-width: 600px;
}
.ui-dialog-content a {
    color: #fff;
}
#view-table td:last-child {
    min-width: 600px;
}
.lbl_s_code {
    display: inline-block;
}
.value_s_code {
    display: inline-block;
}
.lbl_name {
    display: inline-block;
}
.value_name {
    display: inline-block;
}
.lbl_total {
    display: inline-block;
}
.value_total {
    display: inline-block;
}

.divide_money_group {
    margin-top: 0px;
    border-color: grey;
    border: 1px solid;
    padding-left: 2%;
}

.lbl_input {
    display: inline-block;
    padding-right: 2%;
    padding-top: 3%;
    width: 120px;
    position: relative;
}

.input_value {
    display: inline-block;
    padding-left: 2%;
    max-width: 100%;
    position: relative;
    margin-top: 20px;
}

.b_devide_money, .b_exit_screen {
    color: #222222 !important;
    text-decoration: none;
}

.b_u_money {
    color: #222222 !important;
    text-decoration: none;
}

.divide_money {
    margin-bottom: 2%;
}

.height_30 {
    height: 30px;
}
.message
{
    margin-top: 10px;
}

</style>
 
<div class="group_s_code">
    <div class="lbl_s_code">{lang('lbl_s_code')}:</div>
    <div class="value_s_code">{$client_s_code[0]['s_code']}</div>
</div>

<div class="group_name">
    <div class="lbl_name">{lang('lbl_name')}:</div>
    <div class="value_name">
    {$this->client_model->name_with_title($client_s_code[0]['name'], $client_s_code[0]['corporate_status_before'],$client_s_code[0]['corporate_status_after'])}</div>
</div>

<div class="" style="overflow-x: auto ; width=100%">
    <div class="message"></div><br>
    <table id="client-table" class="table-theme odd client_owner">
        <thead>
            <tr>
                <th>{lang('lbl_date_return_money')}</th>
                <th>{lang('lbl_name_bank')}</th>
                <th>{lang('lbl_money_recived')}</th>
                <th>{lang('lbl_comment')}</th>
            </tr>
        </thead>
        <tbody>
            {if empty($payment_data)}
            <tr><td class="a-center" colspan="11" style="width: 100%">{lang('common_msg_no_data')}</td></tr>
            {/if}
            {foreach $payment_data as $payment}
            <tr>
                <td>{date_format(date_create($payment.payment_date),'Y/m/d')}</td>
                <td>{$payment.account_holder}</td>
                <td align="right">
                    <font style="{if $payment.amount < 0} color:#EC1C1C {/if}">{number_format($payment.amount)}</font>
                </td>
                <td style="min-width: 400px">{$payment.comment}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>

    <div class="group_total"> 
        <div class="lbl_total">入金額合計:</div>
        <div class="value_total">
            {if !empty($total_money)}
                {number_format($total_money[0]['payment_amount'])}
            {else}
                0
            {/if}
        </div>
        <input type="hidden" id="total_money_all" value="{if !empty($total_money)}{$total_money[0]['payment_amount']}{else}0{/if}">
       
    </div>
    <div class="height_30"></div>
    <div class="state_detail">
         <div class="lbl_name">入金分割</div>
    {form_open('', ['id' => 'divide_money'], ['client_id' => $id])}
    <div class="divide_money_group">
        <div class="divide_money">
            <div class="divide_s_code_group">
                <div class="lbl_input">
                    <span>{lang('lbl_s_code')}</span>
                    <span class="red-symbol">※</span>
                </div>

                <div class="input_value">
                    {form_dropdown([
                        'name' => 'in_s_code',
                        'items' => ['' => lang('common_select')] + $s_code_client ,
                        'value' => $s_code_client
                    ])}
                    <div class="error"></div>
                </div>
            </div>

            <div class="divide_s_code_group">
                <div class="lbl_input">
                    <span>{lang('lbl_divide_money')}</span>
                    <span class="red-symbol">※</span>
                </div>
                <div class="input_value">
                    <input type="text" name="input_d_money" value="" id="input_d_money" style="width:150px">
                    <div class="error"></div>
                </div>
            </div>

            <div class="divide_before_comment_group">
                <div class="lbl_input">
                    <span>{lang('lbl_divide_before_comment')}</span>
                    <span class="red-symbol">※</span></div>
                <div class="input_value">
                    <input type="text" name="input_b_d_comment" value="" id="input_d_comment" style="width:150px">
                    <div class="error"></div>
                </div>
            </div>

            <div class="divide_comment_group">
                <div class="lbl_input">
                    <span>{lang('lbl_divide_after_comment')}</span>
                    <span class="red-symbol">※</span>
                </div>
                    <div class="input_value">
                        <input type="text" name="input_a_d_comment" value="" id="input_a_d_comment" style="width:150px">
                    <div class="error"></div>
                </div>
            </div>

            <div class="divide_button_group">
                <div class="lbl_input"><a class="b_devide_money btn-green btn-small">{lang('lbl_b_devide')}</a></div>
            </div>
        </div>
    </div>
    {form_close()}
    <div class="height_30"></div>
    <div class="lbl_name">入金調整</div>
    {form_open('', ['id' => 'up_divide_money'], ['client_id' => $id])}
    <div class="divide_money_group">
        <div class="divide_money">
            <div class="divide_s_code_group">
                <div class="divide_s_code_group">
                    <div class="lbl_input">
                        <span>{lang('lbl_divide_money')}</span>
                        <span class="red-symbol">※</span>
                    </div>
                    <div class="input_value">
                        <input type="text" name="input_u_money" value="" id="input_u_money" style="width:150px">
                        <div class="error"></div>
                    </div>
                </div>

                <div class="divide_before_comment_group">
                    <div class="lbl_input">
                        <span>{lang('lbl_u_comment')}</span>
                        <span class="red-symbol">※</span>
                    </div>
                    <div class="input_value">
                        <input type="text" name="input_u_comment" value="" id="input_u_comment" style="width:150px">
                        <div class="error"></div>
                    </div>
                </div>

                <div class="divide_button_group">
                    <div class="lbl_input"><a class="b_u_money btn-green btn-small">{lang('lbl_u_update')}</a></div>
                </div>
            </div>
        </div>
    </div>
    {form_close()}
    </div>
   
</div>

<div class="lbl_input">
    <a class="b_exit_screen btn-green btn-small btn-close">{lang('common_lbl_close')}</a>
</div>


<script>
$(document).ready(function() {
    var status_detail = {$status_detail}; 
  
    if(status_detail == false)
    {
        $(".state_detail").css("display","none");
    }
    else
    {
        $(".state_detail").css("display","block");
    }

    $(".b_devide_money").click(function() {
        $(".message").html('');
        $(".error").html('');
        $('.message').removeClass('caution');
        $('.message').removeClass('success-message blink_me');
        var s_code_client =  $("#in_s_code").val();
        var d_money = $("#input_d_money").val().replace(/,/g,'');
       
        var b_comment = $("#input_d_comment").val();
        var a_comment = $("#input_a_d_comment").val();

        $.ajax({
            url: '/_admin/reconcile_amount/divide_money',
            type: 'POST',
            data: { client_id_own: "{$client_s_code[0]['id']}",in_s_code: s_code_client,input_d_money: d_money,input_b_d_comment: b_comment,input_a_d_comment: a_comment
        },
            success: function(result) {
                $(".message").text(result['message']);
              
                if(result.hasOwnProperty('errors')) {
                    if(result.hasOwnProperty('message')) {
                         $('.message').removeClass("caution");
                        $('.message').removeClass('success-message blink_me');
                        $('.message').addClass('caution');
                        $('.message').css("margin-top","10px");
                        $('.message').css("margin-bottom","10px");
                    }
                    else
                    {
                        $('.message').removeClass("caution");
                        $('.message').removeClass('success-message blink_me');
                    }

                    $(".divide_s_code_group").css("margin-top","10px");
                    $.each(result.errors, function (k, v) {
                        $('[name=' + k + ']').siblings('.error').html(
                        '<div class="validation-error">' + v + '</div>');
                    });
                }

//#9547:Start
                if (typeof result.status != 'undefined' && result.status === false) {
                    $('.message').removeClass('success-message blink_me');
                    $('.message').addClass('caution');
                    $('.message').css("margin-top","10px");
                    $('.message').css("margin-bottom","10px");
                    error_message = '';
                    $.each(result.messages, function(index, message) {
                         error_message += '<p>' + result.s_code + message + '</p>';
                    });
                    return $(".message").html(error_message);
                }
//#9547:End
                if(typeof(result['errors']) === "undefined") {
                     if(result.hasOwnProperty('message')) {
                            $('.message').removeClass('success-message blink_me');
                            $('.message').removeClass('caution');
                            $('.message').addClass('success-message blink_me');
                            $('.message').css("margin-top","10px");
                     }
                     else
                     {
                        $('.message').removeClass('success-message blink_me');
                        $('.message').removeClass('caution');

                     }
                   

                    var html ="<tr>";
                    html += "<td>" + result['month_date'] + "</td>";
                    html += "<td>" + result['account_holder'] + "</td>";
                    html += "<td  align='right'><font style='color:#EC1C1C'>" + result['input_d_money']+ "</font></td>";
                    html += "<td>" + result['input_b_d_comment'] + "</td>";
                    html += "</tr>";

                    $(".client_owner").append(html);
                    var numeric= 0;

                    $(".client_owner tbody tr").each(function(index) {
                        numeric+=  parseInt($(this).children("td").eq(2).text().replace(/,/g,""));
                        $(".value_total").text(format(numeric.toString()));
                        $("#total_money_all").val(numeric);
                    });

                }
            },
        })
    });
            
    $(".b_u_money").click(function() { 
        $(".message").html('');
        $(".error").html('');
        $('.message').removeClass('caution');
        $('.message').removeClass('success-message blink_me');
        var d_money = $("#input_u_money").val().replace(/,/g,'');
        var b_comment = $("#input_u_comment").val();

        $.ajax({
            url: '/_admin/reconcile_amount/edit_payment',
            type: 'POST',
            data: { client_id_u: "{$client_s_code[0]['id']}" ,input_u_money: d_money,input_u_comment: b_comment},
            success: function(result) {
                $(".message").text(result['message']);
                if(result.hasOwnProperty('errors')) {
                    if(result.hasOwnProperty('message')) {
                        $('.message').removeClass("caution");
                        $('.message').removeClass('success-message blink_me');
                        $('.message').addClass('caution');
                        $('.message').css('margin-bottom',"10px");
                    }
                    else
                    {
                          $('.message').removeClass("caution");
                          $('.message').removeClass('success-message blink_me');
                    }

                    $(".divide_s_code_group").css("margin-top","10px");

                    $.each(result.errors, function (k, v) {
                        $('[name=' + k + ']').siblings('.error').html(
                        '<div class="validation-error">' + v + '</div>');
                    });
                }

//#9547:Start
                if (typeof result.status != 'undefined' && result.status === false) {
                    $('.message').removeClass("caution");
                    $('.message').removeClass('success-message blink_me');
                    $('.message').addClass('caution');
                    $('.message').css('margin-bottom',"10px");
                    error_message = '';
                    $.each(result.messages, function(index, message) {
                         error_message += '<p>' + message + '</p>';
                    });
                    return $(".message").html(error_message);
                }
//#9547:End
               
                if(typeof(result['errors']) === "undefined") {

                     $(".client_owner tbody tr").each(function(index)
                        {
                            if($(this).children("td").text() == "{lang('common_msg_no_data')}")
                            {
                                 $(".client_owner tbody tr").remove();
                            }
                        });
                     if(result.hasOwnProperty('message')) {
                                 $('.message').removeClass('caution');
                                 $('.message').removeClass('success-message blink_me');
                                $('.message').addClass('success-message blink_me');
                     }
                     else
                     {
                           $('.message').removeClass('caution');
                           $('.message').removeClass('success-message blink_me');
                     }

                   

                    var html ="<tr>";
                    html += "<td>" + result['month_date'] + "</td>";
                    html += "<td>" + result['account_holder'] + "</td>";
                    html += "<td align='right'><font style='color:#EC1C1C'>" + result['input_d_money'] + "</font></td>";
                    html += "<td>" + result['input_u_comment'] + "</td>";

                    var numeric= 0;
                    html += "</tr>";
                    $(".client_owner").append(html);
                    $(".client_owner tbody tr").each(function(index) {
                        numeric+=  parseInt($(this).children("td").eq(2).text().replace(/,/g,""));
                        $(".value_total").text(format(numeric.toString()));
                        $("#total_money_all").val(numeric);
                    });
                }
            },
        })
    });
    
  
    $(".b_exit_screen").click(function() {
        $("#dialog-detail").dialog("close");
    });
  


    var format = function(num){
        var str = num.toString().replace("$", ""), 
            parts = false, output = [], 
            i = 1,
            formatted = null;
        if(str.indexOf(".") > 0) {
            parts = str.split(".");
            str = parts[0];
        }
        str = str.split("").reverse();
        for(var j = 0, len = str.length; j < len; j++) {
            if(str[j] != ",") {
                output.push(str[j]);
                if(i%3 == 0 && j < (len - 1)) {
                    output.push(",");
                }
                i++;
            }
        }
        formatted = output.reverse().join("");
        return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
    }

    $(function(){
        $("#input_d_money").keyup(function(e){
//#9588:Start
//            $(this).val(format($(this).val()));
            if(e.which >= 37 && e.which <= 40) return;
            $(this).val(addCommas($(this).val().replace(/,/g, '')));
//#9588:End
        });
    });

    $(function(){
        $("#input_u_money").keyup(function(e){
//#9588:Start
//            $(this).val(format($(this).val()));
            if(e.which >= 37 && e.which <= 40) return;
            $(this).val(addCommas($(this).val().replace(/,/g, '')));
//#9588:End
        });
    });

    });
</script>


 