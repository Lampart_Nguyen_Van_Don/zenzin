{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>
        <hr class="blank-10px" />
        <div class="import-container">
            <br>
            <form id="form-import" action="/_admin/reconcile_amount/import" method="post" enctype="multipart/form-data">
                <input id="upload-unique-id" name="unique_id" type="hidden">
                <div id="error-container">
                    {if isset($upload_status)}
                        <div class='error-message'>{lang('lbl_import_error')}</div>
                    {/if}
                </div>
                <table class="table-import">
                    <tr>
                        <td width="120">{lang('lbl_file_import')}</td>
                        <td width="700"><div id="file-path"></div></td>
                        <td width="190" align="center">
                            <div class="input-wrapper">
                                {lang('btn_browse')}
                                <input type="file" name="file_reconcile_import" id="file_reconcile_import"  class="file-input"  accept=".xls,.xlsx"/>
                            </div>
                            <span class="red-symbol">{lang('lbl_file_limit_size')}</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="import-propress-bar">
                                <div class="progress-label"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                             <button type="submit" class="btn-small btn-green" value="file-upload"
                            name="file-upload" id="file-upload-submit">{lang('lbl_import_and_process')}</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

<div id="dialog-import-result" style="display:none"></div>

<style type="text/css">
    .input-wrapper {
      overflow: hidden;
      position: relative;
      cursor: pointer;
      background-color: #1EB9E7;
      color: #FFF;
      display: inline-block;
      padding: 5px 10px 4px 10px;
      border-radius: 4px;
      font-family: 'メイリオ','.HiraKakuInterface-W1', Arial, Helvetica, sans-serif;
      line-height: 1;
      vertical-align: middle;
    }
    #file-path {
      border: 1px solid #aaaaaa;
      width: 100%;
      height: 30px;
      line-height: 30px;
      border-radius: 4px;
      padding-left: 10px;
    }
    .file-input {
      cursor: pointer;
      height: 100%;
      position: absolute;
      top: 0;
      right: 0;
      z-index: 99;
      font-size: 50px;
      opacity: 0;
      -moz-opacity: 0;
      filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);
    }
    .red-symbol {
      color: red;
      font-weight: bold;
    }
    .ui-autocomplete{
        z-index: 9999999999999!important;
    }

    #file-upload-submit {
        margin: 10px;
    }

    @-moz-document url-prefix() {
         .input-wrapper {
            line-height: 1.4;
         }
    }

</style>

<script>
    label = {
        please_select_file: "{lang('lbl_please_select_file')}",
        unsupported_file_type: "{lang('lbl_unsupported_file')}",
        loading: "{lang('lbl_loading')}",
        completed: "{lang('lbl_complete')}",
        over_size: "{lang('lbl_over_size')}",
        btn_import: "{lang('btn_import')}",
        s_code_is_required: "{lang('error_s_code_is_required')}",
        s_code_is_not_exist: "{lang('error_s_code_is_not_exist')}"
    };
    
</script>

{literal}
<script type="text/javascript">
$(document).ready(function() {
    var progress_status = false;
    var xhr;

    $("#file_reconcile_import").on('change', function(event) {
        path = $("#file_reconcile_import").val();
        pos  = path.lastIndexOf('\\');
        $("#file-path").text(path.substring(pos+1));
    });

    $(".import-propress-bar").toggle();
    $(".import-propress-bar").progressbar({
        value: false,
        disabled: true,
        change: function() {
            $(".progress-label").text($(".import-propress-bar").progressbar( "value" ) + "%");
        },
        complete: function() {
            // $(".progress-label").text(label.completed);
        }
    });

    $('#form-import').submit(function(event) {
        event.preventDefault();

        progress_status = false;

        $('#error-container').html('');
        $("#file-upload-submit").attr('disabled', 'disabled');

        $(".import-propress-bar").progressbar({
            value: 0
        });

        file = $("#file_reconcile_import").val();
        if (!file) {
            $('#error-container').html("<div class='error-message'>" + label.please_select_file + "</div>");
            $("#file-upload-submit").removeAttr('disabled');
            return false; 
        }

        var unique_id = generate_unique_id();
        $("#upload-unique-id").val(unique_id);

        $(this).ajaxSubmit({
            target:   '#dialog-import-result',
            beforeSubmit: before_submit,
            success: after_success,
            uploadProgress: on_progress,
            resetForm: true
        });

        var get_progress = function() {
            xhr = $.getJSON("/_admin/reconcile_amount/ajax_get_progress", {unique_id: unique_id}, function(data) {
                var cur_progress = $(".import-propress-bar").progressbar("value");
                if (data.status <= 100 && data.status > 0 && cur_progress != 100) {
                    var status = data.status >= 100 ? 99 : data.status;
                    $(".import-propress-bar").progressbar({
                        value: status,
                    });
                }
                if (!progress_status) get_progress();
            });
        }

        get_progress();
    });

    var generate_unique_id = function() {;
        rand = Math.floor(Math.random() * 26) + Date.now();
        return rand;
    };

    var before_submit = function() {
        $('#dialog-import-result').html('');
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            // file_type = $('#file_reconcile_import')[0].files[0].type;
            // file_size = $('#file_reconcile_import')[0].files[0].size;
            
            // switch(file_type) {
            //     case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            //         break;
            //     case 'application/vnd.ms-excel':
            //         break;
            //     default:
            //         $("#error-container").html("<div class='error-message'>" + label.unsupported_file_type + "</div>");
            //         $("#file-upload-submit").removeAttr('disabled');
            //         return false
            // }

            // // > 10MB
            // if(file_size > 10485760) {
            //     $("#error-container").html("<div class='error-message'>" + label.over_size + "</div>");
            //     $("#file-upload-submit").removeAttr('disabled');
            //     return false;
            // }

        } else {
            $("#file-upload-submit").removeAttr('disabled');
            return false;
        }
        $(".import-propress-bar").toggle();
    };

    var after_success = function(){
        $("#file-path").text('');
        xhr.abort();
        try {
            progress_status = true;

            upload_result = $.parseJSON($("#dialog-import-result").text());
            $("#dialog-import-result").html(upload_result.messages);
            if (typeof upload_result == 'object' && !upload_result.status) {
                $('#error-container').html("<div class='error-message'>" + upload_result.messages + "</div>");
            } else {
                $("#dialog-import-result").dialog({
                    title: '確認',
                    width: 'auto',
                    height: 'auto',
                    modal: true,
                    maxHeight: 500,
                    position: {
                        my: "center", 
                        at: "center", 
                        of: window
                    },
                    show: {
                        duration: 200,
                        delay: 200,
                    },
                    open: function(event, ui) {
                        $("#dialog-import-result").html(upload_result.messages);
                        $(".import-propress-bar").progressbar({
                            value: 100
                        });                        
                    },
                    beforeClose: function( event, ui ) {
                        $(".import-propress-bar").toggle();
                        $("#file-upload-submit").removeAttr('disabled');
                    }
                });
            }
        } catch (error) {
            console.log(error);
            $(".import-propress-bar").toggle();
            $("#file-upload-submit").removeAttr('disabled');
        }
    };

    var on_progress = function(event, position, total, percentComplete) {
        // $(".import-propress-bar").progressbar({
        //     value: percentComplete
        // });
    }
});
</script>
{/literal}
{include file = "inc/footer.tpl"}
