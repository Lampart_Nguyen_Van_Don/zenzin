{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>
        <form id="search-form" action="/_admin/reconcile_amount/import_clearance" method="post">
            <table class="table-simple search-reconcile-table">
                <tr>
                    <th>{lang('lbl_summary_month')}</th>
                    <td>
                        <select name="summary_month" id="cbx-summary-month">
                            {foreach from=$pulldown item=d}
                                {if $d == $summary_month}
                                     <option selected value="{$d}">{$d}</option>
                                {else}
                                     <option value="{$d}">{$d}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>{lang('lbl_remaining_scrubbing')}</th>
                    <td><label><input type="checkbox" name="remaining_scrub" value="1" id="ip-remaining-scrub">{lang('lbl_remaining_scrubbing_show')}</label></td>
                </tr>
                <tr>
                    <th>{lang('lbl_s_code')}</th>
                    <td>
                        <input class="span12" type="text" id="ip-s-code" name="s_code" value="{if isset($form.s_code)}{$form.s_code}{/if}">
                        <p class="search-tooltip">{lang('lbl_s_code_tooltip')}</p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <button type="button" class="btn-small btn-green btn-search m-height">{lang('common_lbl_show')}</button>
<!-- #9694:Start -->
<!--                         <a href="/_admin/reconcile_amount/import"> -->
<!--                             <button type="button" class="btn-small btn-green m-height">{lang('btn_import')}</button> -->
                        <a href="/_admin/reconcile_amount/import" class="btn-small btn-green m-height" style="line-height: 15px">
                            {lang('btn_import')}
                        </a>
<!-- #9694:End -->
                    </td>
                </tr>
            </table>
        </form>
        
        <div class="btn-wrapper">
            <p class="btn-blance-wrapper">
                <button type="button" class="btn-small btn-green btn-1 m-height">{lang('btn_show_1')}</button>
            </p>
            <p class="btn-blance-wrapper">
                <button type="button" class="btn-small btn-green btn-2 m-height">{lang('btn_show_2')}</button>
            </p>
            <p class="btn-blance-wrapper">
                <button type="button" class="btn-small btn-green btn-3 m-height">{lang('btn_show_3')}</button>
            </p>
            
            <span class="show-field-btn-wrapper">
                <button type="button" class="btn-small btn-green show-etc m-height">{lang('btn_show_etc')}</button>

                <button type="button" class="btn-small btn-green show-miscellaneous-loss m-height">{lang('btn_show_miscellaneous_loss')}</button>

                <button type="button" class="btn-small btn-green show-miscellaneous-income m-height">{lang('btn_show_miscellaneous_income')}</button>
            </span>

        </div>

        <hr class="blank-10px" style="clear:both" />
        <div class="messages"></div>
        <div id="reconcile_table" class="grid"></div>
        <hr class="blank-10px" />
        <button type="button" class="btn-small btn-green btn-save">{lang('common_lbl_save')}</button>
        <button type="button" class="btn-small btn-green btn-download-csv">{lang('btn_download_csv')}</button>
        
        
    </div>
</div>
<div id="dialog-error" style="display:none"></div>
<div id="dialog-detail" style="display:none"></div>

<script src="/public/admin/reconcile_amount/app.js" type="text/javascript" charset="utf-8"></script>
<script src="/public/admin/js/handsontable.full.min.js" type="text/javascript" charset="utf-8"></script>
<script>
    
    var summary_month = "{$summary_month}";
    var label = {
        month: parseInt("{substr($summary_month, 5, 2)}"),
        year: parseInt("{substr($summary_month, 0, 4)}"),
        prev_month: "{lang('lbl_previous_month')}",
        cur_month: "{lang('lbl_current_month')}",
        next_month: "{lang('lbl_next_month')}",
        s_code: "{lang('lbl_s_code')}",
        client_name: "{lang('lbl_client_name')}",
        sales_slip_number: "{lang('lbl_sales_slip_number')}",
        sales_slip_amount: "{lang('lbl_sales_slip_amount')}",
        prev_receive_payment: "{lang('lbl_previous_receive_payment')}",
        payment_amount: "{lang('lbl_current_receive_payment')}",
        etc: "{lang('lbl_etc')}",
        fee: "{lang('lbl_fee')}",
        repayment: "{lang('lbl_repayment')}",
        miscellaneous_loss: "{lang('lbl_miscellaneous_loss')}",
        miscellaneous_income: "{lang('lbl_miscellaneous_income')}",
        offset_next_month_receive: "{lang('lbl_offset_next_month_receive')}",
        offset_next_month_ad_receive: "{lang('lbl_offset_next_month_ad_receive')}",
        bugyo_next_month_receive: "{lang('lbl_bugyo_next_month_receive')}",
        bugyo_next_month_ad_receive: "{lang('lbl_bugyo_next_month_ad_receive')}",
        total_previous_receive_payment: "{lang('lbl_total_previous_receive_payment')}",
        total_current_receive_payment: "{lang('lbl_total_current_receive_payment')}",
        total_offset_next_month_receive: "{lang('lbl_total_offset_next_month_receive')}",
        total_offset_next_month_ad_receive: "{lang('lbl_total_offset_next_month_ad_receive')}",
        total_bugyo_next_month_receive: "{lang('lbl_total_bugyo_next_month_receive')}",
        total_bugyo_next_month_ad_receive: "{lang('lbl_total_bugyo_next_month_ad_receive')}",
        no_data: "{lang('common_lbl_no_data')}",
        can_not_blance: "{lang('error_can_not_blance')}",
//#9696:Start
        total_etc: "{lang('lbl_total_etc')}",
        total_fee: "{lang('lbl_total_fee')}",
        total_repayment: "{lang('lbl_total_repayment')}",
        total_miscellaneous_loss: "{lang('lbl_total_miscellaneous_loss')}",
        total_miscellaneous_income: "{lang('lbl_total_miscellaneous_income')}",
//#9696:End
    };
</script>

{include file = "inc/footer.tpl"}