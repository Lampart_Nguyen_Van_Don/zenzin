<style>
.ui-dialog .ui-dialog-content {
    margin: .5em 1em;
    overflow: hidden;
    padding: 0
}
</style>
<form id="form-import" action="/_admin/reconcile_amount/import" method="post">
    <table id="listRecords" class="table-simple" border="1" width="1250">
    <thead>
        <tr>
            <th width="250">{lang('lbl_check')}</th>
            <th width="110">{lang('lbl_datetime')}</th>
            <th width="100">{lang('lbl_source_acc')}</th>
            <th width="100">{lang('lbl_amount')}</th>
            <th width="240">{lang('lbl_s_code')}</th>
            <th width="200">{lang('lbl_client_name')}</th>
            <th>{lang('lbl_action')}</th>
        </tr>
    </thead>
        
        {$has_error = false}
        {$total_account_holder_error = 0}
<!-- #7316:Start -->
        {$total_error = 0}
        {foreach $excel_datas as $key => $data}
<!--        <tr> -->
        <tr id="confirm_row_{$key}">
<!-- #7316:End -->
            <td style="font-size:12px;" class="import-error">
                <div style="width:250px">
                    {if isset($data.errors)}
                        {$has_error = true}
                        {foreach $data.errors as $error_key => $error}
<!-- #7316:Start -->
                            {if $error_key == 'account_holder'}
                                {$total_account_holder_error = $total_account_holder_error + 1}
                            {else}
                                {$total_error = $total_error + 1}
                            {/if}
<!-- #7316:End -->
                            <p class="error_{$error_key}" style="color:red">{$error}</p>
                        {/foreach}
                    {else}
                        OK
                    {/if}
                </div>
            </td>
            <td>
                <div style="width:110px">
                    {str_replace('-', '/', $data.payment_date)}
                    <input type="hidden" name="excel_data[{$key}][payment_date]" value="{$data.payment_date}">
                </div>
            </td>
            <td>
                <div style="width:100px">
                    {$data.account_holder}
                    <input type="hidden" name="excel_data[{$key}][account_holder]" value="{$data.account_holder}">
                </div>
            </td>
            <td>
                <div style="width:100px">
                    {$data.amount}
<!-- #9547:Start -->
<!--                     <input type="hidden" name="excel_data[{$key}][amount]" value="{$data.amount}"> -->
                    <input type="hidden" name="excel_data[{$key}][amount]" value="{$data.amount}" id="amount_ip_{$key}">
<!-- #9547:End -->
                </div>
            </td>
            <td class="s-code-td">
<!-- #7316:Start -->
<!--                 <div style="width:240px"> -->
                <div style="width:240px" id="s_code_name_{$key}">
<!-- #9694:Start -->
<!-- #7316:End -->
                    {*tmp#6865: Start 2015/09/26 if $data.s_code *}
                    <!-- tmp#6865: Start 205/09/26 -->
                    {* if $data.client_id *}
                    <!-- tmp#6865: End -->
                        {* $data.s_code *}
<!--                         <input type="hidden" name="excel_data[{$key}][client_id]" value="{$data.client_id}"> -->
                    {* else *}
<!-- #7316:Start -->
<!--                         <input type="text" class="search-s-code" style="width:98%"> -->
<!--                         <p class="messages"></p> -->
<!--                         <input type="hidden" class="import-client" name="excel_data[{$key}][client_id]" value=""> -->
<!--                         <input type="hidden" class="import-add-new-bank"name="excel_data[{$key}][add_new_bank]" value=""> -->
<!--                         <input type="text" class="search-s-code" style="width:98%" id="s_code_ip_{$key}">
                        <p class="messages" id="s_code_message_{$key}"></p>
                        <input type="hidden" class="import-client" name="excel_data[{$key}][client_id]" value="" id="client_id_ip_{$key}">
                        <input type="hidden" class="import-add-new-bank"name="excel_data[{$key}][add_new_bank]" value="" id="new_bank_ip_{$key}"> -->
<!-- #7316:End -->
                    {* /if *}

                    {$client_id = ''}
                    {$client_name = ''}
                    {if $data.clients && count($data.clients) == 1}
                        {$data.clients[0].s_code}
                        {$client_name = $data.clients[0].client_name}
                        {$client_id = $data.clients[0].client_id}                     
                    {elseif !$data.clients}
                        <input type="text" class="search-s-code" style="width:98%" id="s_code_ip_{$key}">
                        <p class="messages" id="s_code_message_{$key}"></p>
                        <input type="hidden" class="import-add-new-bank"name="excel_data[{$key}][add_new_bank]" value="" id="new_bank_ip_{$key}">
                    {/if}
                    <input type="hidden" class="import-client" name="excel_data[{$key}][client_id]" value="{$client_id}" id="client_id_ip_{$key}">
<!-- #9694:End -->
                </div>
            </td>
            <td class="client-td">
<!-- #7316:Start -->
<!--                 <div style="width:200px">{$data.client_name}</div> -->
<!-- #9694:Start -->
<!--                 <div style="width:200px" id="client_name_{$key}">{$data.client_name}</div> -->
                <div style="width:200px" id="client_name_{$key}">
                {if $data.clients && count($data.clients) > 1}
                    <select id="s_code_cbx_{$key}" class="select-s-code" style="width:98%; height: 27px;">
                        <option value="">{lang('common_please_select')}</option>
                        {foreach $data.clients as $client}
                        <option value="{$client.client_id}" data-s-code="{$client.s_code}">{$client.s_code} {$client.client_name}</option>
                        {/foreach}
                    </select>
                    <p class="messages" id="s_code_message_{$key}"></p>
                {else}
                    {$client_name}
                {/if}
                </div>
<!-- #9694:End -->
<!-- #7316:End -->
            </td>
            <td>
<!-- #9694:Start -->
                {* if !$data.s_code *}
                {if count($data.clients) > 1 || !$data.clients}
<!-- #9694:End -->
<!-- #7316:Start -->
<!--                     <button type="button" id="" class="btn-small btn-green btn-confirm">{lang('lbl_confirm_client')}</button> -->
                    <button type="button" id="btn_confirm_{$key}" class="btn-small btn-green btn-confirm">{lang('lbl_confirm_client')}</button>
<!-- #7316:End -->
                {/if}
            </td>
        </tr>
        {/foreach}
        <tr>
            <td colspan="7">
                {if $has_error == false}
                    <button type="submit" class="btn-small btn-green" value="1" name="save_data">{lang('btn_import')}</button>
                {/if}
                <button type="button" class="btn-small btn-green" name="cancel_upload" id="cancel_upload">{lang('btn_cancel')}</button>
            </td>
        </tr>
    </table>
</form>

<!-- #7316:Start -->
<script>
    var total_error = {$total_error};
    var total_account_holder_error = {$total_account_holder_error};
//#9547:Start
    var clients_total_amount = {json_encode($clients_total_amount)};
//#9547:End
</script>
<!-- #7316:End -->
{literal}
<script>
$(document).ready(function() {

    $('#listRecords').fixed_thead({
        maxHeight: 400
    });

    // $(".search-s-code").autocomplete({
    //     source: "/_admin/reconcile_amount/get_customer_by_scode",
    //     select: function( event, ui ) {
    //         // set client_id
    //         data = {
    //             id: ui.item.id,
    //             client_name: ui.item.client_name,
    //             label: ui.item.label,
    //         };
            
    //         client_td = $(this).parent().next();
    //         s_code_td = $(this).parent();
    //         error_td  = $(this).parent().parent().find('td:first-child');
    //         btn = $(this).parent().next().next().find('.btn-confirm');
    //         set_s_code_data(client_td, s_code_td, error_td, btn, data);
    //     },
    // });

//#7316:Start
    var timer = [];
    var xhr = [];
    
    $(".search-s-code").on('keypress', function(event) {
        // if user press enter key, then check s_code will be fired immediately
        if (event.keyCode != 13) return;
        
        event.preventDefault();
        var $this  = $(this);
        var row_id = $(this).attr('id').substring($(this).attr('id').lastIndexOf('_') + 1);

        if (row_id in timer) clearTimeout(timer[row_id]);
//      get_s_code_data($(this).parent().next().next().find('.btn-confirm'));
        get_s_code_data($this.val(), row_id, false);
    });

    $(".search-s-code").on('keyup', function(event) {
        if (event.keyCode == 13) return;

        var $this  = $(this);
        var row_id = $(this).attr('id').substring($(this).attr('id').lastIndexOf('_') + 1);

        if (row_id in timer) clearTimeout(timer[row_id]);

        if ($(this).val().trim() == '') {
            return;
        }

        // check valid s_code after 2s
        timer[row_id] = setTimeout(function() {
            get_s_code_data($this.val(), row_id, false);
        }, 2000);

//#7316:End
    });

    $(".btn-confirm").on('click', function(event) {
//#7316:Start
//        var $this = $(this);
//        get_s_code_data($(this));
        var row_id = $(this).attr('id').substring($(this).attr('id').lastIndexOf('_') + 1);
        var s_code = $("#s_code_ip_" + row_id).val();

//#9694:Start
        if (!s_code && $("#s_code_cbx_" + row_id).val() != '') {
            s_code = $("#s_code_cbx_" + row_id + ' option:selected').data('s-code');
        }

        if (!s_code || s_code.trim() == '') {
            $("#s_code_message_" + row_id).html('<div class="validation-error">'+ label.s_code_is_required + '</div>');
            return;
        }
//#9694:End
        if (row_id in timer) clearTimeout(timer[row_id]);

        get_s_code_data(s_code, row_id, true);
//#7316:End
    });

//#7316:Start
//  var get_s_code_data = function($this) {
    var get_s_code_data = function(s_code, row_id, is_confirm) {
//        var client_td = $this.parent().prev();
//        var s_code_td = $this.parent().prev().prev();
//        var error_td  = $this.parent().parent().find('td:first-child');
//        var s_code    = s_code_td.find('input.search-s-code').val();

//        if (!s_code) {
//            s_code_td.find('p.messages').html('<div class="validation-error">'+label.s_code_is_required+'</div>');
//            return;
//        }
        if (row_id in xhr) xhr[row_id].abort();

//#9547:Start
        var amount = parseInt($("#amount_ip_" + row_id).val());
        if (is_confirm && clients_total_amount[s_code]) {
            clients_total_amount[s_code] = parseInt(clients_total_amount[s_code]) + parseInt(amount);
            $("#btn_confirm_" + row_id).attr('disabled', 'disabled');
        } else if (is_confirm) {
            clients_total_amount[s_code] = amount;
            $("#btn_confirm_" + row_id).attr('disabled', 'disabled');
        }

        xhr[row_id] = $.ajax({
            url: '/_admin/reconcile_amount/get_customer_by_scode',
            type: 'POST',
//            data: {term: s_code, exactly: 1},
            data: {term: s_code, exactly: 1, amount: clients_total_amount[s_code], is_confirm: is_confirm},
//#9547:End
        })
        .done(function(data) {
            data = $.parseJSON(data);
//            if (!$.isEmptyObject(data)) {
//                set_s_code_data(client_td, s_code_td, error_td, $this, data[0]);
//                return;
//            }

//            s_code_td.find('p.messages').html('<div class="validation-error">'+label.s_code_is_not_exist+'</div>');
            set_s_code_data(data[0], row_id, is_confirm);
        })
    }
//#7316:End

//#7316:Start
//    var set_s_code_data = function(client_td, s_code_td, error_td, btn, data) {
    var set_s_code_data = function(data, row_id, is_confirm) {
//        client_td.text(data.client_name);
//        s_code_td.find('input.import-client').val(data.id);
//        s_code_td.find('input.import-add-new-bank').val(1);
//        s_code_td.find('input.search-s-code, p.messages').remove();
//        // console.log(s_code_td.text());
//        // s_code_td.text(data.label);
//        s_code_td.contents().last()[0].textContent = data.label;

        if ($.isEmptyObject(data)) {
            var data         = {};
            data.id          = '';
            data.client_name = '';
            data.label       = '';
            data.value       = '';
            data.is_error    = true;
            $("#s_code_message_" + row_id).html('<div class="validation-error">'+label.s_code_is_not_exist+'</div>');
        }

        if (!data.is_error) {
            $("#s_code_message_" + row_id).html('');
        }

        $("#client_name_" + row_id).text(data.client_name);
        $("#client_id_ip_" + row_id).val(data.id);
        $("#new_bank_ip_" + row_id).val(1);
        
        if (is_confirm && data.id != '') {
            if (row_id in xhr) xhr[row_id].abort();

            if (row_id in timer) clearTimeout(timer[row_id]);

//#9547:Start
            var error_td = $("#confirm_row_" + row_id).find("td:first");
            error_td.find('p.error_field').remove();
            
            if (data.errors && data.messages) {
                $("#btn_confirm_" + row_id).removeAttr('disabled');
                if (clients_total_amount[data.value]) {
                    clients_total_amount[data.value] = parseInt(clients_total_amount[data.value]) - parseInt($("#amount_ip_" + row_id).val());
                } else {
                    clients_total_amount[data.value] = 0;
                }

                $.each(data.messages, function(index, message) {
                    error_td.append('<p class="error_' + index + '" style="color:red">' + message + '</p>');
                });
                return;
            }
//#9547:End

            $("#s_code_ip_" + row_id).remove();
//#9694:Start
            $("#s_code_cbx_" + row_id).remove();
//#9694:End
            $("#s_code_name_" + row_id).contents().last()[0].textContent = data.label;
            $("#btn_confirm_" + row_id).remove();

//#9547:Start
//            var error_td = $("#confirm_row_" + row_id).find("td:first");
//#9547:End
            error_td.find('p.error_account_holder').remove();
//            btn.hide();

            if (error_td.find('p').length == 0) {
                error_td.text('OK');
            }

            total_account_holder_error--;
            if (total_error == 0 && total_account_holder_error == 0) {
                $("#cancel_upload").before('<button type="submit" class="btn-small btn-green" value="1" name="save_data">' + label.btn_import + '</button>');
            }

//#7316:Start
//            show_import_btn = true;
//            $.each($("#listRecords td.import-error"), function(index, val) {
//                if ($.trim($(val).text()) != 'OK') show_import_btn = false;
//            });

//            if (show_import_btn) {
//                $("#cancel_upload").before('<button type="submit" class="btn-small btn-green" value="1" name="save_data">' + label.btn_import + '</button>');
//            }
//#7316:End
        }
    }
//#7316:End

//#9694:Start
    $(".select-s-code").on('change', function(event) {
        var row_id      = $(this).attr('id').substring($(this).attr('id').lastIndexOf('_') + 1);
        var s_code = $(this).find('option:selected').data('s-code');
        if (!s_code) s_code = '';

        $("#client_id_ip_" + row_id).val($(this).val());
        $("#s_code_name_" + row_id).contents().last()[0].textContent = s_code;
    });
//#9694:End

    $(document).on('click', 'button[type=submit]', function(event){
        $("#cancel_upload").attr('disabled', 'disabled');
    });

    $("#cancel_upload").on('click', function(event) {
        $("#dialog-import-result").html("");
        $("#dialog-import-result").dialog("close");
    });
});

</script>
{/literal}