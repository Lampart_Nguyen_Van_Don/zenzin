{include file = "inc/header.tpl"} {include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

<div class="cell content">
    <div>
        <h1 class="page-title">{$title}</h1>
        <div>{if isset($message)}{$message}{/if}</div>
        <hr class="blank-10px" />
        {if isset($show_btn_import)}
        <form method="POST" action="/_admin/reconcile_amount/show">
            <table class="table-theme odd">
                <tr>
                    <th>{lang('lbl_check')}</th>
                    <th>{lang('lbl_datetime')}</th>
                    <th>{lang('lbl_source_acc')}</th>
                    <th>{lang('lbl_amount')}</th>
                    <th>{lang('lbl_s_code')}</th>
                    <th>{lang('lbl_client_name')}</th>
                    <th>{lang('lbl_action')}</th>
                </tr>
                {if isset($excel_data)} {foreach from=$excel_data item=d}
                <tr>
                    <td>{if $d[3]==1} OK {else} {lang('lbl_import_ng')} {/if}</td>
                    <!-- Datetime -->
                    <td>{$d[0]}</td>
                    <!-- Transfer source -->
                    <td>{$d[4]}</td>
                    <!-- Total money -->
                    <td>{$d[2]}</td>
                    <!-- S_Code -->
                    <td>{if isset($d[1].s_code)} {$d[1].s_code} {/if}</td>
                    <!-- Client name -->
                    <td>{if isset($d[1].name)}{$d[1].name}{/if}</td>
                    <!-- Confirm client -->
                    <td>{if $d[3]==0} {lang('lbl_confirm_client')} {/if}</td>
                </tr>
                {/foreach} {else}
                <tr>
                    <td colspan="7">{lang('common_lbl_no_data')}</td>
                </tr>
                {/if}
            </table>
            <br /> {if $show_btn_import==true}
            <button type="submit" class="btn-small btn-green" value="save_upload_to_db" name="save_upload_to_db">{lang('btn_import')}</button>
            {/if}
            <button type="submit" class="btn-small btn-green" name="cancel_upload">{lang('btn_cancel')}</button>
        </form>
        {else}
        <div
            style="border: 1px solid black; height: auto; width: auto; padding: 20px;">
            <form method="POST" action="/_admin/reconcile_amount/show"
                enctype="multipart/form-data">
                <table>
                    <tr>
                        <td width=200px>{lang('lbl_file_import')}</td>
                        <td><input type="file" name="file_reconcile_import"
                            id="file_reconcile_import" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" class="btn-small btn-green" value="upload_to_show"
                                name="upload_to_show" id="upload_to_show">{lang('lbl_import_and_process')}</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        {/if}
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $( "#upload_to_show" ).click(function() {
            if(!$('#file_reconcile_import').val()) {
                alert(lang['choose_file_before_upload']);
                return false;
            }
        });
    });
</script>
{include file = "inc/footer.tpl"}
