{*#7152:Start*}
<style>
    /*.ui-dialog-content a {
        color: #fff;
    }*/
    .ui-dialog-content a:hover {
        color: #69f;
    }
</style>
{*#7152:End*}

<div class="cl_content" id="sales_slip_form">
    <div>
        <div class="message"></div>

        <hr class="blank-10px" />

        {lang('lbl_invoice_address')}

        {form_open('', ['id' => 'sales-slip-form'])}

            <table id="input-table" class="table-simple">
                <tr>
                    <th class="ms">{lang('lbl_s_code')}</th>
                    <td>
                        {form_input([
                            'id'   => 's_code',
                            'name' => 's_code',
                            'class' => 'ss',
                            'value' => $client['s_code'],
                            'disabled' => 'disabled'
                        ])}
                        <div class="error"></div>
                    </td>
                    <th class="ms">{lang('lbl_company_name')}</td>
                    <td>
                        {form_input([
                        'id'   => 'company',
                        'name' => 'company',
                        'class' => 'xs',
                        'value' => $client['name'],
                        'disabled' => 'disabled'
                        ])}
                        <div class="error"></div>
                    </td>
                </tr>
                <tr>
                    <th class="ms">{lang('lbl_zipcode')} <span class="red-symbol">※</span></th>
                    <td colspan="3">
                        {form_input([
                            'id'   => 'zipcode',
                            'name' => 'zipcode',
                            'class' => 'ss',
                            'value' => $sales_slip['zipcode'],
                            'maxlength' => 7
                        ])}
                        <div class="legend">ハイフンは入力しないでください</div>
                        <div class="error"></div>
                    </td>
                </tr>
                <tr>
                    <th class="ms">{lang('lbl_address_1')} <span class="red-symbol">※</span></th>
                    <td>
                        {form_input([
                            'id'   => 'address_1',
                            'name' => 'address_1',
                            'class' => 'xs',
                            'value' => $sales_slip['address_1st']
                        ])}
                        <div class="error"></div>
                    </td>
                    <td colspan="2">
                        {form_input([
                            'id'   => 'address_2',
                            'name' => 'address_2',
                            'class' => 'xs',
                        'value' => $sales_slip['address_2nd']
                        ])}
                        <div class="error"></div>
                    </td>
                </tr>
                <tr>
                    <th class="ms">{lang('lbl_department')}</td>
                    <td>
                        {form_input([
                            'id'   => 'department',
                            'name' => 'department',
                            'class' => 'xs',
                            'value' => $sales_slip['division_name']
                        ])}
                        <div class="error"></div>
                    </td>
                    <th class="ms">{lang('lbl_customer')} <span class="red-symbol">※</span></td>
                    <td>
                        {form_input([
                            'id'   => 'customer',
                            'name' => 'customer',
                            'class' => 'ms',
                            'value' => $sales_slip['charge_name']
                        ])}様
                        <div class="error"></div>
                    </td>
                </tr>
                <tr>
                    <th class="ms">{lang('lbl_tel')} <span class="red-symbol">※</span></td>
                    <td>
                        {form_input([
                            'id'   => 'tel',
                            'name' => 'tel',
                            'class' => 'ms',
                            'value' => $sales_slip['phone_no']
                        ])}
                        <div class="legend">ハイフンを入力してください</div>
                        <div class="error"></div>
                    </td>
                    <th class="ms">{lang('lbl_fax')}</td>
                    <td>
                        {form_input([
                            'id'   => 'fax',
                            'name' => 'fax',
                            'class' => 'ms',
                            'value' => $sales_slip['fax_no']
                        ])}
                        <div class="legend">ハイフンを入力してください</div>
                        <div class="error"></div>
                    </td>
                </tr>
                <tr>
                    <th class="ms">{lang('lbl_bill_closing_date')}</td>
                    <td>
                        {form_input([
                            'id'   => 'billing_closing_date',
                            'name' => 'billing_closing_date',
                            'class' => 'ss',
                            'value' => str_replace('-', '/', $sales_slip['billing_date']),
                            'disabled' => (!$sales_slip['is_ad_receive_payment']) ? 'disabled' : false
                        ])}
                        <div class="error"></div>
                    </td>
                    <th class="ms">{lang('lbl_payment_expire_date')}</td>
                    <td>
                        {form_input([
                            'id'   => 'payment_expire_date',
                            'name' => 'payment_expire_date',
                            'class' => 'ss',
                            'value' => str_replace('-', '/', $sales_slip['payment_date']),
                            'disabled' => (!$sales_slip['is_ad_receive_payment']) ? 'disabled' : false
                        ])}
                        <div class="error"></div>
                        {* Dummy input for error display purpose *}
{*#7620:Start*}
                        {*<input type="hidden" name="billing_payment_date" disabled="disabled">*}
                        {*<div class="error"></div>*}
{*#7620:End*}
                    </td>
                </tr>
            </table>

            <hr class="blank-10px" />

            <div>
                {lang('lbl_total_untax_amount')}：<span class="ss_total_amount">0</span>円   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                {lang('lbl_total_tax')}：<span class="ss_total_tax">0</span>円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                {lang('lbl_total_tax_amount')}：<span class="ss_total_amount_tax">0</span>円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </div>

            <hr class="blank-10px" />

            <div id="sales_form_table" class="grid"></div>

            <hr class="blank-10px" />

            <div>
                {lang('lbl_total_untax_amount')}：<span class="ss_total_amount">0</span>円   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                {lang('lbl_total_tax')}：<span class="ss_total_tax">0</span>円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                {lang('lbl_total_tax_amount')}：<span class="ss_total_amount_tax">0</span>円 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </div>

            <hr class="blank-10px" />
        {form_close()}

        <div>
        {if $action == 'add'}
            <button name="action" class="btn-small btn-green btn-slCreateSalesSlip" value="edit">{lang('btn_create_sales')}</button>
        {/if}
        {if $action == 'edit'}
            <button name="action" class="btn-small btn-green btn-slEditSalesSlip" data-sales-slip-id="{$sales_slip.id}" value="edit">{lang('common_lbl_save')}</button>
            <button name="action" class="btn-small btn-green btn-slDeleteSalesSlip" data-sales-slip-id="{$sales_slip.id}" value="delete">{lang('btn_delete_sales')}</button>
        {/if}
            <button type="button" ng-click="close()" class="btn-small btn-green btn-close">{lang('common_lbl_close')}</button>
        </div>
    </div>
</div>

<script>
//#7791:Start
    {*var sales_slip_detail = $.parseJSON('{str_replace('\u0022', '\\#', json_encode($sales_slip_detail, 8 + 4))}');*}
    {*var old_sales_slip_detail = $.parseJSON('{str_replace('\u0022', '\\#', json_encode($old_sales_slip_detail, 8 + 4))}');*}
    var sales_slip_detail = {json_encode($sales_slip_detail)};
    var old_sales_slip_detail = {json_encode($old_sales_slip_detail)};
//#7791:End
    exit_confirm();
</script>

<script src="/public/admin/sales/add_edit.js"></script>