<div style="width: 100%">
{if $is_no_data}
    <table class="table-simple" style="table-layout:fixed;width: 100%; overflow-x:auto">
        <tr>
            <th class="ms">{lang("lbl_sales_slip_number")}</th>
            <td>{$sales_data.sales_slip_number}</td>
       
            <th class="ms">{lang("lbl_sales_slip_status")}</th>
            <td>{if isset($sales_data.sales_slip_status)}{$sales_slip_status[$sales_data.sales_slip_status]}{/if}</td>
        </tr>
        
        <tr>
            <th>{lang("lbl_j_account_id")}</th>
            <td>{$sales_data.j_account_name}</td>
        
            <th>{lang("lbl_s_account_id")}</th>
            <td>{$sales_data.s_account_name}</td>
        </tr>
        
        <tr>
            <th>{lang("lbl_s_code")}</th>
            <td>{$sales_data.s_code}</td>
       
            <th>{lang("lbl_company_name")}</th>
            <td>{$sales_data.client_name}</td>
        </tr>
        
        <tr>
            <th>{lang("lbl_zipcode")}</th>
            <td colspan="3">{$sales_data.zipcode}</td>
        </tr>
        
        <tr>
            <th>{lang("lbl_address_1")}</th>
            <td>{$sales_data.address_1st}</td>
        
            <td colspan="2">{if $sales_data.address_2nd}{$sales_data.address_2nd}{/if}</td>
        </tr>
        
        <tr>
            <th>{lang("lbl_department")}</th>
            <td>{$sales_data.division_name}</td>
       
            <th>{lang("lbl_contract")}</th>
            <td>{$sales_data.charge_name}</td>
        </tr>
        
        <tr>
            <th>TEL</th>
            <td>{$sales_data.phone_no}</td>
        
            <th>FAX</th>
            <td>{$sales_data.fax_no}</td>
        </tr>
        
        <tr>
            <th>{lang("lbl_bill_closing_date")}</th>
            <td>{$sales_data.billing_date}</td>
        
            <th>{lang("lbl_payment_expire_date")}</th>
            <td>{$sales_data.payment_date}</td>
        </tr>
        
        <tr>
            <td colspan="4">
            <br>
                {lang("lbl_amount_total")}:{$sales_data.total_amount|number_format:0}{lang("lbl_yen")}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                {lang("lbl_tax_total")}:{$sales_data.total_tax|number_format:0}{lang("lbl_yen")}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                {lang("lbl_amount_tax_total")}:{$sales_data.total_amount_tax|number_format:0}{lang("lbl_yen")}
                <div id="sales_detail" class="grid" style="width: 100%;"></div>
            </td>
        </tr>
    </table>
    <hr class="blank-10px" />
    <!-- Button area -->
    <div class="buttons">
        {if $this->sales_slip_model->is_sales_slip_accountant_opened($sales_data.id)}
            {if $is_edit}
                <button type="button" data-sales_slip_id="{$sales_data.id}" class="btn-small btn-green" id="edit_sales_slip">{lang('common_lbl_edit')}</button>
            {/if}
            {if $is_approval}
                <button type="button" class="btn-small btn-green" id="sales_approval">{lang('btn_sales_slip_approval')}</button>
            {/if}
            {if $is_edit_approval_reject && $can_approve_sales_slip}
                <button type="button" class="btn-small btn-green" id="approval_reject_sale_slip">{lang('lbl_sales_slip_approval_rejection')}</button>
            {/if}
        {/if}
        {if $is_pdf_download}
            <button type="button" data-sales_slip_id="{$sales_data.id}" class="btn-small btn-green" id="sales_slip_pdf">{lang('btn_sales_slip_pdf_download')}</button>
        {/if}
        <button type="button" ng-click="close()" class="btn-small btn-green btn-close">{lang('common_lbl_close')}</button>
    </div>
    <!-- End/ -->
    <script>
    var sales_slip_detail       = {json_encode($sales_slip_detail)};
    var tax_type       = {json_encode($tax_type)};
    var sales_slip_lbl_comment  = "{lang("lbl_sales_slip_comment")}";
    var sales_slip_bt_approval  = "{lang("lbl_sales_slip_approval")}";
    var sales_slip_bt_rejection = "{lang("lbl_sales_slip_rejection")}";
    var sales_slip_id           = "{$sales_data.id}";
</script>
<script src="/public/admin/sales/detail.js"></script>
{else}
<div class="red-txt">{lang('lbl_no_data_available')}</div>

<br><br><br>
<button type="button" ng-click="close()" class="btn-small btn-green btn-close">{lang('common_lbl_close')}</button>
{/if}
</div>

