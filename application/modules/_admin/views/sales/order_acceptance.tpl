<div class="order-acceptance">
    <p>{$j_code} - {$branch_cd}</p>

    <hr class="blank-10px" />

    <p>{lang("lbl_total_taxes_not_determine")}: <span id="total_order_acceptance"></span>円</p>

    <hr class="blank-10px" />

    <table class="table-simple">
        <thead>
            <tr>
                <th>{lang("lbl_source_subcontractor_id")}</th>
                <th>{lang("lbl_shipping_type_cd")}</th>
                <th>{lang("lbl_set_name")}</th>
                <th>{lang("lbl_weight")}</th>
                <th>{lang("lbl_quantity_acceptance")}</th>
                <th>{lang("lbl_delivery_date_acceptance")}</th>
                <th>{lang("lbl_order_acceptance_amount")}</th>
            </tr>
        </thead>

        <tbody>
            {$total = 0}
            {if $order_acceptances}
                {foreach $order_acceptances as $order_acceptance}
                    {$total = $total + $order_acceptance.amount}
                    <tr>
                        <td>{$order_acceptance.subcontractor_name}</td>
                        <td>{$order_acceptance.shipping_type_name}</td>
                        <td>{$order_acceptance.set_name}</td>
                        <td>{$order_acceptance.weight|number_format:0}</td>
                        <td>{$order_acceptance.quantity|number_format:0}</td>
                        <td>{if $order_acceptance.delivery_date != "0000-00-00"} {date('Y/m/d', strtotime($order_acceptance.delivery_date))} {/if}</td>
                        <td>{$order_acceptance.amount|number_format:0}</td>
                    </tr>
                {/foreach}
            {/if}
        </tbody>

    </table>

    <hr class="blank-10px" />
    <!-- Button area -->
    <div class="buttons">
        <button type="button" ng-click="close()" class="btn-small btn-green btn-close">{lang('common_lbl_close')}</button>
    </div>
    <!-- End/ -->
</div>

<script type="text/javascript">
    $(document).ready(function() {
    	var totalall = '{$total|number_format:0}';
        $('#total_order_acceptance').html(totalall);
    });
</script>