{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

{$account_options = map_element($account_data, 'id', 'name')}
{$account_options_group_by_business = map_element($account_data, 'id', 'name', 'business_unit_id')}
{$accounts = index_element($account_data, 'id')}

<script src="/public/admin/js/jquery-handler-toolkit.js"></script>

<style>
    .search-status-row {
        width: 100%;
        min-width: 550px;
        max-width: 650px;
    }
    .search-status {
        display: inline-block;
        width: 30%;
    }
    .handsontable.ht_clone_top {
        z-index: 0;
    }
    .validation-error {
        display: inline-block;
    }

    /*View Order's style*/
    .cell-template-data {
        width: 100%;
        height: 100%;
        line-height: 30px;
    }
    .orange-row {
        background-color: #ea8f15!important;
    }
</style>

<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>

        {form_open('', ['id' => 'ss_search'])}
        <div style="display: inline-block">
            <table id="search_toggle" class="table-simple" style="width: 100%">
                <tr>
                    <td id="search_toggle" style="padding-left: 10px; border-bottom: none">
                        <a href="javascript:void(0)">{lang('common_lbl_hide_search')}</a>
                    </td>
                </tr>
            </table>
            <div id="search_body" style="display: inline-block">
                <table class="table-simple">
                    <tbody>
                    <tr>
                        <th>
                            <div>{lang('lbl_status')}</div>
                        </th>
                        <td>
                            <div class="search-status-row">
                                {$count = 1}
                                {$status_options = $this->sales_slip_model->status_options()}
                                {foreach $status_options as $k => $v}
                                <div class="search-status">
                                    <label>
                                        {form_checkbox([
                                        'name'    => 'search_status[]',
                                        'value'   => $k,
                                        'checked' => set_checkbox('search_status[]', $k)
                                        ])}
                                        {$v}
                                    </label>
                                </div>
                                {if $count++ % 3 == 0}
                            </div><div class="search-status-row">
                                {/if}
                                {/foreach}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_adv_payment')}</th>
                        <td>
                            <label>
                                {form_checkbox([
                                'name' => 'search_is_ad_receive_payment',
                                'value' => 1,
                                'checked' => set_checkbox('search_is_ad_receive_payment', 1)
                                ])}
                                {lang('lbl_adv_payment')}
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>Ｊ{lang('lbl_sales_representative')}</th>
                        <td>
{*#7357:Start*}
                            {if $accounts.{$login_account_id}.is_product_division}
                                {$default_bu = $accounts.{$login_account_id}.business_unit_id}
                            {else}
                                {$default_bu = ''}
                            {/if}
{*#7357:End*}
                            {if $can_create == 'self'}
                                {$default_j_account = $this->auth->get_account_id()}
                                {$disable_j_account_search = true}
                            {else}
{*#7357:Start*}
                                {*{$default_j_account = 0}*}
                                {$default_j_account = set_value('search_j_account', 0)}
                                {$default_bu = set_value('search_j_business_unit', $default_bu)}
{*#7357:End*}
                                {$disable_j_account_search = false}
                            {/if}
                            <div style="display: inline-block; margin-right: 10px;">
                                {lang('lbl_business_unit')}&nbsp;
                                {form_dropdown([
                                'name' => 'search_j_business_unit',
                                'items' => [0 => lang('common_lbl_all')] + $business_unit_options,
                                'value' => $default_bu,
                                'disabled' => $disable_j_account_search,
                                'class' => 'max-width'
                                ])}
                            </div>

                            <div style="display: inline-block;">
                                {lang('lbl_sales_staff')}

                                {if isset($account_options_group_by_business[set_value('search_j_business_unit', $default_bu)])}
                                    {$account_items = [0 => lang('common_lbl_all')] + $account_options_group_by_business[set_value('search_j_business_unit', $default_bu)]}
                                {elseif set_value('search_j_business_unit', $default_bu) == 0}
                                    {$account_items = [0 => lang('common_lbl_all')] + $account_options}
                                {else}
                                    {$account_items = [0 => lang('common_lbl_all')]}
                                {/if}
                                {lang('lbl_j_account_id')}&nbsp;
                                {form_dropdown([
                                'name' => 'search_j_account',
                                'style' => 'width:170px',
                                'data-parent' => 'search_j_business_unit',
                                'data-parent-null-action' => 'load_all',
                                'items' => $account_items,
                                'value' => $default_j_account,
                                'data-group-items' => htmlspecialchars(json_encode($account_options_group_by_business)),
                                'disabled' => $disable_j_account_search,
                                'class' => 'max-width'
                                ])}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_slip_number')}</th>
                        <td>{form_input([
                            'name' => 'search_sales_slip_number',
                            'value' => set_value('search_sales_slip_number', '', false),
                            'class' => ''
                            ])}
                            <div class="legend">{lang('lbl_num_exact')}</div>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_company_name')}</th>
                        <td>{form_input([
                            'name' => 'search_client_name',
                            'value' => set_value('search_client_name', '', false),
                            'class' => ''
                            ])}
                            <div class="legend">{lang('lbl_like_search')}</div>
                        </td>
                    </tr>
                    <tr>
                        <th>Ｊ{lang('lbl_code')}＋{lang('lbl_branch')}</th>
                        <td>
                            {form_input([
                                'name' => 'search_j_code_branch_cd_from',
                                'value' => set_value('search_j_code_branch_cd_from', '', false),
                                'class' => ''
                            ])}
                            ～
                            {form_input([
                                'name' => 'search_j_code_branch_cd_to',
                                'value' => set_value('search_j_code_branch_cd_to', '', false),
                                'class' => ''
                            ])}
                            <div class="legend">{lang('lbl_date_prefix')}</div>
                            <span id="j_code_branch_cd_from"></span>
                            <span id="j_code_branch_cd_to"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_delivery_date')}</th>
                        <td>
                            {form_input([
                                'name' => 'search_delivery_date_from',
                                'value' => set_value('search_delivery_date_from', $default_delivery_date_from->format('Y/m/d'), false),
                                'class' => ''
                            ])}
                            ～
                            {form_input([
                                'name' => 'search_delivery_date_to',
                                'value' => set_value('search_delivery_date_to', '9999/12/31', false),
                                'class' => ''
                            ])}
                            <div class="legend">{lang('lbl_date_specified')}</div>
                            <span id="delivery_date_from"></span>
                            <span id="delivery_date_to"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_order_regist_date')}</th>
                        <td>
                            {form_input([
                                'name' => 'search_order_regist_date_from',
                                'value' => set_value('search_order_regist_date_from', '', false),
                                'class' => ''
                            ])}
                            ～
                            {form_input([
                                'name' => 'search_order_regist_date_to',
                                'value' => set_value('search_order_regist_date_to', '', false),
                                'class' => ''
                            ])}
                            <div class="legend">{lang('lbl_date_specified')}</div>
                            <span id="order_regist_date_from"></span>
                            <span id="order_regist_date_to"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_order_approval_date')}</th>
                        <td>
                            {form_input([
                                'name' => 'search_approval_date_from',
                                'value' => set_value('search_approval_date_from', '', false),
                                'class' => ''
                            ])}
                            ～
                            {form_input([
                                'name' => 'search_approval_date_to',
                                'value' => set_value('search_approval_date_to', '', false),
                                'class' => ''
                            ])}
                            <div class="legend">{lang('lbl_date_specified')}</div>
                            <span id="approval_date_from"></span>
                            <span id="approval_date_to"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_billing_date')}</th>
                        <td>
                            {form_input([
                                'name' => 'search_billing_date_from',
                                'value' => set_value('search_billing_date_from', '', false),
                                'class' => ''
                            ])}
                            ～
                            {form_input([
                                'name' => 'search_billing_date_to',
                                'value' => set_value('search_billing_date_to', '', false),
                                'class' => ''
                            ])}
                            <div class="legend">{lang('lbl_date_specified')}</div>
                            <span id="billing_date_from"></span>
                            <span id="billing_date_to"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_payment_date')}</th>
                        <td>
                            {form_input([
                                'name' => 'search_payment_date_from',
                                'value' => set_value('search_payment_date_from', '', false),
                                'class' => ''
                            ])}
                            ～
                            {form_input([
                                'name' => 'search_payment_date_to',
                                'value' => set_value('search_payment_date_to', '', false),
                                'class' => ''
                            ])}
                            <div class="legend">{lang('lbl_date_specified')}</div>
                            <span id="payment_date_from"></span>
                            <span id="payment_date_to"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>Ｓ{lang('lbl_code')}</th>
                        <td>{form_input([
                            'name' => 'search_s_code',
                            'value' => set_value('search_s_code', '', false),
                            'class' => ''
                            ])}
                            <div class="legend">{lang('lbl_prefix_search')}</div>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_company_name')}{lang('lbl_kana')}</th>
                        <td>{form_input([
                            'name' => 'search_client_name_furigana',
                            'value' => set_value('search_client_name_furigana', '', false),
                            'class' => ''
                            ])}
                            <div class="legend">{lang('lbl_like_search')}</div>
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('lbl_division')}</th>
                        <td>{form_input([
                            'name' => 'search_client_department_name',
                            'value' => set_value('search_client_department_name', '', false),
                            'class' => ''
                            ])}
                            <div class="legend">{lang('lbl_like_search')}</div>
                        </td>
                    </tr>
                    <tr>
                        <th>Ｓ{lang('lbl_sales_representative')}</th>
                        <td>
                            <div style="display: inline-block; margin-right: 10px;">
                                {if $accounts.{$login_account_id}.is_product_division}
                                    {$default_bu = $accounts.{$login_account_id}.business_unit_id}
                                {else}
                                    {$default_bu = ''}
                                {/if}
                                {lang('lbl_business_unit')}&nbsp;
                                {form_dropdown([
                                'name' => 'search_s_business_unit',
                                'items' => [0 => lang('common_lbl_all')] + $business_unit_options,
                                'value' => set_value('search_s_business_unit', $default_bu)
                                ])}
                            </div>

                            <div style="display: inline-block;">
                                {lang('lbl_sales_staff')}

                                {if isset($account_options_group_by_business[set_value('search_s_business_unit', $default_bu)])}
                                    {$account_items = [0 => lang('common_lbl_all')] + $account_options_group_by_business[set_value('search_s_business_unit', $default_bu)]}
                                {elseif set_value('search_s_business_unit', $default_bu) == 0}
                                    {$account_items = [0 => lang('common_lbl_all')] + $account_options}
                                {else}
                                    {$account_items = [0 => lang('common_lbl_all')]}
                                {/if}
                                {lang('lbl_s_account_id')}&nbsp;
                                {form_dropdown([
                                'name' => 'search_s_account',
                                'style' => 'width:170px',
                                'data-parent' => 'search_s_business_unit',
                                'data-parent-null-action' => 'load_all',
                                'items' => $account_items,
                                'value' => set_value('search_s_account'),
                                'data-group-items' => htmlspecialchars(json_encode($account_options_group_by_business))
                                ])}
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="a-center" colspan="2">
                            <button type="submit" class="btn-small btn-green btn-search">{lang('common_lbl_search')}</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <hr class="blank-10px" />

        {form_close()}

        <hr class="blank-10px" />
        <h1></h1>
        <hr class="blank-10px" />

        <div style="margin-bottom: 10px"><div id="message"></div></div>
        <!-- GRID -->
        {if $can_create != 'none'}
            <a href="javascript:void(0)" class="btn-small btn-green btn-save">{lang('common_lbl_save')}</a>
            <a href="javascript:void(0)" class="btn-small btn-green btn-create-sales">{lang('btn_create_sales')}</a>
            {lang('lbl_slip_number')}<input type="text" class="slip_number">{lang('lbl_ni')}<a href="javascript:void(0)" class="btn-small btn-green btn-add-slip-number">{lang('common_lbl_add')}</a>
            <a href="javascript:void(0)" class="btn-small btn-green btn-apply">{lang('btn_bulk_apply')}</a>
        {/if}
        {if $can_approve == 'true'}
            <a href="javascript:void(0)" class="btn-small btn-green btn-select-all">{lang('btn_approval_select_all')}</a>
            <a href="javascript:void(0)" class="btn-small btn-green btn-approve">{lang('btn_bulk_sales_approval')}</a>
        {/if}

        <hr class="blank-10px" />

        <div id="sales_table" class="grid"></div>

        <hr class="blank-10px" />

        {if $can_create != 'none'}
            <a href="javascript:void(0)" class="btn-small btn-green btn-save">{lang('common_lbl_save')}</a>
            <a href="javascript:void(0)" class="btn-small btn-green btn-create-sales">{lang('btn_create_sales')}</a>
            {lang('lbl_slip_number')}<input type="text" class="slip_number">{lang('lbl_ni')}<a href="javascript:void(0)" class="btn-small btn-green btn-add-slip-number">{lang('common_lbl_add')}</a>
            <a href="javascript:void(0)" class="btn-small btn-green btn-apply">{lang('btn_bulk_apply')}</a>
        {/if}
        {if $can_approve == 'true'}
            <a href="javascript:void(0)" class="btn-small btn-green btn-select-all">{lang('btn_approval_select_all')}</a>
            <a href="javascript:void(0)" class="btn-small btn-green btn-approve">{lang('btn_bulk_sales_approval')}</a>
        {/if}
        <!-- END GRID -->

        <div id="dialogContainer" style="overflow:hidden"></div>
        <div id="errorContainer"><div class="popup-error"></div></div>
        <div id="dialog">
            {include file="order/detail.tpl"}
        </div>
    </div>
</div>
        
<script>
    // Order
    var UNAPPROVED = 1;
    var UNDETERMINED = 2;
    var CONFIRMED = 3;
    var CANCELLED = 4;
    var REJECTED = 9;
    var jsGlobals = [
        lbl_branch_cd = '{lang("lbl_branch_cd")}',
        lbl_order_status = '{lang("lbl_order_status")}',
        lbl_change_report = '{lang("lbl_change_report")}',
        lbl_product_name = '{lang("lbl_product_name")}',//3
        lbl_detail_contents = '{lang("lbl_detail_contents")}',
        lbl_delivery_date = '{lang("lbl_delivery_date")}',//5
        lbl_billing_date = '{lang("lbl_billing_date")}',
        lbl_payment_date = '{lang("lbl_payment_date")}',//7
        lbl_quantity = '{lang("lbl_quantity")}',
        lbl_tax_type = '{lang("lbl_tax_type")}',
        lbl_unit_price = '{lang("lbl_unit_price")}',
        lbl_amount = '{lang("lbl_amount")}',//11
        lbl_order_detail_cost_unit_price = '{lang("lbl_order_detail_cost_unit_price")}',
        lbl_order_detail_cost_total_price = '{lang("lbl_order_detail_cost_total_price")}',
        lbl_gross_profit_amount = '{lang("lbl_gross_profit_amount")}',
        lbl_order_detail_gross_profit_rate = '{lang("lbl_order_detail_gross_profit_rate")}',
        lbl_j_code = '{lang("lbl_j_code")}',
        lbl_s_code = '{lang("lbl_s_code")}',
        lbl_account_id = '{lang("lbl_account_id")}',
        lbl_is_ad_sales_slip_input = '{lang("lbl_is_ad_sales_slip_input")}',
        lbl_is_ad_invoice_issue = '{lang("lbl_is_ad_invoice_issue")}',
        lbl_is_order_input = '{lang("lbl_is_order_input")}',
        lbl_is_acceptance_input = '{lang("lbl_is_acceptance_input")}',
        lbl_is_sales_slip_input = '{lang("lbl_is_sales_slip_input")}',
        lbl_is_invoice_issue = '{lang("lbl_is_invoice_issue")}',
        lbl_name = '{lang("lbl_name")}',
        lbl_content = '{lang("lbl_content")}',
        lbl_action = '{lang("lbl_action")}',//27
        lbl_change_report = '{lang("lbl_change_report")}',
        lbl_copy_branch = '{lang("lbl_copy_branch")}',
        lbl_delete_branch = '{lang("lbl_delete_branch")}',
        lbl_cancel_branch = '{lang("lbl_cancel_branch")}',
        lbl_show = '{lang("lbl_show")}',
        lbl_check_approve_more = '{lang("lbl_check_approve_more")}',
        lbl_cancel_branch_release = '{lang("lbl_cancel_branch_release")}',
        edit_nothing_change_msg = '{lang("edit_nothing_change")}',
        change_report_nothing_change_msg = '{lang("change_report_nothing_change")}'
    ];
    var can_edit = 0;
    var can_approve = false;
    var can_invoices = '{$can_create}';
    var can_acceptance = true;
    var can_invoice_issue = true;
    var j_accounts = {json_encode($j_accounts)};
    var tax_division = {$tax_division};
    var bu_id_user_logged = {$bu_id_user_logged};
    var is_submitted = false;
    var my_account = {$this->order_model->my_account};
    // Check order acceptance permission
    // 発注・検収-発注検収力・変更
    var order_acceptance_input_change = {$order_acceptance_input_change};
    // 発注・検収-一覧・参照
    var order_acceptance_view = {$order_acceptance_view};

    var app = angular.module('app', ['ngTouch', 'ngSanitize', 'ui.grid', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.pagination', 'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.cellNav']);
    app.filter('mapTaxType', function() {
        return function(input) {
            if (!input) {
                return '';
            } else {
                for (i = 0; i < tax_division.length; i++) {
                    if (tax_division[i].code == input) {
                        return tax_division[i].name;
                    }
                }
            }
        };
    });
    app.filter("nl2br", function() {
        return function (data) {
            if (!data) return data;
            return data.replace(/\n\r?/g, '<br />');
        };
    });
    app.filter('mapYesNo', function() {
        var mapHash = {
            0 : '未',
            1 : '済'
        };

        return function(input) {
            if (!input){
                return '-';
            } else {
                return mapHash[input];
            }
        };
    });
    $("#dialog").dialog({
        autoOpen : false,
        width : "auto",
        height : "auto",
        modal : true,
        buttons : {
        },
        close : function() {
        }
    });
    // END Order

    var dialogContainer = $('#dialogContainer');
    var errorContainer = $('#errorContainer');
    var label = {
        {foreach $this->lang->language as $key => $value}
            {if substr($key, 0, 4) == 'lbl_' || substr($key, 0, 4) == 'btn_' || substr($key, 0, 4) == 'msg_'}
                {$key}: '{$value}',
            {/if}
        {/foreach}
    };
    label.lbl_edit = '変更';
    label.common_lbl_edit = '{lang('common_lbl_edit')}';
    label.common_lbl_no_data = '{lang('common_lbl_no_data')}';
//#7087:Start
    label.common_msg_over_record = '{lang('common_msg_over_record')}';
//#7087:End
    {*var data = {json_encode($data)}*}
    var status_options = {json_encode($this->sales_slip_model->status_options())};
    var tax_options_obj = {json_encode($this->sales_slip_model->tax_options())};
    var tax_options = [];
    for (var id in tax_options_obj) {
        if (tax_options_obj.hasOwnProperty(id)) {
            tax_options.push({ id: id, text: tax_options_obj[id] });
        }
    }
    var accounts = {json_encode(map_element($this->account_model->get_accounts(), 'id', 'name'))};
    var products = {json_encode(map_element($this->product_model->get_all_products(), 'id', 'name'))};
    {$current_business_unit = $this->product_model->get_business_unit_of_current_logged_user($this->auth->get_account_id())}
    {*var products = {json_encode($this->product_model->get_all_products($current_business_unit.id))}
    var all_products = {json_encode($this->product_model->get_all_products())}
    products = products.map(function (obj) {
        return {
            id: parseInt(obj.id),
            text: obj.name
        };
    });*}
    var current_login_id = {$this->auth->get_account_id()};
    var ss_can_approve = {$can_approve};
    var can_create = '{$can_create}';
    var closing_accountant_date = '{$closing_accountant_date}';

    lbl_open_search = '{lang('common_lbl_open_search')}';
    lbl_hide_search = '{lang('common_lbl_hide_search')}';

    if (loadData('ss_showSearch') == 0) {
        hideSearch();
    } else {
        showSearch();
    }

    $('#search_toggle').click(function () {
        if ($('#search_body').is(':visible')) {
            hideSearch(true);
        } else {
            showSearch(true);
        }
    });

    dialogContainer.dialog({
        autoOpen : false,
        modal : true,
        beforeClose: function( event, ui ) {
            return (is_submitted) ? true : close_confirm();
        },
        close : function() {
            form_changed = false;
            is_submitted = false;
        }
    });
    errorContainer.dialog({
        title: '確認',
        height: 'auto',
        autoOpen: false,
        minWidth: 580,
        minHeight: 250,
        maxHeight: 250,
        resizable: true,
        modal: false,
        dialogClass: 'fixed',
        position: { my: "right top+30", at: "right top+30", of: window},
        dragStart: function(event, ui) {
//            $('.acceptance-dialog').removeClass('fixed-dialog')
        },
        dragStop: function(event, ui) {
            // $('.acceptance-dialog').addClass('fixed-dialog')
        },
        resizeStart: function( event, ui ) {
            $(this).dialog("option", "maxHeight", false);
        }
    });

    $(window).bind('beforeunload', function(){
        // Trigger when form_changed return true then show confirm navigation, else dont show
        if (form_changed)
            return 'このページから移動しますか？ 入力したデータは保存されません。';
    });

</script>
<script src="/public/admin/order/detail.js"></script>

{include file = "inc/footer.tpl"}
