<style rel="styleSheet">
.account_name{
    width: 150px;
}
.department_name, .business_unit_name{
    width: 310px;
}
.business_unit_name{
    padding: 5px;
}

</style>
<h4 class="page-title">{lang('lbl_by_account')}</h4>
<table class="table-theme odd table-account">
    <tr>
        <th class="account_name">{lang('col_account_name')}</th>
        <th class="account_name">{lang('col_reponsible_employee_name')}</th>
        <th class="value_column">{lang('col_target')}</th>
        <th class="value_column">{lang('col_monthly_report')}</th>
        <th class="value_column">{lang('col_achivement_rate')}</th>
        <th class="value_column">{lang('col_last_month_profit')}</th>
        <th class="value_column">{lang('col_last_month_summary')}</th>
        <th class="value_column">{lang('col_cancel')}</th>
        <th class="value_column">{lang('col_current_month_summary')}</th>
        <th class="value_column">{lang('col_last_month_report')}</th>
        <th class="value_column">{lang('col_sales')}</th>
    </tr>
    {if (isset($list_acounts) && !empty($list_acounts))}
    
    {foreach from=$list_acounts item=p}
    <tr>
        <td>{$p.department_name}</td>
        <td>{$p.account_name}</td>
        <td class="number_column">{number_format($p.target_amount)}</td>
        <td class="number_column">{number_format($p.monthly_report)}</td>
<!-- #7617:Start -->
        <td class="number_column">{number_format($p.target_achieve_percentage,2,".",",")}</td>
<!-- #7617:End -->
        <td class="number_column">{number_format($p.last_month_prophit)}</td>
        <td class="number_column">{number_format($p.last_month_summary_month)}</td>
        <td class="number_column">{number_format($p.cancel)}</td>
        <td class="number_column">{number_format($p.summary_month)}</td>
        <td class="number_column">{number_format($p.last_month_report)}</td>
        <td class="number_column">{number_format($p.sales)}</td>
                        
    </tr>
    {/foreach} {else}
    <tr>
        <td class="text-center" colspan="11">{lang('common_lbl_no_data')}</td>
    </tr>
    {/if}
   
</table>

<hr class="blank-10px" />
{if isset($pagination)} {$pagination.links} {/if}

<h4 class="page-title">{lang('lbl_by_department')}</h4>
<table class="table-theme odd table-department">
    <tr>
        <th class="department_name">{lang('col_department_name')}</th>
        <th class="value_column">{lang('col_target')}</th>
        <th class="value_column">{lang('col_monthly_report')}</th>
        <th class="value_column">{lang('col_achivement_rate')}</th>
        <th class="value_column">{lang('col_last_month_profit')}</th>
        <th class="value_column">{lang('col_last_month_summary')}</th>
        <th class="value_column">{lang('col_cancel')}</th>
        <th class="value_column">{lang('col_current_month_summary')}</th>
        <th class="value_column">{lang('col_last_month_report')}</th>
        <th class="value_column">{lang('col_sales')}</th>
    </tr>
    {if (isset($list_departments) && !empty($list_departments))}
    {foreach from=$list_departments item=p}
   <tr>
        <td>{$p.department_name}</td>
        <td class="number_column">{number_format($p.target_amount)}</td>
        <td class="number_column">{number_format($p.monthly_report)}</td>
<!-- #7617:Start -->
        <td class="number_column">{number_format($p.target_achieve_percentage,2,".",",")}</td>
<!-- #7617:End -->
        <td class="number_column">{number_format($p.last_month_prophit)}</td>
        <td class="number_column">{number_format($p.last_month_summary_month)}</td>
        <td class="number_column">{number_format($p.cancel)}</td>
        <td class="number_column">{number_format($p.summary_month)}</td>
        <td class="number_column">{number_format($p.last_month_report)}</td>
        <td class="number_column">{number_format($p.sales)}</td>             
    </tr>
    {/foreach} {else}
    <tr>
        <td class="text-center" colspan="11">{lang('common_lbl_no_data')}</td>
    </tr>
    {/if}
   
</table>

<hr class="blank-10px" />
{if isset($pagination)} {$pagination.links} {/if}

<h4 class="page-title">{lang('lbl_by_business')}</h4>
<table class="table-theme odd table-business">
    <tr>
        <th class="business_unit_name" width="330px">{lang('col_business_unit_name')}</th>
        <th class="value_column">{lang('col_target')}</th>
        <th class="value_column">{lang('col_monthly_report')}</th>
        <th class="value_column">{lang('col_achivement_rate')}</th>
        <th class="value_column">{lang('col_last_month_profit')}</th>
        <th class="value_column">{lang('col_last_month_summary')}</th>
        <th class="value_column">{lang('col_cancel')}</th>
        <th class="value_column">{lang('col_current_month_summary')}</th>
        <th class="value_column">{lang('col_last_month_report')}</th>
        <th class="value_column">{lang('col_sales')}</th>
    </tr>
    {if (isset($list_business_units) && !empty($list_business_units))}
    {foreach from=$list_business_units item=p}
   <tr>
        <td>{$p.department_name}</td>
        <td class="number_column">{number_format($p.target_amount)}</td>
        <td class="number_column">{number_format($p.monthly_report)}</td>
<!-- #7617:Start -->
        <td class="number_column">{number_format($p.target_achieve_percentage,2,".",",")}</td>
<!-- #7617:End -->
        <td class="number_column">{number_format($p.last_month_prophit)}</td>
        <td class="number_column">{number_format($p.last_month_summary_month)}</td>
        <td class="number_column">{number_format($p.cancel)}</td>
        <td class="number_column">{number_format($p.summary_month)}</td>
        <td class="number_column">{number_format($p.last_month_report)}</td>
        <td class="number_column">{number_format($p.sales)}</td>                 
    </tr>
    {/foreach} {else}
    <tr>
        <td class="text-center" colspan="11">{lang('common_lbl_no_data')}</td>
    </tr>
    {/if}
   
</table>

<hr class="blank-10px" />
{if isset($pagination)} {$pagination.links} {/if}