<!-- Show list of products -->
{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<link rel="styleSheet" href="/public/admin/css/default.css" />
<script src="/public/admin/js/angular.js"></script>
<script src="/public/admin/js/angular-touch.js"></script>
<script src="/public/admin/js/angular-animate.js"></script>
<script src="/public/admin/js/angular-sanitize.min.js"></script>
<script src="/public/admin/js/ui-grid-unstable.js"></script>
<link rel="styleSheet" href="/public/admin/css/ui-grid-unstable.css" />
<script type="text/javascript">
var data_list = {$data_list};
</script>
<style rel="styleSheet">
.ui-grid-pager-panel {
    visibility: hidden;
}
.grid {
    margin-bottom: 5px;
    width: 550px;
    height: 350px;
}

.green-row {
    background-color: #F5A9D0 !important;
}

.watermark {
    display: inline-block;
}

input.ng-scope {
    line-height: 30px !important;
}

select.ng-scope {
    height: 30px !important;
}
    
button.btn-small {
    /*margin-bottom: 20px;*/
}

.sales-target-container table,#report-content {
    font-size: 10px;
}

.table-theme {
    
}

.table-account th:first-child, .table-department th:first-child, .table-business th:first-child {
    text-align: center !important;
} 

</style>
    <script>
// jsGlobal variable
label = {
        name: "氏名",
        department: "部署",
        target: "目標粗利額"
    };
    btn = {
        add_new: lang['button_add'],
        delete_row: lang['button_delete_ad_pay']
    };
error_msg = {
        datepicker_from: "対象年月は無効な日付です"
};
$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});
</script>
<div class="cell content slide-right sales-target-container">
    <div ng-controller="saleTarget">
        <div ui-i18n="{literal}{{lang}}{/literal}"></div>
        <h1>{$title}</h1>
        <form action="" method="post">
        <h2 class="report_content_title">{lang('target')}</h2>
        <table border="1" class="tbl_search_sales_target table-simple">
            <tr>
                <th width="100px">
                               {lang('lbl_search_from_to')}<span class="red-symbol"> ※</span>

                </th>
                <td>
                    <input type="text" name="datepicker_from" ng-model="search.datepicker_from" ng-value="datepicker_from" /><!-- ～<input type="text" name="datepicker_to" ng-value="datepicker_to" /> --><br/>
<!--                     <div class="legend">{lang('lbl_search_from_to_description')} </div> -->
                    <div class="error"></div>
                </td>
            </tr>
            <tr>
                <th width="100px">
                     <!-- c -->{lang('lbl_search_division')}<span class="red-symbol"> ※</span>
                </th>
                <td>
                    <div style="display: inline-block; margin-right: 10px;">
                        {if $is_product_division}
                            {form_dropdown([
                            'name' => 'business_unit_id',
                            'items' => $business_options,
                            'ng-model' => 'search.business_unit_id',
                            'value' => set_value('business_unit_id', $business_unit_id)
                            ])}
                        {else}
                            {form_dropdown([
                            'name' => 'business_unit_id',
                            'items' => $business_options,
                            'ng-model' => 'search.business_unit_id',
                            'value' => set_value('business_unit_id', '')
                            ])}
                        {/if}
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="a-center"><button type="button" name="search" ng-click="filter(search)" class="btn-small btn-green btn-search" value="search">{lang('lbl_search')}</button></td>
            </tr>
        </table><br/>
        <div>
            <!-- <button type="button" name="search" ng-click="filter(search)" class="btn-small btn-green btn-search" value="search">{lang('common_lbl_search')}</button> -->
<!--             <button type="submit" name="print" class="btn-small btn-green" value="print" ng-click="printPDF(rows)">{lang('lbl_print_button_description')}</button> -->
        </div>
    </form>

<!--         <hr class="blank-10px" />
        <h1 style="border-color: rgb(68, 147, 208);"></h1>
        <hr class="blank-10px" /> -->

        <div ng-if="displayTable" ng-repeat="row in rows">
            <div ui-grid="row.gridOption" class="grid" ui-grid-pinning ui-grid-resize-columns ui-grid-pagination ui-grid-edit ui-grid-row-edit>
                <div class="watermark" ng-if="displayNoData">
                    {lang('common_lbl_no_data')}
                </div>
            </div>
            <button ng-if="displayBtnSave" class='btn-small btn-green btn-save' ng-click="saveGrid(rows, row.gridOption.data, row)">{lang('common_lbl_save')}</button>
            <br/>
        </div>
    </div>
</div>
<div id="dialog-status" style="display: none"></div>
<!-- e/content -->
<script src="/public/admin/sales_target/sales_target.js"></script>
{include file = "inc/footer.tpl"}
