<!-- Show list of products -->
{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<link rel="styleSheet" href="/public/admin/css/default.css" />
<script src="/public/admin/js/angular.js"></script>
<script src="/public/admin/js/angular-touch.js"></script>
<script src="/public/admin/js/angular-animate.js"></script>
<script src="/public/admin/js/angular-sanitize.min.js"></script>
<script src="/public/admin/js/ui-grid-unstable.js"></script>
<link rel="styleSheet" href="/public/admin/css/ui-grid-unstable.css" />
<style rel="styleSheet">
.ui-grid-pager-panel {
    visibility: hidden;
}
.grid {
    margin-bottom: 5px;
    width: 550px;
    height: 350px;
}

.green-row {
    background-color: #F5A9D0 !important;
}

.watermark {
    display: inline-block;
}

input.ng-scope {
    line-height: 30px !important;
}

select.ng-scope {
    height: 30px !important;
}
    
button.btn-small {
    /*margin-bottom: 20px;*/
}

.sales-target-container table,#report-content {
    font-size: 10px;
}

.table-theme {
    
}

.table-account th:first-child, .table-department th:first-child, .table-business th:first-child {
    text-align: center !important;
} 

</style>
<script>
    // jsGlobal variable
    label = {
        name: "氏名",
        department: "部署",
        target: "目標粗利額"
    };
    
    btn = {
        add_new: lang['button_add'],
        delete_row: lang['button_delete_ad_pay']
    };
    
    error_msg = {
        datepicker_from: "対象年月は無効な日付です"
    };
    
    $(document).ready(function() {
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
    });
});

</script>
<!-- e/content -->

<div class="cell content slide-right sales-target-container">
    <div>
        <h1>{$title}</h1>
        <hr class="blank-10px" />
         <form action="" method="post">
                <h2 class="report_content_title">{lang('report')}</h2>
                <table class="table-simple table-input-sales-report" border="1">
                    <tr>
                        <th width="100px">{lang('lbl_target_date')}<span class="red-symbol"> ※</span></th>
                        <td>
                            <input type="text" name="time" value="{$time}" />
                            <span class="validation-error time-error" id="error_empty_date" style="display: none">{lang('error_empty_date')}</span>
                            <span class="validation-error time-error" id="error_date_format" style="display: none">{lang('error_message_time_format')}</span>
                        </td>
                    </tr>
                    <tr>
                        <th width="100px">{lang('lbl_department_name')}<span class="red-symbol"> ※</span></th>
                        <td>
                            <select name="business_unit" class="full-width">
                                <option value="-1">{lang('common_lbl_all')}</option>
                                {foreach $business_unit_options as $business_unit_option}
                                <option value="{$business_unit_option.id}" {if $business_unit == $business_unit_option.id}{'selected'}{/if}  >{$business_unit_option.name}</option>
                                {/foreach}
                            </select>
                        </td>
                    </tr>
                   
                    <tr>
                         <td colspan="2" class="a-center"><button type="button" name="search-report-btn" class="btn-small btn-green btn-search" value="search">{lang('lbl_search')}</button></td>
                    </tr>
                </table>
                <br class="br-50px" />
            </form>
        <div id="report-content">
            <h4 class="page-title">{lang('lbl_by_account')}</h4>
            <table class="table-theme odd table-account">
                <tr>
                    <th class="account_name">{lang('col_account_name')}</th>
                    <th class="account_name">{lang('col_reponsible_employee_name')}</th>
                    <th class="value_column">{lang('col_target')}</th>
                    <th class="value_column">{lang('col_monthly_report')}</th>
                    <th class="value_column">{lang('col_achivement_rate')}</th>
                    <th class="value_column">{lang('col_last_month_profit')}</th>
                    <th class="value_column">{lang('col_last_month_summary')}</th>
                    <th class="value_column">{lang('col_cancel')}</th>
                    <th class="value_column">{lang('col_current_month_summary')}</th>
                    <th class="value_column">{lang('col_last_month_report')}</th>
                    <th class="value_column">{lang('col_sales')}</th>
                </tr>
                {if (isset($list_acounts) && !empty($list_acounts))}
                
                {foreach from=$list_acounts item=p}
                <tr>
                    <td>{$p.department_name}</td>
                    <td>{$p.account_name}</td>
                    <td class="number_column">{number_format($p.target_amount)}</td>
                    <td class="number_column">{number_format($p.monthly_report)}</td>
<!-- #7617:Start -->
                    <td class="number_column">{number_format($p.target_achieve_percentage,2,".",",")}</td>
<!-- #7617:End -->
                    <td class="number_column">{number_format($p.last_month_prophit)}</td>
                    <td class="number_column">{number_format($p.last_month_summary_month)}</td>
                    <td class="number_column">{number_format($p.cancel)}</td>
                    <td class="number_column">{number_format($p.summary_month)}</td>
                    <td class="number_column">{number_format($p.last_month_report)}</td>
                    <td class="number_column">{number_format($p.sales)}</td>
                                    
                </tr>
                {/foreach} {else}
                <tr>
                    <td class="text-center" colspan="11">{lang('common_lbl_no_data')}</td>
                </tr>
                {/if}
               
            </table>

            <hr class="blank-10px" />
            {if isset($pagination)} {$pagination.links} {/if}

            <h4 class="page-title">{lang('lbl_by_department')}</h4>
            <table class="table-theme odd table-department">
                <tr>
                    <th class="department_name">{lang('col_department_name')}</th>
                    <th class="value_column">{lang('col_target')}</th>
                    <th class="value_column">{lang('col_monthly_report')}</th>
                    <th class="value_column">{lang('col_achivement_rate')}</th>
                    <th class="value_column">{lang('col_last_month_profit')}</th>
                    <th class="value_column">{lang('col_last_month_summary')}</th>
                    <th class="value_column">{lang('col_cancel')}</th>
                    <th class="value_column">{lang('col_current_month_summary')}</th>
                    <th class="value_column">{lang('col_last_month_report')}</th>
                    <th class="value_column">{lang('col_sales')}</th>
                </tr>
                {if (isset($list_departments) && !empty($list_departments))}
                {foreach from=$list_departments item=p}
               <tr>
                    <td>{$p.department_name}</td>
                    <td class="number_column">{number_format($p.target_amount)}</td>
                    <td class="number_column">{number_format($p.monthly_report)}</td>
<!-- #7617:Start -->
                    <td class="number_column">{number_format($p.target_achieve_percentage,2,".",",")}</td>
<!-- #7617:End -->
                    <td class="number_column">{number_format($p.last_month_prophit)}</td>
                    <td class="number_column">{number_format($p.last_month_summary_month)}</td>
                    <td class="number_column">{number_format($p.cancel)}</td>
                    <td class="number_column">{number_format($p.summary_month)}</td>
                    <td class="number_column">{number_format($p.last_month_report)}</td>
                    <td class="number_column">{number_format($p.sales)}</td>             
                </tr>
                {/foreach} {else}
                <tr>
                    <td class="text-center" colspan="11">{lang('common_lbl_no_data')}</td>
                </tr>
                {/if}
               
            </table>
            
            <hr class="blank-10px" />
            {if isset($pagination)} {$pagination.links} {/if}

            <h4 class="page-title">{lang('lbl_by_business')}</h4>
            <table class="table-theme odd table-business">
                <tr>
                    <th class="business_unit_name">{lang('col_business_unit_name')}</th>
                    <th class="value_column">{lang('col_target')}</th>
                    <th class="value_column">{lang('col_monthly_report')}</th>
                    <th class="value_column">{lang('col_achivement_rate')}</th>
                    <th class="value_column">{lang('col_last_month_profit')}</th>
                    <th class="value_column">{lang('col_last_month_summary')}</th>
                    <th class="value_column">{lang('col_cancel')}</th>
                    <th class="value_column">{lang('col_current_month_summary')}</th>
                    <th class="value_column">{lang('col_last_month_report')}</th>
                    <th class="value_column">{lang('col_sales')}</th>
                </tr>
                {if (isset($list_business_units) && !empty($list_business_units))}
                {foreach from=$list_business_units item=p}
               <tr>
                    <td>{$p.department_name}</td>
                    <td class="number_column">{number_format($p.target_amount)}</td>
                    <td class="number_column">{number_format($p.monthly_report)}</td>
<!-- #7617:Start -->
                    <td class="number_column">{number_format($p.target_achieve_percentage,2,".",",")}</td>
<!-- #7617:End -->
                    <td class="number_column">{number_format($p.last_month_prophit)}</td>
                    <td class="number_column">{number_format($p.last_month_summary_month)}</td>
                    <td class="number_column">{number_format($p.cancel)}</td>
                    <td class="number_column">{number_format($p.summary_month)}</td>
                    <td class="number_column">{number_format($p.last_month_report)}</td>
                    <td class="number_column">{number_format($p.sales)}</td>                 
                </tr>
                {/foreach} {else}
                <tr>
                    <td class="text-center" colspan="11">{lang('common_lbl_no_data')}</td>
                </tr>
                {/if}
               
            </table>
            
            <hr class="blank-10px" />
            {if isset($pagination)} {$pagination.links} {/if}
        </div>
    </div>
    
</div>
<script src="/public/admin/sales_target/sales_target.js"></script>
{include file = "inc/footer.tpl"}
