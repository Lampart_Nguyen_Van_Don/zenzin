<html>
<head>
<script>
    $("#close_form").submit(function(){
        if( !$('#message').is(':empty') ) {
            $("#dialog_form_c").dialog("close");
            return true;
        } else {
             $("#dialog_form_c").dialog("close");
             return false;
        }
    });

    $(document).on('click', '#save_add', function() {
        url = "/_admin/department/edit_c_ajax/";
        $.post( url,
                {
                  // d_id            : "1",
                  business_unit_id: $("#business_unit_id").val(),
                  name            : $("#name").val(),
                  zipcode        : $("#zipcode").val(),
                  // zipcode2        : $("#zipcode2").val(),
                  address_1       : $("#address_1").val(),
                  address_2       : $("#address_2").val(),
                  phone_no        : $("#phone_no").val(),
                  fax_no          : $("#fax_no").val(),
                  type			  : "add"
                },
                function( data ) {
                    // If has error messages
                    if (data["error"] == 1) {
                        // $("#message").html("Validation fail!");
                        // Remove old error messages
                        $(".validation-error").remove();
                        // Show error messages
                        $("#business_unit_id").after(data["business_unit_id"]);
                        $("#name").after(data["name"]);
                        $("#zipcode").after(data["zipcode"]);
                        // $("#zipcode2").after(data["zipcode2"]);
                        $("#address_1").after(data["address_1"]);
                        $("#address_2").after(data["address_2"]);
                        $("#phone_no").after(data["phone_no"]);
                        $("#fax_no").after(data["fax_no"]);
                    } else {
                        $(".validation-error").remove();
                        // Successful message
                        $("#message").html($( "#common_insert_successfully" ).val());
                        $("#dialog_form_c").dialog( "option", "height", 450 );
                    }
                },
                "json");
    });
//});
</script>
</head>
<body>
    <div class="cell content">
        <div>
            <hr class="blank-10px" />
            <!-- <form action="/_admin/department/edit_c_ajax/" id="edit_c_form" method="POST"> -->
            <div id="message" class="alert-success"></div>
            <table class="table-theme">
                    <tr>
                        <td width="45%">{lang('common_lbl_business_unit')}</td>
                        <td>
                            <select name="business_unit_id" id="business_unit_id">
                                {if $bu} {foreach from=$bu item=b}
                                    <option value="{$b.id}">{$b.name}</option>
                                {/foreach} {/if}
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>{lang('common_lbl_department_name')}<span class="red-symbol">※</span></td>
                        <td>
                            <input type="text" value="{if isset($form.name)}{$form.name}{/if}" name="name" id="name"/>
                            <br/>{form_error('name')}
                        </td>
                    </tr>
                    <tr>
                        <td>{lang('common_lbl_department_zipcode')}<span class="red-symbol">※</span></td>
                        <td>
                        <input type="text" value="{if isset($form.zipcode)}{$form.zipcode}{/if}" name="zipcode" id="zipcode"/>                        <br/>
                        {form_error('zipcode')}
                        </td>
                    </tr>
                    <tr>
                        <td>{lang('common_lbl_department_address1')}<span class="red-symbol">※</span></td>
                        <td>
                            <input type="text" value="{if isset($form.address_1)}{$form.address_1}{/if}" name="address_1" id="address_1"/>
                            <br/>
                            {form_error('address_1')}
                        </td>
                    </tr>
                    <tr>
                        <td>{lang('common_lbl_department_address2')}</td>
                        <td>
                            <input type="text" value="{if isset($form.address_2)}{$form.address_2}{/if}" name="address_2" id="address_2"/>
                        </td>
                    </tr>
                    <tr>
                        <td>TEL<span class="red-symbol">※</span></td>
                        <td>
                            <input type="text" value="{if isset($form.phone_no)}{$form.phone_no}{/if}" name="phone_no" id="phone_no"/>
                            <br/>
                            {form_error('phone_no')}
                        </td>
                    </tr>
                    <tr>
                        <td>FAX<span class="red-symbol">※</span></td>
                        <td>
                            <input type="text" value="{if isset($form.fax_no)}{$form.fax_no}{/if}" name="fax_no" id="fax_no"/>
                            <br/>{form_error('fax_no')}
                        </td>
                    </tr>
                <tr>
                    <td colspan="2">
                            <div style="float: left;">
                                <button name="save_add" id="save_add" value="Save" class = 'btn-small btn-green btn-save'>{lang('common_lbl_save')}</button>
                            </div>
                            <div style="float: left;">
                                <form method="POST" action="/_admin/sample/show_department" id="close_form">
                                    {make_search_hidden(FALSE)}
                                    <button name="close_copy_c" type="submit" id = "close_copy_c" class="btn-small btn-green btn-close"
                                    value="Close">{lang('common_lbl_close')}</button>
                                </form>
                            </div>
                    </td>
                </tr>
            </table>
            <!-- </form> -->
            <hr class="blank-10px" />
        </div>
    </div>
</body>
</html>
