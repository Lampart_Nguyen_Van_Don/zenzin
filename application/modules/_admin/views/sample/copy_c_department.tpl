<html>
<head>
<script>
//$(document).ready(function() {
    // Close detail c dialog box
    /*$(document).on('click', '#close_copy_c', function() {
    // $("#close").click(function( event ) {
           //event.preventDefault();
           $("#dialog_form_c").dialog("close");
           if(!$('#message').is(':empty') ) {
               location.reload();
           }
    });*/


    $("#close_form").submit(function(){
        if( !$('#message').is(':empty') ) {
            $("#dialog_form_c").dialog("close");
            return true;
        } else {
             $("#dialog_form_c").dialog("close");
             return false;
        }
    });






    $(document).on('click', '#save_copy', function() {
        //$("#save_copy").click(function( event ) {
        //event.preventDefault();
        // var $form = $("#edit_c_form"),
        // $(".validation-error").remove();
        // term = $form.find( "input[name='s']" ).val(),
        // url = $form.attr( "action" );
        url = "/_admin/sample/edit_c_ajax/";
        $.post( url,
                {
                  d_id            : $("#d_id").val(),
                  business_unit_id: $("#business_unit_id").val(),
                  name            : $("#name").val(),
                  zipcode         : $("#zipcode").val(),
                  //zipcode2        : $("#zipcode2").val(),
                  address_1       : $("#address_1").val(),
                  address_2       : $("#address_2").val(),
                  phone_no        : $("#phone_no").val(),
                  fax_no          : $("#fax_no").val(),
                  type			  : "copy"
                },
                function( data ) {
                    // If has error messages
                    if (data["error"] == 1) {
                        // Remove old error messages
                        $(".validation-error").remove();
                        // $("#message").html("");
                        // Show error messages
                        $("#business_unit_id").after(data["business_unit_id"]);
                        $("#name").after(data["name"]);
                        $("#zipcode").after(data["zipcode"]);
                        // $("#zipcode2").after(data["zipcode2"]);
                        $("#address_1").after(data["address_1"]);
                        $("#address_2").after(data["address_2"]);
                        $("#phone_no").after(data["phone_no"]);
                        $("#fax_no").after(data["fax_no"]);
                    } else {
                        $(".validation-error").remove();
                        // Successful message
                        $("#message").html($( "#common_insert_successfully" ).val());
                        $("#dialog_form_c").dialog( "option", "height", 450 );
                    }
                },
                "json");
    });
//});
</script>
</head>
<body>
    <div class="cell content">
        <div>
            <hr class="blank-10px" />
            <!-- <form action="/_admin/sample/edit_c_ajax/" id="edit_c_form" method="POST"> -->
            <div id="message" class="alert-success"></div>
            <table class="table-theme">
                {if $d}
                <tr>
                    <td width="45%">{lang('common_lbl_business_unit')}</td>
                    <td><input type="hidden" name="d_id" id="d_id" value="{$d.id}" /> <select
                        name="business_unit_id" id="business_unit_id"> {if $bu} {foreach from=$bu item=b}
                            <option value="{$b.id}" {if ($b.id==$d.business_unit_id)}selected{/if}>{$b.name}</option>
                            {/foreach} {/if}
                    </select></td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_name')}</td>
                    <td><input type="text" value="{if isset($form.name)}{$form.name}{else}{$d.name}{/if}" name="name" id="name" />
                    {form_error('name')}
                    </td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_zipcode')}</td>
                    <td>
                     <input type="text"
                        value="{if isset($form.zipcode1)}{$form.zipcode1}{else}{$d.zipcode}{/if}"
                        name="zipcode" id="zipcode" />{form_error('zipcode')}
                    <!--
                    <input type="text"
                        value="{if isset($form.zipcode1)}{$form.zipcode1}{else}{$d.zipcode|substr:0:3}{/if}"
                        style="width: 40px;" name="zipcode1" id="zipcode1" /> - <input type="text"
                        style="width: 50px;"
                        value="{if isset($form.zipcode2)}{$form.zipcode2}{else}{$d.zipcode|substr:3:7}{/if}"
                        name="zipcode2" id="zipcode2" />{form_error('zipcode1')}{form_error('zipcode2')}
                    -->
                    </td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_address1')}</td>
                    <td><input type="text"
                        value="{if isset($form.address_1)}{$form.address_1}{else}{$d.address_1}{/if}"
                        name="address_1" id="address_1" />{form_error('address_1')}</td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_address2')}</td>
                    <td><input type="text"
                        value="{if isset($form.address_2)}{$form.address_2}{else}{$d.address_2}{/if}"
                        name="address_2" id="address_2" /></td>
                </tr>
                <tr>
                    <td>TEL</td>
                    <td><input type="text"
                        value="{if isset($form.phone_no)}{$form.phone_no}{else}{$d.phone_no}{/if}"
                        name="phone_no" id="phone_no" />{form_error('phone_no')}</td>
                </tr>
                <tr>
                    <td>FAX</td>
                    <td><input type="text"
                        value="{if isset($form.fax_no)}{$form.fax_no}{else}{$d.fax_no}{/if}"
                        name="fax_no" id="fax_no" />{form_error('fax_no')}</td>
                </tr>
                {else}
                <tr>
                    <th colspan="2">Nodata</th>
                </tr>
                {/if}
                <tr>
                    <td colspan=2>
                            <div style="float: left;">
                            <button name="save_copy" id="save_copy" value="Save" class = 'btn-small btn-green btn-save'>{lang('common_lbl_save')}</button>
                            </div>
                            <div style="float: left;">
                                <form method="POST" action="/_admin/sample/show_department" id="close_form">
                                    <!-- {if isset($search_conditions)}
                                    <div id="search_conditions">
                                        <input type="hidden" name="search_business_unit" value="{$search_conditions['search_business_unit']}"/>
                                        <input type="hidden" name="search_name" value="{$search_conditions['search_name']}"/>
                                        <input type="hidden" name="has_search_value" value="has_value_search"/>
                                    </div>
                                    {/if} -->
                                    {make_search_hidden(FALSE)}
                                    <button name="close_copy_c" type="submit" id = "close_copy_c" class="btn-small btn-green btn-close"
                                    value="Close">{lang('common_lbl_close')}</button>
                                </form>
                            </div>
                    </td>
                </tr>
            </table>
            <!-- </form> -->
            <hr class="blank-10px" />
        </div>
    </div>
</body>

</html>
