{include file = "inc/header.tpl"} {include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<div class="cell content">
    <div>
        <!--  -->
        <h1>{lang('common_copy_department')}</h1>
        <hr class="blank-10px" />
        <form method="POST" action="/_admin/sample/copy_department">
            <table class="table-theme">
                {if $d}
                    <tr>
                        <td width="45%">{lang('common_lbl_business_unit')}</td>
                        <td>
                            <input type="hidden" name="did" value="{$d.id}"/>
                            <select name="business_unit_id">
                                {if $bu} {foreach from=$bu item=b}
                                    <option value="{$b.id}" {if ($b.id == $d.business_unit_id)}selected{/if}>{$b.name}</option>
                                {/foreach} {/if}
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>{lang('common_lbl_department_name')}</td>
                        <td>
                            <input type="text" value="{if isset($form.name)}{$form.name}{else}{$d.name}{/if}" name="name"/>
                            <br/>{form_error('name')}
                        </td>
                    </tr>
                    <tr>
                        <td>{lang('common_lbl_department_zipcode')}</td>
                        <td>
                        <input type="text" value="{if isset($form.zipcode)}{$form.zipcode}{else}{$d.zipcode}{/if}" name="zipcode"/>
                        <br/>
                        {form_error('zipcode')}
                        </td>
                    </tr>
                    <tr>
                        <td>{lang('common_lbl_department_address1')}</td>
                        <td>
                            <input type="text" value="{if isset($form.address_1)}{$form.address_1}{else}{$d.address_1}{/if}" name="address_1"/>
                            <br/>{form_error('address_1')}
                        </td>
                    </tr>
                    <tr>
                        <td>{lang('common_lbl_department_address2')}</td>
                        <td>
                            <input type="text" value="{if isset($form.address_2)}{$form.address_2}{else}{$d.address_2}{/if}" name="address_2"/>
                        </td>
                    </tr>
                    <tr>
                        <td>TEL</td>
                        <td>
                            <input type="text" value="{if isset($form.phone_no)}{$form.phone_no}{else}{$d.phone_no}{/if}" name="phone_no"/>
                            <br/>{form_error('phone_no')}
                        </td>
                    </tr>
                    <tr>
                        <td>FAX</td>
                        <td>
                            <input type="text" value="{if isset($form.fax_no)}{$form.fax_no}{else}{$d.fax_no}{/if}" name="fax_no"/>
                            <br/>{form_error('fax_no')}
                        </td>
                    </tr>
                {else}
                    <tr>
                        <th colspan="2">{lang('common_lbl_no_data')}</th>
                    </tr>
                {/if}
                <tr>
                    <td colspan="2">
                        <input type="hidden" name="d_id" value="{$d.id}"/>
                        <div style="float:left;">
                             <button name="save_copy" value="{lang('common_lbl_add')}" type="submit" class="btn-small btn-green btn-save">{lang('common_lbl_save')}</button>
                        </div>
                        <div style="float:left;">
                            <!-- {form_back_button('/_admin/department/show')} -->
                            <button name="back" type="submit" id="" value="back" class="btn-small btn-gray btn-back">{lang('common_lbl_comeback')}</button>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
        <hr class="blank-10px" />
    </div>
</div>
<!-- e/content -->
</div>
{include file = "inc/footer.tpl"}
