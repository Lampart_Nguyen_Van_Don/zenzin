{include file = "inc/header.tpl"} {include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

<script>
$(document).ready(function() {
     // Load type b
    $(document).on("click", "#delete", function() {
        return confirm("{lang('message_sure_to_delete')}");
    });

});

</script>

<div class="cell content">
    <div>
        <!--  -->
        <h1>{lang('common_detail_department')}</h1>
        <hr class="blank-10px" />
        <table class="table-theme">
            {if $d}
            <tr>
                <td width="40%">{lang('common_lbl_business_unit')}</td>
                <td><input type="hidden" name="did" value="{$d.id}" />
                    {$d.business_unit_name}</td>
            </tr>
            <tr>
                <td>{lang('common_lbl_department_name')}</td>
                <td>{$d.name}</td>
            </tr>
            <tr>
                <td>{lang('common_lbl_department_zipcode')}</td>
                <td>{$d.zipcode|substr:0:3} - {$d.zipcode|substr:3:7}</td>
            </tr>
            <tr>
                <td>{lang('common_lbl_department_address1')}</td>
                <td>{$d.address_1}</td>
            </tr>
            <tr>
                <td>{lang('common_lbl_department_address2')}</td>
                <td>{$d.address_2}</td>
            </tr>
            <tr>
                <td>TEL</td>
                <td>{$d.phone_no}</td>
            </tr>
            <tr>
                <td>FAX</td>
                <td>{$d.fax_no}</td>
            </tr>
            {else}
            <tr>
                <th colspan="2">Nodata</th>
            </tr>
            {/if}
            <tr>
                <td colspan="2">
                    <div style="float: left;">
                        <form method="POST" action="/_admin/sample/edit_department" style="">
                            <input type="hidden" value="{$d.id}" name="d_id"  />
                            <!-- <input type="submit" name="edit" class="btn-small btn-green btn-edit" value="{lang('common_lbl_edit')}" /> -->
                            {$data.class = 'btn-small btn-green btn-edit'}
                            {$data.uri = ''}
                            {$data.name = 'edit'}
                            {$data.id = 'edit'}
                            {$data.value = lang('common_lbl_edit')}
                            {$data.content = lang('common_lbl_edit')}
                            {generate_action_button($data)}
                        </form>
                    </div>
                    <div style="float: left;">
                        <form method="POST" action="/_admin/sample/copy_department">
                            <input type="hidden" value="{$d.id}" name="d_id" />
                            <!-- <input type="submit" name="copy" class="btn-small btn-green btn-copy" value="{lang('common_lbl_copy')}" /> -->
                            {$copy.class = 'btn-small btn-green btn-copy'}
                            {$copy.uri = ''}
                            {$copy.name = 'copy'}
                            {$copy.id = 'copy'}
                            {$copy.value = lang('common_lbl_copy')}
                            {$copy.content = lang('common_lbl_copy')}
                            {generate_action_button($copy)}
                        </form>
                    </div>
                    <div style="float: left;">
                        <form method="POST" action="/_admin/sample/show_department">
                            {form_back_button('/_admin/sample/show')}
                        </form>
                    </div>
                    <div style="float: left;">
                        <form method="POST" action="/_admin/sample/edit_department">
                            <input type="hidden" value="{$d.id}" name="d_id" />
                            <!-- <input
                                type="submit"
                                onClick="return confirm('{lang('message_sure_to_delete')}');"
                                name="delete" value="{lang('common_lbl_delete')}" class="btn-small btn-green btn-delete" />
                             -->
                            {$delete.class = 'btn-small btn-green btn-delete'}
                            {$delete.uri = ''}
                            {$delete.name = 'delete'}
                            {$delete.id = 'delete'}
                            {$delete.value = lang('common_lbl_delete')}
                            {$delete.content = lang('common_lbl_delete')}
                            {generate_action_button($delete)}
                        </form>
                    </div>
                </td>
            </tr>
        </table>
        <hr class="blank-10px" />
    </div>
</div>
<!-- e/content -->
{include file = "inc/footer.tpl"}
