<html>
<head>
{literal}
<script>
//$(document).ready(function() {
     $(document).on('click', '#close_detail_b', function(event) {
     // $("#close").click(function( event ) {
            event.preventDefault();
            $("#dialog_form_b").dialog("close");
     });
    // Confirm delete
    /*$(document).on('click', '#delete', function() {
     // $("#delete").click(function() {
         return confirm($( "#message_sure_delete" ).val());
         // return confirm("Are you sure to delete?");
    });*/
//});
</script>
{/literal}
</head>
<body>
    <div class="cell content">
        <div>
            <hr class="blank-10px" />
            <table class="table-theme">
                {if $d}
                <tr>
                    <td width="40%">{lang('common_lbl_business_unit')}</td>
                    <td><input type="hidden" name="did" value="{$d.id}" />
                        {$d.business_unit_name}</td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_name')}</td>
                    <td>{$d.name}</td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_zipcode')}</td>
                    <td>{$d.zipcode|substr:0:3} - {$d.zipcode|substr:3:7}</td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_address1')}</td>
                    <td>{$d.address_1}</td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_address2')}</td>
                    <td>{$d.address_2}</td>
                </tr>
                <tr>
                    <td>TEL</td>
                    <td>{$d.phone_no}</td>
                </tr>
                <tr>
                    <td>FAX</td>
                    <td>{$d.fax_no}</td>
                </tr>
                {else}
                <tr>
                    <th colspan="2">Nodata</th>
                </tr>
                {/if}
                <!--  -->
                <tr>
                    <td colspan="2">
                        <div style="float: left;">
                            <form method="POST" action="/_admin/sample/edit_department" style="">
                                <input type="hidden" value="{$d.id}" name="d_id" />
                                <button id='edit_b' name='edit_b' value="{lang('common_lbl_edit')}" class = 'btn-small btn-green btn-edit'>{lang('common_lbl_edit')}</button>
                            </form>
                        </div>
                        <div style="float: left;">
                            <form method="POST" action="/_admin/sample/copy_department">
                                <input type="hidden" value="{$d.id}" name="d_id" />
                                <button id='copy_b' name='copy_b' value="{lang('common_lbl_copy')}" class = 'btn-small btn-green btn-copy'>{lang('common_lbl_copy')}</button>
                            </form>
                        </div>
                        <div style="float: left;">
                                <button name="close_detail_b" id="close_detail_b" class="btn-small btn-green">{lang('common_lbl_close')}</button>
                        </div>
                        <div style="float: left;">
                            <form method="POST" action="/_admin/sample/edit_department">
                                <input type="hidden" value="{$d.id}" name="d_id" />
                                <button type="submit" onClick="return confirm('{lang('message_sure_to_delete')}');" name="delete" id="delete" class = "btn-small btn-green btn-delete" value="{lang('common_lbl_delete')}">{lang('common_lbl_delete')}</button>
                            </form>
                        </div>
                    </td>
                </tr>
            </table>
            <hr class="blank-10px" />
        </div>
    </div>
</body>
</html>
