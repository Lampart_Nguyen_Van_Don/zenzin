<html>
<head>
{literal}
<script>
    // Close detail c dialog box
    $(document).on('click', '#close_load_detail_c', function() {
            $("#dialog_form_c").dialog("close");
     });
    // Click edit in dialogbox c
    $(document).on('click', '#edit', function() {
     //$("#edit").click(function(  ) {
         $('#dialog_form_c').load("/_admin/sample/edit_c_department", {
             id : this.value,
             type : "edit",
             redirect_form_post:redirect_data()
         }, function() {
             // $("#dialogStreetView").dialog( "option", "width", 460 );
             // $("#dialog_form_c").dialog( "option", "height", 390 );
             // $("#dialog_form_c").dialog( "option", "width", 393 );
             $('#dialog_form_c').dialog('option', 'title', $( "#common_edit_department" ).val());
             $('#dialog_form_c').dialog("open");
         });
     });
    // Click copy in dialogbox c
    $(document).on('click', '#copy', function() {
         $('#dialog_form_c').load("/_admin/sample/edit_c_department", {
             id : this.value,
             type : "copy",
             redirect_form_post:redirect_data()
         }, function() {
             // $("#dialog_form_c").dialog( "option", "position", "center" );
             // $("#dialogStreetView").dialog( "option", "width", 460 );
             // $("#dialog_form_c").dialog( "option", "height", 390 );
             // $("#dialog_form_c").dialog( "option", "width", 393 );
             $('#dialog_form_c').dialog('option', 'title', $( "#common_copy_department" ).val());
             $('#dialog_form_c').dialog("open");
         });
     });

 // User click delete to delete department
    $(document).on('click', '#delete_department', function(event) {
        event.preventDefault();
        var id = $(this).val();
        confirm_dialog(lang['message_confirm_delete'], {
            'delete' : function() {
                $(this).dialog('close');
                $("#form_delete").submit();
            }
        });
    });


//});
</script>
{/literal}
</head>
<body>
    <div class="cell content">
        <div>
            <hr class="blank-10px" />
            <table class="table-theme">
                {if $d}
                <tr>
                    <td width="40%">{lang('common_lbl_business_unit')}</td>
                    <td><input type="hidden" name="did" value="{$d.id}" />
                        {$d.business_unit_name}</td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_name')}</td>
                    <td>{$d.name}</td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_zipcode')}</td>
                    <td>{$d.zipcode}</td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_address1')}</td>
                    <td>{$d.address_1}</td>
                </tr>
                <tr>
                    <td>{lang('common_lbl_department_address2')}</td>
                    <td>{$d.address_2}</td>
                </tr>
                <tr>
                    <td>TEL</td>
                    <td>{$d.phone_no}</td>
                </tr>
                <tr>
                    <td>FAX</td>
                    <td>{$d.fax_no}</td>
                </tr>
                {else}
                <tr>
                    <th colspan="2">Nodata</th>
                </tr>
                {/if}
                <!--  -->
                <tr>
                    <td colspan="2">
                        <div style="float: left;">
                                <button name="{$d.id}" id="edit" class = 'btn-small btn-green btn-edit'
                                value="{$d.id}">{lang('common_lbl_edit')}</button>
                        </div>
                        <div style="float: left;">
                                <button name="{$d.id}" id="copy" class="btn-small btn-green btn-copy"
                                    value="{$d.id}">{lang('common_lbl_copy')}</button>
                        </div>
                        <div style="float: left;">
                            <button name="close_load_detail_c" id="close_load_detail_c" class="btn-small btn-green"
                                value="Close">{lang('common_lbl_close')}

                            </button>
                        </div>
                        <div style="float: left;">
                            <form method="POST" action="/_admin/sample/delete_department" id="form_delete">
                                {make_search_hidden(FALSE)}
                                <input type="hidden" value="{$d.id}" name="d_id" />
                            </form>
                            <button name="delete_department" type="submit" id="delete_department" value="Delete" class="btn-small btn-green btn-delete">{lang('common_lbl_delete')}</button>
                        </div>
                    </td>
                </tr>
            </table>
            <hr class="blank-10px" />
        </div>
    </div>
</body>
</html>
