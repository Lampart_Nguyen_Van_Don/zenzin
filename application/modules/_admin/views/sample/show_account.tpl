{include file = "inc/header.tpl"}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="styleSheet" href="/public/admin/sample/main.css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-touch.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-animate.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.1/angular-sanitize.min.js"></script>
    <script src="http://ui-grid.info/release/ui-grid-unstable.js"></script>
    <link rel="styleSheet" href="http://ui-grid.info/release/ui-grid-unstable.css"/>
    <script>
    var jsGlobals = [
        code = '{lang("code")}',
        business_unit = '{lang("business_unit")}',
        name_product = '{lang("name_product")}',
        is_no_add = '{lang("is_no_add")}',
        message_sure_to_delete = '{lang("message_sure_to_delete")}',
    ];
    </script>
    <script src="/public/admin/sample/app_account.js"></script>
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

<div class="cell content">
        <div>
            <h1>{$title}</h1>
            <div id="save-confirm" style="display: none;" title="Save?">
                <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>"Want to save your changes?"</p>
            </div>
            <div ng-controller="MainCtrl">
                <div class="red" ng-bind-html="errors"></div>
                <button class="btn-small btn-green" type="button" id="removeRows" ng-click="removeRows()">{lang('common_lbl_delete')}</button>
                <button class="btn-small btn-green" type="button" id="addData" ng-click="addData()">{lang('common_lbl_add')}</button>
                <br>
                <br>
                <div ui-grid="gridOptions" ui-grid-pinning ui-grid-resize-columns ui-grid-pagination ui-grid-edit ui-grid-row-edit ui-grid-cellnav ui-grid-selection class="grid"></div>
                <div class="ui-grid-pager-control">
                    <p>{literal}Current page: {{ gridApi.pagination.getPage() }} of {{ numPages }}{/literal}</p>
                    <button type="button" class="btn btn-success" ng-click="gridApi.pagination.seek(1)">
                        <div class="first-triangle"><div class="first-bar"></div></div>
                    </button>
                    <button type="button" class="btn btn-success" ng-click="gridApi.pagination.previousPage()">
                        <div class="first-triangle"></div>
                    </button>
                    <button type="button" class="btn btn-success" ng-click="gridApi.pagination.nextPage()">
                        <div class="last-triangle"></div>
                    </button>
                 </div>
            </div>
        </div>
    </div>
    <!-- e/content -->
</div>


{include file = "inc/footer.tpl"}