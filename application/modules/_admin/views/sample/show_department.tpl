{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<!-- Department -->
{literal}

<script>
    $(document).ready(function() {

        /* if ($('#search_conditions').length){
             $( "#search_conditions" ).clone().appendTo( "form" );
        } */

        $(function() {
            // Load as type B
            $("#dialog_form_b").dialog({
                autoOpen : false,
                // resizable: false,
                // maxWidth : 600,
                // maxHeight : 500,
                width : "auto",
                height : "auto",
                modal : true,

                open: function(){
                    jQuery('.ui-widget-overlay').bind('click',function(){
                        jQuery('#dialog').dialog('close');
                    })
                },
                buttons : {},
                close : function() {
                }
            });
            // Load as type C
            $("#dialog_form_c").dialog({
                autoOpen : false,
                width : "auto",
                height : "auto",
                modal : true,

                open: function(){
                    jQuery('.ui-widget-overlay').bind('click',function(){
                        jQuery('#dialog').dialog('close');
                    })
                },

                buttons : {
                },
                close : function() {
                }
            });
        });

        // Close C when click outside
        jQuery('body')
          .bind(
           'click',
                   function(e){
                if(
                     jQuery('#dialog_form_c').dialog('isOpen')
                     && !jQuery(e.target).is('.ui-dialog, a')
                     && !jQuery(e.target).closest('.ui-dialog').length
                ){
                     jQuery('#dialog_form_c').dialog('close');
                }
           }
          );
        // Close B when click outside
        $("body").on('click', '.close-dialog', function(event) {
            $(".ui-dialog-content").dialog("close");
            $("#dialog-edit-content").html('');
        });
        $("body").on('click', '.ui-widget-overlay', function(event) {
            $(".ui-dialog-content").dialog("close");
        });
        // Load type b
        $(document).on("click", "#detail_b", function() {
            // alert(99999);
            $('#dialog_form_b').load("/_admin/sample/load_detail_b_department", {
                id : this.name,
                search_business_unit: $("#search_business_unit").val(),
                search_name: $("#search_name").val(),
                has_search_value: $("#has_search_value").val()

            }, function() {
                $('#dialog_form_b').dialog('option', 'title', $( "#detail_department" ).val());
                //$("#dialog_form_b").dialog( "option", "height", 370 );
                //$("#dialog_form_b").dialog( "option", "width", 373 );
                $('#dialog_form_b').dialog("open");
            });
        });

        // Load C
        $(document).on("click", "#detail_c", function() {
            $('#dialog_form_c').load("/_admin/sample/load_detail_c_department", {
                id : this.name,
                redirect_form_post: redirect_data()
            }, function() {
                $('#dialog_form_c').dialog('option', 'title', $( "#detail_department" ).val());
                //$("#dialog_form_c").dialog( "option", "height", 370 );
                //$("#dialog_form_c").dialog( "option", "width", 393 );
                $('#dialog_form_c').dialog("open");
            });
        });

     // Load C
        $(document).on("click", "#add", function() {
            $('#dialog_form_c').load("/_admin/sample/add_department", {
                id : this.name,
                redirect_form_post: redirect_data()
            }, function() {
                $('#dialog_form_c').dialog('option', 'title', $( "#common_add_department" ).val());
                //$("#dialog_form_c").dialog( "option", "height", 400 );
                //$("#dialog_form_c").dialog( "option", "width", 373 );
                $('#dialog_form_c').dialog("open");
            });
        });
});
</script>
{/literal}

<div class="cell content">
    <div>
        <!--  -->
        <h1>{lang('common_listof_department')}</h1>
        <form action="" method="post" id="search_form">
                <table class="table-simple">
                    <tr>
                        <th width="100px">{lang('common_lbl_business_unit')}</th>
                        <td>
                        {if isset($form.search_business_unit)}
                             {html_options name="search_business_unit" options=$bu selected=$form.search_business_unit}
                        {else}
                             {html_options name="search_business_unit" options=$bu}
                        {/if}
                        </td>
                    </tr>
                    <tr>
                        <th>{lang('common_lbl_department_name')}</th>
                        <td><input type="text" name="search_name" value="{if isset($form.search_name)}{$form.search_name}{/if}" /></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="a-center"><button type="submit" name="search" class="btn-small btn-green btn-search"
                            value="{lang('common_lbl_search')}" />{lang('common_lbl_search')}</button></td>
                    </tr>
                </table>
            </form>
        <br/>
        {make_search_hidden()}
        <h1></h1>
        <hr class="blank-10px" />
        <table class="table-theme odd">
            {if $departments}
            <tr>
                <th>{lang('common_lbl_business_unit')}</th>
                <th>{lang('common_lbl_department_name')}</th>
                <th>{lang('common_lbl_action')} (A)</th>
                <th>{lang('common_lbl_action')} (B)</th>
                <th>{lang('common_lbl_action')} (C)</th>
            </tr>
            {foreach from=$departments item=d}
            <tr>
                <td>{$d.business_unit_name}</td>
                <td>{$d.department_name}</td>
                <td>
                    {$hidden.d_id = $d.department_id}
                    {form_view_button('/_admin/sample/detail_department', $hidden)}
                </td>
                <td>
                    <input type="hidden" value="{$d.department_id}" name="d_id" />
                    <button id="detail_b" class="btn-small btn-green btn-view" value="{lang('common_lbl_view')}" name="{$d.department_id}">{lang('common_lbl_view')}</button>
                </td>
                <td>
                    <input type="hidden" value="{$d.department_id}" name="d_id" />
                    <button id="detail_c" class="btn-small btn-green btn-view" value="{lang('common_lbl_view')}" name="{$d.department_id}">{lang('common_lbl_view')}</button>
                </td>
            </tr>
            {/foreach} {else}
            <tr>
                <th>{lang('common_lbl_business_unit')}</th>
                <th>{lang('common_lbl_department_name')}</th>
                <th>{lang('common_lbl_action')} (A)</th>
                <th>{lang('common_lbl_action')} (B)</th>
                <th>{lang('common_lbl_action')} (C)</th>
            </tr>
            <tr>
                <td colspan="5">{lang('common_lbl_no_data')}</td>
            </tr>
            {/if}
            <tr>
                <td colspan="5">
                    <!-- {form_add_button()} -->
                    <button name="add" id="add" value="add" class="btn-small btn-green btn-add">{lang('common_btn_add')}</button>
                </td>
            </tr>
        </table>
        <hr class="blank-10px" />
        {if $pagination} {$pagination.links} {/if}
    </div>
</div>

<!-- Store messages -->
<input type="hidden" id="common_copy_department" value="{lang('common_copy_department')}"/>
<input type="hidden" id="common_add_department" value="{lang('common_add_department')}"/>
<input type="hidden" id="common_edit_department" value="{lang('common_edit_department')}"/>
<input type="hidden" id="detail_department" value="{lang('common_detail_department')}"/>
<input type="hidden" id="common_edit_successfully" value="{lang('common_edit_successfully')}"/>
<input type="hidden" id="common_insert_successfully" value="{lang('common_insert_successfully')}"/>
<!-- End store messages -->

<div id="dialog_form_b" title="{lang('common_detail_department')}"></div>
<div id="dialog_form_c" title="{lang('common_detail_department')}"></div>
<!-- e/content -->
{include file = "inc/footer.tpl"}
