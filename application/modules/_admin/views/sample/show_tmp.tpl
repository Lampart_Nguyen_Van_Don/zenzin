{include file = "inc/header.tpl"}
    <link rel="styleSheet" href="/public/admin/sample/main.css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-touch.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-animate.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.1/angular-sanitize.min.js"></script>
    <script src="http://ui-grid.info/release/ui-grid-unstable.js"></script>
    <link rel="styleSheet" href="http://ui-grid.info/release/ui-grid-unstable.css"/>
    <script>
    var jsGlobals = [
        code = '{lang("code")}',
        business_unit = '{lang("business_unit")}',
        name_product = '{lang("name_product")}',
        is_tax = '{lang("is_tax")}',
        message_sure_to_delete = '{lang("message_sure_to_delete")}',
    ]
    </script>
    <script src="/public/admin/sample/app_tmp.js"></script>
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

<div class="cell content">
        <div>
            <h1>{$title}</h1>
            <div ng-controller="MainCtrl">
                <!-- <div class="red" ng-bind-html="errors"></div>
                <button class="btn-small btn-green" type="button" ng-click="save()">{lang('common_lbl_save')}</button>
                <button class="btn-small btn-green" type="button" id="removeRows" ng-click="removeRows()">{lang('common_lbl_delete')}</button>
                <button class="btn-small btn-green" type="button" id="addData" ng-click="addData()">{lang('common_lbl_add')}</button>
                <br>
                <br> -->
                <div ui-grid="gridOptions" ui-grid-pinning ui-grid-resize-columns ui-grid-pagination ui-grid-edit ui-grid-row-edit ui-grid-cellnav ui-grid-selection class="grid"></div>
            </div>
        </div>
    </div>
    <!-- e/content -->
</div>


{include file = "inc/footer.tpl"}