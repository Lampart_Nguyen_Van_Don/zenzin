<div>
    <table class="table-theme">
        <tr>
            <td>{lang('lbl_code')}</td>
            <td><input type="text" value="{$shipping_type.code}" disabled></td>
        </tr>
        <tr>
            <td>{lang('lbl_name')}</td>
            <td><input type="text" value="{html_escape($shipping_type.name)}" disabled></td>
        </tr>
        <tr>
            <td>{lang('lbl_is_remaining_confirm_flag')}</td>
            <td>
                <label>
                    <input type="checkbox" name="is_remaining_confirm" value="1" {if $shipping_type.is_remaining_confirm == 1}{'checked'}{/if} disabled> {lang('lbl_is_remaining_confirm')}
                </label>
            </td>
        </tr>
        <tr>
            <td>{lang('lbl_is_advance_payment_flag')}</td>
            <td>
                <label>
                    <input type="checkbox" name="is_advance_payment" value="1" {if $shipping_type.is_advance_payment == 1}{'checked'}{/if} disabled> {lang('lbl_is_advance_payment')}
                </label>
            </td>
        </tr>
        <tr>
            <td>{lang('lbl_reporting_flag')}</td>
            <td>
                <label>
                    <input type="checkbox" name="is_reporting" value="1" {if $shipping_type.is_reporting == 1}{'checked'}{/if} disabled> {lang('lbl_reporting')}
                </label>
            </td>
        </tr>
        <tr>
            <td>{lang('lbl_is_display_flag')}</td>
            <td>
                <label>
                    <input type="checkbox" name="is_no_add" value="1" {if $shipping_type.is_no_add == 1}{'checked'}{/if} disabled> {lang('lbl_is_no_add')}
                </label>
            </td>
        </tr>
        <tr>
            <td>{lang('lbl_sequence')}</td>
            <td><input type="text" value="{$shipping_type.sequence}" disabled></td>
        </tr>
        <tr>
            <td colspan="2">
                <button type="button" class="btn-small btn-green btn-edit" data-shipping-id="{$shipping_type.id}">{lang('btn_edit')}</button>
                <button type="button" class="btn-small btn-green btn-close close-dialog">{lang('btn_close')}</button>
            </td>
        </tr>
    </table>
</div>

{literal}
<script>
    $(".btn-edit").on('click', function(event) {
        shipping_id = $(this).data('shipping-id');
        $.ajax({
            url: '/_admin/shipping/edit',
            type: 'POST',
            data: {shipping_id: shipping_id},
        })
        .done(function(data) {
            if (typeof data.error_code !== 'undefined' && data.error_code == 400) {
                $("#dialog-edit-content").html('<div class="error-message">'+data.messages+'</div><br><br>');
                $("#dialog-edit-content").dialog({
                    width: 'auto',
                    height: 'auto',
                    modal : true,
                });
                return;
            }

            $("#dialog-edit-content").html(data);
            $("#dialog-edit-content").dialog({
                title: '発送種別編集',
                width: 'auto',
                height: 'auto',
            });

        })
    });
</script>
{/literal}