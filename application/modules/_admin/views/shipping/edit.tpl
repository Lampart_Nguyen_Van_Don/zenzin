<div>
    <p class="messages"></p>
    {form_open(null, "id='edit_form'")}
        <input type="hidden" name="shipping_id" value="{$shipping_type.id}">
        <table class="table-theme">
            <tr>
                <td>{lang('lbl_code')}</td>
                <td>
                    <input type="text" value="{$shipping_type.code}" disabled>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_name')} <span class="txt-red">※</span></td>
                <td>
                    <input name="name" type="text" value="{html_escape($shipping_type.name)}">
                    <p id="edit-name-error"></p>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_is_remaining_confirm_flag')}</td>
                <td>
                    <label>
                        <input type="checkbox" name="is_remaining_confirm" value="1" {if $shipping_type.is_remaining_confirm == 1}{'checked'}{/if}> {lang('lbl_is_remaining_confirm')}
                    </label>
                    <p id="edit-is-remaining-confirm-error"></p>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_is_advance_payment_flag')}</td>
                <td>
                    <label>
                        <input type="checkbox" name="is_advance_payment" value="1" {if $shipping_type.is_advance_payment == 1}{'checked'}{/if}> {lang('lbl_is_advance_payment')}
                        <p id="edit-is-advance-payment-error"></p>
                    </label>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_reporting_flag')}</td>
                <td>
                    <label>
                        <input type="checkbox" name="is_reporting" value="1" {if $shipping_type.is_reporting == 1}{'checked'}{/if}> {lang('lbl_reporting')}
                        <p id="edit-is-reporting-error"></p>
                    </label>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_is_display_flag')}</td>
                <td>
                    <label>
                        <input type="checkbox" name="is_no_add" value="1" {if $shipping_type.is_no_add == 1}{'checked'}{/if}> {lang('lbl_is_no_add')}
                        <p id="edit-no-display-error"></p>
                    </label>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_sequence')} <span class="txt-red">※</span></td>
                <td>
                    <input type="text" name="sequence" value="{$shipping_type.sequence}" maxlength="5">
                    <p id="edit-sequence-error"></p>

                     <p id="edit-sequence-error-value" style="display:none" class="validation-error">{lang('lbl_invalid_input_minus')}</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button type="button" class="btn-small btn-green btn-save btn-update-shipping">{lang('btn_save')}</button>
                    <button type="button" class="btn-small btn-green btn-close close-dialog">{lang('btn_close')}</button>
                    <button type="button" class="btn-small btn-green btn-delete" data-shipping-id="{$shipping_type.id}">{lang('btn_delete')}</a>
                </td>
            </tr>
        </table>
    {form_close()}
</div>

{literal}
<script>
    // $("#edit_form input[type=text]").on('focus', function(event) {
    //     $(this).val('');
    // });

    $(".btn-update-shipping").on('click', function(event) {
        form_data = $("#edit_form").serialize();
       var valSque  =  $("input[name=sequence]").val();

        $.ajax({
            url: '/_admin/shipping/update',
            type: 'POST',
            data: form_data,
        })
        .done(function(data) {

             data = $.parseJSON(data);
            if (data.status) {
                form_changed = false;
                form_submitted = true;
                $(".messages").html("<div class='success-message blink_me'>"+data.messages+"</div><br><br>");
                $(".messages").addClass('alert-success');
                $(".btn-close").addClass('back-to-show');
                $("#edit-name-error, #edit-is-remaining-confirm-error, #edit-is-advance-payment-error, #edit-is-reporting-error, #edit-no-display-error, #edit-sequence-error").html('');
            } else {
                if (typeof data.messages == 'object') {
                    $(".success-message").remove();
                    $("#edit-name-error").html(data.messages.name);
                    $("#edit-is-remaining-confirm-error").html(data.messages.is_remaining_confirm);
                    $("#edit-is-advance-payment-error").html(data.messages.is_advance_payment);
                    $("#edit-is-reporting-error").html(data.messages.is_reporting);
                    $("#edit-no-display-error").html(data.messages.is_no_add);
                    $("#edit-sequence-error").html(data.messages.sequence);

                } else {
                    $(".messages").html("<div class='error-message'>"+data.messages+"</div>");
                    $(".messages").addClass('alert-error');
                }
            }


        })
    });

    $(".btn-delete").on('click', function(event) {
        shipping_id = $(this).data('shipping-id');

        $("#dialog-confirm").html('発送種別を削除します。よろしいですか？').dialog({
            title: '確認',
            width: 'auto',
            height: 'auto',
            modal : true,
            buttons: {
                "はい": function() {
                     $.ajax({
                        url: '/_admin/shipping/delete',
                        type: 'POST',
                        data: {shipping_id: shipping_id},
                    })
                    .done(function(data) {
                        if (!data.status) {
                            $(".messages").html('<div class="error-message">'+data.messages+'</div>');
                            $("#dialog-confirm").dialog('close');
                            return;
                        }
                        dom =   "<form method='post' action='/_admin/shipping/show'>"+
                                    "<input type='hidden' name='pg' value='"+$("#page").val()+"'>"+
                                "</form>";
                        $(dom).appendTo('body').submit();
                    })
                },
                "いいえ": function () {
                    $(this).dialog('close');
                }
            }
        });
    });


    bind_edit_event = function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            $('.btn-update-shipping').trigger('click');
            return false;
        }
    }

    $(window).bind('keydown', bind_edit_event);

    exit_confirm();
</script>
{/literal}