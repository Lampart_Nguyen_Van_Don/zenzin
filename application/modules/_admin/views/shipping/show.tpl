{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>
        {if isset($message)}{$message}{/if}
        <table class="table-theme odd">
            <thead>
                <tr>
                    <th>{lang('lbl_action')}</th>
                    <th>{lang('lbl_sequence')}</th>
                    <th>{lang('lbl_code')}</th>
                    <th>{lang('lbl_name')}</th>
                    <th>{lang('lbl_is_remaining_confirm')}</th>
                    <th>{lang('lbl_is_advance_payment')}</th>
                    <th>{lang('lbl_reporting')}</th>
                    <th>{lang('lbl_is_display')}</th>
                </tr>
            </thead>
            <tbody>
                {if !empty($shipping_types)}
                {foreach $shipping_types as $shipping_type}
                <tr>
                    <td>
                        <a class="btn-small btn-green btn-view view-shipping" data-shipping-id="{$shipping_type.id}">{lang('lbl_view')}</a>
                    </td>
                    <td>{$shipping_type.sequence}</td>
                    <td>{$shipping_type.code}</td>
                    <td>{html_escape($shipping_type.name)}</td>
                    <td align="center">{if $shipping_type.is_remaining_confirm}●{else}−{/if}</td>
                    <td align="center">{if $shipping_type.is_advance_payment}●{else}−{/if}</td>
                    <td align="center">{if $shipping_type.is_reporting}●{else}−{/if}</td>
                    <td align="center">{$shipping_type.new_input}</td>
                </tr>
                {/foreach}
                {else}
                <tr><td colspan="8">No Data</td></tr>
                {/if}
                <tr>
                    <td colspan="8">
                        <input id="page" type="hidden" value="{if isset($form.pg)}{$form.pg}{else}{1}{/if}">
                        <button type="button" class="btn-small btn-green btn-add">{lang('btn_add')}</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div id="dialog-edit-content"></div>
<div id="dialog-add-content" style="display:none">
    <p class="messages"></p>
    {form_open(null, "id='add_form'")}
        <table class="table-theme">
            <tr>
                <td>{lang('lbl_name')}<span class="red-symbol"> ※</span></td>
                <td>
                    <input name="name" type="text" value="">
                    <p id="add-name-error"></p>
                </td>
            </tr>

            <tr>
                <td>{lang('lbl_is_remaining_confirm_flag')}</td>
                <td>
                    <label>
                        <input type="checkbox" name="is_remaining_confirm" value="1"> {lang('lbl_is_remaining_confirm')}
                    </label>
                    <p id="add-is-remaining-confirm-error"></p>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_is_advance_payment_flag')}</td>
                <td>
                    <label>
                        <input type="checkbox" name="is_advance_payment" value="1"> {lang('lbl_is_advance_payment')}
                    </label>
                    <p id="add-is-advance-payment-error"></p>
                </td>
            </tr>
            <tr>
                <td>{lang('lbl_reporting_flag')}</td>
                <td>
                    <label>
                        <input type="checkbox" name="is_reporting" value="1"> {lang('lbl_reporting')}
                    </label>
                    <p id="add-is-reporting-error"></p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button type="button" class="btn-small btn-green btn-save btn-add-shipping">{lang('btn_add')}</button>
                    <button type="button" class="btn-small btn-green btn-close close-dialog">{lang('btn_close')}</button>
                </td>
            </tr>
        </table>
    {form_close()}
</div>
<div id="dialog-confirm" style="display:none"></div>

{literal}
<script>
    $(".view-shipping").on('click', function(event) {
        shipping_id = $(this).data('shipping-id');

        $.ajax({
            url: '/_admin/shipping/detail',
            type: 'POST',
            data: {shipping_id: shipping_id}
        })
        .done(function(data) {
             if (typeof data.error_code !== 'undefined' && data.error_code == 400) {
                $("#dialog-edit-content").html('<div class="error-message">'+data.messages+'</p>');
                $("#dialog-edit-content").dialog({
                    width: 'auto',
                    height: 'auto',
                    modal : true
                });
                return;
            }

            $("#dialog-edit-content").html(data);
            $("#dialog-edit-content").dialog({
                title: '発送種別参照',
                width: 'auto',
                height: 'auto',
                modal: true,
                open: function() {
                    $(window).unbind('keydown', bind_add_event);
                    console.log('opened');
                },
                close: function() {
                    console.log('closed');
                    $(".back-to-show").trigger('click');

                    if (typeof bind_edit_event != 'undefined') {
                        $(window).unbind('keydown', bind_edit_event);
                    }

                    if (typeof bind_add_event != 'undefined') {
                        $(window).bind('keydown', bind_add_event);
                    }
                },
                beforeClose: function( event, ui ) {
                    var close = close_confirm();
                    return close;
                }

            });

        })
    });

    $(".btn-add").on('click', function(event) {
        $(".messages, #add-name-error, #add-is-remaining-confirm-error, #add-is-advance-payment-error, #add-is-reporting-error").html('');
        $("#dialog-add-content").dialog({
            title: '発送種別追加',
            width: 'auto',
            height: 'auto',
            modal: true,
            beforeClose: function( event, ui ) {
                var close = close_confirm();
                return close;
            },
            close: function() {
                $(".back-to-show").trigger('click');

                reset_form_data('#add_form');
            }
        });
    });

    $(".btn-add-shipping").on('click', function(event) {
        $.ajax({
            url: '/_admin/shipping/add',
            type: 'POST',
            data: $("#add_form").serialize()
        })
        .done(function(data) {
            data = $.parseJSON(data);
            $('.success-message').remove();

            if (data.status) {
                form_changed = false;
                form_submitted = true;

                $(".messages").html("<div class='success-message'>"+data.messages+"</div><br><br>");
                $(".messages").addClass('alert-success');
                $(".btn-close").addClass('back-to-show');
                $("#add-name-error, #add-is-remaining-confirm-error, #add-is-advance-payment-error, #add-is-reporting-error").html('');

                //reset form input
                $('input[name="name"]').val('');
                $('input[name="is_remaining_confirm"]').prop('checked', false);
                $('input[name="is_advance_payment"]').prop('checked', false);
                $('input[name="is_reporting"]').prop('checked', false);
            } else {
                if (typeof data.messages == 'object') {
                    $("#add-name-error").html(data.messages.name);
                    $("#add-is-remaining-confirm-error").html(data.messages.is_remaining_confirm);
                    $("#add-is-advance-payment-error").html(data.messages.is_advance_payment);
                    $("#add-is-reporting-error").html(data.messages.is_reporting);
                } else {
                    $(".messages").html("<div class='error-message'>"+data.messages+"</div><br><br>");
                    $(".messages").addClass('alert-error');
                }
            }
        })
    });

    $('body').on('click', '.back-to-show', function(event) {
        if (form_changed) return;

        dom =   "<form method='post' action='/_admin/shipping/show'>"+
                    "<input type='hidden' name='pg' value='"+$("#page").val()+"'>"+
                "</form>";
        $(dom).appendTo('body').submit();
    });

    $("body").on('click', '.close-dialog', function(event) {
        $(".ui-dialog-content").dialog("close");
    });

    // $("body").on('click', '.ui-widget-overlay', function(event) {
    //     $(".ui-dialog-content").dialog("close");
    // });

    var bind_add_event = function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            if ($("#dialog-add-content").is(":visible")) {
                $('.btn-add-shipping').trigger("click");
            }
            return false;
        }
    }

    $(window).bind('keydown', bind_add_event);

    exit_confirm();

</script>
{/literal}
{include file = "inc/footer.tpl"}