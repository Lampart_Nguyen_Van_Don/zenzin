{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}
<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>

        <!-- Begin #search -->
        <div id="search">
            <table class="table-simple">
                <tr>
                    <th>{lang('lbl_delivery_date')}<span style="color: red"> ※ </span></th>
                    <td>
                        <input type="tel" style="ime-mode:disabled" id="delivery_date_from" value="{$last_thursday}"/>～
                        <input type="tel" style="ime-mode:disabled" id="delivery_date_to" value="{$this_wednesday}" />
                        <button type="button" id="validate_input" class="btn-small btn-green">{lang('lbl_print')}</button>
                        <a id="print_report" href="/_admin/stock_check_list/print_report" style="display: none">{lang('lbl_print')}</a>
                        <div class="legend">{lang('lbl_delivery_date_description')}</div>
                        <span id="delivery_date_from_error_msg" class="validation-error" style="display: none"></span>
                        <span id="delivery_date_to_error_msg" class="validation-error" style="display: none"></span>
                    </td>
                </tr>
            </table>
        </div>
        <!-- End #search -->
    </div>
</div>
<script type="text/javascript" src="/public/admin/stock_check_list/main.js"></script>