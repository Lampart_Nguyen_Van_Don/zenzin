<html>
<head>
{literal}
    <script>
        $(document).ready(function() {

            // Click edit button
            $(document).off('click', '#edit').on('click', '#edit', function() {

                //This variable contain post data.
                var id = $(this).attr('value');
                var post_data = {}

                $.each($('div#post_data').find('input[type="hidden"]'), function(index, val) {
                    var attr = $(val).attr('name');
                    var value = $(val).val();
                    post_data[attr] = value;
                });
                post_data['id'] = id;

                $('#dialog_form').load("/_admin/subcontractor/edit", post_data, function() {
                    $("#dialog_form").dialog({
                            width: 'auto',
                            height: 'auto',
                            modal : true,
                            beforeClose: function( event, ui ) {
                                var close = close_confirm();
                                return close;
                            },
                        });
                        $('#dialog_form').dialog('open');
                    });
            });

            //open form
            var number_relations = $('input[name="number_relations"]').val();
            var height = 400 + number_relations * 20;
            $('#dialog_form').dialog('option', 'title', $("#dialog_form_title").val());
            $("#dialog_form").dialog( "option", "height", height );
            $("#dialog_form").dialog( "option", "width", 450);
            $("#dialog_form").dialog( "open");
        });
    </script>
{/literal}
</head>
<body>
    <div>
        <div>
            <hr class="blank-10px" />
            <!-- Begin #detail -->
            <div>
                <div><input type="hidden" name="form"/></div>
                <div id="post_data" style="display: none">
                    {foreach from=$post key=key item=val}
                        <input type="hidden" name="{$key}" value="{$val}"/>
                    {/foreach}
                </div>

                <table class="table-theme">
                    <tr><td colspan="2" style="display: none"><input type="hidden" name="form"/></td></tr>
                    <tr>
                        <td style="min-width: 150px">{lang('lbl_code')}</td>
                        <td style="min-width: 140px"><input type="text" value="{$subcontractor.code|escape:'html'}" disabled/></td>
                    </tr>

                    <tr>
                        <td>{lang('lbl_name')}</td>
                        <td><input type="text" value="{$subcontractor.name|escape:'html'}" disabled/></td>
                    </tr>

                    <tr>
                        <td>{lang('lbl_abbr_name')}</td>
                        <td><input type="text" value="{$subcontractor.abbr_name|escape:'html'}" disabled/></td>
                    </tr>

                    <tr>
                        <td>{lang('lbl_subcontractor_type')}</td>
                        <td>
                            {if $direct_consignment_configs}
                                {if $direct_consignment_configs[1].value == $subcontractor.direct_consignment}
                                    <input type="checkbox" checked disabled/><span>{$direct_consignment_configs[1].display}</span>
                                {else}
                                    <input type="checkbox" disabled/><span>{$direct_consignment_configs[1].display}</span>
                                {/if}
                            {/if}
                        </td>
                    </tr>

                    <tr>
                        <td>{lang('lbl_is_no_add')}</td>
                        <td>
                             {if $is_no_add_configs}
                                {if $is_no_add_configs[1].value == $subcontractor.is_no_add}
                                    <input type="checkbox" checked disabled/><span>{$is_no_add_configs[1].display}</span>
                                {else}
                                    <input type="checkbox" disabled/><span>{$is_no_add_configs[1].display}</span>
                                {/if}
                           {/if}
                        </td>
                    </tr>

                    <tr>
                        <td>
                            {if $direct_consignment_configs[1].value == $subcontractor.direct_consignment}
                                {lang('lbl_choosen_child_subcontractor')}
                            {else}
                                {lang('lbl_choosen_parent_subcontractor')}
                            {/if}
                        </td>
                        <td>
                            <ul class="listbox-disable" disabled>
                                {$number_relations = 0}

                                {if $subcontractor_relations}
                                    {foreach from=$subcontractor_relations item=val}
                                        <li>{$val.name}</li>

                                        {$number_relations = $number_relations + 1}
                                    {/foreach}
                                {else}
                                    <li>{lang('no_data')}</li>
                                {/if}
                                <input type="hidden" name="number_relations" value="{$number_relations}"/>
                            </ul>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            {if $have_add_edit_delete_role}
                            <div style="float: left"><button id="edit" class="btn-small btn-green btn-edit" value="{$subcontractor.id}">{lang('lnk_edit')}</div>
                            {/if}

                            <div style="float: left">
                                <button  id="close" class="btn-small btn-green btn-close" value="close">{lang('btn_close')}
                            </div>
                        </td>
                    </tr>

                </table>
            </div>
            <hr class="blank-10px" />
        </div>
    </div>
</body>
</html>
