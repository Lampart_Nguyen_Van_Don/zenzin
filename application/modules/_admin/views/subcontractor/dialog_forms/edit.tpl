
   <div>
        <div>
            <!-- Begin #edit -->
            <div id="frm_edit">
                <div id="form_msg"></div>
                <form action="/_admin/subcontractor/edit" method="post">
                    <table id="form_content" class="table-theme">
                       <div id="post_data">
                           {if $hidden_data}
                                {foreach from=$hidden_data key=key item=val}
                                    <input type="hidden" name="{$key}" value="{$val}"/>
                                {/foreach}
                            {/if}
                        </div>
                        <tr>
                            <td style="width: 170px"><label for="code">{lang('lbl_code')}<span class="red-symbol"> ※</span></label></td>
                            <td style="width: 432px">
                                <input type="hidden" name="id" style="width: 150px" value="{$post.id}" />
                                <input type="text" name="code" style="width: 150px" value="{$post.code}" />
                                <span></span>
                            </td>
                        </tr>

                        <tr>
                            <td><label for="name">{lang('lbl_name')}<span class="red-symbol"> ※</span></label></td>
                            <td>
                                <input type="text" name="name" value="{$post.name|htmlspecialchars}" style="width: 230px" />
                                <span></span>
                            </td>
                        </tr>

                        <tr>
                            <td><label for="abbr_name">{lang('lbl_abbr_name')}<span class="red-symbol"> ※</span></label></td>
                            <td>
                                <input type="text" name="abbr_name" value="{$post.abbr_name|htmlspecialchars}" style="width: 230px" />
                                <span></span>
                            </td>
                        </tr>

                        <tr>
                            <td>{lang('lbl_subcontractor_type')}</td>
                            <td>
                                {if $direct_consignment_configs}
                                    {$checked = ''}

                                    {if $direct_consignment_configs[1].value == $post.direct_consignment}
                                        {$checked = 'checked'}
                                    {/if}
                                {/if}

                                <input type="checkbox" id="direct_consignment" name="direct_consignment" value="{$direct_consignment_configs[1].value}" {$checked}/>
                                <label for="direct_consignment">{$direct_consignment_configs[1].display}</label>
                            </td>
                        </tr>

                        <tr>
                            <td>{lang('lbl_is_no_add')}</td>
                            <td>
                                {if $is_no_add_configs}
                                    {$checked = ''}

                                    {if $is_no_add_configs[1].value == $post.is_no_add}
                                        {$checked = 'checked'}
                                    {/if}

                                    <input type="checkbox" id="is_no_add" name="is_no_add" value="{$is_no_add_configs[0].value}" {$checked}/>
                                    <label for="is_no_add">{$is_no_add_configs[0].display}</label>
                                {/if}
                            </td>
                        </tr>

                        <tr id="direct_consignment_section">
                            <td><div style="margin-top: -135px">{lang('lbl_child_subcontractor_relation')}</div></td>
                            <td>
                                <table>
                                    <tr>
                                        <td style="border: none">
                                            <h5>{lang('lbl_child_subcontractor')}</h5>
                                            <p id="direct_consignment_left"></p>
                                        </td>
                                        <td style="border: none">
                                            <button type="button" id="parent_choose" class="btn-small btn-green"> > </button><br/>
                                            <br/>
                                            <button type="button" id="parent_remove_choose" class="btn-small btn-green"> < </button>
                                        </td>
                                        <td style="border: none">
                                            <h5>{lang('lbl_choosen_child_subcontractor')}</h5>
                                            <p id="direct_consignment_right"></p>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>

                        <tr id="indirect_consignment_section">
                            <td><div style="margin-top: -135px">{lang('lbl_parent_subcontractor_relation')}<span class="red-symbol"> ※</span></div></td>
                            <td>
                                <table>
                                    <tr>
                                        <td style="border: none">
                                            <h5>{lang('lbl_parent_subcontractor')}</h5>
                                            <p id="indirect_consignment_left"></p>
                                        </td>
                                        <td style="border: none">
                                            <button type="button" id="child_choose" class="btn-small btn-green"> > </button><br/>
                                            <br/>
                                            <button type="button" id="child_remove_choose" class="btn-small btn-green"> < </button>
                                        </td>
                                        <td style="border: none">
                                            <h5>{lang('lbl_choosen_parent_subcontractor')}</h5>
                                            <p id="indirect_consignment_right"></p>
                                        </td>
                                    </tr>
                                </table>

                                <table>
                                    <tr>
                                        <td style="padding: 0px!important; margin: 0px!important; border: none">
                                            <input type="text" name="parent_subcontractor_error_msg" style="display: none" />
                                            <span></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                 <button id="save" type="button" class="btn-small btn-green btn-save">{lang('btn_save')}</button>
                                 <button id="close" type="button" class="btn-small btn-green btn-close">{lang('btn_close')}</button>
                                 <button id="delete" value="{$post.id}" class="btn-small btn-green btn-delete">{lang('common_lbl_delete')}</button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <!-- End #add -->
        </div>
    </div>
{literal}
    <script type="text/javascript">
        $(document).ready(function () {

            //redirect page have post
            var redirectPage = function() {

                var form = $('<form action="' + $('div#post_data').find('input[name="back_link"]').val() + '" method="post">' +
                              $('div#post_data').html() + '</form>');

                $('body').append(form);
                $(form).submit();
            };

            $('#dialog_form').dialog('option', 'title', $("#dialog_edit_form_title").val());
            $("#dialog_form").dialog( "option", "height", 620 );
            $("#dialog_form").dialog( "option", "width", 700 );

            //init data
            var have_changed = false;
            var id = $('input[name="id"]').val();

            $("#direct_consignment_right").jqxListBox({theme: 'office', source: {}, displayMember: "name", valueMember: "id", multipleextended: true, width: 170, height: 250, rendered: function () {
                $("#direct_consignment_right").jqxListBox('focus');
            }});

            $("#direct_consignment_left").jqxListBox({theme: 'office', source: {}, displayMember: "name", valueMember: "id", multipleextended: true, width: 170, height: 250, rendered: function () {
                $("#direct_consignment_left").jqxListBox('focus');
            }});

            $("#indirect_consignment_right").jqxListBox({theme: 'office', source: {}, displayMember: "name", valueMember: "id", multipleextended: true, width: 170, height: 250, rendered: function () {
                $("#indirect_consignment_right").jqxListBox('focus');
            }});

            $("#indirect_consignment_left").jqxListBox({theme: 'office', source: {}, displayMember: "name", valueMember: "id", multipleextended: true, width: 170, height: 250, rendered: function () {
                $("#indirect_consignment_left").jqxListBox('focus');
            }});

            if ($('input[name="direct_consignment"]:checked').length > 0) {
                $('tr#indirect_consignment_section').hide();
            } else {
                $('tr#direct_consignment_section').hide();
            }

            var getParentExceptId = function() {
                var items = $("#direct_consignment_right").jqxListBox('getItems');
                var result = [];

                $.each(items, function(index, val) {
                    result.push(val.value);
                });
                result.push(id);
                return result;
            }

            var getChildExceptId = function() {
                var items = $("#indirect_consignment_right").jqxListBox('getItems');
                var result = [];

                $.each(items, function(index, val) {
                    result.push(val.value);
                });
                result.push(id);
                return result;
            }

            var parent_source =
            {
                    datatype: "json",
                    data: {
                            direct_consignment : 0,
                            except_id : getParentExceptId
                          },
                    datafields: [
                          { name: 'id' },
                          { name: 'name' }
                          ],
                    url: '/_admin/subcontractor/ajax_get_subcontractors',
                    type: 'post',
            };
            var parentDataAdapter = new $.jqx.dataAdapter(parent_source);

            var parent_choosen_source =
            {
                    datatype: "json",
                    data: {
                             parent_subcontractor_id : id,
                          },
                    datafields: [
                          { name: 'id' },
                          { name: 'name' }
                          ],
                    url: '/_admin/subcontractor/ajax_get_subcontractor_relations',
                    type: 'post',
                    async: false,
            };
            var parentChoosenDataAdapter = new $.jqx.dataAdapter(parent_choosen_source);

            var child_source =
            {
                    datatype: "json",
                    datafields: [
                        { name: 'id' },
                        { name: 'name' }
                    ],
                    data: {
                        direct_consignment : 1,
                        except_id : getChildExceptId
                      },
                    url: '/_admin/subcontractor/ajax_get_subcontractors',
                    type: 'post',
            };
            var childDataAdapter = new $.jqx.dataAdapter(child_source);

            var child_choosen_source =
            {
                    datatype: "json",
                    datafields: [
                        { name: 'id' },
                        { name: 'name' }
                    ],
                    data: {
                        child_subcontractor_id : id,
                      },
                    url: '/_admin/subcontractor/ajax_get_subcontractor_relations',
                    type: 'post',
                    async: false,
            };
            var childChoosenDataAdapter = new $.jqx.dataAdapter(child_choosen_source);

            //get data
            $("#direct_consignment_right").jqxListBox({theme: 'office', source: parentChoosenDataAdapter, displayMember: "name", valueMember: "id", multipleextended: true, width: 170, height: 250, rendered: function () {
                $("#direct_consignment_right").jqxListBox('focus');
            }});

            $("#indirect_consignment_right").jqxListBox({theme: 'office', source: childChoosenDataAdapter, displayMember: "name", valueMember: "id", multipleextended: true, width: 170, height: 250, rendered: function () {
                $("#indirect_consignment_right").jqxListBox('focus');
            }});

            $("#direct_consignment_left").jqxListBox({theme: 'office', source: parentDataAdapter, displayMember: "name", valueMember: "id", multipleextended: true, width: 170, height: 250, rendered: function () {
                $("#direct_consignment_left").jqxListBox('focus');
            }});

            $("#indirect_consignment_left").jqxListBox({theme: 'office', source: childDataAdapter, displayMember: "name", valueMember: "id", multipleextended: true, width: 170, height: 250, rendered: function () {
                $("#indirect_consignment_left").jqxListBox('focus');
            }});

            //Parent choose
            $(document).on('click', '#parent_choose', function() {
                var items = $("#direct_consignment_left").jqxListBox('getSelectedItems');

                if (items) {
                    $.each(items, function(index, val) {
                        $("#direct_consignment_right").jqxListBox('addItem', {'id': val.value, 'name' : val.label});
                        $("#direct_consignment_left").jqxListBox('removeAt', val.index);
                    });
                    $("#direct_consignment_left").jqxListBox('clearSelection');
                }
            });

            //Remove parent choose
            $(document).on('click', '#parent_remove_choose', function() {
                var items = $("#direct_consignment_right").jqxListBox('getSelectedItems');

                if (items) {
                    $.each(items, function(index, val) {
                        $("#direct_consignment_left").jqxListBox('addItem', {'id': val.value, 'name' : val.label});
                        $("#direct_consignment_right").jqxListBox('removeAt', val.index);
                    });
                    $("#direct_consignment_right").jqxListBox('clearSelection');
                }
            });

            //Child choose
            $(document).on('click', '#child_choose', function() {
                var items = $("#indirect_consignment_left").jqxListBox('getSelectedItems');

                if (items) {
                    $.each(items, function(index, val) {
                        $("#indirect_consignment_right").jqxListBox('addItem', {'id': val.value, 'name' : val.label});
                        $("#indirect_consignment_left").jqxListBox('removeAt', val.index);
                    });
                    $("#indirect_consignment_left").jqxListBox('clearSelection');
                }
            });

            //Remove child choose
            $(document).on('click', '#child_remove_choose', function() {
                var items = $("#indirect_consignment_right").jqxListBox('getSelectedItems');

                if (items) {
                    $.each(items, function(index, val) {
                        $("#indirect_consignment_left").jqxListBox('addItem', {'id': val.value, 'name' : val.label});
                        $("#indirect_consignment_right").jqxListBox('removeAt', val.index);
                    });
                    $("#indirect_consignment_right").jqxListBox('clearSelection');
                }
            });

            //listbox item double click event
            $('#direct_consignment_left').on('dblclick', function() {
                var items = $("#direct_consignment_left").jqxListBox('getSelectedItems');

                if (items) {
                    $.each(items, function(index, val) {
                        $("#direct_consignment_right").jqxListBox('addItem', {'id': val.value, 'name' : val.label});
                        $("#direct_consignment_left").jqxListBox('removeAt', val.index);

                    });
                    $("#direct_consignment_left").jqxListBox('clearSelection');
                }
            });

            $('#direct_consignment_right').on('dblclick', function() {
                var items = $("#direct_consignment_right").jqxListBox('getSelectedItems');

                if (items) {
                    $.each(items, function(index, val) {
                        $("#direct_consignment_left").jqxListBox('addItem', {'id': val.value, 'name' : val.label});
                        $("#direct_consignment_right").jqxListBox('removeAt', val.index);

                    });
                    $("#direct_consignment_right").jqxListBox('clearSelection');
                }
            });

            $('#indirect_consignment_left').on('dblclick', function() {
                var items = $("#indirect_consignment_left").jqxListBox('getSelectedItems');

                if (items) {
                    $.each(items, function(index, val) {
                        $("#indirect_consignment_right").jqxListBox('addItem', {'id': val.value, 'name' : val.label});
                        $("#indirect_consignment_left").jqxListBox('removeAt', val.index);

                    });
                    $("#indirect_consignment_left").jqxListBox('clearSelection');
                }
            });

            $('#indirect_consignment_right').on('dblclick', function() {
                var items = $("#indirect_consignment_right").jqxListBox('getSelectedItems');

                if (items) {
                    $.each(items, function(index, val) {
                        $("#indirect_consignment_left").jqxListBox('addItem', {'id': val.value, 'name' : val.label});
                        $("#indirect_consignment_right").jqxListBox('removeAt', val.index);
                    });
                    $("#indirect_consignment_right").jqxListBox('clearSelection');
                }
            });

            //direct consignment change event
            $('input[name="direct_consignment"]').on('change', function() {
                if ( ! this.checked) {
                    $('tr#indirect_consignment_section').show();
                    $('tr#direct_consignment_section').hide();
                } else {
                    $('tr#indirect_consignment_section').hide();
                    $('tr#direct_consignment_section').show();
                }
            });

            //Save
            $('button#save').on('click', function() {
                //init variables
                var url = '/_admin/subcontractor/edit';
                var post_data = {};
                var attrs = [];

                $.each($('div#post_data').find('input[type="hidden"]'), function(index, val) {
                    var attr = $(val).attr('name');
                    var value = $(val).val();
                    post_data[attr] = value;
                });

                //get data from input
                $.each($('table#form_content').find('input'), function(index, val) {
                    var attr = $(val).attr('name');
                    var type = $(val).attr('type');
                    var value = $(val).val();

                    if (attr != null && attr != '' && $.inArray(attr, attrs) == -1) {
                        attrs.push(attr);
                        post_data[attr] = value;

                        if (type == 'checkbox') {
                            if ( ! this.checked) {
                                post_data[attr] = 0;
                            } else {
                                post_data[attr] = 1;
                            }
                        }
                    }
                });

                //get data from listbox
                var items = $("#indirect_consignment_right").jqxListBox('getItems');
                if ($('input[name="direct_consignment"]').is(':checked')) {
                    items = $("#direct_consignment_right").jqxListBox('getItems');
                }

                post_data['new_relations'] = [];
                $.each(items, function(index, val) {
                    post_data['new_relations'].push(val.value);
                });
                post_data['save'] = true;

                //search ajax to process
                $.post(
                       url,
                       post_data,
                       function(data) {
                           if (data.result !== false) {
                               form_changed = false;
                               form_submitted = true;
                               //reset
                               $(".validation-error").parent().find('br').remove();
                               $(".validation-error").remove();

                               $('.error-message').remove();
                               $('.success-message').remove();
                               $("#dialog_form").dialog( "option", "height", 650);

                               //show message
                               $('div#form_msg').html('<span class="success-message blink_me">' + data.error_msg['form'] + '</span>');
                               if ($('input[name="direct_consignment"]').is(':checked')) {
                                   $("#indirect_consignment_right").jqxListBox('refresh');
                                   $("#indirect_consignment_left").jqxListBox('refresh');
                               } else {
                                   $("#direct_consignment_right").jqxListBox('refresh');
                                   $("#direct_consignment_left").jqxListBox('refresh');
                               }

                               //update status
                               have_changed = true;

                           } else {

                               //reset
                               $(".validation-error").parent().find('br').remove();
                               $(".validation-error").remove();
                               $('.error-message').remove();
                               $('.success-message').remove();

                               //show message
                               var height = 620;
                               $.each(attrs, function(index, val) {
                                   if (data.error_msg[val]) {
                                       $('input[name="' + val + '"]').parent().find('span').after('<br/>' + data.error_msg[val]);

                                       height += 30;
                                   }
                               });

                               $("#dialog_form").dialog( "option", "height", height);

                           }
                       },
                    "json"
                );
            });

            bind_edit_event = function(e) {
                if (e.ctrlKey && (e.which == 83)) {
                    e.preventDefault();
                    $('button#save').click();
                    return false;
                }
            }

            $(document).bind('keydown', bind_edit_event);

            //Click delete button
            $(document).off('click', '#delete').on('click', '#delete', function(event) {
                event.preventDefault();

                //This variable contain post data.
                var id = $(this).attr('value');
                var post_data = {id : id};

                confirm_dialog(lang['message_confirm_delete'], {
                    'delete' : function() {
                        $(this).dialog('close');
                        $.post(
                                '/_admin/subcontractor/delete',
                                post_data,
                                function(data) {
                                    if (data.result !== false) {
                                        redirectPage();
                                    } else {
                                        if (data.error_msg['form']) {
                                            $('span.error-message').remove();
                                            $('div#form_msg').after('<span class="error-message">' + data.error_msg['form'] + '</span>');
                                            return false;
                                        }
                                    }
                                },
                             "json"
                       );
                    }
                });
            });
        });
    exit_confirm();
    </script>
{/literal}
