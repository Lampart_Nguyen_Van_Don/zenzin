{include file = "inc/header.tpl"}
{include file = "inc/top-header.tpl"}
{include file = "inc/nav-left.tpl"}

<div class="cell content slide-right">
    <div>
        <h1 class="page-title">{$title}</h1>

        <!-- Begin #search -->
        <div id="search">
            <form name="search" action="" method="post">
                <table class="table-simple">
                    <tr>
                        <th><label for="search_code">{lang('lbl_code')}</label></th>
                        <td>
                            <input type="text" name="search_code" value="{$post.search_code}" /><br/>
                            <div class="legend">&nbsp;{lang('description_for_code')} </div>
                        </td>
                    </tr>

                    <tr>
                        <th><label for="search_name">{lang('lbl_name')}</label></th>
                        <td>
                            <input type="text" name="search_name" value="{$post.search_name}" /><br/>
                            <span class="legend">&nbsp;{lang('description_for_name')}</span>
                        </td>
                    </tr>

                    <tr>
                        <th><label for="search_abbr_name">{lang('lbl_abbr_name')}</label></th>
                        <td>
                            <input type="text" name="search_abbr_name" value="{$post.search_abbr_name}" /><br/>
                            <span class="legend">&nbsp;{lang('description_for_abbr_name')}</span>
                        </td>
                    </tr>

                    <tr>
                        <th><label for="search_direct_consignment">{lang('lbl_subcontractor_type')}</label></th>
                        <td>
                            {if $subcontractor_configs}
                                {$i = 1}
                                {foreach from=$subcontractor_configs item=val}
                                    {$is_checked = ''}

                                    {if $val.value == $post.search_direct_consignment}
                                        {$is_checked = 'checked'}
                                    {/if}

                                    <input type="radio" id="direct_consignment_{$i}" name="search_direct_consignment" value="{$val.value}" {$is_checked}/><label for="direct_consignment_{$i}"> {$val.display}</label>
                                    {$i = $i + 1}
                                {/foreach}
                            {/if}
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <div align="center">
                                <button type="submit" name="search" class="btn-small btn-green btn-search" value="search">{lang('btn_search')}</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <!-- End #search -->
        <br>
        <h1></h1>
        <hr class="blank-10px"/>

        <!-- Begin .table-theme odd -->
        <div id="post_data" style="display: none">
                {foreach from=$post key=key item=val}
                    <input type="hidden" name="{$key}" value="{$val}"/>
                {/foreach}
        </div>
        <table class="table-theme odd">

            <tbody>

                <tr>
                    <th>{lang('lbl_action')}</th>
                    <th>{lang('lbl_code')}</th>
                    <th>{lang('lbl_name')}</th>
                    <th>{lang('lbl_abbr_name')}</th>
                    <th>{lang('lbl_direct_consignment')}</th>
                    <th>{lang('is_no_add')}</th>
                </tr>

                {if $subcontractors}
                    {foreach from=$subcontractors item=val}
                        <tr>
                            <td>
                                <button id="detail" class="btn-small btn-green btn-view" value="{$val.id}">{lang('common_lbl_view')}</button>
                            </td>
                            <td>{$val.code|escape:'html'}</td>
                            <td>{$val.name|escape:'html'}</td>
                            <td>{$val.abbr_name|escape:'html'}</td>
                            <td style="text-align: center">{if $val.direct_consignment == 1}●{else}−{/if}</td>
                            <td style="text-align: center">{$val.new_input}</td>
                        </tr>
                    {/foreach}
                {else}
                    <tr><td colspan="6" style="text-align: center">{lang('no_data')}</td></tr>
                {/if}

                {if $have_add_edit_delete_role}
                    <tr>
                        <td colspan="6"><button id="add" class="btn-small btn-green btn-add">{lang('common_lbl_add')}</button></td>
                    </tr>
                {/if}
            </tbody>
        </table>
        <!-- .End table-theme odd -->

        <hr class="blank-10px" />
        <!-- Pagination -->
        {if $pagination} {$pagination.links} {/if}
        <!-- End pagination -->
    </div>
</div>

<input type="hidden" id="dialog_form_title" value="{lang('lbl_detail_form_title')}"/>
<input type="hidden" id="dialog_add_form_title" value="{lang('lbl_add_form_title')}"/>
<input type="hidden" id="dialog_edit_form_title" value="{lang('lbl_edit_form_title')}"/>
<div id="dialog_form" title="{lang('lbl_detail_form_title')}"></div>

{literal}
<script>
    $(document).ready(function() {

        //init dialog
        $("#dialog_form").dialog({
            autoOpen : false,
            width : 373,
            height : 370,
            modal : true,

            open: function(){
                jQuery('.ui-widget-overlay').bind('click',function(){
                    jQuery('#dialog').dialog('close');
                })
            },
            beforeClose: function( event, ui ) {
                var close = close_confirm();
                return close;
            },
            buttons : {

            },
            close : function() {
                console.log('after close unbind');
                if (typeof bind_edit_event != 'undefined') {
                    $(document).unbind('keydown', bind_edit_event)
                }

                if (typeof bind_add_event != 'undefined') {
                    $(document).unbind('keydown', bind_add_event)
                }
            }
        });

        // close dialog when click outside
        $("body").on('click', '.close-dialog', function(event) {
            $(".ui-dialog-content").dialog("close");
            $("#dialog-edit-content").html('');
        });

        $("body").on('click', '.ui-dialog-titlebar-close', function(event) {
            if (form_submitted && !form_changed) {
                    redirectPage();
            }
        });

        $(document).on('click', '#close', function() {
            $("#dialog_form").dialog("close");
            if (form_submitted && !form_changed) {
                    redirectPage();
            }
        });
        //redirect page have post
        var redirectPage = function() {

            var form = $('<form action="' + $('div#post_data').find('input[name="back_link"]').val() + '" method="post">' +
                          $('div#post_data').html() + '</form>');

            $('body').append(form);
            $(form).submit();
        };

        // show dialog
        $(document).on("click", "#detail", function() {
            //This variable contain post data.
            var id = $(this).attr('value');
            var post_data = {}

            $.each($('div#post_data').find('input[type="hidden"]'), function(index, val) {
                var attr = $(val).attr('name');
                var value = $(val).val();
                post_data[attr] = value;
            });
            post_data['id'] = id;
            $('#dialog_form').load("/_admin/subcontractor/detail", post_data, function() {
                $("#dialog_form").dialog({
                    width: 'auto',
                    height: 'auto',
                    modal : true
                });
                $('#dialog_form').dialog('open');
            });
        });

        // show dialog
        $(document).on("click", "#add", function() {
            var post_data = {}

            $.each($('div#post_data').find('input[type="hidden"]'), function(index, val) {
                var attr = $(val).attr('name');
                var value = $(val).val();
                post_data[attr] = value;
            });

            $('#dialog_form').load("/_admin/subcontractor/add", post_data, function() {
                $('#dialog_form').dialog('option', 'title', $("#dialog_add_form_title").val());
                $("#dialog_form").dialog( "option", "height", 620 );
                $("#dialog_form").dialog( "option", "width", 700 );
                $('#dialog_form').dialog("open");
            });
        });
    });
</script>
{/literal}

{include file = "inc/footer.tpl"}