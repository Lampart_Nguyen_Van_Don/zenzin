<?php
/**
 * This class contains some functions work with client
 *
 * @author hoang_minh
 * @since 2015-05-27
 *
 */
class Client extends ZR_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Checking client's credit informations.
     *
     * @author hoang_minh
     * @since 2015-05-27
     */
    public function check_credit_group_b() {
        //load library
        $this->load->library('email_template');

        //load model
        $this->load->model('client_model');
        $this->load->model('account_model');
        $this->load->model('authority_model');

        //init variable
        $clients_to_check_credit = array();
        $params = array();
        $mail_address = array();

        #7175:Start
		$file_suffix = $this->config->item("credit_levelB_expired");
		$this->app_logger->cron_log(lang('client_level_b_expire_list_header'), $file_suffix);
		$this->app_logger->cron_log(str_repeat("-", 30), $file_suffix);
		#7175:End

        if ($clients_to_check_credit = $this->client_model->get_client_credit_groub_b()) {

            //get client ids to update
            $client_ids = array();
            foreach ($clients_to_check_credit as $item) {
                $client_ids[] = $item['id'];
            }

            //update client
            try {
                $this->client_model->begin_transaction();

                $update_data = array(
                        'status' => CLIENT_STATUS_RE_CREDIT_WAITING,
                        'lastup_account_id' => 0,
                        'lastup_datetime' => $this->client_model->_db_now()
                );

                $update_conditions = array(
                        'ids' => implode(',', $client_ids)
                );

                if ( ! $this->client_model->update_client($update_data, $update_conditions)) {
                    throw new Exception('Update client to status credit waiting fail.');
                }

                //get account to recive cron check credit mail
                $authorities = $this->authority_model->get_authority_by_quick_setting('mail_client_recredit');

                $params = array(
                    'columns' => array('mail_address'),
                    'conditions' => array(
                            'authority_ids' => implode(',', $authorities)
                    )
                );

                $accounts = $this->account_model->get_accounts_by_parameters($params);
                foreach ($accounts as $item) {
                    $mail_address[] = $item['mail_address'];
                }

                #7175:Start
                $initial_flag = true;
                #7175:End

                foreach ($clients_to_check_credit as $item) {

                	#7175:Start
					if ($initial_flag)
						$this->app_logger->cron_log(lang('cron_client_id_header') . " \t/\t " . lang('cron_s_code_header'), $file_suffix);

					#7175:End

                    //prepair mail content
                    $replace_params = array(
                            'system_subject'      => str_replace('%param%', $item['name'], lang('customer_subject')),
                            's_code'              => htmlspecialchars($item['s_code']),
                            'name'                => htmlspecialchars($item['name']),
                            'name_kana'           => htmlspecialchars($item['name_kana']),
                            's_account_id_1st'      => $item['account_name_1'],
                            's_account_id_2nd'      => $item['account_name_2'],
//#7427:Start                            
                            'credit_amount'       => (number_format($item['is_ad_receive_payment'])) ? '前受金' : number_format($item['credit_amount']),
//#7427:End
                            'payment_term'        => lang('lbl_closing_date') .   ' : ' . $item['closing_date'] . "   "
                            . lang('lbl_payment_month_cd') .  ' : ' . $item['payment_month_cd'] . "   "
                            . lang('lbl_payment_day_cd') . ' : ' . $item['payment_day_cd'],
                            'last_delivery_month' => ( ! empty($item['max_delivery_date'])) ? date('m', strtotime($item['max_delivery_date'])) : '',
                            'zipcode'             => $item['zipcode'],
                            'address1'            => htmlspecialchars($item['address_1st']),
                            'address2'            => htmlspecialchars($item['address_2nd']),
                            'phone_no'            => $item['phone_no'],
                            'business_category'   => $item['business_category_name'],
                            'remark'              => htmlspecialchars($item['remark']),
                            'comment'             => htmlspecialchars($item['comment']),
                            'credit_approval_date'=> htmlspecialchars(date('Y/m/d', strtotime($item['credit_approval_date'])))
                    );

//#7427:Start
                    $this->send_mail_by_authority('mail_client_recredit', 'cron_check_credit', $replace_params);

//                     foreach ($mail_address as $key => $val) {
//                         if ( ! $this->email_template->send_mail('cron_check_credit', array('system_to' => $val), $replace_params)) {
//                             throw new Exception('Cron check client group b: ' . date('YmdHis') . ' ===> Send mail to ' . $val . ' fail.');
//                         }
//                     }
//#7427:End
                    #7175:Start
                    $this->app_logger->cron_log(str_pad($item['id'], strlen(lang('cron_client_id_header')), " ", STR_PAD_LEFT) . " \t \t " . $item['s_code'], $file_suffix);
                    $initial_flag= false;
                    #7175:End
                }

                $this->client_model->commit_transaction();

            } catch (Exception $ex) {
                $this->app_logger->error_log($ex->getMessage());
                $this->client_model->rollback_transaction();

                echo 'Fail';
                exit();
            }
        }
        #7175:Start
        else {
        	$this->app_logger->cron_log(lang('msg_no_data_found'), $file_suffix);
        }
        #7175:End

        echo 'Completed';
        exit();
    }

    /**
     * Update client expired who din't regist during 3 months.
     *
     * @author hoang_minh
     * @since 2015-05-29
     */
    public function check_client_regist_expired() {
        //load model
        $this->load->model('client_model');

        //init variable
        $client_regist_expired = array();

        #7175:Start
        $file_suffix = $this->config->item("credit_expired");
        $this->app_logger->cron_log(lang('client_credit_expire_list_header'), $file_suffix);
        $this->app_logger->cron_log(str_repeat("-", 30), $file_suffix);
        #7175:End

        if ($client_regist_expired = $this->client_model->get_client_regist_expired()) {

            //get client ids to update
            $client_ids = array();
            foreach ($client_regist_expired as $item) {
                $client_ids[] = $item['id'];
            }

            try {
                $update_data = array(
                    'status' => CLIENT_STATUS_CREDIT_EXPIRED,
                    'lastup_account_id' => 0,
                    'lastup_datetime' => $this->client_model->_db_now()
                );

                $update_conditions = array(
                        'ids' => implode(',', $client_ids)
                );

                if ( ! $this->client_model->update_client($update_data, $update_conditions)) {
                    throw new Exception('Update client to status credit expired fail.');
                }
                #7175:Start
                else {
                	$initial_flag = true;

                	for ($i = 0; $i < count($client_ids); $i++) {
                		if ($initial_flag)
                			$this->app_logger->cron_log(lang('cron_client_id_header'), $file_suffix);

                		$this->app_logger->cron_log(str_pad($client_ids[$i], strlen(lang('cron_client_id_header')), " ", STR_PAD_LEFT), $file_suffix);
                		$initial_flag = false;
                	}
                }
                #7175:End
            } catch (Exception $ex) {
                $this->app_logger->error_log($ex->getMessage());

                echo 'Fail';
                exit();
            }
        }
        #7175:Start
        else {
        	$this->app_logger->cron_log(lang('msg_no_data_found'), $file_suffix);
        }
        #7175:End

        echo 'Completed';
        exit();
    }

    #7175:Start
    public function send_cron_email() {

    	$this->load->helper('file');
    	$this->load->library('email');

    	$log_path = APPPATH .'logs/cron/';
    	$file_credit_expired = $log_path . date('Y_m_d') . "_" . $this->config->item("credit_expired") . ".log";

    	$credit_expired_content = "";
    	$credit_levelB_expired_content = "";

    	if (!file_exists($file_credit_expired)){
    		$credit_expired_content = lang('client_credit_expire_list_header') . "\n";
    		$credit_expired_content .= str_repeat("-", 30) . "\n";
    		$credit_expired_content .= lang('msg_target_file_not_exist') . "\n";
    	} else {
    		$credit_expired_content = $this->read_file_content($file_credit_expired);

    	}

    	$file_credit_levelB_expired = $log_path . date('Y_m_d') . "_" . $this->config->item("credit_levelB_expired") . ".log";

    	if (!file_exists($file_credit_levelB_expired)){
    		$credit_levelB_expired_content = lang('client_level_b_expire_list_header') . "\n";
    		$credit_levelB_expired_content .= str_repeat("-", 30) . "\n";
    		$credit_levelB_expired_content .= lang('msg_target_file_not_exist') . "\n";
    	} else {
    		$credit_levelB_expired_content = $this->read_file_content($file_credit_levelB_expired);
    	}

    	//Ready to send email
    	$system_mail_address = $this->config->item('from_mail_address');
    	$this->email->from($system_mail_address['email'], $system_mail_address['name']);
    	$this->email->to($this->config->item('cron_monitoring_mail_address'));
    	$email_subject = str_replace("[CURRENT_DATE]", date('Y/m/d'),  lang('msg_cron_email_subject'));
    	$this->email->subject($email_subject);
    	$this->email->message($credit_expired_content . "\n\n" . $credit_levelB_expired_content);

    	$this->email->send();
    }

    public function read_file_content($file) {

    	$file_content = "";
    	$handle = fopen($file, "r");

    	if ($handle) {
    		while (($line = fgets($handle)) !== false) {
    			$line = preg_replace('/\t+/', '5XXXXX5', $line);
    			if (strpos($line, "/"))
    				$line = str_replace("5XXXXX5", str_repeat(" ", 3), $line);
    			else
    				$line = str_replace("5XXXXX5", str_repeat(" ", 3), $line);
    			$file_content .= $line;
    			$line = "";
    		}

    		fclose($handle);
    	}
    	else {
    		return "";
    	}

    	return $file_content;
    }

    #7175:End
}