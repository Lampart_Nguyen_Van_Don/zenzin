<?php
$lang['customer_subject'] = '【再与信確認】 %param%';
$lang['lbl_mail_check_credit_title'] = '%param%　様の再与信を　{最新与信承認日の1年後}までにお願いします。';
$lang['lbl_s_code'] = "S-Code";
$lang['lbl_name'] = "Customer Name";
$lang['lbl_name_kana'] = "Customer Kana Name";
$lang['lbl_sales_representative'] = 'Sales Representative';
$lang['lbl_amount'] = "Amount";
$lang['lbl_payment_term'] = "Payment Term";
$lang['lbl_last_delivery_month'] = "Last Delivery Month";
$lang['lbl_zip_code'] = "Zip Code";
$lang['lbl_address'] = "Address";
$lang['lbl_tel'] = "TEL";
$lang['lbl_bussiness_category'] = "Bussiness Category";
$lang['lbl_remark'] = "Remark";
$lang['lbl_comment'] = "Comment";
$lang['lbl_closing_date'] = 'Closing Date';
$lang['lbl_payment_month_cd'] = 'Payment Month';
$lang['lbl_payment_day_cd'] = 'Payment Day';
#7175:Start
$lang['msg_no_data_found'] = 'There is no data.';
$lang['msg_target_file_not_exist'] = 'The file does not exist.';
$lang["msg_cron_email_subject"] = '【ZENRIN】[CURRENT_DATE] CRON RESULT';
$lang["client_credit_expire_list_header"] = '[CLIENT_CREDIT_EXPIRE_LIST]';
$lang["client_level_b_expire_list_header"] = '[CLIENT_LEVEL_B_EXPIRE_LIST]';
$lang["cron_client_id_header"] = 'client_id';
$lang["cron_s_code_header"] = 's_code';
#7175:End