<?php
$lang['customer_subject'] = '【再与信確認】 %param%';
$lang['lbl_mail_check_credit_title'] = '%param%　様の再与信を　{最新与信承認日の1年後}までにお願いします。';
$lang['lbl_s_code'] = "Ｓコード";
$lang['lbl_name'] = "会社名";
$lang['lbl_name_kana'] = "フリガナ";
$lang['lbl_sales_representative'] = '営業担当者';
$lang['lbl_amount'] = "与信額";
$lang['lbl_payment_term'] = "支払条件";
$lang['lbl_last_delivery_month'] = "最終取引月";
$lang['lbl_zip_code'] = "郵便番号";
$lang['lbl_address'] = "住所";
$lang['lbl_tel'] = "TEL";
$lang['lbl_bussiness_category'] = "業種";
$lang['lbl_remark'] = "特記事項";
$lang['lbl_comment'] = "コメント";
$lang['lbl_closing_date'] = '締め日';
$lang['lbl_payment_month_cd'] = '支払月';
$lang['lbl_payment_day_cd'] = '支払日';
$lang['lbl_payment_day_cd'] = '支払日';
#7175:Start
$lang['msg_no_data_found'] = '対象データがありません。';
$lang['msg_target_file_not_exist'] = '対象ファイルが存在しません。';
$lang["msg_cron_email_subject"] = '【ZENRIN】[CURRENT_DATE] CRON結果';
$lang["client_credit_expire_list_header"] = '[CLIENT_CREDIT_EXPIRE_LIST]';
$lang["client_level_b_expire_list_header"] = '[CLIENT_LEVEL_B_EXPIRE_LIST]';
$lang["cron_client_id_header"] = 'client_id';
$lang["cron_s_code_header"] = 's_code';
#7175:End
