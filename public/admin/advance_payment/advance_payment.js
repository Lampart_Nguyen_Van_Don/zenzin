/**
 * LIST FUNCTIONS:
 * $scope.getPageData
 * load_data
 * $scope.filter
 * $scope.printPDF
 * $scope.print
 * $scope.saveGrid
 * CTR+S
 * CHECK CHANGE PAGE NAVIGATION
 * ENTER TO SEARCH
 * validate_and_search
 * alert_messages(messages) {
 * check_data_has_been_changed
 * validate_delivery_date
 * validate_date_from_to_search
 * check_search_from_bigger_search_to
 * check_month_below_10
 * $scope.save = function()
 * $scope.check_has_closed_acc
 * $scope.restore_after_delete
 * $scope.check_is_extra_item_and_not_closed_acc
 * $scope.insertRow
 * $scope.deleteRow
 * filter('calculateTotalAmount')
 */
 //#7846:Start
// var app = angular.module('app', [ 'ngTouch', 'ngSanitize', 'ui.grid',
//     'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.pagination',
//     'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.cellNav',
//     'ui.grid.selection' ], function($compileProvider) {
//         $compileProvider.directive('compile', function($compile) {
//             return function(scope, element, attrs) {
//                 scope.$watch(
//                     function(scope) {
//                         return scope.$eval(attrs.compile);
//                     },
//                     function(value) {
//                         element.html(value);
//                         $compile(element.contents())(scope);
//                     } // End function(value)
//                 ); // End scope.$watch(
//             }; // End return function(scope, element, attrs)
//         }); // End $compileProvider.directive
//     } // function($compileProvider)
// );
var app = angular.module('app', ['ngSanitize', 'ui.grid',
    'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.pagination',
    'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.cellNav'], function($compileProvider) {
        $compileProvider.directive('compile', function($compile) {
            return function(scope, element, attrs) {
                scope.$watch(
                    function(scope) {
                        return scope.$eval(attrs.compile);
                    },
                    function(value) {
                        element.html(value);
                        $compile(element.contents())(scope);
                    } // End function(value)
                ); // End scope.$watch(
            }; // End return function(scope, element, attrs)
        }); // End $compileProvider.directive
    } // function($compileProvider)
);
//#7846:End
// Declaration and construction
app.controller('adPayCtrl', [ '$scope', '$rootScope', '$http', '$q', '$interval', 'uiGridConstants', '$controller', function($scope, $rootScope, $http, $q, $interval, uiGridConstants, $controller) {
    //#6972:Start
    $rootScope.lang = 'ja';
    //#6972:End

    // Only display table when click "search"
    $scope.displayTable = false;
    // $scope.has_open = false;
    $scope.rows = [];
    // For search
    $scope.search = {
        'datepicker_from' : '',
        'datepicker_to': '',
        'subcontractor_code': '',
        'subcontractor_name': '',
        'subcontractor_abbr_name' : ''
    };
    // Get data of order acceptance table for showing in grids
    $scope.allGrids = null;
    var getPageData = function(params, notFirstTime) {
        // If first time = OK => The page has loaded and user clicks "Search"
        $( "#error_date_msg" ).empty();
        if(notFirstTime) {
            $('#btn_search').addClass('loading icon-spin');
            $http({
                url : '/_admin/advance_payment/get_data',
                method : 'POST',
                data :  params
            })
            .success(function(data_listonse) {
                data_list = {};
                data_list = data_listonse;
                load_data(data_list);
                $('#btn_search').removeClass('loading icon-spin');
            });
        } else {
            load_data(data_list);
            $('#btn_search').removeClass('loading icon-spin');
        }
    }; // End  var getPageData

    // Assign data to grid to view
    function load_data(data_list) {
        $scope.nearest_closed_accounting = data_list.nearest_closed_accounting;
        $scope.grid_change = false;
        // $scope.firstTime = true;
        var subs = data_list.subcontractors;

        // Assign value time search to the header of each grid
        $scope.header_search_from = "";
        $scope.header_search_to = "";
        if(data_list.date_from!="") {
        // if(data_list.date_from.split('-').length >= 2) {
            var year = data_list.date_from.split('-')[0];
            var month = parseInt(data_list.date_from.split('-')[1]);
            // Add "0" if month is less than 10
            month = check_month_below_10(month);
            $scope.header_search_from = year+"/"+month;
            // $scope.search.datepicker_from = $scope.header_search_from;
        }
        if(data_list.date_to!="") {
        //if(data_list.date_to.split('-').length >= 2) {
            var year = data_list.date_to.split('-')[0];
            var month = parseInt(data_list.date_to.split('-')[1]);
            // Add "0" if month is less than 10
            month = check_month_below_10(month);
            $scope.header_search_to = year+"/"+month; //data_list.date_to.split('-')[0]+"/"+data_list.date_to.split('-')[1]; //data_list.subcontractors[0].information.date_search_to;
            // $scope.search.datepicker_to = $scope.header_search_to;
        }

        // if(($scope.header_search_to=="" && $scope.header_search_from=="")) {
        if(data_list.time_frame!="") {
            $scope.search_time_frame = data_list.time_frame;
        } else if($scope.header_search_from!="" && $scope.header_search_to=="") {
            $scope.search_time_frame = $scope.header_search_from + " ~ ";// + $scope.header_search_to;
        } else {
            $scope.search_time_frame = $scope.header_search_from + " ~ " + $scope.header_search_to;
        }
        // $scope.last_month = data_list.last_month;
        $scope.displayNoData = false;
        if(subs.length != 0) {
            $scope.displayTable = true;
            $scope.allGrids = subs;
        } else {
            $scope.displayNoData = true;
        }

        // Check authority to display or hide button SAVE
        if(data_list.has_authority) {
            $scope.displayBtnSave = true;
        } else {
            $scope.displayBtnSave = false;
        }
        $scope.rows = [];
        // Detail of data_list.subcontractors: key=>value: subcontractor_id => subcontractor (information (1) - orderaceptance (array))
        // console.log(data_list.subcontractors);
        var count_data_return = 0 ;
        angular.forEach(data_list.subcontractors, function(subcontractor, subcontractor_id) {
            var gridData = subcontractor.information;
            gridData.gridOption = {
                // enableFiltering : false,
                // enableRowSelection : true,
                // enableSelectAll : true,
                selectionRowHeaderWidth : 35,
                // paginationPageSizes : [ 20, 30, 50 ],
                // paginationPageSize : 20,
                rowEditWaitInterval : -1,
                // showColumnFooter : true,
                // enableCellEditOnFocus : true,
                // multiSelect : true,
                // infiniteScrollRowsFromEnd: 40,
                // infiniteScrollUp: true,
                // infiniteScrollDown: true,
                enablePagination: false,
                rowTemplate:
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ui-grid-cell ng-class="{\'back_ground_color\': (row.entity.is_deleted == 1)}" ></div>',
                data : subcontractor.order_acceptances, // (row.entity.is_deleted == 1)

                onRegisterApi: function(gridApi){
                    $scope.gridApi = gridApi;

                    $('#slide-left-btn').click(function () {
                        $interval(function () {
                            gridApi.core.handleWindowResize();
                        }, 500, 1);
                    });

                    //<-------------------------------------------------CHANGE CELL OF GRID------------------------------------------------------->
                    gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue){
                        rowEntity.has_changed = false;
                        // $scope.grid_change = true
                        //<----------------------------------------------CHANGE CELL (DELIVERY DATE)------------------------------------------------->
                        if(colDef.name == "delivery_date") {
                            var total = subcontractor.order_acceptances.length;
                            for (var i =0; i< total; i++) {
                                if(subcontractor.order_acceptances[i]['$$hashKey'] == rowEntity['$$hashKey']) {
                                   var cur_date = new Date(newValue);
                                   if ( Object.prototype.toString.call(cur_date) === "[object Date]" ) {
                                       // if ( isNaN((new Date(newValue)).getTime()) ) {
                                       if(!isNaN(cur_date.getTime())) {
                                           // cur_date = cur_date.getFullYear()+"-"+(cur_date.getMonth()+1)+"-"+cur_date.getDate();
                                           // $scope.nearest_closed_accounting = "";
                                           closed_acc_date = new Date($scope.nearest_closed_accounting);
                                           // console.log(closed_acc_date);
                                           // console.log(cur_date <= $scope.nearest_closed_accounting);
                                           if(cur_date <= closed_acc_date) {
                                               var messages = "<div class='error-message'>";
                                               messages += "ブレーンコード: "+subcontractor.information.code +"~ブレーン名:"+subcontractor.information.name+"<br/>";
                                               messages += (i+1)+"行目: <br/>";
                                               messages += "経理締め済みの年月に更新できません。"+"</div>";
                                               rowEntity.delivery_date = oldValue;
                                               alert_messages(messages);
                                           } else {
                                               rowEntity.has_changed = true;
                                           }
                                       } else {
                                           var messages = "<div class='error-message'>";
                                           messages += "ブレーンコード: "+subcontractor.information.code +"~ブレーン名:"+subcontractor.information.name+"<br/>";
                                           messages += (i+1)+"行目: <br/>";
                                           messages += lang['date_is_not_valid']+"</div>";
                                           rowEntity.delivery_date = oldValue;
                                           alert_messages(messages);
                                       }
                                    } else {
                                        var messages = "<div class='error-message'>";
                                        messages += "ブレーンコード: "+subcontractor.information.code +"~ブレーン名:"+subcontractor.information.name+"<br/>";
                                        messages += (i+1)+"行目: <br/>";
                                        messages += lang['date_is_not_valid']+"</div>";
                                        rowEntity.delivery_date = oldValue;
                                        alert_messages(messages);
                                        // alert("Date is not valid!");
                                        rowEntity.delivery_date = oldValue;
                                    }
                                }
                            }
                       } // End if(colDef.name == "delivery

                        // <-----------------------------------------------CHANGE CELL (UNIT PRICE)-------------------------------------->
                        if(colDef.name=="unit_price") {
                            rowEntity.has_changed = true;
// #7102 (Not unit_price below zero): Start 9/October/2015
                            if(!jQuery.isNumeric(newValue) || newValue < 0) {
                                rowEntity.unit_price = 0;
                            }
// #7102: End 9/October/2015
                        }

                        // <-----------------------------------------------CHANGE CELL (QUANTITY)-------------------------------------->
                        if(colDef.name=="quantity") {
                            rowEntity.has_changed = true;
                            // rowEntity.unit_price = 40;
                            if(!jQuery.isNumeric(newValue) || newValue < 0) {
                                rowEntity.quantity = 0;
                            }
                        }

                        // <-----------------------------------------------CHANGE CELL (ADJUSTED AMOUNT)-------------------------------------->
                        if(colDef.name=="adjusted_amount") {
                            rowEntity.has_changed = true;
                            // rowEntity.unit_price = 40;
                            if(!jQuery.isNumeric(newValue)) {
                                rowEntity.adjusted_amount = 0;
                            }
                        }

                        // <-----------------------------------------------CHANGE CELL (CLIENT_NAME)-------------------------------------->
                        // No delete "■"
                        if(colDef.name == "client_name") {
                            rowEntity.has_changed = true;
                            if(newValue=="") {
                                rowEntity.client_name = "■";
                            } else  {
                                if(newValue.split("")[0] !="■")
                                    rowEntity.client_name = "■"+newValue;
                            }
                        }
                    }); // End gridApi.edit.on.afterCellEdit
                    gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
                }, // End onRegisterApi: function(

                // <-----------------------------------------------COLUMN DECLARATION------------------------------------------>
                // Define columns: 12 columns: Action, Changes?, Delivery Date, Branch, Client Name, Price, Quantity, Total Amount(), Manual Input,  Accumulative Amount, Confirm
                columnDefs : [
                    {
                        visible: data_list.has_authority,
                        name : 'action',
                        enableSorting: false,
                        enableCellEdit : false,
                        // pinnedLeft : true,
                        displayName : label.action,
                        width: 120, // grid.appScope.insertRow(row.entity, row.grid)  ng-show='row.entity.delivery_date > grid.appScope.nearest_closed_accounting'
                        cellTemplate : "<button ng-click='grid.appScope.insertRow(row.entity, row.grid)' ng-show='grid.appScope.check_has_closed_acc(row.entity.delivery_date)' class='btn-small btn-green'>"+btn.add_new+"</button>"
                            +"<button ng-show='grid.appScope.check_is_extra_item_and_not_closed_acc(row.entity) " +
                                    "&& !row.entity.is_deleted' " +

                                    "ng-click='grid.appScope.deleteRow(row)' class='btn-small btn-green'>削除</button>"
                            +"<button ng-show ='grid.appScope.check_show_btn_restore(row.entity)' ng-click='grid.appScope.restore_after_delete(row.entity)' class='btn-small btn-green'>戻す</button>"
                    }, // ng-show='grid.appScope.check_is_extra_item_and_not_closed_acc(row.entity)'
                    {
                        visible: data_list.has_authority,
                        name : 'change',
                        enableSorting: false,
                        enableCellEdit : false,
                        width: 50,
                        displayName : label.change,
                        cellTemplate : "<div class='checkbox_change_css'><input  ng-show='grid.appScope.check_is_extra_item_and_not_closed_acc(row.entity)' type='checkbox' name='change' value='change' ng-model='row.entity.can_change_or_not'></div>"
                            // ng-show='grid.appScope.check_is_extra_item_and_not_closed_acc(row.entity)'
                            // ng-click='grid.appScope.can_change(row.entity)'
                    },
                    {
                        name : 'delivery_date',
                        width: 100,
                        displayName : label.delivery_date,
                        cellFilter: 'date:"yyyy/MM/dd"',
                        cellEditableCondition: function($scope) {
                            // if new row then can edit j_code
                            return $scope.row.entity.is_new_delivery_date ? true : false;
                        }
                    },
                    {
                        name : 'j_code',
                        enableCellEdit : false,
                        displayName : label.j_code,
                        width: 100
                    },
                    {
                        name : 'branch_cd',
                        width: 50,
                        enableCellEdit : false,
                        displayName : label.branch_cd
                    },
                    {
                        name : 'client_name',
                        // enableCellEdit : true,
                        displayName : label.client_name,
                        // cellFilter: 'eraseTextClientName:row',
                        cellFilter: 'calculateTotalAmount:row',
                        cellEditableCondition: function($scope) {
                            // if new row then can edit j_code
                            return $scope.row.entity.is_new_client_name ? true : false;
                        }
                    },
                    {
                        name : 'unit_price',
                        // enableCellEdit : false,
                        displayName : label.unit_price,
                        cellClass: 'a-right',
                        // cellFilter: 'calculateTotalAmount:row',
                        cellFilter: 'number:2',
// #7102 (Not unit_price below zero): Start 9/October/2015
                        cellTemplate : "<div ng-show='row.entity.unit_price > 0' > {{COL_FIELD CUSTOM_FILTERS}} </div>",
// #7102: End 9/October/2015
                        cellEditableCondition: function($scope) {
                            // if new row then can edit j_code
                            return $scope.row.entity.is_new_price ? true : false;
                        }
                    },
                    {
                        name : 'quantity',
                        // enableCellEdit : false,
                        displayName : label.quantity,
                        cellClass: 'a-right',
                        cellEditableCondition: function($scope) {
                            // if new row then can edit j_code
                            return $scope.row.entity.is_new_quantity ? true : false;
                        },
                        cellFilter: 'number:0',
                        cellTemplate : "<div ng-show='row.entity.quantity > 0'> {{COL_FIELD CUSTOM_FILTERS}} </div>"
                    },
                    {
                        name : 'totalAmount',
                        enableCellEdit : false,
                        // cellFilter: 'number:0',
                        cellClass: 'a-right',
                        displayName : label.total_amount,
                        cellFilter: 'number:0',
// #7102 (Allow total amount below zero): Start 9/October/2015
                        cellTemplate : "<div ng-show='row.entity.totalAmount != 0'> {{COL_FIELD CUSTOM_FILTERS}} </div>"
// #7102: End 9/October/2015
                    },
                    {
                        name : 'adjusted_amount',
                        cellClass: 'a-right',
                        // cellFilter: 'format_number:row',
                        // enableCellEdit : true,
                        cellFilter: 'number:0',
                        displayName : label.adjusted_amount,
                        cellEditableCondition: function($scope) {
                            // if new row then can edit j_code
                            return $scope.row.entity.is_new_manualInput ? true : false;
                        },
                        // cellTemplate : "<div ng-show='row.entity.adjusted_amount > 0'> {{COL_FIELD CUSTOM_FILTERS}} </div>"
                        cellTemplate : "<div ng-show='row.entity.adjusted_amount != 0'> {{COL_FIELD CUSTOM_FILTERS}} </div>"
                    },
                    {
                        name : 'accumulative',
                        cellClass: 'a-right',
                        cellFilter: 'number:0',
                        enableCellEdit : false,
                        displayName : label.accumulative
                    },
                    {
                        name : 'confirm',
                        enableCellEdit : false,
                        displayName : label.confirm
                    }
                    ,
                    //<!--------------------- KEEP DATA, NOT SHOW ------------------->
                    {
                        name : 'order_acceptance_status',
                        enableCellEdit : false,
                        visible: false,
                        displayName : "OA status"
                    }
                    ,
                    {
                        name : 'shipping_charge_tax',
                        enableCellEdit : false,
                        visible: false,
                        displayName : "OA sh ch tax"
                    }
                ]
            };
            //
            $scope.rows.push(gridData);
            count_data_return++;
        }); // End angular.forEach(data_list.subcontractors, function(

        if(count_data_return == 0){
            setTimeout(function () {
                $scope.$apply(function(){
                    $scope.displayNoData = true;
                });
            }, 100);
        }
    } // End function load_data

    // <!-------------------------------------------------- SET DEFAULT VALUE TO SEARCH TIME ---------------------------------------------->
    // Default date in search
    var d = new Date();
    var month = d.getMonth()+1; //(d.getMonth()+1) <10 ? "0"+(d.getMonth()+1) :(d.getMonth()+1);
    var year = d.getFullYear();
    if(data_list.last_month) {
        if(month ==1) {
            month = 12;
            year = year - 1;
        } else {
            month = month - 1;
        }
    }
    // Add "0" if month <10
    month = check_month_below_10(month); // month<10? "0" + month : month;
    var default_value = year+"/" + month;
    $scope.search.datepicker_from = default_value;
    $scope.search.datepicker_to = ""; //default_value;
    // Search data as default first load
    getPageData($scope.search, false);
    // Click "Search"
    $scope.filter = function(search) {
        validate_and_search(search);
    };

    // <!------------------------------------------------------ USER CLICK PRINT------------------------------------------->
    $scope.printPDF = function(allGrids) {
        var has_new = false;
        angular.forEach(allGrids, function(value, key) {
            angular.forEach(value.gridOption.data, function(gr_value, gr_key) {
                if(!isNaN(gr_value.is_new_added_row)) {
                    has_new = true; return;
                }
            });
        });
        // If has new data not yet saved
        if (has_new){
            // return label.confirm_close_windows;
            /*if(confirm(lang['new_value_save_and_print'])) {
                $scope.print(allGrids);
            } // End if(confirm(lang['new_value_save_and_pr */
             $("#dialog_form_messages").html(lang['new_value_save_and_print']).dialog({
                 title: "アラートメッセージ",
                 width: 'auto',
                 height: 'auto',
                 modal : true,
                 buttons: {
                     "はい": function() {
                         $(this).dialog('close');
                         // $scope.save();
                         $scope.print(allGrids);
                     },
                     "いいえ": function() {
                         $(this).dialog('close');
                     }
                 },
                 dialogClass: "order-dialog",
                 position: {my: "center", at: "center", of: window}
             });
        } // End if (has_new){
        else {
            $scope.print(allGrids);
        }
    };

    // <!------------------------------------------- USER CONFIRM SAVE NEW DATA BEFORE PRINT------------------------------------------->
    $scope.print = function (rows) {
        // var car = {type:"Fiat", model:500, color:"white"};
        var ObjPost = [];
        for(var i=0; i<rows.length; i++) {
            var jObject = {code: rows[i].code, name: rows[i].name, gridOption: rows[i].gridOption.data };
            ObjPost.push(jObject);
        }
        // return;
        // tmp#6942: Start 2015/09/30 (Fix the first time after adding new row, could not print pdf)
        // $scope.printPDFData = JSON.stringify(ObjPost);
        $("input[name='PDFData']").val(JSON.stringify(ObjPost));
        // tmp#6913: End
        if(ObjPost.length==0) {
            alert(lang['nothing_to_print']);
        } else {
            setTimeout(function(){
                $("#printPDFForm").submit();
           }, 100);
        }
    };

    // <------------------------------------------ CLICK SAVE --------------------------------------------->
    $scope.saveGrid = function(allGrids, grid, subcontractor) {
        // <!---------------------------------------  CHECK OTHER GRID HAS BEEN CHANGED ----------------------------------->
        // var has_new = false;
        var null_name = false;
        var errors = "";
        var count = 0;
        errors += "<div class='error-message'>";
        errors+= "ブレーンコード:"+subcontractor.code+" ~ ブレーン名:"+subcontractor.name+"<br/>"
        var check_has_changed = false;
        angular.forEach(grid, function(gr_value, gr_key) {
            count++;
            // If has added a row, and this row is not deleted
            if(!isNaN(gr_value.is_new_added_row) && ((gr_value.is_deleted == 0) | !isNaN(gr_value.is_deleted) ) ) {
                check_has_changed = true;
            }
            // If data of row has changed
            if(gr_value.has_changed) {
                check_has_changed = true;
            }
            //
            if(!isNaN(gr_value.is_deleted) && (isNaN(gr_value.is_new_added_row) | (gr_value.is_new_added_row==0) ) ) {
                if(gr_value.is_deleted == 1) {
                    check_has_changed = true;
                }
            }

            if(!isNaN(gr_value.is_extra_item) && gr_value.is_extra_item == 1 && (isNaN(gr_value.is_deleted) | gr_value.is_deleted == 0)) {
                // count++;
                // has_new = true;
                // errors += "<div class='error-message'>";
                if(gr_value.client_name=="■") {
                    null_name = true;
                    errors+= count + "行目: <br/> "
                    errors+= "<p class='import-error'><span class='validation-error'>会社名 が必須項目です</span></p>";
                }
            }
        });
        errors += "</div>";
        if(null_name) {
            // alert(lang['fill_client_name_before_save']);
            // return;
            alert_messages(errors);
            return;
        }

        if(!check_has_changed) {
            var messages = "<div class='error-message'>"+lang['no_new_data_to_save']+"</div>"
            alert_messages(messages);
            return;
        }
        // if(has_new) {
            url = "/_admin/advance_payment/save_grid/";
            $.post( url,
                    {
                      order_acceptances: grid,
                      id_subcontractor: subcontractor.id
                    },
                    function( data ) {
                        if (data["error"] == 0) {
                            // alert(lang['save_ok_for_subcontractor'] + subcontractor.name); save_all_ok
                            var string = lang['save_ok_for_subcontractor'];
                            string = string.replace("<<code>>", subcontractor.code);
                            string = string.replace("<<name>>", subcontractor.name);
                            var messages = "<div class='alert-success'>"+string+"</div>";
                            alert_messages(string);
                            getPageData($scope.search, true);
                        } else if(data["error"] == 2) {
                            var messages = "<div class='error-message'>"+lang['no_new_data_to_save']+"</div>"
                            alert_messages(messages);
                            return false;
                        }
                        else {
                            var messages = "<div class = 'error-message'>"+lang['save_fail_try_again']+"</div>";
                            // alert(lang['save_fail_try_again']);
                            alert_messages(messages);
                            return false;
                        }
                    },"json"
             ); // End $.post( url,
    };

    // <-------------------------------------------------------- CTR+S ----------------------------------------------------------->
   $(document).bind('keydown', function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            $scope.save();
            return false;
        }
    });

   // <!----------------------------------------------------- CHECK CHANGE PAGE NAVIGATION ------------------------------>
   $(window).bind('beforeunload', function(){
        // Trigger when form_changed return true then show confirm navigation, else dont show
        if (check_data_has_been_changed())
            return 'このページから移動しますか？ 入力したデータは保存されません。';
    });

    // <------------------------------------------------------------ CLICK ENTER TO SEARCH ----------------------------------------------------------->
    $(document).keypress(function (e) {
         var key = e.which;
         if(key == 13)  // the enter key code
          {
             var datepicker_from = $("input[name='datepicker_from']").is(':focus');
             var datepicker_to = $("input[name='datepicker_to']").is(':focus');
             var subcontractor_code = $("input[name='subcontractor_code']").is(':focus');
             var subcontractor_name = $("input[name='subcontractor_name']").is(':focus');
             var subcontractor_abbr_name = $("input[name='subcontractor_abbr_name']").is(':focus');
             if((datepicker_from | datepicker_to | subcontractor_code | subcontractor_name | subcontractor_abbr_name)) {
                 validate_and_search($scope.search);
             }
          }
     });

    // <!------------------------------------------- VALIDATION AND SEARCH ----------------------------------------------->
    function validate_and_search(search) {
        // Editted 29/9/2015 - Redmine ID: none
        if(search.datepicker_from == "") {
            $( "#error_date_msg" ).empty();
            $("#error_date_msg").append("<p class='validation-error'>発送年月を入力してください。</p>");
            return;
        }
        // If search_from = null and search_to != null => Alert insert search_from
        /*if((search.datepicker_from == "" && search.datepicker_to!="")) {
            // alert(lang['input_search_from']);
            $( "#error_date_msg" ).empty();
            $("#error_date_msg").append("<p class='validation-error'>検索範囲が正しくありません。</p>");
            return;
        }*/
        // End editted 29/9/2015
        if((search.datepicker_from != "" && search.datepicker_to!="")) {
            // If one of two day is not formatted or both are not formatted
            var processed_date_from = validate_date_from_to_search(search.datepicker_from);
            var processed_date_to = validate_date_from_to_search(search.datepicker_to);
            // CASE: from or to is not valid format
            if(!(processed_date_from && processed_date_to)) {
                $( "#error_date_msg" ).empty();
                $("#error_date_msg").append("<p class='validation-error'>発送日は日付フォーマットが無効です。</p>");
                return;
            } else {
                // Check if search from bigger than search to
                if(processed_date_from!==false && processed_date_to!== false ) {
                    if(!check_search_from_bigger_search_to(processed_date_from, processed_date_to)) {
                        $("#error_date_msg").empty();
                        $("#error_date_msg").append("<p class='validation-error'>検索範囲が正しくありません。</p>");
                        return;
                    }
                }
                if(check_data_has_been_changed()) {
                    $("#dialog_form_messages").html("変更を保存していないデータがあります。検索を続けてもよろしいですか？").dialog({
                        title: "アラートメッセージ",
                        width: 'auto',
                        height: 'auto',
                        modal : true,
                        buttons: {
                            "はい": function() {
                                $(this).dialog('close');
                                // $scope.save();
                                search.datepicker_from = processed_date_from;
                                search.datepicker_to = processed_date_to;
                                getPageData(search, true);
                            },
                            "いいえ": function() {
                                $(this).dialog('close');
                            }
                        },
                        dialogClass: "order-dialog",
                        position: {my: "center", at: "center", of: window}
                    });
                } else {
                    $( "#error_date_msg" ).empty();
                    search.datepicker_from = processed_date_from;
                    search.datepicker_to = processed_date_to;
                    getPageData(search, true);
                }
            }
        } else if(search.datepicker_from != "" && search.datepicker_to == "") {
            var processed_date_from = validate_date_from_to_search(search.datepicker_from);
            // var processed_date_to = validate_date_from_to_search(search.datepicker_to);
            if(!processed_date_from) {
                $( "#error_date_msg" ).empty();
                $("#error_date_msg").append("<p class='validation-error'>発送日は日付フォーマットが無効です。</p>");
                return;
            } else {
                $( "#error_date_msg" ).empty();
                search.datepicker_from = processed_date_from;
                /*if(processed_date_to) {
                    search.datepicker_to = processed_date_to;
                }*/
                getPageData(search, true);
            }
        } else {
            // CASE: search.datepicker_from == "" && search.datepicker_to == ""
            $( "#error_date_msg" ).empty();
            getPageData(search, true);
        }
    }

    // <!----------------------------------------------------------- MESSAGES STICKY ------------------------------------------------->
    function alert_messages(messages) {
        $(".ui-button-text").css("height", "5px");
        $("#dialog_form_messages").html(messages);
        /*if(!$scope.has_open) {
            $scope.has_open = true;*/
        $("#dialog_form_messages").dialog({
            title: '確認',
            height: 'auto',
            minWidth: 500,
            minHeight: 50,
            maxHeight: 550,
            resizable: true,
            modal: false,
            buttons: {
            },
            dialogClass: 'acceptance-dialog fixed-dialog',
            position: { my: "right top+30", at: "right top+30", of: window},
            dragStart: function(event, ui) {
                $('.acceptance-dialog').removeClass('fixed-dialog')
            },
            dragStop: function(event, ui) {
                // $('.acceptance-dialog').addClass('fixed-dialog')
            },
            resizeStart: function( event, ui ) {
                $(this).dialog("option", "maxHeight", false);
            }
        })
        // }
    }

    // <!----------------------------------------------------------------- check_data_has_been_changed ----------------------------------------------->
    function check_data_has_been_changed() {
        var has_changed = false;
        angular.forEach($scope.allGrids, function(value, key) {
            if(!has_changed) {
                angular.forEach(value.order_acceptances, function(gr_value, gr_key) {
                    // If has added a new row, and this row is not deleted
                    if(!isNaN(gr_value.is_new_added_row) && ((gr_value.is_deleted == 0) | isNaN(gr_value.is_deleted) ) ) {
                        has_changed = true;
                    }
                    // If one row has been changed
                    if(gr_value.has_changed) {
                        has_changed = true;
                    }
                    // If one row (get from DB) has been deleted
                    if(!isNaN(gr_value.is_deleted)) {
                        if((gr_value.is_deleted == 1) && (isNaN(gr_value.is_new_added_row) | (gr_value.is_new_added_row==0) ) ) {
                            has_changed = true;
                        }
                    }
                });
            }
        }); // End angular.forEach($scope.allGrids, function(value, key) {
        return has_changed;
    }

    //<!------------------------------------------------------ VALIDATE DELIVERY DATE INPUTTED ---------------------------------------------------->
    /**
     * cur_date: the date user inputted
     * before: the delivery_date of above row
     * i: row number i of grid
     * total: total rows of grid
     */
    function validate_delivery_date(cur_date, before, i, total, subcontractor) {
        if ( Object.prototype.toString.call(cur_date) === "[object Date]" ) {
            // It is a date
            if ( isNaN( cur_date.getTime() ) ) {  // Date is not valid
                // alert("1");
                return false;
            }
            else { // Date is valid
                if(i<total-1) { // If this is not the last row
                    var after = new Date(subcontractor.order_acceptances[i+1]['delivery_date']);
                    if(cur_date>after | cur_date<before) { // Date must be between "+before+" and "+after
                        // alert("2");
                        return false;
                    }
                } else if(i=total-1) { // If this is the last row
                    if($scope.search.datepicker_to!="" && validate_date_from_to_search($scope.search.datepicker_to)) {
                        var time_limit_to = $scope.search.datepicker_to.split("/")
                        var after = new Date(time_limit_to[0], time_limit_to[1], 0);
                        if(cur_date>after | cur_date<before) {
                            return false;
                        }
                    } else {
                        if(cur_date<before) {
                            return false;
                        }
                    }
                } // End } else if(i=total-1)
            }
      } else { // NOT a date
          return false;
      }
    }

    // <!-------------------------------------------------- VALIDATE SEARCH FROM - TO --------------------------------------------------->
    /**
     * 13/02 => 2013/02
     * 13/02/02 => 2013/02
     * 13/13 => false
     * 13/12/32 => false
     * 2005/03/03 => 2005/03
     * 2005/03 => 2005/03
     */
    function validate_date_from_to_search(date_var) {
        var delimiter = "-";
        if(date_var.indexOf("-") == -1) {
            delimiter = "/";
        }
        var arr = date_var.split(delimiter);

        // Check if year has limitness 4 nums and month 2 nums (fx: 00008 => error)
        /*if(arr[0].length > 4 | arr[1].length > 2) {
            console.log(arr[0]+"  /  :"+arr[1]);
            return false;
        }*/

        if(arr.length==2) {
            if(!(!isNaN(arr[0]) && !isNaN(arr[1]) && arr[0]>0 && arr[1]>0 && arr[1]<13)) {
                return false;
            } else {
                // return true;
                if(arr[0] < 100) {
                    return (parseInt(arr[0])+2000)+"/"+check_month_below_10(arr[1]);
                } else {
                    return (parseInt(arr[0]))+"/"+check_month_below_10(arr[1]);
                }
            }
        } else if(arr.length==3) {
            if(!(!isNaN(arr[0]) && !isNaN(arr[1]) && !isNaN(arr[2]) && arr[0]>0 && arr[1]>0 && arr[1]<13 && arr[2]>0 && arr[2]<32)) {
                return false;
            } else {
                // return true;
                if(arr[0] < 100) {
                    return (parseInt(arr[0])+2000)+"/"+check_month_below_10(arr[1]);
                } else {
                    return (arr[0])+"/"+check_month_below_10(arr[1]);
                }
            }
        } else {
            return false;
        }
    }

    // <!--------------------------------------------------------- VALIDATE SEARCH FROM BIGGER THAN SEARCH TO ------------>
    function check_search_from_bigger_search_to(search_from, search_to) {
        var from = search_from.split("/");
        var to = search_to.split("/");
        if(parseInt(from[0]) > parseInt(to[0])) {
            // alert("検索範囲が正しくありません。");
            return false;
        } else if (parseInt(from[0]) == parseInt(to[0])) {
            if(parseInt(from[1]) > parseInt(to[1])) {
                // alert("検索範囲が正しくありません。");
                return false;
            }
        }
        return true;
    }

    // <------------------------------------------------------------- CHECK MONTH LESS THAN 10 ---------------------------------------->
    /**
     * 1 => 01
     * 9 => 09
     * 10 => 10
     */
    function check_month_below_10(month) {
        if( parseInt(month)<10 ) {
            return "0"+parseInt(month);
        } else {
            return parseInt(month);
        }
    }

    // <------------------------------------------------------------- SAVE ALL ---------------------------------------------------------------------->
    $scope.save = function() {
        var null_name = false;
        var errors = "";
        var has = false;
        errors += "<div class='error-message'>";
        // errors+= "ブレーンコード:"+subcontractor.code+" ~ ブレーン名:"+subcontractor.name+"<br/>"
        angular.forEach($scope.allGrids, function(value, key) {
            var count = 0;
            angular.forEach(value.order_acceptances, function(gr_value, gr_key) {
                count++;
                // if(!isNaN(gr_value.is_new_added_row)) {
                if(!isNaN(gr_value.is_extra_item) && gr_value.is_extra_item == 1 && (isNaN(gr_value.is_deleted) | gr_value.is_deleted == 0)) {
                    if(!has) {
                        errors+= "ブレーンコード:"+value.information.code+" ~ ブレーン名:"+value.information.name+"<br/>";
                    }
                    has = true;
                    if(gr_value.client_name=="■") {
                        errors+= count + "行目:<br/>"
                        null_name = true;
                        errors+= "<p class='import-error'><span class='validation-error'>会社名 が必須項目です</span></p>";
                    }
                }
            });
        });
        errors += "</div>";
        // Check if has null client name
        if(null_name) {
            // alert(lang['fill_client_name_before_save']);
            alert_messages(errors);
            return;
        }
        // If no null client name => Save all grid
         $http.post('/_admin/advance_payment/save_all_grids/', {allGrids: $scope.allGrids}).
          success(function(data, status, headers, config) {
              // Save all grids OK
              if (data["error"] == 0) {
                  // alert(lang['save_all_ok']); .alert-success
                  getPageData($scope.search, true);
                  var messages = "<div class='alert-success'>"+lang['save_all_ok']+"</div>"
                  alert_messages(messages);
                  return true;
              }
              // No data changed to save
              else if(data['error']==2){
                  // alert(lang['no_new_data_to_save']);
                  var messages = "<div class='error-message'>"+lang['no_new_data_to_save']+"</div>"
                  alert_messages(messages);
                  return false;
              }
              // Save processing error
              else {
                  // alert(lang['save_fail_try_again']);
                  var messages = "<div class='error-message'>"+lang['save_fail_try_again']+"</div>"
                  alert_messages(messages);
                  return false;
              }
          }).error(function(data, status, headers, config) {
              return false;
          });
    };

    // <!----------------------------------------------------- 	CLICK CHECKBOX CHANGE? -------------------------------------->
    $scope.check_has_closed_acc = function (delivery_date) {
         // if ( Object.prototype.toString.call(delivery_date) === "[object Date]" ) {
             return (delivery_date > $scope.nearest_closed_accounting);
         // }
         // return false;
    }

    $scope.check_show_btn_restore = function (entity) {
        return (entity.is_deleted==1 && entity.is_extra_item==1);
    }
    $scope.restore_after_delete = function (entity) {
        var has_changed = false;
        // Only one grid can be able to change before change another grid.
        var gridList;
        angular.forEach($scope.allGrids, function(value, key) {
            if(!has_changed) {
                angular.forEach(value.order_acceptances, function(gr_value, gr_key) {
                    // If has added a new row, and this row is not deleted
                    if(!isNaN(gr_value.is_new_added_row) && ((gr_value.is_deleted == 0) | isNaN(gr_value.is_deleted) ) ) {
                        has_changed = true;
                        gridList = value.order_acceptances;
                        // return;
                    }
                    // If one row has been changed
                    if(gr_value.has_changed) {
                        has_changed = true;
                        gridList = value.order_acceptances;
                        // return;
                    }
                    // If one row (get from DB) has been deleted
                    if(!isNaN(gr_value.is_deleted)) {
                        if((gr_value.is_deleted == 1) && (isNaN(gr_value.is_new_added_row) | (gr_value.is_new_added_row==0) ) ) {
                            has_changed = true;
                            gridList = value.order_acceptances;
                            // return;
                        }
                    }
                });
            }
        }); // End angular.forEach($scope.allGrids, function(value, key) {
        var same_grid = false;
        if(gridList) {
            for(var i = 0; i< gridList.length; i++) {
                if(gridList[i]["$$hashKey"] == entity["$$hashKey"]) {
                    same_grid = true;
                }
            }
        } else {
            // If not grid has been changed => Add normally
            same_grid = true;
        }

        if(!same_grid) {
            /*if(confirm(lang['other_grids_changed_want_to_save'])) {
                $scope.save();
            }*/
            $("#dialog_form_messages").html("編集中のブレーンがあります。保存するか、変更を破棄して下さい。").dialog({
                title: "アラートメッセージ",
                width: 'auto',
                height: 'auto',
                modal : true,
                buttons: {
                    "保存する": function() {
                        $(this).dialog('close');
                        $scope.save();
                    },
                    "変更を破棄する": function() {
                        $(this).dialog('close');
                    }
                },
                dialogClass: "order-dialog",
                position: {my: "center", at: "center", of: window}
            });
        } else {
            entity.is_deleted = 0;
        }
    }

    $scope.check_is_extra_item_and_not_closed_acc = function (entity) {
        // if ( Object.prototype.toString.call(delivery_date) === "[object Date]" ) {
            // return date > $scope.nearest_closed_accounting;  | ((entity.is_closed_acc_record == 1) | isNaN(entity.is_closed_acc_record))
            if( (entity.delivery_date <= $scope.nearest_closed_accounting) | ((entity.is_closed_acc_record == 1) | isNaN(entity.is_closed_acc_record))  ) {
                entity.show_btn_delete = false;
                return false;
            } else {
                // If this row is loaded from DB
                if(entity.is_extra_item == 1) {
                    // If this is not added item or added item but can_change_or_not = true
                    //if(isNaN(entity.can_change_or_not) | (entity.can_change_or_not==true)) {
                    if((entity.can_change_or_not==true)) {
                        entity.can_change_or_not = true
                        entity.is_new_manualInput = true;
                        entity.is_new_quantity = true;
                        entity.is_new_price = true;
                        entity.is_new_client_name = true;
                        entity.is_new_delivery_date = true;
                    } else {
                        entity.show_btn_delete = false;
                        entity.can_change_or_not = false;
                        entity.is_new_manualInput = false;
                        entity.is_new_quantity = false;
                        entity.is_new_price = false;
                        entity.is_new_client_name = false;
                        entity.is_new_delivery_date = false;
                    }
                } else {
                    entity.show_btn_delete = false;
                    return false;
                }
                entity.show_btn_delete = true;
                return true;
            }
        // }
    }
    //<------------------------------------- INSERT NEW ROW ------------------------------------------>
    $scope.insertRow = function(row, api) {
        var has_changed = false;
        // Only one grid can be able to change before change another grid.
        var gridList;
        angular.forEach($scope.allGrids, function(value, key) {
            if(!has_changed) {
                angular.forEach(value.order_acceptances, function(gr_value, gr_key) {
                    // If has added a new row, and this row is not deleted
                    if(!isNaN(gr_value.is_new_added_row) && ((gr_value.is_deleted == 0) | isNaN(gr_value.is_deleted) ) ) {
                        has_changed = true;
                        gridList = value.order_acceptances;
                        // return;
                    }
                    // If one row has been changed
                    if(gr_value.has_changed) {
                        has_changed = true;
                        gridList = value.order_acceptances;
                        // return;
                    }
                    // If one row (get from DB) has been deleted
                    if(!isNaN(gr_value.is_deleted)) {
                        if((gr_value.is_deleted == 1) && (isNaN(gr_value.is_new_added_row) | (gr_value.is_new_added_row==0) ) ) {
                            has_changed = true;
                            gridList = value.order_acceptances;
                            // return;
                        }
                    }
                });
            }
        }); // End angular.forEach($scope.allGrids, function(value, key) {

        var same_grid = false;
        if(gridList) {
            for(var i = 0; i< gridList.length; i++) {
                if(gridList[i]["$$hashKey"] == row["$$hashKey"]) {
                    same_grid = true;
                }
            }
        } else {
            // If not grid has been changed => Add normally
            same_grid = true;
        }

        if(!same_grid) {
            /*if(confirm(lang['other_grids_changed_want_to_save'])) {
                $scope.save();
            }*/
            $("#dialog_form_messages").html("編集中のブレーンがあります。保存するか、変更を破棄して下さい。").dialog({
                title: "アラートメッセージ",
                width: 'auto',
                height: 'auto',
                modal : true,
                buttons: {
                    "保存する": function() {
                        $(this).dialog('close');
                        $scope.save();
                    },
                    "変更を破棄する": function() {
                        $(this).dialog('close');
                    }
                },
                dialogClass: "order-dialog",
                position: {my: "center", at: "center", of: window}
            });

        } else {
            var rowIndex = api.options.data.indexOf(row) + 1;
            newData = {
                    delivery_date: row.delivery_date,
                    // j_code: row.j_code,
                    // branch_cd: row.branch_cd,
                    client_name: "■",
                    can_delete: true,
                    accumulative: rowIndex + 1,
                    adjusted_amount: 0,
                    on_change: true,
                    can_change_or_not: true,
                    is_new_manualInput: true,
                    // is_new_quantity: true,
                    // is_new_price: true,
                    is_new_client_name: true,
                    is_new_delivery_date: true,
                    is_new_added_row: 1,
                    is_extra_item: 1,
                    show_btn_delete: true,
                    is_closed_acc_record: 0,
                    source_subcontractor_id: row.source_subcontractor_id,
                    shipping_type_cd: row.shipping_type_cd
            };
            api.options.data.splice(rowIndex, 0, newData);

            //<!----------------------------- FOCUS ON LAST CREATED ROW -------------------------------->
            if(api.options.data.length == (rowIndex+1)){
                $(".ui-grid-viewport").animate({ scrollTop: $(".ui-grid-viewport")[0].scrollHeight}, 'slow');
            }
            else{
                $scope.gridApi.core.scrollTo(api.options.data[rowIndex+1],api.options.columnDefs[0]);
            }
            //<!----------------------------- END FOCUS----------->
        }
    };

  //<-------------------------------------------------- DELETE NEW ROW ------------------------------------------>
    $scope.deleteRow = function(row) {

        if(isNaN(row.entity.is_deleted) | row.entity.is_deleted == 0) {
            row.entity.is_deleted = 1;
            row.entity.show_btn_delete = false;
            // $scope.btn_delete = "DELETED";
        } else {
            row.entity.show_btn_delete = true;
            row.entity.is_deleted = 0;
            // $scope.btn_delete = "DELETE";
        }
    }; // End $scope.deleteRow = function(row
} ])
    //<------------------------- AUTO CALCULATE AMOUNT, SEPARATE ORDER ACCEPTANCE STATUS AND ACCUMULATIVE ----------------------------->
    .filter('calculateTotalAmount', function() {
        return function(input, row) {
             var grid_ = row.grid;
             for(var i = 0; i< grid_.rows.length; i++) {
                 if(grid_.rows[i].entity.delivery_date == '0000-00-00' | grid_.rows[i].entity.delivery_date == null) {
                     grid_.rows[i].entity.delivery_date = "";
                 }
                 // Total amount = quantity * unit_price
                 //var totalAmount = f_floor(parseInt(grid_.rows[i].entity.quantity) * (grid_.rows[i].entity.unit_price));
                 var totalAmount = (new BigNumber(grid_.rows[i].entity.unit_price)).mul(grid_.rows[i].entity.quantity).floor().toNumber();
                 totalAmount = !isNaN(totalAmount)? totalAmount: "";
                 // Phong editted 18 Dec 2015 - Change specs - Redmine ID: 8538
                 // If shipping_charge_tax or tax_free not null (dont care order_acceptance_status)
                 // => Total amount = shipping_charge_tax + tax_free
                 // If order_acceptance_status = 2 => (shipping_charge_tax + tax_free) > 0
                 // => No happen case: status = 2 and totalAmount = price*quantity
                 // totalAmount = price*quantity: In case order_acceptance_status = 1 and two above fields equal zero
                 if(grid_.rows[i].entity.shipping_charge_tax!=0 | grid_.rows[i].entity.shipping_charge_tax!="") {
                     totalAmount = grid_.rows[i].entity.shipping_charge_tax;
                     grid_.rows[i].entity.totalAmount = totalAmount;
                 }
                 // If order acceptance has been accepted (order_acceptance_status == 2)
                 if(grid_.rows[i].entity.order_acceptance_status==2) {
                     grid_.rows[i].entity.unit_price = "";
                     grid_.rows[i].entity.confirm = "確定";
                 } else {
                     grid_.rows[i].entity.totalAmount = totalAmount;
                 }

                 var totalAmount = isNaN(grid_.rows[i].entity.totalAmount)? 0 : grid_.rows[i].entity.totalAmount;
                 var adjusted_amount = parseInt(grid_.rows[i].entity.adjusted_amount);
                 totalAmount = !isNaN(totalAmount)? totalAmount: 0;
                 adjusted_amount = !isNaN(adjusted_amount)? adjusted_amount: 0;

                 if(i>0) {
                     var accumulative_before = isNaN(grid_.rows[i-1].entity.accumulative)? 0 : grid_.rows[i-1].entity.accumulative;
                     // shipping_is_advance_payment = (isNaN(grid_.rows[i].entity.shipping_is_advance_payment) | grid_.rows[i].entity.shipping_is_advance_payment=="0")? false: true;

                     grid_.rows[i].entity.accumulative = accumulative_before - totalAmount + adjusted_amount;

                     /*if(shipping_is_advance_payment) {
                         grid_.rows[i].entity.accumulative = accumulative_before - totalAmount + adjusted_amount;
                     } else {
                         grid_.rows[i].entity.accumulative = accumulative_before - totalAmount;
                     }*/
                 } else {
                     grid_.rows[i].entity.accumulative = adjusted_amount - totalAmount;
                 }

                 /*if(grid_.rows[i].entity.unit_price == 0) {
                     grid_.rows[i].entity.unit_price = "";
                 }
                 if(grid_.rows[i].entity.quantity == 0) {
                     grid_.rows[i].entity.quantity = "";
                 }*/

                 var cummulative_previous_month = grid_.rows[i].entity.cummulative_amount_from_previous_month;
                 if(!isNaN(cummulative_previous_month) && cummulative_previous_month!=0) {
                     grid_.rows[i].entity.accumulative = cummulative_previous_month;
                 }
             }
             return input;
         } // End return function(input,
    })
 ; // End filter('calculateTotalAmount
