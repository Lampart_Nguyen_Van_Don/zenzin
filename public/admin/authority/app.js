var app = angular.module('app', ['ngTouch', 'ui.grid','ui.grid.pinning', 'angularGrid']);

app.controller('MainCtrl', ['$scope', '$http', '$q', '$interval', 'uiGridConstants', function ($scope, $http, $q, $interval, uiGridConstants) {

    $scope.gridOptions = {
        enableSorting: false,
        enableColumnMenus: false,
        groupHeaders: true,
        enableColResize: false,
        headerHeight: 100,
        angularCompileRows: true,
        rowHeight: 38
    };

    //init columns
    $scope.gridOptions.columnDefs = [
    {
        field: 'menu_group_name',
        headerName: jsGlobals[0],
        height: 'auto',
        width: 500,
        cellClass: 'cell-light'
    },
    {
        field: 'name',
        headerName: jsGlobals[1],
        height: 'auto',
        width: 850,
        template:'<a href="javascript:void(0)" data-transition="none" data-seq="{{data.id}}" id="view_detail" ng-bind="data.name"></a>'
    }
    ];

    var total = column_au.length;
    for (var i = 0; i< total; i++) {
        var auth_list = [];
        auth_list['field'] = column_au[i];
        auth_list['headerName'] = jsGlobals[i + 3];
        auth_list['editable'] = false;
        auth_list['headerGroup'] = jsGlobals[2];
        auth_list['width'] = 300;
        $scope.gridOptions.columnDefs.push(auth_list);
    }

    //get page data
    var getPageData = function(params) {
        $http({
            url : '/_admin/authority/ajax_authority_features',
            method : 'POST',
            data :  params
        })
        .success(function(resp) {
            $scope.gridOptions.rowData = resp.data;
            $scope.gridOptions.api.onNewRows();
            $scope.gridOptions.api.sizeColumnsToFit();
        });
    };

    getPageData($scope.search);

    //processing search
    $scope.filter = function(search) {
        getPageData(search);
    };

}]);

$(document).ready(function() {
    if ($('.authority-features').length) {
        $('.authority-features').resize(function() {
            var ag_body_container_width = $('.ag-scrolls .ag-body-container').width();

            $('.ag-scrolls .ag-header-container').width(ag_body_container_width);
        });
    }
});