/**
 * Common functions use in client feature
 */

var saving = false;
var storageSupport = (typeof(Storage) !== "undefined") ? true : false;

if (typeof cl_functions_loaded === 'undefined' || !cl_functions_loaded) {
    // ENTER => SEARCH
    $(document).ready(function() {
        $("#search_body").keypress(function (e) {
            var key = e.which;
            if(key == 13)  // the enter key code
            {
                e.preventDefault();
                // alert(11);
                // return;
                var search_status = $("input[name='search_status[]']").is(':focus');
                // alert(search_status);
                var search_s_code = $("input[name='search_s_code']").is(':focus');
                var search_name = $("input[name='search_name']").is(':focus');

                var search_name_kana = $("input[name='search_name_kana']").is(':focus');
                var search_division_name = $("input[name='search_division_name']").is(':focus');
                var search_phone_no = $("input[name='search_phone_no']").is(':focus');

                var search_zipcode = $("input[name='search_zipcode']").is(':focus');
                var search_select = $("select").is(':focus');

                /*var search_account = $("select[name='search_account']").is(':focus');
                var search_closing_date = $("select[name='search_closing_date']").is(':focus');
                var search_payment_month_cd = $("select[name='search_payment_month_cd']").is(':focus');
                var search_payment_day_cd = $("select[name='search_payment_day_cd']").is(':focus');*/

                var search_is_ad_receive_payment_request = $("input[name='search_is_ad_receive_payment_request']").is(':focus');
                var search_issue_s_code_date_from = $("input[name='search_issue_s_code_date_from']").is(':focus');
                var search_issue_s_code_date_to = $("input[name='search_issue_s_code_date_to']").is(':focus');

                if(search_status | search_s_code | search_name
                    | search_name_kana | search_division_name| 	search_phone_no
                    | search_zipcode | search_status | search_select
                    //| search_account | search_closing_date | search_payment_month_cd | search_payment_day_cd
                    | search_is_ad_receive_payment_request | search_issue_s_code_date_from | search_issue_s_code_date_to
                    ) {
                    $('.btn-search').click();
                }
            }
        });
        
        // ajax auto load clients
        var offset = 1;
        var is_loading = false;

        function call_ajax_get_client() {
            if (offset == null || is_loading || !is_load) {
                return;
            }
            
            is_loading = true;
            
            var loading = $('.ajax-loading');
            loading.fadeIn();
            
            var search_data = ($('[name=search_data]').length) ? $('[name=search_data]').val() : null;
            
            $.ajax({
                dataType: 'json',
                type: 'post',
                url: '/_admin/client/ajax_get_clients',
                data: {
                    offset: offset,
                    business_default: business_default,
                    search_data: search_data
                },
                success: function(data) {
                    if (data.status && data.clients != '') {
                        offset++;
                   
                        var client_table = $('#client-table');
                        var body = '';
                   
                        $.each(data.clients, function(index, client) {
                            // tr class
                            var tr_class = '';
                       
                            if (client.status == 29) {
                                tr_class = ' class="danger"';
                            } else if (client.status == 19) {
                                tr_class = ' class="gray"';
                            }
                       
                            // button add order
                            var bt_add_order = '';
                       
                            if ($.inArray(parseInt(client.status), status_is_edit) > -1) {
                                var in_charge_bu = [];
                            
                                if (accounts[client.s_account_id_1st]) {
                                    in_charge_bu.push(parseInt(accounts[client.s_account_id_1st].business_unit_id));
                                }
                                if (accounts[client.s_account_id_2nd]) {
                                    in_charge_bu.push(parseInt(accounts[client.s_account_id_2nd].business_unit_id));
                                }
//#8354:Start
                                //if ((is_edit_all && $.inArray(parseInt(accounts[login_account_id].business_unit_id), in_charge_bu) > -1)
                                //    || (is_edit_only && $.inArray(login_account_id, [parseInt(client.s_account_id_1st), parseInt(client.s_account_id_2nd)]) > -1)) {
                                if (is_add_order) {
//#8354:End
                                    bt_add_order += ' <a href="javascript:void(0)" ng-click="showAddByClient('+client.id+')" class="btn-small btn-green add-order">' + btn_order_manage + '</a>';
                                }
                            }
                       
                            body += '<tr data-id="' + client.id + '"' + tr_class + '>';
                            // buttons
                            body +=  '<td style="white-space: nowrap;"><a href="javascript:void(0)" class="btn-small btn-green btn-view view">' + common_lbl_view + '</a>' + bt_add_order + '</td>';
                            // status name
                            var status_name = '';
                            if (typeof client_status_name[client.status]['name'] != 'undefined') {
                                status_name = client_status_name[client.status]['name'];
                            }
                            body += '<td>' + status_name + '</td>';                       
                            // s_code
                            body += '<td>' + htmlspecialchars(client.s_code) + '</td>';  
                            
                            // name
                            var client_name = client.name;
                            
                            if (client.corporate_status_before > client.corporate_status_after) {
                                if (typeof company_title[client.corporate_status_before] != 'undefined') {
                                    client_name = company_title[client.corporate_status_before]['name'] + client_name;
                                }
                            } else {
                                if (typeof company_title[client.corporate_status_after] != 'undefined') {
                                    client_name = client_name + company_title[client.corporate_status_after]['name'];                                    
                                }
                            }
                            
                            body += '<td><a class="view">' + htmlspecialchars(client_name) + '</a></td>';
                            // division_name
                            body += '<td>' + htmlspecialchars(client.division_name) + '</td>';                       
                            // charge_name
                            body += '<td>' + htmlspecialchars(client.charge_name) + '</td>';                       
                            // zipcode substr($client.zipcode, 0, 3)}-{substr($client.zipcode, 3)}
                            body += '<td>' + client.zipcode.substr(0, 3) + '-' + client.zipcode.substr(3) + '</td>';                       
                            // address 1
                            body += '<td>' + htmlspecialchars(client.address_1st) + '</td>';                       
                            // address 2
                            body += '<td>' + htmlspecialchars(client.address_2nd) + '</td>';                       
                            // account name
                            body += '<td>' + htmlspecialchars(client.acc_1_name) + '</td>';
                            // credit_amount
                            body += '<td class="a-right">' + $.number(client.credit_amount) + '</td>';
                       
                            body += '</tr>';
                        });
                   
                        client_table.find('tbody tr:last-child').after(body);
                        client_table.floatThead('reflow');
                        
                        // call_ajax_get_client();
                    } else {
                        offset = null;
                    }
                },
                complete: function (data) {
                    loading.hide();
                    is_loading = false;
                },
                error: function(e){
                //alert('An error occurred: ' + e.responseText.message);
                }
            });
        }
        
        // window scroll
        $(window).scroll(function () {        	
            if ($(window).scrollTop() == ($(document).height() - $(window).height())) {
                // call ajax get clients
                if ($('#client-table').length) {
                    call_ajax_get_client();
                }
            }
        });        

        // view detail client
        $('#client-table').on('click', '.view', function() {           
            var id = $(this).parents('tr').data('id');
            $('#dialog').load('/_admin/client/detail', {
                id: id,
                redirect_form_post: redirect_data()
            }, function() {
                $('#dialog').dialog('option', 'title', title_client_detail);
                $('#dialog').dialog("option", "height", 'auto');
                $('#dialog').dialog("option", "width", 'auto');
                $('#dialog').dialog("open");
            });
        });
        
    });
    
    function saveData(key, value) {
        if (storageSupport) {
            localStorage.setItem(key, value);
        } else {
            cookies[key] = value;
        }
    }

    function loadData(key) {
        if (storageSupport) {
            return localStorage.getItem(key);
        } else {
            return cookies[key];
        }
    }

    function hideSearch(animate) {
        $('#search_toggle').css('width', $('#search_body').width());
        $('#search_toggle td').css('border-bottom', '1px solid #ddd');
        $('#search_toggle a').text(lbl_open_search);
        if (animate) {
            $('#search_body').slideUp();
        } else {
            $('#search_body').hide();
        }

        saveData('cl_showSearch', 0);
    }

    function showSearch(animate) {
        $('#search_toggle').css('width', '100%');
        $('#search_toggle td').css('border-bottom', 'none');
        $('#search_toggle a').text(lbl_hide_search);
        if (animate) {
            $('#search_body').slideDown();
        } else {
            $('#search_body').show();
        }

        saveData('cl_showSearch', 1);
    }

    function company_title_handler() {
        $('.company_title').each(function () {
            if ($(this).val() != '') {
                $('.company_title').not(this).prop('disabled', true);
            }
        });
        $('.company_title').change(function () {
            if ($(this).val() == '') {
                $('.company_title').prop('disabled', false);
            } else {
                $('.company_title').not(this).prop('disabled', true);
            }
        });
    }

    function convert_kana_handler() {
        $('#name').bind("change keyup input", function () {
            setRuby('#name', '#name_kana');
        });
    }

    function auto_calculate_profit() {
        function calculate(profit_amount, delivery_amount) {
            if (profit_amount && delivery_amount) {
                //return f_floor((profit_amount / delivery_amount * 100) * 100) / 100;
                return (new BigNumber(profit_amount)).div(delivery_amount).mul(100).round(2, BigNumber.ROUND_FLOOR).toNumber();
            } else {
                return '';
            }
        }

        $('.delivery_amount').change(function () {
            var profit_amount = parseInt($(this).parents('td').find('.profit_amount').val());
            var delivery_amount = parseInt($(this).val());
            $(this).parents('td').find('.profit_rate').val(calculate(profit_amount, delivery_amount));
        });
        $('.profit_amount').change(function () {
            var delivery_amount = parseInt($(this).parents('td').find('.delivery_amount').val());
            var profit_amount = parseInt($(this).val());
            $(this).parents('td').find('.profit_rate').val(calculate(profit_amount, delivery_amount));
        });
    }

    function credit_input_handler(disabled_is_ad_receive_payment) {
        if (disabled_is_ad_receive_payment) {
            return;
        }

        var LEVEL_A = 1;
        var LEVEL_B = 2;
        var LEVEL_C = 3;
        var LEVEL_NG = 9;

        var is_receive_payment_change_handler = function () {
            if ($('[name=is_ad_receive_payment]').prop('checked')) {
                $('[name=credit_amount]').val('0');
            } else {
//#6869:Start
                //var credit_amount = parseInt($('[name=credit_amount]').val());
                var credit_amount = parseInt($('[name=credit_amount]').val().replace(/,/g, ''));
//#6869:End
                if (credit_amount == 0) {
                    $('[name=credit_amount]').val('');
                }
            }
        };
        var credit_amount_change_handler = function () {
//#6869:Start
            //var credit_amount = parseInt($('[name=credit_amount]').val());
            var credit_amount = parseInt($('[name=credit_amount]').val().replace(/,/g, ''));
//#6869:End
            if (credit_amount == 0) {
                $('[name=is_ad_receive_payment]').prop('checked', true);
            } else {
                $('[name=is_ad_receive_payment]').prop('checked', false);
            }
        };

        $('[name=credit_level]').change(
            function () {
                var credit_level = $(this).val();
                // Case credit level is A or B
                if (credit_level == LEVEL_A || credit_level == LEVEL_B) {
                    // Attach change event handler
                    $('[name=is_ad_receive_payment]').change(
                        is_receive_payment_change_handler);
                    $('[name=credit_amount]').change(
                        credit_amount_change_handler);

                    // Enable both input
                    $('[name=is_ad_receive_payment]').prop('disabled', false);
                    $('[name=credit_amount]').prop('disabled', false);

                    // Trigger change handler for 1st load
                    is_receive_payment_change_handler();
                    credit_amount_change_handler();
                } else {
                    // If it's not A or B, unbind the change handler
                    $('[name=is_ad_receive_payment]').unbind('change',
                        is_receive_payment_change_handler);
                    $('[name=credit_amount]').unbind('change',
                        credit_amount_change_handler);

                    // Case credit level C: uncheck & disable advance payment
                    if (credit_level == LEVEL_C) {
                        $('[name=is_ad_receive_payment]')
                            .prop('checked', false);
                        $('[name=is_ad_receive_payment]')
                            .prop('disabled', true);

                        $('[name=credit_amount]').prop('disabled', false);

                        // Case credit level NG, check advance payment, assign
                        // 0 to credit amount then disable both input
                    } else if (credit_level == LEVEL_NG) {
                        $('[name=is_ad_receive_payment]')
                            .prop('checked', true);
                        $('[name=is_ad_receive_payment]')
                            .prop('disabled', true);

                        $('[name=credit_amount]').val('0');
                        $('[name=credit_amount]').prop('disabled', true);

                    } else {
                        $('[name=is_ad_receive_payment]').prop('disabled',
                            false);
                        $('[name=credit_amount]').prop('disabled', false);
                    }
                }
            });
    }

    function adv_receive_handler(disable) {
        if (disable == 'disabled') {
            return;
        }
        $('[name=is_ad_receive_payment_request]').change(function () {
            var disabled = $(this).prop('checked');
            $('[name=closing_date]').prop('disabled', disabled);
            $('[name=payment_month_cd]').prop('disabled', disabled);
            $('[name=payment_day_cd]').prop('disabled', disabled);
        });
        $('[name=is_ad_receive_payment_request]').trigger('change');
    }

    var save_key_handler = function (e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            if (!saving) {
                e.data.btn.trigger('click');
            }
            return false;
        }
    };

    function bind_save_key(submit_btn) {
        $(document).bind('keydown', {btn: submit_btn}, save_key_handler);
    }

    function unbind_save_key() {
        $(document).unbind('keydown', save_key_handler);
    }
//#7158:Start
//    function do_action(url, action, data, id) {
    function do_action(url, action, data, id, dialog_container) {
//#7158:End
        saving = true;
        $('.error').html('');
        $('.message').html('');
        $('.message').removeClass('caution').removeClass('success-message');
        $('button[name=action]').prop('disabled', true).removeClass('btn-green').addClass('btn-gray');

        data = 'action=' + action + '&' + data;

        $.post(url, data, function (result) {
            $('.message').text(result.message);
            if (!result.status) {

                saving = false;

                $('button[name=action]').prop('disabled', false).addClass('btn-green').removeClass('btn-gray');

                if (result.hasOwnProperty('errors')) {
                    $.each(result.errors, function (k, v) {
                        $('[name=' + k + ']').siblings('.error').html(
                            '<div class="validation-error">' + v + '</div>');
                    });
                } else {
                    $('.message').addClass('caution');
                }
            } else {
                modified = true;
                form_changed = false;
                if (action == 'edit') {
                    modified = false;
                    $('.message').html('');
//#7158:Start
//                    load_detail('/_admin/client/detail', id, '#dialog');
                    load_detail('/_admin/client/detail', id, dialog_container);
//#7158:End
                    return;
                }
                if (action != 'delete') {
                    $('.message').addClass('success-message blink_me');
                } else {
                    $('#search_form').submit();
                }
            }
        });
    }

    function load_detail(url, id, dialog_form) {
        $(dialog_form).load(url, {
            id: id,
//#7158:Start
            dialog_selector: dialog_form
//#7158:End
        }, function() {
            $(dialog_form).dialog('close');
            // #6849 - Modifly - S
            $(dialog_form).dialog( "option", "title", lang['text_title_show_client'] );
             // #6849 - Modifly -E
            $(dialog_form).dialog( "option", "height", 'auto' );
            $(dialog_form).dialog( "option", "width", 'auto' );
            $(dialog_form).dialog("open");
            $(".result_message").addClass('success-message blink_me');
            $(".result_message").html("保存しました。");
        });
    }

    cl_functions_loaded = true;
}
