$( document ).ready(function() {
    $('button[name=take_action]').click(function () {
        $('button[name=take_action]').addClass('loading icon-spin').blur();
        $.post(
            '/_admin/closing_accountant/take_action',
            {take_action_type: $('input[name=take_action_type]').val()},
            function (data) {
                $('button[name=take_action]').removeClass('loading icon-spin');
                if (data.status == 0) {
                    alert_messages(data.message);
                } else {
                    // $("#dialog-messages").html(data.message);
                    $("#dialog-messages").html(data.message).dialog({
                        title: "確認",
                        width: 'auto',
                        height: 'auto',
                        modal : true,
                        close: function () {
                            window.location.href = '/_admin/closing_accountant/show';
                        },
                        buttons: {
                            "OK": function() {
                                $(this).dialog('close');
                                window.location.href = '/_admin/closing_accountant/show';
                            }
                        },
                        dialogClass: "order-dialog",
                    });
                    /*$("#dialog-messages").dialog({
                        title: '確認',
                        width: 'auto',
                        height: 160,
                        modal: true,
                        dialogClass: "acceptance-dialog",
                        close: function () {
                            window.location.href = '/_admin/closing_accountant/show';
                        },
                        buttons: [{
                            text: "OK",
                            click: function() {
                                $(this).dialog("close");
                                window.location.href = '/_admin/closing_accountant/show';
                            }
                        }],
                    });*/
                }
            },
            "json"
        );
    });
});

function alert_messages(messages) {
    $(".ui-button-text").css("height", "5px");
    $("#dialog_form_messages").html(messages);
    $("#dialog_form_messages").dialog({
        title: '確認',
        height: 'auto',
        minWidth: 500,
        minHeight: 50,
        maxHeight: 550,
        resizable: true,
        modal: false,
        buttons: {
        },
        dialogClass: 'acceptance-dialog fixed-dialog',
        position: { my: "right top+30", at: "right top+30", of: window},
        dragStart: function(event, ui) {
            $('.acceptance-dialog').removeClass('fixed-dialog')
        },
        resizeStart: function( event, ui ) {
            $(this).dialog("option", "maxHeight", false);
        }
    })
}