// FINISH WORK REPORT

//#7846:Start
// var app = angular.module('app', ['ngTouch', 'ui.grid', 'ui.grid.edit', 'ui.grid.pinning', 'ui.grid.resizeColumns']);
var app = angular.module('app', ['ui.grid', 'ui.grid.edit', 'ui.grid.pinning', 'ui.grid.resizeColumns']);
//#7846:End
app.controller('MainCtrl', ['$scope', '$rootScope', '$http', '$q', '$interval', 'uiGridConstants', function ($scope, $rootScope, $http, $q, $interval, uiGridConstants) {

    //init search data
    var today = new Date();
//7184:Start
    var initial_status = false;
//7184:End
    $scope.search = {
            'delivery_date_from' : '',
            'delivery_date_to': '',
            'j_code_branch_code_from': '',
            'j_code_branch_code_to': '',
            's_code' : '',
            'client_name' : '',
            'report_is_complete': "0",
            'arrange_rep' : "0"
    };
    $rootScope.lang = 'ja';
    $scope.gridOptions = {
          enableColumnResizing: true,
          enableSorting: true,
          rowTemplate:
              '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ui-grid-cell></div>'
    };

    //init columns
    $scope.gridOptions.columnDefs = [
    { name: 'id', displayName: fwr_jsGlobals[0], enableCellEdit: false, enableSorting: false, enablePinning:true, width: '4%', cellClass: 'grid-align',
        cellTemplate: "<input type='checkbox' ng-model='row.entity.edit' />"
    },
    { name: 'download_link', displayName: fwr_jsGlobals[1], enableCellEdit: false, enableSorting: false, enablePinning:true, width: '4%', cellClass: 'grid-align',
        cellTemplate: '<div class="ngCellText cell" ng-class="col.colIndex()"><a class="cell-link" ng-click="grid.appScope.states.export_excel(row)">[DL]</a></div>'
    },
    { name: 'report_is_complete', displayName: fwr_jsGlobals[2] , enableCellEdit: false, width: '4%', cellClass: 'grid-align'},
    { name: 'j_code', displayName: fwr_jsGlobals[3] , enableCellEdit: false, width: '7%',
    	//#6965: Start
    	//cellTemplate: '<div class="ngCellText cell"><a ng-click="grid.appScope.loadDetail(row.entity.order_id, true)">{{row.entity.j_code}}</a></div>'
    	cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.loadDetail(row.entity.order_id, true)">{{row.entity.j_code}}</a></div>'
    	//#6965:End
    },
    { name: 'branch_cd', displayName: fwr_jsGlobals[4] , enableCellEdit: false, width: '3%', cellClass: 'grid-align'},
    { name: 'client_name', displayName: fwr_jsGlobals[5] , enableCellEdit: false, width: '14%',
    	cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.showClient(row.entity.client_id)">{{row.entity.client_name}}</a></div>'
    },
//7910:Start
    { name: 'report_division_name', displayName: fwr_jsGlobals[6] , width: '12%', enableCellEdit: ($.parseJSON(show_column)["number_of_print"] !== undefined)? false : true},
    //{ name: 'report_charge_name', displayName: fwr_jsGlobals[7] , enableCellEdit: true, width: '12%'},
    { name: 'report_charge_name', displayName: fwr_jsGlobals[7] , enableCellEdit: true, width: '12%', visible: ($.parseJSON(hide_column)["report_charge_name"] !== undefined)? false : true},
    //{ name: 'report_fax_no', displayName: fwr_jsGlobals[8] , enableCellEdit: true, width: '9%'},
    { name: 'report_fax_no', displayName: fwr_jsGlobals[8] , enableCellEdit: true, width: '9%', visible: ($.parseJSON(hide_column)["report_fax_no"] !== undefined)? false : true},
    { name: 'quantity', displayName: fwr_jsGlobals[17] , enableCellEdit: false, width: '9%', visible: ($.parseJSON(show_column)["number_of_print"] !== undefined)? true : false},
//7910:End
    { name: 'delivery_date', displayName: fwr_jsGlobals[9] , enableCellEdit: false, width: '7%', cellClass: 'grid-align', cellFilter : 'formatDateFilter'},
//7910:Start
    //{ name: 'report_contents', displayName: fwr_jsGlobals[10] , enableCellEdit: true, width: '12%'},
    { name: 'report_contents', displayName: fwr_jsGlobals[10] , enableCellEdit: true, width: '12%', visible: ($.parseJSON(hide_column)["report_contents"] !== undefined)? false : true},
//7910:End
    { name: 'report_remarks', displayName: fwr_jsGlobals[11] , enableCellEdit: true},
    ];

    $scope.gridOptions.onRegisterApi = function(gridApi){
        //set gridApi on scope
        $scope.gridApi = gridApi;
        gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue) {
            if (newValue != oldValue) {
                updatePageData(rowEntity);
                $scope.$apply();
            }
        });
    };
    $scope.showClient = function (client_id) {
        $.post(
            '/_admin/client/detail',
            {id : client_id, dialog_selector: '#clientDialog', from_finish_work_report: true},
            function(data) {
                clientDialog.html(data);
                clientDialog.dialog("option", "position", {my: "center", at: "center", of: window});
                clientDialog.dialog('open');
            });
    };

//7184:Start
    $scope.checkalllist = function() {
    	datas = $scope.gridApi.grid.appScope.gridOptions.data;
    	for(i = 0; i < datas.length; i++) {
    		datas[i].edit = !initial_status
    	}
    	initial_status = !initial_status;
    };
//7184:End

    $('#slide-left-btn').click(function () {
        $interval(function () {
            $scope.gridApi.core.handleWindowResize();
        }, 500, 1);
    });

    //get page data
    var getPageData = function(params) {
        // Add circle loading to button search
        $('.btn-search').addClass('loading icon-spin');

        $http({
            url : '/_admin/finish_work_report/ajax_get_order_acceptance',
            method : 'POST',
            data :  params
        })
        .success(function(resp) {
            $('span.validation-error').html('').hide();

            if (resp.result === true) {
                //$('span#delivery_date_error_msg').html('').hide();
                //
                //display data to grid
                $scope.gridOptions.data = resp.data;
//7184:Start
                initial_status = false;
//7184:End
                if (resp.data.length == 0) {
                    $scope.showWatermark = true;
                } else {
                    $scope.showWatermark = false;
                    $scope.gridOptions.data = resp.data;
                }

            } else {
                var error_msg = '';
                $.each(resp.msg, function(index, val) {
                    error_msg += val;
                    $('span#' + index).html(val).show();
                });
                //$('span#delivery_date_error_msg').html(error_msg).show();

                $scope.gridOptions.data = resp.data;
                $scope.showWatermark = true;
            }
            // Remove circle loading from button search
            $('.btn-search').removeClass('loading icon-spin');
        });
    };

    // <------------------------------------------------------------ CLICK ENTER TO SEARCH ----------------------------------------------------------->
    $(".table-simple").keypress(function (e) {
           var key = e.which;
           if(key == 13)  // the enter key code
            {
               // alert("FFFFFFFF");
               // $('.btn-search').click();
               // return;
               e.preventDefault();
               var delivery_date_from = $("input[name='delivery_date_from']").is(':focus');
               var delivery_date_to = $("input[name='delivery_date_to']").is(':focus');
               var j_code_branch_code_from = $("input[name='j_code_branch_code_from']").is(':focus');
               var j_code_branch_code_to = $("input[name='j_code_branch_code_to']").is(':focus');

               var s_code = $("input[name='s_code']").is(':focus');
               var client_name = $("input[name='client_name']").is(':focus');

               var arrange_rep = $("select[name='arrange_rep']").is(':focus');

               var report_is_complete = $("input[name='report_is_complete']").is(':focus');

               if(delivery_date_from | delivery_date_to | j_code_branch_code_from | j_code_branch_code_to
                       | s_code | client_name
                       | arrange_rep
                       | report_is_complete
                      ) {
                   // Search
                   // alert(111);
                   // updatePageData($scope.search);
                   $("#submit").click();
               }
            }
   });
   // <------------ END CLICK ENTER TO SEARCH ------------------------>

    //update page data
    var updatePageData = function(rowEntity) {
        $('.btn-download').attr('disabled', 'disabled');

        $http({
            url : '/_admin/finish_work_report/ajax_update_order_acceptance',
            method : 'POST',
            data :  rowEntity
        })
        .success(function(resp) {
            $('div.error-message').hide();
            $('div.success-message').hide();

            if ($("#dialog-errors").is(':visible')) {
                $("#dialog-errors").dialog('close');
            }

            $('.btn-download').removeAttr('disabled');

            // update success
            if (resp.result === true) {
                rowEntity.fax_invalid = false;
                $("#message").html('<span class="success-message blink_me">' + resp.msg[0] + '</span>').show();
            } else {
                /*$("#message").html('');
                var error_msg = '';
                $.each(resp.msg, function(index, val) {
                    error_msg += val;
                    error_msg += '<br/>';
                });
                $('div.error-message').html(error_msg).show();*/
                rowEntity.fax_invalid = true;

                $("#message").hide();


                error = "<div class='error-message'>";
                error += '<span class="row-error-icon">' + resp.msg['title'] + '</span>';
                error += '<p class="import-error"><span class="validation-error">' + resp.msg['message'] + '</span></p>';
                error += "</div>";

                $("#dialog-errors").html(error);
                $("#dialog-errors").dialog({
                    title: '確認',
                    height: 'auto',
                    minWidth: 500,
                    minHeight: 250,
                    maxHeight: 250,
                    resizable: true,
                    modal: false,
                    dialogClass: 'acceptance-dialog fixed-dialog',
                    position: {
                        my: "right top+30",
                        at: "right top+30",
                        of: window
                    },
                    dragStart: function(event, ui) {
                        $('.acceptance-dialog').removeClass('fixed-dialog')
                    },
                    dragStop: function(event, ui) {
                    // $('.acceptance-dialog').addClass('fixed-dialog')
                    },
                    resizeStart: function( event, ui ) {
                        $(this).dialog("option", "maxHeight", false);
                    }
                });
                $("#dialog-errors").dialog('open');
            }
        });
    };

    getPageData(null);

    //processing search
    $scope.filter = function(search) {
        $("#message").html('');
        form_changed = false;
        form_submitted = true;
        getPageData(search);
        $('div.success-message').hide();
        $('div.error-message').hide();
    };

    function check_valid_fax(fax) {
        if(!/^[0-9\-]+$/i.test(fax)) {
            return false
        } else {
            if (fax.split('-').length == 1) {
                return false;
            }
        }

        return true;
    }
    $scope.states = {
            export_excel: function(row) {
            	   submitform1 = 1;
            	   flag = false;
//            	   console.log('aaaaaaaaaaaaaaaaaaaa'+submitform1+' aaaaaaaaaaaaaaaaaa: /_admin/finish_work_report/export_excel');
//            	   submitform1 = 0;
//                   return;
                // var msg_error = '';

                /*if (!row.entity.report_fax_no) {
                    msg_error = fwr_jsGlobals[15];
                } else if (!check_valid_fax(row.entity.report_fax_no)) {
                    msg_error = fwr_jsGlobals[16];
                }*/
                /*if(!row.entity.report_contents | row.entity.report_contents=='') {
                    msg_error = '内容が必要です。';
                }
                if (msg_error != '') {
                    $("div#frm_print").html(msg_error);
                    $("div#frm_print").dialog({
                        title: '注意',
                        width: 180,
                        height: 140,
                        modal: true,
                        dialogClass: "finish-work-report-dialog",
                        resizable: false,
                        buttons: [{
                            text: "OK",
                            click: function() {
                                $(this).dialog("close");
                            }
                        }],
                        position: {my: "center", at: "center", of: window}
                    });

                    return;
                }*/

                //submit form

            		var form_changed = false;
            		var form_submitted = true;

                     //var form = '<form action="/_admin/finish_work_report/export_excel" method="POST">'

                     var form2 = '<form id="frm2" action="/_admin/finish_work_report/export_excel" method="POST" target="_self">'
                                + '<input type="hidden" name="id" value="' + row.entity.id + '"/>'
                                + '</form>';

                     $('div#frm_print').append(form2);
                     $('div#frm_print').find('#frm2').submit();
                     return;
            }

    };


    $("body").on('click', '.btn-download-print', function(event) {

			      datas = $scope.gridApi.grid.appScope.gridOptions.data;
			      //print_r(datas);
			      printIds = [];
			      var flag = false;
			      var errors = [];
			      for(i = 0; i < datas.length; i++) {
			          if (datas[i].edit) {

			              /*if (datas[i].report_fax_no) {
			                  var fax = datas[i].report_fax_no;

			                  if (!check_valid_fax(datas[i].report_fax_no)) {
			                      errors.push({
			                          num: i + 1,
			                          msg: fwr_jsGlobals[16]
			                      });
			                  } else {
			                      printIds.push(datas[i].id);
			                      flag = true;
			                  }
			              } else {
			                  errors.push({
			                      num: i + 1,
			                      msg: 'FAX番号が必要です。'
			                  });
			              }*/


//7910:Start
			        	  //if(!datas[i].report_contents | datas[i].report_contents=='') {
			        	  if(!datas[i].report_contents || datas[i].report_contents=='') {
			            	  if (business_unit_name != "DP") {
				                  errors.push({
				                      num: i + 1,
				                      msg: '内容が必要です。'
				                  });

			            	  } else {
				                  printIds.push(datas[i].id);
				                  flag = true;
			            	  }

//7910:End
			              } else {
			                  printIds.push(datas[i].id);
			                  flag = true;
			              }
			          }
			      }

			      if (errors.length != 0) {
			          error = "<div class='error-message'>";

			          $.each(errors, function( key, value ) {
			              error += '<span class="row-error-icon">' + value.num + '行目</span>';
			              error += '<p class="import-error"><span class="validation-error">' + value.msg + '</span></p>';
			          });

			          error += "</div>";

			          $("#dialog-errors").html(error);
			          $("#dialog-errors").dialog({
			              title: '確認',
			              height: 'auto',
			              minWidth: 500,
			              minHeight: 125,
			              maxHeight: 500,
			              resizable: true,
			              modal: false,
			              dialogClass: 'acceptance-dialog fixed-dialog',
			              position: {
			                  my: "right top+30",
			                  at: "right top+30",
			                  of: window
			              },
			              dragStart: function(event, ui) {
			                  $('.acceptance-dialog').removeClass('fixed-dialog')
			              },
			              dragStop: function(event, ui) {
			              // $('.acceptance-dialog').addClass('fixed-dialog')
			              },
			              resizeStart: function( event, ui ) {
			                  $(this).dialog("option", "maxHeight", false);
			              }
			          });
			          $("#dialog-errors").dialog('open');

			          return;
			      }

			      if (printIds.length == 0) {

			          $("div#frm_print").html(fwr_jsGlobals[13]);
			          $("div#frm_print").dialog({
			              title: '注意',
			              width: 'auto',
			              height: 150,
			              modal: true,
			              dialogClass: "finish-work-report-dialog",
			              resizable: false,
			              buttons: [{
			                  text: "OK",
			                  click: function() {
			                  $(this).dialog("close");
			                  }
			              }],
			              position: {my: "center", at: "center", of: window}
			          });

			          return;
			      }

			      var form_changed = false;
			      var form_submitted = true;
			     // console.log(printIds.join());
			     // return;
			      //submit form
			      var form = '<form id="frm1" action="/_admin/finish_work_report/print_report?id_download=0" method="POST" target="_blank">'
			                 + '<input type="hidden" name="ids" value="' + printIds.join() + '"/>'
			                 + '</form>';

			      $('div#frm_print').html(form);
			      $('div#frm_print').find('#frm1').submit();
      return;
    });
    //processing print
//    $scope.print_pdf = function() {
//
//    };

    //processing download
      return;
}])
.filter('formatDateFilter', function() {
    return function (value) {
        if (value) {
            return value = value.replace(/-/g, '/');
        }
    };
})
.filter('mapYesNo', function() {
    var mapHash = {
            0 : '未',
            1 : '済'
        };

    return function(input) {
        if (!input){
            return '-';
        } else {
            // console.log(input);
            // return "xx";
            return mapHash[input];
        }
    };
})
.filter('mapTaxType', function() {
    return function(input) {
        if (!input) {
            return '';
        } else {
            for (i = 0; i < tax_division.length; i++) {
                if (tax_division[i].code == input) {
                    return tax_division[i].name;
                }
            }
        }
    };
})
.filter("nl2br", function() {
    return function(data) {
        if (!data) return data;
        return data.replace(/\n\r?/g, '<br />');
  };
});