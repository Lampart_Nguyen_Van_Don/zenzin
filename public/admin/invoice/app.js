// INVOICE
//#7846:Start
// var app = angular.module('app', [ 'ngTouch', 'ngSanitize', 'ui.grid', 'ui.grid.pinning', 'ui.grid.resizeColumns']);
var app = angular.module('app', ['ngSanitize', 'ui.grid', 'ui.grid.pinning', 'ui.grid.resizeColumns']);
//#7846:End
angular.module('app').directive('compile', ['$compile', function ($compile) {
  return function(scope, element, attrs) {
      scope.$watch(
        function(scope) {
          return scope.$eval(attrs.compile);
        },
        function(value) {
          element.html(value);
          $compile(element.contents())(scope);
        }
      )};
}]);

app.controller('MainCtrl', ['$scope', '$rootScope', '$http', '$q', '$interval', 'uiGridConstants', function($scope, $rootScope, $http, $q, $interval, uiGridConstants, uiGridExporterConstants) {

    $rootScope.lang = 'ja';
    $rootScope.load = true;
    $rootScope.print_invoice = function(sales_slip_id) {

        dom = '<form action="/_admin/invoice/approve_and_print" method="POST" target="_blank">' +
                    '<input type="hidden" name="sales_slip_ids" value="' + sales_slip_id + '"/>' +
            '</form>';
        angular.forEach($scope.gridOptions.data, function(row, key) {

             if(row.sales_slip_status == 22){
                  if (row.id == sales_slip_id) row.sales_slip_status = ss_status.inv_issued;
             }
        });
        $(dom).appendTo('body').submit();
        return;

    }

    // <------------------------------------------------------------ CLICK ENTER TO SEARCH ----------------------------------------------------------->
     $("#search-form").keypress(function (e) {
            var key = e.which;
            if(key == 13)  // the enter key code
             {

                e.preventDefault();
                var sales_slip_number = $("input[name='sales_slip_number']").is(':focus');
                var client_name = $("input[name='client_name']").is(':focus');
                var delivery_date_from = $("input[name='delivery_date_from']").is(':focus');
                var delivery_date_to = $("input[name='delivery_date_to']").is(':focus');

                var from_billing_date = $("input[name='from_billing_date']").is(':focus');
                var to_billing_date = $("input[name='to_billing_date']").is(':focus');
                var from_payment_date = $("input[name='from_payment_date']").is(':focus');
                var to_payment_date = $("input[name='to_payment_date']").is(':focus');

                var s_code = $("input[name='s_code']").is(':focus');
                var name_kana = $("input[name='name_kana']").is(':focus');
                var division_name = $("input[name='division_name']").is(':focus');

                var sales_slip_status = $("input[name='sales_slip_status[]']").is(':focus');

                var j_account_business_unit_id = $("select[name='j_account_business_unit_id']").is(':focus');
                var j_account_id = $("select[name='j_account_id']").is(':focus');
                var s_account_business_unit_id = $("select[name='s_account_business_unit_id']").is(':focus');
                var s_account_id = $("select[name='s_account_id']").is(':focus');

                if(sales_slip_number | client_name | delivery_date_from | delivery_date_to
                        | from_billing_date | to_billing_date | from_payment_date | to_payment_date
                        | s_code | name_kana | division_name
                        | sales_slip_status
                        | j_account_business_unit_id | j_account_id | s_account_business_unit_id | s_account_id
                       ) {
                    // Search
                    $rootScope.search();
                }
             }
    });
    // <------------ END CLICK ENTER TO SEARCH ------------------------>

    $rootScope.search = function() {
    	 $scope.is_over_record = false;
        // Add circle loading to button search
        $('.btn-search').addClass('loading icon-spin');

//#6688:Start
        // don't auto check status of invoice after search
        if (typeof is_submit_default != 'undefined') {
            is_submit_default = false;
        }
//#6688:End

        form_data = $("#search-form").serialize();

        $.ajax({
            url: '/_admin/invoice/show',
            type: 'POST',
            data: form_data
        })
        .done(function(data) {
           $("#billing_date_error").hide();
           $("#payment_date_error").hide();
           $("#delivery_date_error").hide();

           if (data.status == 'success') {

//#7087:Start
//               if (data.is_over_record) {
//
//               }
//#7087:End

                 // #6818:S_  Modifly
                    var  data_sales_slip_search  = set_is_approvel(data.sales_slips);
                    // Search date invice -S
                    if(data_sales_slip_search.length == 0){
                        $scope.showWatermark = true;
//#7087:Start
                        $scope.is_over_record = false;
//#7087:End
                    }
                    else{
                        $scope.showWatermark = false;
//#7087:Start
                        $scope.is_over_record = false;
//#7087:End
                    }
                    // Search date invice -E
                $scope.gridOptions.data = data_sales_slip_search;
                // #6818:E_ Modifly
                // Assign data to grid
                //$scope.gridOptions.data = data.sales_slips;


                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);

            }

//#7087:Start
            else if(data.status == 'is_over_record'){
            	 $scope.gridOptions.data = [];
            	 $scope.showWatermark = false;
            	 $scope.is_over_record = true;
            }
//#7087:End
            else {

                $scope.showWatermark = true;
//#7087:Start
                $scope.is_over_record = false;
//#7087:End
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                $scope.gridOptions.data = [];

                // Show errors on form
                if (data.mesg.billing_date) {
                    $("#billing_date_error").html(data.mesg.billing_date).show();
                }

                if (data.mesg.payment_date) {
                    $("#payment_date_error").html(data.mesg.payment_date).show();
                }

                if(data.mesg.delivery_date){
                    $("#delivery_date_error").html(data.mesg.delivery_date).show();
                }
            }
           // Remove circle loading from button search
           $('.btn-search').removeClass('loading icon-spin');
        })

    }
    $("body").on('click', '.discard-invoice', function(event) {

        $(".messages").text('');
        var sales_slip_id = $(this).attr('data-sales-slip-id');
        $("#dialogdiscard").dialog({
            title: '請求書破棄',
            width: 'auto',
            height: 147,
            modal: true,
            dialogClass: "acceptance-dialog",
            buttons: {
                   " はい": function() {
                           // console.log('sdsdsd'+sales_slip_id);
                            $.ajax({
                             url: '/_admin/invoice/ajax_discard_invoice_sale',
                             type: 'POST',
                             data: {sales_slip_id: sales_slip_id}
                         })
                         .done(function(data) {

                                     if (data.status) {

                                          $(".messages").html("<div class='success-message blink_me'>"+lang['discardsusscesinvoice']+"</div>");
                                          $scope.search();
                                          $('html,body').animate({scrollTop:$('.messages').offset().top}, 'slow');

                                             $(".discard-invoice").hide();
                                          return false;
                                    }
                                    if (!data.messages) {
                                        $(".messages").html('<span class="error-message">'+data+'</span>');
                                     }
                                    else{
                                        $(".messages").html('<span class="error-message">'+data.messages+'</span>');
                                    }

                         });

                         $(this).dialog("close");
                         return;

                 },
                 'キャンセル': function() {
                    // $(".discard-invoice").hide();
                     $(this).dialog("close");
                     }

            }
        });
        return;

    });
    $scope.can_approve = function(row) {
        // #6818: Start Add
         return   row.entity.sales_slip_status == ss_status.inv_approval_pending;
        // #6818: End  add
    // #6818: Start DELETE
//          var loadPage = $("#loadpage").val();
//          if(loadPage == 0){
////                   console.log('asas');
////                   if (!row.entity.sales_slip_status) return false;
////                   if (row.entity.sales_slip_status == ss_status.inv_approval_pending) {
////
////                      row.entity.is_approving = true;
////                      return true;
////                  }
////                   else{
////                        row.entity.is_approving = false;
////                       return false;
////                   }
//         }
//          else{
////                  if (!row.entity.sales_slip_status) return;
////                  if (row.entity.sales_slip_status == ss_status.inv_approval_pending) {
////                      row.entity.is_approving = true;
////                     return true;
////                 }
////                  else{
////                      row.entity.is_approving = false;
////                      return false;
////                  }
//          }
        // #6818: Start DELETE
   }
    $scope.can_change = function(row){

        var loadPage = $("#loadpage").val();
        if(loadPage == 0){
        // #6818-S- Modifly

            if( row.entity.is_approving){
                     row.entity.is_approving = false;
              }
              else{
                     row.entity.is_approving = true;
              }
            $("#loadpage").val(1);
            // #6818-S: Modifly
        }
        var loadPage = $("#loadpage").val();
         if(loadPage == 1){
             if( row.entity.is_approving == true){

                    row.entity.is_approving = false;
                }
             else{
                 row.entity.is_approving = true;
             }
             $("#loadpage").val(2);
         }

    }

    // 6846: Start
    // is settlement
    $scope.settlement = function(row) {
        if (new Date(row.entity.delivery_date).getTime() < new Date(closing_month).getTime()) {
            row.entity.is_approving = false;
            return false;
        }

        return true;
    }
    // 6846: End

     $scope.can_change = function(row){
         var loadPage = $("#loadpage").val();
         if(loadPage == 0){
            // #6818-S- Modifly
             if( row.entity.is_approving){
                     row.entity.is_approving = false;
              }
              else{
                     row.entity.is_approving = true;
              }
             $("#loadpage").val(1);
             // #6818-S: Modifly

         }
         var loadPage = $("#loadpage").val();
          if(loadPage == 1){
              if( row.entity.is_approving == true){

                     row.entity.is_approving = false;
                 }
              else{
                  row.entity.is_approving = true;
              }
              $("#loadpage").val(2);
          }
     }
    $scope.show_client = function(row) {
        if (!row.entity.client_id) return;

//#7461:Start
//        $.ajax({
//            url: '/_admin/client/detail',
//            type: 'POST',
//            data: {id: row.entity.client_id, dialog_selector: '#dialog-show-client'}
//        })
//        .done(function(data) {
//
//            $("#dialog-show-client").html(data);
//            $("#dialog-show-client").dialog({
//                title: lang['text_title_show_client'],    //#6849 :  MOdify -S
//                width: 'auto',
//                height: 'auto',
//                modal: true
//            });
//        });
        $.post(
                '/_admin/client/detail',
                {id : row.entity.client_id, dialog_selector: '#clientDialog'},
                function(data) {
                	clientDialog.html(data);
                	clientDialog.dialog("option", "position", {my: "center", at: "center", of: window});
                	clientDialog.dialog('open');
                });

//#7461:End

    }


    $scope.approve_and_print = function () {

        approve_datas = [];
        flag = false;
        for (i = 0; i < $scope.gridOptions.data.length; i++) {
            if ($scope.gridOptions.data[i].is_approving) {
                approve_datas[i] = $scope.gridOptions.data[i].id;
                $scope.gridOptions.data[i].sales_slip_status = ss_status.inv_issued;
                flag = true;
            }
        }

        if (!flag) {
            $("#dialog-messages").html(label.please_select_approve_print);
            $("#dialog-messages").dialog({
                title: '注意',
                width: 'auto',
                height: 147,
                modal: true,
                dialogClass: "acceptance-dialog",
                buttons: [{
                    text: "OK",
                    click: function() {
                    $(this).dialog("close");
                    }
                }]
            });
            return;
        }
       // console.log( approve_datas.join());
        //return;
        dom = '<form action="/_admin/invoice/approve_and_print" method="POST" target="_blank">' +
                    '<input type="hidden" name="sales_slip_ids" value="' + approve_datas.join() + '"/>' +
                    '<input type="hidden" name="approve_and_print_all" value="1"/>' +
            '</form>';

        $(dom).appendTo('body').submit();
    };

    $scope.view_detail = function(row) {

        //console.log('SSSSS'+row.entity.id);
        $http({
            url : "/_admin/invoice/ajax_detail",
            method : 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data : $.param({sales_slip_id: row.entity.id})
        })
        .success(function(data, status, headers, config) {
            // console.log(data);
            if (typeof data.status != 'undefined') {
                $("#dialog-detail").html(data.messages);
                $("#dialog-detail").dialog({
                    title: '請求書参照',
                    width: 'auto',
                    height: 'auto',
                    modal: true
                });
                return;
            }

            $("#dialog-detail").html(data);
            $scope.sales_slip_detail = data;
            $("#dialog-detail").dialog({
                title: '請求書参照',
                width: 'auto',
                height: 'auto',
                modal: true,
                position: { my: "center", at: "center", of: window }
            });
        })
    }

    // #6818_S_ ADD
    var data_load_frist = $.parseJSON($("#sales_slips_result").text());
//#7087:Start
    if (data_load_frist.hasOwnProperty('is_over_record')) {
    	data_load_frist = [];
    	$scope.showWatermark = false;
    	$scope.is_over_record = true;

    	// return;
    }
    else{
    	  data_load_frist = set_is_approvel(data_load_frist);
    }
//#7087:End

    function set_is_approvel(data){
          for(var  p = 0 ; p < data.length;p++){
                //console.log('p'+p);
                // console.log(data_load_frist[p]);
                 var row_data = data[p];
                 if (row_data.sales_slip_status)  data[p].is_approving = false;
                 if (row_data.sales_slip_status == ss_status.inv_approval_pending) {

                     data[p].is_approving = true;
                }
                 else{
                     data[p].is_approving = false;
                 }
            }
          return data;
    }
 // #6818_D_

    $scope.gridOptions = {
        columnDefs: [
            {
                field: 'approve_all',
                displayName: label.approve_all,
                width: '70', //70
                cellTemplate: '<div class="center-checkbox-wrapper" ng-show="grid.appScope.can_approve(row) && grid.appScope.settlement(row)"><input type="checkbox" checkbox="checkbox"  ng-checked="row.entity.is_approving && grid.appScope.settlement(row)"  ng-click="grid.appScope.can_change(row)" ng-model="row.entity.is_approving"></div>',
                visible: approve_permission,
                enableSorting: false,
                enableColumnResizing: true
            },
            {
                field: 'action',
                displayName: label.action,
                width: '60', //58
                cellTemplate: '<div align="center"><a class="btn-small btn-green btn-view" ng-click="grid.appScope.view_detail(row)">' + label.view + '</a></div>',
                enableSorting: false,
                enableColumnResizing: true
            },
            {
                field: 'sales_slip_status',
                displayName: label.sales_slip_status,
                width: '140', //140
                cellFilter: 'mapSalesSlipStatus:row'
            },
            {
                field: 'sales_slip_number',
                displayName: label.sales_slip_number,
                width: '9%', //100
                cellFilter: 'mapSalesSlipNumber:row',
                cellTemplate: '<a><div class="cell-line-height" ng-click="grid.appScope.view_detail(row)">{{COL_FIELD CUSTOM_FILTERS}}</div></a>'
            },
            {
                field: 'account_name',
                displayName: label.j_account_id,
                width: '10%' //100
            },
            {
                field: 's_code',
                displayName: label.s_code,
                width: '75', //70
                cellTemplate: '<a><div class="cell-line-height" ng-click="grid.appScope.show_client(row)">{{row.entity.s_code}}</div></a>'
            },
            {
                field: 'client_name',
                displayName: label.client_name,
                width: '15%', //120
                cellTemplate: '<a><div class="cell-line-height" ng-click="grid.appScope.show_client(row)">{{row.entity.client_name}}</div></a>'
            },
            {
                field: 'charge_name',
                displayName: label.charge_name,
                width: '100' //120
            },
            {
                field: 'billing_date',
                displayName: label.billing_date,
                cellFilter: 'mapDate:row',
                width: 90 //120
            },
            {
                field: 'payment_date',
                displayName: label.payment_date,
                cellFilter: 'mapDate:row',
                width: 90 //120
            },

            {
                field: 'total_amount',
                displayName: label.cost_total_price,
                width: '8%', //70
                type:'number',
                cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                    if (grid.getCellValue(row, col) < 0) {
                        return 'negative-num a-right error-color';
                    } else {
                        return 'a-right';
                    }
                },
                cellFilter: 'mapAmount:row'
            },
            {
                field: 'total_tax',
                displayName: label.consumption_tax,
                width: '8%', //70
                type:'number',
                cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                    if (grid.getCellValue(row, col) < 0) {
                        return 'negative-num a-right error-color';
                    } else {
                        return 'a-right';
                    }
                },
                cellFilter: 'mapTaxRate:row'
            },
            {
                field: 'total_amount_tax',
                displayName: label.tax_total,
                width: '8%', //70
                type:'number',
                cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                    if (grid.getCellValue(row, col) < 0) {
                        return 'negative-num a-right error-color';
                    } else {
                        return 'a-right';
                    }
                },
                cellFilter: 'mapTotalAmount:row'
            },
//#8123:Start
            {
            	field: 'invoice_approval_account_date',
//            	field: 'lbl_invoice_approval_account_date',
                displayName: label.lbl_invoice_approval_account_date,
                width: 90, //70
//                type:'number',
                cellFilter: 'invoiceApprovalAccountDate:row'

            },
//#8123:End
            {
                field: 'check_date',
                //displayName: label.income_month,
                displayName: label.check_date,
                cellFilter: 'mapDate2:row',
                width: '8%' //70,
            },
        ],
        rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ui-grid-cell ng-class="{\'discarded\': row.entity.sales_slip_status == grid.appScope.ss_status.inv_discarded, \'rejected\': row.entity.sales_slip_status == grid.appScope.ss_status.ss_created}" ></div>',
        data: data_load_frist,       // #6818: Modifly
        onRegisterApi: function(gridApi){
            $scope.gridApi = gridApi;
        }
    };

    $('#slide-left-btn').click(function () {
        $interval(function () {
            $scope.gridApi.core.handleWindowResize();
        },500, 1);
    });
    $scope.ss_status = ss_status;



}]);

app.filter('mapDate', function() {
    return function(input, row) {
        return input.replace(/-/g, '/');
    };
})
app.filter('mapDate2', function() {
    return function(input, row) {

         var ischeckSL =  row.entity.is_checked;
         if(ischeckSL == 1){
             var date =  input.replace(/-/g, '/');
             listdate = date.split("/");
             var lengthdate = listdate.length;
             if(lengthdate >=3){
                 date = listdate[0]+"/"+listdate[1];
             }
             return date;
         }
         else{
         }
    };
})
.filter('mapAmount', function() {
    return function(input, row) {
        return addCommas(input);
    };
})
.filter('mapTaxRate', function() {
    return function(input, row) {
        return addCommas(input);
    };
})
.filter('mapTotalAmount', function() {
    return function(input, row) {
        return addCommas(input);
    };
})
.filter('mapSalesSlipNumber', function() {
    return function(input, row) {
        if (row.entity.is_ad_receive_payment != 0){ return 'AR' + input;}
        else{
            return 'BN'+ input;
        }

    };
})
//#8123:start
app.filter('invoiceApprovalAccountDate', function() {
    return function(input, row) {


	         	 var date =  row.entity.invoice_approval_account_date;
	             listdate = date.split("-");
	             var lengthdate = listdate.length;
	             if(lengthdate >=3){
	                 date = listdate[0]+"/"+listdate[1]+"/"+listdate[2];
	             }
	             console.log(date);
	             if(date != '0000/00/00'){
	            	 return date;
	             }

	    };
})
//#8123:end

.filter('mapSalesSlipStatus', function() {
    return function(input, row) {

        //status = label.status_approval_pending;
        var list = row.entity;
        var status = list['sales_slip_status'];

//#6688:Start
        // auto check status of invoice after search
        if (typeof is_submit_default != 'undefined' && is_submit_default) {
            $('#filter_status_' + status).prop('checked', true);
        }
//#6688:End

        if (input == ss_status.inv_approval_pending) status = label.status_approval_pending;
        if (input == ss_status.inv_approved) status = label.status_approved;
        if (input == ss_status.inv_issued) status = label.status_issued;
        if (input == ss_status.inv_discarded) status = label.status_discarded;

        return status;
    };
});

//#7434:Start
function export_csv(){
	 $("#search-form").attr('action', '/_admin/invoice/export_csv');
     $("#search-form").submit();
}
//#7434:End


