//nav ──────────────────────────────────────
var MAX_SEQUENCE_ALLOW = 65535;

$(document).ready( function(){
    $.cookie.defaults = {
            raw: true,
            expires: 1,
            path: '/',
            domain: document.domain
    };
    if ($.cookie('menu_toggle') !== undefined) {
        cookies = $.cookie();
        if (cookies['menu_toggle'] === '1') {
            $('.slide-left-col > div').css('transition','0s ease');
            $('.slide-left-col > div').addClass('close');

            $('.slide-left-col').css('width', '0');
            $('.content').removeClass('slide-right');
        } else {
            $('.slide-left-col > div').removeClass('close');
            $('.slide-left-col').css('width', '200');
            $('.content').css('transition','0s ease');
            $('.content').addClass('slide-right');
        }
    } else {
        $('.content').addClass('slide-right');
    }

    //左右開閉
    $('#slide-left-btn').on('click', function () {
        $('.content').css('transition','.5s ease');
        // nav is closing, click -> show
        if ($('.slide-left-col > div').hasClass('close')) {
            $('.slide-left-col > div').css('transition','.5s ease');
            $('.slide-left-col').css('transition','.5s ease');
            $.cookie('menu_toggle', 0);
            $('.slide-left-col').css('width', '200');
            // $('.div_wrapper_au_fe').css('width','86%');
            $('.content').addClass('slide-right');
        } else { // nav is openning, click -> hide
            $.cookie('menu_toggle', 1);
            $('.slide-left-col').css('width', '0');
            $('.content').removeClass('slide-right');
        }
        $('.slide-left-col > div').toggleClass('close');

//#6972:Start
        reset_grid_width();
//#6972:End
    });

    //上下開閉
  $('.slide-left-col > div > h3').on('click', function() {
        $(this).toggleClass('on');
        $(this).next().slideToggle();
  });

  set_slide_col_left_scroll_bar();
});
  // Match height of sidebar to content
  sidebar_height_match_content();
  $(window).resize(function() {
      sidebar_height_match_content();
  });

  function sidebar_height_match_content() {
      if ($('.slide-left-col').height() < $('.content').height()) {
          if ($('.content').height() < 300) {
              $('.slide-left-col').css('height', 300);
          } else {
              $('.slide-left-col').css('height', $('.content').css('height'));
          }
      } else {
          $('.content').css('height', $('.slide-left-col').css('height'));
      }
  }

//#6972:Start
function reset_grid_width() {
    var ui_grid = $('.wrapper .content').find('div[ui-grid]').each(function() {
        var grid = $(this);
        var grid_canvas = grid.find('.ui-grid-render-container-body .ui-grid-viewport .ui-grid-canvas');//.removeAttr('style');
        var grid_cell_last = grid_canvas.find('.ui-grid-row:first-child .ui-grid-cell:last-child');
       try{
    	   if(grid_cell_last != ""){
         	  var class_grid_cell_end = grid_cell_last.attr('class').split(' ');
         	  var class_end = null;

               $.each(class_grid_cell_end, function(index, item) {
                   if (item.indexOf("ui-grid-coluiGrid") >= 0) {
                       class_end = item;
                   }
               });

               grid_canvas.removeAttr('style');
               grid_canvas.find('.' + class_end).removeAttr('style');
         }
       }
       catch(err){
    	   grid_canvas.removeAttr('style');
           grid_canvas.find('.' + class_end).removeAttr('style');
       }
      
        
        
       
      
    });
}
//#6972:End

//alert ──────────────────────────────────────
$(document).ready( function(){
    //BUTTON EDIT
    $('.btn-edit').on('click', function () {
        $(this).closest('form').submit();
    });
    //END BUTTON EDIT

    //BUTTON ADD
    $('.btn-add').on('click', function () {
        $(this).closest('form').submit();
    });
    //END BUTTON ADD

    // Init UI elements
    if ($('.input_datepicker').length) {
        $('.input_datepicker').datepicker({
                'dateFormat': 'yy-mm-dd'
        });
    }
    // END Init UI elements

    // UI events through data attribute
    // ## Change dropdown options list upon parent value changed ##
    // Options: data-parent = {parent input name}
    //          data-parent-null-action = {load_all: if parent value is null, load all options
    //                                     else: empty options
    $('[data-parent]').each(function() {
        var parent = $(this).data('parent');
        var child = $(this);
        $('[name=' + parent + ']').change(function() {
            var group_items = child.data('groupItems');
            if ($(this).val() != 0 && $(this).val() != '') {
                load_dropdown_items(child, group_items[$(this).val()], null, {0: lang['all']});
            } else {
                if (child.data('parentNullAction') == 'load_all') {
                    load_dropdown_items(child, ungroup_items(group_items), null, {0: lang['all']});
                } else {
                    load_dropdown_items(child, {}, null, {0: lang['all']});
                }
            }
        });
    })

    // ACTION: change sequence on the fly
    $("input.sequence").on('keyup',function() {
        if (check_sequence_button() == true) {
            $('.btn-update-sequence').removeClass('btn-disable');
            $('.btn-update-sequence').addClass('btn-green');
        } else {
            $('.btn-update-sequence').addClass('btn-disable');
            $('.btn-update-sequence').removeClass('btn-green');
        }
    });

    $('.btn-update-sequence').on('click', function (event) {
        event.preventDefault();
        if($('.btn-update-sequence').hasClass('btn-disable') == true) {
            return false;
        }
        confirm_dialog(lang['message_confirm_sequence_update'], {
            'yes' : function() {
                // Close dialog
                $(this).dialog('close');
                $('#frm_update_sequence').submit();
            }
        });
    });

    // END UI events through class name
});

function redirect_data() {
    var post = '';
    if ($("input[name='redirect_form_post']").length > 0) {
        post = $("input[name='redirect_form_post']").val();
    }
    return post;
}

//Start delete confirm box
function delete_confirm() {
    var is_confirm = false;
    confirm_dialog(lang['message_confirm_delete'], {
        'delete' : function() {
            $(this).dialog('close');
            is_confirm = true;
        }
    });
    return is_confirm;
}
//End delete confirm box

function confirm_dialog(msg, buttons, confirm_yes, form_width) {
    buttons = buttons || {};
    var local_buttons = {};
    $.each(buttons, function(k, v) {
        local_buttons[lang['button_' + k]] = v;
    });

    var dialog = $("<div class='confirm-dialog ' title='"+ lang['title_confirm'] +"'>" + msg + "</div>");
    if (!(lang['button_close'] in local_buttons)) {
        if (confirm_yes == true) {
            local_buttons[lang['button_no']] = function() {
                dialog.dialog("close");
            };
        } else {
            local_buttons[lang['button_cancel']] = function() {
                dialog.dialog("close");
            };
        }
    }
    dialog.dialog({
        modal: true,
        resizable : false,
        buttons : local_buttons,
        dialogClass: "order-dialog",
    });

    if (form_width) {
        dialog.dialog({
            width : form_width
        });
    }
}

// Check onchange on form, show confirm message
var form_submitted = form_changed = false;

function exit_confirm() {
    var input_change = function() {
        if (!form_changed) {
            form_changed = true;
        }
    };
//#8437:Start
    // $('input, textarea').keyup(input_change);
    // $('input:checkbox, input:radio, select').change(input_change);
    // $('#dialogAdd .btn-add').click(input_change);

    $('body').on('keyup', 'input, textarea', input_change);
    $('body').on('change', 'input:checkbox, input:radio, select', input_change);
    $('body').on('click', '#dialogAdd .btn-add', input_change);
//#8437:End
    // have navigation away
    $(window).bind('beforeunload', function(){
        // Trigger when form_changed return true then show confirm navigation, else dont show
        if (form_changed && !form_submitted)
            return 'このページから移動しますか？ 入力したデータは保存されません。';
    });
}

function close_confirm() {
    // check dialog confirm if form have change
    if (form_changed) {
        r = confirm('このページから移動しますか？ 入力したデータは保存されません。');
        if (r) {
            form_changed = false; // if confirm yes, dont want to run beforeunload
        }
        return r; // if cancel return false (keep dialog), else return true (close dialog)
    }
    return true;
}

function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }

function load_dropdown_items(dropdown, items, selected, first_item) {
    var options = '';
    if (first_item !== null && typeof first_item === 'object') {
        options += '<option value="' + Object.keys(first_item)[0] + '">' + first_item[Object.keys(first_item)[0]] + '</option>';
    }
    if (items !== null && typeof items === 'object') {
        $.each(items, function(k, v) {
            if ((selected != undefined) && (selected == k)) {
                options += '<option value="' + k + '" selected>' + escapeHtml(v) + '</option>';
            } else {
                options += '<option value="' + k + '">' + escapeHtml(v) + '</option>';
            }
        });
    }
    dropdown.html(options);
}

function ungroup_items(obj) {
    var new_obj = {};
    $.each(obj, function(k, v) {
        $.extend(new_obj, v);
    });
    return new_obj;
}

function check_sequence_button() {
    $status_error = true;
    $status_def = false;
    var seq_form = $('form[name=frm_update_sequence]');

    $("input[name^='sequence']").each(function(index){

        var classes = $(this).attr('class').split(' ');
        var val = $(this).val();
        var seq_changed_name = $(this).attr('name');
        seq_changed_name = 'post_' + seq_changed_name;
        $(this).removeClass('error')
        if ((/^[0-9]+$/.test(val) == false)) {
            $(this).addClass('error');
            if ($status_error == true) {
                $status_error = false;
            }
        } else if (classes[2] != val){
            if ((parseInt(val) == 0) && (val.length > 1)) {
                $status_error = false;
                $(this).addClass('error');
            } else if ($status_def == false){
                $status_def = true;
            }
            var seq_exist = seq_form.find('input[name="' + seq_changed_name + '"]');

            if (seq_exist.length == 0) {
                seq_form.prepend('<input type="hidden" name="' + seq_changed_name + '" value="' + val + '">');
            } else {
                seq_exist.val(val);
            }
        } else if (classes[2] == val) {
            var seq_exist = seq_form.find('input[name="' + seq_changed_name + '"]');
            if (seq_exist.length != 0) {
                seq_exist.val(val);
            }
        }

        if ((/^[0-9]+$/.test(val)) && (parseInt(val, 10) > MAX_SEQUENCE_ALLOW)) {
            $(this).addClass('error');
            if ($status_error == true) {
                $status_error = false;
            }
        }
    });
    if (($status_error == true)) {
        return true;
    }
    return false;
}

// add commas to string number
function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
//#7090:Start
function formatWeight(str)
{
    var pos = str.indexOf('.');
    if (!str.length) {
        return 0;
    } else if (pos != -1) {
        return str.substr(0, pos + 2);
    } else {
        return str + '.' + '0';
    }
}
//#7090:End

// RESET ALL INPUT DATA
function reset_form_data(selector) {
    object = $(selector).find('input, textarea, select');
    object.each(function(index, element) {
        switch ($(this).prop('tagName')) {
            case 'SELECT':
                $(this).children().removeAttr('selected');
                break;
            case 'INPUT':
                if ($(this).attr('type') == 'checkbox') {
                    $(this).removeAttr('checked');
                } else {
                    $(this).val('');
                }
                break;
        }
    });
}

//set vertical scrollbar for slide-col-left
function set_slide_col_left_scroll_bar() {

    //get window height
    $windown_height = $(window).height();

    //get header height
    $header_height = $('.header').height();

    //get footer height
    $footer_height = $('.footer').height();

    //get current height
    $current_height = Math.floor($windown_height - $header_height - $footer_height);
    $('.slide-left-col >div').css({'height' : $current_height, 'overflow' : 'auto'});
}


$(window).resize(function() {
    set_slide_col_left_scroll_bar();
});

function parse_date(str) {
    str = str.trim();
    date = year = month = day = '';
    if (!str) return false;

    delimiter = '-';
    if (str.indexOf('/') != -1) delimiter = '/';

    parts = str.split(delimiter);

    if (parts.length == 2) {
        date_obj = new Date();

        if (parts[0].trim().length == 4) {
            year = parts[0].trim();
            month = parts[1].trim();
            day = date_obj.getDate().toString();
            date = year + delimiter + month + delimiter + day;
        } else {
            year = date_obj.getFullYear();
            month = parts[0].trim();
            day = parts[1].trim();
            date = year + delimiter + month + delimiter + day;
        }


    } else if (parts.length == 3) {
        year = parts[0].trim();
        if (year.length < 4) {
            year = '2';
            for (i = 0; i < 3 - parts[0].trim().length; i++) {
                year += '0';
            }
            year += parts[0].trim();
        }
        month = parts[1].trim();
        day = parts[2].trim();
        date = year + delimiter + month + delimiter + day;
    }

    date_obj = new Date(date);
    if (date_obj.getFullYear() == year && date_obj.getMonth() + 1 == month && date_obj.getDate() == day) {
        return year + '/' + month.replace(/^\d$/,'0$&') + '/' + day.replace(/^\d$/,'0$&');
    }
    return false;
}

function cloneObj(obj, deepClone) {
    if (deepClone === true) {
        return $.extend(true, {}, obj);
    } else {
        return $.extend({}, obj);
    }
}

// Move an element of array from old_index to new_index
// This will directly affect array in the arguments
function array_move(array, old_index, new_index) {
    while (old_index < 0) {
        old_index += array.length;
    }
    while (new_index < 0) {
        new_index += array.length;
    }
    if (new_index >= array.length) {
        var k = new_index - array.length;
        while ((k--) + 1) {
            array.push(undefined);
        }
    }
    array.splice(new_index, 0, array.splice(old_index, 1)[0]);
}

// Round float number to nearest integer in some special case
// @see http://floating-point-gui.de for more detail
function strip(number) {
    //return (parseFloat(number.toPrecision(12)));
    return number;
}

function f_ceil(number) {
    return Math.ceil(strip(number));
}

function f_floor(number) {
    return Math.floor(strip(number));
}

// htmlspecialchars string
function htmlspecialchars(string) {
    return $('<div/>').text(string).html();
}




//* #6946:Start

function formatMoneyAll(valueinput,c, d, t){
	if((valueinput != "")  && (valueinput!= null)){
	var n = valueinput,
	    c = isNaN(c = Math.abs(c)) ? 2 : c,
	    d = d == undefined ? "." : d,
	    t = t == undefined ? "," : t,
	    s = n < 0 ? "-" : "",
	    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;

	   var valreturn =  s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	  
	   if(c != 0){
			   try {
				   var n = valueinput.indexOf("."); // Check  input have decimal 
				}
				catch(err) {
				
				   if (!isNaN(valueinput) && valueinput.toString().indexOf('.') != -1){
						var n = 1;
					}
					else{
						var n = 0;
					}
				   
				}
			   if(n > 0){    // True
				   return valreturn;
			   }
			   else{ // False
				   var end_cut = parseInt(c)+1;
				   valreturn = valreturn.slice(0,-end_cut);
				   return valreturn;
			   }
	   }
	   else{
		   return valreturn;
	   }
	   
	   
	}
	else{
		return valueinput;
	}
};
//* #6946:End

//#6869:Start
(function( $ ) {

    // This function will make the binded input field to display number with thousand seperator
    // and remove the seperator when it has focus
    // Usage: $('[name=credit_amount]').inputNumberFormat();
    // Warning: The seperator will also be send back to server on submit,
    // so you will need to implement some kind of filter manually.
    $.fn.inputNumberFormat = function() {
        this.filter( "input[type=text]" ).each(function() {
            var input = $( this );
            input.focus(function () {
                var content = $(this).val();
                content = content.replace(/,/g, '');
                $(this).val(content);
            });
            input.blur(function () {
                var content = $(this).val();
                content = parseFloat(content);
                if (!isNaN(content)) {
                    $(this).val(content.toLocaleString());
                }
            });
        });

        return this;
    };

}( jQuery ));
//#6869:End

//#7589:Start
var get_viewport = function() {
    var viewport_width;
    var viewport_height;

    if (typeof window.innerWidth != 'undefined') {
        viewport_width  = window.innerWidth,
        viewport_height = window.innerHeight
    }

    // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
    else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) {
        viewport_width = document.documentElement.clientWidth,
        viewport_height = document.documentElement.clientHeight
    }

    // older versions of IE
    else {
        viewport_width = document.getElementsByTagName('body')[0].clientWidth,
        viewport_height = document.getElementsByTagName('body')[0].clientHeight
    }
     return [viewport_width, viewport_height];
}

var get_handsontable_viewport_height = function() {
    viewport    = get_viewport();
    width       = viewport[0];
    height      = viewport[1];
    grid_height = 450;

    if (width >= 1008 && height >= 655) grid_height = 440;
    if (width >= 1136 && height >= 751) grid_height = 550;
    if (width >= 1264 && height >= 600) grid_height = 370;
    if (width >= 1264 && height >= 655) grid_height = 470;
    if (width >= 1264 && height >= 687) grid_height = 500;
    if (width >= 1264 && height >= 847) grid_height = 650;
    if (width >= 1264 && height >= 911) grid_height = 720;
    if (width >= 1344 && height >= 655) grid_height = 470;
    if (width >= 1366 && height >= 643) grid_height = 450;
    if (width >= 1384 && height >= 787) grid_height = 600;
    if (width >= 1384 && height >= 937) grid_height = 720;
    if (width >= 1584 && height >= 787) grid_height = 600;
    if (width >= 1664 && height >= 937) grid_height = 720;
    if (width >= 1920 && height >= 955) grid_height = 750;

    return grid_height;
}
//#7589:End

//#9183 :Start
function formatMoneyAllRevert(valueinput)
{
      try {
          valueinput = valueinput.split(",");
         	$n_vl = valueinput.length;
            va_return = "";
            for(var $i = 0 ; $i <$n_vl; $i++ ){
                va_return +=  valueinput[$i];
            }
       }
       catch(err) {
           va_return = valueinput;
       }

    return parseInt(va_return);
}


//#9183 :End

BigNumber.config({ERRORS: false});

//#9696:Start
/**
 * truncate a decimal number to the specify degit
 * @param  mixed number
 * @param  int digits
 * @return mixed
 *
 * number = 2.789, degits = 2 => 2.78
 */
truncateDecimal = function(number, digits) {
    var re = new RegExp("(\\d+\\.\\d{" + digits + "})(\\d)"),
        m = number.toString().match(re);
    return m ? parseFloat(m[1]) : number;
};
//#9696:End