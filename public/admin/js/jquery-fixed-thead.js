/**
 * Jquery fixed thead of table
 * @license FixedThead v1.0.0
 * @author Huynh Cong Tien
 */
(function ($) {

    $.fn.fixed_thead = function(options) {
        this.each(function(){
            // This is the easiest way to have default options.
            var settings = $.extend({
                // These are the defaults.
                maxHeight: 500,
                scrollTop: true
            }, options );
        
            var table_origin = $(this);
            
            if (table_origin.attr('id') == undefined) {
                table_origin.attr('id', 'table_fix_thead');
            }

            // clone table
            var table_clone = table_origin.clone();
        
            // remove tbody, tfoot
            table_clone.find('tbody, tfoot').remove();
        
            table_clone.removeAttr('id').addClass('fix-header');

            // remove thead of table origin
            table_origin.find('thead').remove();

            // create wrap table origin
            table_origin.wrap('<div class="wrap-table-origin"></div>');
            
            var wrap_table_origin = $('.wrap-table-origin');
            
            // scroll top if it is scroll bottom
            if (settings.scrollTop) {
                wrap_table_origin.animate({
                    scrollTop: -wrap_table_origin[0].scrollHeight
                }, 'fast');
            }
            
            wrap_table_origin.css({
                'max-height': settings.maxHeight + 'px',
                'overflow-y': 'scroll'
            });
            
            // add table clone to before wrap table origin
            wrap_table_origin.before(table_clone);

            table_clone.wrap('<div class="wrap-fix-header"></div>');
            
            // get width of thead table clone
            var th_width = [];
            $('table.fix-header thead tr th').each(function() {
                if ($(this).width() != 0) {
                    th_width.push($(this).width());
                }
            });

            var table_id = table_origin.attr('id');
            
            // set width of td table origin
            $('#' + table_id + ' tr').find('td').each(function(index) {
                if (typeof(th_width[index]) != "undefined" && th_width[index] !== null) {
                    $(this).width(th_width[index]);
                }
            });
      
            // create wrap table clone
            var wrap_table_clone = $('.wrap-fix-header');
            
            // scroll wrap table origin and synchrony wrap table clone
            wrap_table_origin.scroll(function() {
                var scrollLeft = $(this).scrollLeft();

                wrap_table_clone.css({
                    'margin-left': -scrollLeft + 'px'
                });
            });
        });
    }
    
}(jQuery));