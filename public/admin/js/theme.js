$(document).ready( function(){
    // Set Theme Color
    var themecolor = '#4493D0';



    ////////////////////////////////////////////
    //Background Color
    var bg = 'body, .dropnav, .dropnav > div > ul > li > ul, a.btn-theme, .pager > li.active, .pager > li > button.active';

    //Text Color
    var txt = '.login-wrapper .login-form input[type="submit"], .txt-theme';
    var txton = 'a.btn-theme';

    //Border Color
    var bc = '.content > div > h1, .content > div > h2, a.btn-theme, .pager > li.active, .pager > li > button.active';

    $(txt).css('color', themecolor);

    $(txton).hover(
        function() {
        $(this).css("color", themecolor);
    },
        function() {
        $(this).css("color", '#fff');
    }
    );

    $(bc).css('border-color', themecolor);
    ////////////////////////////////////////////

});