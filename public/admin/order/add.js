
var convertDate = function (inputArray) {
    if (inputArray.length == 3) {
        if (inputArray[0].length == 2) {
            inputArray[0] = '20' + inputArray[0];
        }
        return inputArray.join('/');
    }
    else if (inputArray.length == 2) {
        if(inputArray[0].length == 4)
            return inputArray.join('/') + '/01';
        return new Date().getFullYear() + '/' + inputArray.join('/');
    }
};

app.controller('AddCtrl', ['$scope', '$rootScope', '$http', '$q', '$interval', 'uiGridConstants', function ($scope, $rootScope, $http, $q, $interval, uiGridConstants) {
    //#6972:Start
    $rootScope.lang = 'ja';
    //#6972:End

    $(document).ready(function() {
        function get_tax_rate() {
            $http({
                url : '/_admin/consumption_tax/get_tax_rate',
                method : 'POST',
                data : {
                    tax_type : 1
                }
            }).success(function(data) {
                $rootScope.tax_rates = data;
            });
        }

        /* start volyminhnhan@gmail.com modification*/
        if (typeof $rootScope.tax_rates == 'undefined') {
            get_tax_rate();
        }
        /* end volyminhnhan@gmail.com modification*/

        // add order in client view
        $('#client-table').on('click', '.add-order', function() {
            var client_id = $(this).parents('tr').data('id');

            if (typeof $rootScope.tax_rates == 'undefined') {
                get_tax_rate();
            }

            $rootScope.showAddByClient(client_id);
        });

    });

    if (typeof $rootScope.search !== 'function') {

        $rootScope.search = function () {
            form_changed = false;
            form_submitted = true;
        }
    }
    $scope.gridOptionsAdd = {
        selectionRowHeaderWidth: 35,
        rowEditWaitInterval: 1,
        rowTemplate:
            '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ui-grid-cell ng-class="{\'green-row\': (row.invalid)}" ></div>'
    };
    $scope.gridOptionsAdd.enableCellEditOnFocus = true;
    $scope.gridOptionsAdd.multiSelect = true;
    // Check if can be saved
    $scope.can_be_save = true;
    $scope.message = "";
    $scope.is_submitted = false;
    $scope.is_credit_over = false;

    $scope.change_color = function(row) {
        if ($scope.client.is_ad_receive_payment == 1) {
            return {
                "background-color": '#FFCC99'
            }
        } else {
            return {
                "background-color": '#fff'
            }
        }
    }

    $scope.gridOptionsAdd.columnDefs = [
                                     { name: 'action', displayName: jsGlobals[27], pinnedLeft:true, width: 160, enableCellEdit:false,
                                         cellTemplate:'<button type="button" ng-class="grid.appScope.is_credit_over ? \'btn-gray\' : \'btn-green\'" ng-disabled="grid.appScope.is_credit_over" class="btn primary btn-small btn-green btn-copy txt-center" ng-click="grid.appScope.copyRow(row.entity, row)">'+jsGlobals[29]+'</button>'
                                             + '<button type="button" class="btn primary btn-small btn-green btn-delete txt-center" ng-click="grid.appScope.deleteRow(row.entity)">'+jsGlobals[30]+'</button>'},
                                     { name: 'branch', displayName: jsGlobals[0], width: 40, enableCellEdit:false},
                                     {
                                         name: 'product_name', displayName: jsGlobals[3], width: 100,
                                         editableCellTemplate : 'ui-grid/dropdownEditor',
                                         cellFilter : 'mapProduct',
                                         editDropdownValueLabel : 'name',
                                         editDropdownOptionsArray : products
                                         //cellClass: 'pink-row'
                                     },
                                     { name: 'contents', displayName: jsGlobals[4], width: 100, editableCellTemplate : '<form name="inputForm"><input type="text" maxlength="30" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'contents\']" ui-grid-editor=""></form>'
                                         //cellClass: 'pink-row'
                                     },
                                     { name: 'delivery_date', displayName: jsGlobals[5], width: 100, cellFilter: 'date: "yyyy/MM/dd"',
                                         editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'delivery_date\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                     },//, cellClass: 'pink-row'},
                                     { name: 'billing_date', displayName: jsGlobals[6], width: 100, cellFilter: 'date: "yyyy/MM/dd"', cellEditableCondition: function(){ if($scope.client.is_ad_receive_payment == "1") return true; return false;},//,
                                         //cellTemplate: '<div class="cell-template-data" ng-style="grid.appScope.change_color(row)">{{row.entity.billing_date}}</div>'
                                         editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'billing_date\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                     },
                                     { name: 'payment_date', displayName: jsGlobals[7], width: 100, cellFilter: 'date: "yyyy/MM/dd"', cellEditableCondition: function(){ if($scope.client.is_ad_receive_payment == "1") return true; return false;},//,
                                         //cellTemplate: '<div class="cell-template-data" ng-style="grid.appScope.change_color(row)">{{row.entity.payment_date}}</div>'
                                         editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'payment_date\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                     },
                                     { name: 'quantity', displayName: jsGlobals[8], width: 75, cellFilter: 'number:0', cellClass: 'a-right',
                                         editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'quantity\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                     },//, cellClass: 'pink-row'},
                                     { name: 'tax_type', displayName: jsGlobals[9], width: 40, editableCellTemplate: 'ui-grid/dropdownEditor',//, cellClass: 'pink-row',
                                         cellFilter: 'mapTaxType', editDropdownIdLabel: 'code', editDropdownValueLabel: 'name', editDropdownOptionsArray: tax_division },
                                     { name: 'unit_price', displayName: jsGlobals[10], width: 100, cellFilter: 'number:2', cellClass: 'a-right',
                                         editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'unit_price\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                     },//, cellClass: 'pink-row'},
                                     { name: 'amount()', displayName: jsGlobals[11], width: 71, enableCellEdit:false, cellFilter: 'number:0'},
                                     { name: 'cost_unit_price', displayName: jsGlobals[12], width: 109, cellFilter: 'number:2', cellClass: 'a-right',
                                         editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'cost_unit_price\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                     },//, cellClass: 'pink-row'},
                                     { name: 'cost_total_price', displayName: jsGlobals[13], width: 128, cellFilter: 'number:0',cellClass: 'a-right', cellEditableCondition: function($scope){ if($scope.row.entity.cost_unit_price) return false; return true;},
                                         editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'cost_total_price\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                     },//,
                                         //cellTemplate: '<div class="cell-template-data" ng-style="row.entity.cost_unit_price == 0 && {\'background-color\': \'#FFCC99\'} || row.entity.cost_unit_price != 0 && {\'background-color\': \'#fff\'}">{{row.entity.cost_total_price}}</div>'
//#7390: Start
                                     { name: 'gross_profit_amount()', displayName: jsGlobals[14], width: 131, cellFilter: 'number:0',
                                         cellTemplate: '<div class="cell-template-data a-right" ng-class="{red: row.entity.gross_profit_amount_value < 0}">{{row.entity.gross_profit_amount()}}</div>'
                                         , enableCellEdit: false},
                                     { name: 'gross_profit_rate()', displayName: jsGlobals[15], width: 117,
                                         cellTemplate: '<div class="cell-template-data a-right" ng-class="{red: row.entity.gross_profit_rate_value < 0}">{{row.entity.gross_profit_rate()}}</div>'
                                         ,enableCellEdit: false},
                                   ];
//#7390: End
    // <!------------------------------- INIT ADD FUNCTION ----------------------------------------------------------------->
    $rootScope.showAdd = function(order_id_input) {
//7941:Start
        if (!$('.success-message').is(':empty')) {
            $('.success-message').remove();
        }
//7941:End
        $scope.show_search = true;
        $scope.errors = '';
        $scope.error_success = '';
        $scope.is_submitted = false;
        $scope.is_credit_over = false;
        $scope.client = {};
        $scope.client.id = clients[0].id;
//#7070: Start
        if (typeof $scope.operators[0] !== 'undefined') {
            $scope.client.id = $scope.operators[0].id;
        }
//#7070: End
        $scope.client.j_account = my_account.id;
        $scope.search_s_code = "";
        $scope.search_client_name = "";
        $scope.gridOptionsAdd.data = [];

        $('#dialogAdd').dialog('option', 'title', "申込書作成");
        $("#dialogAdd").dialog( "option", "height", "auto");
        $("#dialogAdd").dialog( "option", "width", "80%" );
        //setTimeout(function(){
            $('#dialogAdd').dialog("open");
        //}, 1);


        var dialog_height = $('#dialogAdd').parents('.ui-dialog').height();
        var window_height = $(window).height();

        if (dialog_height > window_height) {
            $('#dialogAdd').dialog("option", "position", {my: "top", at: "top", of: window});
        } else {
            $('#dialogAdd').dialog("option", "position", {my: "center", at: "center", of: window});
        }

        $('#dialogAdd .grid').width($('#dialogAdd').width());
        $(window).trigger('resize');
        if (order_id_input) {
            $scope.show_search = false;
            $http({
                url : '/_admin/order/get_detail',
                method : 'POST',
                data : {
                    order_id : order_id_input,
                    form: 'edit'
                }
            }).success(function(data) {
                $scope.client.id = data[0].c_id;
                $scope.client.s_code = data[0].c_s_code;
                $scope.client.name = data[0].c_name;
                $scope.client.name_kana = data[0].c_name_kana;
                $scope.client.zipcode = data[0].o_zipcode;
                $scope.client.address_1st = data[0].o_address_1st;
                $scope.client.address_2nd = data[0].o_address_2nd;
                $scope.client.phone_no = data[0].o_phone_no;
                $scope.client.fax_no = data[0].o_fax_no;
                $scope.client.division_name = data[0].o_division_name;
                $scope.client.charge_name = data[0].o_charge_name;
                $scope.client.c_is_ad_receive_payment = data[0].c_is_ad_receive_payment;
                $scope.client.closing_date = data[0].c_closing_date;
                $scope.client.payment_month_cd = data[0].c_payment_month_cd;
                $scope.client.payment_day_cd = data[0].c_payment_day_cd;
                $scope.client.is_ad_receive_payment_init = data[0].o_is_ad_receive_payment;
                //$scope.client.j_account = data[0].o_account_id; //Comment out as per the Change_spec No. 5852
                $scope.client.cost_total_price = data[0].o_cost_total_price;
                $scope.client.consumption_tax = data[0].o_consumption_tax;
                $scope.client.tax_total = data[0].o_tax_total;
                $scope.client.gross_profit_total = data[0].o_gross_profit_total;
                $scope.client.remark = data[0].o_remark;
                $scope.client.credit_level = data[0].c_credit_level;
                $scope.client.credit_amount_show = addCommas(data[0].c_credit_amount);
                $scope.client.is_ad_receive_payment = parseInt(data[0].o_is_ad_receive_payment);

                if(data[0].closing_date_name!=null) {
                    $("#closing_date_name").text(data[0].closing_date_name+"日");
                } else {
                    $("#closing_date_name").text("日");
                }
                if(data[0].payment_day_cd_name!=null) {
                    $("#payment_day_cd_name").text(data[0].payment_day_cd_name+"日");
                } else {
                    $("#payment_day_cd_name").text("日");
                }
                if(data[0].payment_month_cd_name!=null) {
                    $("#payment_month_cd_name").text(data[0].payment_month_cd_name+"月");
                } else {
                    $("#payment_month_cd_name").text("月");
                }

                if($scope.client.c_is_ad_receive_payment == 1) {
                    $("#message_alert").html("<span class='validation-error'>前受金のお客様です。</span>").dialog({
                        title: "注意",
                        width: 'auto',
                        height: 'auto',
                        modal : true,
                        buttons: {
                            "OK": function() {
                                $(this).dialog('close');
                            }
                        },
                        dialogClass: "order-dialog",
                        position: {my: "center", at: "center", of: window}
                    });
                    $scope.client.is_ad_receive_payment = 1;
                } else if (($scope.client.credit_level == 3) && ($scope.client.is_ad_receive_payment == 0)) {
                    $scope.message = '';
//#7764:Start
//                    $scope.message += "<span class='validation-error'>こちらのお客様は与信が必要です。</span></br>"
                    $scope.message += "<span class='validation-error'>このクライアントは申込書を作成する都度与信確認が必要です。</span></br>"
//#7764:End
                    if ($scope.client.credit_amount_show == 0) {
                        $scope.message += "<span class='validation-error'>与信限度額を超えています。</span><br/>";
                    }

                    if ($scope.message != '') {
                        $("#message_alert").html($scope.message).dialog({
                            title: "注意",
                            width: 'auto',
                            height: 'auto',
                            modal: true,
                            buttons: {
                                "OK": function() {
                                    $(this).dialog('close');
                                }
                            },
                            dialogClass: 'order-dialog'
                        });
                    }
                }

                $scope.client.is_ad_receive_payment_init = $scope.client.c_is_ad_receive_payment;
                angular.forEach(data, function(value, key) {
                    var item = {};
                    item.branch = value.od_branch_cd;
                    item.product_name = value.p_id;
                    item.contents = value.od_contents;
                    item.delivery_date = value.od_delivery_date;
                    item.billing_date = value.od_billing_date;
                    item.payment_date = value.od_payment_date;
                    item.quantity = value.od_quantity;
                    item.tax_type = value.od_tax_type;
                    item.unit_price = value.od_unit_price;
                    item.cost_unit_price = value.od_cost_unit_price;
                    item.cost_total_price = value.od_cost_total_price;

                    item.amount = function () {
                        //this.amount_value = f_floor(this.quantity * this.unit_price);
                        //result = f_floor(this.quantity * this.unit_price);
                        this.amount_value = (new BigNumber(this.unit_price)).mul(this.quantity).floor().toNumber();
                        result = this.amount_value;
                        result = result || 0;
                        return result;
                    };
                    item.gross_profit_amount = function () {
                        var gross_profit_amount;
                        if (this.tax_type == 2) {
                            var tax_rate;
                            var delivery_date = new Date(this.delivery_date);
                            var arr = jQuery.grep($rootScope.tax_rates, function( i ) {
                                var da = new Date(i.adaptation_date);
                                delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                                da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                                return da <= delivery_date;
                            });
                            var tax_rates = [];
                            $.each(arr, function(key, value){
                                tax_rates.push(value.rate);
                            });
                            tax_rate = Math.max.apply(Math, tax_rates);
                            //gross_profit_amount = f_ceil(this.amount()/(1 + (tax_rate/100))) - this.cost_total_price;
                            gross_profit_amount = (new BigNumber(this.amount())).div((new BigNumber(tax_rate)).div(100).plus(1)).ceil().toNumber() - this.cost_total_price;
                        }
                        else {
                            gross_profit_amount = this.amount() - this.cost_total_price;
                        }

                        this.gross_profit_amount_value = gross_profit_amount;
                        return this.gross_profit_amount_value;
                    };
                    item.gross_profit_rate = function () {
                        if (this.amount() <= 0) {
                            return '0.00 %';
                        }
                        var gross_profit_rate;
                        if (this.tax_type == 2) {
                            var tax_rate;
                            var delivery_date = new Date(this.delivery_date);
                            var arr = jQuery.grep($rootScope.tax_rates, function( i ) {
                                var da = new Date(i.adaptation_date);
                                delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                                da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                                return da <= delivery_date;
                            });
                            var tax_rates = [];
                            $.each(arr, function(key, value){
                                tax_rates.push(value.rate);
                            });
                            tax_rate = Math.max.apply(Math, tax_rates);
                            // gross_profit_rate = Math.ceil(this.gross_profit_amount() / (this.amount()/(1 + tax_rate/100)) * 100);
                            //tmp_amount = f_ceil(this.amount() / (1 + (tax_rate / 100)));
                            var tmp_amount = (new BigNumber(this.amount())).div((new BigNumber(tax_rate)).div(100).plus(1)).ceil().toNumber();
                            //gross_profit_rate = f_floor((this.gross_profit_amount() / tmp_amount) * 100 * 100) / 100;
                            gross_profit_rate = (new BigNumber(this.gross_profit_amount())).div(tmp_amount).mul(100).round(2, BigNumber.ROUND_FLOOR).toNumber();
                        }
                        else {
                            //gross_profit_rate = f_floor(this.gross_profit_amount() / this.amount() * 100 * 100) / 100;
                            gross_profit_rate = (new BigNumber(this.gross_profit_amount())).div(this.amount()).mul(100).round(2, BigNumber.ROUND_FLOOR).toNumber();
                        }

                        this.gross_profit_rate_value = gross_profit_rate;
                        return this.gross_profit_rate_value.toFixed(2) + ' %';
                    };

                    $scope.gridOptionsAdd.data.push(item);
                })
                $scope.sum();
                $scope.compute_credit();
            });
        } // End if ($order_id_input) {
    } // End $rootScope.showAdd = function($order_id_input) {

    $rootScope.showAddByClient = function (client_id) {
        $scope.show_search = false;
        $scope.errors = '';
        $scope.error_success = '';
        $scope.is_submitted = false;
        $scope.is_credit_over = false;
        $scope.client = {};
        //$scope.client.id = clients[0].id;
        $scope.client.j_account = j_accounts[0].id;
        $scope.search_s_code = "";
        $scope.search_client_name = "";
        $scope.gridOptionsAdd.data = [];

        $('#dialogAdd').dialog('option', 'title', "申込書作成");
        $("#dialogAdd").dialog( "option", "height", "auto");
        $("#dialogAdd").dialog( "option", "width", "80%" );
        $('#dialogAdd').dialog("open");
        $('#dialogAdd .grid').width($('#dialogAdd').width());
        $(window).trigger('resize');
        $scope.client.id = client_id;
        $scope.clientChange();
    }

    $scope.back = function() {
        $('#dialogAdd').dialog("close");
    }

    // <!----------------------------------------------ADD NEW ROW------------------------------------->
    $scope.addData = function() {
        var gridData = $scope.gridOptionsAdd.data;
//        console.log(gridData.length);

        if(gridData.length>23) {
            return false;
        }
        gridData.push({
                    "branch" : pad(gridData.length + 1, 2),
                    "quantity": 0,
                    "tax_type": 1,
                    "unit_price": 0,
                    "cost_unit_price": 0,
                    "delivery_date": "",
                    "cost_total_price": 0
                });

        var row = gridData[gridData.length - 1];
        row.amount = function () {
            //result = f_floor(this.quantity * this.unit_price);
            result = (new BigNumber(this.unit_price)).mul(this.quantity).floor().toNumber();
            result = result || 0;
            this.amount_value = result;
            return result;
        };
        row.gross_profit_amount = function () {
            var gross_profit_amount;
            if (this.tax_type == 2) {
                var tax_rate;
                var delivery_date = new Date(this.delivery_date);
                var arr = jQuery.grep($rootScope.tax_rates, function( i ) {
                    var da = new Date(i.adaptation_date);
                    delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                    da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                    return da <= delivery_date;
                });
                var tax_rates = [];
                $.each(arr, function(key, value){
                    tax_rates.push(value.rate);
                });
                tax_rate = Math.max.apply(Math, tax_rates);

                //gross_profit_amount = f_ceil(this.amount()/(1 + (tax_rate/100))) - this.cost_total_price;
                gross_profit_amount = (new BigNumber(this.amount())).div((new BigNumber(tax_rate)).div(100).plus(1)).ceil().toNumber() - this.cost_total_price;
            }
            else {
                gross_profit_amount = this.amount() - this.cost_total_price;
            }

            this.gross_profit_amount_value =  gross_profit_amount;
//#9183 :Start
//            return this.gross_profit_amount_value;
              return formatMoneyAll(this.gross_profit_amount_value);
 //#9183 :End
        };
        row.gross_profit_rate = function () {
            if (this.amount() <= 0) {
                return '0.00 %';
            }
            var gross_profit_rate;
            if (this.tax_type == 2) {
                var tax_rate;
                var delivery_date = new Date(this.delivery_date);
                var arr = jQuery.grep($rootScope.tax_rates, function( i ) {
                    var da = new Date(i.adaptation_date);
                    delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                    da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                    return da <= delivery_date;
                });
                var tax_rates = [];
                $.each(arr, function(key, value){
                    tax_rates.push(value.rate);
                });

                tax_rate = Math.max.apply(Math, tax_rates);
                //tmp_amount = f_ceil(this.amount() / (1 + (tax_rate / 100)));
                var tmp_amount = (new BigNumber(this.amount())).div((new BigNumber(tax_rate)).div(100).plus(1)).ceil().toNumber();
                //gross_profit_rate = f_floor((this.gross_profit_amount() / tmp_amount) * 100 * 100) / 100;
//#9183 :Start
                var gross_profit_amount = this.gross_profit_amount();
                gross_profit_amount = formatMoneyAllRevert(gross_profit_amount);
//				gross_profit_rate = (new BigNumber(this.gross_profit_amount())).div(tmp_amount).mul(100).round(2, BigNumber.ROUND_FLOOR).toNumber();
                gross_profit_rate = (new BigNumber(gross_profit_amount)).div(tmp_amount).mul(100).round(2, BigNumber.ROUND_FLOOR).toNumber();

//#9183 :End
            }
            else {
                //gross_profit_rate = f_floor(this.gross_profit_amount() / this.amount() * 100 * 100) / 100;
//#9183 :Start
                var gross_profit_amount = this.gross_profit_amount();
                gross_profit_amount = formatMoneyAllRevert(gross_profit_amount);
//              gross_profit_rate = (new BigNumber(this.gross_profit_amount())).div(this.amount()).mul(100).round(2, BigNumber.ROUND_FLOOR).toNumber();
                gross_profit_rate = (new BigNumber(gross_profit_amount)).div(this.amount()).mul(100).round(2, BigNumber.ROUND_FLOOR).toNumber();
//#9183 :End
            }

            this.gross_profit_rate_value = gross_profit_rate;
            return this.gross_profit_rate_value.toFixed(2) + ' %';
        };
    };
    // <!----------------------------------------------END ADD NEW ROW------------------------------------->

    //<!------------------------------------ Set payament date when change delivery date -------------------------------------->
    var checkHolidayAndSetPaymentDate = function(year, month, day_delivery, date, item) {

        // console.log(year+"/"+month+"/"+day_delivery+"/"+date);
        // month: 9
        // 2015 9 9 31
        // payment_date = new Date(year, month, date);
        // var year = payment_date.getFullYear();
        // Month from 0
        // var month = payment_date.getMonth() + 1;
        // var day = payment_date.getDate();
        month = month + 1;
        // month 10 => Deccember
        payment_date = new Date(year, month, 0); // Month october (9) has 31 days
        var noofday = payment_date.getDate(); // = 31

        if(date > noofday) {
            date = noofday;
        }
        // month 10 -> December
        payment_date = new Date(year, month-1, date);

        payment_date_month = payment_date.getMonth() + 1;
        payment_date_month = (payment_date_month < 10) ? '0' + payment_date_month : payment_date_month;

        payment_date = payment_date.getFullYear() + '/' + payment_date_month + '/' + date;
        //#6992:Start
        if (!item.payment_date_without_check_holiday) {
            item.payment_date_without_check_holiday = payment_date;
        }
        //#6992:End

        $http({
            url : '/_admin/holiday/check_holiday',
            method : 'POST',
            data : {
                date : payment_date
            }
        }).success(function(data) {
                        if (data == 0) {
                            //#6992:Start
                            // item.payment_date = payment_date; // If payment date is not holiday date => keep the same
                            var tmp_billing_date_obj = new Date(item.billing_date);
                            var tmp_payment_date_obj = new Date(payment_date);
                            if (tmp_payment_date_obj.getTime() < tmp_billing_date_obj.getTime()) {
                                date = new Date(item.payment_date_without_check_holiday).getDate();
                                checkHolidayAndSetPaymentDate(year, month, day_delivery, date, item);
                            } else {
                                item.payment_date = payment_date; // If payment date is not holiday date => keep the same
                                delete(item.payment_date_without_check_holiday);
                            }
                            //#6992:End

                        } else {
                            // If payment date is holiday date, pay before one day
                            if(date>1) {
                                date = date - 1;
                            } else {
                                month = month - 1;
                                date = (new Date(year, month, 0)).getDate();
                            }
                            checkHolidayAndSetPaymentDate(year, month-1, day_delivery, date, item);
                        }
                    });
    }

    //<!--------------------------- Set billing date when change delivery date --------------------------------------->
    /**
     * [checkHolidayAndSetBillingDate description]
     * @param  {[type]} year         [year of closing date]
     * @param  {[type]} month        [month of closing date]
     * @param  {[type]} day_delivery [day of closing date]
     * @param  {[type]} closing_date [closing date]
     * @param  {[type]} item         [object on row]
     * @return {[type]}              [set date on cell billing date]
     */
    var checkHolidayAndSetBillingDate = function(year, month, day_delivery, closing_date, item) {

        month = month + 1;//13

        var billing_date = '';

        var date_obj = new Date(year, month, 0);//month: 13
        var last_date_of_month = date_obj.getDate();

        if (day_delivery > closing_date) { // delivery_date = 2015/12/31 => billing_date = 2016/01/20

            if (closing_date > last_date_of_month) {
                closing_date = last_date_of_month;
            }

        } else {

            if (closing_date > last_date_of_month) {
                closing_date = last_date_of_month;
            }
        }

        var billing_date_month = '';
        billing_date_month = date_obj.getMonth() + 1;
        billing_date_month = (billing_date_month < 10) ? '0' + billing_date_month : billing_date_month;

        billing_date = (date_obj.getFullYear()) + '/' + billing_date_month + '/' + closing_date;

        item.billing_date = billing_date;
    }

    //<!-------------------------------- Set payment date and billing date following delivery date------------------------------>
    $scope.setPaymentDateAndBillingDate = function (item) {
        // item: rowEntity
        if ($scope.client.is_ad_receive_payment == "0") {
            // Billing date
            var year = new Date(item.delivery_date).getFullYear();
            var month = new Date(item.delivery_date).getMonth();
            var day_delivery = new Date(item.delivery_date).getDate();
            // If delivery date > closing date => Billing in next month
            if (day_delivery > $scope.client.closing_date) {
                month = month + 1;
            }
            checkHolidayAndSetBillingDate(year, month, day_delivery, parseInt($scope.client.closing_date), item);
            // Payment date = payment date + month pending from DB
            month = month + parseInt($scope.client.payment_month_cd);
            checkHolidayAndSetPaymentDate(year, month, day_delivery, parseInt($scope.client.payment_day_cd), item);
        } else {
            item.payment_date = "";
            item.billing_date = "";;
        }
    }; // End $scope.setPaymentDateAndBillingDate = function (item) {

    $scope.client = {};
    $scope.client.id = 0;

    if (clients) {
        $scope.operators = clients.slice();
    }

    $scope.search_s_code = "";
    $scope.search_client_name = "";

    $scope.searchClients = function() {
        form_changed = true;

        //get clients
        $http({
            url : '/_admin/order/ajax_get_clients',
            method : 'POST',
            data : {
                's_code' : $scope.search_s_code,
                'client_name' : $scope.search_client_name
            }
        }).success(function(resp) {
            if (resp.result !== false) {
//#7070: Start
                if (typeof resp.data[0] !== 'undefined') {
                    $scope.client.id = resp.data[0].id;
                }
//#7070: End
                $scope.operators = resp.data;
            } else {
                $scope.operators = [];
            }
        });
    }

    //<!-------------------------------------------CLICK CLIENT CHANGE------------------------------------------>
    // $scope.hasChoseClient = false;
    $scope.clientChange = function() {
        // $scope.hasChoseClient = true;
        $scope.is_over_order = false;
        form_changed = true;

        //get selected client
        $http({
            url : '/_admin/order/ajax_get_clients',
            method : 'POST',
            data : {
                'client_id' : $scope.client.id
            }
        }).success(function(resp) {
            if (resp.result !== false) {

                var item = resp.data[0];
                angular.copy(item, $scope.client);
//#9763:Start
                $scope.client.client_debt = addCommas(resp.client_debt);
//#9763:End
                //$scope.client.address_1st = item.address_1st + item.address_2nd;
                $scope.client.address_1st = item.address_1st;
                $scope.client.address_2nd = item.address_2nd;

                $scope.client.remark = j_accounts[0].template;
                if(item.closing_date_name!=null) {
                    $("#closing_date_name").text(item.closing_date_name+"日");
                } else {
                    $("#closing_date_name").text("日");
                }
                if(item.payment_day_cd_name!=null) {
                    $("#payment_day_cd_name").text(item.payment_day_cd_name+"日");
                } else {
                    $("#payment_day_cd_name").text("日");
                }
                if(item.payment_month_cd_name!=null) {
                    $("#payment_month_cd_name").text(item.payment_month_cd_name+"月");
                } else {
                    $("#payment_month_cd_name").text("月");
                }
                // console.log($scope.client);
                $scope.client.is_ad_receive_payment_init = $scope.client.is_ad_receive_payment;
                $scope.client.j_account = my_account.id;
                $scope.client.credit_amount_show = addCommas($scope.client.credit_amount);
                $scope.can_be_save = true;
                $scope.message = "";

                if(item.is_ad_receive_payment == 1) {
                    $scope.message+="<span class='validation-error'>前受金のお客様です。</span><br/>";
                    $("#message_alert").html($scope.message).dialog({
                        title: "注意",
                        width: 'auto',
                        height: 'auto',
                        modal : true,
                        buttons: {
                            "OK": function() {
                                 $(this).dialog('close');
                            }
                        },
                        dialogClass: "order-dialog"
                     });
                    $scope.client.is_ad_receive_payment = 1;
                } else {
                    if(item.credit_level == 3) {
                        $scope.can_be_save = true;
//#7764:Start
//                        $scope.message+="<span class='validation-error'>こちらのお客様は与信が必要です。</span><br/>";
                        $scope.message+="<span class='validation-error'>このクライアントは申込書を作成する都度与信確認が必要です。</span><br/>";
//#7764:End
                        if ($scope.client.credit_amount == 0) {
                            $scope.message+="<span class='validation-error'>与信限度額を超えています。</span><br/>";
                        }

                        if($scope.message != "") {
                            $("#message_alert").html($scope.message).dialog({
                                title: "注意",
                                width: 'auto',
                                height: 'auto',
                                modal : true,
                                buttons: {
                                    "OK": function() {
                                        $(this).dialog('close');
                                    }
                                },
                                dialogClass: "order-dialog"
                            });
                        }
                    }
                }
            }
        });
        // console.log($scope.client);
        $scope.gridApi.rowEdit.setRowsDirty($scope.gridOptionsAdd.data);
    };
    // <!-------------------------------------END CLICK CLIENT CHANGE----------------------------------------->

    // <!-------------------------------------COPY ROW----------------------------------------->
    $scope.copyRow = function(row, api) {
        if(api.grid.rows.length > 23) {
            return false;
        }
//#7648:Start
//        var indexRow = $scope.gridOptionsAdd.data.indexOf(row);
//#7648:End
        // // console.log(api.grid.rows.length);
        var newrow = angular.copy(row);
//#7648:Start
//        $scope.gridOptionsAdd.data.splice(indexRow + 2, 0, newrow);
        $scope.gridOptionsAdd.data.splice(api.grid.rows.length + 2, 0, newrow);
//#7648:End
        var i = 0;
        angular.forEach($scope.gridOptionsAdd.data, function(item) {
            i++;
            item.branch = pad(i, 2);
        });
        $scope.sum();
        $scope.compute_credit();
    };
    // <!-------------------------------------END COPY ROW----------------------------------------->

    $scope.deleteRow = function(row) {
        var indexRow = $scope.gridOptionsAdd.data.indexOf(row);
        $scope.gridOptionsAdd.data.splice(indexRow, 1);
        var i = 0;
        angular.forEach($scope.gridOptionsAdd.data, function(item) {
            i++;
            item.branch = pad(i, 2);
        });
        $scope.sum();
        $scope.compute_credit();
    };

    $scope.sum = function() {
        // 税別合計
        $scope.client.cost_total_price = 0;
        // 消費税
        $scope.client.consumption_tax = 0;
        // 税込合計
        $scope.client.tax_total = 0;
        // 粗利合計
        $scope.client.gross_profit_total = 0;

        // Loop all items in detail part when change something related
        angular.forEach($scope.gridOptionsAdd.data, function(item) {
            // Get tax_rate (consumption_tax_adaptation.remark)
            if (item.delivery_date) {
                var tax_rate;
                var delivery_date = new Date(item.delivery_date);
                var arr = jQuery.grep($rootScope.tax_rates, function (i) {
                    var da = new Date(i.adaptation_date);
                    delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                    da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                    return da <= delivery_date;
                });
                var tax_rates = [];
                $.each(arr, function (key, value) {
                    tax_rates.push(value.rate);
                });
                tax_rate = Math.max.apply(Math, tax_rates);
                /*// Only tax type is 「内」 has the tax included in amount
                var no_tax_amount = (item.tax_type != 2) ? item.amount() : (item.amount() / (1 + tax_rate/100));
                $scope.client.cost_total_price += no_tax_amount;
                // Only tax type is 「非」 is nontaxable, means no tax needed for the product price
                $scope.client.consumption_tax += (item.tax_type != 3) ? (no_tax_amount * (tax_rate / 100)) : 0;
                // Gross profit total += gross profit amount
                $scope.client.gross_profit_total += item.gross_profit_amount();*/

                if(item.tax_type == 2) {
                    // (②/(1+税率)+小数点以下切り上げ)
                    //$scope.client.cost_total_price += f_ceil(item.amount()/(1+tax_rate/100));
                    var amount_pre_tax = (new BigNumber(item.amount())).div((new BigNumber(tax_rate)).div(100).plus(1)).ceil().toNumber();
                    $scope.client.cost_total_price += amount_pre_tax;
                    // (②-(②/(1+税率)+小数点以下切り上げ))
                    //$scope.client.consumption_tax += (item.amount() - f_ceil(item.amount()/(1+tax_rate/100)));
                    $scope.client.consumption_tax += (item.amount() - amount_pre_tax);
                    // ②+③
                    $scope.client.tax_total+=item.amount();
                }
                if(item.tax_type == 1 ) {
                    // (①*税率+小数点以下切り捨て)
                    //$scope.client.consumption_tax += f_floor(item.amount()*tax_rate/100);
                    $scope.client.consumption_tax += (new BigNumber(item.amount())).mul(tax_rate).div(100).floor().toNumber();
                    // ①+③
                    $scope.client.cost_total_price += item.amount();
                    // ①*(1+税率)+小数点以下切り捨て
                    //$scope.client.tax_total += f_floor(item.amount()*(1+tax_rate/100));
                    $scope.client.tax_total += (new BigNumber(tax_rate)).div(100).plus(1).mul(item.amount()).floor().toNumber();
                }
                if(item.tax_type == 3) {
                    // ①+③
                    $scope.client.cost_total_price += item.amount();
                    // ②+③
                    $scope.client.tax_total+=item.amount();
                }
//#9183 :Start
//                $scope.client.gross_profit_total += item.gross_profit_amount();
                $scope.client.gross_profit_total += formatMoneyAllRevert(item.gross_profit_amount());
//#9183 :End
            }
        }); // End angular.forEach($scope.gridOptionsAdd.data, function(item) {
    }


//<!-----------------------------------------------------------------END SUM-------------------------------------------------------->


    $scope.compute_credit = function() {
        // show errors on dialog
        for(var i = 0; i < $scope.gridApi.grid.rows.length; i ++) {
            $scope.gridApi.grid.rows[i].invalid = false;
        }

        if ($scope.client.is_ad_receive_payment == 0) {
            var gridData = $scope.gridOptionsAdd.data;
            var credit_amount = $scope.client.credit_amount;
            var client_id = $scope.client.id;
            $http({
                url : '/_admin/client/ajax_check_payment',
                method : 'POST',
                data : {client_id : client_id, branchs: $scope.gridOptionsAdd.data }
            }).success(function(data) {
                if (data.is_over_credit) {
                    $scope.is_credit_over = true;
                    angular.forEach(data.over_credit, function(item, key) {
                        for(var j = 0; j < $scope.gridApi.grid.rows.length; j ++) {
                            if($scope.gridApi.grid.rows[j].entity.branch == key) {
                                $scope.gridApi.grid.rows[j].invalid = true;
                            }
                        }
                    });
                    $("#message_alert").html("<span class='validation-error'>与信限度額を超えています。</span>").dialog({
                        title: "注意",
                        width: 'auto',
                        height: 'auto',
                        modal : true,
                        buttons: {
                            "OK": function() {
                                $(this).dialog('close');
                            }
                        },
                        dialogClass: "order-dialog"
                    });
                } else {
                    $scope.is_credit_over = false;
                }
            });
        } else {
             $scope.is_credit_over = false;
        }
    }

    //<!------------------------------------ WHEN EDIT VALUE IN ROW---------------------------------------->
    $scope.saveRow = function( rowEntity ) {
        // create a fake promise - normally you'd use the promise returned by $http or $resource
        var promise = $q.defer();
        $scope.gridApi.rowEdit.setSavePromise( rowEntity, promise.promise );
        // When change quantity
        if(rowEntity.quantity) {
            if(!jQuery.isNumeric(rowEntity.quantity) || rowEntity.quantity < 0) {
                rowEntity.quantity = 0
            }
        }
        // When change unit price
        if(rowEntity.unit_price) {
            // rowEntity.unit_price = 40;
            if(!jQuery.isNumeric(rowEntity.unit_price) || rowEntity.unit_price < 0) {
                rowEntity.unit_price = 0
            }
        }

        // When change delivery_date
        if (rowEntity.delivery_date) {
            if(rowEntity.delivery_date.indexOf('/') != -1 && rowEntity.delivery_date.indexOf('-') != -1) {
                rowEntity.delivery_date = '';
            } else {
                inputArray = rowEntity.delivery_date.split('/');

                if (rowEntity.delivery_date.indexOf('-') != -1) {
                    inputArray = rowEntity.delivery_date.split('-');
                }

                rowEntity.delivery_date = convertDate(inputArray);
                // Check valid inputted date
                var delivery_date = new Date(rowEntity.delivery_date);

                // If valid
                if (!isNaN(delivery_date)) {
                    rowEntity.delivery_date = delivery_date.getFullYear() + '/' + (delivery_date.getMonth() + 1) + '/' + delivery_date.getDate();

                    /*var fd = new Date();
                    // // // console.log("Delivery date: "+delivery_date)
                    var last_date_of_month = new Date(fd.getFullYear(), (fd.getMonth()+1), 0).getDate();
                    // // // console.log("Last day of month: "+last_date_of_month);
                    var closing_date = $scope.client.closing_date;
                    if(closing_date > last_date_of_month) {
                        closing_date = last_date_of_month;
                    }
                    var closing_date = new Date(fd.getFullYear(), fd.getMonth(), closing_date);
                    if (delivery_date <= closing_date) {
                        rowEntity.delivery_date = fd.getFullYear() + '/' + (fd.getMonth() + 1) + '/' + $scope.client.closing_date;
                    }*/
                } else {
                    rowEntity.delivery_date = '';
                }
            }
        }
        if (rowEntity.billing_date) {
            if(rowEntity.billing_date.indexOf('/') != -1 && rowEntity.billing_date.indexOf('-') != -1) {
                rowEntity.billing_date = '';
            } else {
                inputArray = rowEntity.billing_date.split('/');
                if (rowEntity.billing_date.indexOf('-') != -1) {
                    inputArray = rowEntity.billing_date.split('-');
                }

                rowEntity.billing_date = convertDate(inputArray);
                var billing_date = new Date(rowEntity.billing_date);
                if (isNaN(billing_date)) {
                    rowEntity.billing_date = '';
                }
            }
        }
        if (rowEntity.payment_date) {
            if(rowEntity.payment_date.indexOf('/') != -1 && rowEntity.payment_date.indexOf('-') != -1) {
                rowEntity.payment_date = '';
            } else {
                inputArray = rowEntity.payment_date.split('/');
                if (rowEntity.payment_date.indexOf('-') != -1) {
                    inputArray = rowEntity.payment_date.split('-');
                }

                rowEntity.payment_date = convertDate(inputArray);
                var payment_date = new Date(rowEntity.payment_date);
                if (isNaN(payment_date)) {
                    rowEntity.payment_date = '';
                }
            }
        }
        rowEntity.tax_type = parseInt(rowEntity.tax_type)
//#7441:Start
//        rowEntity.unit_price = Math.floor(rowEntity.unit_price * 100) / 100;
//        rowEntity.unit_price = Math.floor(strip(rowEntity.unit_price * 100)) / 100;
        rowEntity.unit_price = (new BigNumber(rowEntity.unit_price)).round(2, BigNumber.ROUND_FLOOR).toNumber();
//#7441:End
        rowEntity.unit_price = rowEntity.unit_price || 0;
        if (rowEntity.cost_unit_price) {
//#7441:Start
//            rowEntity.cost_unit_price = Math.floor(strip(rowEntity.cost_unit_price * 100)) / 100;
//            rowEntity.cost_unit_price = Math.floor(strip(rowEntity.cost_unit_price * 100)) / 100;
            rowEntity.cost_unit_price = (new BigNumber(rowEntity.cost_unit_price)).round(2, BigNumber.ROUND_FLOOR).toNumber();
//#7441:End
            rowEntity.cost_unit_price = rowEntity.cost_unit_price || 0;
            //rowEntity.cost_total_price = Math.floor(strip(rowEntity.cost_unit_price * rowEntity.quantity));
            rowEntity.cost_total_price = (new BigNumber(rowEntity.cost_unit_price)).mul(rowEntity.quantity).floor().toNumber();
            rowEntity.cost_total_price = rowEntity.cost_total_price || 0;
        }

        // Set payment date and billing date following delivery date (when is_ad_receive_payment = 0)
        if(rowEntity.delivery_date && $scope.client.is_ad_receive_payment == 0) {
            $scope.setPaymentDateAndBillingDate(rowEntity);
        }

        $scope.sum();
        $scope.compute_credit();

        promise.resolve();
    };

    $scope.gridOptionsAdd.onRegisterApi = function(gridApi) {
        $scope.gridApi = gridApi;
        gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
//#7623:Start
        gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue) {
            if (colDef.name == 'cost_unit_price' && newValue == '') {
                rowEntity.cost_unit_price = 0;
            } else if (colDef.name == 'cost_total_price' && newValue == '') {
                rowEntity.cost_total_price = 0;
            } else if (colDef.name == 'quantity' && newValue == '') {
                rowEntity.quantity = 0;
            }
        });
//#7623:End
    }
    // // // console.log($scope.client);
    // <!---------------------------CRTL + S---------------------------->
    $(document).bind('keydown', function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
//#9160:Start
            if ($("#dialogAdd").dialog( "isOpen" )===true) {
                $scope.saveData();
            }
//#9160:End
            return false;
            // $scope.save();
            // return false;
        }
    });
    // <!--------------------------CLICK SAVE---------------------------->
    $scope.saveData = function () {

//#8222:Start
        $(".both-blind").show();
//#8222:End
        angular.forEach(j_accounts, function (item) {

            if (item.id == $scope.client.j_account) {
//#8222:Start
//                $(".both-blind").hide();
 //#8222:End
                $scope.client.bu_j_code = item.j_code;
                return;
            }
        });
       /* if(!$scope.hasChoseClient) {
            $scope.errors = "クライアントを選択してください";
            return;
        }*/
//#9160:Start
        angular.forEach($scope.gridOptionsAdd.data, function(item) {
//#9335:Start
//        if (item.od_cost_unit_price) {
          if (item.od_cost_unit_price && item.od_cost_unit_price!= 0 ) {
//#9335:End
                item.cost_total_price = (new BigNumber(item.cost_unit_price)).mul(item.quantity).floor().toNumber();
                item.cost_total_price = item.cost_total_price || 0;
            }
        });
//#9160:End
            $http({
                url : '/_admin/order/save',
                method : 'POST',
                data : {
                    order : $scope.client,
                    branchs : $scope.gridOptionsAdd.data
                }
            }).success(function(data) {
//#8222:Start
                $(".both-blind").hide();
//#8222:End
                $scope.errors = '';
                var errors = '';
                // show errors on dialog
                for(var i = 0; i < $scope.gridApi.grid.rows.length; i ++) {
                    $scope.gridApi.grid.rows[i].invalid = false;
                }

                if (data.error_string || data.error_string_branchs) {
                    if(data.error_string) {
                        errors += '<div>' + data.error_string +'</div>';
                    }

                    angular.forEach(data.error_string_branchs, function(item, key) {
                        for(var j = 0; j < $scope.gridApi.grid.rows.length; j ++) {
                            if($scope.gridApi.grid.rows[j].entity.branch == key) {
                                $scope.gridApi.grid.rows[j].invalid = true;
                            }
                        }
                        errors += '<span class="title-icon">枝' + key + ' を修正して下さい。</span>'
                            + '<div><p>' + item + '</p></div>';
                    });

                    $("#dialog-errors").html('<div class="errors_dialog">'+errors+'</div>');
                    $("#dialog-errors").dialog({
                        title: '注意',
                        minWidth: 560,
                        minHeight: 100,
                        maxHeight: 250,
                        resizable: true,
                        modal: false,
                        dialogClass: 'order-dialog fixed-dialog',
                        position: { my: "right top+30", at: "right top+30", of: window},
                        dragStart: function(event, ui) {
                            $('.order-dialog').removeClass('fixed-dialog')
                        },
                        dragStop: function(event, ui) {
                            // $('.acceptance-dialog').addClass('fixed-dialog')
                        },
                        resizeStart: function( event, ui ) {
                            $(this).dialog("option", "maxHeight", false);
                        }
                    });
                }
                else {
                    if ($("#dialog-errors").hasClass('ui-dialog-content')) {
                        $("#dialog-errors").dialog('destroy');
                    }
                    $scope.is_submitted = true;
                    $scope.error_success = '<div class="success-message blink_me">申請しました。</div>';
                    $rootScope.search();
                }
            });
    }
}])
.filter('mapProduct', function() {
    return function(input) {
        if (!input) {
            return '';
        } else {
            for (i = 0; i < products.length; i++) {
                if (products[i].id == input) {
                    return products[i].name;
                }
            }
        }
    };
})
;
