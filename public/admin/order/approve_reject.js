app.controller('approveRejectCtrl', ['$scope', '$rootScope', '$http', '$q', '$interval', 'uiGridConstants', function ($scope, $rootScope, $http, $q, $interval, uiGridConstants) {
    $scope.is_submitted = false;
    $scope.gridOptionsDetailApproveReject = {
            enableSorting: false,
            enableColumnMenus: false,
    };

    //init columns
    $scope.gridOptionsDetailApproveReject.columnDefs = [
                                     { name: 'od_branch_cd', displayName: jApproveRejects[0], width: 100, cellClass: 'grid-align'},
                                     { name: 'od_order_status', displayName: jApproveRejects[1], width: 100, cellClass: 'grid-align', visible: false},
                                     { name: 'od_order_status_name', displayName: jsGlobals[1], width: 100},
                                     { name: 'p_name', displayName: jApproveRejects[2], width: 100, cellClass: 'grid-align'},
                                     { name: 'od_contents', displayName: jApproveRejects[3], width: 100, cellClass: 'grid-align'},
                                     { name: 'od_delivery_date', displayName: jApproveRejects[4], width: 100, cellClass: 'grid-align', cellFilter: 'formatDate'},
                                     { name: 'od_billing_date', displayName: jApproveRejects[5], width: 100, cellClass: 'grid-align', cellFilter: 'formatDate'},
                                     { name: 'od_payment_date', displayName: jApproveRejects[6], width: 100, cellFilter: 'formatDate'},
                                     { name: 'od_quantity', displayName: jApproveRejects[7], width: 100, cellClass: 'a-right', cellFilter: 'number:0'},
                                     { name: 'od_tax_type', displayName: jApproveRejects[8], width: 100, cellFilter: 'mapTaxType'},
                                     { name: 'od_unit_price', displayName: jApproveRejects[9], width: 100, cellClass: 'a-right', cellFilter: 'number:2'},
                                     { name: 'od_amount', displayName: jApproveRejects[10], width: 100, cellClass: 'a-right', cellFilter: 'number:0'},
                                     { name: 'od_cost_unit_price', displayName: jApproveRejects[11], width: 100, cellClass: 'a-right', cellFilter: 'number:2'},
                                     { name: 'od_cost_total_price', displayName: jApproveRejects[12], width: 100, cellClass: 'a-right', cellFilter: 'number:0'},
                                     { name: 'od_gross_profit_amount', displayName: jApproveRejects[13], width: 100, cellClass: 'a-right', cellFilter: 'number:0'},
                                     { name: 'od_gross_profit_rate', displayName: jApproveRejects[14], width: 100, cellClass: 'a-right'},
    ];

    $scope.close = function() {
        $("#dialog_approve_reject").dialog("close");
    }

    //processing approve order
    $scope.approve = function(order_id) {

//#8222:Start
    $(".both-blind").show();
//#8222:End
        $http({
            url : '/_admin/order/approve_order',
            method : 'POST',
            data : {
                order_id : order_id,
                remark: $scope.remark,
                comment: $scope.comment
            }
        }).success(function(resp) {
//#8222:Start
            $(".both-blind").hide();
//#8222:End
            if (resp.result !== false) {
                $scope.is_submitted = true;
                $scope.error_success = '<div class="success-message blink_me">受注承認しました。</div>';

                $rootScope.search();
            } else {
                $('div#form_error_msg_reject').append('<span class="error-message">' + resp.error_msg + '</span>');
            }
        });
    };

    //processing reject order
    $scope.reject = function(order_id) {
//#8222:Start
        $(".both-blind").show();
//#8222:End
        $http({
            url : '/_admin/order/reject_order',
            method : 'POST',
            data : {
                order_id : order_id,
                remark: $scope.remark,
                comment: $scope.comment
            }
        }).success(function(resp) {
//#8222:Start
            $(".both-blind").hide();
//#8222:End
            if (resp.result !== false) {
                $scope.is_submitted = true;
                $scope.error_success = '<div class="success-message blink_me">却下しました。</div>';
                $rootScope.search();
            } else {
                $('div#form_error_msg').append('<span class="error-message">' + resp.error_msg + '</span>');
            }
        });
    };

    $rootScope.loadApproveReject = function(data_grid, data_order_master) {
        $scope.is_submitted = false;
        //tmp#6841: Start 2015/09/24
        $scope.isCanEdit = true;
        //tmp#6841: End
        $scope.error_success = '';
        $scope.gridOptionsDetailApproveReject.data = data_grid;

        //$scope.order = $scope.gridOptionsDetailApproveReject.data[0];
        $scope.order = data_order_master;

        $('#dialog').dialog("close");
        $('#dialog_approve_reject').dialog('option', 'title', $('input[name="dialog_approve_reject_title"]').val());
        $("#dialog_approve_reject").dialog( "option", "height", "auto");
        $("#dialog_approve_reject").dialog( "option", "width", "80%" );
        $('#dialog_approve_reject').dialog("option", "position", {my: "center", at: "center", of: window});
        $('#dialog_approve_reject').dialog("open");
        $('#dialog_approve_reject .grid').width($('#dialog_approve_reject').width());
        $(window).trigger('resize');

        $scope.order.o_cost_total_price = 0;
        $scope.order.o_consumption_tax = 0;
        $scope.order.o_tax_total = 0;
        $scope.order.o_gross_profit_total = 0;
        $scope.comment = '';
        angular.forEach($scope.gridOptionsDetailApproveReject.data, function(item) {
			//tmp#6841: Start 2015/09/24
            if ((typeof item.isCanEdit != 'undefined') && (item.isCanEdit == false)) {
                $scope.isCanEdit = false;
            }
            //tmp#6841: Start 2015/09/24
            var tax_rate;
            var delivery_date = new Date(item.od_delivery_date);
            var arr = jQuery.grep($rootScope.tax_rates, function( i ) {
                var da = new Date(i.adaptation_date);
                delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                return da <= delivery_date;
            });
            var tax_rates = [];
            $.each(arr, function(key, value){
                tax_rates.push(value.rate);
            });
            tax_rate = Math.max.apply(Math, tax_rates);

            // Only tax type is 「内」 has the tax included in amount
            /*var no_tax_amount = (item.od_tax_type != 2) ? item.od_amount() : (item.od_amount() / (1 + tax_rate/100));
            $scope.order.o_cost_total_price += no_tax_amount;
            // Only tax type is 「非」 is nontaxable, means no tax needed for the product price
            $scope.order.o_consumption_tax += (item.od_tax_type != 3) ? (no_tax_amount * (tax_rate / 100)) : 0;
            $scope.order.o_gross_profit_total += item.od_gross_profit_amount();*/


            if(item.od_tax_type == 2) {
                // (②/(1+税率)+小数点以下切り上げ)
                var amount_pre_tax = (new BigNumber(item.od_amount)).div((new BigNumber(tax_rate)).div(100).plus(1)).ceil().toNumber();
                //$scope.order.o_cost_total_price += f_ceil(item.od_amount/(1+tax_rate/100));
                $scope.order.o_cost_total_price += amount_pre_tax;
                // (②-(②/(1+税率)+小数点以下切り上げ))
                //$scope.order.o_consumption_tax += (item.od_amount - f_ceil(item.od_amount/(1+tax_rate/100)));
                $scope.order.o_consumption_tax += (item.od_amount - amount_pre_tax);
                // ②+③
                $scope.order.o_tax_total+=item.od_amount;
            }
            if(item.od_tax_type == 1 ) {
                // (①*税率+小数点以下切り捨て)
                //$scope.order.o_consumption_tax += f_floor(item.od_amount*tax_rate/100);
                $scope.order.o_consumption_tax += (new BigNumber(item.od_amount)).mul(tax_rate).div(100).floor().toNumber();
                // ①+③
                $scope.order.o_cost_total_price += item.od_amount;
                // ①*(1+税率)+小数点以下切り捨て
                //$scope.order.o_tax_total += f_floor(item.od_amount*(1+tax_rate/100));
                $scope.order.o_tax_total += (new BigNumber(item.od_amount)).mul((new BigNumber(tax_rate)).div(100).plus(1)).floor().toNumber();
            }
            if(item.od_tax_type == 3) {
                // ①+③
                $scope.order.o_cost_total_price += item.od_amount;
                // ②+③
                $scope.order.o_tax_total+=item.od_amount;
            }
            $scope.order.o_gross_profit_total += parseInt(item.od_gross_profit_amount);

        });

        // $scope.order.o_cost_total_price = Math.ceil($scope.order.o_cost_total_price);
        // $scope.order.o_consumption_tax = Math.floor($scope.order.o_consumption_tax);
        // $scope.order.o_tax_total = $scope.order.o_cost_total_price + $scope.order.o_consumption_tax;
    }

}])
.filter('formatDate', function() { //#6853: Start (3 files refer to this function: approve_reject.js, change_repo.js, detail.js)
    return function(input) {
        return input.replace(/-/g, '/');
    };
}); //#6853: End

