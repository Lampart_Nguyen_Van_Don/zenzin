app.controller('ChangeRepoCtrl', ['$scope', '$rootScope', '$http', '$q', '$interval', 'uiGridConstants', '$controller', function($scope, $rootScope, $http, $q, $interval, uiGridConstants, $controller) {
    $scope.gridChangeReportOptions = {
            selectionRowHeaderWidth: 35,
            rowEditWaitInterval: -1,
            rowTemplate:
                '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ui-grid-cell ng-class="{\'orange-row\': (row.entity.od_is_cancel_request == 1)}" ></div>',
    };
    $scope.gridChangeReportOptions.enableCellEditOnFocus = false;
    $scope.gridChangeReportOptions.multiSelect = true;
    $scope.gridChangeReportOptions.enableCellEdit = false;
    $scope.is_submitted = false;

    $scope.gridChangeReportOptions.columnDefs = [
                                     // No show
                                     { name: 'ol_id', visible: false, displayName: 'ol_id' },
                                     // 枝
                                     { name: 'od_branch_cd', displayName: jsGlobals[0], width: 100 },
                                     // No show
                                     { name: 'od_order_status', displayName: jsGlobals[1], width: 100, visible: false },
                                     // 受注ステータス
                                     { name: 'od_order_status_name', displayName: jsGlobals[1], width: 100 },
                                     // 変レポ
                                     { name: 'change_report', displayName: jsGlobals[2], width: 100, 
                                        // cellTemplate: '<div class="cell-template-data" ng-style="grid.appScope.change_color(row)">{{row.entity.change_report}}</div>'
                                        cellClass:
                                            function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                return $rootScope.check_value('change_report', row.entity);
                                            }
                                     },
                                     // 商品名
                                     { name: 'p_name', displayName: jsGlobals[3], width: 100 },
                                     // 内容
                                     { name: 'od_contents', displayName: jsGlobals[4], width: 100 },
                                     // 納品日
                                     { name: 'od_delivery_date', displayName: jsGlobals[5], width: 100, 
                                        // cellTemplate: '<div class="cell-template-data" ng-style="grid.appScope.change_color(row)">{{COL_FIELD CUSTOM_FILTERS}}</div>', cellFilter: 'formatDate'
                                        cellClass:
                                            function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                return $rootScope.check_value('od_delivery_date', row.entity);
                                            }

                                     },
                                     // 請求日
                                     { name: 'od_billing_date', displayName: jsGlobals[6], width: 100, 
                                        // cellTemplate: '<div class="cell-template-data" ng-style="grid.appScope.change_color(row)">{{COL_FIELD CUSTOM_FILTERS}}</div>', cellFilter: 'formatDate'
                                        cellClass:
                                            function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                return $rootScope.check_value('od_billing_date', row.entity);
                                            }

                                     },
                                     // 支払日
                                     { name: 'od_payment_date', displayName: jsGlobals[7], width: 100, 
                                        // cellTemplate: '<div class="cell-template-data" ng-style="grid.appScope.change_color(row)">{{COL_FIELD CUSTOM_FILTERS}}</div>', cellFilter: 'formatDate'
                                        cellClass:
                                            function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                return $rootScope.check_value('od_payment_date', row.entity);
                                            }
                                     },
                                     // No show
                                     { name: 'od_is_cancel_request', width: 100, visible: false },
                                     // No show
                                     { name: 'od_shift_color_cd', width: 100, visible: false },
                                     // 数量
                                     { name: 'od_quantity', width: 100, displayName: jsGlobals[8], cellFilter: 'number:0' },
                                     // 税
                                     { name: 'od_tax_type', width: 100, displayName: jsGlobals[9], cellFilter: 'mapTaxType', editDropdownIdLabel: 'code', editDropdownValueLabel: 'name', editDropdownOptionsArray: tax_division},
                                     // 単価
                                     { name: 'od_unit_price', width: 100, displayName: jsGlobals[10], cellFilter: 'number:2', cellClass: 'a-right' },
                                     // 合計
                                     { name: 'od_amount', width: 100, displayName: jsGlobals[11], cellFilter: 'number:0', cellClass: 'a-right' },
                                     // 原価税別単価
                                     {
                                        name: 'od_cost_unit_price',
                                        width: 100,
                                        displayName: jsGlobals[12],
                                        cellFilter: 'number:2',
                                        // cellTemplate: '<div class="ui-grid-cell-contents" ng-style="grid.appScope.change_color_when_change(row)">{{row.entity.od_cost_unit_price}}</div>',
                                        // cellClass: 'a-right'
//#7085:Start
                                        cellClass:
                                            function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                return $rootScope.check_value('od_cost_unit_price', row.entity);
                                            }
//#7085:End
                                    },
                                     // 原価税別合計
                                     {
                                        name: 'od_cost_total_price',
                                        width: 100,
                                        displayName: jsGlobals[13],
                                        cellFilter: 'number:0',
                                        // cellTemplate: '<div class="ui-grid-cell-contents" ng-style="grid.appScope.change_color_when_change(row)">{{row.entity.od_cost_total_price | number}}</div>',
//#7085:Start                                        
                                        cellClass:
                                            function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                               return $rootScope.check_value('od_cost_total_price', row.entity);
                                            }
//#7085:End
                                    },
                                     // 粗利合計
                                     {
                                        name: 'od_gross_profit_amount',
                                        width: 100,
                                        displayName: jsGlobals[14],
                                        cellFilter: 'number:0',
                                        // cellTemplate: '<div class="ui-grid-cell-contents" ng-style="grid.appScope.change_color_when_change(row)">{{row.entity.od_gross_profit_amount | number}}</div>',
//#7390:Start
//#7085:Start
                                        cellClass:
                                            function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                return $rootScope.check_value('od_gross_profit_amount', row.entity);
                                            }
//#7085:End
                                    },
//#7390:End
                                    // 粗利率
                                     {  name: 'od_gross_profit_rate',
                                        width: 100,
                                        displayName: jsGlobals[15],
                                        // cellTemplate: '<div class="ui-grid-cell-contents" ng-style="grid.appScope.change_color_when_change(row)">{{row.entity.od_gross_profit_rate}}</div>',
//#7085:Start                                        
                                        cellClass:
                                             function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                return $rootScope.check_value('od_gross_profit_amount', row.entity);
                                             }
//#7085:End
//#7390:End
                                     }

                                     ];

    // Approve report order
    $scope.approve_report = function() {
//#8222:Start
    	$(".both-blind").show();
//#8222:End
        $http({
            url: '/_admin/order/approve_report',
            method: 'POST',
            data: {
                order_id: $scope.order.o_id,
                j_code: $scope.order.o_j_code,
                // branch_cd: $scope.order.od_branch_cd,
                old_comment: $scope.order.o_comment,
                comment: $scope.report_comment,
                account_id: $scope.order.o_account_id
            }
        }).success(function(data) {
//#8222:Start
        	$(".both-blind").hide();
//#8222:End
            $scope.is_submitted = true;
            $scope.error_success = '<div class="success-message blink_me">変レポ承認しました。</div>';
            $rootScope.search();
        });
    }

    // reject report order
    $scope.reject_report = function() {

//#8222:Start
    	$(".both-blind").show();
//#8222:End

        $http({
            url: '/_admin/order/reject_report',
            method: 'POST',
            data: {
                order_id: $scope.order.o_id,
                j_code: $scope.order.o_j_code,
                old_comment: $scope.order.o_comment,
                reject_comment: $scope.report_comment,
            }
        }).success(function(data){

//#8222:Start
        	$(".both-blind").hide();
//#8222:End

        	$scope.is_submitted = true;
            $scope.error_success = '<div class="success-message blink_me">却下しました。</div>';
            $rootScope.search();
        });
    }

    $scope.close = function() {
        $("#dialog_change_report").dialog("close");
    }

    $rootScope.loadChangeReport = function (order_id) {
        $http({
            url : '/_admin/order/change_repo_approve_reject',
            method : 'POST',
            data : {
                order_id : order_id,
                type: 'approve_reject'
            }
        }).success(function(data) {
            $scope.report_comment = '';
            $scope.is_submitted = false;
            $scope.error_success = '';
            $('#dialog').dialog("close");
            $("#dialog_change_report").dialog('option', 'title', '変レポ承認/却下');
            $("#dialog_change_report").dialog( "option", "height", "auto");
            $("#dialog_change_report").dialog( "option", "width", "80%" );
            $('#dialog_change_report').dialog("open");
            $('#dialog_change_report .grid').width($('#dialog_change_report').width());
            $(window).trigger('resize');
            $scope.gridChangeReportOptions.data = data.list_grid_detail;
            $scope.order = data.master_information;
            if($scope.order.o_is_ad_receive_payment == 1) {
                $scope.order.payment_terms = "前受金";
            }
            else {
                $scope.order.payment_terms = "締め日：" + $scope.order.closing_date_name+" 日"
                                            + "　支払月：" + $scope.order.payment_month_cd_name+" 月"
                                            + "　支払日：" + $scope.order.payment_day_cd_name +" 日";
            }

            angular.forEach(data.list_grid_detail , function(value, key) {
                value.od_gross_profit_rate =　parseFloat(value.od_gross_profit_rate).toFixed(2)　+ "%";
                // console.log(value.od_gross_profit_rate);
                // value.change_report = '-';
            });

            // console.log('change report');
            // console.log($scope.gridChangeReportOptions.data);
            /*angular.forEach(data , function(value, key) {
                // if have log data
                if (value.ol_id != null) {
                    // define item to insert
                    var item = {};
//                    item.ol_id = value.ol_id;
                    item.od_branch_cd = value.od_branch_cd;
                    item.od_order_status = value.od_order_status;
                    item.o_order_status_name = value.o_order_status_name;
                    item.od_is_change_report_apply = value.od_is_change_report_apply;
                    item.p_name = value.p_name;
                    item.od_contents = value.ol_contents;
                    item.od_delivery_date = value.ol_delivery_date;
                    item.od_billing_date = value.ol_billing_date;
                    item.od_payment_date = value.ol_payment_date;
                    item.od_quantity = value.ol_quantity;
                    item.od_tax_type = value.ol_tax_type;
                    item.od_unit_price = value.ol_unit_price;
                    item.od_amount = value.ol_amount;
                    item.od_cost_unit_price = value.ol_cost_unit_price;
                    item.od_cost_total_price = value.ol_cost_total_price;
                    item.od_gross_profit_amount = value.ol_gross_profit_amount;
                    item.od_gross_profit_rate = parseFloat(value.ol_gross_profit_rate).toFixed(2) + "%";
                    item.od_shift_color_cd = value.od_shift_color_cd;
                    item.log = true;
                    item.change_report = '変更後';

                    //old branch
                    value.change_report = '変更前';
                    // add row at position index, 0: no remove, item object data for log
                    $scope.gridChangeReportOptions.data.splice(key+1, 0, item);
                }
                else if (value.od_is_cancel_request == 1) {
                    value.change_report = 'キャンセル';
                }

            });*/

        });
    };
}])
.filter('formatDate', function() { //#6853: Start (3 files refer to this function: approve_reject.js, change_repo.js, detail.js)
    return function(input) {
        return input.replace(/-/g, '/');
    };
}); //#6853: End
