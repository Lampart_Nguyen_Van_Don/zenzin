
app.controller('DetailCtrl', ['$rootScope', '$scope', '$http', '$q', '$interval', 'uiGridConstants', function ($rootScope, $scope, $http, $q, $interval, uiGridConstants) {
    //#6972:Start
    $rootScope.lang = 'ja';
    //#6972:End

    //tmp #6888: Start 2015/09/25
    if (typeof $rootScope.tax_rates == 'undefined') {
        get_tax_rate();
    }
    //tmp #6888: End 2015/09/25

    $scope.gridOptionsDetail = {
        selectionRowHeaderWidth: 35,
        paginationPageSizes: [20, 30, 50],
        paginationPageSize: 20,
        rowTemplate:
            '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ui-grid-cell ng-class="{\'orange-row\': (row.entity.od_is_cancel_request == 1)}" ></div>'
    };
    $scope.gridOptionsDetail.enableCellEditOnFocus = false;
    $scope.gridOptionsDetail.multiSelect = true;
    $scope.gridOptionsDetail.enableCellEdit = false;

    $scope.show_approved_cell = function(row) {
        if (row.entity.od_order_status == 1 && row.entity.od_branch_cd == 1) {
            return true;
        }
        return false;
    }
    $scope.show_approved_column = function() {
        return true;
    }
    $scope.my_account = my_account;
    // Set value for: o_is_ad_sales_slip_input (前売伝) & o_is_ad_invoice_issue (前請求)


    $rootScope.check_value =  function(colName, rowEntity) {

        switch (colName) {

            case 'change_report':
                if (typeof rowEntity.order_detail_log_id != 'undefined' && typeof rowEntity.od_shift_color_cd != 'undefined') {
                    var color_class_name = rowEntity.od_shift_color_cd;
                    if (rowEntity.od_shift_color_cd.indexOf("#") == 0) {
                        color_class_name = rowEntity.od_shift_color_cd.substring(1);
                    }
                    // create class shift color with value from db, just create unique
                    if (global_css.indexOf(color_class_name) == -1) {
                        $("#shift_color_style").append(".color_" +  color_class_name + " { background-color: "+ rowEntity.od_shift_color_cd +" !important;}");
                        global_css.push(color_class_name);
                    }
                    return "color_" +  color_class_name;
                }
                return "";
                break;

            case 'od_delivery_date':
                if (typeof rowEntity.order_detail_log_id != 'undefined' && typeof rowEntity.od_shift_color_cd != 'undefined') {
                    var color_class_name = rowEntity.od_shift_color_cd;
                    if (rowEntity.od_shift_color_cd.indexOf("#") == 0) {
                        color_class_name = rowEntity.od_shift_color_cd.substring(1);
                    }
                    // create class shift color with value from db, just create unique
                    if (global_css.indexOf(color_class_name) == -1) {
                        $("#shift_color_style").append(".color_" +  color_class_name + " { background-color: "+ rowEntity.od_shift_color_cd +" !important;}");
                        global_css.push(color_class_name);
                    }
                    return "color_" +  color_class_name;
                }
                return "";
                break;

            case 'od_billing_date':
                if (typeof rowEntity.order_detail_log_id != 'undefined' && typeof rowEntity.od_shift_color_cd != 'undefined') {
                    var color_class_name = rowEntity.od_shift_color_cd;
                    if (rowEntity.od_shift_color_cd.indexOf("#") == 0) {
                        color_class_name = rowEntity.od_shift_color_cd.substring(1);
                    }
                    // create class shift color with value from db, just create unique
                    if (global_css.indexOf(color_class_name) == -1) {
                        $("#shift_color_style").append(".color_" +  color_class_name + " { background-color: "+ rowEntity.od_shift_color_cd +" !important;}");
                        global_css.push(color_class_name);
                    }
                    return "color_" +  color_class_name;
                }
                return "";
                break;

            case 'od_payment_date':
                if (typeof rowEntity.order_detail_log_id != 'undefined' && typeof rowEntity.od_shift_color_cd != 'undefined') {
                    var color_class_name = rowEntity.od_shift_color_cd;
                    if (rowEntity.od_shift_color_cd.indexOf("#") == 0) {
                        color_class_name = rowEntity.od_shift_color_cd.substring(1);
                    }
                    // create class shift color with value from db, just create unique
                    if (global_css.indexOf(color_class_name) == -1) {
                        $("#shift_color_style").append(".color_" +  color_class_name + " { background-color: "+ rowEntity.od_shift_color_cd +" !important;}");
                        global_css.push(color_class_name);
                    }
                    return "color_" +  color_class_name;
                }
                return "";
                break;

            case 'od_cost_unit_price':
                if (typeof rowEntity.order_detail_log_id != 'undefined' && rowEntity.is_change_price && rowEntity.od_cost_unit_price < 0) {
                    return 'color_minus_price text-right red';
                } else if (typeof rowEntity.order_detail_log_id != 'undefined' && rowEntity.is_change_price) {
                    return 'color_minus_price text-right';
                } else if (rowEntity.od_cost_unit_price < 0) {
                    return 'text-right red';
                }
                return 'text-right';
                break;

            case 'od_cost_total_price':
                if (typeof rowEntity.order_detail_log_id != 'undefined' && rowEntity.is_change_price && rowEntity.od_cost_total_price < 0) {
                    return 'color_minus_price text-right red';
                } else if (typeof rowEntity.order_detail_log_id != 'undefined' && rowEntity.is_change_price) {
                    return 'color_minus_price text-right';
                } else if (rowEntity.od_cost_total_price < 0) {
                    return 'text-right red';
                }
                return 'text-right';
                break;

            case 'od_gross_profit_amount':
                if (typeof rowEntity.order_detail_log_id != 'undefined' && rowEntity.is_change_price && rowEntity.od_gross_profit_amount < 0) {
                    return 'color_minus_price text-right red';
                } else if (typeof rowEntity.order_detail_log_id != 'undefined' && rowEntity.is_change_price) {
                    return 'color_minus_price text-right';
                } else if (rowEntity.od_gross_profit_amount < 0) {
                    return 'text-right red';
                }
                return 'text-right';
                break;

            case 'od_gross_profit_rate':
                var gross_profit_rate = rowEntity.od_gross_profit_rate.replace('%', '');
                if (typeof rowEntity.order_detail_log_id != 'undefined' && rowEntity.is_change_price && gross_profit_rate < 0) {
                    return 'color_minus_price text-right red';
                } else if (typeof rowEntity.order_detail_log_id != 'undefined' && rowEntity.is_change_price) {
                    return 'color_minus_price text-right';
                } else if (gross_profit_rate < 0) {
                    return 'text-right red';
                }
                return 'text-right';
                break;
        }
    } //end check value

    function ext_link(colName) {
        /*var out = '';
        // var ng = false; o_is_ad_receive_payment
        out += '<div class="ui-grid-cell-contents" ng-if="row.entity.o_is_ad_receive_payment != \'1\'">－</div>';
        out += '<div class="ui-grid-cell-contents" ng-if="row.entity.o_is_ad_receive_payment == \'1\'">{{row.entity.' + colName + ' | mapYesNo}}</div>';

        // console.log("Colname:");
        // console.log(colName);
        // console.log("Out:");
        // console.log(out);

        return out;*/




        var out = '';
        var ng = false;

        if (colName == 'o_is_ad_sales_slip_input' || colName=='o_is_ad_receive_payment_sales_input' || colName == 'o_is_ad_invoice_issue') {
//#7194:Start
            // out += '<div ng-if="row.entity.o_is_ad_receive_payment != \'1\'">－</div>';
            out += '<div class="ui-grid-cell-contents" ng-if="row.entity.o_is_ad_receive_payment != \'1\'">－</div>';
            out += '<div ng-if="row.entity.o_is_ad_receive_payment == \'1\'">';
//#7194:End
        }

        switch (colName) {
            case 'o_is_ad_sales_slip_input':
            case 'o_is_ad_receive_payment_sales_input':
            case 'o_is_sales_slip_input':
                var action = '/_admin/sales/show';
                //var hidden = '<input type="hidden" name="" ng-value="">'; //j_code + branch
                var hidden = '<input type="hidden" name="search_j_code_branch_cd_from" ng-value="row.entity.o_j_code + row.entity.od_branch_cd">' +
                '<input type="hidden" name="search_j_code_branch_cd_to" ng-value="row.entity.o_j_code + row.entity.od_branch_cd">' + //j_code + branch
                '<input type="hidden" name="search_delivery_date_from" value="">' +
                '<input type="hidden" name="search_delivery_date_to" value="">';
                break;
            case 'o_is_ad_invoice_issue':
            case 'o_is_invoice_issue':
                var action = '/_admin/invoice/show';
                var hidden = '<input type="hidden" name="s_code" ng-value="row.entity.c_s_code">'; //s_code
                break;
            case 'o_is_order_input':
            case 'o_is_acceptance_input':
                var action = '/_admin/order_acceptance/show';
                var hidden = '<input type="hidden" name="from_jcode_branch" ng-value="row.entity.o_j_code + row.entity.od_branch_cd">' +
                             '<input type="hidden" name="to_jcode_branch" ng-value="row.entity.o_j_code + row.entity.od_branch_cd">'; //j_code + branch
                break;
        }

        switch (colName) {
            case 'o_is_ad_sales_slip_input':
            case 'o_is_ad_receive_payment_sales_input':
            case 'o_is_sales_slip_input':
                if (can_invoices == 'all') {
                    out += '<div class="ui-grid-cell-contents">';
//#7066: Start
//                out += '<div class="ui-grid-cell-contents" ng-if="row.entity.o_account_id != grid.appScope.my_account.id">－</div>';
//#7194:Start
                  out += '<div ng-if="row.entity.o_account_id != grid.appScope.my_account.id"><div class="a-padding-left" ng-if="1">{{row.entity.' + colName + ' | mapYesNo}}</div></div>';
                  out += '<div ng-if="row.entity.o_account_id == grid.appScope.my_account.id">';
//#7194:End
              } else {
//#7194:Start
                  // out += '<div class="ui-grid-cell-contents">';
//#7194:End
//#7066: End
                  ng = true;
                }
                break;

            case 'o_is_order_input':
            case 'o_is_acceptance_input':
                if (can_acceptance) {
                    out +=  '<div class="ui-grid-cell-contents">';
                } else {
                    ng = true;
                }
                break;

            case 'o_is_ad_invoice_issue':
            case 'o_is_invoice_issue':
                if (can_invoice_issue) {
                    out +=  '<div class="ui-grid-cell-contents">';
                } else {
                    ng = true;
                }
                break;
        }

        if (ng) {
            // out +=  '<form action="' + action + '" method="post">' + hidden;
            if(colName=='o_is_order_input' ) {
                // 「発注・検収-発注検収力・変更」権限有り：「未」「済」でリンクできてOK
                if(order_acceptance_input_change) {
                    // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-value="row.entity.' + colName + ' | mapYesNo">';
//#7194:Start
                    out += '<div class="ui-grid-cell-contents">';
//#7194:End
                    out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7194:Start
                    out += '</div>';
//#7194:End
                }
                // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限有り：「済」だけリンクできてOK
                if(!order_acceptance_input_change && order_acceptance_view) {
                    // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="row.entity.o_is_order_input== 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
//#7194:Start
                    out += '<div class="ui-grid-cell-contents">';
//#7194:End
                    out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7194:Start
                    out += '</div>';
//#7194:End
                }
                // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限無し：リンクできてない
                if(!order_acceptance_input_change && !order_acceptance_view) {
//#7194:Start
                    out += '<div class="ui-grid-cell-contents">';
//#7194:End
                    out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7194:Start
                    out += '</div>';
//#7194:End

                }
            } else if(colName=='o_is_acceptance_input') {
                // 「発注・検収-発注検収力・変更」権限有り：「未」「済」でリンクできてOK
                if(order_acceptance_input_change) {
                    //out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-value="row.entity.' + colName + ' | mapYesNo">';
//#7194:Start
                    out += '<div class="ui-grid-cell-contents">';
//#7194:End
                    out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7194:Start
                    out += '</div>';
//#7194:End
                }
                // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限有り：「済」だけリンクできてOK
                if(!order_acceptance_input_change && order_acceptance_view) {
                    // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="row.entity.o_is_acceptance_input== 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
//#7194:Start
                    out += '<div class="ui-grid-cell-contents">';
//#7194:End
                    out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7194:Start
                    out += '</div>';
//#7194:End
                }
                // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限無し：リンクできてない
                if(!order_acceptance_input_change & !order_acceptance_view) {
//#7194:Start
                    out += '<div class="ui-grid-cell-contents">';
//#7194:End
                    out += '<div class="a-padding-left" ng-if="1">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7194:Start
                    out += '</div>';
//#7194:End
                }
            } else {
//#7066: Start
//              out += '<div class="ui-grid-cell-contents">－</div>';
//#7194:Start
              out += '<div class="ui-grid-cell-contents a-padding-left" ng-if="1">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7194:End
//#7066: End
            }
            // out += '</form>';
        } else {
            // out +=  '<form action="' + action + '" method="post">' + hidden;

            if (colName != 'o_is_ad_invoice_issue' &&  colName != 'o_is_invoice_issue') {
                if(colName=='o_is_order_input' ) {
                    // 「発注・検収-発注検収力・変更」権限有り：「未」「済」でリンクできてOK
                    if(order_acceptance_input_change) {
                        // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-value="row.entity.' + colName + ' | mapYesNo">';
                        out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                    }
                    // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限有り：「済」だけリンクできてOK
                    if(!order_acceptance_input_change && order_acceptance_view) {
                        // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="row.entity.o_is_order_input== 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                        out += '<div class="a-padding-left" >{{row.entity.' + colName + ' | mapYesNo}}</div>';
                    }
                    // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限無し：リンクできてない
                    if(!order_acceptance_input_change && !order_acceptance_view) {
                        out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                    }
                } else if(colName=='o_is_acceptance_input') {
                    // 「発注・検収-発注検収力・変更」権限有り：「未」「済」でリンクできてOK
                    if(order_acceptance_input_change) {
                        // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-value="row.entity.' + colName + ' | mapYesNo">';
                        out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                    }
                    // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限有り：「済」だけリンクできてOK
                    if(!order_acceptance_input_change && order_acceptance_view) {
                        // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="row.entity.o_is_acceptance_input== 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                        out += '<div class="a-padding-left" >{{row.entity.' + colName + ' | mapYesNo}}</div>';
                    }
                    // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限無し：リンクできてない
                    if(!order_acceptance_input_change & !order_acceptance_view) {
                        out += '<div class="a-padding-left" ng-if="1">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                    }
                } else {
                    // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="row.entity.od_order_status != 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                    out += '<div class="a-padding-left" >{{row.entity.' + colName + ' | mapYesNo}}</div>';
                }
            } else {
                if (colName == 'o_is_ad_invoice_issue') {
                    // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="row.entity.od_order_status != 1 && row.entity.o_is_ad_invoice_issue != 0" ng-value="row.entity.' + colName + ' | mapYesNo">';
//#7194:Start - add class ui-grid-cell-contents
                    out += '<div class="a-padding-left" ng-if="(row.entity.o_is_ad_invoice_issue == 0) || (row.entity.od_order_status != 1 && row.entity.o_is_ad_invoice_issue != 0)">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7194:End
                } else {
                    // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="row.entity.od_order_status != 1 && row.entity.o_is_invoice_issue != 0" ng-value="row.entity.' + colName + ' | mapYesNo">';
//#7194:Start
                    out += '<div class="a-padding-left" ng-if="(row.entity.o_is_invoice_issue == 0) || (row.entity.od_order_status != 1 && row.entity.o_is_invoice_issue != 0)">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7194:End
                }
            }

            // out += '</form>' + '</div>';
            out += '</div>';
        }
//#7194:Start
        if (colName=='o_is_ad_receive_payment_sales_input' || colName == 'o_is_ad_invoice_issue') {
//#7194:end
            out += '</div>';
        }


        /*console.log("Colname:");
        console.log(colName);
        console.log("Out:");
        console.log(out);*/

        return out;
    }

    $scope.change_color = function(row) {

        if (row.entity.order_detail_log_id) {
            // console.log("have detail log_id:");
            return {
                "background-color": row.entity.od_shift_color_cd
            }
        }
    }

    $scope.change_color_when_change = function(row) {
        if (row.entity.is_change_price) {
            return {
                "background-color": "#008000",
                "color": "white"
            }
        }
    }
    function get_tax_rate() {
        $http({
            url : '/_admin/consumption_tax/get_tax_rate',
            method : 'POST',
            data : {
                tax_type : 1
            }
        }).success(function(data) {
            $rootScope.tax_rates = data;
        });
    }

    //#6959:Start
    // var resizeTimer = null;
    //#6959:End

    $(window).resize(function(e) {
        /*#6959:Start
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function() {
        $('#dialogAdd .grid').width($('#dialogAdd').width());
        $(window).trigger('resize');
        #6959:End*/
        set_td_width('.table-equal');
        //#6959:Start
        //}, 500);
        //#6959:End
    });

    function set_td_width(table_name) {
        var table_simple = $('.ui-dialog-content .table-equal');
        var table_width = table_simple.outerWidth();

        var th_max = 0;
        var th_no = null;

        $('.ui-dialog-content ' + table_name + ' tr').each(function(index) {
            if ($(this).find('th').length > th_max) {
                th_no = index;
                th_max = $(this).find('th').length;
            }
        });

        $('.ui-dialog-content ' + table_name + ' tr:eq(' + th_no + ') td').removeAttr('width');

        var th_width_sum = 0;

        $('.ui-dialog-content ' + table_name + ' tr:eq(' + th_no + ') th').each(function() {
            th_width_sum += $(this).outerWidth();
        });

        var td_no = $('.ui-dialog-content ' + table_name + ' tr:eq(' + th_no + ')').find('td').length;

        var td_width = (table_width - th_width_sum) / td_no;

        $('.ui-dialog-content ' + table_name + ' tr:eq(' + th_no + ') td').attr('width', td_width);
    }

    $rootScope.loadDetail = function (order_id, from_finish_work_report_screen) {
        /* start volyminhnhan@gmail.com modification*/
        if (typeof $rootScope.tax_rates == 'undefined') {
            get_tax_rate();
        }

        $http({
             url : '/_admin/order/change_repo_approve_reject',
             method : 'POST',
             data : {
                 order_id : order_id
             }
         }).success(function(data) {
             $('#dialog').dialog('option', 'title', "詳細");
             $("#dialog").dialog( "option", "height", "auto");
             $("#dialog").dialog( "option", "width", "80%" );
             $('#dialog').dialog("option", "position", {my: "center", at: "center", of: window});
             $('#dialog').dialog("open");

             $(window).trigger('resize');

             set_td_width('.table-equal');

             // Get data detail order load to grid
              //$scope.gridOptionsDetail.data = data;
             $scope.gridOptionsDetail.data = data.list_grid_detail;
             // Get detail order
              //$scope.order = $scope.gridOptionsDetail.data[0];
             $scope.order = data.master_information;

             $scope.order.o_order_regist_date = $scope.order.o_order_regist_date.replace(/-/g, '/');
             $scope.order.o_order_approval_date = $scope.order.o_order_approval_date.replace(/-/g, '/');

             if($scope.order.o_is_ad_receive_payment == 1) {
                 $scope.order.payment_terms = "前受金";
             }
             else {
                 $scope.order.payment_terms = "";
                 if($scope.order.closing_date_name!=null) {
                     $scope.order.payment_terms+="締め日：" + $scope.order.closing_date_name+"日";
                 } else {
                     $scope.order.payment_terms+="締め日： 日";
                 }
                 if($scope.order.payment_month_cd_name!=null){
                     $scope.order.payment_terms+="　支払月：" + $scope.order.payment_month_cd_name+"月";
                 } else {
                     $scope.order.payment_terms+="　支払月： 月";
                 }
                 if($scope.order.payment_day_cd_name!=null){
                     $scope.order.payment_terms+= "　支払日：" + $scope.order.payment_day_cd_name+"日";
                 } else {
                     $scope.order.payment_terms+= "　支払日： 日";
                 }
             }

             /*
             UNAPPROVED = 1;
             UNDETERMINED = 2;
             CONFIRMED = 3;
             CANCELLED = 4;
             REJECTED = 9;
             */

             $scope.is_show_edit = false;
             $scope.is_show_change_report = false;
             $scope.is_show_copy_order = false;
             $scope.is_show_print = false;
             $is_unapproved = false;
             $is_undetermined = false;
             $is_confirmed = false;
             $is_change_report_apply = false;
             // Show/hide button approve & change report approve
             $scope.status_for_approve_decision = 0;
             $scope.is_show_change_report_approve = false;
             $scope.is_show_approve = false;
             $scope.isCanEdit = true;

             angular.forEach($scope.gridOptionsDetail.data , function(value) {
                 if(!$is_unapproved &&(value.od_order_status == UNAPPROVED)) {
                     $is_unapproved = true;
                 }
                 //tmp#6841: Start 2015/09/24
                 if ((typeof value.isCanEdit != 'undefined') && (value.isCanEdit == false)) {
                     $scope.isCanEdit = false;
                 }
                 //tmp#6841: End
                 if(!$is_undetermined &&(value.od_order_status == UNDETERMINED)) {
                     $is_undetermined = true;
                 }

                 if(!$is_confirmed &&(value.od_order_status == CONFIRMED)) {
                     $is_confirmed = true;
                 }

                 if(!$is_change_report_apply &&(value.od_is_change_report_apply == 1)) {
                     $is_change_report_apply = true;
                 }
                 // For approve button show/hide
                 if(value.od_order_status == UNDETERMINED || value.od_order_status == CONFIRMED) {
                     $scope.status_for_approve_decision = 2;
                 }
                 else if(value.od_order_status == UNAPPROVED) {
                     $scope.status_for_approve_decision = 1;
                 }
             });

             if(can_approve && ($scope.status_for_approve_decision == 1) && checkJAccount($scope.order.o_account_id)) {
                 // Show btn approve
                 $scope.is_show_approve = true;
             }
             if(can_approve && ($scope.status_for_approve_decision == 2) && $is_change_report_apply && checkJAccount($scope.order.o_account_id)) {
                 // Show btn change report approve
                 $scope.is_show_change_report_approve = true;
             }

             if ($is_unapproved
                 && ((can_edit == 2) || ((can_edit == 1) && ($scope.order.o_account_id == my_account.id)))
             ) {
                 $scope.is_show_edit = true;
                 $scope.is_show_print = true;
             }

             if (($is_undetermined || $is_confirmed)
                 && ((can_edit == 2) || ((can_edit == 1) && ($scope.order.o_account_id == my_account.id)))
             ) {
                 $scope.is_show_change_report = true;
                 $scope.is_show_print = true;
             }

             function checkJAccount(j_account_check) {
                 result_j_account_check = false;
                 angular.forEach(j_accounts, function (item) {
                     //debugger;
                     if (j_account_check == item.id) {
                         result_j_account_check = true;
                     }
                 });
                 return result_j_account_check;
             }

             function checkJAccountExt(j_account_check) {
                 result_j_account_check = false;
                 if (typeof from_finish_work_report_screen === "undefined" && from_finish_work_report_screen == true)
                	 return result_j_account_check;

                 angular.forEach(j_accounts_ext, function (item) {
                     //debugger;
                     if (j_account_check == item.id) {
                         result_j_account_check = true;
                     }
                 });
                 return result_j_account_check;
             }
             // Re-discuss to determine this below:

             if (($is_undetermined || $is_confirmed) && can_edit==2) {
                 $scope.is_show_change_report = true;
                 $scope.is_show_print = true;
             } else if (($is_undetermined || $is_confirmed) && can_edit==1 && $scope.order.o_account_id==my_account.id) {
                 $scope.is_show_change_report = true;
                 $scope.is_show_print = true;
             }
//#7427:Start
             if ($scope.order.client_status != CLIENT_STATUS_RE_CREDIT_WAITING) {
                 if (can_edit == 2) {
                     $scope.is_show_copy_order = true;
                 } else if (can_edit == 1) {
                     var jAccountValue = checkJAccount($scope.order.o_account_id);
                     if (jAccountValue == false) {
                         jAccountValue = checkJAccountExt($scope.order.o_account_id);
                     }
                     $scope.is_show_copy_order = jAccountValue;
                 }
             }
//#7427:End
             //alert($scope.order.o_account_id);
             if (!checkJAccount($scope.order.o_account_id)) {
                 $scope.is_show_edit = false;
                 $scope.is_show_change_report = false;
                 $scope.is_show_print = false;
                 // $scope.is_show_approve = false;
                 //$scope.is_show_copy_order = false;
             }

             // $scope.waiting_change_report = false;
             angular.forEach($scope.gridOptionsDetail.data , function(value) {
                 if($scope.is_show_change_report && value.od_is_change_report_apply == 1) {
                     $scope.is_show_change_report = false;
                 }
             });

             // Check which grid to show: view normally or changed report view
             // Add 26/7/2015 - Phong
             $scope.changed_report_view = false;
             $scope.check_status = false;
             $scope.check_is_change_report_apply = false;
             angular.forEach($scope.gridOptionsDetail.data , function(value) {
                 if(value.od_order_status == 2 || value.od_order_status == 2) {
                     $scope.check_status = true;
                 }
                 if(value.od_is_change_report_apply == 1) {
                     $scope.check_is_change_report_apply = true;
                 }
             });
             if($scope.check_status && $scope.check_is_change_report_apply) {
                 $scope.changed_report_view = true;
             }
             // End 26/7/2015 - Phong
             // if ($scope.order.od_is_change_report_apply == 1) {
             // if(!$scope.is_show_change_report) {
             // console.log("changed_report_view:");
             // console.log($scope.changed_report_view);
             if(data.type_of_changed_report_view) {
                 // console.log("changed_report_view: view changed....");
                 // If has changed => not change anymore
                 // $scope.is_show_change_report = false;
                 $scope.gridOptionsDetail.columnDefs = [
                                                        {
                                                           name: 'od_branch_cd',
                                                           displayName: jsGlobals[0],
                                                           width: 50
                                                        },
                                                        // 前売伝
                                                        { name: 'o_is_ad_sales_slip_input',
                                                            displayName: jsGlobals[19],
                                                            width: 60,
                                                            // cellFilter : 'mapYesNo'
                                                            cellTemplate: ext_link('o_is_ad_sales_slip_input')
                                                        },
                                                        // 前請求
                                                        { name: 'o_is_ad_invoice_issue',
                                                            displayName: jsGlobals[20],
                                                            width: 60,
                                                            // cellFilter : 'mapYesNo'
                                                            cellTemplate: ext_link('o_is_ad_invoice_issue')
                                                       },
                                                        { name: 'o_is_order_input',
                                                           displayName: jsGlobals[21],
                                                           width: 50,
                                                           // cellFilter : 'mapYesNo'
                                                           cellTemplate: ext_link('o_is_order_input')
                                                       },
                                                        { name: 'o_is_acceptance_input',
                                                           displayName: jsGlobals[22],
                                                           width: 50,
                                                           // cellFilter : 'mapYesNo'
                                                           cellTemplate: ext_link('o_is_acceptance_input')
                                                       },
                                                        { name: 'o_is_sales_slip_input',
                                                           displayName: jsGlobals[23],
                                                           width: 50,
                                                           // cellFilter : 'mapYesNo'
                                                           cellTemplate: ext_link('o_is_sales_slip_input')
                                                       },
                                                        { name: 'o_is_invoice_issue',
                                                           displayName: jsGlobals[24],
                                                           width: 50,
                                                           // cellFilter : 'mapYesNo'
                                                           cellTemplate: ext_link('o_is_invoice_issue')
                                                       },
                                                        {
                                                           name: 'od_order_status',
                                                           displayName: jsGlobals[1],
                                                           width: 100,
                                                           visible: false
                                                        },
                                                        {
                                                            name: 'od_order_status_name',
                                                            displayName: jsGlobals[1],
                                                            width: 100,
                                                            cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.od_is_change_report_apply == 1 ? "変レポ申請中" : row.entity.od_order_status_name}}</div>'
                                                        },
                                                        {
                                                           name: 'change_report',
                                                           displayName: jsGlobals[2],
                                                           width: 100,
//#7058:Start
                                                           // cellTemplate: '<div class="cell-template-data" ng-style="grid.appScope.change_color(row)">{{row.entity.change_report}}</div>',
                                                           cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                                return $rootScope.check_value('change_report', row.entity);
                                                           }
//#7058:End
                                                        },
                                                        {
                                                           name: 'p_name',
                                                           displayName: jsGlobals[3],
                                                           width: 100
                                                        },
                                                        {
                                                           name: 'od_contents',
                                                           displayName: jsGlobals[4],
                                                           width: 100
                                                        },
                                                        {
                                                           name: 'od_delivery_date',
                                                           displayName: jsGlobals[5],
                                                           width: 100,
                                                           // cellTemplate: '<div class="cell-template-data" ng-style="grid.appScope.change_color(row)">{{row.entity.od_delivery_date}}</div>',
                                                           cellFilter: 'formatDate',
//#7058:Start
                                                           cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                                return $rootScope.check_value('od_delivery_date', row.entity);
                                                           }
//#7058:End
                                                        },
                                                        {
                                                           name: 'od_billing_date',
                                                           displayName: jsGlobals[6],
                                                           width: 100,
                                                           // cellTemplate: '<div class="cell-template-data" ng-style="grid.appScope.change_color(row)">{{row.entity.od_billing_date}}</div>',
                                                           cellFilter: 'formatDate',
//#7058:Start
                                                           cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                                return $rootScope.check_value('od_billing_date', row.entity);
                                                           }
//#7058:End
                                                        },
                                                        {
                                                           name: 'od_payment_date',
                                                           displayName: jsGlobals[7],
                                                           width: 100,
                                                           // cellTemplate: '<div class="cell-template-data" ng-style="grid.appScope.change_color(row)">{{row.entity.od_payment_date}}</div>',
                                                           cellFilter: 'formatDate',
//#7058:Start
                                                           cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                                return $rootScope.check_value('od_payment_date', row.entity);
                                                           }
//#7058:End
                                                        },
                                                        { name: 'od_is_cancel_request', width: 100, visible: false },
                                                        { name: 'od_shift_color_cd', width: 100, visible: false },
                                                        {
                                                           name: 'od_quantity',
                                                           width: 100,
                                                           displayName: jsGlobals[8],
                                                           cellFilter: 'number:0'
                                                        },
                                                        {
                                                           name: 'od_tax_type',
                                                           width: 50,
                                                           cellFilter: 'mapTaxType',
                                                           displayName: jsGlobals[9]
                                                        },
                                                        {
                                                           name: 'od_unit_price',
                                                           width: 100,
                                                           displayName: jsGlobals[10],
                                                           cellFilter: 'number:2',
                                                           cellClass: 'text-right'
                                                        },
                                                        {
                                                           name: 'od_amount',
                                                           width: 100,
                                                           displayName: jsGlobals[11],
                                                           cellFilter: 'number:0',
                                                           cellClass: 'text-right'
                                                        },
                                                        {
                                                           name: 'od_cost_unit_price',
                                                           width: 100,
                                                           displayName: jsGlobals[12],
                                                           cellFilter: 'number:2',
//#7085:Start
                                                           // cellTemplate: '<div class="ui-grid-cell-contents" ng-style="grid.appScope.change_color_when_change(row)">{{row.entity.od_cost_unit_price}}</div>',
//#7085:End
                                                           // cellClass: 'text-right'
                                                           cellClass:
                                                                function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                                    return $rootScope.check_value('od_cost_unit_price', row.entity);
                                                                }
                                                        },
                                                        {
                                                           name: 'od_cost_total_price',
                                                           width: 100,
                                                           displayName: jsGlobals[13],
                                                           cellFilter: 'number:0',
//#7085:Start
                                                           // cellTemplate: '<div class="ui-grid-cell-contents" ng-style="grid.appScope.change_color_when_change(row)">{{row.entity.od_cost_total_price | number}}</div>',
//#7085:End
                                                           // cellClass: 'text-right'
                                                           cellClass:
                                                                function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                                    return $rootScope.check_value('od_cost_total_price', row.entity);
                                                                }
                                                        },
                                                        {
                                                            name: 'od_gross_profit_amount',
                                                            width: 100,
                                                            displayName: jsGlobals[14],
                                                            cellFilter: 'number:0',
//#7085:Start
                                                            // cellTemplate: '<div class="ui-grid-cell-contents" ng-style="grid.appScope.change_color_when_change(row)">{{row.entity.od_gross_profit_amount | number}}</div>',
//#7085:End
//#7390:Start
//#7085:Start
                                                            cellClass:
                                                                function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                                    return $rootScope.check_value('od_gross_profit_amount', row.entity);
                                                                }
                                                        },
//#7085:End
//#7390:End
                                                        {
                                                             name: 'od_gross_profit_rate',
                                                             width: 100,
                                                             displayName: jsGlobals[15],
//#7085:Start
                                                             // cellTemplate: '<div class="ui-grid-cell-contents" ng-style="grid.appScope.change_color_when_change(row)">{{row.entity.od_gross_profit_rate}}</div>',
//#7085:End
//#7390:Start
//#7085:Start
                                                             cellClass:
                                                                function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                                    return $rootScope.check_value('od_gross_profit_amount', row.entity);
                                                                }
//#7085:End
                                                        }
                                                        ];
//#7390:End
                   angular.forEach(data , function(value, key) {
                       value.change_report = '-';
                   });
             } // End if ($scope.order.o_is_change_report_apply == 1) {
             else {
                 $scope.gridOptionsDetail.columnDefs = [
                                                        { name: 'od_branch_cd', displayName: jsGlobals[0], width: 50},
                                                        { name: 'o_is_ad_receive_payment_sales_input',
                                                            displayName: jsGlobals[19],
                                                            width: 60,
                                                            // cellFilter : 'mapYesNo'
                                                            cellTemplate: ext_link('o_is_ad_receive_payment_sales_input')
                                                       },
                                                        { name: 'o_is_ad_invoice_issue',
                                                           displayName: jsGlobals[20],
                                                           width: 60,
                                                           // cellFilter : 'mapYesNo'
                                                           cellTemplate: ext_link('o_is_ad_invoice_issue')
                                                       },
                                                        { name: 'o_is_order_input',
                                                           displayName: jsGlobals[21],
                                                           width: 50,
                                                           // cellFilter : 'mapYesNo'
                                                           cellTemplate: ext_link('o_is_order_input')
                                                       },
                                                        { name: 'o_is_acceptance_input',
                                                           displayName: jsGlobals[22],
                                                           width: 50,
                                                           // cellFilter : 'mapYesNo'
                                                           cellTemplate: ext_link('o_is_acceptance_input')
                                                       },
                                                        { name: 'o_is_sales_slip_input',
                                                           displayName: jsGlobals[23],
                                                           width: 50,
                                                           // cellFilter : 'mapYesNo'
                                                           cellTemplate: ext_link('o_is_sales_slip_input')
                                                       },
                                                        { name: 'o_is_invoice_issue',
                                                           displayName: jsGlobals[24],
                                                           width: 50,
                                                           // cellFilter : 'mapYesNo'
                                                           cellTemplate: ext_link('o_is_invoice_issue')
                                                       },
                                                        { name: 'o_account_id', displayName: jsGlobals[18], width: 100, visible: false},
                                                        /*{ name: 'a_name', displayName: jsGlobals[18], width: 100},
                                                        { name: 'c_s_code', displayName: jsGlobals[17], width: 100},
                                                        { name: 'c_name', displayName: jsGlobals[25], width: 100},*/
                                                        { name: 'od_order_status', displayName: jsGlobals[1], width: 100, visible: false},
                                                        { name: 'od_order_status_name', displayName: jsGlobals[1], width: 100,
                                                            cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.od_is_change_report_apply == 1 ? "変レポ申請中" : row.entity.od_order_status_name}}</div>'
                                                        },
                                                        { name: 'p_name', displayName: jsGlobals[3], width: 100},
                                                        { name: 'od_contents', displayName: jsGlobals[26], width: 100},
                                                        { name: 'od_delivery_date', displayName: jsGlobals[5], width: 100, cellFilter: 'formatDate'},
                                                        { name: 'od_billing_date', displayName: jsGlobals[6], width: 100, cellFilter: 'formatDate'},
                                                        { name: 'od_payment_date', displayName: jsGlobals[7], width: 100, cellFilter: 'formatDate'},
                                                        { name: 'od_quantity', displayName: jsGlobals[8], width: 100, cellFilter: 'number:0'},
                                                        { name: 'od_tax_type', cellFilter: 'mapTaxType', displayName: jsGlobals[9], width: 50},
                                                        { name: 'od_unit_price', displayName: jsGlobals[10], width: 100, cellFilter: 'number:2', cellClass: 'a-right'},
                                                        { name: 'od_amount', displayName: jsGlobals[11], width: 100, cellFilter: 'number:0', cellClass: 'a-right'},
                                                        { name: 'od_cost_unit_price', displayName: jsGlobals[12], width: 100, cellFilter: 'number:2', cellClass: 'a-right'},
                                                        { name: 'od_cost_total_price', displayName: jsGlobals[13], width: 100, cellFilter: 'number:0', cellClass: 'a-right'},
                                                        { name: 'od_gross_profit_amount', displayName: jsGlobals[14], width: 100, cellFilter: 'number:0',
//#7390:Start
                                                            cellClass:
                                                                function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                                    if (grid.getCellValue(row ,col) < 0) {
                                                                        return 'text-right red';
                                                                    }
                                                                    return 'text-right';
                                                                }

                                                        },
                                                        { name: 'od_gross_profit_rate', displayName: jsGlobals[15], width: 100,

                                                            cellClass:
                                                                function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                                    var gross_profit_rate = grid.getCellValue(row ,col).replace('%', '');
                                                                    if (gross_profit_rate < 0) {
                                                                            return 'text-right red';
                                                                        }
                                                                    return 'text-right';
                                                                }
                                                        },
                                                      ];
//#7390:End
             } // End else {

             // 税別合計
             $scope.order.o_cost_total_price = 0;
             // 消費税
             $scope.order.o_consumption_tax = 0;
             // 税込合計
             $scope.order.o_tax_total = 0;
             // 粗利合計
             $scope.order.o_gross_profit_total = 0;
             // Loop all items in detail part when change something related
             angular.forEach($scope.gridOptionsDetail.data, function(item) {
                 // console.log("ITEM:");
                 // console.log(item);
                 // console.log(item.order_detail_log_id);
                 // Log => Calculate
                 // Not log => No has same branch in log => Calculate
//#8216:Start
//            	 if((((item.order_detail_log_id) || (isNaN(item.order_detail_log_id) && !check_has_order_detail_log(item.od_detail_id, item.od_branch_cd)))) && (item.od_is_cancel_request!=1)) {
                 if((((item.order_detail_log_id) || (isNaN(item.order_detail_log_id) && !check_has_order_detail_log(item.od_detail_id, item.od_branch_cd)))) && (item.od_is_cancel_request!=1) && (item.od_order_status != REJECTED) && (item.od_order_status != CANCELLED)) {
//#8216:Start
                     var tax_rate;
                     var delivery_date = new Date(item.od_delivery_date);
                     var arr = jQuery.grep($rootScope.tax_rates, function( i ) {
                         var da = new Date(i.adaptation_date);
                         delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                         da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                         return da <= delivery_date;
                     });
                     var tax_rates = [];
                     $.each(arr, function(key, value){
                         tax_rates.push(value.rate);
                     });
                     tax_rate = Math.max.apply(Math, tax_rates);
                     // End get tax_rate
                     // Order detail amount (=price*quantity)
                     item.od_amount = parseInt(item.od_amount);

                     // Only tax type is 「内」 has the tax included in amount
                     /*var no_tax_amount = (item.od_tax_type != 2) ? item.od_amount : (item.od_amount / (1 + tax_rate/100));
                     $scope.order.o_cost_total_price += no_tax_amount;
                     // Only tax type is 「非」 is nontaxable, means no tax needed for the product price
                     $scope.order.o_consumption_tax += (item.od_tax_type != 3) ? (no_tax_amount * (tax_rate / 100)) : 0;
                     // Gross profit total += gross profit amount
                     item.od_gross_profit_amount = parseInt(item.od_gross_profit_amount);
                     $scope.order.o_gross_profit_total += item.od_gross_profit_amount;*/
                     if(item.od_tax_type == 2) {
                         // (②/(1+税率)+小数点以下切り上げ)
                         $scope.order.o_cost_total_price += f_ceil(item.od_amount/(1+tax_rate/100));
                         // (②-(②/(1+税率)+小数点以下切り上げ))
                         $scope.order.o_consumption_tax += (item.od_amount - f_ceil(item.od_amount/(1+tax_rate/100)));
                         // ②+③
                         $scope.order.o_tax_total+=item.od_amount;
                     }
                     if(item.od_tax_type == 1 ) {
                         // (①*税率+小数点以下切り捨て)
                         $scope.order.o_consumption_tax += f_floor(item.od_amount*tax_rate/100);
                         // ①+③
                         $scope.order.o_cost_total_price += item.od_amount;
                         // ①*(1+税率)+小数点以下切り捨て
                         $scope.order.o_tax_total += f_floor(item.od_amount*(1+tax_rate/100));
                     }
                     if(item.od_tax_type == 3) {
                         // ①+③
                         $scope.order.o_cost_total_price += item.od_amount;
                         // ②+③
                         $scope.order.o_tax_total+=item.od_amount;
                     }
                     $scope.order.o_gross_profit_total += parseInt(item.od_gross_profit_amount);
                     // console.log("od_gross_profit_amount:" + item.od_gross_profit_amount);
                 }
                 item.od_gross_profit_rate =  parseFloat(item.od_gross_profit_rate).toFixed(2) + "%";
             }); // End angular.forEach($scope.gridOptionsD
             //$scope.order.o_cost_total_price = Math.ceil($scope.order.o_cost_total_price);
             //$scope.order.o_consumption_tax = Math.floor($scope.order.o_consumption_tax);
             //$scope.order.o_tax_total = $scope.order.o_cost_total_price + $scope.order.o_consumption_tax;
         });
      /* end volyminhnhan@gmail.com modification*/
    };

    // Check if one order detail has been editted or not (saved in log table)
    function check_has_order_detail_log(od_id_, od_branch_cd_) {
        // console.log("od_branch_cd_:"+od_branch_cd_);
        var result = false;
        angular.forEach($scope.gridOptionsDetail.data, function(item) {
            // From log & = branch
            if((item.order_detail_log_id) && (item.od_branch_cd == od_branch_cd_)) {
                // console.log("+++++item.od_branch_cd_:"+item.od_branch_cd);
                result = true;
            }
        });
        return result;
    }

    // Go to approve/reject (normally) screen
    $scope.approveReject = function() {
       $rootScope.loadTemplate('approve_reject', $scope.gridOptionsDetail.data, $scope.order);
        //$rootScope.loadApproveReject($scope.gridOptionsDetail.data, $scope.order );
 
    };

    // Go to approve/reject (after changed report) screen
    $scope.changeReportApproveReject = function() {
         $rootScope.loadTemplate('change_report', $scope.order.o_id);
        //$rootScope.loadChangeReport($scope.order.o_id);
    };

    // Close this screen
    $scope.close = function() {
        $('#dialog').dialog("close");
    }

    // Copy this order to another screen
    $scope.copy = function() {
        $('#dialog').dialog("close");
        $rootScope.loadTemplate($scope.order.o_id);
    }
}])
.filter('formatDate', function() { //#6853: Start (3 files refer to this function: approve_reject.js, change_repo.js, detail.js)
    return function(input) {
        return input.replace(/-/g, '/');
    };
}); //#6853: End
