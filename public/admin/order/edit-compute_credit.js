app.controller('EditCtrl', ['$rootScope', '$scope', '$http', '$q', '$interval', 'uiGridConstants', function ($rootScope, $scope, $http, $q, $interval, uiGridConstants) {
    $scope.gridOptionsEdit = {
        selectionRowHeaderWidth: 35,
        paginationPageSizes: [20, 30, 50],
        paginationPageSize: 20,
        rowEditWaitInterval: 1,
        rowTemplate:
           '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ui-grid-cell ng-class="{\'gray-row\' : (row.entity.od_is_cancel_request == 1)}" ></div>'
    };

    $scope.gridOptionsEdit.enableCellEditOnFocus = false;
    $scope.gridOptionsEdit.multiSelect = true;
    $scope.is_submitted = false;
    $scope.isShowAddButton = false;
    $scope.isShowEditButtonSection = false;
    $scope.isShowChangeReportButtonSection = false;
    $scope.isShowAdReceivePaymentSalesInput = false;

    $scope.show_approved_cell = function(row) {
        if (row.entity.od_order_status == 1 && row.entity.od_branch_cd == 1) {
            return true;
        }
        return false;
    }
    $scope.show_approved_column = function() {
        return true;
    }

    $scope.showCancelBranch = function(row) {
        if (row.o_is_ad_receive_payment_sales_input == 1
                || row.o_is_ad_receive_payment == 1
                || row.o_is_order_input == 1
                || row.o_is_sales_slip_input == 1
                || row.o_is_invoice_issue == 1
                || row.o_is_acceptance_input == 1
            ) {
            return false;
        }

        if (row.od_is_cancel_request != 1
                && row.isCanEdit
                && (row.od_order_status == 2 || row.od_order_status == 3)
        )
            return true;

        return false;
    } // End $scope.showCancelBranch = function(row)
    $scope.showRevertCancelBranch = function(row) {
        if (row.od_is_cancel_request == 1 && row.isCanEdit)
            return true;
    }

    $scope.parse_date = function() {
        if (parse_date($scope.order.o_order_form_input_date)) {
            $scope.order.o_order_form_input_date = parse_date($scope.order.o_order_form_input_date);
        }
    }

    $scope.change_color = function(row) {
        /*
        if ($scope.order.o_is_ad_receive_payment == 1) {
            return {
                "background-color": '#FFCC99',
            }
        } else {
            return {
                "background-color": '#fff',
            }
        }
        */
    }

    $scope.check_closest_closing_date = function(row) {
        var delivery_date = new Date(row.entity.od_delivery_date);

        if (delivery_date.getFullYear() < closest_year_closing_date) {
            return false;
        }

        if ((delivery_date.getFullYear() == closest_year_closing_date) && ((delivery_date.getMonth() + 1) <= closest_month_closing_date)) {
            return false;
        }

        return true;
    }

    // <!-------------------------------------------------END LOAD EDIT---------------------------------------------->
    $rootScope.loadEdit = function (order_id, form) {
        $scope.errors = '';
        $scope.is_submitted = false;
        $scope.error_success = '';
        $scope.order = {};
        $scope.gridOptionsEdit.data = [];
        $scope.gridOptionsEditDelete = [];

        $http({
            url : '/_admin/order/get_detail',
            method : 'POST',
            data : {
                order_id : order_id
            }
        }).success(function(data) {

            $("#closing_date_name").text(data[0].closing_date_name);
            $("#payment_month_cd_name").text(data[0].payment_month_cd_name);
            $("#payment_day_cd_name").text(data[0].payment_day_cd_name);

            $('#dialog').dialog("close");
            if (form == 'edit') {
                $('#dialogEdit').dialog('option', 'title', '編集');
            } else if (form == 'change_report') {
                $('#dialogEdit').dialog('option', 'title', '変レポ');
            }

            $("#dialogEdit").dialog( "option", "height", "auto");
            $("#dialogEdit").dialog( "option", "width", "80%" );
            $('#dialogEdit').dialog("open");
            $('#dialogEdit .grid').width($('#dialogEdit').width());
            $(window).trigger('resize');

            $scope.order = data[0];
            if ($scope.order.o_order_form_input_date == '0000-00-00') {
                $scope.order.o_order_form_input_date = '';
            } else {
                $scope.order.o_order_form_input_date = $scope.order.o_order_form_input_date.replace(/-/g, '/');
            }
            $scope.order.credit_amount_show = addCommas($scope.order.c_credit_amount);
            $scope.order.credit_amount = $scope.order.c_credit_amount;
            $scope.order.o_order_regist_date = $scope.order.o_order_regist_date.replace(/-/g, '/');
            $scope.branchs_old = angular.copy(data);
            if($scope.order.o_is_ad_receive_payment == 1) {
                $scope.order.payment_terms = "前受金";
            }
            else {
                $scope.order.payment_terms = "";
                if($scope.order.closing_date_name!=null) {
                    $scope.order.payment_terms+="締め日：" + $scope.order.closing_date_name+"日";
                } else {
                    $scope.order.payment_terms+="締め日： 日";
                }
                if($scope.order.payment_month_cd_name!=null){
                    $scope.order.payment_terms+="　支払月：" + $scope.order.payment_month_cd_name+"月";
                } else {
                    $scope.order.payment_terms+="　支払月： 月";
                }
                if($scope.order.payment_day_cd_name!=null){
                    $scope.order.payment_terms+= "　支払日：" + $scope.order.payment_day_cd_name+"日";
                } else {
                    $scope.order.payment_terms+= "　支払日： 日";
                }
            }

            $scope.order.o_tax_total = $scope.order.o_total_amount;
            $scope.order.o_consumption_tax = $scope.order.o_total_amount_tax;
            // od_order_status
            $scope.waiting_change_report = false;
            angular.forEach(data , function(value) {
                if(value.od_order_status==2 || value.od_order_status==3) {
                    $scope.waiting_change_report = true;
                }
            });

            // if ($scope.order.od_order_status == UNAPPROVED) {
                if (!$scope.waiting_change_report) {
                $scope.gridOptionsEdit.columnDefs = [
                                                         { name: '処理', displayName: jsGlobals[27], width: 165, enableCellEdit:false, cellTemplate:'<button type="button" class="btn primary btn-small btn-green txt-center" ng-click="grid.appScope.copyRow(row.entity, row)">'+jsGlobals[29]+'</button> '
                                                             + '<button type="button" class="btn primary btn-small btn-green btn-delete txt-center" ng-show="row.entity.isCanEdit && row.entity.od_order_status == 1" ng-click="grid.appScope.deleteRow(row.entity)">'+jsGlobals[30]+'</button>'},
                                                         { name: 'od_branch_cd', displayName: jsGlobals[0], width: 100, enableCellEdit:false},
                                                         { name: 'od_order_status', displayName: jsGlobals[1], width: 100, cellFilter : 'mapOrderStatus',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.is_new == 1) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             }
                                                         },
                                                         { name: 'p_id', displayName: jsGlobals[3], width: 100,
                                                             editableCellTemplate : 'ui-grid/dropdownEditor',
                                                             cellFilter : 'mapProduct',
                                                             editDropdownValueLabel : 'name',
                                                             editDropdownOptionsArray : products,
                                                             //cellClass: 'pink-row',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             }
                                                         },
                                                         { name: 'od_contents', displayName: jsGlobals[26], width: 100, editableCellTemplate : '<form name="inputForm"><input type="text" maxlength="30" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_contents\']" ui-grid-editor=""></form>',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="{\'background-color\': \'#FFCC99\'}">{{row.entity.od_contents}}</div>'
                                                         },
                                                         { name: 'od_delivery_date', displayName: jsGlobals[5], width: 100,
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="{\'background-color\': \'#FFCC99\'}">{{row.entity.od_delivery_date}}</div>'
                                                         },
                                                         { name: 'od_billing_date', displayName: jsGlobals[6], width: 100,
                                                             cellEditableCondition: function(scope){
                                                                 if ((scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) && ($scope.order.o_is_ad_receive_payment == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="grid.appScope.change_color(row)">{{row.entity.od_billing_date}}</div>'
                                                         },
                                                         { name: 'od_payment_date', displayName: jsGlobals[7], width: 100,
                                                             cellEditableCondition: function(scope){
                                                                 if ((scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) && ($scope.order.o_is_ad_receive_payment == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="grid.appScope.change_color(row)">{{row.entity.od_payment_date}}</div>'
                                                         },
                                                         { name: 'od_quantity', displayName: jsGlobals[8], width: 100, cellFilter: 'number:0', cellClass: 'a-right',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_quantity\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="{\'background-color\': \'#FFCC99\'}">{{row.entity.od_quantity}}</div>'
                                                         },
                                                         { name: 'od_tax_type', cellFilter: 'mapTaxType', editableCellTemplate: 'ui-grid/dropdownEditor', editDropdownIdLabel: 'code', editDropdownValueLabel: 'name', editDropdownOptionsArray: tax_division, displayName: jsGlobals[9], width: 100,
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             //cellClass: 'pink-row'
                                                         },
                                                         { name: 'od_unit_price', displayName: jsGlobals[10], width: 100, cellFilter: 'number:2', cellClass: 'a-right',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_unit_price\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="{\'background-color\': \'#FFCC99\'}">{{row.entity.od_unit_price}}</div>'
                                                         },
                                                         { name: 'od_amount()', displayName: jsGlobals[11], width: 100, enableCellEdit:false, cellFilter: 'number:0', cellClass: 'a-right'},
                                                         { name: 'od_cost_unit_price', displayName: jsGlobals[12], width: 100, cellFilter: 'number:2', cellClass: 'a-right',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_cost_unit_price\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="{\'background-color\': \'#FFCC99\'}">{{row.entity.od_cost_unit_price}}</div>'
                                                         },
                                                         { name: 'od_cost_total_price', displayName: jsGlobals[13], width: 100, cellFilter: 'number:0', cellClass: 'a-right',
                                                             cellEditableCondition: function(scope){
                                                                 if ((scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) && (scope.row.entity.od_cost_unit_price == 0)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_cost_total_price\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="row.entity.od_cost_unit_price == 0 && {\'background-color\': \'#FFCC99\'} || row.entity.od_cost_unit_price != 0 && {\'background-color\': \'#fff\'}">{{row.entity.od_cost_total_price}}</div>'
                                                         },
                                                         { name: 'od_gross_profit_amount()', displayName: jsGlobals[14], width: 100, cellFilter: 'number:0', cellClass: 'a-right', enableCellEdit: false},
                                                         { name: 'od_gross_profit_rate()', displayName: jsGlobals[15], width: 100, cellClass: 'a-right', enableCellEdit: false}
                                                       ];
            }
            //else if ($scope.order.od_order_status == UNDETERMINED || $scope.order.od_order_status == CONFIRMED) {
                else { // load grid for change report
                $scope.gridOptionsEdit.columnDefs = [
                                                         { name: '処理', displayName: jsGlobals[27], width: 155, enableCellEdit:false,
                                                             cellTemplate:'<button type="button" class="btn primary btn-small btn-green e-width" ng-show="grid.appScope.showCancelBranch(row.entity)" ng-click="grid.appScope.cancelBranch(row.entity)">'+jsGlobals[31]+'</button>'
                                                                         +'<button type="button" class="btn primary btn-small btn-green e-width" ng-show="grid.appScope.showRevertCancelBranch(row.entity)" ng-click="grid.appScope.revertCancelBranch(row.entity)">'+jsGlobals[34]+'</button>'},
                                                         { name: 'change_report', displayName: jsGlobals[28], width: 55, cellTemplate: '<div class="center-checkbox-wrapper" ng-show="row.entity.isCanEdit && (row.entity.od_order_status == 2 || row.entity.od_order_status == 3)"><input type="checkbox" ng-model="row.entity.change_report"></div>', enableCellEdit:false,},
                                                         { name: 'od_branch_cd', displayName: jsGlobals[0], width: 100, enableCellEdit:false},
                                                         { name: 'o_is_ad_receive_payment_sales_input', displayName: jsGlobals[19], width: 100, enableCellEdit:false, cellFilter : 'mapYesNo', visible: $scope.isShowAdReceivePaymentSalesInput},
                                                         { name: 'o_is_ad_receive_payment', displayName: jsGlobals[20], width: 100, enableCellEdit:false, cellFilter : 'mapYesNo'},
                                                         { name: 'o_is_order_input', displayName: jsGlobals[21], width: 100, enableCellEdit:false, cellFilter : 'mapYesNo'},
                                                         { name: 'o_is_acceptance_input', displayName: jsGlobals[22], width: 100, enableCellEdit:false, cellFilter : 'mapYesNo'},
                                                         { name: 'o_is_sales_slip_input', displayName: jsGlobals[23], width: 100, enableCellEdit:false, cellFilter : 'mapYesNo'},
                                                         { name: 'o_is_invoice_issue', displayName: jsGlobals[24], width: 100, enableCellEdit:false, cellFilter : 'mapYesNo'},
                                                         { name: 'od_order_status', displayName: jsGlobals[1], width: 100, enableCellEdit:false, cellFilter : 'mapOrderStatus'},
                                                         { name: 'p_id', displayName: jsGlobals[3], width: 100,
                                                             editableCellTemplate : 'ui-grid/dropdownEditor',
                                                             cellFilter : 'mapProduct',
                                                             editDropdownValueLabel : 'name',
                                                             editDropdownOptionsArray : products,
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit && scope.row.entity.change_report && (scope.row.entity.od_is_cancel_request != 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             }
                                                         },
                                                         { name: 'od_contents', displayName: jsGlobals[26], width: 100, editableCellTemplate : '<form name="inputForm"><input type="text" maxlength="30" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_contents\']" ui-grid-editor=""></form>',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit && scope.row.entity.change_report && (scope.row.entity.od_is_cancel_request != 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             }
                                                         },
                                                         { name: 'od_delivery_date', displayName: jsGlobals[5], width: 100,
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit && scope.row.entity.change_report && (scope.row.entity.od_is_cancel_request != 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             }
                                                         },
                                                         { name: 'od_billing_date', displayName: jsGlobals[6], width: 100,
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit && scope.row.entity.change_report && ($scope.order.o_is_ad_receive_payment == 1) && (scope.row.entity.od_is_cancel_request != 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             }
                                                         },
                                                         { name: 'od_payment_date', displayName: jsGlobals[7], width: 100,
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit && scope.row.entity.change_report && ($scope.order.o_is_ad_receive_payment == 1) && (scope.row.entity.od_is_cancel_request != 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             }
                                                         },
                                                         { name: 'od_quantity', displayName: jsGlobals[8], width: 100, cellFilter: 'number:0',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit && scope.row.entity.change_report && (scope.row.entity.od_is_cancel_request != 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_quantity\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                         },
                                                         { name: 'od_tax_type', editableCellTemplate: 'ui-grid/dropdownEditor', cellFilter: 'mapTaxType', editDropdownIdLabel: 'code', editDropdownValueLabel: 'name', editDropdownOptionsArray: tax_division, displayName: jsGlobals[9], width: 100,
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit && scope.row.entity.change_report && (scope.row.entity.od_is_cancel_request != 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             }
                                                         },
                                                         { name: 'od_unit_price', displayName: jsGlobals[10], width: 100, cellFilter: 'number:2',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit && scope.row.entity.change_report && (scope.row.entity.od_is_cancel_request != 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_unit_price\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                         },
                                                         { name: 'od_amount()', displayName: jsGlobals[11], width: 100, cellFilter: 'number:0'},
                                                         { name: 'od_cost_unit_price', displayName: jsGlobals[12], width: 100, cellFilter: 'number:2', enableCellEdit:false},
                                                         { name: 'od_cost_total_price', displayName: jsGlobals[13], width: 100, cellFilter: 'number:0', enableCellEdit:false},
                                                         { name: 'od_gross_profit_amount()', displayName: jsGlobals[14], width: 100, cellFilter: 'number:0', enableCellEdit: false},
                                                         { name: 'od_gross_profit_rate()', displayName: jsGlobals[15], width: 100, enableCellEdit: false}
                                                       ];
            }

            angular.forEach(data, function(value, key) {
                value.od_amount_value =  value.od_amount;
                value.od_gross_profit_amount_value =  value.od_gross_profit_amount;
                value.od_gross_profit_rate_value =  value.od_gross_profit_rate;
                value.shift_color_cd = '#0459D0';
                value.od_delivery_date = value.od_delivery_date.replace(/-/g, "/");
                value.od_payment_date = value.od_payment_date.replace(/-/g, "/");
                value.od_billing_date = value.od_billing_date.replace(/-/g, "/");
                value.delivery_month = (new Date(value.od_delivery_date).getFullYear() * 100) + new Date(value.od_delivery_date).getMonth();
                value.disable = 0;

                var fd = new Date();
                var d = fd.getDate();
                var m = fd.getMonth();
                var y = fd.getFullYear();
                var delivery_month = new Date(value.od_delivery_date).getMonth();
                var delivery_year = new Date(value.od_delivery_date).getFullYear();
                var last_date_of_month = new Date(fd.getFullYear(), (fd.getMonth()+1), 0).getDate();
                var closing_date = value.c_closing_date;
                if(closing_date > last_date_of_month) {
                    closing_date = last_date_of_month;
                }
                if (y == delivery_year) {
                    if(d < closing_date) {
                        if (delivery_month >= m - 1) {
                            value.isCanEdit = true;
                            return;
                        }
                        value.isCanEdit = false;
                    }
                    else {
                        if (delivery_month >= m){
                            value.isCanEdit = true;
                            return;
                        }
                        value.isCanEdit = false;
                    }
                } else if (y < delivery_year) {
                    value.isCanEdit = true;
                } else {
                    value.isCanEdit = false;
                }
            });

            $scope.gridOptionsEdit.data = data;

            angular.forEach($scope.gridOptionsEdit.data, function(value, key) {
                value.od_amount = function () {
                    result = f_floor(this.od_quantity * this.od_unit_price);
                    result = result || 0;
                    this.od_amount_value = result;
                    return result;
                };
                value.od_gross_profit_amount = function () {
                    var gross_profit_amount;
                    if (this.od_tax_type == 2) {
                        var tax_rate;
                        var delivery_date = new Date(this.od_delivery_date);
                        var arr = jQuery.grep($rootScope.tax_rates, function( i ) {
                            var da = new Date(i.adaptation_date);
                            delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                            da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                            return da <= delivery_date;
                        });
                        var tax_rates = [];
                        $.each(arr, function(key, value){
                            tax_rates.push(value.rate);
                        });
                        tax_rate = Math.max.apply(Math, tax_rates);
                        gross_profit_amount = f_ceil(this.od_amount()/(1 + (tax_rate/100))) - this.od_cost_total_price;
                    }
                    else {
                        gross_profit_amount = this.od_amount() - this.od_cost_total_price;
                    }

                    this.od_gross_profit_amount_value = (gross_profit_amount >= 0) ? gross_profit_amount : 0;
                    return this.od_gross_profit_amount_value;
                };
                value.od_gross_profit_rate = function () {
                    if (this.od_amount() <= 0) {
                        return '0.00 %';
                    }
                    var gross_profit_rate;
                    if (this.od_tax_type == 2) {
                        var tax_rate;
                        var delivery_date = new Date(this.od_delivery_date);
                        var arr = jQuery.grep($rootScope.tax_rates, function( i ) {
                            var da = new Date(i.adaptation_date);
                            delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                            da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                            return da <= delivery_date;
                        });
                        var tax_rates = [];
                        $.each(arr, function(key, value){
                            tax_rates.push(value.rate);
                        });
                        tax_rate = Math.max.apply(Math, tax_rates);
                        tmp_amount = f_ceil(this.od_amount() / (1 + (tax_rate / 100)));
                        gross_profit_rate = f_floor((this.od_gross_profit_amount() / tmp_amount) * 100 * 100) / 100;
                    }
                    else {
                        gross_profit_rate = f_floor(this.od_gross_profit_amount() / this.od_amount() * 100 * 100) / 100;
                    }

                    this.od_gross_profit_rate_value = (gross_profit_rate >= 0) ? gross_profit_rate : 0;
                    return this.od_gross_profit_rate_value.toFixed(2) + ' %';
                };

                if ( ! $scope.waiting_change_report && value.od_order_status == 1) {
                    $scope.isShowAddButton = true;
                    $scope.isShowEditButtonSection = true;
                    $scope.isShowChangeReportButtonSection = false;
                } else {
                     $scope.isShowAddButton = false;
                     $scope.isShowEditButtonSection = false;
                }

                if ($scope.waiting_change_report && (value.od_order_status == 2 || value.od_order_status == 3)) {
                    $scope.isShowChangeReportButtonSection = true;
                    $scope.isShowEditButtonSection = false;

                    if (value.isCanEdit) {
                        $scope.isShowAdReceivePaymentSalesInput = true;
                    } else {
                        $scope.isShowAdReceivePaymentSalesInput = false;
                    }
                } else {
                    $scope.isShowAdReceivePaymentSalesInput = false;
                }
            });

            $scope.order.o_cost_total_price = 0;
            $scope.order.o_consumption_tax = 0;
            $scope.order.o_tax_total = 0;
            $scope.order.o_gross_profit_total = 0;
            $scope.sum();
        });
    };
    // <!----------------------------------------------END LOAD EDIT---------------------------------------------->

    $scope.cancelApplyOrder = function() {

        confirm_dialog('申込みキャンセルをしてもよろしいですか？', {
            'delete': function() {
              var error = '';
              angular.forEach($scope.gridOptionsEdit.data, function(item) {
                  if (item.o_is_ad_receive_payment_sales_input == 1
                      || item.o_is_ad_receive_payment == 1
                      || item.o_is_order_input == 1
                      || item.o_is_sales_slip_input == 1
                      || item.o_is_invoice_issue == 1
                      || item.o_is_acceptance_input == 1
                  ) {
                      error += '申込キャンセルできません。<br>';
                      return;
                  }
              })

              if ((typeof $scope.order.o_new_comment == 'undefined') || ($scope.order.o_new_comment == '')) {
                  error += '変レポコメントを入力してください。';
              }
              $(this).dialog('close');
              if (!error) {
                  $http({
                      url : '/_admin/order/cancel_apply_order',
                      method : 'POST',
                      data : {
                          order_id : $scope.order.o_id,
                          comment : $scope.order.o_new_comment
                      }
                  }).success(function(data) {
                      form_changed = false;
                      form_submitted = true;
                      $('#dialogEdit').dialog("close");
                      $rootScope.search();
                  });
              } else {
                  $scope.errors = error;
                  $scope.$digest();
              }
            }
        });
    }

    // <!---------------------------CRTL + S---------------------------->
    $(document).bind('keydown', function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            $scope.changeReport(false);
            return false;
            // $scope.save();
            // return false;
        }
    });
    $scope.changeReport = function(is_change_report) {
        var branchs_del = $scope.gridOptionsEditDelete;
        var branchs = $scope.gridOptionsEdit.data;

        if ( ! form_changed) {

            $("#message_alert").html(jsGlobals[35]);
            if (is_change_report) {
                $("#message_alert").html(jsGlobals[36]);
            }

            $("#message_alert").dialog({
                title: '注意',
                width: 'auto',
                height: 147,
                modal: true,
                dialogClass: "order-dialog",
                buttons: [{
                    text: "OK",
                    click: function() {
                    $(this).dialog("close");
                    }
                }],
            });
            return;
        }

        if ($scope.order.o_is_ad_receive_payment == 0) {
            if ($scope.order.c_credit_amount < 0) {
                 $message = "<span class='validation-error'>与信限度額を超えています。</span>";
                 $("#message_alert").html($message).dialog({
                     title: "注意",
                     width: 'auto',
                     height: 'auto',
                     modal : true,
                     buttons: {
                         "OK": function() {
                             $(this).dialog('close');
                         }
                     },
                     dialogClass: "order-dialog",
                 });
                 return;
            }
        }

        $http({
            url : '/_admin/order/change_report',
            method : 'POST',
            data : {
                order : $scope.order,
                branchs : branchs,
                branchs_del: branchs_del,
                changed_report_type: $scope.waiting_change_report
            }
        }).success(function(data) {
             $scope.errors = '';
            if (data.error_string || data.error_string_branchs) {
                $scope.errors = data.error_string;
                angular.forEach(data.error_string_branchs, function(item, key) {
                    $scope.errors += '<p class="title">枝'
                    + key + ' を修正して下さい。 </p>'
                    + item;
                });
            }
            else {
                $scope.is_submitted = true;
                // Save edit
                if($scope.order.od_order_status==1) {
                    $scope.error_success = '<div class="success-message blink_me">保存しました。</div>';
                }
                // Change report
                else if($scope.order.od_order_status==2 || $scope.order.od_order_status==3) {
                    $scope.error_success = '<div class="success-message blink_me">変レポ申請をしました。</div>';
                }
                $rootScope.search();
            }
        })
    }

  //<!------------------------------------------------------------------------DELETE ORDER---------------------------------------------->
    $scope.deleteOrder = function() {
        confirm_dialog(' 削除します。よろしいですか？', {
            'delete': function() {
                $(this).dialog('close');
                $http({
                    url : '/_admin/order/delete',
                    method : 'POST',
                    data : {
                        order_id : $scope.order.o_id
                    }
                }).success(function(data) {
                    $scope.is_submitted = true;
                    $scope.errors = '';
                    $scope.error_success = '<div class="success-message blink_me">削除しました。</div>';
                    $rootScope.search();
                })
            }
        });
    }

    $scope.back = function() {
        $('#dialogEdit').dialog("close");
    }

    $scope.addData = function() {
        form_changed = true;
        var gridData = $scope.gridOptionsEdit.data;
        if(gridData.length>23) {
            return false;
        }
        gridData.push({
                    "od_order_status" : 1,
                    "od_branch_cd" : pad(gridData.length + 1, 2),
                    "od_quantity": 0,
                    "od_tax_type": 1,
                    "od_unit_price": 0,
                    "od_delivery_date": "",
                    "od_cost_unit_price" : 0,
                    "od_cost_total_price": 0,
                    "isCanEdit": true,
                    "is_new": 1,
                });

        var row = gridData[gridData.length - 1];
        row.od_amount = function () {
            result = f_floor(this.od_quantity * this.od_unit_price);
            result = result || 0;
            this.od_amount_value = result;
            return result;
        };
        row.od_gross_profit_amount = function () {
            var gross_profit_amount;
            if (this.od_tax_type == 2) {
                var tax_rate;
                var delivery_date = new Date(this.od_delivery_date);
                var arr = jQuery.grep($rootScope.tax_rates, function( i ) {
                    var da = new Date(i.adaptation_date);
                    delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                    da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                    return da <= delivery_date;
                });
                var tax_rates = [];
                $.each(arr, function(key, value){
                    tax_rates.push(value.rate);
                });
                tax_rate = Math.max.apply(Math, tax_rates);

                gross_profit_amount = f_ceil(this.od_amount()/(1 + (tax_rate/100))) - this.od_cost_total_price;
            }
            else {
                gross_profit_amount = this.od_amount() - this.od_cost_total_price;
            }

            this.od_gross_profit_amount_value = (gross_profit_amount >= 0) ? gross_profit_amount : 0;
            return this.od_gross_profit_amount_value;
        };
        row.od_gross_profit_rate = function () {
            if (this.od_amount() <= 0) {
                return '0.00 %';
            }

            var gross_profit_rate;
            if (this.od_tax_type == 2) {
                var tax_rate;
                var delivery_date = new Date(this.od_delivery_date);
                var arr = jQuery.grep($rootScope.tax_rates, function( i ) {
                    var da = new Date(i.adaptation_date);
                    delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                    da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                    return da <= delivery_date;
                });
                var tax_rates = [];
                $.each(arr, function(key, value){
                    tax_rates.push(value.rate);
                });

                tax_rate = Math.max.apply(Math, tax_rates);
                tmp_amount = f_ceil(this.od_amount() / (1 + (tax_rate / 100)));
                gross_profit_rate = f_floor((this.od_gross_profit_amount() / tmp_amount) * 100 * 100) / 100;
            }
            else {
                gross_profit_rate = f_floor(this.od_gross_profit_amount() / this.od_amount() * 100 * 100) / 100;
            }

            this.od_gross_profit_rate_value = (gross_profit_rate >= 0) ? gross_profit_rate : 0;
            return this.od_gross_profit_rate_value.toFixed(2) + ' %';
        };
    }

    $scope.gridOptionsEditDelete = [];
    $scope.deleteRow = function(row) {
        form_changed = true;
        /*confirm_dialog('削除します。よろしいですか？', {
            'delete': function() {*/
                row.disable = 1;
                $scope.gridOptionsEditDelete.push(row);
                var indexRow = $scope.gridOptionsEdit.data.indexOf(row);
                $scope.gridOptionsEdit.data.splice(indexRow, 1);
                // Refresh grid
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                var i = 0;
                angular.forEach($scope.gridOptionsEdit.data, function(item) {
                    i++;
                    item.od_branch_cd = pad(i, 2);
                });
                $scope.sum();
                /*$(this).dialog('close');
            }
        });*/
//       $scope.compute_credit();
    }; // END DELETE
    $scope.cancelBranch = function(row) {
        if (row.o_is_ad_receive_payment_sales_input == 1
                || row.o_is_ad_receive_payment == 1
                || row.o_is_order_input == 1
                || row.o_is_sales_slip_input == 1
                || row.o_is_invoice_issue == 1
                || row.o_is_acceptance_input == 1) {

            $("#message_alert").html("枝キャンセルできません。").dialog({
                title: "注意",
                width: 'auto',
                height: 'auto',
                modal : true,
                buttons: {
                    "OK": function() {
                        $(this).dialog('close');
                    }
                },
                dialogClass: "order-dialog",
            });

                return;
            }

        $("#message_alert").html("枝キャンセルしてもよろしいですか？").dialog({
            title: "注意",
            width: 'auto',
            height: 'auto',
            modal : true,
            buttons: {
                "OK": function() {
                    $(this).dialog('close');
                    angular.forEach($scope.branchs_old, function(item) {
                        if (item.od_id == row.od_id) {
                            // console.log("ITEM IN CANCEL:");
                            // console.log(item);
                            // str.replace("Microsoft", "W3Schools");
                            row.p_id = item.p_id;
                            row.od_contents = item.od_contents;
                            var d_date = new Date(item.od_delivery_date);
                            row.od_delivery_date = d_date.getFullYear()+"/"+(d_date.getMonth()+1)+"/"+d_date.getDate();
                            d_date = new Date(item.od_billing_date);
                            row.od_billing_date = d_date.getFullYear()+"/"+(d_date.getMonth()+1)+"/"+d_date.getDate();
                            d_date = new Date(item.od_payment_date);
                            row.od_payment_date = d_date.getFullYear()+"/"+(d_date.getMonth()+1)+"/"+d_date.getDate();
                            row.od_quantity = item.od_quantity;
                            row.od_tax_type = item.od_tax_type;
                            row.od_unit_price = item.od_unit_price;
                        }
                    });
                    $scope.$apply(function () {
                        row.change_report = true;
                        row.od_is_cancel_request = 1;
                    });
                }
            },
            dialogClass: "order-dialog",
        });
        form_changed = true;
    }; // End $scope.cancelBranch = function(row) {

    $scope.revertCancelBranch = function(row) {
        row.change_report = false;
        row.od_is_cancel_request = 0;
        form_changed = true;
    };

    $scope.copyRow = function(row, api) {
        form_changed = true;
        if(api.grid.rows.length > 23) {
            return false;
        }
        var indexRow = $scope.gridOptionsEdit.data.indexOf(row);
        var newrow = angular.copy(row);
        newrow.od_id = '';
        newrow.isCanEdit = true;
        newrow.is_new = 1;
        $scope.gridOptionsEdit.data.splice(indexRow + 1, 0, newrow);
        var i = 0;
        angular.forEach($scope.gridOptionsEdit.data, function(item) {
            i++;
            item.od_branch_cd =  pad(i, 2);
        });
        $scope.sum();
//        $scope.compute_credit();
    };

  //<!------------------------------------ Set payament date when change delivery date -------------------------------------->
    var checkHolidayAndSetPaymentDate = function(year, month, day_delivery, date, item) {

        // console.log(year+"/"+month+"/"+day_delivery+"/"+date);
        // month: 9
        // 2015 9 9 31
        // payment_date = new Date(year, month, date);
        // var year = payment_date.getFullYear();
        // Month from 0
        // var month = payment_date.getMonth() + 1;
        // var day = payment_date.getDate();
        month = month + 1;
        // month 10 => Deccember
        payment_date = new Date(year, month, 0); // Month october (9) has 31 days
        var noofday = payment_date.getDate(); // = 31
        // console.log("payment_date1:"+payment_date); // 31/10
        // console.log("noofday:"+noofday); // 31
        // console.log("date:"+date); // 31

        if(date > noofday) {
            date = noofday;
        }
        // month 10 -> December
        payment_date = new Date(year, month-1, date);
        // console.log("payment_date2:"+payment_date);
        payment_date = payment_date.getFullYear() + '/' + (payment_date.getMonth()+1) + '/' + date;
        // console.log("payment_date3:"+payment_date);
        $http({
            url : '/_admin/holiday/check_holiday',
            method : 'POST',
            data : {
                date : payment_date
            }
        }).success(function(data) {
                        if (data == 0) {
                            // If payment date is not holiday date => keep the same
                            // // console.log(payment_date);
                            item.od_payment_date = payment_date;
                        } else {
                            // If payment date is holiday date, pay before one day
                            if(date>1) {
                                date = date - 1;
                            } else {
                                month = month - 1;
                                date = (new Date(year, month, 0)).getDate();
                            }
                            checkHolidayAndSetPaymentDate(year, month-1, day_delivery, date, item);
                        }
                    });
    }

    //<!--------------------------- Set billing date when change delivery date --------------------------------------->
    var checkHolidayAndSetBillingDate = function(year, month, day_delivery, date, item) {
        // billing_date = new Date(year, month, date);
        // // console.log("billingdate:"+billing_date);
        // console.log("month:"+month);
        month = month+1;
        // console.log("day_delivery:"+day_delivery+"/"+date);
        if(day_delivery > date) {
            // month = month + 1;
            var noofday = new Date(year, month, 0).getDate();
            if(date>noofday) {
                date = noofday;
            }
        } else {
            var noofday = new Date(year, month, 0).getDate();
            if(date>noofday) {
                date = noofday;
            }
        }
        /*var year = billing_date.getFullYear();
        var month = billing_date.getMonth() + 1;
        var day = billing_date.getDate();
        var noofday = new Date(year, month, 0).getDate();

        // console.log("day:"+day+"/"+noofday);

        if(day>noofday) {
            day = noofday;
        }*/
        billing_date = year + '/' + month + '/' + date;
        item.od_billing_date = billing_date;
    }

    //<!-------------------------------- Set payment date and billing date following delivery date------------------------------>
    $scope.setPaymentDateAndBillingDate = function (item) {
        // console.log("---------------------$scope.order-------------------");
        // console.log($scope.order);
        // item: rowEntity
        if ($scope.order.o_is_ad_receive_payment == "0") {
            // Billing date
            var year = new Date(item.od_delivery_date).getFullYear();
            var month = new Date(item.od_delivery_date).getMonth();
            var day_delivery = new Date(item.od_delivery_date).getDate();
            // If delivery date > closing date => Billing in next month
            if (day_delivery > $scope.order.c_closing_date) {
                month = month + 1;
            }
            checkHolidayAndSetBillingDate(year, month, day_delivery, parseInt($scope.order.c_closing_date), item);
            // Payment date = payment date + month pending from DB
            month = month + parseInt($scope.order.c_payment_month_cd);
            checkHolidayAndSetPaymentDate(year, month, day_delivery, parseInt($scope.order.c_payment_day_cd), item);
        } else {
            item.od_payment_date = "";
            item.od_billing_date = "";;
        }
    }; // End $scope.setPaymentDateAndBillingDate = function (item) {

    $scope.sum = function() {
        // 税別合計
        $scope.order.o_cost_total_price = 0;
        // 消費税
        $scope.order.o_consumption_tax = 0;
        // 税込合計
        $scope.order.o_tax_total = 0;
        // 粗利合計
        $scope.order.o_gross_profit_total = 0;

        angular.forEach($scope.gridOptionsEdit.data, function(item) {
            if (item.od_delivery_date) {
                var tax_rate;
                var delivery_date = new Date(item.od_delivery_date);
                var arr = jQuery.grep($rootScope.tax_rates, function (i) {
                    var da = new Date(i.adaptation_date);
                    delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                    da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                    return da <= delivery_date;
                });
                var tax_rates = [];
                $.each(arr, function (key, value) {
                    tax_rates.push(value.rate);
                });
                tax_rate = Math.max.apply(Math, tax_rates);

                // Only tax type is 「内」 has the tax included in amount
                /*var no_tax_amount = (item.od_tax_type != 2) ? item.od_amount() : (item.od_amount() / (1 + tax_rate/100));
                $scope.order.o_cost_total_price += no_tax_amount;
                // Only tax type is 「非」 is nontaxable, means no tax needed for the product price
                $scope.order.o_consumption_tax += (item.od_tax_type != 3) ? (no_tax_amount * (tax_rate / 100)) : 0;
                $scope.order.o_gross_profit_total += item.od_gross_profit_amount();*/


                if(item.od_tax_type == 2) {
                    // (②/(1+税率)+小数点以下切り上げ)
                    $scope.order.o_cost_total_price += f_ceil(item.od_amount()/(1+tax_rate/100));
                    // (②-(②/(1+税率)+小数点以下切り上げ))
                    $scope.order.o_consumption_tax += (item.od_amount() - f_ceil(item.od_amount()/(1+tax_rate/100)));
                    // ②+③
                    $scope.order.o_tax_total+=item.od_amount();
                }
                if(item.od_tax_type == 1 ) {
                    // (①*税率+小数点以下切り捨て)
                    $scope.order.o_consumption_tax += f_floor(item.od_amount()*tax_rate/100);
                    // ①+③
                    $scope.order.o_cost_total_price += item.od_amount();
                    // ①*(1+税率)+小数点以下切り捨て
                    $scope.order.o_tax_total += f_floor(item.od_amount()*(1+tax_rate/100));
                }
                if(item.od_tax_type == 3) {
                    // ①+③
                    $scope.order.o_cost_total_price += item.od_amount();
                    // ②+③
                    $scope.order.o_tax_total+=item.od_amount();
                }
                $scope.order.o_gross_profit_total += item.od_gross_profit_amount();
            }
        });

        //$scope.order.o_cost_total_price = Math.ceil($scope.order.o_cost_total_price);
        //$scope.order.o_consumption_tax = Math.floor($scope.order.o_consumption_tax);
        //$scope.order.o_tax_total = $scope.order.o_cost_total_price + $scope.order.o_consumption_tax;
    }

//    $scope.compute_credit = function() {
//        if ($scope.order.o_is_ad_receive_payment == 0) {
//            var gridData = $scope.gridOptionsEdit.data;
//            var credit_amount = $scope.order.credit_amount;
//            var order_id = $scope.order.o_id;
//            var client_id = $scope.order.o_client_id;
//            console.log('grid Data');
//            console.log($scope.order.c_credit_amount);
//            $http({
//                url : '/_admin/client/ajax_check_payment',
//                method : 'POST',
//                data : {client_id : client_id, order_id: order_id, branchs: $scope.gridOptionsEdit.data }
//            }).success(function(data) {
//                console.log('c_credit_amount');
//                credit_amount = credit_amount - data.amount;
//                $scope.order.c_credit_amount = credit_amount;
//            });
//        }
//    }

    $scope.saveRow = function( rowEntity ) {
        var promise = $q.defer();
        $scope.gridApi.rowEdit.setSavePromise( rowEntity, promise.promise );
        rowEntity.new_delivery_month = (new Date(rowEntity.od_delivery_date).getFullYear() * 100) + new Date(rowEntity.od_delivery_date).getMonth();

        if (rowEntity.new_delivery_month > rowEntity.delivery_month) {
            rowEntity.shift_color_cd = '#0459D0';
        } else {
            rowEntity.shift_color_cd = '#C9D219';
        }

        // When change quantity
        if(rowEntity.od_quantity) {
            if(!jQuery.isNumeric(rowEntity.od_quantity) || rowEntity.od_quantity < 0) {
                rowEntity.od_quantity = 0
            }
        }
        // When change unit price
        if(rowEntity.od_unit_price) {
            // rowEntity.unit_price = 40;
            if(!jQuery.isNumeric(rowEntity.od_unit_price) || rowEntity.od_unit_price < 0) {
                rowEntity.od_unit_price = 0
            }
        }
        // delivery_date
        if (rowEntity.od_delivery_date) {
            inputArray = rowEntity.od_delivery_date.split('/');
            rowEntity.od_delivery_date = convertDate(inputArray);

            // credit amount
            var delivery_date = new Date(rowEntity.od_delivery_date);
            if (!isNaN(delivery_date)) {
                var fd = new Date();
                var last_date_of_month = new Date(fd.getFullYear(), (fd.getMonth()+1), 0).getDate();
                var closing_date = $scope.order.od_closing_date;
                if(closing_date > last_date_of_month) {
                    closing_date = last_date_of_month;
                }
                var closing_date = new Date(fd.getFullYear(), fd.getMonth(), closing_date);
                if (delivery_date <= closing_date) {
                    rowEntity.od_delivery_date = fd.getFullYear() + '/' + (fd.getMonth() + 1) + '/' + $scope.order.od_closing_date;
                }
            } else {
                rowEntity.od_delivery_date = '';
            }
        }
        if (rowEntity.od_billing_date) {
            inputArray = rowEntity.od_billing_date.split('/');
            rowEntity.od_billing_date = convertDate(inputArray);
            var billing_date = new Date(rowEntity.od_billing_date);
            if (isNaN(billing_date)) {
                rowEntity.od_billing_date = '';
            }
        }
        if (rowEntity.od_payment_date) {
            inputArray = rowEntity.od_payment_date.split('/');
            rowEntity.od_payment_date = convertDate(inputArray);
            var payment_date = new Date(rowEntity.od_payment_date);
            if (isNaN(payment_date)) {
                rowEntity.od_payment_date = '';
            }
        }

        rowEntity.od_tax_type = parseInt(rowEntity.od_tax_type)
        rowEntity.od_unit_price = f_floor(rowEntity.od_unit_price * 100) / 100;
        rowEntity.od_unit_price = rowEntity.od_unit_price || 0;
        if (rowEntity.od_cost_unit_price) {
            rowEntity.od_cost_unit_price = f_floor(rowEntity.od_cost_unit_price * 100) / 100;
            rowEntity.od_cost_unit_price = rowEntity.od_cost_unit_price || 0;
            rowEntity.od_cost_total_price = f_floor(rowEntity.od_cost_unit_price * rowEntity.od_quantity);
            rowEntity.od_cost_total_price = rowEntity.od_cost_total_price || 0;
        }

        if(rowEntity.od_delivery_date && $scope.order.o_is_ad_receive_payment == 0) {
            $scope.setPaymentDateAndBillingDate(rowEntity);
        }

        $scope.sum();
//        $scope.compute_credit();

        promise.resolve();
    };

    $scope.gridOptionsEdit.onRegisterApi = function(gridApi) {
        $scope.gridApi = gridApi;
        gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
    }

}])