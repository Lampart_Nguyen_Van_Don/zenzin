app.controller('EditCtrl', ['$rootScope', '$scope', '$http', '$q', '$interval', 'uiGridConstants', function ($rootScope, $scope, $http, $q, $interval, uiGridConstants) {
    $scope.gridOptionsEdit = {
        selectionRowHeaderWidth: 35,
        paginationPageSizes: [20, 30, 50],
        paginationPageSize: 20,
        rowEditWaitInterval: -1,
        rowTemplate:
           '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ui-grid-cell ng-class="{\'green-row\': (row.invalid) , \'gray-row\' : (row.entity.od_is_cancel_request == 1)}" ></div>'
    };

    $scope.gridOptionsEdit.enableCellEditOnFocus = true;
    $scope.gridOptionsEdit.multiSelect = true;
    $scope.is_submitted = false;
    $scope.isShowAddButton = false;
    $scope.isShowEditButtonSection = false;
    $scope.isShowChangeReportButtonSection = false;
    $scope.isShowAdReceivePaymentSalesInput = false;
    $scope.show_approved_cell = function(row) {
        if (row.entity.od_order_status == 1 && row.entity.od_branch_cd == 1) {
            return true;
        }
        return false;
    }
    $scope.show_approved_column = function() {
        return true;
    }

    $scope.showCancelBranch = function(row) {
        // Old code. Comment on 1/August/2015 (Phong)
        /*if (row.o_is_ad_receive_payment_sales_input == 1            ========================> is_ad_sales_slip_input ????
                || row.o_is_ad_receive_payment == 1                   ========================> o_is_ad_receive_payment
                || row.o_is_order_input == 1                          ========================> o_is_order_input
                || row.o_is_sales_slip_input == 1                     ========================> o_is_sales_slip_input
                || row.o_is_invoice_issue == 1                        ========================> o_is_invoice_issue
                || row.o_is_acceptance_input == 1                     ========================> o_is_acceptance_input
            ) {
            return false;
        }*/
        /**
        Check: [前売伝]～[請求]
           1/ od_is_ad_sales_slip_input
           2/ od_is_ad_invoice_issue,
           3/ o_is_order_input
           4/ o_is_acceptance_input
           5/ o_is_sales_slip_input,
           6/ o_is_invoice_issue
           From order_detail
        */
        if(row.o_is_order_input==1
                || row.o_is_acceptance_input==1
                || row.o_is_sales_slip_input==1
                || row.o_is_invoice_issue==1
//#6956: Start
//                || row.od_is_ad_sales_slip_input==1
//                || row.od_is_ad_invoice_issue==1
//#6956: End
        ) {
            return false;
        }

        if (row.od_is_cancel_request != 1 && row.isCanEdit && (row.od_order_status == 2 || row.od_order_status == 3)){
            return true;
        }
        return false;
    } // End $scope.showCancelBranch = function(row)
    $scope.showRevertCancelBranch = function(row) {
        if (row.od_is_cancel_request == 1 && row.isCanEdit)
            return true;
    }

    $scope.parse_date = function() {
        if (parse_date($scope.order.o_order_form_input_date)) {
            $scope.order.o_order_form_input_date = parse_date($scope.order.o_order_form_input_date);
        }
    }
    $scope.my_account = my_account;
    // Set value for: od_is_ad_sales_slip_input (前売伝) & od_is_ad_invoice_issue (前請求)
    function ext_link(colName) {
        /*var out = '';
        // var ng = false; o_is_ad_receive_payment
        out += '<div class="ui-grid-cell-contents" ng-if="row.entity.o_is_ad_receive_payment != \'1\'">－</div>';
        out += '<div class="ui-grid-cell-contents" ng-if="row.entity.o_is_ad_receive_payment == \'1\'">{{row.entity.' + colName + ' | mapYesNo}}</div>';
        return out;*/


        var out = '';
        var ng = false;

        if (colName == 'od_is_ad_sales_slip_input' || colName=='o_is_ad_receive_payment_sales_input' || colName == 'od_is_ad_invoice_issue') {
            out += '<div class="ui-grid-cell-contents" ng-if="row.entity.o_is_ad_receive_payment != \'1\'">－</div>';
            out += '<div ng-if="row.entity.o_is_ad_receive_payment == \'1\'">';
        }

        switch (colName) {
            case 'od_is_ad_sales_slip_input':
            case 'o_is_ad_receive_payment_sales_input':
            case 'o_is_sales_slip_input':
                var action = '/_admin/sales/show';
                //var hidden = '<input type="hidden" name="" ng-value="">'; //j_code + branch
                var hidden = '<input type="hidden" name="search_j_code_branch_cd_from" ng-value="row.entity.o_j_code + row.entity.od_branch_cd">' +
                '<input type="hidden" name="search_j_code_branch_cd_to" ng-value="row.entity.o_j_code + row.entity.od_branch_cd">' + //j_code + branch
                '<input type="hidden" name="search_delivery_date_from" value="">' +
                '<input type="hidden" name="search_delivery_date_to" value="">';
                break;
            case 'od_is_ad_invoice_issue':
            case 'o_is_invoice_issue':
                var action = '/_admin/invoice/show';
                var hidden = '<input type="hidden" name="s_code" ng-value="row.entity.c_s_code">'; //s_code
                break;
            case 'o_is_order_input':
            case 'o_is_acceptance_input':
                var action = '/_admin/order_acceptance/show';
                var hidden = '<input type="hidden" name="from_jcode_branch" ng-value="row.entity.o_j_code + row.entity.od_branch_cd">' +
                             '<input type="hidden" name="to_jcode_branch" ng-value="row.entity.o_j_code + row.entity.od_branch_cd">'; //j_code + branch
                break;
        }

        switch (colName) {
            case 'od_is_ad_sales_slip_input':
            case 'o_is_ad_receive_payment_sales_input':
            case 'o_is_sales_slip_input':
                if (can_invoices == 'all') {
                    out += '<div class="ui-grid-cell-contents">';
                } else if (can_invoices == 'self') {
//#7066: Start
//                  out += '<div class="ui-grid-cell-contents" ng-if="row.entity.o_account_id != grid.appScope.my_account.id">－</div>';
                    out += '<div class="ui-grid-cell-contents" ng-if="row.entity.o_account_id != grid.appScope.my_account.id"><div class="a-padding-left" ng-if="1">{{row.entity.' + colName + ' | mapYesNo}}</div></div>';
                    out += '<div class="ui-grid-cell-contents" ng-if="row.entity.o_account_id == grid.appScope.my_account.id">';
                } else {
                    out += '<div class="ui-grid-cell-contents">';
  //#7066: End
                    ng = true;
                }
                break;

            case 'o_is_order_input':
            case 'o_is_acceptance_input':
                if (can_acceptance) {
                    out +=  '<div class="ui-grid-cell-contents">';
                } else {
                    ng = true;
                }
                break;

            case 'od_is_ad_invoice_issue':
            case 'o_is_invoice_issue':
                if (can_invoice_issue) {
                    out +=  '<div class="ui-grid-cell-contents">';
                } else {
//#7194:Start
                    out +=  '<div class="ui-grid-cell-contents">';
//#7194:End
                    ng = true;
                }
                break;
        }

        if (ng) {
            // out +=  '<form action="' + action + '" method="post">' + hidden;
            if(colName=='o_is_order_input' ) {
                // 「発注・検収-発注検収力・変更」権限有り：「未」「済」でリンクできてOK
                if(order_acceptance_input_change) {
                    // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-value="row.entity.' + colName + ' | mapYesNo">';
                    out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                }
                // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限有り：「済」だけリンクできてOK
                if(!order_acceptance_input_change && order_acceptance_view) {
                    // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="row.entity.o_is_order_input== 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                    out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                }
                // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限無し：リンクできてない
                if(!order_acceptance_input_change && !order_acceptance_view) {
                    out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                }
            } else if(colName=='o_is_acceptance_input') {
                // 「発注・検収-発注検収力・変更」権限有り：「未」「済」でリンクできてOK
                if(order_acceptance_input_change) {
                    out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-value="row.entity.' + colName + ' | mapYesNo">';
                }
                // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限有り：「済」だけリンクできてOK
                if(!order_acceptance_input_change && order_acceptance_view) {
                    // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="row.entity.o_is_acceptance_input== 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                    out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                }
                // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限無し：リンクできてない
                if(!order_acceptance_input_change & !order_acceptance_view) {
                    out += '<div class="a-padding-left" ng-if="1">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                }
            } else {
//#7066: Start
//              out += '<div class="ui-grid-cell-contents">－</div>';
              out += '<div class="a-padding-left" ng-if="1">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7066: End
            }
            // out += '</form>';
        } else {
            // out +=  '<form action="' + action + '" method="post">' + hidden;

            if (colName != 'od_is_ad_invoice_issue' &&  colName != 'o_is_invoice_issue') {
                if(colName=='o_is_order_input' ) {
                    // 「発注・検収-発注検収力・変更」権限有り：「未」「済」でリンクできてOK
                    if(order_acceptance_input_change) {
                        // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-value="row.entity.' + colName + ' | mapYesNo">';
                        out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                    }
                    // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限有り：「済」だけリンクできてOK
                    if(!order_acceptance_input_change && order_acceptance_view) {
                        // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="row.entity.o_is_order_input== 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                        out += '<div class="a-padding-left" >{{row.entity.' + colName + ' | mapYesNo}}</div>';
                    }
                    // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限無し：リンクできてない
                    if(!order_acceptance_input_change && !order_acceptance_view) {
                        out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                    }
                } else if(colName=='o_is_acceptance_input') {
                    // 「発注・検収-発注検収力・変更」権限有り：「未」「済」でリンクできてOK
                    if(order_acceptance_input_change) {
                        // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-value="row.entity.' + colName + ' | mapYesNo">';
                        out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                    }
                    // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限有り：「済」だけリンクできてOK
                    if(!order_acceptance_input_change && order_acceptance_view) {
                        // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="row.entity.o_is_acceptance_input== 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                        out += '<div class="a-padding-left" >{{row.entity.' + colName + ' | mapYesNo}}</div>';
                    }
                    // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限無し：リンクできてない
                    if(!order_acceptance_input_change & !order_acceptance_view) {
                        out += '<div class="a-padding-left" ng-if="1">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                    }
                } else {
                    // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="row.entity.od_order_status != 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                    out += '<div class="a-padding-left" >{{row.entity.' + colName + ' | mapYesNo}}</div>';
                }
            } else {
                if (colName == 'od_is_ad_invoice_issue') {
                    // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="row.entity.od_order_status != 1 && row.entity.od_is_ad_invoice_issue != 0" ng-value="row.entity.' + colName + ' | mapYesNo">';
                    out += '<div class="a-padding-left" ng-if="(row.entity.od_is_ad_invoice_issue == 0) || (row.entity.od_order_status != 1 && row.entity.od_is_ad_invoice_issue != 0)">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                } else {
                    // out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="row.entity.od_order_status != 1 && row.entity.o_is_invoice_issue != 0" ng-value="row.entity.' + colName + ' | mapYesNo">';
                    out += '<div class="a-padding-left" ng-if="(row.entity.o_is_invoice_issue == 0) || (row.entity.od_order_status != 1 && row.entity.o_is_invoice_issue != 0)">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                }
            }

            // out += '</form>' + '</div>';
            out += '</div>';
        }

        if (colName == 'od_is_ad_sales_slip_input' || colName=='o_is_ad_receive_payment_sales_input' || colName == 'od_is_ad_invoice_issue') {
            out += '</div>';
        }
        return out;
    }

    $scope.check_closest_closing_date = function(row) {
        var delivery_date = new Date(row.entity.od_delivery_date);

        if (delivery_date.getFullYear() < closest_year_closing_date) {
            return false;
        }

        if ((delivery_date.getFullYear() == closest_year_closing_date) && ((delivery_date.getMonth() + 1) <= closest_month_closing_date)) {
            return false;
        }

        return true;
    }

    // <!-------------------------------------------------END LOAD EDIT---------------------------------------------->
    $rootScope.loadEdit = function (order_id, form) {
        $scope.errors = '';
        $scope.is_submitted = false;
        $scope.error_success = '';
        $scope.order = {};
        $scope.gridOptionsEdit.data = [];
        $scope.gridOptionsEditDelete = [];

        $http({
            url : '/_admin/order/get_detail',
            method : 'POST',
            data : {
                order_id : order_id,
                form: form
            }
        }).success(function(data) {
            $scope.is_credit_over = false;
            //tmp#6841: Start 2015/09/24
            $scope.isCanEdit = true;
            //tmp#6841: End
            $('#dialog').dialog("close");
            if (form == 'edit') {
                $('#dialogEdit').dialog('option', 'title', '編集');
            } else if (form == 'change_report') {
                $('#dialogEdit').dialog('option', 'title', '変レポ');
            }

            $("#dialogEdit").dialog( "option", "height", "auto");
            $("#dialogEdit").dialog( "option", "width", "80%" );
            $('#dialogEdit').dialog("open");
            // $('#dialogEdit .grid').width($('#dialogEdit').width());
            $(window).trigger('resize');

            $scope.order = data[0];
//#9763:Start
            $scope.order.c_debt = addCommas($scope.order.c_debt);
//#9763:End
            if ($scope.order.o_order_form_input_date == '0000-00-00') {
                $scope.order.o_order_form_input_date = '';
            } else {
                $scope.order.o_order_form_input_date = $scope.order.o_order_form_input_date.replace(/-/g, '/');
            }
            $scope.order.credit_amount_show = addCommas($scope.order.c_credit_amount);
            $scope.order.credit_amount = $scope.order.c_credit_amount;
            $scope.order.o_order_regist_date = $scope.order.o_order_regist_date.replace(/-/g, '/');
            $scope.branchs_old = angular.copy(data);
            $scope.order.o_is_ad_receive_payment = parseInt(data[0].o_is_ad_receive_payment);
            $scope.order.c_is_ad_receive_payment = parseInt(data[0].c_is_ad_receive_payment);
            if(data[0].closing_date_name!=null) {
                $("#e_closing_date_name").text(data[0].closing_date_name+"日");
            } else {
                $("#e_closing_date_name").text("日");
            }
            if(data[0].payment_day_cd_name!=null) {
                $("#e_payment_day_cd_name").text(data[0].payment_day_cd_name+"日");
            } else {
                $("#e_payment_day_cd_name").text("日");
            }
            if(data[0].payment_month_cd_name!=null) {
                $("#e_payment_month_cd_name").text(data[0].payment_month_cd_name+"月");
            } else {
                $("#e_payment_month_cd_name").text("月");
            }

            $scope.order.o_tax_total = $scope.order.o_total_amount;
            $scope.order.o_consumption_tax = $scope.order.o_total_amount_tax;
            // od_order_status
            $scope.waiting_change_report = false;
            angular.forEach(data , function(value) {
                if(value.od_order_status==2 || value.od_order_status==3) {
                    $scope.waiting_change_report = true;
                }
                if ((typeof value.isCanEdit != 'undefined') && (value.isCanEdit == false)) {
                    $scope.isCanEdit = false;
                }
            });

            // if ($scope.order.od_order_status == UNAPPROVED) {
                if (!$scope.waiting_change_report) {
                $scope.gridOptionsEdit.columnDefs = [
                                                         { name: '処理', displayName: jsGlobals[27], width: 165, enableCellEdit:false, cellTemplate:
                                                             '<button type="button" ng-class="grid.appScope.is_credit_over ? \'btn-gray\' : \'btn-green\'" ng-disabled="grid.appScope.is_credit_over" ng-show="row.entity.isCanEdit && row.entity.od_order_status == 1" class="btn primary btn-small btn-green txt-center" ng-click="grid.appScope.copyRow(row.entity, row)">'+jsGlobals[29]+'</button> '
                                                             + '<button type="button" class="btn primary btn-small btn-green btn-delete txt-center" ng-show="row.entity.isCanEdit && row.entity.od_order_status == 1" ng-click="grid.appScope.deleteRow(row.entity)">'+jsGlobals[30]+'</button>'},
                                                         { name: 'od_branch_cd', displayName: jsGlobals[0], width: 45, enableCellEdit:false},
                                                         { name: 'od_order_status', displayName: jsGlobals[1], width: 100, cellFilter : 'mapOrderStatus',
                                                             cellEditableCondition: function(scope){
                                                                 /*tmp#6876: Start 2015/09/25
                                                                 if (scope.row.entity.is_new == 1) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 /*tmp#6876: End*/
                                                                 return false;
                                                             },
                                                             /*tmp#6876: Start 2015/09/25
                                                             cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.o_is_change_report_apply == 1 ? "変レポ申請中" : row.entity.od_order_status_name}}</div>'
                                                              /*tmp#6876: End*/
                                                         },
                                                         { name: 'p_id', displayName: jsGlobals[3], width: 100,
                                                             editableCellTemplate : 'ui-grid/dropdownEditor',
                                                             cellFilter : 'mapProduct',
                                                             editDropdownValueLabel : 'name',
                                                             editDropdownOptionsArray : products,
                                                             //cellClass: 'pink-row',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             }
                                                         },
                                                         { name: 'od_contents', displayName: jsGlobals[26], width: 100, editableCellTemplate : '<form name="inputForm"><input type="text" maxlength="30" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_contents\']" ui-grid-editor=""></form>',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="{\'background-color\': \'#FFCC99\'}">{{row.entity.od_contents}}</div>'
                                                         },
                                                         { name: 'od_delivery_date', displayName: jsGlobals[5], width: 100,
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="{\'background-color\': \'#FFCC99\'}">{{row.entity.od_delivery_date}}</div>'
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_delivery_date\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                         },
                                                         { name: 'od_billing_date', displayName: jsGlobals[6], width: 100,
                                                             cellEditableCondition: function(scope){
                                                                 if ((scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) && ($scope.order.o_is_ad_receive_payment == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="grid.appScope.change_color(row)">{{row.entity.od_billing_date}}</div>'
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_billing_date\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                         },
                                                         { name: 'od_payment_date', displayName: jsGlobals[7], width: 100,
                                                             cellEditableCondition: function(scope){
                                                                 if ((scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) && ($scope.order.o_is_ad_receive_payment == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="grid.appScope.change_color(row)">{{row.entity.od_payment_date}}</div>'
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_payment_date\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                         },
                                                         { name: 'od_quantity', displayName: jsGlobals[8], width: 100, cellFilter: 'number:0', cellClass: 'a-right',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_quantity\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="{\'background-color\': \'#FFCC99\'}">{{row.entity.od_quantity}}</div>'
                                                         },
                                                         { name: 'od_tax_type', cellFilter: 'mapTaxType', editableCellTemplate: 'ui-grid/dropdownEditor', editDropdownIdLabel: 'code', editDropdownValueLabel: 'name', editDropdownOptionsArray: tax_division, displayName: jsGlobals[9], width: 45,
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             //cellClass: 'pink-row'
                                                         },
                                                         { name: 'od_unit_price', displayName: jsGlobals[10], width: 100, cellFilter: 'number:2', cellClass: 'a-right',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_unit_price\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="{\'background-color\': \'#FFCC99\'}">{{row.entity.od_unit_price}}</div>'
                                                         },
                                                         { name: 'od_amount()', displayName: jsGlobals[11], width: 100, enableCellEdit:false, cellFilter: 'number:0', cellClass: 'a-right'},
                                                         { name: 'od_cost_unit_price', displayName: jsGlobals[12], width: 100, cellFilter: 'number:2', cellClass: 'a-right',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_cost_unit_price\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="{\'background-color\': \'#FFCC99\'}">{{row.entity.od_cost_unit_price}}</div>'
                                                         },
                                                         { name: 'od_cost_total_price', displayName: jsGlobals[13], width: 100, cellFilter: 'number:0', cellClass: 'a-right',
                                                             cellEditableCondition: function(scope){
                                                                 if ((scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) && (scope.row.entity.od_cost_unit_price == 0)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_cost_total_price\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="row.entity.od_cost_unit_price == 0 && {\'background-color\': \'#FFCC99\'} || row.entity.od_cost_unit_price != 0 && {\'background-color\': \'#fff\'}">{{row.entity.od_cost_total_price}}</div>'
                                                         },
//#7390: Start
                                                         { name: 'od_gross_profit_amount()', displayName: jsGlobals[14], width: 100, cellFilter: 'number:0', enableCellEdit: false,
                                                             cellTemplate: '<div class="cell-template-data a-right" ng-class="{red: row.entity.od_gross_profit_amount_value < 0}">{{row.entity.od_gross_profit_amount() | number}}</div>'
                                                         },
                                                         { name: 'od_gross_profit_rate()', displayName: jsGlobals[15], width: 100, enableCellEdit: false,
                                                             cellTemplate: '<div class="cell-template-data a-right" ng-class="{red: row.entity.od_gross_profit_rate_value < 0}">{{row.entity.od_gross_profit_rate()}}</div>'
                                                         },
//#7390: End
                                                       ];
            }
            //else if ($scope.order.od_order_status == UNDETERMINED || $scope.order.od_order_status == CONFIRMED) {
                else { // load grid for change report
                $scope.gridOptionsEdit.columnDefs = [
                                                         { name: '処理', displayName: jsGlobals[27], width: 155, enableCellEdit:false,
                                                             cellTemplate:'<button type="button" class="btn primary btn-small btn-green e-width" ng-show="grid.appScope.showCancelBranch(row.entity)" ng-click="grid.appScope.cancelBranch(row.entity)">'+jsGlobals[31]+'</button>'
                                                                         +'<button type="button" class="btn primary btn-small btn-green e-width" ng-show="grid.appScope.showRevertCancelBranch(row.entity)" ng-click="grid.appScope.revertCancelBranch(row.entity)">'+jsGlobals[34]+'</button>'},
//#6983: Start
//                                                         { name: 'change_report', displayName: jsGlobals[28], width: 55, cellTemplate: '<div class="center-checkbox-wrapper" ng-show="row.entity.isCanEdit && (row.entity.od_order_status == 2 || row.entity.od_order_status == 3)"><input type="checkbox" ng-model="row.entity.change_report"></div>', enableCellEdit:false,},
//#7194:Start
                                                         { name: 'change_report', displayName: jsGlobals[28], width: 55, cellTemplate: '<div class="a-center" ng-show="row.entity.isCanEdit && (row.entity.od_order_status == 2 || row.entity.od_order_status == 3) && (row.entity.od_change_report_approval_account_id == 0)"><input type="checkbox" ng-model="row.entity.change_report"></div>', enableCellEdit:false,},
//#7194:End
//#6983: End
                                                         { name: 'od_branch_cd', displayName: jsGlobals[0], width: 50, enableCellEdit:false},
                                                         { name: 'od_is_ad_sales_slip_input',
                                                             displayName: jsGlobals[19],
                                                             width: 50, enableCellEdit:false,
                                                             // cellFilter : 'mapYesNo',
                                                             cellTemplate: ext_link('od_is_ad_sales_slip_input')
                                                             //#6834 Start 2015/09/22
                                                             //visible: $scope.isShowAdReceivePaymentSalesInput
                                                             //#6834 End
                                                        },
                                                        { name: 'od_is_ad_invoice_issue',
                                                             displayName: jsGlobals[20],
                                                             width: 50,
                                                             enableCellEdit:false,
                                                             // cellFilter : 'mapYesNo'
                                                             cellTemplate: ext_link('od_is_ad_invoice_issue')
                                                        },
                                                         { name: 'o_is_order_input',
                                                            displayName: jsGlobals[21],
                                                            width: 50,
                                                            enableCellEdit:false,
                                                            // cellFilter : 'mapYesNo'
                                                            cellTemplate: ext_link('o_is_order_input')
                                                         },
                                                         { name: 'o_is_acceptance_input',
                                                             displayName: jsGlobals[22],
                                                             width: 50,
                                                             enableCellEdit:false,
                                                             // cellFilter : 'mapYesNo'
                                                             cellTemplate: ext_link('o_is_acceptance_input')

                                                         },
                                                         { name: 'o_is_sales_slip_input',
                                                             displayName: jsGlobals[23],
                                                             width: 50,
                                                             enableCellEdit:false,
                                                             // cellFilter : 'mapYesNo'
                                                             cellTemplate: ext_link('o_is_sales_slip_input')
                                                         },
                                                         { name: 'o_is_invoice_issue',
                                                             displayName: jsGlobals[24],
                                                             width: 50,
                                                             enableCellEdit:false,
                                                             // cellFilter : 'mapYesNo'
                                                             cellTemplate: ext_link('o_is_invoice_issue')
                                                         },
                                                         { name: 'od_order_status', displayName: jsGlobals[1], width: 100, enableCellEdit:false, /*cellFilter : 'mapOrderStatus'*/
                                                             cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.o_is_change_report_apply == 1 ? "変レポ申請中" : row.entity.od_order_status_name}}</div>'
                                                         },
                                                         { name: 'p_id', displayName: jsGlobals[3], width: 100,
                                                             editableCellTemplate : 'ui-grid/dropdownEditor',
                                                             cellFilter : 'mapProduct',
                                                             editDropdownValueLabel : 'name',
                                                             editDropdownOptionsArray : products,
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit && scope.row.entity.change_report && (scope.row.entity.od_is_cancel_request != 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             }
                                                         },
                                                         { name: 'od_contents', displayName: jsGlobals[26], width: 100, editableCellTemplate : '<form name="inputForm"><input type="text" maxlength="30" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_contents\']" ui-grid-editor=""></form>',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit && scope.row.entity.change_report && (scope.row.entity.od_is_cancel_request != 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             }
                                                         },
                                                         { name: 'od_delivery_date', displayName: jsGlobals[5], width: 100,
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit && scope.row.entity.change_report && (scope.row.entity.od_is_cancel_request != 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_delivery_date\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                         },
                                                         { name: 'od_billing_date', displayName: jsGlobals[6], width: 100,
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit && scope.row.entity.change_report && ($scope.order.o_is_ad_receive_payment == 1) && (scope.row.entity.od_is_cancel_request != 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_billing_date\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                         },
                                                         { name: 'od_payment_date', displayName: jsGlobals[7], width: 100,
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit && scope.row.entity.change_report && ($scope.order.o_is_ad_receive_payment == 1) && (scope.row.entity.od_is_cancel_request != 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_payment_date\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                         },
                                                         { name: 'od_quantity', displayName: jsGlobals[8], width: 100, cellFilter: 'number:0',
                                                             cellEditableCondition: false
                                                         },
                                                         { name: 'od_tax_type', editableCellTemplate: 'ui-grid/dropdownEditor', cellFilter: 'mapTaxType', editDropdownIdLabel: 'code', editDropdownValueLabel: 'name', editDropdownOptionsArray: tax_division, displayName: jsGlobals[9], width: 40,
                                                             cellEditableCondition: false
                                                         },
                                                         { name: 'od_unit_price', displayName: jsGlobals[10], width: 100, cellFilter: 'number:2',
                                                             cellEditableCondition: false, cellClass: 'a-right'
                                                         },
                                                         { name: 'od_amount()', displayName: jsGlobals[11], width: 100, cellFilter: 'number:0',enableCellEdit:false, cellClass: 'a-right'},
//                                                         { name: 'od_cost_unit_price', displayName: jsGlobals[12], width: 100, cellFilter: 'number:2', enableCellEdit:false},
//                                                         { name: 'od_cost_total_price', displayName: jsGlobals[13], width: 100, cellFilter: 'number:0', enableCellEdit:false},
                                                         { name: 'od_cost_unit_price', displayName: jsGlobals[12], width: 100, cellFilter: 'number:2', cellClass: 'a-right',
                                                             cellEditableCondition: function(scope){
                                                                 if (scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_cost_unit_price\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="{\'background-color\': \'#FFCC99\'}">{{row.entity.od_cost_unit_price}}</div>'
                                                         },
                                                         { name: 'od_cost_total_price', displayName: jsGlobals[13], width: 100, cellFilter: 'number:0', cellClass: 'a-right',
                                                             cellEditableCondition: function(scope){
                                                                 if ((scope.row.entity.isCanEdit || (scope.row.entity.is_new == 1)) && (scope.row.entity.od_cost_unit_price == 0)) {
                                                                     form_changed = true;
                                                                     return true;
                                                                 }
                                                                 return false;
                                                             },
                                                             editableCellTemplate : '<form name="inputForm"><input type="tel" ng-class="\'colt\' + col.uid" ng-model="row.entity[\'od_cost_total_price\']" ng-style="{\'ime-mode\': \'disabled\'}" ui-grid-editor=""></form>'
                                                             //cellTemplate: '<div class="cell-template-data" ng-style="row.entity.od_cost_unit_price == 0 && {\'background-color\': \'#FFCC99\'} || row.entity.od_cost_unit_price != 0 && {\'background-color\': \'#fff\'}">{{row.entity.od_cost_total_price}}</div>'
                                                         },
//#7390: Start
                                                         { name: 'od_gross_profit_amount()', displayName: jsGlobals[14], width: 100, cellFilter: 'number:0', enableCellEdit: false,
                                                             cellTemplate: '<div class="cell-template-data a-right" ng-class="{red: row.entity.od_gross_profit_amount_value < 0}">{{row.entity.od_gross_profit_amount() | number}}</div>'
                                                         },
                                                         { name: 'od_gross_profit_rate()', displayName: jsGlobals[15], width: 100, enableCellEdit: false,
                                                             cellTemplate: '<div class="cell-template-data a-right" ng-class="{red: row.entity.od_gross_profit_rate_value < 0}">{{row.entity.od_gross_profit_rate()}}</div>'
                                                         },
//#7390: End
                                                       ];
            }

            angular.forEach(data, function(value, key) {
                value.od_amount_value =  value.od_amount;
                value.od_gross_profit_amount_value =  value.od_gross_profit_amount;
                value.od_gross_profit_rate_value =  value.od_gross_profit_rate;
                value.shift_color_cd = '';
                value.od_delivery_date = value.od_delivery_date.replace(/-/g, "/");
                value.od_payment_date = value.od_payment_date.replace(/-/g, "/");
                value.od_billing_date = value.od_billing_date.replace(/-/g, "/");
                value.delivery_month = (new Date(value.od_delivery_date).getFullYear() * 100) + new Date(value.od_delivery_date).getMonth();
                value.disable = 0;
            });
            $scope.gridOptionsEdit.data = data;

            angular.forEach($scope.gridOptionsEdit.data, function(value, key) {
                value.od_amount = function () {
                    result = f_floor(this.od_quantity * this.od_unit_price);
                    result = result || 0;
                    this.od_amount_value = result;
                    return result;
                };
                value.od_gross_profit_amount = function () {
                    var gross_profit_amount;
                    if (this.od_tax_type == 2) {
                        var tax_rate;
                        var delivery_date = new Date(this.od_delivery_date);
                        var arr = jQuery.grep($rootScope.tax_rates, function( i ) {
                            var da = new Date(i.adaptation_date);
                            delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                            da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                            return da <= delivery_date;
                        });
                        var tax_rates = [];
                        $.each(arr, function(key, value){
                            tax_rates.push(value.rate);
                        });
                        tax_rate = Math.max.apply(Math, tax_rates);
                        gross_profit_amount = f_ceil(this.od_amount()/(1 + (tax_rate/100))) - this.od_cost_total_price;
                    }
                    else {
                        gross_profit_amount = this.od_amount() - this.od_cost_total_price;
                    }
//#7390:Start
//                    this.od_gross_profit_amount_value = (gross_profit_amount >= 0) ? gross_profit_amount : 0;
                    this.od_gross_profit_amount_value = gross_profit_amount;
//#7390:End
                    return this.od_gross_profit_amount_value;
                };
                value.od_gross_profit_rate = function () {
                    if (this.od_amount() <= 0) {
                        return '0.00 %';
                    }
                    var gross_profit_rate;
                    if (this.od_tax_type == 2) {
                        var tax_rate;
                        var delivery_date = new Date(this.od_delivery_date);
                        var arr = jQuery.grep($rootScope.tax_rates, function( i ) {
                            var da = new Date(i.adaptation_date);
                            delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                            da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                            return da <= delivery_date;
                        });
                        var tax_rates = [];
                        $.each(arr, function(key, value){
                            tax_rates.push(value.rate);
                        });
                        tax_rate = Math.max.apply(Math, tax_rates);
                        tmp_amount = f_ceil(this.od_amount() / (1 + (tax_rate / 100)));
                        gross_profit_rate = f_floor((this.od_gross_profit_amount() / tmp_amount) * 100 * 100) / 100;
                    }
                    else {
                        gross_profit_rate = f_floor(this.od_gross_profit_amount() / this.od_amount() * 100 * 100) / 100;
                    }
//#7390:Start
//                    this.od_gross_profit_rate_value = (gross_profit_rate >= 0) ? gross_profit_rate : 0;
                    this.od_gross_profit_rate_value = gross_profit_rate;
//#7390:End
                    return this.od_gross_profit_rate_value.toFixed(2) + ' %';
                };

                if ( ! $scope.waiting_change_report && value.od_order_status == 1) {
                    $scope.isShowAddButton = true;
                    $scope.isShowEditButtonSection = true;
                    $scope.isShowChangeReportButtonSection = false;
                } else {
                     $scope.isShowAddButton = false;
                     $scope.isShowEditButtonSection = false;
                }

                if ($scope.waiting_change_report && (value.od_order_status == 2 || value.od_order_status == 3)) {
                    $scope.isShowChangeReportButtonSection = true;
                    $scope.isShowEditButtonSection = false;

                    if (value.isCanEdit) {
                        $scope.isShowAdReceivePaymentSalesInput = true;
                    } else {
                        $scope.isShowAdReceivePaymentSalesInput = false;
                    }
                } else {
                    $scope.isShowAdReceivePaymentSalesInput = false;
                }
            });

            $scope.order.o_cost_total_price = 0;
            $scope.order.o_consumption_tax = 0;
            $scope.order.o_tax_total = 0;
            $scope.order.o_gross_profit_total = 0;
            $scope.sum();
        });
    };
    // <!----------------------------------------------END LOAD EDIT---------------------------------------------->
    // <!---------------------------CRTL + S---------------------------->
    $(document).bind('keydown', function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
//#9160:Start
            if ($("#dialogEdit").dialog( "isOpen" )===true) {
            	$scope.changeReport(false);
            }
//#9160:End
            return false;
            // $scope.save();
            // return false;
        }
    });
    $scope.changeReport = function(is_change_report) {
//#8222:Start
        $(".both-blind").show();
//#8222:End
//#9160:Start
//    	console.log($scope.gridOptionsEdit.data);

        angular.forEach($scope.gridOptionsEdit.data, function(item) {
//#9335:Start
//            if (item.od_cost_unit_price) {
            if (item.od_cost_unit_price && item.od_cost_unit_price!= 0 ) {
//#9335:End
                item.od_cost_total_price = (new BigNumber(item.od_cost_unit_price)).mul(item.od_quantity).floor().toNumber();
                item.od_cost_total_price = item.od_cost_total_price || 0;
            }
        });
//#9160:End
        var branchs_del = $scope.gridOptionsEditDelete;
        var branchs = $scope.gridOptionsEdit.data;

        if ( ! form_changed) {

//#8222:Start
            $(".both-blind").hide();
//#8222:End

            $("#message_alert").html(jsGlobals[35]);
            if (is_change_report) {
                $("#message_alert").html(jsGlobals[36]);
            }

            $("#message_alert").dialog({
                title: '注意',
                width: 'auto',
                height: 147,
                modal: true,
                dialogClass: "order-dialog",
                buttons: [{
                    text: "OK",
                    click: function() {
                    $(this).dialog("close");
                    }
                }],
            });
            return;
        }

        $http({
            url : '/_admin/order/change_report',
            method : 'POST',
            data : {
                order : $scope.order,
                branchs : branchs,
                branchs_del: branchs_del,
                changed_report_type: $scope.waiting_change_report,
                is_change_report: is_change_report
            }
        }).success(function(data) {

//#8222:Start
        	$(".both-blind").hide();
//#8222:End

            $scope.errors = '';
            var errors = '';
            for(var i = 0; i < $scope.gridApi.grid.rows.length; i ++) {
                    $scope.gridApi.grid.rows[i].invalid = false;
            }
            if (data.error_string || data.error_string_branchs) {

                if(data.error_string) {
                    errors += '<div><p>' + data.error_string +'</p></div>';
                }

                angular.forEach(data.error_string_branchs, function(item, key) {
                    for(var j = 0; j < $scope.gridApi.grid.rows.length; j ++) {
                        if($scope.gridApi.grid.rows[j].entity.od_branch_cd == key) {
                            $scope.gridApi.grid.rows[j].invalid = true;
                        }
                    }
                    errors += '<span class="title-icon">枝' + key + ' を修正して下さい。</span>'
                        + '<div><p>' + item + '</p></div>';
                });
                $("#dialog-errors").html('<div class="errors_dialog">'+errors+'</div>');

                $("#dialog-errors").dialog({
                    title: '注意',
                    minWidth: 560,
                    minHeight: 250,
                    maxHeight: 250,
                    resizable: true,
                    modal: false,
                    dialogClass: 'order-dialog fixed-dialog',
                    position: { my: "right top+30", at: "right top+30", of: window},
                    dragStart: function(event, ui) {
                        $('.order-dialog').removeClass('fixed-dialog')
                    },
                    dragStop: function(event, ui) {
                        // $('.acceptance-dialog').addClass('fixed-dialog')
                    },
                    resizeStart: function( event, ui ) {
                        $(this).dialog("option", "maxHeight", false);
                    }
                });
            }
            else {

//#8222:Start
                $(".both-blind").hide();
//#8222:End
                if ($("#dialog-errors").hasClass('ui-dialog-content')) {
                    $("#dialog-errors").dialog('destroy');
                }

                $scope.is_submitted = true;
                // Save edit
                if($scope.order.od_order_status==1) {
                    $scope.error_success = '<div class="success-message blink_me">保存しました。</div>';
                }
                // Change report
                else if($scope.order.od_order_status==2 || $scope.order.od_order_status==3) {
                    $scope.error_success = '<div class="success-message blink_me">変レポ申請をしました。</div>';
                }
//#8133:Start
                $rootScope.search(true);
//#8133:End
            }
        })
    }

  //<!-------------------------------DELETE ORDER---------------------------------------------->
    $scope.deleteOrder = function() {

        confirm_dialog(' 削除します。よろしいですか？', {
            'delete': function() {
                $(this).dialog('close');
                $http({
                    url : '/_admin/order/delete',
                    method : 'POST',
                    data : {
                        order_id : $scope.order.o_id
                    }
                }).success(function(data) {
                    $scope.is_submitted = true;
                    $scope.errors = '';
                    $scope.error_success = '<div class="success-message blink_me">削除しました。</div>';
//#8133:Start
                	data_check_approved.remove($scope.order.o_id);
                    $rootScope.search(true);
//#8133:End
                })
            }
        });
    }

    $scope.back = function() {
        $('#dialogEdit').dialog("close");
    }

    $scope.addData = function() {
        form_changed = true;
        var gridData = $scope.gridOptionsEdit.data;
        if(gridData.length>23) {
            return false;
        }
        gridData.push({
                    "od_order_status" : 1,
                    "od_branch_cd" : pad(gridData.length + 1, 2),
                    "od_quantity": 0,
                    "od_tax_type": 1,
                    "od_unit_price": 0,
                    "od_delivery_date": "",
                    "od_cost_unit_price" : 0,
                    "od_cost_total_price": 0,
                    "isCanEdit": true,
                    "is_new": 1,
                });

        var row = gridData[gridData.length - 1];
        row.od_amount = function () {
            result = f_floor(this.od_quantity * this.od_unit_price);
            result = result || 0;
            this.od_amount_value = result;
            return result;
        };
        row.od_gross_profit_amount = function () {
            var gross_profit_amount;
            if (this.od_tax_type == 2) {
                var tax_rate;
                var delivery_date = new Date(this.od_delivery_date);
                var arr = jQuery.grep($rootScope.tax_rates, function( i ) {
                    var da = new Date(i.adaptation_date);
                    delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                    da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                    return da <= delivery_date;
                });
                var tax_rates = [];
                $.each(arr, function(key, value){
                    tax_rates.push(value.rate);
                });
                tax_rate = Math.max.apply(Math, tax_rates);

                gross_profit_amount = f_ceil(this.od_amount()/(1 + (tax_rate/100))) - this.od_cost_total_price;
            }
            else {
                gross_profit_amount = this.od_amount() - this.od_cost_total_price;
            }
//#7390:Start
//            this.od_gross_profit_amount_value = (gross_profit_amount >= 0) ? gross_profit_amount : 0;
            this.od_gross_profit_amount_value = gross_profit_amount;
//#7390:End
            return this.od_gross_profit_amount_value;
        };
        row.od_gross_profit_rate = function () {
            if (this.od_amount() <= 0) {
                return '0.00 %';
            }

            var gross_profit_rate;
            if (this.od_tax_type == 2) {
                var tax_rate;
                var delivery_date = new Date(this.od_delivery_date);
                var arr = jQuery.grep($rootScope.tax_rates, function( i ) {
                    var da = new Date(i.adaptation_date);
                    delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                    da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                    return da <= delivery_date;
                });
                var tax_rates = [];
                $.each(arr, function(key, value){
                    tax_rates.push(value.rate);
                });

                tax_rate = Math.max.apply(Math, tax_rates);
                tmp_amount = f_ceil(this.od_amount() / (1 + (tax_rate / 100)));
                gross_profit_rate = f_floor((this.od_gross_profit_amount() / tmp_amount) * 100 * 100) / 100;
            }
            else {
                gross_profit_rate = f_floor(this.od_gross_profit_amount() / this.od_amount() * 100 * 100) / 100;
            }
//#7390:Start
//            this.od_gross_profit_rate_value = (gross_profit_rate >= 0) ? gross_profit_rate : 0;
            this.od_gross_profit_rate_value = gross_profit_rate;
//#7390:End
            return this.od_gross_profit_rate_value.toFixed(2) + ' %';
        };
    }

    $scope.gridOptionsEditDelete = [];
    $scope.deleteRow = function(row) {
        form_changed = true;
        /*confirm_dialog('削除します。よろしいですか？', {
            'delete': function() {*/
                row.disable = 1;
                $scope.gridOptionsEditDelete.push(row);
                var indexRow = $scope.gridOptionsEdit.data.indexOf(row);
                $scope.gridOptionsEdit.data.splice(indexRow, 1);
                // Refresh grid
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                var i = 0;
                angular.forEach($scope.gridOptionsEdit.data, function(item) {
                    i++;
                    item.od_branch_cd = pad(i, 2);
                });
                $scope.sum();
       $scope.compute_credit();
    }; // END DELETE
    $scope.cancelBranch = function(row) {
        if(row.o_is_order_input==1
                || row.o_is_acceptance_input==1
                || row.o_is_sales_slip_input==1
                || row.o_is_invoice_issue==1
//#6956: Start
//                || row.od_is_ad_sales_slip_input==1
//                || row.od_is_ad_invoice_issue==1
//#6956: End
        ) {
            $("#message_alert").html("枝キャンセルできません。").dialog({
                title: "注意",
                width: 'auto',
                height: 'auto',
                modal : true,
                buttons: {
                    "OK": function() {
                        $(this).dialog('close');
                    }
                },
                dialogClass: "order-dialog",
            });

                return;
            }

        $("#message_alert").html("枝キャンセルしてもよろしいですか？").dialog({
            title: "注意",
            width: 'auto',
            height: 'auto',
            modal : true,
            buttons: {
                "OK": function() {
                    $(this).dialog('close');
                    angular.forEach($scope.branchs_old, function(item) {
                        if (item.od_id == row.od_id) {
                            row.p_id = item.p_id;
                            row.od_contents = item.od_contents;
                            var d_date = new Date(item.od_delivery_date);
                            row.od_delivery_date = d_date.getFullYear()+"/"+(d_date.getMonth()+1)+"/"+d_date.getDate();
                            d_date = new Date(item.od_billing_date);
                            row.od_billing_date = d_date.getFullYear()+"/"+(d_date.getMonth()+1)+"/"+d_date.getDate();
                            d_date = new Date(item.od_payment_date);
                            row.od_payment_date = d_date.getFullYear()+"/"+(d_date.getMonth()+1)+"/"+d_date.getDate();
                            row.od_quantity = item.od_quantity;
                            row.od_tax_type = item.od_tax_type;
                            row.od_unit_price = item.od_unit_price;
                        }
                    });
                    $scope.$apply(function () {
                        row.change_report = true;
                        row.od_is_cancel_request = 1;
                    });
                }
            },
            dialogClass: "order-dialog",
        });
        form_changed = true;
    }; // End $scope.cancelBranch = function(row) {

    $scope.revertCancelBranch = function(row) {
        row.change_report = false;
        row.od_is_cancel_request = 0;
        form_changed = true;
    };

    $scope.copyRow = function(row, api) {
        form_changed = true;
        if(api.grid.rows.length > 23) {
            return false;
        }
//#7648:Start
//        var indexRow = $scope.gridOptionsEdit.data.indexOf(row);
//#7648:End
        var newrow = angular.copy(row);
        newrow.od_id = '';
        newrow.isCanEdit = true;
        newrow.is_new = 1;
//#7648:Start
//        $scope.gridOptionsEdit.data.splice(indexRow + 1, 0, newrow);
        $scope.gridOptionsEdit.data.splice(api.grid.rows.length + 1, 0, newrow);
//#7648:End
        var i = 0;
        angular.forEach($scope.gridOptionsEdit.data, function(item) {
            i++;
            item.od_branch_cd =  pad(i, 2);
        });
        $scope.sum();
        $scope.compute_credit();
    };

  //<!------------------------------------ Set payament date when change delivery date -------------------------------------->
    var checkHolidayAndSetPaymentDate = function(year, month, day_delivery, date, item) {

        month = month + 1;
        // month 10 => Deccember
        payment_date = new Date(year, month, 0); // Month october (9) has 31 days
        var noofday = payment_date.getDate(); // = 31

        if(date > noofday) {
            date = noofday;
        }
        // month 10 -> December
        payment_date = new Date(year, month-1, date);

        payment_date_month = payment_date.getMonth() + 1;
        payment_date_month = (payment_date_month < 10) ? '0' + payment_date_month : payment_date_month;

        payment_date = payment_date.getFullYear() + '/' + payment_date_month + '/' + date;
        //#6992:Start
        if (!item.od_payment_date_without_check_holiday) {
            item.od_payment_date_without_check_holiday = payment_date;
        }
        //#6992:End
        $http({
            url : '/_admin/holiday/check_holiday',
            method : 'POST',
            data : {
                date : payment_date
            }
        }).success(function(data) {
                        if (data == 0) {
                            //#6992:Start
                            // If payment date is not holiday date => keep the same
                            // item.od_payment_date = payment_date;
                            var tmp_billing_date_obj = new Date(item.od_billing_date);
                            var tmp_payment_date_obj = new Date(payment_date);
                            if (tmp_payment_date_obj.getTime() < tmp_billing_date_obj.getTime()) {
                                date = new Date(item.od_payment_date_without_check_holiday).getDate();
                                checkHolidayAndSetPaymentDate(year, month, day_delivery, date, item);
                            } else {
                                item.od_payment_date = payment_date;
                                delete(item.od_payment_date_without_check_holiday);
                            }
                            //#6992:End
                        } else {
                            // If payment date is holiday date, pay before one day
                            if(date>1) {
                                date = date - 1;
                            } else {
                                month = month - 1;
                                date = (new Date(year, month, 0)).getDate();
                            }
                            checkHolidayAndSetPaymentDate(year, month-1, day_delivery, date, item);
                        }
                    });
    }

    //<!--------------------------- Set billing date when change delivery date --------------------------------------->
    var checkHolidayAndSetBillingDate = function(year, month, day_delivery, closing_date, item) {

        month = month + 1;//13

        var billing_date = '';

        var date_obj = new Date(year, month, 0);//month: 13
        var last_date_of_month = date_obj.getDate();

        if (day_delivery > closing_date) { // delivery_date = 2015/12/31 => billing_date = 2016/01/20

            if (closing_date > last_date_of_month) {
                closing_date = last_date_of_month;
            }

        } else {

            if (closing_date > last_date_of_month) {
                closing_date = last_date_of_month;
            }
        }

        var billing_date_month = '';
        billing_date_month = date_obj.getMonth() + 1;
        billing_date_month = (billing_date_month < 10) ? '0' + billing_date_month : billing_date_month;

        billing_date = (date_obj.getFullYear()) + '/' + billing_date_month + '/' + closing_date;

        item.od_billing_date = billing_date;
    }

    //<!-------------------------------- Set payment date and billing date following delivery date------------------------------>
    $scope.setPaymentDateAndBillingDate = function (item) {
        // item: rowEntity
        if ($scope.order.o_is_ad_receive_payment == "0") {
            // Billing date
            var year = new Date(item.od_delivery_date).getFullYear();
            var month = new Date(item.od_delivery_date).getMonth();
            var day_delivery = new Date(item.od_delivery_date).getDate();
            // If delivery date > closing date => Billing in next month
            if (day_delivery > $scope.order.c_closing_date) {
                month = month + 1;
            }
            checkHolidayAndSetBillingDate(year, month, day_delivery, parseInt($scope.order.c_closing_date), item);
            // Payment date = payment date + month pending from DB
            month = month + parseInt($scope.order.c_payment_month_cd);
            checkHolidayAndSetPaymentDate(year, month, day_delivery, parseInt($scope.order.c_payment_day_cd), item);
        } else {
            item.od_payment_date = "";
            item.od_billing_date = "";;
        }
    }; // End $scope.setPaymentDateAndBillingDate = function (item) {

    $scope.sum = function() {
        // 税別合計
        $scope.order.o_cost_total_price = 0;
        // 消費税
        $scope.order.o_consumption_tax = 0;
        // 税込合計
        $scope.order.o_tax_total = 0;
        // 粗利合計
        $scope.order.o_gross_profit_total = 0;

        angular.forEach($scope.gridOptionsEdit.data, function(item) {
//#8216:Start
            if (item.od_delivery_date && (item.od_is_cancel_request!=1) && (item.od_order_status != REJECTED) && (item.od_order_status != CANCELLED)) {
//#8216:End
                var tax_rate;
                var delivery_date = new Date(item.od_delivery_date);
                var arr = jQuery.grep($rootScope.tax_rates, function (i) {
                    var da = new Date(i.adaptation_date);
                    delivery_date = new Date(delivery_date.getFullYear(),(delivery_date.getMonth()),delivery_date.getDate());
                    da = new Date(da.getFullYear(),(da.getMonth()),da.getDate());
                    return da <= delivery_date;
                });
                var tax_rates = [];
                $.each(arr, function (key, value) {
                    tax_rates.push(value.rate);
                });
                tax_rate = Math.max.apply(Math, tax_rates);

                // Only tax type is 「内」 has the tax included in amount
                /*var no_tax_amount = (item.od_tax_type != 2) ? item.od_amount() : (item.od_amount() / (1 + tax_rate/100));
                $scope.order.o_cost_total_price += no_tax_amount;
                // Only tax type is 「非」 is nontaxable, means no tax needed for the product price
                $scope.order.o_consumption_tax += (item.od_tax_type != 3) ? (no_tax_amount * (tax_rate / 100)) : 0;
                $scope.order.o_gross_profit_total += item.od_gross_profit_amount();*/


                if(item.od_tax_type == 2) {
                    // (②/(1+税率)+小数点以下切り上げ)
                    $scope.order.o_cost_total_price += f_ceil(item.od_amount()/(1+tax_rate/100));
                    // (②-(②/(1+税率)+小数点以下切り上げ))
                    $scope.order.o_consumption_tax += (item.od_amount() - f_ceil(item.od_amount()/(1+tax_rate/100)));
                    // ②+③
                    $scope.order.o_tax_total+=item.od_amount();
                }
                if(item.od_tax_type == 1 ) {
                    // (①*税率+小数点以下切り捨て)
                    $scope.order.o_consumption_tax += f_floor(item.od_amount()*tax_rate/100);
                    // ①+③
                    $scope.order.o_cost_total_price += item.od_amount();
                    // ①*(1+税率)+小数点以下切り捨て
                    $scope.order.o_tax_total += f_floor(item.od_amount()*(1+tax_rate/100));
                }
                if(item.od_tax_type == 3) {
                    // ①+③
                    $scope.order.o_cost_total_price += item.od_amount();
                    // ②+③
                    $scope.order.o_tax_total+=item.od_amount();
                }
                $scope.order.o_gross_profit_total += item.od_gross_profit_amount();
            }
        });

        //$scope.order.o_cost_total_price = Math.ceil($scope.order.o_cost_total_price);
        //$scope.order.o_consumption_tax = Math.floor($scope.order.o_consumption_tax);
        //$scope.order.o_tax_total = $scope.order.o_cost_total_price + $scope.order.o_consumption_tax;
    }
    $scope.compute_credit = function() {
        // show errors on dialog
        for(var i = 0; i < $scope.gridApi.grid.rows.length; i ++) {
            $scope.gridApi.grid.rows[i].invalid = false;
        }

        if ($scope.order.o_is_ad_receive_payment == 0) {
            var order_id = $scope.order.o_id;
            var client_id = $scope.order.o_client_id;
            $http({
                url : '/_admin/client/ajax_check_payment',
                method : 'POST',
                data : {client_id : client_id, order_id: order_id, branchs: $scope.gridOptionsEdit.data }
            }).success(function(data) {
                if (data.is_over_credit) {
                    $scope.is_credit_over = true;
                    angular.forEach(data.over_credit, function(item, key) {
                        for(var j = 0; j < $scope.gridApi.grid.rows.length; j ++) {
                            if($scope.gridApi.grid.rows[j].entity.od_branch_cd == key) {
                                $scope.gridApi.grid.rows[j].invalid = true;
                            }
                        }
                    });
                    $("#message_alert").html("<span class='validation-error'>与信限度額を超えています。</span>").dialog({
                        title: "注意",
                        width: 'auto',
                        height: 'auto',
                        modal : true,
                        buttons: {
                            "OK": function() {
                                $(this).dialog('close');
                            }
                        },
                        dialogClass: "order-dialog",
                    });
                } else {
                    $scope.is_credit_over = false;
                }
            });
        } else {
             $scope.is_credit_over = false;
        }
    }

    $scope.gridOptionsEdit.onRegisterApi = function(gridApi) {
        $scope.gridApi = gridApi;
//7085:Start
        // gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
//#7623:Start
        gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue) {

            // delivery_date - just check only new delivery date
            if (colDef.name == 'od_delivery_date' && (newValue != oldValue)) {

                // assign color when change rp change month delivery date
                rowEntity.new_delivery_month = (new Date(rowEntity.od_delivery_date).getFullYear() * 100) + new Date(rowEntity.od_delivery_date).getMonth();

                if (rowEntity.new_delivery_month > rowEntity.delivery_month) {
                    rowEntity.shift_color_cd = '#C9D219';
                } else if (rowEntity.new_delivery_month < rowEntity.delivery_month) {
                    rowEntity.shift_color_cd = '#0459D0';
                } else {
                    rowEntity.shift_color_cd = '';
                }                

                inputArray = rowEntity.od_delivery_date.split('/');
                rowEntity.od_delivery_date = convertDate(inputArray);

                // calculate delivery date follow closing date.
                var delivery_date = new Date(rowEntity.od_delivery_date);
                if (!isNaN(delivery_date)) {
                    var fd = new Date();
                    var last_date_of_month = new Date(fd.getFullYear(), (fd.getMonth()+1), 0).getDate();
                    var closing_date = $scope.order.od_closing_date;
                    if(closing_date > last_date_of_month) {
                        closing_date = last_date_of_month;
                    }
                    var closing_date = new Date(fd.getFullYear(), fd.getMonth(), closing_date);
                    if (delivery_date <= closing_date) {
                        rowEntity.od_delivery_date = fd.getFullYear() + '/' + (fd.getMonth() + 1) + '/' + $scope.order.od_closing_date;
                    }
                } else {
                    rowEntity.od_delivery_date = '';
                }

                // calculate billing_date & payment_date follow delivery_date
                if(rowEntity.od_delivery_date && $scope.order.o_is_ad_receive_payment == 0) {
                    $scope.setPaymentDateAndBillingDate(rowEntity);
                }

            }
            // Check valid od_billing_date
            if (rowEntity.od_billing_date) {
                inputArray = rowEntity.od_billing_date.split('/');
                rowEntity.od_billing_date = convertDate(inputArray);
                var billing_date = new Date(rowEntity.od_billing_date);
                if (isNaN(billing_date)) {
                    rowEntity.od_billing_date = '';
                }
            }
            // Check valid od_payment_date
            if (rowEntity.od_payment_date) {
                inputArray = rowEntity.od_payment_date.split('/');
                rowEntity.od_payment_date = convertDate(inputArray);
                var payment_date = new Date(rowEntity.od_payment_date);
                if (isNaN(payment_date)) {
                    rowEntity.od_payment_date = '';
                }
            }

            if (colDef.name == 'od_cost_unit_price' && (newValue == '' || !jQuery.isNumeric(newValue) || newValue < 0)) {
                rowEntity.od_cost_unit_price = 0;
            }
            if (colDef.name == 'od_cost_total_price' && (newValue == '' || !jQuery.isNumeric(newValue) || newValue < 0)) {
                rowEntity.od_cost_total_price = 0;
            }
            if (colDef.name == 'od_quantity' && (newValue == '' || !jQuery.isNumeric(newValue) || newValue < 0)) {
                rowEntity.od_quantity = 0;
            }

            rowEntity.od_tax_type = parseInt(rowEntity.od_tax_type)
//#7441:Start
//        rowEntity.od_unit_price = Math.floor(rowEntity.od_unit_price * 100) / 100;
            rowEntity.od_unit_price = Math.floor(strip(rowEntity.od_unit_price * 100)) / 100;
//#7441:End
            rowEntity.od_unit_price = rowEntity.od_unit_price || 0;
// #7610:Start
// if (rowEntity.od_cost_unit_price) {


        if (rowEntity.od_cost_unit_price != 0) {
// #7610:End

//#7441:Start
//            rowEntity.od_cost_unit_price = Math.floor(rowEntity.od_cost_unit_price * 100) / 100;
                rowEntity.od_cost_unit_price = Math.floor(strip(rowEntity.od_cost_unit_price * 100)) / 100;
//#7441:Start
                rowEntity.od_cost_unit_price = rowEntity.od_cost_unit_price || 0;
                rowEntity.od_cost_total_price = f_floor(rowEntity.od_cost_unit_price * rowEntity.od_quantity);
                rowEntity.od_cost_total_price = rowEntity.od_cost_total_price || 0;
            }

            $scope.sum();
            $scope.compute_credit();
//            $scope.$apply();
//7085:End
        });
//#7623:End
    }

}])
