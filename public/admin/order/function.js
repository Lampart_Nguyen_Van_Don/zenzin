/**
 * Common functions use in client feature
 */

var storageSupport = (typeof(Storage) !== "undefined") ? true : false;

if (typeof od_functions_loaded === 'undefined' || !od_functions_loaded) {

    function saveData(key, value) {
        if (storageSupport) {
            localStorage.setItem(key, value);
        } else {
            cookies[key] = value;
        }
    }

    function loadData(key) {
        if (storageSupport) {
            return localStorage.getItem(key);
        } else {
            return cookies[key];
        }
    }

    function hideSearch(animate) {
        $('#search_toggle').css('width', $('#search_body').width());
        $('#search_toggle td').css('border-bottom', '1px solid #ddd');
        $('#search_toggle a').text(lbl_open_search);
        if (animate) {
            $('#search_body').slideUp();
        } else {
            $('#search_body').hide();
        }

        saveData('cl_showSearchOrder', 0);
    }

    function showSearch(animate) {
        $('#search_toggle').css('width', '100%');
        $('#search_toggle td').css('border-bottom', 'none');
        $('#search_toggle a').text(lbl_hide_search);
        if (animate) {
            $('#search_body').slideDown();
        } else {
            $('#search_body').show();
        }

        saveData('cl_showSearchOrder', 1);
    }

    od_functions_loaded = true;
}

$(document).ready(function() {
    
    // Delivery Statement Export
    $('.table-export .btn-downloads').click(function(e) {
        e.preventDefault();
        
        var $button     = $(this);
        var date_from   = $('.table-export input[name="date_from"]');
        var date_to     = $('.table-export input[name="date_to"]');
        var error_container =  $('#error-container');
        
        $button.addClass('loading icon-spin').blur();
        
        error_container.fadeOut('fast');
        
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/_admin/order/ajax_convert_date',
            data: ({
                date_from: date_from.val(),
                date_to: date_to.val()
            }),
            complete: function() {
                $button.removeClass('loading icon-spin');
            },
            success: function(data) {
                if (data.status == 'success') {
                    date_from.val(data.date.date_from);
                    date_to.val(data.date.date_to);
                    $('.import-container form').submit();
                } else {
                    if (data.date) {
                        date_from.val(data.date.date_from);
                        date_to.val(data.date.date_to);
                    }
                    error_container.html('<div class="error-message">' + data.msg + '</div>').fadeIn('fast');
                }
            },
            error: function(e){
                alert('An error occurred: ' + e.responseText.message);
            }
        });
        
        return false;
    });
    
});