/* start volyminhnhan@gmail.com modification*/
var data_check_approved = Array();
var loaded_template = {};
var keepcheckval = false;
var set_j_account;
$( document ).ready(function() {
   $( window ).scrollTop( 0 );
//#7525:Start
   set_j_account = function() {
        $('#j_account').html('');
        $('#j_account').append($('<option>', {
            value: 0,
            text : '＝＝すべて＝＝'
        }));
        $.each(account_list, function (i, item) {

            if ($('#j_business_unit').val() != 0 && item.bu_id == $('#j_business_unit').val()) {
                $('#j_account').append($('<option>', {
                    value: item.id,
                    text: item.name
                }));
            }

            else if ($('#j_business_unit').val() == 0 || item.bu_id == $('#j_business_unit').val()) {
                $('#j_account').append($('<option>', {
                    value: item.id,
                    text : item.name
                }));
            }
        });
    }
    set_j_account();

    // Check devision of business unit of account login.
    if (my_account.is_product_division == 1) {
        $("#j_account").val(my_account.id);
    }
//#7525:End
});
/* end volyminhnhan@gmail.com modification*/

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}



$('#j_business_unit').change(function () {
    // $('#j_account').val("0");
    set_j_account();
    //$('#j_account').val("0");
});

var set_s_account = function() {
    $('#s_account').html('');
    $('#s_account').append($('<option>', {
        value: 0,
        text : '＝＝すべて＝＝'
    }));
    $.each(account_list, function (i, item) {
        if (item.bu_id == $('#s_business_unit').val() || $('#s_business_unit').val() == 0) {
            $('#s_account').append($('<option>', {
                value: item.id,
                text : item.name
            }));
        }
    });
}
set_s_account();
$('#s_business_unit').change(function () {
    set_s_account();
});

/*if (my_account.is_product_division == 1) {
    $("#j_account").val(my_account.id);
    $("#s_account").val(my_account.id);
}*/
//#7577: change id export order to class export order
$('.export_order').click(function () {
    $("form[id='searchform']").attr("action", "/_admin/order/export_order");
    $("form[id='searchform']").submit();
});

//#7846:Start
// var app = angular.module('app', ['ngTouch', 'ngSanitize', 'ui.grid', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.pagination', 'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.cellNav']);
var app = angular.module('app', ['ngSanitize', 'ui.grid', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.pagination', 'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.cellNav']);
//#7846:End

app.controller('MainCtrl', ['$scope', '$rootScope', '$http', '$q', '$interval', 'uiGridConstants', '$controller', function ($scope, $rootScope, $http, $q, $interval, uiGridConstants, $controller, $compile) {
    $rootScope.lang = 'ja';
    $scope.gridOptions = {
        selectionRowHeaderWidth: 35,
        rowEditWaitInterval: -1,
        rowTemplate:
            '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ui-grid-cell ng-class="{\'orange-row\': (row.entity.od_order_status==4), \'gray-row\': (row.entity.od_order_status==9), \'green-row\': (row.entity.od_is_change_report_apply==1)}" ></div>',
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
        }
    };
    $scope.gridOptions.enableCellEditOnFocus = false;
    $scope.gridOptions.multiSelect = true;
    $scope.gridOptions.enableCellEdit = false;

    $('#slide-left-btn').click(function () {
        $interval(function () {
            $scope.gridApi.core.handleWindowResize();
        }, 500, 1);
    });

    $scope.show_approved_cell = function(row) {

//#8133:Start
        var $orderid =  row.entity.order_id;
        var $find_orderid = jQuery.inArray( $orderid, data_check_approved);

        if(keepcheckval){
        	if($find_orderid != -1){
            	row.entity.cbx_arpprove  =  true;
            }
            else{
            	 row.entity.cbx_arpprove =  row.entity.cbx_arpprove;
            }
        }

//#8133:End

    //tmp#6841: Start 2015/09/24
        if ( (row.entity.od_order_status == 1 && row.entity.od_branch_cd == '01') && (row.entity.isCanEdit == true) && (bu_id_user_logged == row.entity.bu_id)) {
            return true;
        }
    //tmp#6841: End
        return false;
    }

    $rootScope.loadTemplate = function () {
        var template_name = arguments[0];
        if (!loaded_template.hasOwnProperty(template_name)) {
            var dialog;
            switch (template_name) {
                case 'add':
                    var order_id = arguments[1];
                    dialog = $('#dialogAdd');
                    break;
                case 'detail':
                    var detail_id = arguments[1];
                    dialog = $('#dialog');
                    break;
                case 'edit':
                    var order_id = arguments[1];
                    var form = arguments[2];
                    dialog = $('#dialogEdit');
                    break;
                case 'approve_reject':
                    var detail_data = arguments[1];
                    var order = arguments[2];
                    dialog = $('#dialog_approve_reject');
                    break;
                case 'change_report':
                    var order_id = arguments[1];
                    dialog = $('#dialog_change_report');
                    break;
            }

            $.post(
                '/_admin/order/load_template',
                {template: template_name},
                function (data) {
                    loaded_template[template_name] = true;
                    dialog.html(data);
                    var scope = angular.element(dialog).scope();
                    $compile(dialog)(scope);
                    //scope.$apply();
                    switch (template_name) {
                        case 'add':
                            if (typeof order_id !== 'undefined') {
                                $rootScope.showAdd(order_id);
                            } else {
                                $rootScope.showAdd();
                            }
                            break;
                        case 'detail':
                            $rootScope.loadDetail(detail_id);
                            break;
                        case 'edit':
                            $rootScope.loadEdit(order_id, form);
                            break;
                        case 'approve_reject':
                            $rootScope.loadApproveReject(detail_data, order );
                            break;
                        case 'change_report':
                            $rootScope.loadChangeReport(order_id);
                            break;
                    }
                }
            );
        } else {
            switch (template_name) {
                case 'add':
                    if (typeof arguments[1] !== 'undefined') {
                        $rootScope.showAdd(arguments[1]);
                    } else {
                        $rootScope.showAdd();
                    }
                    break;
                case 'detail':
                    $rootScope.loadDetail(arguments[1]);
                    break;
                case 'edit':
                    $rootScope.loadEdit(arguments[1], arguments[2]);
                    break;
                case 'approve_reject':
                    $rootScope.loadApproveReject(arguments[1], arguments[2]);
                    break;
                case 'change_report':
                    $rootScope.loadChangeReport(arguments[1]);
                    break;
            }
        }


    };
    $scope.showClient = function (client_id) {
        $.post(
            '/_admin/client/detail',
            {id : client_id, dialog_selector: '#clientDialog'},
            function(data) {
                clientDialog.html(data);
                clientDialog.dialog("option", "position", {my: "center", at: "center", of: window});
                clientDialog.dialog('open');
            });
    };


    $scope.my_account = my_account;

    function ext_link(colName) {
        var out = '';
        var ng = false;

        if (colName == 'o_is_ad_sales_slip_input' || colName == 'o_is_ad_invoice_issue') {
            out += '<div class="ui-grid-cell-contents" ng-if="row.entity.o_is_ad_receive_payment != \'1\'">－</div>';
            out += '<div ng-if="row.entity.o_is_ad_receive_payment == \'1\'">';
        }
        //console.log($scope);
        switch (colName) {

            case 'o_is_ad_sales_slip_input':
            case 'o_is_sales_slip_input':
//#9748:Start
                var action                     = '/_admin/order_acceptance/show/';
                var action_order_acceptance_dp = '/_admin/order_acceptance/show/type/dp';
//                if (my_account.finish_work_report_template == 2) {
//                    var action = '/_admin/order_acceptance/show/type/dp';
//                } else {
//                    var action = '/_admin/order_acceptance/show';
//                }
//#9748:End
                //var hidden = '<input type="hidden" name="" ng-value="">'; //j_code + branch
//#7067:Start
                var hidden = '<input type="hidden" name="search_j_code_branch_cd_from" ng-value="row.entity.o_j_code + \'-\' + row.entity.od_branch_cd">' +
                '<input type="hidden" name="search_j_code_branch_cd_to" ng-value="row.entity.o_j_code + \'-\' + row.entity.od_branch_cd">' + //j_code + branch
                '<input type="hidden" name="search_delivery_date_from" value="">' +
                '<input type="hidden" name="search_delivery_date_to" value="">' +
//#7119:Start
                '<input type="hidden" name="search_j_business_unit" value="0">' +
                '<input type="hidden" name="search_j_account" value="0">' +
                '<input type="hidden" name="search_s_business_unit" value="0">' +
                '<input type="hidden" name="search_s_account" value="0">';
//#7119:End

//#7067:End
                break;
            case 'o_is_ad_invoice_issue':
            case 'o_is_invoice_issue':
                var action = '/_admin/invoice/show';
                var hidden = '<input type="hidden" name="s_code" ng-value="row.entity.c_s_code">'; //s_code
//#6688:Start
                hidden += '<input type="hidden" name="delivery_date" ng-value="row.entity.od_delivery_date">'; // delivery_date
                hidden += '<input type="hidden" name="is_link_from_order" value="">'; //
//#6688:End
                break;
            case 'o_is_order_input':
            case 'o_is_acceptance_input':
                var action = '/_admin/order_acceptance/show';
//#7067:Start
                var hidden = '<input type="hidden" name="from_jcode_code" ng-value="row.entity.o_j_code">' +
                             '<input type="hidden" name="from_branch_code" ng-value="row.entity.od_branch_cd">' +
                             '<input type="hidden" name="to_jcode_code" ng-value="row.entity.o_j_code">'+
                             '<input type="hidden" name="to_branch_code" ng-value="row.entity.od_branch_cd">'+
                             '<input type="hidden" name="text_is_acceptance_input"  ng-value="row.entity.o_is_acceptance_input" >'+ //j_code + branch  #6901-modify-line
                             '<input type="hidden" id="text_is_change_report_apply" name="text_is_change_report_apply"  ng-value="row.entity.od_is_change_report_apply" >'; //change report apply  #6901-add -line
                break;
//#7067:End
        }

        switch (colName) {
            case 'o_is_ad_sales_slip_input':
            case 'o_is_sales_slip_input':
                if (can_invoices == 'all') {
                    out += '<div class="ui-grid-cell-contents">';
                } else if (can_invoices == 'self') {
//#7066: Start
//                    out += '<div class="ui-grid-cell-contents" ng-if="row.entity.o_account_id != grid.appScope.my_account.id">－</div>';
                    out += '<div class="ui-grid-cell-contents" ng-if="row.entity.o_account_id != grid.appScope.my_account.id"><div class="a-padding-left" ng-if="1">{{row.entity.' + colName + ' | mapYesNo}}</div></div>';
                    out += '<div class="ui-grid-cell-contents" ng-if="row.entity.o_account_id == grid.appScope.my_account.id">';
                } else {
                    out += '<div class="ui-grid-cell-contents">';
//#7066: End
                    ng = true;
                }
//#7135:Start
                if (!is_sales_view) {
                    ng = true;
                }
//#7135:End
                break;

            case 'o_is_order_input':
            case 'o_is_acceptance_input':
                if (can_acceptance) {
                    out +=  '<div class="ui-grid-cell-contents">';
                } else {
                    ng = true;
                }
                break;

            case 'o_is_ad_invoice_issue':
            case 'o_is_invoice_issue':
                if (can_invoice_issue) {
                    out +=  '<div class="ui-grid-cell-contents">';
                } else {
                    out +=  '<div class="ui-grid-cell-contents">';
                    ng = true;
                }
                break;
        }

         /*
          #6901-modify-S
         */


        if (ng) {
//#7194:Start
            // out += '<div class="ui-grid-cell-contents">';
//#7194:End
//#9748:Start
//            out +=  '<form onsubmit="return submitcheck(this)" action="' + action + '" method="post">' + hidden;
            out +=  '<form onsubmit="return submitcheck(this)" action="{{row.entity.finish_work_report_template == 2 && (col.field == \'o_is_order_input\' || col.field == \'o_is_acceptance_input\') ? '+ "'" + action_order_acceptance_dp + "'" +' : ' + "'"+ action + "'" +'}}" method="post">' + hidden;
//#9748:End
            if(colName=='o_is_order_input' ) {
                // 「発注・検収-発注検収力・変更」権限有り：「未」「済」でリンクできてOK
                if(order_acceptance_input_change) {
//#7194:Start
                    out += '<div class="ui-grid-cell-contents">';
//#7194:End
//#6901:Start
                    out += '<input class="submit-link submit-check-change-report"  ng-if="row.entity.od_is_change_report_apply != 1" style="line-height: 15px !important;" type="submit" ng-value="row.entity.' + colName + ' | mapYesNo">';

                    out += '<div class="a-padding-left" ng-if="row.entity.od_is_change_report_apply == 1">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7194:Start
                    out += '</div>';
//#7194:End
//#6901:End
                }
                // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限有り：「済」だけリンクできてOK
                if(!order_acceptance_input_change && order_acceptance_view) {
                    out += '<input class="submit-link submit-check-change-report" style="line-height: 15px !important;" type="submit" ng-if="row.entity.o_is_order_input== 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                    out += '<div class="a-padding-left" ng-if="row.entity.o_is_order_input != 1">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                }
                // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限無し：リンクできてない
                if(!order_acceptance_input_change && !order_acceptance_view) {
                    out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                }
            } else if(colName=='o_is_acceptance_input') {
                // 「発注・検収-発注検収力・変更」権限有り：「未」「済」でリンクできてOK
                if(order_acceptance_input_change) {
//#7194:Start
                    out += '<div class="ui-grid-cell-contents">';
//#7194:End

//#6901:Start
                    out += '<input class="submit-link submit-check-change-report" ng-if="row.entity.od_is_change_report_apply != 1" style="line-height: 15px !important;" type="submit" ng-value="row.entity.' + colName + ' | mapYesNo">';
                    out += '<div class="a-padding-left" ng-if="row.entity.od_is_change_report_apply == 1">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#6901:End
                    out += '</div>';
                }
                // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限有り：「済」だけリンクできてOK
                if(!order_acceptance_input_change && order_acceptance_view) {
                    out += '<input class="submit-link submit-check-change-report" style="line-height: 15px !important;" type="submit" ng-if="row.entity.o_is_acceptance_input== 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                    out += '<div class="a-padding-left" ng-if="row.entity.o_is_acceptance_input != 1">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                }
                // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限無し：リンクできてない
                if(!order_acceptance_input_change & !order_acceptance_view) {
                    out += '<div class="a-padding-left" ng-if="1">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                }
            } else {
//#7066: Start
//                out += '<div class="ui-grid-cell-contents">－</div>';
                if (is_sales_view) {
                    out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="row.entity.' + colName +' == 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                    out += '<div class="a-padding-left" ng-if="row.entity.' + colName +' == 0">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                } else {
                    out += '<div class="a-padding-left" ng-if="1">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                }
//#7066: End
            }
//#7194:Start
            out += '</form>';
//#7194:End
        } else {
//#9748:Start
//            out +=  '<form  onsubmit="return submitcheck(this)" action="' + action + '" method="post">' + hidden;
            out +=  '<form  onsubmit="return submitcheck(this)" action="{{row.entity.finish_work_report_template == 2 && (col.field == \'o_is_order_input\' || col.field == \'o_is_acceptance_input\') ? '+ "'" + action_order_acceptance_dp + "'" +' : ' + "'"+ action + "'" +'}}" method="post">' + hidden;
//#9748:End

            if (colName != 'o_is_ad_invoice_issue' &&  colName != 'o_is_invoice_issue') {
                if(colName=='o_is_order_input' ) {
                    // 「発注・検収-発注検収力・変更」権限有り：「未」「済」でリンクできてOK
                    if(order_acceptance_input_change) {
//#6901:Start
//#7357:Start
                        out += '<input class="submit-link submit-check-change-report" ng-if="(row.entity.od_order_status == 2 || row.entity.od_order_status == 3) && row.entity.od_is_change_report_apply != 1" style="line-height: 15px !important;" type="submit" ng-value="row.entity.' + colName + ' | mapYesNo">';
                        out += '<div class="a-padding-left" ng-if="!((row.entity.od_order_status == 2 || row.entity.od_order_status == 3) && row.entity.od_is_change_report_apply != 1)">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7357:End
//#6901:End
                    }
                    // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限有り：「済」だけリンクできてOK
                    if(!order_acceptance_input_change && order_acceptance_view) {
//#7357:Start
                        out += '<input class="submit-link submit-check-change-report" style="line-height: 15px !important;" type="submit" ng-if="(row.entity.od_order_status == 2 || row.entity.od_order_status == 3) && row.entity.o_is_order_input== 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                        out += '<div class="a-padding-left" ng-if="!((row.entity.od_order_status == 2 || row.entity.od_order_status == 3) && row.entity.o_is_order_input== 1)">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7357:End
                    }
                    // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限無し：リンクできてない
                    if(!order_acceptance_input_change && !order_acceptance_view) {
                        out += '<div class="a-padding-left">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                    }
                } else if(colName=='o_is_acceptance_input') {
                    // 「発注・検収-発注検収力・変更」権限有り：「未」「済」でリンクできてOK
                    if(order_acceptance_input_change) {
//#6901:Start
//#7357:Start
                        out += '<input class="submit-link submit-check-change-report" ng-if="(row.entity.od_order_status == 2 || row.entity.od_order_status == 3) && row.entity.o_is_order_input == 1 && row.entity.od_is_change_report_apply != 1" style="line-height: 15px !important;" type="submit" ng-value="row.entity.' + colName + ' | mapYesNo">';
                        out += '<div class="a-padding-left" ng-if="!((row.entity.od_order_status == 2 || row.entity.od_order_status == 3) && row.entity.o_is_order_input == 1 && row.entity.od_is_change_report_apply != 1)">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7357:End
//#6901:End
                    }
                    // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限有り：「済」だけリンクできてOK
                    if(!order_acceptance_input_change && order_acceptance_view) {
//#7357:Start
                        out += '<input class="submit-link submit-check-change-report" style="line-height: 15px !important;" type="submit" ng-if="(row.entity.od_order_status == 2 || row.entity.od_order_status == 3) && row.entity.o_is_order_input == 1 && row.entity.o_is_acceptance_input== 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                        out += '<div class="a-padding-left" ng-if="!((row.entity.od_order_status == 2 || row.entity.od_order_status == 3) && row.entity.o_is_order_input == 1 && row.entity.o_is_acceptance_input== 1)">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7357:End
                    }
                    // 「発注・検収-発注検収力・変更」権限無し、「発注・検収-一覧・参照」権限無し：リンクできてない
                    if(!order_acceptance_input_change & !order_acceptance_view) {
                        out += '<div class="a-padding-left" ng-if="1">{{row.entity.' + colName + ' | mapYesNo}}</div>';
                    }
                } else {
//#6900:Start
//#7357:Start
                    out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="(row.entity.od_order_status == 2 || row.entity.od_order_status == 3) && row.entity.od_is_change_report_apply != 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                    out += '<div class="a-padding-left" ng-if="!(row.entity.od_order_status == 2 || row.entity.od_order_status == 3) || row.entity.od_is_change_report_apply == 1">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7357:End
//#6900:End
                }
            } else {
//#6901:Start
                if (colName == 'o_is_ad_invoice_issue') {
//#7357:Start
                    out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="(row.entity.od_order_status == 2 || row.entity.od_order_status == 3) && row.entity.o_is_ad_invoice_issue != 0 && row.entity.od_is_change_report_apply != 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                    out += '<div class="a-padding-left" ng-if="!(row.entity.od_order_status == 2 || row.entity.od_order_status == 3) || row.entity.o_is_ad_invoice_issue == 0 || row.entity.od_is_change_report_apply == 1 ">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7357:End
                } else {
//#7357:Start
                    out += '<input class="submit-link" style="line-height: 15px !important;" type="submit" ng-if="(row.entity.od_order_status == 2 || row.entity.od_order_status == 3) && row.entity.o_is_invoice_issue != 0 && row.entity.od_is_change_report_apply != 1" ng-value="row.entity.' + colName + ' | mapYesNo">';
                    out += '<div class="a-padding-left" ng-if="!(row.entity.od_order_status == 2 || row.entity.od_order_status == 3) || row.entity.o_is_invoice_issue == 0 || row.entity.od_is_change_report_apply == 1">{{row.entity.' + colName + ' | mapYesNo}}</div>';
//#7357:End
                }
//#6901:End
            }

            out += '</form>' +
                '</div>';
        }

        if (colName == 'o_is_ad_sales_slip_input' || colName == 'o_is_ad_invoice_issue') {
            out += '</div>';
        }
        // Check permission to view/edit order acceptance
        return out;
    };

    /*
          #6901-modify -E
     */

    if(can_approve==true) {
        $scope.gridOptions.columnDefs = [
                                         // 一括承認
                                         { name: 'cbx_arpprove', displayName: jsGlobals[33], pinnedLeft:true, width: 70, visible: can_approve,
                                             cellTemplate: "<div class='a-center'><input type='checkbox' ng-model='row.entity.cbx_arpprove' ng-show='grid.appScope.show_approved_cell(row)' ng-click='grid.appScope.addApproveList(row.entity.cbx_arpprove, row.entity.order_id, row.entity.od_branch_cd, row.entity.bu_id,row)'></div>"},

                                         // 処理
                                         { name: 'action', displayName: jsGlobals[27], width: 65,
                                                 cellTemplate: "<button type='button' class='btn primary btn-small btn-green btn-view' ng-click='grid.appScope.loadTemplate(row.entity.order_id)'>"+ jsGlobals[32] +"</button>"},
                                         // Ｊコード
                                         { name: 'o_j_code', displayName: jsGlobals[16], width: 80,
                                                     cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.loadTemplate(row.entity.order_id)">{{row.entity.o_j_code}}</a></div>'
                                         },
                                         // 枝
                                         { name: 'od_branch_cd', displayName: jsGlobals[0], width: 30},
                                         // Ｊ担当営業
                                         { name: 'o_account_id', displayName: jsGlobals[18], width: 100, visible: false},
                                         // Ｊ担当営業
                                         { name: 'a_name', displayName: jsGlobals[18], width: 100},
                                         // Ｓコード
                                         { name: 'c_s_code', displayName: jsGlobals[17], width: 80,
                                                 cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.showClient(row.entity.c_id)">{{row.entity.c_s_code}}</a></div>'},
                                         // 社名
                                         { name: 'c_name', displayName: jsGlobals[25], pinnedLeft:true, width: 200,
                                                 cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.showClient(row.entity.c_id)">{{row.entity.c_name}}</a></div>'},
                                         // 前売伝
                                         { name: 'o_is_ad_sales_slip_input', displayName: jsGlobals[19], width: 60,
                                                cellTemplate: ext_link('o_is_ad_sales_slip_input')
                                         },
                                         // 前請求
                                         { name: 'o_is_ad_invoice_issue', displayName: jsGlobals[20], width: 60,
                                                cellTemplate: ext_link('o_is_ad_invoice_issue')
                                         },
                                         // 発注 => link
                                         { name: 'o_is_order_input', displayName: jsGlobals[21], width: 50,
                                             cellTemplate: ext_link('o_is_order_input')
                                             // cellTemplate: "<span ng-bind='grid.appScope.set_links(row.entity.o_is_order_input)'></span>"
                                         },
                                         // 検収 => link
                                         { name: 'o_is_acceptance_input', displayName: jsGlobals[22], width: 50,
                                                cellTemplate: ext_link('o_is_acceptance_input')
                                         },
                                         // 売伝
                                         { name: 'o_is_sales_slip_input', displayName: jsGlobals[23], width: 50,
                                                cellTemplate: ext_link('o_is_sales_slip_input')
                                         },
                                         // 請求
                                         { name: 'o_is_invoice_issue', displayName: jsGlobals[24], width: 50,
                                                cellTemplate: ext_link('o_is_invoice_issue')},
                                         // 受注ステータス
                                         { name: 'od_order_status', displayName: jsGlobals[1], width: 100, visible: false},
                                         // 受注ステータス
                                         { name: 'od_is_change_report_apply', displayName: jsGlobals[1], width: 100, visible: false},
                                         // 受注ステータス
                                         { name: 'od_order_status_name', displayName: jsGlobals[1], width: 110,
                                                cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.od_is_change_report_apply == 1 ? "変レポ申請中" : row.entity.od_order_status_name}}</div>'
                                         },
                                         // 商品名
                                         { name: 'p_name', displayName: jsGlobals[3], width: 100},
                                         // 内容
                                         { name: 'od_contents', displayName: jsGlobals[26], width: 100},
                                         // 納品日
                                         { name: 'od_delivery_date', displayName: jsGlobals[5], width: 100, cellFilter: 'formatDate'},
                                         // 請求日
                                         { name: 'od_billing_date', displayName: jsGlobals[6], width: 100, cellFilter: 'formatDate'},
                                         // 支払日
                                         { name: 'od_payment_date', displayName: jsGlobals[7], width: 100, cellFilter: 'formatDate'},
                                         // 数量
                                         { name: 'od_quantity', displayName: jsGlobals[8], width: 50, cellFilter : 'number:0'},
                                         // 税
                                         { name: 'od_tax_type', cellFilter: 'mapTaxType', displayName: jsGlobals[9], width: 30},
                                         // 単価
                                         { name: 'od_unit_price', displayName: jsGlobals[10], width: 100, cellFilter: 'number:2', cellClass: 'a-right'},
                                         // 合計
                                         { name: 'od_amount', displayName: jsGlobals[11], width: 100, cellFilter: 'number:0', cellClass: 'a-right'},
                                         // 原価税別単価
                                         { name: 'od_cost_unit_price', displayName: jsGlobals[12], width: 100, cellFilter: 'number:2', cellClass: 'a-right'},
                                         // 原価税別合計
                                         { name: 'od_cost_total_price', displayName: jsGlobals[13], width: 100, cellFilter: 'number:0', cellClass: 'a-right'},
                                         // 粗利合計
                                         { name: 'od_gross_profit_amount', displayName: jsGlobals[14], width: 80, cellFilter: 'number:0',
                                             cellClass:
                                                     function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                     if (grid.getCellValue(row ,col) < 0) {
                                                         return 'a-right red';
                                                     }
                                                 return 'a-right';
                                             }
                                          },
                                         // 粗利率
                                         { name: 'od_gross_profit_rate', displayName: jsGlobals[15], width: 100, visible: false},
                                         // 粗利率
                                         { name: 'od_gross_profit_rate_percent', displayName: jsGlobals[15], width: 80,

                                             cellClass:
                                                 function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                 var gross_profit_rate = grid.getCellValue(row ,col).replace('%', '');
                                                 if (gross_profit_rate < 0) {
                                                     return 'a-right red';
                                                }
                                                 return 'a-right';
                                             }
                                         },
                                       ];
    } else {
        $scope.gridOptions.columnDefs = [
                                         { name: 'action', displayName: jsGlobals[27], width: 65,
                                                 cellTemplate: "<button type='button' class='btn primary btn-small btn-green btn-view' ng-click='grid.appScope.loadTemplate(row.entity.order_id)'>"+ jsGlobals[32] +"</button>"},
                                         { name: 'o_j_code', displayName: jsGlobals[16], width: 80,
                                                     cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.loadTemplate(row.entity.order_id)">{{row.entity.o_j_code}}</a></div>'
                                         },
                                         { name: 'od_branch_cd', displayName: jsGlobals[0], width: 30},
                                         { name: 'o_account_id', displayName: jsGlobals[18], width: 100, visible: false},
                                         { name: 'a_name', displayName: jsGlobals[18], width: 100},
                                         { name: 'c_s_code', displayName:     jsGlobals[17], width: 80,
                                                 cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.showClient(row.entity.c_id)">{{row.entity.c_s_code}}</a></div>'},
                                         { name: 'c_name', displayName: jsGlobals[25], width: 200,
                                                 cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.showClient(row.entity.c_id)">{{row.entity.c_name}}</a></div>'},
                                         { name: 'o_is_ad_sales_slip_input', displayName: jsGlobals[19], width: 60,
                                                cellTemplate: ext_link('o_is_ad_sales_slip_input')},
                                         { name: 'o_is_ad_invoice_issue', displayName: jsGlobals[20], width: 60,
                                                cellTemplate: ext_link('o_is_ad_invoice_issue')},
                                         { name: 'o_is_order_input', displayName: jsGlobals[21], width: 50,
                                                cellTemplate: ext_link('o_is_order_input')},
                                         { name: 'o_is_acceptance_input', displayName: jsGlobals[22], width: 50,
                                                cellTemplate: ext_link('o_is_acceptance_input')},
                                         { name: 'o_is_sales_slip_input', displayName: jsGlobals[23], width: 50,
                                                cellTemplate: ext_link('o_is_sales_slip_input')},
                                         { name: 'o_is_invoice_issue', displayName: jsGlobals[24], width: 50,
                                                cellTemplate: ext_link('o_is_invoice_issue')},
                                         { name: 'od_order_status', displayName: jsGlobals[1], width: 100, visible: false},
                                         { name: 'od_is_change_report_apply', displayName: jsGlobals[1], width: 100, visible: false},
                                         { name: 'od_order_status_name', displayName: jsGlobals[1], width: 110,
                                                cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.od_is_change_report_apply == 1 ? "変レポ申請中" : row.entity.od_order_status_name}}</div>'
                                         },
                                         { name: 'p_name', displayName: jsGlobals[3], width: 100},
                                         { name: 'od_contents', displayName: jsGlobals[26], width: 100},
                                         { name: 'od_delivery_date', displayName: jsGlobals[5], width: 100, cellFilter: 'formatDate'},
                                         { name: 'od_billing_date', displayName: jsGlobals[6], width: 100, cellFilter: 'formatDate'},
                                         { name: 'od_payment_date', displayName: jsGlobals[7], width: 100, cellFilter: 'formatDate'},
                                         { name: 'od_quantity', displayName: jsGlobals[8], width: 50, cellFilter: 'number:0'},
                                         { name: 'od_tax_type', cellFilter: 'mapTaxType', displayName: jsGlobals[9], width: 30},
                                         { name: 'od_unit_price', displayName: jsGlobals[10], width: 100, cellFilter: 'number:2', cellClass: 'a-right'},
                                         { name: 'od_amount', displayName: jsGlobals[11], width: 100, cellFilter: 'number:0', cellClass: 'a-right'},
                                         { name: 'od_cost_unit_price', displayName: jsGlobals[12], width: 100, cellFilter: 'number:2', cellClass: 'a-right'},
                                         { name: 'od_cost_total_price', displayName: jsGlobals[13], width: 100, cellFilter: 'number:0', cellClass: 'a-right'},
                                         { name: 'od_gross_profit_amount', displayName: jsGlobals[14], width: 80, cellFilter: 'number:0',
                                             cellClass:
                                                     function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                     if (grid.getCellValue(row ,col) < 0) {
                                                         return 'a-right red';
                                                     }
                                                 return 'a-right';
                                             }
                                         },
                                         { name: 'od_gross_profit_rate', displayName: jsGlobals[15], width: 100, visible: false},
                                         { name: 'od_gross_profit_rate_percent', displayName: jsGlobals[15], width: 80,
                                             cellClass:
                                                 function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                     var gross_profit_rate = grid.getCellValue(row ,col).replace('%', '');
                                                     if (gross_profit_rate < 0) {
                                                         return 'a-right red';
                                                    }
                                                     return 'a-right';
                                                 }
                                         },
                                       ];
    }

    /* start volyminhnhan@gmail.com modification*/
    //this is redundant
    // $http({
    //     url : '/_admin/consumption_tax/get_tax_rate',
    //     method : 'POST',
    //     data : {
    //         tax_type : 1
    //     }
    // }).success(function(data) {
    //     $rootScope.tax_rates = data;
    // });
    /* end volyminhnhan@gmail.com modification*/
     /* start volyminhnhan@gmail.com modification*/
    $scope.setTaxRate = function() {
      if (typeof $rootScope.tax_rates == 'undefined') {
            $http({
                url : '/_admin/consumption_tax/get_tax_rate',
                method : 'POST',
                data : {
                    tax_type : 1
                }
            }).success(function(data) {
                $rootScope.tax_rates = data;
            });
        }
    }
    /* end volyminhnhan@gmail.com modification*/

    $scope.j_business_unit = 0;
    $scope.j_account = 0;
    $scope.s_business_unit = 0;
    $scope.s_account = 0;

    if (typeof search_result.is_over_record != 'undefined') {
        $scope.is_over_record = true;
        $scope.gridOptions.data = [];
    } else if (search_result.length == 0) {
        $scope.showWatermark = true;
        $scope.gridOptions.data = [];
    } else {
        $scope.gridOptions.data = search_result;
    }

    var approveList = [];
    var businessOrderList = [];

    Array.prototype.remove = function() {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };
    $scope.addApproveList = function(checkbox_status, order_id, branch_cd, bu_id,row){

//#8133:Start
    	if(checkbox_status == true){

    		var find_id_order = jQuery.inArray(order_id, data_check_approved );

    		if(find_id_order != -1){
    			console.log('find_id_order'+find_id_order);
    			data_check_approved.remove(order_id);
    			row.entity.cbx_arpprove = false;
    		}
    		else{
    			row.entity.cbx_arpprove = true;
    			data_check_approved.push(order_id);
    		}
    	}
    	else{
    		data_check_approved.remove(order_id);
    		row.entity.cbx_arpprove = false;
    	}

//#8133:End

        if (checkbox_status) {
            approveList.push(order_id);
            businessOrderList.push(bu_id);
        } else {
            var index_approve = approveList.indexOf(order_id);
            var index_bu = businessOrderList.indexOf(bu_id);

            if (index_approve > -1) {
                approveList.splice(index_approve, 1);
            }
            if (index_bu > -1) {
                businessOrderList.splice(index_bu, 1);
            }
        }


    };

    //processing approve order
    $scope.approve = function() {
        if (approveList.length == 0) {
            return;
        }
        $scope.is_submitted = true;
        var can_approve_follow_business = true;

        angular.forEach(businessOrderList, function(item) {
            if (item != bu_id_user_logged) {
                can_approve_follow_business = false;
            }
        });

//#8133:Start
    	if(keepcheckval){
    		approveList = data_check_approved;
    	}
//#8133:End

        if (!can_approve_follow_business) {
            $('div#form_error_msg').html('');
            $('div#form_error_msg').append('<span class="error-message blink_me">' + '売上伝票承認できません。' + '</span>');
        } else {
            $http({
                url : '/_admin/order/approve_multiple_orders',
                method : 'POST',
                data : {
                    order_ids: approveList,
                    remark: '',
                    comment: ''
                }
            }).success(function(resp) {
                $scope.is_submitted = false;
                $('div#form_error_msg').html('');

                if (resp.result == false) {
                    $('div#form_error_msg').append('<span class="error-message blink_me">' + resp.error_msg + '</span>');
                } else {

                    $('div#form_error_msg').append('<span class="success-message blink_me">' + resp.error_msg + '</span>');
//#7941:Start
                    $scope.approved = true;
//#7941:End
                    $rootScope.search();

                    //resest approveList
                    approveList = [];
                }
            });
        }

    };

    $scope.order = {
        j_business_unit: "0",
        j_account: "0",
        s_business_unit: "0",
        s_account: "0"
    };

//#7525:Start
// Check devision of business unit of account login.
   if (my_account.is_product_division == 1) {
       $scope.order.j_business_unit = my_account.bu_id;
       $scope.order.j_account = my_account.id;
       $("#j_account").val(my_account.id);
       // $scope.order.j_account = my_account.id;
       // $scope.order.s_account = my_account.id;
   }
//#7525:End
    $scope.order_temp = {};
    $scope.order_temp.order_status_arr = [];
    $scope.order_temp.order_status_arr = {
        1: false,
        2: false,
        3: false,
        4: false,
        9: false
    };

 // <------------------------------------------------------------ CLICK ENTER TO SEARCH ----------------------------------------------------------->
    $(".search-container").keypress(function (e) {
         var key = e.which;
         if(key == 13)  // the enter key code
          {
             e.preventDefault();
             var client_name = $("input[name='client_name']").is(':focus');
             var j_code_branch_cd_from = $("input[name='j_code_branch_cd_from']").is(':focus');
             var j_code_branch_cd_to = $("input[name='j_code_branch_cd_to']").is(':focus');
             var delivery_date_from = $("input[name='delivery_date_from']").is(':focus');
             var delivery_date_to = $("input[name='delivery_date_to']").is(':focus');

             var order_regist_date_from = $("input[name='order_regist_date_from']").is(':focus');
             var order_regist_date_to = $("input[name='order_regist_date_to']").is(':focus');
             var approval_date_from = $("input[name='approval_date_from']").is(':focus');
             var approval_date_to = $("input[name='approval_date_to']").is(':focus');
             var payment_date_from = $("input[name='payment_date_from']").is(':focus');

             var payment_date_to = $("input[name='payment_date_to']").is(':focus');
             var s_code = $("input[name='s_code']").is(':focus');
             var client_name_furigana = $("input[name='client_name_furigana']").is(':focus');
             var client_department_name = $("input[name='client_department_name']").is(':focus');

             var j_business_unit = $("select[name='j_business_unit']").is(':focus');
             var j_account = $("select[name='j_account']").is(':focus');
             var s_business_unit = $("select[name='s_business_unit']").is(':focus');
             var s_account = $("select[name='s_account']").is(':focus');

             var order_status_0 = $("input[id='order_status_0']").is(':focus');
             var order_status_1 = $("input[id='order_status_1']").is(':focus');
             var order_status_2 = $("input[id='order_status_2']").is(':focus');
             var order_status_3 = $("input[id='order_status_3']").is(':focus');
             var order_status_4 = $("input[id='order_status_4']").is(':focus');

             var is_ad_sales_slip_input = $("input[name='is_ad_sales_slip_input[]']").is(':focus');
             var is_ad_invoice_issue = $("input[name='is_ad_invoice_issue[]']").is(':focus');
             var is_order_input = $("input[name='is_order_input[]']").is(':focus');
             var is_acceptance_input = $("input[name='is_acceptance_input[]']").is(':focus');
             var is_sales_slip_input = $("input[name='is_sales_slip_input[]']").is(':focus');

             var is_invoice_issue = $("input[name='is_invoice_issue[]']").is(':focus');
             var is_ad_receive_payment = $("input[name='is_ad_receive_payment']").is(':focus');
             var is_change_report_apply = $("input[name='is_change_report_apply']").is(':focus');



             if(client_name | j_code_branch_cd_from | j_code_branch_cd_to | delivery_date_from | delivery_date_to
                     | order_regist_date_from | order_regist_date_to | approval_date_from |approval_date_to | payment_date_from
                     | payment_date_to | s_code | client_name_furigana | client_department_name
                     | j_business_unit | j_account | s_business_unit | s_account
                     | order_status_0 | order_status_1 | order_status_2 | order_status_3 | order_status_4
                     | is_ad_sales_slip_input | is_ad_invoice_issue | is_order_input | is_acceptance_input | is_sales_slip_input
                     | is_invoice_issue | is_ad_receive_payment | is_change_report_apply
                     ) {
                 // console.log("scope.search.datepicker_from & to:");
                 // console.log($scope.search.datepicker_from);
                 // console.log($scope.search.datepicker_to);
                 // Validate and search
                 $rootScope.search();
             }
          }
     });
    // <------------ END CLICK ENTER TO SEARCH ------------------------>



    $rootScope.search = function (keepcheckvalinput ) {

//#8133:Start
    	if(keepcheckvalinput == undefined){
    		keepcheckval = false;
    	}
    	else{
    		keepcheckval = true;
    	}
//#8133:End

//#7941:Start
        if ($scope.approved) {
            $scope.approved = false;
        } else {
            if (!$('.success-message').is(':empty')) {
                $('.success-message').remove();
            }
        }
//#7941:End
        // Add circle loading to button search
        $('.btn-search').addClass('loading icon-spin');
        $scope.order.j_account = $("#j_account").val();
        $scope.order.s_account = $("#s_account").val();

        form_changed = false;
        form_submitted = true;

        if ($scope.order_temp.order_status_arr) {
            $scope.order.order_status = [];
            angular.forEach($scope.order_temp.order_status_arr, function(value, key) {
                if (value)
                    $scope.order.order_status.push(key);
            });
        }
        if ($scope.order_temp.is_ad_sales_slip_input_arr) {
            $scope.order.is_ad_sales_slip_input = [];
            angular.forEach($scope.order_temp.is_ad_sales_slip_input_arr, function(value, key) {
                if (value)
                    $scope.order.is_ad_sales_slip_input.push(key);
            });
        }
        if ($scope.order_temp.is_ad_invoice_issue_arr) {
            $scope.order.is_ad_invoice_issue = [];
            angular.forEach($scope.order_temp.is_ad_invoice_issue_arr, function(value, key) {
                if (value)
                    $scope.order.is_ad_invoice_issue.push(key);
            });
        }
        if ($scope.order_temp.is_order_input_arr) {
            $scope.order.is_order_input = [];
            angular.forEach($scope.order_temp.is_order_input_arr, function(value, key) {
                if (value)
                    $scope.order.is_order_input.push(key);
            });
        }
        if ($scope.order_temp.is_acceptance_input_arr) {
            $scope.order.is_acceptance_input = [];
            angular.forEach($scope.order_temp.is_acceptance_input_arr, function(value, key) {
                if (value)
                    $scope.order.is_acceptance_input.push(key);
            });
        }
        if ($scope.order_temp.is_sales_slip_input_arr) {
            $scope.order.is_sales_slip_input = [];
            angular.forEach($scope.order_temp.is_sales_slip_input_arr, function(value, key) {
                if (value)
                    $scope.order.is_sales_slip_input.push(key);
            });
        }
        if ($scope.order_temp.is_invoice_issue_arr) {
            $scope.order.is_invoice_issue = [];
            angular.forEach($scope.order_temp.is_invoice_issue_arr, function(value, key) {
                if (value)
                    $scope.order.is_invoice_issue.push(key);
            });
        }

        angular.forEach($scope.order, function(value, key) {
            if (value.constructor === Array && value.length == 0) {
                delete $scope.order[key];
            }
        });

        if ($scope.order_temp.is_ad_receive_payment) {
            $scope.order.is_ad_receive_payment = 1;
        } else {
            delete $scope.order.is_ad_receive_payment;
        }

        if ($scope.order_temp.is_change_report_apply) {
            $scope.order.is_change_report_apply = 1;
        } else {
            delete $scope.order.is_change_report_apply;
        }

        $http({
            url : '/_admin/order/search_order',
            method : 'POST',
            data : {
                order : $scope.order
            }
        }).success(function(data) {

            /*console.log("can_invoices:");
            console.log(can_invoices);
            console.log("can_acceptance:");
            console.log(can_acceptance);
            console.log("can_invoice_issue:");
            console.log(can_invoice_issue);
            console.log(data);*/


            $('#delivery_date_from').hide();
            $('#delivery_date_to').hide();
            $('#delivery_date').hide();
            $('#order_regist_date_from').hide();
            $('#order_regist_date_to').hide();
            $('#order_regist_date').hide();
            $('#approval_date_from').hide();
            $('#approval_date_to').hide();
            $('#approval_date').hide();
            $('#payment_date_from').hide();
            $('#payment_date_to').hide();
            $('#payment_date').hide();
            $('#j_code_branch_cd_1').hide();
            $('#j_code_branch_cd_2').hide();

            $scope.showWatermark = false;
            $scope.is_over_record = false;

            if (data.error_form) {
//#7067:Start
                $scope.showWatermark = true;
//#7067:End
                $scope.gridOptions.data = [];

                if (data.error_delivery_date_from) {
                    $('#delivery_date_from').html(data.error_delivery_date_from).show();
                }

                if (data.error_delivery_date_to) {
                    $('#delivery_date_to').html(data.error_delivery_date_to).show();
                }

                if (data.error_delivery_date) {
                    $('#delivery_date').html(data.error_delivery_date).show();
                }

                if (data.error_order_regist_date_from) {
                    $('#order_regist_date_from').html(data.error_order_regist_date_from).show();
                }

                if (data.error_order_regist_date_to) {
                    $('#order_regist_date_to').html(data.error_order_regist_date_to).show();
                }

                if (data.error_order_regist_date) {
                    $('#order_regist_date').html(data.error_order_regist_date).show();
                }

                if (data.error_approval_date_from) {
                    $('#approval_date_from').html(data.error_approval_date_from).show();
                }

                if (data.error_approval_date_to) {
                    $('#approval_date_to').html(data.error_approval_date_to).show();
                }

                if (data.error_approval_date) {
                    $('#approval_date').html(data.error_approval_date).show();
                }

                if (data.error_payment_date_from) {
                    $('#payment_date_from').html(data.error_payment_date_from).show();
                }

                if (data.error_payment_date_to) {
                    $('#payment_date_to').html(data.error_payment_date_to).show();
                }

                if (data.error_payment_date) {
                    $('#payment_date').html(data.error_payment_date).show();
                }

                if (data.error_j_code_branch_cd_1) {
                    $('#j_code_branch_cd_1').html(data.error_j_code_branch_cd_1).show();
                }

                if (data.error_j_code_branch_cd_2) {
                    $('#j_code_branch_cd_2').html(data.error_j_code_branch_cd_2).show();
                }
            } else {
                if (typeof data.is_over_record != 'undefined') {
                     $scope.is_over_record = true;
                     $scope.gridOptions.data = [];
                } else if (data.length == 0) {
                    $scope.showWatermark = true;
                    $scope.gridOptions.data = [];
                } else {
                    $scope.gridOptions.data = data;
                }
            }
            // Remove circle loading from button search
            $('.btn-search').removeClass('loading icon-spin');
        });
    }
}]).filter('mapYesNo', function() {
    var mapHash = {
            0 : '未',
            1 : '済'
        };

    return function(input) {
        if (!input){
            return '-';
        } else {
            return mapHash[input];
        }
    };
})
.filter('mapTaxType', function() {
    return function(input) {
        if (!input) {
            return '';
        } else {
            for (i = 0; i < tax_division.length; i++) {
                if (tax_division[i].code == input) {
                    return tax_division[i].name;
                }
            }
        }
    };
}).filter("nl2br", function() {
     return function(data) {
           if (!data) return data;
           return data.replace(/\n\r?/g, '<br />');
     };
}).filter('mapOrderStatus', function() {
    return function(input) {
        if (!input) {
            return '';
        } else {
            for (i = 0; i < list_order_status.length; i++) {
                if (list_order_status[i].code == input) {
                    return list_order_status[i].name;
                }
            }
        }
    };
}).filter('formatDate', function() { //#6853: Start (3 files refer to this function: approve_reject.js, change_repo.js, detail.js)
    return function(input) {
        return input.replace(/-/g, '/');
    };
}); //#6853: End

/*
#6901-Add -S
*/

function submitcheck(obj) {
       var check_change_report_apply =obj["text_is_change_report_apply"].value;
       if(check_change_report_apply == 1){

           $("#dialog_error_check_change_report").dialog({
               title: '受注',
               width: 'auto',
               height: 'auto',
               modal: true,
               dialogClass: "acceptance-dialog-error",
               buttons: [{
                   text: "OK",
                   click: function() {
                           $(this).dialog("close");
                           return false;
                   }
               }]
            });
            return false;

       }
       else{
           return true;
       }

}

/*
#6901-Add -E
*/