// Order acceptance
// #6784-S
//#7846:Start
// var app = angular.module('app', [ 'ngTouch', 'ngSanitize', 'ui.grid', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.pagination', 'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.exporter', 'ui.grid.cellNav']);
var app = angular.module('app', ['ngSanitize', 'ui.grid', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.cellNav']);
//#7846:End
//#6784-E
var postion_return;
var isCheckAll= true;
//#7846:Start
//var isAdddButton = false;
//* #6946:Start
//var colCurrentName = "";
//#7846:End
var just_save = false;
//* #6946:End

//#7183:Start
var list_row_update_error = [];
var list_row_insert_error = [];
//#7183:End
//#7846:Start
//app.controller('MainCtrl', ['$scope', '$rootScope', '$http', '$q', '$interval', 'uiGridConstants', 'uiGridExporterConstants', function($scope, $rootScope, $http, $q, $interval, uiGridConstants, uiGridExporterConstants) {
app.controller('MainCtrl', ['$scope', '$rootScope', '$http', '$q', '$interval', 'uiGridConstants', function($scope, $rootScope, $http, $q, $interval, uiGridConstants) {
//#7846:End
    $rootScope.lang = 'ja';
//#7158:Start
    var $outsideScope = $scope;
//#7158:End

//#7846:Start
    /* start volyminhnhan@gmail.com modification*/
    // convert thí block to a function to call whenever you need
//    $scope.setTaxRate = function() {
//      if (typeof $rootScope.tax_rates == 'undefined') {
//            $http({
//                url : '/_admin/consumption_tax/get_tax_rate',
//                method : 'POST',
//                data : {
//                    tax_type : 1
//                }
//            }).success(function(data) {
//                $rootScope.tax_rates = data;
//            });
//        }
//    }
    /* end volyminhnhan@gmail.com modification*/
//#7846:End


    $('#slide-left-btn').click(function () {
        $interval(function () {
            $scope.gridApi.core.handleWindowResize();
        },500, 1);
    });


 // <------------------------------------------------------------ CLICK ENTER TO SEARCH ----------------------------------------------------------->
    $("#search-form").keypress(function (e) {
         var key = e.which;
         if(key == 13)  // the enter key code
          {
             e.preventDefault();
             // alert("FFFFFF");
             // return;
             var from_jcode_branch = $("input[name='from_jcode_branch']").is(':focus');
             var to_jcode_branch = $("input[name='to_jcode_branch']").is(':focus');
             var client_name = $("input[name='client_name']").is(':focus');
             var source_subcontractor_name = $("input[name='source_subcontractor_name']").is(':focus');
             var work_subcontractor_name = $("input[name='work_subcontractor_name']").is(':focus');

             var shipping_subcontractor_name = $("input[name='shipping_subcontractor_name']").is(':focus');
             var from_delivery_date = $("input[name='from_delivery_date']").is(':focus');
             var to_delivery_date = $("input[name='to_delivery_date']").is(':focus');
             var from_summary_month = $("input[name='from_summary_month']").is(':focus');
             var to_summary_month = $("input[name='to_summary_month']").is(':focus');

             var order_acceptance_status = $("input[name='order_acceptance_status[]']").is(':focus');

             var business_unit_id = $("select[name='business_unit_id']").is(':focus');
             var account_id = $("select[name='account_id']").is(':focus');
             var arrange_rep = $("select[name='arrange_rep']").is(':focus');
             var shipping_type_cd = $("select[name='shipping_type_cd']").is(':focus');



             if(from_jcode_branch | to_jcode_branch | client_name | source_subcontractor_name | source_subcontractor_name
                     | work_subcontractor_name | shipping_subcontractor_name | from_delivery_date |to_delivery_date | from_summary_month
                     | to_summary_month
                     | order_acceptance_status
                     | business_unit_id | account_id | arrange_rep | shipping_type_cd) {
                 // Validate and search
                 $rootScope.search();
             }
          }
     });
    // <------------ END CLICK ENTER TO SEARCH ------------------------>

    $rootScope.search = function() {
        // Add circle loading to button search
        $scope.error_messages = '';
        $('.btn-search').addClass('loading icon-spin');
//#7846:Start
//        form_data = $("#search-form").serialize();
        $.ajax({
            url: '/_admin/order_acceptance/show',
            type: 'POST',
//            data: form_data
            data: $("#search-form").serialize(),
//#7846:End
        })
        .done(function(data) {

            if (data.status == 'success') {


//#7087:Start

            	  if (typeof data.search_result.is_over_record != 'undefined') {
                 	 $scope.is_over_record = true;
                 	 $scope.showWatermark = false;
                 	 $scope.gridOptions.data = [];
                 	 $('.btn-search').removeClass('loading icon-spin');
                 	 return;
                 }

//#7087:End

                for(var i = 0; i < data.search_result.length;i ++) {
//#7846:Start
//                    data.search_result[i].account_options                = glb_account_options;
//#7167:Start
//                    data.search_result[i].arrange_rep_options            = glb_arrange_rep_options;
//#7167:End

//                    data.search_result[i].direct_subcontractor_options   = glb_direct_subcontractor_options;
//                    data.search_result[i].indirect_subcontractor_options = glb_indirect_subcontractor_options;
//                    data.search_result[i].shipping_type_options          = glb_shipping_type_options;
//                    data.search_result[i].shipments_shape_options        = glb_shipments_shape_options;
//                    data.search_result[i].consumption_tax                = glb_consumption_tax;
//#7846:End

                    /* #6784 - Add -S */

                    data.search_result[i].is_new						 = false;
                    data.search_result[i].is_editing					 = false;
                    data.search_result[i].is_change						 = false;

                     /* #6784 - Add -E */
//#7766:Start
                    data.search_result[i].summary_month_for_save = data.search_result[i].summary_month;
//#7766:End
                }

                if ($("#delivery_date_error").text()) {
                    $("#delivery_date_error").html('').hide();
                }
                if ($("#summary_month_error").text()) {
                    $("#summary_month_error").html('').hide();
                }
                if ($("#jcode_branch_error").text()) {
                    $("#jcode_branch_error").html('').hide();
                }
                // Assign data to grid when data not empty
//#7846:Start
//                $scope.gridOptions.data = [];
                $scope.gridOptions.data = data.search_result;
//                angular.forEach(data.search_result, function(data, key) {
//                    $scope.gridOptions.data.push(data);
//                });
//#7846:End

                $scope.showWatermark = false;
//#7087:Start
                $scope.is_over_record = false;
//#7087:End

                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);

                if(data.search_result.length == 0){
                    $scope.showWatermark = true;
                    $scope.gridOptions.data = [];
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                }
            }
            else {

                $("#delivery_date_error").html('').hide();
                $("#summary_month_error").html('').hide();

                $scope.showWatermark = true;

//#7087:Start
                $scope.is_over_record = false;
//#7087:End

                $scope.gridOptions.data = [];
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);

                // Show errors on form
                if (data.mesg.delivery_date) {
                    $("#delivery_date_error").html(data.mesg.delivery_date).show();
                }
                if (data.mesg.summary_month) {
                    $("#summary_month_error").html(data.mesg.summary_month).show();
                }
                if (data.mesg.jcode_branch) {
                    $("#jcode_branch_error").html(data.mesg.jcode_branch).show();
                }
            }
            // Remove circle loading from button search
            $('.btn-search').removeClass('loading icon-spin');
        })
    }

//#7125:Start
    function get_consumption_tax(summary_month) {
        if (!summary_month) {
            var today = new Date();
            summary_month = today.getFullYear() + '-';
            summary_month += (today.getMonth() + 1) < 10 ? '0' + (today.getMonth() + 1) : (today.getMonth() + 1);
        }
        if (summary_month.length > 7) {
            summary_month = summary_month.substr(0, 7);
        }
        summary_month = summary_month.replace(/\//g, '-');
        if (/^([0-9]{4}-(0[1-9]|1[012]))$/.test(summary_month) === false) {
            console.error('The date ' + summary_month + ' is not format as YYYY-mm');
            return false;
        }

        var i = 0;
        while (i < glb_consumption_tax.length && summary_month < glb_consumption_tax[i].adaptation_date.substr(0, 7)) {
            i++;
        }

        if (i < glb_consumption_tax.length) {
            return glb_consumption_tax[i].rate;
        } else {
            console.error('Cannot get tax rate of date ' + summary_month);
            return false;
        }
    }
//#7125:End

    $scope.show_approved_all = function(row) {
    	// #6741-S Modify
        var amount = parseFloat(row.entity.amount);
        var can_approvel = row.entity.can_approve;
//#7115:Start
//#7158:Start
//        if (can_approvel && row.entity.summary_month_has_been_edited == true) {
//            return true;
//        }
//#7158:End
//#7115:End

        var  datsummary_month = 	closingdateend;
		var  datenow = row.entity.summary_month;
		var  newsmday = row.entity.summary_month_for_save;

//#7115:Start
        // var  boolhien =  compareSummarydate(datsummary_month,datenow,newsmday);
		var  boolhien = 1;

        if (datenow != null && datenow != '') {
            boolhien =  compareSummarydate(datsummary_month,datenow,newsmday);
        }
//#7115:End

		var is_acceptance_input = row.entity.flag_is_accceptance_input;

	    if(boolhien == 0){
	    	can_approvel = false;
	    }
	    else{
	    	can_approvel = boolhien;
	    }
        if(amount == 0){
        	return true && can_approvel;
        }
        return amount && can_approvel;
     // #6741-E Modify
    }
    function compareSummarydate(datsummary_month,datenow,newsmday2){

//#8436:START
    	if((datenow == ""  || datenow == null ) && (newsmday2 != "" && newsmday2 != null )){
    		datenow = newsnday2;
    	}
//#8436:END
    	var nfind = datsummary_month.indexOf("/");
    	if(nfind > 0){
    		datsummary_month = datsummary_month.replace('/', '-');
    	}
        var listdatsummary_month = datsummary_month.split('-');

        if(datenow == null ){

		        	var client_name = '';
		          	datenow = datsummary_month;
        }

        if(datenow != ""){
        	var nfind2 = datenow.indexOf("/");
        	if(nfind2 > 0){
        	datenow = datenow.replace('/', '-');
        	}
        }
        if(listdatenow != ""){
           var listdatenow = datenow.split('-');
        }
        var boolhien = 1;
        if(listdatsummary_month[0] == listdatenow[0]){
//#7115:Start
            // if(listdatsummary_month[1] >= listdatenow[1]){
        	if(parseInt(listdatsummary_month[1]) >= parseInt(listdatenow[1])){
//#7115:End
        		boolhien = 0;
        	}
        }
        if(listdatsummary_month[0] > listdatenow[0]){
        	boolhien = 0;
        }
        return boolhien;

    }
    $scope.can_add = function(row) {

    	if (row.entity.can_add) {
//#7115:Start
            if (row.entity.summary_month_has_been_edited == true) return true;
//#7115:End

    		var  datsummary_month = 	closingdateend;
    		var  datenow = row.entity.summary_month;
    		var  newsmday = row.entity.summary_month_for_save;
//#7115:Start
            // var  boolhien =  compareSummarydate(datsummary_month,datenow,newsmday);
            var  boolhien = 1;

            if (datenow != null && datenow != '') {
                boolhien =  compareSummarydate(datsummary_month,datenow,newsmday);
            }
//#7115:End
    		//console.log("SSSSSSSSSSSSSSSSS"+row.entity.flag_is_accceptance_input);
//#7158:Start
//    		var is_acceptance_input = row.entity.flag_is_accceptance_input;
//    		if(is_acceptance_input == null) is_acceptance_input = 0 ;
//    		if(post_is_acceptance_input == 1 || is_acceptance_input == 1){
//	    	    	boolhien = 0;
//	    	}
//#7158:End
    	    if(boolhien == 0){
    	    	return false;
    	    }
    	    else{
    	        return row.entity.can_add;
    	    }


    	}
    	return false;
    }

    $scope.can_separate = function(row) {
//#7158:Start
//	    	var datsummary_month = 	closingdateend;
//	        var datenow = row.entity.summary_month;
//	        var  newsmday = row.entity.summary_month_for_save;
            var datsummary_month = closingdateend;
            var datenow          = row.entity.summary_month_for_save;
            var newsmday         = row.entity.summary_month_for_save;
//#7158:End
//#7115:Start
	        // var boolhien =  compareSummarydate(datsummary_month,datenow,newsmday);
	  	    var  boolhien = 1;

            if (datenow != null && datenow != '') {
                boolhien =  compareSummarydate(datsummary_month,datenow,newsmday);
            }
//#7115:End

//#7158:Start
//	  	    var is_acceptance_input = row.entity.flag_is_accceptance_input;
//  		    if(is_acceptance_input == null) is_acceptance_input = 0 ;
//  	    	if(post_is_acceptance_input == 1 || is_acceptance_input == 1){
//		    	boolhien = 0;
//		    }
//#7158:End
		    if(boolhien == 0){
		    	return false;
		    }
		    else{
//#7158:Start
//                return  row.entity.can_separate && row.entity.j_code && row.entity.branch_cd && row.entity.order_id;
                return  row.entity.can_separate
		    }
//            return row.entity.can_separate && row.entity.j_code && row.entity.branch_cd && row.entity.order_id;
            return  row.entity.can_separate
//#7158:End
    }
    $scope.can_delete = function(row) {
//#7113:Start
	  //   	var is_acceptance_input = row.entity.flag_is_accceptance_input;
			// if(is_acceptance_input == null) is_acceptance_input = 0 ;
			// if(post_is_acceptance_input == 1 || is_acceptance_input == 1){
	  // 	    	return false;
	  // 	    }
//#7113:End

	        if (row.entity.is_deleting) return false;
//#7113:Start
            // return row.entity.can_delete && row.entity.order_acceptance_status == order_acceptance_status_val.unapproved;
            if (row.entity.can_delete) {
                var boolhien = 1;
//#8436:START
//                if (row.entity.summary_month != null && row.entity.summary_month != '') {
                if ((row.entity.summary_month != null && row.entity.summary_month != '') || (row.entity.summary_month_for_save != null && row.entity.summary_month_for_save != '')) {
//#8436:End
//#7158:Start
//#8436:START
                    boolhien = compareSummarydate(closingdateend, row.entity.summary_month, row.entity.summary_month_for_save);
//                    boolhien = compareSummarydate(closingdateend, row.entity.summary_month_for_save, row.entity.summary_month_for_save);
//#8436:End

//#7158:End
                }
                return boolhien;
            }

            return false;
//#7113:End

    }

    $scope.can_edit = function(row) {
//#7115:Start
        if (row.entity.summary_month_has_been_edited == true) return true;
//#7115:End

    	// #6536 -S- modify
    	var  can_edit = row.entity.can_edit;
    	var  datsummary_month = 	closingdateend;
 		var  datenow = row.entity.summary_month;
 		var  newsmday = row.entity.summary_month_for_save;
//#7115:Start
        // var  boolhien =  compareSummarydate(datsummary_month,datenow,newsmday);
        var  boolhien = 1;

        if (datenow != null && datenow != '') {
            boolhien =  compareSummarydate(datsummary_month,datenow,newsmday);
        }
//#7115:End

 		var is_acceptance_input = row.entity.flag_is_accceptance_input;

 	    if(boolhien == 0){

//#6946:Start
 	    	var is_new = row.entity.is_new;

 	    	if(is_new){
 	    		can_edit = true;
 	    	}
 	    	else{
 	    		can_edit = false;
 	    	}
//#6946:End
 	    }
 	    else{
 	    	can_edit = boolhien;
 	    }


        return can_edit && row.entity.order_acceptance_status != order_acceptance_status_val.account_tighten;

        // #6536 -E - modify
    }

    $scope.deleteRow = function(row) {
        row.entity.is_deleting = true;
        row.entity.is_editing = true;
    };

    $scope.undo_delete = function(row) {

//#6945:Start
        row.entity.is_editing = true;
//#6945:End

        row.entity.is_deleting = false;
    }

//#7846:Start
//    function set_width_of_action() {
//    	//test = $(".action-btn-wrapper").style("background", );
//		    	//console.log(test)
//		//    	 datas = $scope.gridApi.grid.appScope.gridOptions.data;
//		//    	 console.log(datas);
//		    	//console.log(isAdddButton);
//		return 200;
//    }
//#7846:End

//#7990:Start
    $scope.backupdaterow = function(row){
    	if(!row.is_editing){
    		backupdate(row,true);
    	}


    }
//#7990:End

    $scope.difference_common = function(row) {

//#7990:Start
    	if(row.entity.j_code_has_oldVal  == undefined){ row.entity.j_code_has_oldVal = row.entity.j_code; }
    	if(row.entity.branch_cd_has_oldVal  == undefined){ row.entity.branch_cd_has_oldVal = row.entity.branch_cd; }
    	if(row.entity.order_id_has_oldVal  == undefined){ row.entity.order_id_has_oldVal = row.entity.order_id;  }
    	if(row.entity.account_id_has_oldVal  == undefined){  row.entity.account_id_has_oldVal = row.entity.account_id; }
    	if(row.entity.client_name_has_oldVal  == undefined){  row.entity.client_name_has_oldVal = row.entity.client_name; }
    	if(row.entity.client_id_has_oldVal  == undefined){  row.entity.client_id_has_oldVal = row.entity.client_id; }
    	if(row.entity.summary_month_has_oldVal  == undefined){  row.entity.summary_month_has_oldVal = row.entity.summary_month; }
    	row.entity.is_change = true;
//#7990:End

        row.entity.j_code        = '';
        row.entity.branch_cd     = '';
        row.entity.order_id      = null;
        row.entity.account_id    = null;
        row.entity.client_name   = null;
        row.entity.client_id     = null;
        row.entity.summary_month = '';
//#7115:Start
        row.entity.summary_month_for_save = '';
        row.entity.is_editing = true;

//#7115:End
//#7158:Start
//        row.entity.is_error_j_code_branch = false;
//#7158:End  if((colDef.field == 'j_code') || (colDef.field == 'branch_cd')){

    }

    $scope.post_is_acceptance_input = post_is_acceptance_input;

    var columnDefs = [
        {
            field: 'approved_all',
            displayName: label.approve_all,
            width: 73,
            visible: oa_can_approve,
            enableCellEdit: false,
            enableSorting: false,
//#7155:Start
//            enableColumnResize: false,
            enableColumnResize: true,
//#7155:End
            cellTemplate: "<div class='center-checkbox-wrapper'><input type='checkbox' ng-show='grid.appScope.show_approved_all(row)' ng-model='row.entity.is_approved' ng-init='row.entity.is_approved = row.entity.is_approved ? true : false' ng-disabled='row.entity.is_deleting'></div>"
        },
        {

            field: 'action',
            displayName: label.action,
//#7846:Start
//            width : set_width_of_action(),
            width : 200,
//#7846:End
            visible: oa_can_modify,
            enableCellEdit: false,
//#7105:Start
            // enableColumnResize: false,
            enableColumnResize: true,
//#7105:End
            enableSorting: false,
            cellTemplate:
                    '<div class="action-btn-wrapper">'+
                        '<button class="btn primary btn-small btn-green e-control" ng-show="grid.appScope.can_add(row)" ng-click="grid.appScope.insertRow(row.entity)" ng-disabled="row.entity.is_deleting" ng-class="{\'hide-btn\': row.entity.is_deleting}">'+btn.add+'</button>'+
                        '<button ng-show="grid.appScope.can_separate(row)" ng-click="grid.appScope.separateRow(row.entity)" class="btn primary btn-small btn-green e-control" ng-disabled="row.entity.is_deleting" ng-class="{\'hide-btn\': row.entity.is_deleting}">'+btn.separate+'</button>'+
                        '<button class="btn primary btn-small btn-green e-control" ng-show="grid.appScope.can_delete(row)" ng-click="grid.appScope.deleteRow(row)">'+btn.delete+'</button>'+
                        '<button class="btn primary btn-small btn-green btn-undo e-control" ng-show="row.entity.is_deleting" ng-click="grid.appScope.undo_delete(row)">'+btn.undo+'</button>'+
                    '</div>'
        },
        {
            field: 'edit',
            displayName: label.edit,
            width: 55,
            visible: oa_can_modify,
            enableSorting: false,
            enableCellEdit: false,
//#7154:Start
//            enableColumnResize: false,
            enableColumnResize: true,
//#7154:End
//#7990:Start
//#6946:Start
//            cellTemplate: "<div class='center-checkbox-wrapper' ng-show='grid.appScope.can_edit(row)'><input type='checkbox' ng-init='row.entity.is_editing = row.entity.is_editing == true ? true : false' ng-model='row.entity.is_editing' ng-disabled='row.entity.order_acceptance_status == "+UNAPPROVED+" ||row.entity.is_deleting || grid.appScope.post_is_acceptance_input'></div>"


//#8134:Start
//            cellTemplate: "<div class='center-checkbox-wrapper' ng-show='grid.appScope.can_edit(row)'><input type='checkbox' ng-init='row.entity.is_editing = row.entity.is_editing == true ? true : false' ng-model='row.entity.is_editing' ng-disabled='row.entity.is_deleting  || grid.appScope.post_is_acceptance_input '></div>"
//            cellTemplate: "<div class='center-checkbox-wrapper' ng-show='grid.appScope.can_edit(row)'><input type='checkbox' ng-init='row.entity.is_editing = row.entity.is_editing == true ? true : false' ng-model='row.entity.is_editing' ng-disabled='row.entity.is_deleting'></div>"
 			cellTemplate: "<div class='center-checkbox-wrapper' ng-show='grid.appScope.can_edit(row)'><input type='checkbox' ng-click='grid.appScope.backupdaterow(row.entity,true)' ng-init='row.entity.is_editing = row.entity.is_editing == true ? true : false' ng-model='row.entity.is_editing' ng-disabled='row.entity.is_deleting'></div>"

//#8134:End

//#6946:End
//#7990:End

        },
        {
            field: 'order_acceptance_status',
            displayName: label.order_acceptance_status,
            width: 100,
            enableCellEdit: false,
            cellFilter: 'mapOrderAcceptanceStatus'
        },
        {
            field: 'difference_common',
            displayName: label.difference_common,
            width: 85,

//#7042:Start
//#6946:Start
//            cellTemplate: "<div class='center-checkbox-wrapper'><input type='checkbox' ng-disabled=' (row.entity.order_acceptance_status !="+ UNAPPROVED+" && (!row.entity.is_editing) ) || row.entity.is_separate || row.entity.is_deleting'  ng-checked='row.entity.is_difference_common' ng-model='row.entity.is_difference_common' ng-click='grid.appScope.difference_common(row)'></div>",
//#6946:End
//#7158:Start
//            cellTemplate: "<div class='center-checkbox-wrapper'><input type='checkbox' ng-disabled='"+!is_auth_change+" || (row.entity.order_acceptance_status !="+ UNAPPROVED+" && (!row.entity.is_editing) ) || row.entity.is_separate || row.entity.is_deleting'  ng-checked='row.entity.is_difference_common' ng-model='row.entity.is_difference_common' ng-click='grid.appScope.difference_common(row)'></div>",
//#7766:Start
//            cellTemplate: "<div class='center-checkbox-wrapper'><input type='checkbox' ng-disabled='"+!is_auth_change+" || (row.entity.order_acceptance_status !="+ UNAPPROVED+" && (!row.entity.is_editing) ) || row.entity.is_deleting || !grid.appScope.can_edit(row)'  ng-checked='row.entity.is_difference_common' ng-model='row.entity.is_difference_common' ng-click='grid.appScope.difference_common(row)'></div>",
            cellTemplate: "<div class='center-checkbox-wrapper'><input type='checkbox' ng-disabled='"+!is_auth_change+" || (row.entity.order_acceptance_status !="+ UNAPPROVED+" && (!row.entity.is_editing) ) || row.entity.is_deleting || !grid.appScope.can_edit(row) || (row.entity.is_separate && row.entity.separate_has_j_code_branch_cd)'  ng-checked='row.entity.is_difference_common' ng-model='row.entity.is_difference_common' ng-click='grid.appScope.difference_common(row)'  ></div>",
//#7766:End
//#7158:End
//#7042:End
            enableCellEdit: false

        },
        {
            field: 'j_code',
            displayName: label.jcode,
            width: 80,
            cellFilter: 'mapJCode:row',
//#7111:Start
            //cellTemplate: '<a ng-class="{\'no-link\' : !row.entity.order_id || row.entity.is_editing || row.entity.order_id == 0}"><div class="cell-line-height" ng-click="grid.appScope.show_order(row)">{{COL_FIELD CUSTOM_FILTERS}}</div></a>',
            cellTemplate: '<div class="ui-grid-cell-contents"><a ng-class="{\'no-link\' : !row.entity.order_id || row.entity.is_editing || row.entity.order_id == 0}" ng-click="grid.appScope.show_order(row)">{{COL_FIELD CUSTOM_FILTERS}}</a></div>',
//#7111:End
            cellEditableCondition: function($scope) {

//#7042:Start
//#6946:Start
//return ($scope.row.entity.is_editing || $scope.row.entity.order_acceptance_status == UNAPPROVED) && !$scope.row.entity.is_difference_common && !$scope.row.entity.is_separate && !$scope.row.entity.is_deleting;
//#6946:End
//#7158:Start
//                return  is_auth_change && ($scope.row.entity.is_editing || $scope.row.entity.order_acceptance_status == UNAPPROVED) && !$scope.row.entity.is_difference_common && !$scope.row.entity.is_separate && !$scope.row.entity.is_deleting;
//#7766:Start
//                return  is_auth_change && ($scope.row.entity.is_editing || $scope.row.entity.order_acceptance_status == UNAPPROVED) && !$scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting && $outsideScope.can_edit($scope.row);
                return  is_auth_change && ($scope.row.entity.is_editing || $scope.row.entity.order_acceptance_status == UNAPPROVED) && !$scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting && $outsideScope.can_edit($scope.row) && (!$scope.row.entity.is_separate || ($scope.row.entity.is_separate && !$scope.row.entity.separate_has_j_code_branch_cd));
//#7766:End

//#7158:End
//#7042:End
            }
        },
        {
            field: 'branch_cd',
            width: 40,
            displayName: label.branch,
            cellFilter: 'mapBranchCd:row',
            cellEditableCondition: function($scope) {
//#7042:Start
//#6946:Start
//                return  ($scope.row.entity.is_editing || $scope.row.entity.order_acceptance_status == UNAPPROVED) && !$scope.row.entity.is_difference_common && !$scope.row.entity.is_separate && !$scope.row.entity.is_deleting;
//#6946:End
//#7158:Start
//                return  is_auth_change && ($scope.row.entity.is_editing || $scope.row.entity.order_acceptance_status == UNAPPROVED) && !$scope.row.entity.is_difference_common && !$scope.row.entity.is_separate && !$scope.row.entity.is_deleting;
//#7766:Start
//                return  is_auth_change && ($scope.row.entity.is_editing || $scope.row.entity.order_acceptance_status == UNAPPROVED) && !$scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting && $outsideScope.can_edit($scope.row);
                return  is_auth_change && ($scope.row.entity.is_editing || $scope.row.entity.order_acceptance_status == UNAPPROVED) && !$scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting && $outsideScope.can_edit($scope.row) && (!$scope.row.entity.is_separate || ($scope.row.entity.is_separate && !$scope.row.entity.separate_has_j_code_branch_cd));
//#7766:End

//#7158:End
//#7042:End
            }
        },
        {
            field: 'account_id',
            width: 100,
            displayName: label.account_id,
            editableCellTemplate: 'ui-grid/dropdownEditor',
//#7846:Start
//            cellFilter : 'mapAccountId:row.entity.account_options',
            cellFilter : 'mapAccountId',
            editDropdownIdLabel: 'id',
            editDropdownValueLabel: 'name',
            enableCellEdit: true,
//            editDropdownRowEntityOptionsArrayPath : 'account_options',
            editDropdownOptionsArray : glb_account_options,
//#7846:End
            cellEditableCondition: function($scope) {
//#7126:Start
//                return $scope.row.entity.is_editing && $scope.row.entity.is_difference_common && !$scope.row.entity.is_separate && !$scope.row.entity.is_deleting;
                if ($scope.row.entity.order_acceptance_status == order_acceptance_status_val.approved) {
//#7158:Start
//                    return $scope.row.entity.is_editing && $scope.row.entity.is_difference_common && !$scope.row.entity.is_separate && !$scope.row.entity.is_deleting;
//#7766:Start
//                    return $scope.row.entity.is_editing && $scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting;
                    return $scope.row.entity.is_editing && $scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting && (!$scope.row.entity.is_separate || ($scope.row.entity.is_separate && !$scope.row.entity.separate_has_j_code_branch_cd));
//#7766:End
//#7158:end
                }
//#7158:Start
//                return $scope.row.entity.is_difference_common && !$scope.row.entity.is_separate && !$scope.row.entity.is_deleting;
//#7766:Start
//                return $scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting && $outsideScope.can_edit($scope.row);
                return $scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting && $outsideScope.can_edit($scope.row) && (!$scope.row.entity.is_separate || ($scope.row.entity.is_separate && !$scope.row.entity.separate_has_j_code_branch_cd));
//#7766:End
//#7158:End
//#7126:End
            }
        },
        {
            field: 'arrange_rep',
            width: 100,
            enableCellEdit: true,
            displayName: label.arrange_rep,
            editableCellTemplate : 'ui-grid/dropdownEditor',
//#7846:Start
//#7167:Start
//          cellFilter: 'mapArrangeRep:row.entity.account_options',
//            cellFilter: 'mapArrangeRep:row.entity.arrange_rep_options',
            cellFilter: 'mapArrangeRep',
//#7167:End
//#7846:End
            editDropdownIdLabel: 'id',
            editDropdownValueLabel : 'name',
//#7846:Start
//#7167:Start
//          editDropdownRowEntityOptionsArrayPath : 'account_options'
//            editDropdownRowEntityOptionsArrayPath : 'arrange_rep_options'
            editDropdownOptionsArray : glb_arrange_rep_options,
//#7167:End
//#7846:End
        },
        {
            field: 'client_name',
            width: 100,
            displayName: label.client_name,
//#7111:Start
            //cellTemplate: '<a ng-class="{\'no-link\' : !row.entity.client_id}"><div class="cell-line-height" ng-click="grid.appScope.show_client(row)">{{row.entity.client_name}}</div></a>',
            cellTemplate: '<div class="ui-grid-cell-contents"><a ng-class="{\'no-link\' : !row.entity.client_id}" ng-click="grid.appScope.show_client(row)">{{row.entity.client_name}}</a></div>',
//#7111:End
            cellEditableCondition: function($scope) {
//#7126:Start
//                return $scope.row.entity.is_editing && $scope.row.entity.is_difference_common && !$scope.row.entity.is_separate && !$scope.row.entity.is_deleting;
                if ($scope.row.entity.order_acceptance_status == order_acceptance_status_val.approved) {
//#7158:Start
//                    return $scope.row.entity.is_editing && $scope.row.entity.is_difference_common && !$scope.row.entity.is_separate && !$scope.row.entity.is_deleting;
//#7766:Start
//                    return $scope.row.entity.is_editing && $scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting;
                    return $scope.row.entity.is_editing && $scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting && (!$scope.row.entity.is_separate || ($scope.row.entity.is_separate && !$scope.row.entity.separate_has_j_code_branch_cd));
//#7766:End
//#7158:End
                }
//#7158:Start
//                return $scope.row.entity.is_difference_common && !$scope.row.entity.is_separate && !$scope.row.entity.is_deleting;
//#7766:Start
//                return $scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting && $outsideScope.can_edit($scope.row);
                return $scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting && $outsideScope.can_edit($scope.row) && (!$scope.row.entity.is_separate || ($scope.row.entity.is_separate && !$scope.row.entity.separate_has_j_code_branch_cd));
//#7766:End
//#7158:End
//#7126:End
            },
            cellFilter: 'mapClientName:row'
        },
        {
            field: 'source_subcontractor_id',
            displayName: label.source_subcontractor_id,
            width: 100,
            editableCellTemplate : 'ui-grid/dropdownEditor',
            cellFilter: 'mapSubcontractorId',
            editDropdownIdLabel: 'id',
            editDropdownValueLabel : 'abbr_name',
//#7846:Start
//            editDropdownRowEntityOptionsArrayPath : 'direct_subcontractor_options'
            editDropdownOptionsArray : glb_direct_subcontractor_options,
//#7846:End
        },
        {
            field: 'work_subcontractor_id',
            displayName: label.work_subcontractor_id,
            width: 100,
            editableCellTemplate : 'ui-grid/dropdownEditor',
//#7846:Start
            cellFilter: 'mapIndirectSubcontractorId',
            editDropdownIdLabel: 'id',
            editDropdownValueLabel: 'abbr_name',
//            editDropdownRowEntityOptionsArrayPath: 'indirect_subcontractor_options'
            editDropdownOptionsArray: glb_indirect_subcontractor_options,
//#7846:End
        },
        {
            field: 'shipping_subcontractor_id',
            displayName: label.shipping_subcontractor_id,
            width: 100,
            editableCellTemplate: 'ui-grid/dropdownEditor',
//#7846:Start
            cellFilter: 'mapIndirectSubcontractorId',
            editDropdownIdLabel: 'id',
            editDropdownValueLabel: 'abbr_name',
//            editDropdownRowEntityOptionsArrayPath: 'indirect_subcontractor_options'
            editDropdownOptionsArray: glb_indirect_subcontractor_options,
//#7846:End
        },
        {
            field: 'shipping_type_cd',
            displayName: label.shipping_type_cd,
            width: 100,
            editableCellTemplate: 'ui-grid/dropdownEditor',
            editDropdownIdLabel: 'code',
            editDropdownValueLabel: 'name',
//#7846:Start
//            cellFilter: 'mapShippingTypeCd:row.entity.shipping_type_options',
//            editDropdownRowEntityOptionsArrayPath : 'shipping_type_options'
            cellFilter: 'mapShippingTypeCd',
            editDropdownOptionsArray : glb_shipping_type_options,
//#7846:End
        },
        {
            field: 'set_name',
            displayName: label.set_name,
            width: 80
        },
        {
            field: 'unit_price',
            displayName: label.unit_price,
            width: 80,
            cellFilter: 'mapUnitPrice:row',

//* #6946:Start
//            cellFilter: 'number:2',
//* #6946:End

            cellClass: 'a-right'
        },
        {
            field: 'quantity',
            width: 80,
            displayName: label.quantity,
            cellFilter: 'mapQuantity:row',

//* #6946:Start
//            cellFilter: 'number:0'
//* #6946:End

        },
        {
            field: 'delivery_date',
            displayName: label.delivery_date,
            width: 90,
            cellFilter: 'mapDeliveryDate:row'
        },
        {
            field: 'fee',
            width: 40,
            displayName: label.fee,
            cellFilter: 'mapFee:row',
            cellFilter: 'number:0',
            cellClass: 'a-right'
        },
        {
            field: 'weight',
            displayName: label.weight,
            width: 65,
            cellFilter: 'mapWeight:row'
        },
        {
            field: 'shipments_shape_cd',
            displayName: label.shipments_shape_cd,
            width: 100,
            editableCellTemplate: 'ui-grid/dropdownEditor',
            editDropdownIdLabel: 'code',
            editDropdownValueLabel: 'name',
//#7846:Start
            //cellFilter: 'mapShippmentsShapeCd:row.entity.shipments_shape_options',
            cellFilter: 'mapShippmentsShapeCd',
//            editDropdownRowEntityOptionsArrayPath : 'shipments_shape_options'
            editDropdownOptionsArray : glb_shipments_shape_options,
//#7846:End

        },
        {
            field: 'summary_month',
            width: 70,
            displayName: label.summary_month,
            cellFilter: 'mapSummaryMonth:row',
            cellEditableCondition: function($scope) {
//#7126:Start
//                return $scope.row.entity.is_editing && $scope.row.entity.is_difference_common && !$scope.row.entity.is_separate && !$scope.row.entity.is_deleting;
                if ($scope.row.entity.order_acceptance_status == order_acceptance_status_val.approved) {
//#7158:Start
//                    return $scope.row.entity.is_editing && $scope.row.entity.is_difference_common && !$scope.row.entity.is_separate && !$scope.row.entity.is_deleting;
//#7766:Start
//                    return $scope.row.entity.is_editing && $scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting;
                    return $scope.row.entity.is_editing && $scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting && (!$scope.row.entity.is_separate || ($scope.row.entity.is_separate && !$scope.row.entity.separate_has_j_code_branch_cd));
//#7766:End
//#7158:End
                }
//#7158:Start
//                return $scope.row.entity.is_difference_common && !$scope.row.entity.is_separate && !$scope.row.entity.is_deleting;
//#7766:Start
//                return $scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting && $outsideScope.can_edit($scope.row);;
                return $scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting && $outsideScope.can_edit($scope.row) && (!$scope.row.entity.is_separate || ($scope.row.entity.is_separate && !$scope.row.entity.separate_has_j_code_branch_cd));
//#7766:End
//#7158:End
//#7126:End
            }
        },
        {
            field: 'tax_free',
            displayName: label.free_tax,
            width: 75,
            cellFilter: 'mapTaxFree:row',
            cellClass: 'a-right',

//#6946:Start
//            cellFilter: 'number:0'
//#6946:End
        },
        {
            field: 'shipping_charge_tax',
            displayName: label.shipping_charge_tax,
            width: 100,
            cellFilter: 'mapShippingChargeTax:row',

//#6946:Start
//            cellFilter: 'number:0',
//#6946:End

            cellClass: 'a-right'
        },
        {
            field: 'stamp_tax',
            displayName: label.stamp_tax,
            width: 100,
            cellFilter: 'mapStampTax:row',

//#6946:Start
//            cellFilter: 'number:0',
//#6946:End

            cellClass: 'a-right'
        },
        {
            field: 'etc_tax',
            width: 100,
            displayName: label.etc_tax,
            cellFilter: 'mapEtcTax:row',

//#6946:Start
//            cellFilter: 'number:0',
//#6946:End

            cellClass: 'a-right'
        },
        {
            field: 'shipping_charge',
            displayName: label.shipping_charge,
            width: 100,
            cellFilter: 'mapShippingCharge:row',

//#6946:Start
//            cellFilter: 'number:0',
//#6946:End
            cellClass: 'a-right'
        },
        {
            field: 'etc',
            displayName: label.etc,
            width: 100,
            cellFilter: 'mapEtc:row',

//#6946:Start
//            cellFilter: 'number:0',
//#6946:End

            cellClass: 'a-right'
        },
        {
            field: 'amount',
            displayName: label.amount,
            width: 100,
            enableCellEdit: false,
            type: 'number',
            cellClass: 'a-right',
//#7114:Start
            // cellFilter: 'number:0'
//#7114:End
//#6838:Start
            cellFilter: 'mapAmount:row',
//#6838:End
        },
    ];
    $scope.gridOptions = {
    	// #6784-S
    	enableCellEditOnFocus: true, // set any editable column to allow edit on focus
//#7846:Start
//    	nameColunmLast:label.amount,
//#7846:End
    	// #6784-E
        enablePagination: false,
        enableSorting: true,
        enableFiltering: false,

        // enableCellEdit: false,
        // multiSelect  : true,
        columnDefs: columnDefs,
        enableRowSelection: false,
        // enableSelectAll: false,
        //show menu in the right hand
        enableGridMenu: false,
        //each column have own menu, set to true to show the menu of that column
        enableColumnMenus: true,
//#7846:Start
//        exporterCsvFilename: label.export_csv_file_name,
//#7846:End
        // trigger for the time auto call save event when a cell was edited. Set -1 to turn it off
        rowEditWaitInterval : -1,
        // control the scroll bar of grid, there are two value accepted ALWAYS/NEVER
        enableHorizontalScrollbar: uiGridConstants.scrollbars.ALWAYS,
        enableVerticalScrollbar: uiGridConstants.scrollbars.ALWAYS,
        rowTemplate:
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ui-grid-cell ng-class="{\'new-grid-row\': row.entity.is_new, \'row-error\': row.entity.is_error_j_code || row.entity.is_error_branch_cd || row.entity.is_error_unit_price || row.entity.is_error_quantity || row.entity.is_error_delivery_date || row.entity.is_error_fee || row.entity.is_error_weight || row.entity.is_error_summary_month || row.entity.is_error_tax_free || row.entity.is_error_shipping_charge_tax || row.entity.is_error_stamp_tax || row.entity.is_error_etc_tax || row.entity.is_error_shipping_charge || row.entity.is_error_etc || row.entity.is_error_j_code_branch, \'row-deleting\': row.entity.is_deleting}" ></div>',

        // use this to customize the header name
//#7846:Start
//        exporterHeaderFilter: function(displayName) {
//            return displayName;
//        },
//#7846:End
        cellEditableCondition: function($scope){

//#7042:Start
        	if(is_auth_change){
                if ($scope.row.entity.is_deleting) return false;
//#6946:Start
                          if($scope.row.entity.order_acceptance_status == UNAPPROVED){
//#7158:Start
//                          	return true;
                            return $outsideScope.can_edit($scope.row);
//#7158:End
                          }
                          else{
                          	 return $scope.row.entity.is_editing;
                          }

//                          return true;
//#6946:End
        	}
//#7042:End

        },
        onRegisterApi: function(gridApi){

            $scope.gridApi = gridApi;

            gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
//#7790:Start
            	 if(rowEntity.amount_has_oldVal  == undefined){
            	 
                	rowEntity.amount_has_oldVal = rowEntity.amount;
    			}
//#7790:End

//#6946:Start

            	if (oldValue != newValue) {

            		rowEntity.is_change  = true;

            		rowEntity.is_editing = true;
            		just_save = false;
//#7115:Start
                    rowEntity[colDef.field + '_has_been_edited'] = true;
//#7990:Start

                    if(rowEntity[colDef.field + '_has_oldVal']  == undefined){
                    	rowEntity[colDef.field + '_has_oldVal'] = oldValue;
        			}


                    if((colDef.field == 'j_code') || (colDef.field == 'branch_cd')){
                    var $colNameGetVal =['account_id',
                                         'client_name',
                                         'summary_month'
                                        ];

//            			console.log($colName);
                        $colNameGetVal.forEach(function($rowcolName) {

            					if(rowEntity[$rowcolName +'_has_oldVal'] ==  undefined){
            						$rowcolName = $rowcolName.trim();
            						rowEntity[$rowcolName + '_has_oldVal'] = rowEntity[$rowcolName];

            					}

            			});
                    }
//#7990:End

//#7115:End
            	}
//#6946:End
                if (colDef.field == 'tax_free' || colDef.field == 'shipping_charge_tax' || colDef.field == 'stamp_tax' || colDef.field == 'etc_tax' || colDef.field == 'shipping_charge' || colDef.field == 'etc') {

                    rowEntity.amount = null;
                    var allrong = true;

                    if((rowEntity.tax_free != "") && (rowEntity.tax_free != "null")){
                    	allrong = false;
                    }
                    if((rowEntity.shipping_charge_tax != "") && (rowEntity.shipping_charge_tax != "null")){
                    	allrong = false;
                    }
                    if((rowEntity.stamp_tax != "") && (rowEntity.stamp_tax != "null")){
                    	allrong = false;
                    }
                    if((rowEntity.etc_tax != "") && (rowEntity.etc_tax != "null")){
                    	allrong = false;
                    }
//#7115:Start
                    if((rowEntity.shipping_charge != "") && (rowEntity.shipping_charge != "null")){
                        allrong = false;
                    }
//#7115:End
                    if((rowEntity.etc != "") && (rowEntity.etc != "null")){
                    	allrong = false;
                    }
                    if(allrong){rowEntity.amount = null;}
                    if(!allrong){
	                    if (rowEntity.tax_free || rowEntity.shipping_charge_tax || rowEntity.stamp_tax || rowEntity.etc_tax || rowEntity.shipping_charge || rowEntity.etc) {
//#7846:Start
                            var tmp_tax_rate            = 0,
                                shipping_charge_tax     = 0,
                                stamp_tax               = 0,
                                etc_tax                 = 0,
                                tax_free                = 0,
                                shipping_charge         = 0,
                                etc                     = 0,
                                tmp_shipping_charge_tax = 0,
                                tmp_stamp_tax           = 0,
                                tmp_etc_tax             = 0;
//#7846:End
//#7125:Start
	                        //tmp_tax_rate        = parseFloat(rowEntity.consumption_tax.rate);
	                        tmp_tax_rate        = parseFloat(get_consumption_tax(rowEntity.summary_month_for_save));
//#7125:End
	                        shipping_charge_tax = parseFloat(rowEntity.shipping_charge_tax) ? parseFloat(rowEntity.shipping_charge_tax) : 0;
	                        stamp_tax           = parseFloat(rowEntity.stamp_tax) ? parseFloat(rowEntity.stamp_tax) : 0;
	                        etc_tax             = parseFloat(rowEntity.etc_tax) ? parseFloat(rowEntity.etc_tax) : 0;
	                        tax_free            = parseFloat(rowEntity.tax_free) ? parseFloat(rowEntity.tax_free) : 0;
	                        shipping_charge     = parseFloat(rowEntity.shipping_charge) ? parseFloat(rowEntity.shipping_charge) : 0;
	                        etc                 = parseFloat(rowEntity.etc) ? parseFloat(rowEntity.etc) : 0;

	                        tmp_shipping_charge_tax = f_ceil(shipping_charge_tax / (1 + (tmp_tax_rate / 100)));
	                        tmp_stamp_tax           = f_ceil(stamp_tax / (1 + (tmp_tax_rate / 100)));
	                        tmp_etc_tax             = f_ceil(etc_tax / (1 + (tmp_tax_rate / 100)));
	                        rowEntity.amount        =  tmp_shipping_charge_tax + tmp_stamp_tax + tmp_etc_tax + tax_free + shipping_charge + etc;

                            if (rowEntity.amount <= -1000000000 || rowEntity.amount >= 1000000000) {

//#6946:Start
//                                    var error = "<div class='error-message'>" + '合計(税別、非課税)は１０桁以上の入力ができません。管理部に問い合わせてください。' + "</div>";
//                                    $("#dialog-errors").html(error);
//                                    $("#dialog-errors").dialog({
//                                        title: '確認',
//                                        height: 'auto',
//                                        minWidth: 500,
//                                        minHeight: 250,
//                                        maxHeight: 250,
//                                        resizable: true,
//                                        modal: false,
//                                        dialogClass: 'acceptance-dialog fixed-dialog',
//                                        position: { my: "right top+30", at: "right top+30", of: window},
//                                        dragStart: function(event, ui) {
//                                            $('.acceptance-dialog').removeClass('fixed-dialog')
//                                        },
//                                        dragStop: function(event, ui) {
//                                            // $('.acceptance-dialog').addClass('fixed-dialog')
//                                        },
//                                        resizeStart: function( event, ui ) {
//                                            $(this).dialog("option", "maxHeight", false);
//                                        }
//                                    });
//#6946:End

                            }
	                    }
                    }
                    else{
                    	rowEntity.amount = null; return;
                    }

                }

                if (colDef.field == 'j_code' || colDef.field == 'branch_cd') {
                    rowEntity.account_id    = null;
                    rowEntity.client_name   = null;
                    rowEntity.client_id     = null;
                    rowEntity.order_id      = null;
                    rowEntity.summary_month = null;

                    if (!rowEntity.is_difference_common && rowEntity.j_code && rowEntity.branch_cd) {
                        var set_jcode_branch_cd_data = function(data) {
                             rowEntity.is_error_j_code_branch = true;
                            if (data) {
                            	// #6901 -Modify -S
                            	var is_change_report_apply = data['is_change_report_apply'];
                            	if(is_change_report_apply == 0){
//#7846:Start
                                    var summary_month = null;
//#7846:End
                            		 summary_month = data.delivery_date;
                                     summary_month = summary_month.replace(/-/g, '/');

                                     rowEntity.account_id             = data.j_account_id;
                                     rowEntity.client_name            = data.client_name;
                                     rowEntity.client_id              = data.client_id;
                                     rowEntity.order_id               = data.id;
                                     rowEntity.summary_month          = summary_month.substring(0, 7);
                                     rowEntity.is_error_j_code_branch = false;
                            	}
                            	// #6901 -Modify -E
                            }
                        }

                        $scope.get_order_detail(rowEntity.j_code, rowEntity.branch_cd, set_jcode_branch_cd_data);

                        // $http({
                        //     url : "/_admin/order_acceptance/ajax_get_order_detail",
                        //     method : 'POST',
                        //     data : {j_code: rowEntity.j_code, branch_cd: rowEntity.branch_cd}
                        // })
                        // .success(function(data, status, headers, config) {
                        //     rowEntity.is_error_j_code_branch = true;
                        //     if (data) {
                        //         summary_month = data.delivery_date;
                        //         summary_month = summary_month.replace(/-/g, '/');

                        //         rowEntity.account_id             = data.j_account_id;
                        //         rowEntity.client_name            = data.client_name;
                        //         rowEntity.client_id              = data.client_id;
                        //         rowEntity.order_id               = data.id;
                        //         rowEntity.summary_month          = summary_month.substring(0, 7);
                        //         rowEntity.is_error_j_code_branch = false;

                        //     }
                        // })
                    }
                }

                // if (colDef.field == 'source_subcontractor_id') {
                //     rowEntity.work_subcontractor_id = null;
                //     rowEntity.shipping_subcontractor_id = null;
                // }

//#6946:Start
                var currentcell = $scope.gridApi.cellNav.getFocusedCell();
                var fieldname = currentcell.col.field;
                setAllValidateNumber(rowEntity,fieldname);
//#6946:End

            });

            // trigger for auto save event. Will automatic called method $scope.saveRow
            // It is no use if grifOptions has set rowEditWaitInterval -1
            // then saves will never be triggered by timer, and only be triggered when manually called.
            //console.log('sdsdsdload');
            gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
        }

    };

    $scope.export = function(){
        // var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
        // $scope.gridApi.exporter.csvExport(uiGridExporterConstants.ALL, uiGridExporterConstants.VISIBLE, myElement);

        $("#search-form").attr('action', '/_admin/order_acceptance/export_csv');
        $("#search-form").submit();
        $("#search-form").attr('action', '/_admin/order_acceptance/show');
    };

    $scope.saveRow = function(rowEntity) {

        promise = $q.defer();
        $scope.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
        promise.reject();

    };

    $scope.show_order = function (row) {

        if (row.entity.is_editing || !row.entity.order_id || row.entity.order_id == 0) return;

        $("#dialog").dialog({
            title: lang['text_title_show_order'],     //#6849 :  MOdify -S
        })
        $rootScope.loadDetail(row.entity.order_id);
        $(window).trigger('resize');
    };

//#7846:Start
//    function display_error_for_disapprovel(data){
    function display_error_for_disapprovel(data, save_datas){
//#7846:End
    	 $(".messages").show();

         if (data.status) {
             $scope.error_messages = "<div class='success-message'>"+data.messages+"</div>";

             for (i = 0; i < save_datas.length; i++) {
                 if (save_datas[i]) {
                     save_datas[i].order_acceptance_status = order_acceptance_status_val.unapproved;
                 }
             }
             return;
         } else {
             if (typeof data.messages == 'object') {
//#7846:Start
                var error     = null,
                    row_num   = null,
                    flag      = false;
//#7846:End
                 error = "<div class='error-message'>";
                 angular.forEach(data.messages, function(rows, key) {
                     if (rows) {
                         row_num = parseInt(key) + 1;

                         if (!flag) {
                             flag = true;
                             error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
                         } else {
                             error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
                         }

                         angular.forEach(rows, function(message, key) {
                             error += "<p class='import-error'>" + message + "</p>";
                         });
                         // if save error set whatever column error status to true, then row turns red
                         datas[key].is_error_j_code_branch = true;
                     }
                 });
                 error += "</div>";
                 $("#dialog-errors").html(error);
                 $("#dialog-errors").dialog({
                     title: '確認',
                     height: 'auto',
                     minWidth: 500,
                     minHeight: 250,
                     maxHeight: 250,
                     resizable: true,
                     modal: false,
                     dialogClass: 'acceptance-dialog fixed-dialog',
                     position: { my: "right top+30", at: "right top+30", of: window},
                     dragStart: function(event, ui) {
                         $('.acceptance-dialog').removeClass('fixed-dialog')
                     },
                     dragStop: function(event, ui) {
                         // $('.acceptance-dialog').addClass('fixed-dialog')
                     },
                     resizeStart: function( event, ui ) {
                         $(this).dialog("option", "maxHeight", false);
                     }
                 })


             } else {
                 $scope.error_messages = "<div class='error-message'>"+data.messages+"</div>"
             }
         }
    }
    $scope.cancel_acceptance = function() {
//#7846:Start
        var datas = null,
            flag = false,
            save_datas = [];
//#7846:End
    	$(".loadgif").show();
        $scope.error_messages = '';
        datas = $scope.gridApi.grid.appScope.gridOptions.data;
//#6946:Start
        var changedata = false;
//#7846:Start
//        save_datas = [];
//        flag = false;
//#7846:End
        var k = 0;
        for(i = 0; i < datas.length; i++) {
            if (datas[i].is_approved && datas[i].order_acceptance_status != order_acceptance_status_val.unapproved) {

                save_datas[k] = datas[i];
                k++;
                flag = true;
                if(datas[i].is_editing || datas[i].is_deleting ){
        	    	changedata = true;

        	    }
//#6946:End
            }
        }

        if (!flag) {
            $("#dialog-messages").html(label.please_select_unapprove);
            $("#dialog-messages").dialog({
                title: '注意',
                width: 'auto',
                height: 147,
                modal: true,
                dialogClass: "acceptance-dialog",
                buttons: [{
                    text: "OK",
                    click: function() {
                    $(this).dialog("close");
                    }
                }],
            });
        	$(".loadgif").hide();
            return;
        }
//#6946:Start
        if(changedata){
        	if(!just_save){
        		$(".loadgif").hide();
        		  approved_now = false;
	           	 $("#dialog-messages").html('編集中のデータがあります。先に保存してください。');
	                $("#dialog-messages").dialog({
	                    title: '注意',
	                    width: 'auto',
	                    height: 147,
	                    modal: true,
	                    dialogClass: "acceptance-dialog",
	                    buttons: {
	                   	    " OK": function() {
	                   	    	just_save = false;
	                   	    	 $(this).dialog("close");
	                   	    	$(".loadgif").hide();
	                   	    	 return;
	                         }
	                    }
	               });
        	}
        }
//#6946:End
        var n_save = save_datas.length;

//        console.log(save_datas);
//        return;
		if(n_save >400){
			 $(".loadgif").show();
			var recode = 200;
			var n_lenght_for = n_save /recode;
			var n_du =  n_save % recode;
			var status = true;
			if(n_du > 0 ){
				n_lenght_for = parseInt(n_lenght_for) + 1;
			}

			var today = new Date();
			today = today.getDate()+"_"+today.getMonth()+"_"+today.getFullYear()+"_"+today.getHours()+"_"+today.getMinutes()+"_"+today.getSeconds();
			var name_file ="result_disapprovel_"+today+'.json';
			//console.log(name_file);
			//return;
			var tongkey = 0;
			var dem = 0;
//			n_lenght_for = 3;
			var successall = true;
//			console.log(n_lenght_for);
			var tongkey = 0;
			var dem = 0;
			var successall = true;

			for(var i = 0 ; i < n_lenght_for ; i++){
				$status = true;

				if(i == 0){

					var j_start = 0;
				}
				else{

					var j_start = parseInt(i)*recode;
				}

				if(i == (n_lenght_for -1) ){
					var  n_j = n_save;
				}
				else{
					var  n_j = parseInt(j_start) + recode;
				}

				var databach = new Array();
//                			console.log(j_start);
//                			console.log(n_j);

				for(var j  = j_start  ; j < n_j ;j++){
//					console.log(j);
//            		console.log(save_datas[j]);
				   if(save_datas[j] != undefined){
						databach.push(save_datas[j]);
					}

				}
				$http({
		            url : "/_admin/order_acceptance/cancel_acceptance",
		            method : 'POST',
		            data : {order_acceptance: databach , multi_larger:true,readfile:false,name_file:name_file,key:i}
		        })
		        .success(function(data, status, headers, config) {
		        	status  =  data['status'];
                    tongkey = tongkey +1;
                    if(status){
                    	dem ++;
                    }
                    if(!status){
                    	 $(".loadgif").hide();
//#7846:Start
//                         display_error_for_disapprovel(data);
                    	 display_error_for_disapprovel(data, save_datas);
//#7846:End

                    	 return false;

                    }
                    if(dem == n_lenght_for){
                    	 $http({
                    		 url : "/_admin/order_acceptance/cancel_acceptance",
                             method : 'POST',
                             data : {name_file:name_file, multi_larger:true,readfile:true,tongkey:tongkey}
                         })
                         .success(function(update_funcall, status, headers, config) {
                        	 $(".loadgif").hide();
//#7846:Start
//                              display_error_for_disapprovel(update_funcall);
                        	  display_error_for_disapprovel(update_funcall, save_datas);
//#7846:End
                         });

                    }
		        });
				if(!status){
					 $(".loadgif").hide();
					//break;
				}
			}
		}
		else{
		    $http({
	            url : "/_admin/order_acceptance/cancel_acceptance",
	            method : 'POST',
	            data : {order_acceptance: save_datas , multi_larger:false}
	        })
	        .success(function(data, status, headers, config) {
	        	$(".loadgif").hide();
//#7846:Start
//                 display_error_for_disapprovel(data);
	        	 display_error_for_disapprovel(data, save_datas);
//#7846:End

	        })
		}


    };

    $scope.checkalllist = function(){
//#7846:Start
        var datas = null;
//#7846:End
    	 datas = $scope.gridApi.grid.appScope.gridOptions.data;

    	     for(var i = 0; i < datas.length; i++) {
    			 var  is_approved_row = datas[i]['order_acceptance_status'];
                 var  jcode     = datas[i]['j_code'];
                 var  brandcode = datas[i]['branch_cd'];
                 var  amount = datas[i]['amount'];
                 var  isamountTrue = true;

	                if(amount == ''){
				 		isamountTrue = false;
				 	}
				 	else if(amount == null){
				 		 isamountTrue = false;
				 	}
	                if ((is_approved_row == order_acceptance_status_val.unapproved) && (isamountTrue)) {
//	                if (isamountTrue) {
	                	 datas[i].is_approved =  isCheckAll ;

	                 }
	                else {

	                	 datas[i].is_approved = false;

	                 }
	                if(!isamountTrue){

		                datas[i].can_approve = false;
	                }
	                else{

		                datas[i].can_approve = true;
	                }
             }
    	 isCheckAll = !isCheckAll;
    };

//#6946:Start
    $scope.approved_all=function(){
//#7846:Start
        var datas        = null,
            update_datas = null;
//#7846:End
    	$(".loadgif").show();
    	$scope.error_messages = '';
        datas = $scope.gridApi.grid.appScope.gridOptions.data;
        var changedata = false;
        var approved_flag  = false;
        var approved_now = true;
        update_datas = [];
        var k = 0;
        for( var i = 0; i < datas.length; i++) {
        	    datas[i].is_change_acceptance_status = datas[i].is_approved ? true : false;
        	    if(datas[i].is_approved && !datas[i].is_new){
        	    	if(!datas[i].is_deleting){
                	    update_datas[k] = datas[i];
                	    approved_flag = true;
                	    k++;
        	    	}
            	    if(datas[i].is_editing || datas[i].is_deleting ){
            	    	changedata = true;
            	    }


        	    }
        	    if(datas[i].is_new){
        	    	changedata = true;
        	    }
        }
        if(!approved_flag){
        	$(".loadgif").hide();
           $("#dialog-messages").html(label.please_select_approve);
           $("#dialog-messages").dialog({
               title: '注意',
               width: 'auto',
               height: 147,
               modal: true,
               dialogClass: "acceptance-dialog",
               buttons: [{
                   text: "OK",
                   click: function() {
                   $(this).dialog("close");
                   }
               }],
           });
           return;
        }

        if(changedata){
//        	console.log(just_save);
        	if(!just_save){
        		$(".loadgif").hide();
        		  approved_now = false;
	           	 $("#dialog-messages").html('編集中のデータがあります。先に保存してください。');
	                $("#dialog-messages").dialog({
	                    title: '注意',
	                    width: 'auto',
	                    height: 147,
	                    modal: true,
	                    dialogClass: "acceptance-dialog",
	                    buttons: {
	                   	    " OK": function() {
	                   	    	just_save = false;
	                   	    	 $(this).dialog("close");
	                   	    	 return;
	                         }
	                    }
	               });
        	}


        }

        if(approved_now){
        	$scope.saveupdateall(update_datas,true);
        }

    }

    $scope.saveupdateall = function(update_datas,is_approve_action) {

    	 just_save = true;
    	 var url = is_approve_action ? "/_admin/order_acceptance/approve" : "/_admin/order_acceptance/update";
			/*
			 * dividng many item to upload
			 * One refresh 100 record
			 */
			var n_date_update = update_datas.length;

			if(n_date_update > 400){
				        var status = true;
         			var recode = 200;
         			var n_lenght_for = n_date_update /recode;
         			var n_du = n_date_update % recode;
         			if(n_du > 0 ){
         				n_lenght_for = parseInt(n_lenght_for) + 1;
         			}

         			var today = new Date();
         			today = today.getDate()+"_"+today.getMonth()+"_"+today.getFullYear()+"_"+today.getHours()+"_"+today.getMinutes()+"_"+today.getSeconds();
         			var name_file ="result_approvel_"+today+'.json';
         			//console.log(name_file);
         			//return;
         			var tongkey = 0;
         			var dem = 0;
//         			n_lenght_for = 3;
         			var successall = true;
         			for(var i = 0 ; i < n_lenght_for ; i++){
         				$status = true;
         				var j_start = parseInt(i)*recode + 1;
         				if(i == (n_lenght_for -1) ){
         					var  n_j = n_date_update;
         				}
         				else{
         					var  n_j = parseInt(j_start) + recode;
         				}

         				var databach = new Array();
//                			console.log(j_start);
//                			console.log(n_j);

         				for(var j  = j_start  ; j < n_j ;j++){
//            					console.log(update_datas[j]);
         				   if(update_datas[j] != undefined){
         						databach.push(update_datas[j]);
         					}

         				}
         				var update_func = $http({
                             url : url,
                             method : 'POST',
                             data : {order_acceptance: databach,name_file:name_file,readfile:false,key:i,app_sel:false}
                         })
                        .success(function(update_func, status, headers, config) {
                             status = update_func['status'];

//                         	console.log(mess_err);

                             tongkey = tongkey +1;
                             if(status){
                             	dem ++;
                             }

                         	if(!status){
	            					    successall = false;
	            					    $(".loadgif").hide();
//
//	            					    console.log('update func: ' + mess_err);
//	            					    var mess_err = update_func['messages'];
	            					    var error ="";
	            					    angular.forEach(update_func.messages, function(rows, key) {
	            		                    if (rows) {
	            		                        row_num = parseInt(key) + 1 + j_start;

	            		                        error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
	            		                        angular.forEach(rows, function(message, key) {
	            		                            error += "<p class='import-error'>" + message + "</p>";
	            		                        });
	            		                    }
	            		                });
	            					    error += "</div>";
	            					    $("#dialog-errors").html(error);
	            				        $("#dialog-errors").dialog({
	            				            title: '確認',
	            				            height: 'auto',
	            				            minWidth: 500,
	            				            minHeight: 250,
	            				            maxHeight: 250,
	            				            resizable: true,
	            				            modal: false,
	            				            dialogClass: 'acceptance-dialog fixed-dialog',
	            				            position: { my: "right top+30", at: "right top+30", of: window},
	            				            dragStart: function(event, ui) {
	            				                $('.acceptance-dialog').removeClass('fixed-dialog')
	            				            },
	            				            dragStop: function(event, ui) {
	            				                // $('.acceptance-dialog').addClass('fixed-dialog')
	            				            },
	            				            resizeStart: function( event, ui ) {
	            				                $(this).dialog("option", "maxHeight", false);
	            				            }
	            				        });

	            				        return;
                         	}
                         	if(dem == n_lenght_for){
                         		var update_funcall = $http({
                                     url : url,
                                     method : 'POST',
                                     data : {name_file:name_file,readfile:true,tongkey:tongkey,app_sel:false}
                                 })
                                 .success(function(update_funcall, status, headers, config) {
                                 	  $(".loadgif").hide();
                                 	   if(successall){
		           			            		 if ($("#dialog-errors").hasClass('ui-dialog-content')) {
		           			                         $("#dialog-errors").dialog('destroy');
		           			                     }

		           			                     $scope.error_messages = "<div class='success-message'>"+update_funcall.messages+"</div>";
		           			                     $scope.gridApi.rowEdit.setRowsClean(update_datas);

			           			                 for (i = 0; i < update_datas.length; i++) {
			           			                    if (update_datas[i]) {
			           			                        update_datas[i].order_acceptance_status = is_approve_action ? order_acceptance_status_val.approved : order_acceptance_status_val.unapproved;
//#6946:Start
		           			                        update_datas[i].is_editing  = false;
//#6946:End
			           			                        $scope.setErrorStatus(update_datas[i], false);
			           			                    }
//#8088:Start
			           			                    update_datas[i].jcode_branch = update_datas[i].jcode+update_datas[i].branch_cd;
//#8088:End

			           			                 }

			           			                 if (delete_flag) {
			           			                    for (i = 0; i < delete_datas.length; i++) {
			           			                        if (delete_datas[i]) {
			           			                            rowIndex = $scope.gridApi.grid.appScope.gridOptions.data.indexOf(delete_datas[i]);
			           			                            $scope.gridOptions.data.splice(rowIndex, 1);
			           			                            $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
			           			                        }
			           			                    }
			           			                 }
		           			            	}
                                 });
                         	}
                         });


         				}
         				if(!status){
         					 $(".loadgif").hide();
         				}

//
// 			       }


         	return;
     	 }
     	else{

     		    var update_func = $http({
	                      url : url,
	                      method : 'POST',
	                      data : {order_acceptance: update_datas,app_sel:true}
	                  })
	                  .success(function(update_result, status, headers, config) {
	                	  $(".loadgif").hide();
	                	  if(is_approve_action){
	                		    status = update_result['status'];
	                		   if(status){
	                			  $scope.error_messages = "<div class='success-message'>"+update_result.messages+"</div>";
    			                     $scope.gridApi.rowEdit.setRowsClean(update_datas);

        			                 for (i = 0; i < update_datas.length; i++) {
        			                    if (update_datas[i]) {
        			                        update_datas[i].order_acceptance_status = is_approve_action ? order_acceptance_status_val.approved : order_acceptance_status_val.unapproved;
//#6946:Start
        			                        update_datas[i].is_editing  = false;
//#6946:End
        			                        $scope.setErrorStatus(update_datas[i], false);
        			                    }
        			                 }
    			            	}
	                		   else{
//#6946:Start
	                			        var error ="";
//	                			        console.log(update_result.messages);
	                			        if( typeof update_result.messages === 'string' ){
	                			        	 error += "<div><p class='import-error validation-error'><span class='row-error-icon'>" + update_result.messages + "</span></p>";

	                			        }
	                			        else{
	                			        	 angular.forEach(update_result['messages'], function(rows, key) {
	 		           		                    if (rows) {
	 		           		                        row_num = parseInt(key) + 1;

	 		           		                        error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
	 		           		                        angular.forEach(rows, function(message, key) {
	 		           		                            error += "<p class='import-error'>" + message + "</p>";
	 		           		                        });
	 		           		                    }
	 		           		                });
	                			        }

//#6946:End
		           					    error += "</div>";
		           					    $("#dialog-errors").html(error);
		           				        $("#dialog-errors").dialog({
		           				            title: '確認',
		           				            height: 'auto',
		           				            minWidth: 500,
		           				            minHeight: 250,
		           				            maxHeight: 250,
		           				            resizable: true,
		           				            modal: false,
		           				            dialogClass: 'acceptance-dialog fixed-dialog',
		           				            position: { my: "right top+30", at: "right top+30", of: window},
		           				            dragStart: function(event, ui) {
		           				                $('.acceptance-dialog').removeClass('fixed-dialog')
		           				            },
		           				            dragStop: function(event, ui) {
		           				                // $('.acceptance-dialog').addClass('fixed-dialog')
		           				            },
		           				            resizeStart: function( event, ui ) {
		           				                $(this).dialog("option", "maxHeight", false);
		           				            }
		           				        });

	                		   }

	                	  }
	                  });

     	}
    };

//#6946:End

    $scope.save = function(is_approve_action) {
//#7846:Start
        var datas        = null,
            insert_datas = null,
            update_datas = null,
            delete_datas = null,
            data_refesh  = [],
            insert_flag  = false,
            update_flag  = false,
            delete_flag  = false,
            flag,
            has_error    = null,
            array_errors = [],
            error        = null,
            row_num      = null,
            rowIndex     = null;
            ;
//#7846:End
        $scope.error_messages = '';
        datas = $scope.gridApi.grid.appScope.gridOptions.data;

        insert_datas = [];
        update_datas = [];
        delete_datas = [];
//#7846:Start
//#7990:Start
//        data_refesh  = [];
//#7990:End
//        insert_flag = false;
//        update_flag = false;
//        delete_flag = false;
//#7846:End
//#6946:Start
        var k = 0;
//#6946:End

        // Filter action and get data on row follow action (add, update, delete)

//#7990:Start
//#6946:Start
        for( var i = 0; i < datas.length; i++) {
//#8088:Start
        	if((datas[i].jcode_branch != "" && datas[i].jcode_branch != null ) && (datas[i].is_difference_common == true)){
        		datas[i].returnStatusReturn = 1;
        	}
        	else{
//#8390:Start

        		if(datas[i].is_editing){
            		datas[i].returnStatusReturn = 1;
        		}
        		else{
        			datas[i].returnStatusReturn = 0;
        		}
        		
//#8390:End

        	}
//#8088:End

        	if((datas[i].is_change) &&(!datas[i].is_editing)){

        			data_refesh[i] =  datas[i];
        	}

//#6946:End
//#7990:End

            if (is_approve_action) {
//                datas[i].is_change_acceptance_status = datas[i].is_approved ? true : false;

                if (datas[i].is_approved && datas[i].is_new) {
                    insert_datas[i] = datas[i];
                    insert_flag = true;
                }
                if (datas[i].is_approved && !datas[i].is_new) {
//#6946:Start
                    update_datas[k] = datas[i];
//#7126:Start
                    update_datas[k].updating_row = i;
//#7126:End
                    k++;
//#6946:End
                    update_flag = true;
                }
            } else {
                datas[i].is_change_acceptance_status = false;

                // Add new row
                if (datas[i].is_editing && datas[i].is_new && !datas[i].is_deleting) {
                    insert_datas[i] = datas[i];
                    insert_flag = true;
                }
                // Update
//#7990:Start
//                if (datas[i].is_editing && !datas[i].is_new) {
                if ((datas[i].is_editing == true) && (!datas[i].is_new)) {
//#7990:End
//#6946:Start
                    update_datas[k] = datas[i];
//#7126:Start
                    update_datas[k].updating_row = i;
//#7126:End
                    k++;
//#6946:End
                    update_flag = true;
                }
                // Delete
                if (datas[i].is_deleting) {
                    delete_datas[i] = datas[i];
                    delete_flag = true;
                }
            }

        }

//#7990:Start
        backupdate(data_refesh);
//#7990:End

        // If notthing change when save
        if ((!insert_flag && !update_flag) && !delete_flag) {
            $("#dialog-messages").html(label.nothing_change);
            if (is_approve_action) $("#dialog-messages").html(label.please_select_approve);
            $("#dialog-messages").dialog({
                title: '注意',
                width: 'auto',
                height: 147,
                modal: true,
                dialogClass: "acceptance-dialog",
                buttons: [{
                    text: "OK",
                    click: function() {
                    $(this).dialog("close");
                    }
                }],
            });
            return;
        }
        var update_func = {"data":{"status":true,"messages":label.update_success}};
        if (update_flag) {
//#6946:Start

        	 		 just_save = true;

//#6946:End
        	          $(".loadgif").show();
                       var url = is_approve_action ? "/_admin/order_acceptance/approve" : "/_admin/order_acceptance/update";
//#7126:Start
            			/*
            			 * dividng many item to upload
            			 * One refresh 100 record
            			 */
//            			var n_date_update = update_datas.length;
//            			if(n_date_update > 400){
//            				        var status = true;
//			            			var recode = 200;
//			            			var n_lenght_for = n_date_update /recode;
//			            			var n_du = n_date_update % recode;
//			            			if(n_du > 0 ){
//			            				n_lenght_for = parseInt(n_lenght_for) + 1;
//			            			}
//
//			            			var today = new Date();
//			            			today = today.getDate()+"_"+today.getMonth()+"_"+today.getFullYear()+"_"+today.getHours()+"_"+today.getMinutes()+"_"+today.getSeconds();
//			            			var name_file ="result_approvel_"+today+'.json';
//			            			//console.log(name_file);
//			            			//return;
//			            			var tongkey = 0;
//			            			var dem = 0;
////			            			n_lenght_for = 3;
//			            			var successall = true;
//			            			for(var i = 0 ; i < n_lenght_for ; i++){
//			            				$status = true;
//			            				var j_start = parseInt(i)*recode + 1;
//			            				if(i == (n_lenght_for -1) ){
//			            					var  n_j = n_date_update;
//			            				}
//			            				else{
//			            					var  n_j = parseInt(j_start) + recode;
//			            				}
//
//			            				var databach = new Array();
//			//                			console.log(j_start);
//			//                			console.log(n_j);
//
//			            				for(var j  = j_start  ; j < n_j ;j++){
//			//            					console.log(update_datas[j]);
//			            				   if(update_datas[j] != undefined){
//			            						databach.push(update_datas[j]);
//			            					}
//
//			            				}
//			            				var update_func = $http({
//			                                url : url,
//			                                method : 'POST',
//			                                data : {order_acceptance: databach,name_file:name_file,readfile:false,key:i,app_sel:false}
//			                            })
//			                           .success(function(update_func, status, headers, config) {
//			                                status = update_func['status'];
//
////			                            	console.log(mess_err);
//
//			                                tongkey = tongkey +1;
//			                                if(status){
//			                                	dem ++;
//			                                }
//
//			                            	if(!status){
//				            					    successall = false;
//				            					    $(".loadgif").hide();
////
////				            					    console.log('update func: ' + mess_err);
////				            					    var mess_err = update_func['messages'];
//				            					    var error ="";
//				            					    angular.forEach(update_func.messages, function(rows, key) {
//				            		                    if (rows) {
//				            		                        row_num = parseInt(key) + 1 + j_start;
//
//				            		                        error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
//				            		                        angular.forEach(rows, function(message, key) {
//				            		                            error += "<p class='import-error'>" + message + "</p>";
//				            		                        });
//				            		                    }
//				            		                });
//				            					    error += "</div>";
//				            					    $("#dialog-errors").html(error);
//				            				        $("#dialog-errors").dialog({
//				            				            title: '確認',
//				            				            height: 'auto',
//				            				            minWidth: 500,
//				            				            minHeight: 250,
//				            				            maxHeight: 250,
//				            				            resizable: true,
//				            				            modal: false,
//				            				            dialogClass: 'acceptance-dialog fixed-dialog',
//				            				            position: { my: "right top+30", at: "right top+30", of: window},
//				            				            dragStart: function(event, ui) {
//				            				                $('.acceptance-dialog').removeClass('fixed-dialog')
//				            				            },
//				            				            dragStop: function(event, ui) {
//				            				                // $('.acceptance-dialog').addClass('fixed-dialog')
//				            				            },
//				            				            resizeStart: function( event, ui ) {
//				            				                $(this).dialog("option", "maxHeight", false);
//				            				            }
//				            				        });
//
//				            				        return;
//			                            	}
//			                            	if(dem == n_lenght_for){
//			                            		var update_funcall = $http({
//			                                        url : url,
//			                                        method : 'POST',
//			                                        data : {name_file:name_file,readfile:true,tongkey:tongkey,app_sel:false}
//			                                    })
//			                                    .success(function(update_funcall, status, headers, config) {
//			                                    	  $(".loadgif").hide();
//			                                    	   if(successall){
//					           			            		 if ($("#dialog-errors").hasClass('ui-dialog-content')) {
//					           			                         $("#dialog-errors").dialog('destroy');
//					           			                     }
//
//					           			                     $scope.error_messages = "<div class='success-message'>"+update_funcall.messages+"</div>";
//					           			                     $scope.gridApi.rowEdit.setRowsClean(update_datas);
//
//						           			                 for (i = 0; i < update_datas.length; i++) {
//						           			                    if (update_datas[i]) {
////#6946:Start
//						           			                        update_datas[i].order_acceptance_status = is_approve_action ? order_acceptance_status_val.approved : order_acceptance_status_val.unapproved;
//						           			                    	update_datas[i].is_editing  = false;
////#6946:End
//						           			                        $scope.setErrorStatus(update_datas[i], false);
//						           			                    }
//						           			                 }
//
//						           			                 if (delete_flag) {
//						           			                    for (i = 0; i < delete_datas.length; i++) {
//						           			                        if (delete_datas[i]) {
//						           			                            rowIndex = $scope.gridApi.grid.appScope.gridOptions.data.indexOf(delete_datas[i]);
//						           			                            $scope.gridOptions.data.splice(rowIndex, 1);
//						           			                            $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
//						           			                        }
//						           			                    }
//						           			                 }
//					           			            	}
//			                                    });
//			                            	}
//			                            });

//			            				if(i==2){
//			            					break;
//			            				}
//
//
//			            				}
//			//            				console.log(databach);
//			            				if(!status){
//			            					 $(".loadgif").hide();
//			            					//break;
//			            				}
//
//	//
//	            			       }
//
//
//			            	return;
//		            	 }
//		            	else{

//#7790:Start
		            		    var update_func = $http({
				                      url : url,
				                      method : 'POST',
				                      data : {order_acceptance: update_datas,app_sel:true}
				                  })
				                  .success(function(update_result, status, headers, config) {



//#8088:Start
				                	  if(update_result['status']){
				                		    for (i = 0; i < update_datas.length; i++) {
		           			                    if (update_datas[i]) {
		           			                    	if(update_datas[i].returnStatusReturn == 1){
		           			                    		update_datas[i].order_acceptance_status = order_acceptance_status_val.unapproved;
			           			                       	update_datas[i].is_editing  = false;
			           			                    	$scope.setErrorStatus(update_datas[i], false);
		           			                    	}
		           			                    	update_datas[i].jcode_branch = update_datas[i].j_code+update_datas[i].branch_cd;
		           			                    	update_datas[i].j_code_has_oldVal =  update_datas[i].j_code;
		           			                    	update_datas[i].branch_cd_has_oldVal =  update_datas[i].branch_cd;
		           			                    }
		           			                 }
				                	  }
//#8088:End


				                	  if(update_result['status']){
				                		  for( var i = 0; i < update_datas.length; i++) {
							                	var  $colName2 =['j_code','branch_cd','account_id','arrange_rep','source_subcontractor_id',
							          			           'work_subcontractor_id',
							          			           'shipping_subcontractor_id',
							          			           'shipping_type_cd','set_name',
							          			           'unit_price',
							          			           'quantity',
							          			           'delivery_date',
							          			           'fee',
							          			           'weight',
							          			           'shipments_shape_cd',
							          			           'tax_free',
							          			           'shipping_charge_tax',
							          			           'stamp_tax',
							          			           'etc_tax',
							          			           'shipping_charge',
							          			           'etc','amount',
							          			           'account_id',
							                                'client_name',
							                                'summary_month',
							                                'amount'
							          			           ];

							          			$colName2.forEach(function($rowcolName) {

							          					if(update_datas[i][$rowcolName +'_has_oldVal'] != undefined){
							          						$rowcolName = $rowcolName.trim();
							          						update_datas[i][$rowcolName +'_has_oldVal'] = update_datas[i][$rowcolName];
							          						update_datas[i]['is_error_'+$rowcolName] = false;
							          					}


							          			});
							          			update_datas[i].is_error_j_code_branch = false;
							          			update_datas[i].is_editing = false;
					                	  }
				                	  }


				                	$(".loadgif").hide();

				                  });

//#7790:End
//		            	}
            			/*
            			 * end dividng many item to upload
            			 *
            			 */
//            			return;

//            				var update_func = $http({
//                                url : url,
//                                method : 'POST',
//                                data : {order_acceptance: update_datas}
//                            })
//                            .success(function(update_result, status, headers, config) {
//                            });
            			// name_file ="result_approvel_17_8_2015_14_32_47.json";

        }

       // return;
        var insert_func = {"data":{"status":true,"messages":label.update_success}};

        if (insert_flag) {

//#7290:Start
        	$('.both-blind').show();
//#7290:End
            var insert_func = $http({
                                url : "/_admin/order_acceptance/insert",
                                method : 'POST',
                                data : {order_acceptance: insert_datas}
                            })
                            .success(function(insert_result, status, headers, config) {
//#8081:Start
//                            	console.log(insert_result['status']);
                            	if(insert_result['status']){
                            		$rootScope.search();
                            	}
//#7290:Start
                            	$('.both-blind').hide();
//#7968:Start
//                            	$rootScope.search();
//#7968:End
//#7290:End
//#8081:End

                            })
        }

        $q.all(
            [
               update_func,
               insert_func
            ]
        ).then(function(result) {
            update_result = result[0].data;
            insert_result = result[1].data;
            flag      = false;
            has_error = false;
//#7158:Start
//            error = "<div class='error-message'>";
            array_errors = [];
//#7158:End
            if (update_result.status) {
                $scope.gridApi.rowEdit.setRowsClean(update_datas);
//#7183:Start
                list_row_update_error = [];
//#7183:End

                for (i = 0; i < update_datas.length; i++) {
                    if (update_datas[i]) {
//#6946:Start
//                        update_datas[i].order_acceptance_status = is_approve_action ? order_acceptance_status_val.approved : order_acceptance_status_val.unapproved;
                    	  update_datas[i].is_editing  = false;
//#6946:End
                        $scope.setErrorStatus(update_datas[i], false);
                    }
                }
//#7183:Start
// Only remove row after insert/update to ensure data index wont be mess up
//                if (delete_flag) {
//                    for (i = 0; i < delete_datas.length; i++) {
//                        if (delete_datas[i]) {
//                            rowIndex = $scope.gridApi.grid.appScope.gridOptions.data.indexOf(delete_datas[i]);
//                            $scope.gridOptions.data.splice(rowIndex, 1);
//                            $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
//                        }
//                    }
//                }
//#7183:End

            } else {
                has_error = true;

                angular.forEach(update_result.messages, function(rows, key) {
                    if (rows) {
//#7158:Start
                        error = '';
//#7158:End
                        row_num = parseInt(key) + 1;

                        if (!flag) {
                            flag = true;
                            error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
                        } else {
                            error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
                        }

                        angular.forEach(rows, function(message, key) {
//#7158:Start
//                            error += "<p class='import-error'>" + message + "</p>";
                            if (message != '') {
                                error += "<p class='import-error'>" + message + "</p>";
                            }
                        });

                        array_errors[row_num] = error;
//#7158:End
                        // if save error set whatever column error status to true, then row turns red
//#7183:Start
//                        datas[key].is_error_j_code_branch = true;
                        if (list_row_update_error.indexOf(parseInt(key)) == -1) {
                            list_row_update_error.push(parseInt(key));
                            datas[key].is_error_j_code_branch = true;
                        }
//#7183:End
                    }
                });

//#7183:Start
                angular.forEach(list_row_update_error, function(val, index) {
                    if (update_result.messages.hasOwnProperty(parseInt(val)) == false) {
                        datas[val].is_error_j_code_branch = false;
                        list_row_update_error.splice(index, 1);
                    }
                });
//#7183:End
            }

            // Result insert action
            if (insert_result.status) {
                $scope.gridApi.rowEdit.setRowsClean(insert_datas);
//#7183:Start
                list_row_insert_error = [];
//#7183:End

                for (i = 0; i < insert_datas.length; i++) {
                    if (insert_datas[i]) {
                        insert_datas[i].order_acceptance_status = is_approve_action ? order_acceptance_status_val.approved : order_acceptance_status_val.unapproved;
                        insert_datas[i].id = insert_result.data_id[i];
//#7183:Start
                        insert_datas[i].is_editing  = false;
                        insert_datas[i].is_separate = false;
//#7183:End
                        $scope.setErrorStatus(insert_datas[i], false);
                    }
                }
            } else {
                has_error = true;
//#7183:Start
                var total_will_be_deleted;
//#7183:End
                angular.forEach(insert_result.messages, function(rows, key) {
                    if (rows) {
                        row_num = parseInt(key) + 1;
//#7183:Start

//#7158:Start
                        error = '';
//#7158:End
                        total_will_be_deleted = 0;
                        if (delete_flag) {
                            angular.forEach(delete_datas, function(val, index) {
                                if (val.is_new && index < key) {
                                    total_will_be_deleted++;
                                }
                            });
                        } else if (delete_flag && !update_result.status) {
                            angular.forEach(delete_datas, function(val, index) {
                                if (!val.is_new && index < key) {
                                    total_will_be_deleted++;
                                }
                            });
                        }

                        row_num -= total_will_be_deleted;
//#7183:End

                        if (!flag) {
                            flag = true;
                            error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
                        } else {
                            error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
                        }

                        angular.forEach(rows, function(message, key) {
//#7158:Start
//                            error += "<p class='import-error'>" + message + "</p>";
                            if (message != '') {
                                error += "<p class='import-error'>" + message + "</p>";
                            }
                        });
                        array_errors[row_num] = error;
//#7158:End
                        // if save error set whatever column error status to true, then row turns red
//#7183:Start
//                        datas[key].is_error_j_code_branch = true;
                        if (list_row_insert_error.indexOf(parseInt(key)) == -1) {
                            list_row_insert_error.push(parseInt(key));
                            datas[key].is_error_j_code_branch = true;
                        }
//#7183:End
                    }
                });
//#7183:Start
                angular.forEach(list_row_insert_error, function(val, index) {
                    if (insert_result.messages.hasOwnProperty(parseInt(val)) == false) {
                        datas[val].is_error_j_code_branch = false;
                        list_row_insert_error.splice(index, 1);
                    }
                });
//#7183:End
            }
//#7158:Start
            // set all error row to be red once more time
            angular.forEach(list_row_insert_error, function(val, index) {
                datas[val].is_error_j_code_branch = true;
            });
            angular.forEach(list_row_update_error, function(val, index) {
//#7990:Start
            	if(datas[val].is_editing == true){
                    datas[val].is_error_j_code_branch = true;
            	}
//#7990:End
            	
            });
//#7158:End
//#7183:Start
            if (update_result.status) {
                if (delete_flag) {
                    for (i = 0; i < delete_datas.length; i++) {
                        if (delete_datas[i]) {
                            rowIndex = $scope.gridApi.grid.appScope.gridOptions.data.indexOf(delete_datas[i]);
                            $scope.gridOptions.data.splice(rowIndex, 1);
                            $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                        }
                    }
                }
            }
//#7183:End

            if (has_error) {
//#7158:Start
//                error += "</div>";
                error = "<div class='error-message'>" + array_errors.join("") + "</div>";
//#7158:End
                $("#dialog-errors").html(error);
                $("#dialog-errors").dialog({
                    title: '確認',
                    height: 'auto',
                    minWidth: 500,
                    minHeight: 250,
                    maxHeight: 250,
                    resizable: true,
                    modal: false,
                    dialogClass: 'acceptance-dialog fixed-dialog',
                    position: { my: "right top+30", at: "right top+30", of: window},
                    dragStart: function(event, ui) {
                        $('.acceptance-dialog').removeClass('fixed-dialog')
                    },
                    dragStop: function(event, ui) {
                        // $('.acceptance-dialog').addClass('fixed-dialog')
                    },
                    resizeStart: function( event, ui ) {
                        $(this).dialog("option", "maxHeight", false);
                    }
                })
            } else {
                if ($("#dialog-errors").hasClass('ui-dialog-content')) {
                    $("#dialog-errors").dialog('destroy');
                }

                $scope.error_messages = "<div class='success-message'>"+update_result.messages+"</div>";
            }
        });

    };

    $scope.setErrorStatus = function(data, status) {
        if (!status) status = false;

        data.is_error_j_code              = status;
        data.is_error_branch_cd           = status;
        data.is_error_unit_price          = status;
        data.is_error_quantity            = status;
        data.is_error_delivery_date       = status;
        data.is_error_fee                 = status;
        data.is_error_weight              = status;
        data.is_error_summary_month       = status;
        data.is_error_tax_free            = status;
        data.is_error_shipping_charge_tax = status;
        data.is_error_stamp_tax           = status;
        data.is_error_etc_tax             = status;
        data.is_error_shipping_charge     = status;
        data.is_error_etc                 = status;
        data.is_error_j_code_branch       = status;
        data.is_new                       = status;
    }

    $scope.insertRow = function(row) {
        var rowIndex = $scope.gridApi.grid.appScope.gridOptions.data.indexOf(row) + 1;
//#7846:Start
//        account_options                = angular.copy(row.account_options);
//#7167:Start
//        arrange_rep_options            = angular.copy(row.arrange_rep_options);
//#7167:End
//        direct_subcontractor_options   = angular.copy(row.direct_subcontractor_options);
//        indirect_subcontractor_options = angular.copy(row.indirect_subcontractor_options);
//        shipping_type_options          = angular.copy(row.shipping_type_options);
//        shipments_shape_options        = angular.copy(row.shipments_shape_options);
//        consumption_tax                = angular.copy(row.consumption_tax);
//        can_add                        = angular.copy(row.can_add);
//        can_approve                    = angular.copy(row.can_approve);
//        can_separate                   = angular.copy(row.can_separate);
//        can_edit                       = angular.copy(row.can_edit);
//        can_delete                     = angular.copy(row.can_delete);

        var newData = null;
//#7846:End
        newData = {
            order_acceptance_status        : order_acceptance_status_val.unapproved,
            id                             : null,
            order_id                       : null,
            j_code                         : null,
            branch_cd                      : null,
            account_id                     : null,
            arrange_rep                    : null,
            client_name                    : null,
            source_subcontractor_id        : null,
            work_subcontractor_id          : null,
            shipping_subcontractor_id      : null,
            shipping_type_cd               : null,
            set_name                       : null,
            unit_price                     : null,
            quantity                       : null,
            delivery_date                  : null,
            fee                            : null,
            weight                         : null,
            shipments_shape_cd             : null,
            summary_month                  : null,
            tax_free                       : null,
            shipping_charge_tax            : null,
            stamp_tax                      : null,
            etc_tax                        : null,
            shipping_charge                : null,
            etc                            : null,
            amount                         : null,
//#7846:Start
//            account_options                : account_options,
//            direct_subcontractor_options   : direct_subcontractor_options,
//            indirect_subcontractor_options : indirect_subcontractor_options,
//            shipping_type_options          : shipping_type_options,
//            shipments_shape_options        : shipments_shape_options,
//            can_add                        : can_add,
//            can_edit                       : can_edit,
//            can_approve                    : can_approve,
//            can_separate                   : can_separate,
//            can_delete                     : can_delete,
//            consumption_tax                : consumption_tax,
            can_add                        : angular.copy(row.can_add),
            can_edit                       : angular.copy(row.can_edit),
            can_approve                    : angular.copy(row.can_approve),
            can_separate                   : angular.copy(row.can_separate),
            can_delete                     : angular.copy(row.can_delete),
//#7846:End
            is_new                         : true,
//#6946:Start
//            is_editing                     : true,
//#7103:Start
           is_editing                     : true,
//#7103:End
            is_deleting                    : false,
//#6946:End
//#7846:Start
//#7167:Start
//            arrange_rep_options            : arrange_rep_options,
//#7167:End
//#7846:End
//#7990:Start
            is_change					   : false,
//#7990:End

        };


        $scope.gridOptions.data.splice(rowIndex, 0, newData);

        // $scope.gridApi.grid.registerDataChangeCallback(function(data) {
        //   $scope.gridApi.core.scrollTo($scope.gridOptions.data[rowIndex], $scope.gridOptions.columnDefs[0]);
        // }, [uiGridConstants.dataChange.ROW]);

        if($scope.gridOptions.data.length == (rowIndex+1)){
            $(".ui-grid-viewport").animate({ scrollTop: $(".ui-grid-viewport")[0].scrollHeight}, 'slow');
        }
        else{
            $scope.gridApi.core.scrollTo($scope.gridOptions.data[rowIndex+1],$scope.gridOptions.columnDefs[0]);
        }
        // a = $(".ui-grid-viewport")[0].scrollHeight + 30;
        // console.log(a);
        // $(".ui-grid-viewport").scrollTop(a + 30);


        $("html, body").animate({
             scrollTop:postion_return
             },"");
        return true;
    };
    $(function(){
          $(window).scroll(function(){
            var aTop = $('.ui-grid-contents-wrapper').height();
            postion_return = $(this).scrollTop();

          });
    });
    $scope.separateRow = function(row) {

        var rowIndex = $scope.gridApi.grid.appScope.gridOptions.data.indexOf(row) + 1;
//#7846:Start
        var newData = null;
//#7846:End
        newData                         = angular.copy(row);
        newData.order_acceptance_status = order_acceptance_status_val.unapproved;
        newData.id                      = null;
        newData.is_new                  = true;
//#6946:Start
//        newData.is_editing              = true;
//#7103:Start
       newData.is_editing              = true;
//#7103:End
//#6946:End
        newData.is_separate             = true;
        newData.is_deleting             = false;
        newData.shipping_charge_tax     = null;
        newData.tax_free                = null;
        newData.stamp_tax               = null;
        newData.etc_tax                 = null;
        newData.shipping_charge         = null;
        newData.etc                     = null;
        newData.amount                  = null;

//#7990:Start
        newData.new_is_separate			= true;
        newData.is_change				= false;
//#7990:End

//#7766:Start
        if (newData.j_code && newData.branch_cd && newData.order_id) {
            newData.separate_has_j_code_branch_cd = true;
        } else {
            newData.separate_has_j_code_branch_cd = false;
        }
//#7766:End

        $scope.gridOptions.data.splice(rowIndex, 0, newData);
    };

    $scope.show_client = function(row) {
        if (!row.entity.client_id) return;

//#7461:Start
//        $.ajax({
//            url: '/_admin/client/detail',
//            type: 'POST',
//            data: {id: row.entity.client_id, dialog_selector: '#dialog-show-client'},
//        })
//        .done(function(data) {
//            $("#dialog-show-client").html(data);
//            $("#dialog-show-client").dialog({
//                title: lang['text_title_show_client'],
//                width: 'auto',
//                height: 'auto',
//                modal: true,
//            });
//        })
//        .fail(function() {
//        	form_changed = false;
//            if (typeof cl_functions_loaded !== 'undefined' && cl_functions_loaded) {
//                unbind_save_key();
//            }
//        })
//        .always(function() {
//            console.log("complete");
//        });
        $.post(
                '/_admin/client/detail',
                {id :row.entity.client_id, dialog_selector: '#clientDialog'},
                function(data) {
                	clientDialog.html(data);
                	clientDialog.dialog("option", "position", {my: "center", at: "center", of: window});
                	clientDialog.dialog('open');
         });
//#7461:End

    }

    $scope.get_order_detail = function(j_code, branch_cd, callback) {
        $http({
            url : "/_admin/order_acceptance/ajax_get_order_detail",
            method : 'POST',
            data : {j_code: j_code, branch_cd: branch_cd}
        })
        .success(function(data, status, headers, config) {
            callback(data);
        });
    }

    $scope.roundNumber = function(number, length) {
        number = parseFloat(number);
        return Math.round(number * Math.pow(10, length)) / Math.pow(10, length);
    };

//#7846:Start
    var fetchData = function() {
        if ($("#search_result").text()) {
            var postFormOrder = $("#postFormOrder").val();
            var data = null;
            data = $.parseJSON($("#search_result").text());

    //#7087:Start
            if (typeof data.is_over_record != 'undefined') {
            	 $scope.is_over_record = true;
            	 $scope.showWatermark = false;
            	 $scope.gridOptions.data = [];
            	 return;
            }

    //#7087:End

            var lengdata = data.length;
            for(var i = 0; i < data.length;i ++) {

    //#7846:Start
//               data[i].account_options                = glb_account_options;
//               data[i].direct_subcontractor_options   = glb_direct_subcontractor_options;
//               data[i].indirect_subcontractor_options = glb_indirect_subcontractor_options;
//               data[i].shipping_type_options          = glb_shipping_type_options;
//               data[i].shipments_shape_options        = glb_shipments_shape_options;
//               data[i].consumption_tax                = glb_consumption_tax;
    //#7167:Start
    //            data[i].arrange_rep_options            = glb_arrange_rep_options;
    //#7167:End
    //#7846:End

                 /* #6784 - Add -S */
                data[i].is_new							= false;
                data[i].is_editing					    = false;
                 /* #6784 - Add -E*/

                data[i].is_change						= false;

            }

            $scope.gridOptions.data = data;
            if(lengdata == 0 && postFormOrder == 1 && post_is_acceptance_input == 0){
                var jcodeidPOst = $('#jcodeidPOst').val();

                var brandcode = $('#brandcode').val();
                var rowIndex = 0;
                var newData = null;
                newData = {
                    order_acceptance_status        : order_acceptance_status_val.unapproved,
                    id                             : null,
                    order_id                       : null,
                    j_code                         : jcodeidPOst,
                    branch_cd                      : brandcode,
                    account_id                     : null,
                    arrange_rep                    : null,
                    client_name                    : null,
                    source_subcontractor_id        : null,
                    work_subcontractor_id          : null,
                    shipping_subcontractor_id      : null,
                    shipping_type_cd               : null,
                    set_name                       : null,
                    unit_price                     : null,
                    quantity                       : null,
                    delivery_date                  : null,
                    fee                            : null,
                    weight                         : null,
                    shipments_shape_cd             : null,
                    summary_month                  : null,
                    tax_free                       : null,
                    shipping_charge_tax            : null,
                    stamp_tax                      : null,
                    etc_tax                        : null,
                    shipping_charge                : null,
                    etc                            : null,
                    amount                         : null,
    //#7846:Start
//                   account_options                : glb_account_options,
//                   direct_subcontractor_options   : glb_direct_subcontractor_options,
//                   indirect_subcontractor_options : glb_indirect_subcontractor_options,
//                   shipping_type_options          : glb_shipping_type_options,
//                   shipments_shape_options        : glb_shipments_shape_options,
                    can_add                        : true,
                    can_edit                       : true,
                    can_approve                    : true,
                    can_separate                   : true,
                    can_delete                     : true,
//                    consumption_tax                : glb_consumption_tax,
                    is_new                         : true,
    //#6946:Start
    //                is_editing                     : true,
    //#6946:End
    //#7167:Start
    //                arrange_rep_options            : glb_arrange_rep_options,
    //#7167:End
    //#7846:End
                    is_change					   : false,
                };

                var set_jcode_branch_cd_data = function(data) {
                    newData.is_error_j_code_branch = true;
                 // #6901 -Modify -S
                    if (data) {
                    	var is_change_report_apply = data['is_change_report_apply'];
                    	if(is_change_report_apply == 0){
                            var summary_month = null;
    	                    summary_month = data.delivery_date;
    	                    summary_month = summary_month.replace(/-/g, '/');

    	                    newData.account_id             = data.j_account_id;
    	                    newData.client_name            = data.client_name;
    	                    newData.client_id              = data.client_id;
    	                    newData.order_id               = data.id;
    	                    newData.summary_month          = summary_month.substring(0, 7);
    	                    newData.is_error_j_code_branch = false;
    	                    newData.is_change			   = false;
                    	}
                    }
                 // #6901 -Modify -E
                }

                $scope.get_order_detail(jcodeidPOst, brandcode, set_jcode_branch_cd_data);

                $scope.gridOptions.data.splice(rowIndex, 0, newData);

            }

            $scope.showWatermark = false;
    //#7087:Start
            $scope.is_over_record = false;
    //#7087:End

        } else {

            $scope.showWatermark = true;
    //#7087:Start
            $scope.is_over_record = false;
    //#7087:End
        }
        columnDefs = null;
    }

    fetchData();
//#7846:End

        //    if ($.isEmptyObject(data)) {
        //        $scope.showWatermark = true;
        //    }

    // trigger for CTRL + S to do save action
    $(document).bind('keydown', function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            $scope.save();
            return false;
        }
    });

    // warning if there are some data have changed before close tab
    $(window).bind('beforeunload', function() {
        if ($scope.gridApi.rowEdit.getDirtyRows().length){
            return label.confirm_close_windows;
        }
    });


}])
    .filter('mapJCode', function() {
        return function(input, row) {
//#7846:Start
            if (typeof row.entity == 'undefined') return;
//#7846:End
            if (input && input.toString().length > 10) {
                row.entity.is_error_j_code = true;
            } else {
                row.entity.is_error_j_code = false;
                // console.log(row);

            }

            return input;
        }
    })
    .filter('mapBranchCd', function() {
        return function(input, row) {
//#7846:Start
            if (typeof row.entity == 'undefined') return;
//#7846:End
            if (input && input.toString().length > 2) {
                row.entity.is_error_branch_cd = true;
            } else {
                row.entity.is_error_branch_cd = false;
            }

            if (input && input.toString().length == 1) return '0'+input;
            return input;
        }
    })
    .filter('mapAccountId', function() {
//#7846:Start        
//        return function(input, account_options) {
//            if (!input) return '';

//            for (i = 0; i < account_options.length; i++) {
//                if (account_options[i].id == input) {
//                    return account_options[i].name;
//                }
//            }
//        };

        return function(input) {
            if (!input) return '';

            for (i = 0; i < glb_account_options.length; i++) {
                if (glb_account_options[i].id == input) {
                    return glb_account_options[i].name;
                }
            }
        };
//#7846:End
    })
    .filter('mapArrangeRep', function() {
//#7846:Start
//        return function(input, arrange_rep_options) {
//            if (!input) return '';

//            for (i = 0; i < arrange_rep_options.length; i++) {
//                if (arrange_rep_options[i].id == input) {
//                    return arrange_rep_options[i].name;
//                }
//            }
//        };

        return function(input) {
            if (!input) return '';

            for (i = 0; i < glb_arrange_rep_options.length; i++) {
                if (glb_arrange_rep_options[i].id == input) {
                    return glb_arrange_rep_options[i].name;
                }
            }
        };
//#7846:End
    })
    .filter('mapClientName', function() {
        return function(input, row) {
           return input;
        }
    })
    .filter('mapSubcontractorId', function() {
//#7846:Start
//        return function(input, row) {
        return function(input) {
//            if (typeof row.entity == 'undefined') return;

            if (!input) return '';

//            for (i = 0; i < row.entity.direct_subcontractor_options.length; i++) {
//                if (row.entity.direct_subcontractor_options[i].id == input) {
//                    // row.entity.indirect_subcontractor_options = row.entity.subcontractor_options[i].children;
//                    return row.entity.direct_subcontractor_options[i].abbr_name;
//                }
//            }
            
            for (i = 0; i < glb_direct_subcontractor_options.length; i++) {
                if (glb_direct_subcontractor_options[i].id == input) {
                    return glb_direct_subcontractor_options[i].abbr_name;
                }
            }
//#7846:End
        }
    })
    .filter('mapIndirectSubcontractorId', function() {
//#7846:Start
//        return function(input, row) {
        return function(input) {
//            if (typeof row.entity == 'undefined') return;

            if (!input) return '';

//            if (row.entity.indirect_subcontractor_options) {
//                for (i = 0; i < row.entity.indirect_subcontractor_options.length; i++) {
//                    if (row.entity.indirect_subcontractor_options[i].id == input) {
//                        return row.entity.indirect_subcontractor_options[i].abbr_name;
//                    }
//                }
//            }

            for (i = 0; i < glb_indirect_subcontractor_options.length; i++) {
                if (glb_indirect_subcontractor_options[i].id == input) {
                    return glb_indirect_subcontractor_options[i].abbr_name;
                }
            }
//            return '';
            return input;
//#7846:End
        }
    })
    .filter('mapShippingTypeCd', function() {
//#7846:Start
//        return function(input, shipping_type_options) {
//            if (!input) return '';

//            for (i = 0; i < shipping_type_options.length; i++) {
//                if (shipping_type_options[i].code == input) {
//                    return shipping_type_options[i].name;
//                }
//            }
//        };

        return function(input) {
            if (!input) return '';

            for (i = 0; i < glb_shipping_type_options.length; i++) {
                if (glb_shipping_type_options[i].code == input) {
                    return glb_shipping_type_options[i].name;
                }
            }

            return input;
        };
//#7846:End
    })
    .filter('mapUnitPrice', function() {
        return function(input, row) {
//#7846:Start
            if (typeof row.entity == 'undefined') return;
            if (!input) return '';

//            if (isNaN(input)　|| contain_non_latin_characters()) {
            if (isNaN(input)) {
//#7846:End
                row.entity.is_error_unit_price = true;
                return input;
            }

            if (input % 1 == 0) {
//6946:Start
//                row.entity.is_error_unit_price = false;
            	var input_return = parseFloat(input);
            	row.entity.is_error_unit_price = !IsNumberLessThanTen(input_return);
//#7846:Start
//                if(!row.entity.is_error_unit_price){
//                 	 colCurrentName = "unit_price";
//                }
//#7846:End
                input_return =  parseFloat(input)+'.00';
                input_return = formatMoneyAll(input_return,2);
                return input_return;
//6946:End
            }

//6946:Start
//            row.entity.is_error_unit_price = false;
//6946:End

//6946:Start
            var input_return = Math.round(input * Math.pow(10, 2)) / Math.pow(10, 2);
//            console.log('bbbb'+input_return);
            row.entity.is_error_unit_price = !IsNumberLessThanTen(input_return);
//#7846:Start
//            if(!row.entity.is_error_unit_price){
//            	 colCurrentName = "unit_price";
//            }
//#7846:End
            input_return = formatMoneyAll(input_return,2);
            return input_return;
 //            return Math.round(input * Math.pow(10, 2)) / Math.pow(10, 2);

//6946:End

        }
    })
    .filter('mapQuantity', function() {
        return function(input, row) {
//#7846:Start
            if (typeof row.entity == 'undefined') return;
            if (!input) return '';

//            if (isNaN(input) || input < 0　|| contain_non_latin_characters()) {
            if (isNaN(input) || input < 0) {
//#7846:End
                row.entity.is_error_quantity = true;
                return input;
            }

            row.entity.is_error_quantity = false;

//6946:Start
//colCurrentName = "quantity";

            var input_return = parseFloat(input);
//#8258:Start
//			row.entity.is_error_quantity = !IsNumberLessThanSeven(input_return);
            row.entity.is_error_quantity = !IsNumberLessThanEight(input_return);
//#8258:Start
//#7846:Start
//            if(!row.entity.is_error_quantity){
//           	 colCurrentName = "quantity";
//           }
//#7846:End
            input_return = formatMoneyAll(input_return,0);

            return input_return;
//6946:End
        }
    })
    .filter('mapDeliveryDate', function() {
        return function(input, row) {
//#7846:Start
            if (typeof row.entity == 'undefined') return;
//#7846:End
            if (!input) {
                row.entity.delivery_date_for_save = '';
                return input;
            }

            if (input == '0000-00-00') {
                row.entity.delivery_date = '';
                row.entity.delivery_date_for_save = '';
                return '';
            }
            result = parse_date(input);
            if (!result) {
                row.entity.delivery_date_for_save = input;
                row.entity.is_error_delivery_date = true;
                return input;
            }
            row.entity.delivery_date_for_save = result;
            row.entity.is_error_delivery_date = false;
            return result;
        }
    })
    .filter('mapFee', function() {
        return function(input, row) {
//#7846:Start
            if (typeof row.entity == 'undefined') return;
            if (!input) return '';

//            if (isNaN(input) || input < 0　|| contain_non_latin_characters()) {
            if (isNaN(input) || input < 0) {
//#7846:End
                row.entity.is_error_fee = true;
                return input;
            }

            row.entity.is_error_fee = false;
            return input;
        }
    })
    .filter('mapWeight', function() {
        return function(input, row) {
//#7846:Start
            if (typeof row.entity == 'undefined') return;
            if (!input) return '';

//            if (isNaN(input) || input < 0　|| contain_non_latin_characters()) {
            if (isNaN(input) || input < 0) {
//#7846:End
                row.entity.is_error_weight = true;
                return input;
            }
//#7090:Start
            row.entity.is_error_weight = false;

            if (input.indexOf('.') != -1) {
                row.entity.weight = input.substr(0, input.indexOf('.') + 2);
                return row.entity.weight;
            }
            return input;
//#7090:End
        }
    })
    .filter('mapShippmentsShapeCd', function() {
//#7846:Start
        return function(input) {
            // if (typeof scope.row == 'undefined') return;
            if (!input) return '';

            for (i = 0; i < glb_shipments_shape_options.length; i++) {
                if (glb_shipments_shape_options[i].code == input) {
                    return glb_shipments_shape_options[i].name;
                }
            }
            // return scope.row.entity.shipments_shape_name;
            return input;
        }
//#7846:End
    })
    /*
    .filter('mapShippmentsShapeCd', function() {
        return function(input, shipments_shape_options) {
            if (!input) return '';

            for (i = 0; i < shipments_shape_options.length; i++) {
                if (shipments_shape_options[i].code == input) {
                    return shipments_shape_options[i].name;
                }
            }
        }
    })
    */
    .filter('mapSummaryMonth', function() {
        return function(input, row) {
//#7846:Start
            if (typeof row.entity == 'undefined') return;

            if (!input) return input;
            var result = null;
//#7846:End
            result = parse_summary_month(input);
            if (!result) {
                row.entity.summary_month_for_save = input;
                row.entity.is_error_summary_month = true;
                return input;
            }
            row.entity.summary_month_for_save = result;
            row.entity.is_error_summary_month = false;
            return result;
        };
    })
    .filter('mapTaxFree', function() {
        return function(input, row) {
//#7846:Start
            if (typeof row.entity == 'undefined') return;
        	if(input ==""){return '';}
        	if(input == 0){ return '0';}
            if (!input) return '';
//            if (isNaN(input)　|| contain_non_latin_characters()) {
            if (isNaN(input)) {
//#7846:End
                row.entity.is_error_tax_free = true;
                return input;
            }

            row.entity.is_error_tax_free = false;
            row.entity.tax_free = parseFloat(input);
//6946:Start

            var input_return = parseFloat(input);
            row.entity.is_error_tax_free = !IsNumberLessThanTen(input_return);
            input_return = formatMoneyAll(input_return,0);
            return input_return;
//6946:End

        };
    })
    .filter('mapShippingChargeTax', function() {
        return function(input, row) {
//#7846:Start
            if (typeof row.entity == 'undefined') return;
        	if(input ==""){return '';}
        	if(input == 0){ return '0';}
            if (!input) return '';

//            if (isNaN(input)　|| contain_non_latin_characters()) {
            if (isNaN(input)) {
//#7846:End
                row.entity.is_error_shipping_charge_tax = true;
                return input;
            }
            row.entity.is_error_shipping_charge_tax = false;
            row.entity.shipping_charge_tax = parseFloat(input);
//6946:Start

            var input_return = parseFloat(input);
            row.entity.is_error_shipping_charge_tax = !IsNumberLessThanTen(input_return);
            input_return = formatMoneyAll(input_return,0);
            return input_return;
//6946:End

        };
    })
    .filter('mapStampTax', function() {
        return function(input, row) {
//#7846:Start
            if (typeof row.entity == 'undefined') return;
        	if(input ==""){return '';}
        	if(input == 0){ return '0';}
        	if (!input) return '';

//            if (isNaN(input)　|| contain_non_latin_characters()) {
            if (isNaN(input)) {
//#7846:End
                row.entity.is_error_stamp_tax = true;
                return input;
            }


            row.entity.is_error_stamp_tax = false;
            row.entity.stamp_tax = parseFloat(input);
//6946:Start

            var input_return = parseFloat(input);
            row.entity.is_error_stamp_tax = !IsNumberLessThanTen(input_return);
            input_return = formatMoneyAll(input_return,0);
            return input_return;
//            return parseFloat(input);
//6946:End

        };
    })
    .filter('mapEtcTax', function() {
        return function(input, row) {
//#7846:Start
            if (typeof row.entity == 'undefined') return;
        	if(input ==""){return '';}
        	if(input == 0){ return '0';}
            if (!input) return '';

//            if (isNaN(input)　|| contain_non_latin_characters()) {
            if (isNaN(input)) {
//#7846:End
                row.entity.is_error_etc_tax = true;
                return input;
            }

            row.entity.is_error_etc_tax = false;
            row.entity.etc_tax = parseFloat(input);
//6946:Start

            var input_return = parseFloat(input);
            row.entity.is_error_etc_tax = !IsNumberLessThanTen(input_return);
            input_return = formatMoneyAll(input_return,0);
            return input_return;
//            return parseFloat(input);
//6946:End

        };
    })
    .filter('mapShippingCharge', function() {
        return function(input, row) {
//#7846:Start
            if (typeof row.entity == 'undefined') return;
        	if(input ==""){return '';}
        	if(input == 0){ return '0';}
            if (!input) return '';

//            if (isNaN(input)　|| contain_non_latin_characters()) {
            if (isNaN(input)) {
//#7846:End
                row.entity.is_error_shipping_charge = true;
                return input;
            }

            row.entity.is_error_shipping_charge = false;
            row.entity.shipping_charge = parseFloat(input);
//6946:Start

            var input_return = parseFloat(input);
            row.entity.is_error_shipping_charge = !IsNumberLessThanTen(input_return);
            input_return = formatMoneyAll(input_return,0);
            return input_return;
//            return parseFloat(input);
//6946:End
        };
    })
    .filter('mapEtc', function() {
        return function(input, row) {
//#7846:Start
            if (typeof row.entity == 'undefined') return;
        	if(input ==""){return '';}
        	if(input == 0){ return '0';}
            if (!input) return '';

//            if (isNaN(input)　|| contain_non_latin_characters()) {
            if (isNaN(input)) {
//#7846:End
                row.entity.is_error_etc = true;
                return input;
            }

            row.entity.is_error_etc = false;
            row.entity.etc = parseFloat(input);
//6946:Start

            var input_return = parseFloat(input);
            row.entity.is_error_etc = !IsNumberLessThanTen(input_return);
            input_return = formatMoneyAll(input_return,0);
            return input_return;
//            return parseFloat(input);
//6946:End
        };
    })
    .filter('mapOrderAcceptanceStatus', function() {
        return function(input) {

            var status = label.unapproved;
            if (input == order_acceptance_status_val.approved) status = label.approved;
            if (input == order_acceptance_status_val.account_tighten) status = label.account_tighten;
            return status;
        }
    })

    .filter("nl2br", function() {
        return function(data) {
        if (!data) return data;
            return data.replace(/\n\r?/g, '<br />');
        };
    })
    .filter('mapYesNo', function() {
        var mapHash = {
                0 : '未',
                1 : '済'
            };

        return function(input) {
            if (!input){
                return '-';
            } else {
                return mapHash[input];
            }
        };
    })
    .filter('mapTaxType', function() {
        return function(input) {
            if (!input) {
                return '';
            } else {
                for (i = 0; i < tax_division.length; i++) {
                    if (tax_division[i].code == input) {
                        return tax_division[i].name;
                    }
                }
            }
        };
    })
//#6838:Start
    .filter('mapAmount', function() {
        return function(input, row) {

            if (input === '' || input == null) return '';

            var input_return = parseFloat(input);
            input_return = formatMoneyAll(input_return);
            return input_return;
        };
    });
//#6838:End


   //#6849 :  DElete - S
//var parse_date = function(str) {

//    str = str.trim();
//    date = year = month = day = '';
//    if (!str) return false;
//
//    delimiter = '-';
//    if (str.indexOf('/') != -1) delimiter = '/';
//
//    parts = str.split(delimiter);
//
//    if (parts.length == 2) {
//        date_obj = new Date();
//
//        if (parts[0].trim().length == 4) {
//            year = parts[0].trim();
//            month = parts[1].trim();
//            day = date_obj.getDate().toString();
//            date = year + delimiter + month + delimiter + day;
//        } else {
//            year = date_obj.getFullYear();
//            month = parts[0].trim();
//            day = parts[1].trim();
//            date = year + delimiter + month + delimiter + day;
//        }
//
//
//    } else if (parts.length == 3) {
//        year = parts[0].trim();
//        if (year.length < 4) {
//            year = '2';
//            for (i = 0; i < 3 - parts[0].trim().length; i++) {
//                year += '0';
//            }
//            year += parts[0].trim();
//        }
//        month = parts[1].trim();
//        day = parts[2].trim();
//        date = year + delimiter + month + delimiter + day;
//    }
//
//    date_obj = new Date(date);
//    if (date_obj.getFullYear() == year && date_obj.getMonth() + 1 == month && date_obj.getDate() == day) {
//        return year + '/' + month.replace(/^\d$/,'0$&') + '/' + day.replace(/^\d$/,'0$&');
//    }
//    return false;
//}
 //#6849 :  DElete - E
 
var parse_summary_month = function(str) {
//#7158:Start
    if (typeof str == 'undefined' || str === null) return '';
//#7158:End
    str = str.trim();

//#7846:Start
    var year, month, delimiter, parts, i = null;
//#7846:End
    year = month = '';

    delimiter = '-';
    if (str.indexOf('/') != -1) delimiter = '/';

    parts = str.split(delimiter);
    if (parts.length == 2) {
        // 2015/12
        year = parts[0].trim();
        if (year.length < 4) {
            year = '2';
            for (i = 0; i < 3 - parts[0].trim().length; i++) {
                year += '0';
            }
            year += parts[0].trim();
        }

        month = parts[1].trim();
        if (month.length > 2 || month < 1 || month > 12) {
            return false;
        }
        month = month.replace(/^\d$/,'0$&');
        return year + '/' + month;
    }
//#7846:Start
//    date_obj = new Date();
//#7846:End
    switch(str.length) {
        case 6:
            year = str.substr(0, 4);
            month = str.substr(4,2);
            if (month < 1 || month > 12) {
                return false
            }
            month = month.replace(/^\d$/,'0$&');
            return year + '/' + month;

            break;

    }
}

//#7846:Start
//var contain_non_latin_characters = function(string) {
//    return /[^\u0000-\u00ff]/.test(string);
//}
//#7846:End

//6946:Start
function show_error_message(texterror){
	 var error = "<div class='error-message'>" +texterror + "</div>";
	  $("#dialog-errors").html(error);
	  $("#dialog-errors").dialog({
	      title: '確認',
	      height: 'auto',
	      minWidth: 500,
	      minHeight: 250,
	      maxHeight: 250,
	      resizable: true,
	      modal: false,
	      dialogClass: 'acceptance-dialog fixed-dialog',
	      position: { my: "right top+30", at: "right top+30", of: window},
	      dragStart: function(event, ui) {
	          $('.acceptance-dialog').removeClass('fixed-dialog')
	      },
	      dragStop: function(event, ui) {
	          // $('.acceptance-dialog').addClass('fixed-dialog')
	      },
	      resizeStart: function( event, ui ) {
	          $(this).dialog("option", "maxHeight", false);
	      }
	  });
}
//#7846:Start
//function IsNumberLessThanSeven(input_number){

//	   if(isNaN(input_number)){
//		   return true;
//	   }
//	   else{
//		   if (input_number <= -1000000 || input_number >= 1000000) {
//	           return false;
//	       }
//	       else{
//	       	  return true;
//	       }
//	   }

//}
//#7846:End

//#8258:Start
function IsNumberLessThanEight(input_number){

	   if(isNaN(input_number)){
		   return true;
	   }
	   else{
		   if (input_number <= -10000000 || input_number >= 10000000) {
	           return false;
	       }
	       else{
	       	  return true;
	       }
	   }

}
//#8258:End
function IsNumberLessThanTen(input_number){

	   if(isNaN(input_number)){
		   return true;
	   }
	   else{
		   if (input_number <= -1000000000 || input_number >= 1000000000) {
	           return false;
	       }
	       else{
	       	  return true;
	       }
	   }

}
function setAllValidateNumber(rowdata,field){

	var message_error = "";
	var valuefield = rowdata[field];

	if(field == "quantity"){
//#8258:Start
//		var checkNumberField  = IsNumberLessThanSeven(valuefield);
		var checkNumberField  = IsNumberLessThanEight(valuefield);
//#8258:End
		if(!checkNumberField){
			message_error = message_error + lang['error_'+field+'_max_length'] +"<br/>";
		}

   }
   else{
	   if((field == "unit_price")||( field == "tax_free")||( field == "shipping_charge_tax")||( field == "shipping_charge")||( field == "stamp_tax")||(field == "etc_tax")||(field =="etc")){

		   var checkNumberField  = IsNumberLessThanTen(valuefield);
			if(!checkNumberField){
				message_error = message_error + lang['error_'+field+'_max_length'] +"<br/>";
			}

	   }
	   else{
		   message_error ="";
	   }
  }


   if(message_error != ""){
		show_error_message(message_error);
	}
   else{
      try{
    	  $("#dialog-errors").dialog( "close" );
      }
      catch(err){
    	  $("#dialog-errors").html("");
      }

	}
}

//6946:End


//#7990:Start
function backupdate($databk,$rowone){

	$colName =['j_code','branch_cd','account_id',
	           'arrange_rep','source_subcontractor_id',
	           'work_subcontractor_id',
	           'shipping_subcontractor_id',
	           'shipping_type_cd','set_name',
	           'unit_price',
	           'quantity',
	           'delivery_date',
	           'fee',
	           'weight',
	           'shipments_shape_cd',
	           'tax_free',
	           'shipping_charge_tax',
	           'stamp_tax',
	           'etc_tax',
	           'shipping_charge',
	           'etc','amount',
	           'account_id',
               'client_name',
               'summary_month',
               'order_id',
               'client_id'
	           ];

	if($rowone){

			var new_is_separate = false;
			if($databk.new_is_separate != undefined){
				new_is_separate = $databk.new_is_separate;
			}
			if(!new_is_separate && $databk.is_new){

				$colName.forEach(function($rowcolName) {
						$rowcolName = $rowcolName.trim();
						$databk[$rowcolName] = "";
						$databk['is_error_'+$rowcolName] = false;
						$databk['is_error_j_code_branch'] = false;
				});

			}
			else{
				$colName.forEach(function($rowcolName) {
					if($databk[$rowcolName +'_has_oldVal'] != undefined){
						$rowcolName = $rowcolName.trim();
						$databk[$rowcolName] = $databk[$rowcolName +'_has_oldVal'];
						$databk['is_error_'+$rowcolName] = false;
					}

					$databk['is_error_j_code_branch'] = false;
				});
				if(($databk['j_code'] != "") && ($databk['branch_cd'] !=  "")){
					$databk.is_difference_common= false;
				}
				else{
					$databk.is_difference_common= true;
				}
				$databk.is_editing = false;
			}
	}
	else{
			 $databk.forEach(function($rowdatabk) {
					var new_is_separate = false;
					if($rowdatabk.new_is_separate != undefined){
						new_is_separate = $rowdatabk.new_is_separate;
					}

					if(!new_is_separate && $rowdatabk.is_new){

						$colName.forEach(function($rowcolName) {
								$rowcolName = $rowcolName.trim();
								$rowdatabk[$rowcolName] = "";
								$rowdatabk['is_error_'+$rowcolName] = false;
								$rowdatabk['is_error_j_code_branch'] = false;
						});

					}
					else{

						$colName.forEach(function($rowcolName) {

								if($rowdatabk[$rowcolName +'_has_oldVal'] != undefined){
									$rowcolName = $rowcolName.trim();
									$rowdatabk[$rowcolName] = $rowdatabk[$rowcolName +'_has_oldVal'];
									$rowdatabk['is_error_'+$rowcolName] = false;
								}
								$rowdatabk['is_error_j_code_branch'] = false;

						});
						 if(($rowdatabk['j_code'] != "") && ($rowdatabk['branch_cd'] !=  "")){
							 $rowdatabk.is_difference_common= false;
						 }
						 else{
							 $rowdatabk.is_difference_common= true;
						 }

						 $rowdatabk.is_error_j_code_branch = false;
						 $rowdatabk.is_editing = false;

					}
			});

	}

}
//#7990:Start


