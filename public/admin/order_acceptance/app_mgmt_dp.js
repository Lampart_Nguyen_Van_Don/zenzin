// Order acceptance
// #6784-S
//#7846:Start
// var app = angular.module('app', [ 'ngTouch', 'ngSanitize', 'ui.grid', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.pagination', 'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.exporter', 'ui.grid.cellNav']);
var app = angular.module('app', ['ngSanitize', 'ui.grid', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.cellNav']);
//#7846:End
//#6784-E
var postion_return;
var isCheckAll= true;
//#7846:Start
//var isAdddButton = false;
//* #6946:Start
//var colCurrentName = "";
//#7846:End
var just_save = false;
//* #6946:End

//#7183:Start
var list_row_update_error = [];
var list_row_insert_error = [];
//#7183:End
//#7846:Start
//app.controller('MainCtrl', ['$scope', '$rootScope', '$http', '$q', '$interval', 'uiGridConstants', 'uiGridExporterConstants', function($scope, $rootScope, $http, $q, $interval, uiGridConstants, uiGridExporterConstants) {
app.controller('MainCtrl', ['$scope', '$rootScope', '$http', '$q', '$interval', 'uiGridConstants', function($scope, $rootScope, $http, $q, $interval, uiGridConstants) {
//#7846:End
    $rootScope.lang = 'ja';
//#7158:Start
    var $outsideScope = $scope;
//#7158:End

//#7846:Start
    /* start volyminhnhan@gmail.com modification*/
    // convert thí block to a function to call whenever you need
//    $scope.setTaxRate = function() {
//      if (typeof $rootScope.tax_rates == 'undefined') {
//            $http({
//                url : '/_admin/consumption_tax/get_tax_rate',
//                method : 'POST',
//                data : {
//                    tax_type : 1
//                }
//            }).success(function(data) {
//                $rootScope.tax_rates = data;
//            });
//        }
//    }
    /* end volyminhnhan@gmail.com modification*/
//#7846:End


    $('#slide-left-btn').click(function () {
        $interval(function () {
            $scope.gridApi.core.handleWindowResize();
        },500, 1);
    });


 // <------------------------------------------------------------ CLICK ENTER TO SEARCH ----------------------------------------------------------->
    $("#search-form").keypress(function (e) {
         var key = e.which;
         if(key == 13)  // the enter key code
          {
             e.preventDefault();
             var from_jcode_branch = $("input[name='from_jcode_branch']").is(':focus');
             var to_jcode_branch = $("input[name='to_jcode_branch']").is(':focus');
             var client_name = $("input[name='client_name']").is(':focus');
             var source_subcontractor_name = $("input[name='source_subcontractor_name']").is(':focus');
             var from_delivery_date = $("input[name='from_delivery_date']").is(':focus');
             var to_delivery_date = $("input[name='to_delivery_date']").is(':focus');
             var from_summary_month = $("input[name='from_summary_month']").is(':focus');
             var to_summary_month = $("input[name='to_summary_month']").is(':focus');
             var order_acceptance_status = $("input[name='order_acceptance_status[]']").is(':focus');
             var business_unit_id = $("select[name='business_unit_id']").is(':focus');
             var account_id = $("select[name='account_id']").is(':focus');
             if(from_jcode_branch | to_jcode_branch | client_name | source_subcontractor_name | source_subcontractor_name
                     | from_delivery_date |to_delivery_date | from_summary_month
                     | to_summary_month
                     | order_acceptance_status
                     | business_unit_id | account_id ) {
                 $rootScope.search();
             }
          }
     });
    // <------------ END CLICK ENTER TO SEARCH ------------------------>

    $rootScope.search = function() {
        $scope.error_messages = '';
        $('.btn-search').addClass('loading icon-spin');
        $.ajax({
            url: '/_admin/order_acceptance/show/type/dp',
            type: 'POST',
            data: $("#search-form").serialize(),
        })
        .done(function(data) {

            if (data.status == 'success') {
                  if (typeof data.search_result.is_over_record != 'undefined') {
                      $scope.is_over_record = true;
                      $scope.showWatermark = false;
                      $scope.gridOptions.data = [];
                      $('.btn-search').removeClass('loading icon-spin');
                      return;
                 }
                for(var i = 0; i < data.search_result.length;i ++) {

                    data.search_result[i].is_new						 = false;
                    data.search_result[i].is_editing					 = false;
                    data.search_result[i].is_change						 = false;
                    data.search_result[i].summary_month_for_save = data.search_result[i].summary_month;
                }

                if ($("#delivery_date_error").text()) {
                    $("#delivery_date_error").html('').hide();
                }
                if ($("#summary_month_error").text()) {
                    $("#summary_month_error").html('').hide();
                }
                if ($("#jcode_branch_error").text()) {
                    $("#jcode_branch_error").html('').hide();
                }
                $scope.gridOptions.data = data.search_result;


                $scope.showWatermark = false;
                $scope.is_over_record = false;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);

                if(data.search_result.length == 0){
                    $scope.showWatermark = true;
                    $scope.gridOptions.data = [];
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                }
            }
            else {

                $("#delivery_date_error").html('').hide();
                $("#summary_month_error").html('').hide();

                $scope.showWatermark = true;
                $scope.is_over_record = false;
                $scope.gridOptions.data = [];
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);

                // Show errors on form
                if (data.mesg.delivery_date) {
                    $("#delivery_date_error").html(data.mesg.delivery_date).show();
                }
                if (data.mesg.summary_month) {
                    $("#summary_month_error").html(data.mesg.summary_month).show();
                }
                if (data.mesg.jcode_branch) {
                    $("#jcode_branch_error").html(data.mesg.jcode_branch).show();
                }
            }
            // Remove circle loading from button search
            $('.btn-search').removeClass('loading icon-spin');
        })
    }

    function get_consumption_tax(summary_month) {
        if (!summary_month) {
            var today = new Date();
            summary_month = today.getFullYear() + '-';
            summary_month += (today.getMonth() + 1) < 10 ? '0' + (today.getMonth() + 1) : (today.getMonth() + 1);
        }
        if (summary_month.length > 7) {
            summary_month = summary_month.substr(0, 7);
        }
        summary_month = summary_month.replace(/\//g, '-');
        if (/^([0-9]{4}-(0[1-9]|1[012]))$/.test(summary_month) === false) {
            console.error('The date ' + summary_month + ' is not format as YYYY-mm');
            return false;
        }

        var i = 0;
        while (i < glb_consumption_tax.length && summary_month < glb_consumption_tax[i].adaptation_date.substr(0, 7)) {
            i++;
        }

        if (i < glb_consumption_tax.length) {
            return glb_consumption_tax[i].rate;
        } else {
            console.error('Cannot get tax rate of date ' + summary_month);
            return false;
        }
    }

    $scope.show_approved_all = function(row) {
        // #6741-S Modify
//Fixbug taskcheckbox:Start
        var amount = parseFloat(row.entity.amount_frist_bd);
//Fixbug taskcheckbox:End
        var can_approvel = row.entity.can_approve;

        var  datsummary_month = 	closingdateend;
        var  datenow = row.entity.summary_month;
        var  newsmday = row.entity.summary_month_for_save;
        var  boolhien = 1;

        if (datenow != null && datenow != '') {
            boolhien =  compareSummarydate(datsummary_month,datenow,newsmday);
        }
        var is_acceptance_input = row.entity.flag_is_accceptance_input;

        if(boolhien == 0){
            can_approvel = false;
        }
        else{
            can_approvel = boolhien;
        }
        if(amount == 0){
            return true && can_approvel;
        }
        return amount && can_approvel;
     // #6741-E Modify
    }
    function compareSummarydate(datsummary_month,datenow,newsmday2){

//#8436:START
        if((datenow == ""  || datenow == null ) && (newsmday2 != "" && newsmday2 != null )){
            datenow = newsmday2;
        }
//#8436:END
        var nfind = datsummary_month.indexOf("/");
        if(nfind > 0){
            datsummary_month = datsummary_month.replace('/', '-');
        }
        var listdatsummary_month = datsummary_month.split('-');

        if(datenow == null ){

                    var client_name = '';
                      datenow = datsummary_month;
        }

        if(datenow != ""){
            var nfind2 = datenow.indexOf("/");
            if(nfind2 > 0){
            datenow = datenow.replace('/', '-');
            }
        }
        if(listdatenow != ""){
           var listdatenow = datenow.split('-');
        }
        var boolhien = 1;
        if(listdatsummary_month[0] == listdatenow[0]){
//#7115:Start
            // if(listdatsummary_month[1] >= listdatenow[1]){
            if(parseInt(listdatsummary_month[1]) >= parseInt(listdatenow[1])){
//#7115:End
                boolhien = 0;
            }
        }
        if(listdatsummary_month[0] > listdatenow[0]){
            boolhien = 0;
        }
        return boolhien;

    }
    $scope.can_add = function(row) {

        if (row.entity.can_add) {
            if (row.entity.summary_month_has_been_edited == true) return true;
            var  datsummary_month = 	closingdateend;
            var  datenow = row.entity.summary_month;
            var  newsmday = row.entity.summary_month_for_save;
            var  boolhien = 1;

            if (datenow != null && datenow != '') {
                boolhien =  compareSummarydate(datsummary_month,datenow,newsmday);
            }
            if(boolhien == 0){
                return false;
            }
            else{
                return row.entity.can_add;
            }


        }
        return false;
    }

    $scope.can_separate = function(row) {
            var datsummary_month = closingdateend;
            var datenow          = row.entity.summary_month_for_save;
            var newsmday         = row.entity.summary_month_for_save;
            var  boolhien = 1;

            if (datenow != null && datenow != '') {
                boolhien =  compareSummarydate(closingdateend, row.entity.summary_month,row.entity.summary_month_for_save);
            }
            if(boolhien == 0){
                return false;
            }
            else{
                return  row.entity.can_separate
            }
            return  row.entity.can_separate
    }
    $scope.can_delete = function(row) {

            if (row.entity.is_deleting) return false;
            if (row.entity.can_delete) {
                var boolhien = 1;
                if ((row.entity.summary_month != null && row.entity.summary_month != '') || (row.entity.summary_month_for_save != null && row.entity.summary_month_for_save != '')) {
                    boolhien = compareSummarydate(closingdateend, row.entity.summary_month, row.entity.summary_month_for_save);
                }
                return boolhien;
            }

            return false;

    }

    $scope.can_edit = function(row) {
        if (row.entity.summary_month_has_been_edited == true) return true;
        var  can_edit = row.entity.can_edit;
        var  datsummary_month = 	closingdateend;
        var  datenow = row.entity.summary_month;
        var  newsmday = row.entity.summary_month_for_save;
        var  boolhien = 1;
        if (datenow != null && datenow != '') {
            boolhien =  compareSummarydate(datsummary_month,datenow,newsmday);
        }
         var is_acceptance_input = row.entity.flag_is_accceptance_input;

         if(boolhien == 0){
             var is_new = row.entity.is_new;

             if(is_new){
                 can_edit = true;
             }
             else{
                 can_edit = false;
             }
         }
         else{
             can_edit = boolhien;
         }
        return can_edit && row.entity.order_acceptance_status != order_acceptance_status_val.account_tighten;
    }

    $scope.deleteRow = function(row) {
        row.entity.is_deleting = true;
        row.entity.is_editing = true;
    };

    $scope.undo_delete = function(row) {
        row.entity.is_editing = true;
        row.entity.is_deleting = false;
    }
    $scope.backupdaterow = function(row){
        if(!row.is_editing){
            backupdate(row,true);
        }


    }
    $scope.difference_common = function(row) {

        if(row.entity.j_code_has_oldVal  == undefined){ row.entity.j_code_has_oldVal = row.entity.j_code; }
        if(row.entity.branch_cd_has_oldVal  == undefined){ row.entity.branch_cd_has_oldVal = row.entity.branch_cd; }
        if(row.entity.order_id_has_oldVal  == undefined){ row.entity.order_id_has_oldVal = row.entity.order_id;  }
        if(row.entity.account_id_has_oldVal  == undefined){  row.entity.account_id_has_oldVal = row.entity.account_id; }
        if(row.entity.client_name_has_oldVal  == undefined){  row.entity.client_name_has_oldVal = row.entity.client_name; }
        if(row.entity.client_id_has_oldVal  == undefined){  row.entity.client_id_has_oldVal = row.entity.client_id; }
        if(row.entity.summary_month_has_oldVal  == undefined){  row.entity.summary_month_has_oldVal = row.entity.summary_month; }
        row.entity.is_change = true;
        row.entity.j_code        = '';
        row.entity.branch_cd     = '';
        row.entity.order_id      = null;
        row.entity.account_id    = null;
        row.entity.client_name   = null;
        row.entity.client_id     = null;
        row.entity.summary_month = '';
        row.entity.summary_month_for_save = '';
        row.entity.is_editing = true;

    }

    $scope.post_is_acceptance_input = post_is_acceptance_input;

    var columnDefs = [
        {
            field: 'approved_all',
            displayName: label.approve_all,
            width: 73,
            visible: oa_can_approve,
            enableCellEdit: false,
            enableSorting: false,
//#7155:Start
//            enableColumnResize: false,
            enableColumnResize: true,
//#7155:End
            cellTemplate: "<div class='center-checkbox-wrapper'><input type='checkbox' ng-show='grid.appScope.show_approved_all(row)' ng-model='row.entity.is_approved' ng-init='row.entity.is_approved = row.entity.is_approved ? true : false' ng-disabled='row.entity.is_deleting'></div>"
        },
        {

            field: 'action',
            displayName: label.action,
            width : 135,
            visible: oa_can_modify,
            enableCellEdit: false,
            enableColumnResize: true,
            enableSorting: false,
            cellTemplate:
                    '<div class="action-btn-wrapper">'+
                        '<button ng-show="grid.appScope.can_separate(row)" ng-click="grid.appScope.separateRow(row.entity)" class="btn primary btn-small btn-green e-control" ng-disabled="row.entity.is_deleting" ng-class="{\'hide-btn\': row.entity.is_deleting}">'+btn.separate+'</button>'+
                        '<button class="btn primary btn-small btn-green e-control" ng-show="grid.appScope.can_delete(row)" ng-click="grid.appScope.deleteRow(row)">'+btn.delete+'</button>'+
                        '<button class="btn primary btn-small btn-green btn-undo e-control" ng-show="row.entity.is_deleting" ng-click="grid.appScope.undo_delete(row)">'+btn.undo+'</button>'+
                    '</div>'
        },
        {
            field: 'edit',
            displayName: label.edit,
            width: 55,
            visible: oa_can_modify,
            enableSorting: false,
            enableCellEdit: false,
            enableColumnResize: true,
            cellTemplate: "<div class='center-checkbox-wrapper' ng-show='grid.appScope.can_edit(row)'><input type='checkbox'  ng-click='grid.appScope.backupdaterow(row.entity,true)' ng-init='row.entity.is_editing = row.entity.is_editing == true ? true : false' ng-model='row.entity.is_editing' ng-disabled='row.entity.is_deleting'></div>"

        },
        {
            field: 'order_acceptance_status',
            displayName: label.order_acceptance_status,
            width: 100,
            enableCellEdit: false,
            cellFilter: 'mapOrderAcceptanceStatus'
        },
        {
            field: 'j_code',
            displayName: label.jcode,
            width: 80,
            cellFilter: 'mapJCode:row',
            cellTemplate: '<div class="ui-grid-cell-contents"><a ng-class="{\'no-link\' : !row.entity.order_id || row.entity.is_editing || row.entity.order_id == 0}" ng-click="grid.appScope.show_order(row)">{{COL_FIELD CUSTOM_FILTERS}}</a></div>',
            cellEditableCondition: function($scope) {
              return  is_auth_change && ($scope.row.entity.is_new || $scope.row.entity.is_editing || $scope.row.entity.order_acceptance_status == UNAPPROVED) && !$scope.row.entity.is_deleting && $outsideScope.can_edit($scope.row) && (( !$scope.row.entity.separate_has_j_code_branch_cd))  && !$scope.row.entity.is_separate;
            }
        },
        {
            field: 'branch_cd',
            width: 40,
            displayName: label.branch,
            cellFilter: 'mapBranchCd:row',
            cellEditableCondition: function($scope) {
                  return  is_auth_change && ($scope.row.entity.is_new || $scope.row.entity.is_editing || $scope.row.entity.order_acceptance_status == UNAPPROVED) && !$scope.row.entity.is_deleting && $outsideScope.can_edit($scope.row) && (( !$scope.row.entity.separate_has_j_code_branch_cd))  && !$scope.row.entity.is_separate;
            }
        },
        {
            field: 'account_id',
            width: 100,
            displayName: label.account_id,
            editableCellTemplate: 'ui-grid/dropdownEditor',
            cellFilter : 'mapAccountId',
            editDropdownIdLabel: 'id',
            editDropdownValueLabel: 'name',
            enableCellEdit: true,
            editDropdownOptionsArray : glb_account_options,
            cellEditableCondition:false,
        },
        {
            field: 'client_name_order',
            width: 100,
            displayName: label.client_name,
            cellTemplate: '<div class="ui-grid-cell-contents"><a ng-class="{\'no-link\' : !row.entity.client_id}" ng-click="grid.appScope.show_client(row)">{{row.entity.client_name_order}}</a></div>',
            cellEditableCondition:false,
            cellFilter: 'mapClientName:row'
        },
        {
            field: 'pro_name',
            width: 120,
            displayName: label.product_name,
            cellEditableCondition:false,
            cellFilter: 'mapClientName:row'

        },
        {
            field: 'content_pro',
            width: 120,
            displayName: label.content_order,
            cellEditableCondition:false,
            cellFilter: 'mapClientName:row'
        },
        {
            field: 'source_subcontractor_id',
            displayName: label.source_subcontractor_id,
            width: 120,
            editableCellTemplate : 'ui-grid/dropdownEditor',
            cellFilter: 'mapSubcontractorId:row',
            editDropdownIdLabel: 'id',
            editDropdownValueLabel : 'abbr_name',
            editDropdownOptionsArray : glb_direct_subcontractor_options,
        },
        {
            field: 'unit_price_in_order',
            width: 100,
            displayName: label.unit_price_in_order,
            cellEditableCondition:false,
            cellFilter: 'mapUnitPrice:row'
        },
        {
            field: 'quanity_in_order',
            width: 40,
            displayName: label.quanity_in_order,
            cellEditableCondition:false,
            cellFilter: 'mapQuantity:row',
        },
        {
            field: 'sales_in_order',
            width: 100,
            displayName: label.sales_in_order,
            cellEditableCondition:false,
            cellFilter: 'mapUnitPrice:row',
        },
        {
            field: 'total_cost_in_order_excluding_tax',
            width: 100,
            displayName: label.total_cost_in_order_excluding_tax,
            cellEditableCondition:false,
            cellFilter: 'mapTotalPriceFormat:row',
        },
        {
            field: 'amount',
            displayName: label.amount,
            width: 100,
            cellClass: 'a-right',
            cellFilter: 'mapTotalPriceFormat:row'
        },
        {
            field: 'delivery_date',
            displayName: label.delivery_date,
            width: 90,
            cellFilter: 'mapDeliveryDate:row'
        },
        {
            field: 'summary_month',
            width: 90,
            displayName: label.summary_month,
            cellFilter: 'mapSummaryMonth:row',
            cellEditableCondition: function($scope) {
                if ($scope.row.entity.order_acceptance_status == order_acceptance_status_val.approved) {
                    return $scope.row.entity.is_editing && $scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting && (!$scope.row.entity.is_separate || ($scope.row.entity.is_separate && !$scope.row.entity.separate_has_j_code_branch_cd));
                }
                return $scope.row.entity.is_difference_common && !$scope.row.entity.is_deleting && $outsideScope.can_edit($scope.row) && (!$scope.row.entity.is_separate || ($scope.row.entity.is_separate && !$scope.row.entity.separate_has_j_code_branch_cd));
            }
        },

    ];
    $scope.gridOptions = {
        enableCellEditOnFocus: true, // set any editable column to allow edit on focus
        enablePagination: false,
        enableSorting: true,
        enableFiltering: false,
        columnDefs: columnDefs,
        enableRowSelection: false,
        enableGridMenu: false,
        //each column have own menu, set to true to show the menu of that column
        enableColumnMenus: true,
        // trigger for the time auto call save event when a cell was edited. Set -1 to turn it off
        rowEditWaitInterval : -1,
        // control the scroll bar of grid, there are two value accepted ALWAYS/NEVER
        enableHorizontalScrollbar: uiGridConstants.scrollbars.ALWAYS,
        enableVerticalScrollbar: uiGridConstants.scrollbars.ALWAYS,
        rowTemplate:
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ui-grid-cell ng-class="{\'new-grid-row\': row.entity.is_new, \'row-error\': row.entity.is_error_j_code || row.entity.is_error_branch_cd || row.entity.is_error_unit_price || row.entity.is_error_quantity || row.entity.is_error_delivery_date || row.entity.is_error_fee || row.entity.is_error_weight || row.entity.is_error_summary_month || row.entity.is_error_tax_free || row.entity.is_error_shipping_charge_tax || row.entity.is_error_stamp_tax || row.entity.is_error_etc_tax || row.entity.is_error_shipping_charge || row.entity.is_error_etc || row.entity.is_error_j_code_branch, \'row-deleting\': row.entity.is_deleting}" ></div>',
        cellEditableCondition: function($scope){

            if(is_auth_change){
                if ($scope.row.entity.is_deleting) return false;
                          if($scope.row.entity.order_acceptance_status == UNAPPROVED){
                            return $outsideScope.can_edit($scope.row);
                          }
                          else{
                               return $scope.row.entity.is_editing;
                          }

            }
        },
        onRegisterApi: function(gridApi){

            $scope.gridApi = gridApi;

            gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                 if(rowEntity.amount_has_oldVal  == undefined){

                    rowEntity.amount_has_oldVal = rowEntity.amount;
                }

                if (oldValue != newValue) {

                    rowEntity.is_change  = true;

                    rowEntity.is_editing = true;
                    just_save = false;
                    rowEntity[colDef.field + '_has_been_edited'] = true;

                    if(rowEntity[colDef.field + '_has_oldVal']  == undefined){
                        rowEntity[colDef.field + '_has_oldVal'] = oldValue;
                    }


                    if((colDef.field == 'j_code') || (colDef.field == 'branch_cd')){
                    var $colNameGetVal =['account_id',
                                         'client_name_order',
                                         'summary_month',
                                         'pro_name',
                                         'content_pro',
                                         'unit_price_in_order',
                                         'quanity_in_order',
                                         'sales_in_order',
                                         'total_cost_in_order_excluding_tax'
                                        ];

//            			console.log($colName);
                        $colNameGetVal.forEach(function($rowcolName) {

                                if(rowEntity[$rowcolName +'_has_oldVal'] ==  undefined){
                                    $rowcolName = $rowcolName.trim();
                                    rowEntity[$rowcolName + '_has_oldVal'] = rowEntity[$rowcolName];

                                }

                        });
                    }
//#7990:End
//#7115:End
                }
//#6946:End
                if (colDef.field == 'j_code' || colDef.field == 'branch_cd') {

                    rowEntity.account_id    = null;
                    rowEntity.client_name_order   = null;
                    rowEntity.client_id     = null;
                    rowEntity.order_id      = null;
                    rowEntity.summary_month = null;
                    rowEntity.pro_name				  =  null;
                    rowEntity.content_pro			  =  null;
                    rowEntity.unit_price_in_order	  =  null;
                    rowEntity.quanity_in_order	      =  null;
                    rowEntity.sales_in_order	      =  null;
                    rowEntity.total_cost_in_order_excluding_tax =  null;

                    if (rowEntity.j_code && rowEntity.branch_cd) {
                        var set_jcode_branch_cd_data = function(data) {
                             rowEntity.is_error_j_code_branch = true;
                            if (data) {
                                // #6901 -Modify -S
                                var is_change_report_apply = data['is_change_report_apply'];
                                if(is_change_report_apply == 0){


//                                    console.log(data);
                                    var summary_month = null;
                                     summary_month = data.delivery_date;
                                     summary_month = summary_month.replace(/-/g, '/');
                                     rowEntity.account_id             = data.j_account_id;
                                     rowEntity.client_name_order      = data.client_name;
                                     rowEntity.client_id              = data.client_id;
                                     rowEntity.order_id               = data.id;
                                     rowEntity.summary_month          = summary_month.substring(0, 7);
                                     rowEntity.pro_name				  =  data.pro_name;
                                     rowEntity.content_pro			  =  data.content_pro;
                                     rowEntity.unit_price_in_order	  =  data.unit_price_in_order;
                                     rowEntity.quanity_in_order	      =  data.quanity_in_order;
                                     rowEntity.sales_in_order	      =  data.sales_in_order;
                                     rowEntity.total_cost_in_order_excluding_tax =  data.total_cost_in_order_excluding_tax;
                                     rowEntity.is_error_j_code_branch = false;
                                }
                                // #6901 -Modify -E
                            }
                        }

                        $scope.get_order_detail(rowEntity.j_code, rowEntity.branch_cd, set_jcode_branch_cd_data);
                    }
                }
                var currentcell = $scope.gridApi.cellNav.getFocusedCell();
                var fieldname = currentcell.col.field;
                setAllValidateNumber(rowEntity,fieldname);

            });
            gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
        }

    };

    $scope.export = function(){

        $("#search-form").attr('action', '/_admin/order_acceptance/export_csv');
//#9748:Start
        $("#search-form").append('<input type="hidden" name="is_dp" value="1">');
//#9748:End
        $("#search-form").submit();
        $("#search-form").attr('action', '/_admin/order_acceptance/show/type/dp');
    };

    $scope.saveRow = function(rowEntity) {

        promise = $q.defer();
        $scope.gridApi.rowEdit.setSavePromise(rowEntity, promise.promise);
        promise.reject();

    };

    $scope.show_order = function (row) {

        if (row.entity.is_editing || !row.entity.order_id || row.entity.order_id == 0) return;

        $("#dialog").dialog({
            title: lang['text_title_show_order'],     //#6849 :  MOdify -S
        })
        $rootScope.loadDetail(row.entity.order_id);
        $(window).trigger('resize');
    };

    function display_error_for_disapprovel(data, save_datas){
         $(".messages").show();

         if (data.status) {
             $scope.error_messages = "<div class='success-message'>"+data.messages+"</div>";

             for (i = 0; i < save_datas.length; i++) {
                 if (save_datas[i]) {
                     save_datas[i].order_acceptance_status = order_acceptance_status_val.unapproved;
                 }
             }
             return;
         } else {
             if (typeof data.messages == 'object') {
                var error     = null,
                    row_num   = null,
                    flag      = false;
                 error = "<div class='error-message'>";
                 angular.forEach(data.messages, function(rows, key) {
                     if (rows) {
                         row_num = parseInt(key) + 1;

                         if (!flag) {
                             flag = true;
                             error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
                         } else {
                             error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
                         }

                         angular.forEach(rows, function(message, key) {
                             error += "<p class='import-error'>" + message + "</p>";
                         });
                         // if save error set whatever column error status to true, then row turns red
                         datas[key].is_error_j_code_branch = true;
                     }
                 });
                 error += "</div>";
                 $("#dialog-errors").html(error);
                 $("#dialog-errors").dialog({
                     title: '確認',
                     height: 'auto',
                     minWidth: 500,
                     minHeight: 250,
                     maxHeight: 250,
                     resizable: true,
                     modal: false,
                     dialogClass: 'acceptance-dialog fixed-dialog',
                     position: { my: "right top+30", at: "right top+30", of: window},
                     dragStart: function(event, ui) {
                         $('.acceptance-dialog').removeClass('fixed-dialog')
                     },
                     dragStop: function(event, ui) {
                         // $('.acceptance-dialog').addClass('fixed-dialog')
                     },
                     resizeStart: function( event, ui ) {
                         $(this).dialog("option", "maxHeight", false);
                     }
                 })


             } else {
                 $scope.error_messages = "<div class='error-message'>"+data.messages+"</div>"
             }
         }
    }
    $scope.cancel_acceptance = function() {
        var datas = null,
            flag = false,
            save_datas = [];
        $(".loadgif").show();
        $scope.error_messages = '';
        datas = $scope.gridApi.grid.appScope.gridOptions.data;
        var changedata = false;
        var k = 0;
        for(i = 0; i < datas.length; i++) {
            if (datas[i].is_approved && datas[i].order_acceptance_status != order_acceptance_status_val.unapproved) {

                save_datas[k] = datas[i];
                k++;
                flag = true;
                if(datas[i].is_editing || datas[i].is_deleting ){
                    changedata = true;

                }
            }
        }

        if (!flag) {
            $("#dialog-messages").html(label.please_select_unapprove);
            $("#dialog-messages").dialog({
                title: '注意',
                width: 'auto',
                height: 147,
                modal: true,
                dialogClass: "acceptance-dialog",
                buttons: [{
                    text: "OK",
                    click: function() {
                    $(this).dialog("close");
                    }
                }],
            });
            $(".loadgif").hide();
            return;
        }
        if(changedata){
            if(!just_save){
                $(".loadgif").hide();
                  approved_now = false;
                    $("#dialog-messages").html('編集中のデータがあります。先に保存してください。');
                    $("#dialog-messages").dialog({
                        title: '注意',
                        width: 'auto',
                        height: 147,
                        modal: true,
                        dialogClass: "acceptance-dialog",
                        buttons: {
                               " OK": function() {
                                   just_save = false;
                                    $(this).dialog("close");
                                   $(".loadgif").hide();
                                    return;
                             }
                        }
                   });
            }
        }
        var n_save = save_datas.length;
        if(n_save >400){
             $(".loadgif").show();
            var recode = 200;
            var n_lenght_for = n_save /recode;
            var n_du =  n_save % recode;
            var status = true;
            if(n_du > 0 ){
                n_lenght_for = parseInt(n_lenght_for) + 1;
            }

            var today = new Date();
            today = today.getDate()+"_"+today.getMonth()+"_"+today.getFullYear()+"_"+today.getHours()+"_"+today.getMinutes()+"_"+today.getSeconds();
            var name_file ="result_disapprovel_"+today+'.json';
            var tongkey = 0;
            var dem = 0;
            var successall = true;
            var tongkey = 0;
            var dem = 0;
            var successall = true;

            for(var i = 0 ; i < n_lenght_for ; i++){
                $status = true;

                if(i == 0){

                    var j_start = 0;
                }
                else{

                    var j_start = parseInt(i)*recode;
                }

                if(i == (n_lenght_for -1) ){
                    var  n_j = n_save;
                }
                else{
                    var  n_j = parseInt(j_start) + recode;
                }

                var databach = new Array();

                for(var j  = j_start  ; j < n_j ;j++){
                   if(save_datas[j] != undefined){
                        databach.push(save_datas[j]);
                    }

                }
                $http({
                    url : "/_admin/order_acceptance/cancel_acceptance",
                    method : 'POST',
//#9748:Start
//                    data : {order_acceptance: databach , multi_larger:true,readfile:false,name_file:name_file,key:i}
                    data : {order_acceptance: databach , multi_larger:true,readfile:false,name_file:name_file,key:i, is_dp: true}
//#9748:End
                })
                .success(function(data, status, headers, config) {
                    status  =  data['status'];
                    tongkey = tongkey +1;
                    if(status){
                        dem ++;
                    }
                    if(!status){
                         $(".loadgif").hide();
                         display_error_for_disapprovel(data, save_datas);
                         return false;

                    }
                    if(dem == n_lenght_for){
                         $http({
                             url : "/_admin/order_acceptance/cancel_acceptance",
                             method : 'POST',
//#9748:Start
//                             data : {name_file:name_file, multi_larger:true,readfile:true,tongkey:tongkey}
                             data : {name_file:name_file, multi_larger:true,readfile:true,tongkey:tongkey, is_dp: true}
//#9748:End
                         })
                         .success(function(update_funcall, status, headers, config) {
                             $(".loadgif").hide();
                              display_error_for_disapprovel(update_funcall, save_datas);
                         });

                    }
                });
                if(!status){
                     $(".loadgif").hide();
                    //break;
                }
            }
        }
        else{
            $http({
                url : "/_admin/order_acceptance/cancel_acceptance",
                method : 'POST',
//#9748:Start
//                data : {order_acceptance: save_datas , multi_larger:false}
                data : {order_acceptance: save_datas , multi_larger:false, is_dp: true}
//#9748:End
            })
            .success(function(data, status, headers, config) {
                $(".loadgif").hide();
                 display_error_for_disapprovel(data, save_datas);

            })
        }


    };

    $scope.checkalllist = function(){
        var datas = null;
         datas = $scope.gridApi.grid.appScope.gridOptions.data;

             for(var i = 0; i < datas.length; i++) {
                 var  is_approved_row = datas[i]['order_acceptance_status'];
                 var  jcode     = datas[i]['j_code'];
                 var  brandcode = datas[i]['branch_cd'];
                 var  amount = datas[i]['amount'];
                 var  isamountTrue = true;

                    if(amount == ''){
                         isamountTrue = false;
                     }
                     else if(amount == null){
                          isamountTrue = false;
                     }
                    if ((is_approved_row == order_acceptance_status_val.unapproved) && (isamountTrue)) {
                         datas[i].is_approved =  isCheckAll ;

                     }
                    else {

                         datas[i].is_approved = false;

                     }
                    if(!isamountTrue){

                        datas[i].can_approve = false;
                    }
                    else{

                        datas[i].can_approve = true;
                    }
             }
         isCheckAll = !isCheckAll;
    };
    $scope.approved_all=function(){

        var datas        = null,
            update_datas = null;

        $(".loadgif").show();
        $scope.error_messages = '';
        datas = $scope.gridApi.grid.appScope.gridOptions.data;
        var changedata = false;
        var approved_flag  = false;
        var approved_now = true;
        update_datas = [];
        var k = 0;
        for( var i = 0; i < datas.length; i++) {
                datas[i].is_change_acceptance_status = datas[i].is_approved ? true : false;
                if(datas[i].is_approved && !datas[i].is_new){
                    if(!datas[i].is_deleting){
                        update_datas[k] = datas[i];
                        k++;
                    }
                    if(datas[i].is_editing || datas[i].is_deleting ){
                        changedata = true;
                    }


                }

                if(datas[i].is_change_acceptance_status ){
                    if(datas[i].order_acceptance_status == order_acceptance_status_val.unapproved && datas[i].is_approved && !datas[i].is_new){
                        approved_flag = true;
                    }
                }

                if(datas[i].is_new){
                    changedata = true;
                }
        }
        if(!approved_flag){
            $(".loadgif").hide();
           $("#dialog-messages").html(label.please_select_approve);
           $("#dialog-messages").dialog({
               title: '注意',
               width: 'auto',
               height: 147,
               modal: true,
               dialogClass: "acceptance-dialog",
               buttons: [{
                   text: "OK",
                   click: function() {
                   $(this).dialog("close");
                   }
               }],
           });
           return;
        }

        if(changedata){
            if(!just_save){
                $(".loadgif").hide();
                  approved_now = false;
                    $("#dialog-messages").html('編集中のデータがあります。先に保存してください。');
                    $("#dialog-messages").dialog({
                        title: '注意',
                        width: 'auto',
                        height: 147,
                        modal: true,
                        dialogClass: "acceptance-dialog",
                        buttons: {
                               " OK": function() {
                                   just_save = false;
                                    $(this).dialog("close");
                                    return;
                             }
                        }
                   });
            }


        }

        if(approved_now){
            $scope.saveupdateall(update_datas,true);
        }

    }

    $scope.saveupdateall = function(update_datas,is_approve_action) {

         just_save = true;
         var url = is_approve_action ? "/_admin/order_acceptance/approve" : "/_admin/order_acceptance/update";
            /*
             * dividng many item to upload
             * One refresh 100 record
             */
            var n_date_update = update_datas.length;

            if(n_date_update > 400){
                        var status = true;
                     var recode = 200;
                     var n_lenght_for = n_date_update /recode;
                     var n_du = n_date_update % recode;
                     if(n_du > 0 ){
                         n_lenght_for = parseInt(n_lenght_for) + 1;
                     }

                     var today = new Date();
                     today = today.getDate()+"_"+today.getMonth()+"_"+today.getFullYear()+"_"+today.getHours()+"_"+today.getMinutes()+"_"+today.getSeconds();
                     var name_file ="result_approvel_"+today+'.json';
                     var tongkey = 0;
                     var dem = 0;
                     var successall = true;
                     for(var i = 0 ; i < n_lenght_for ; i++){
                         $status = true;
                         var j_start = parseInt(i)*recode + 1;
                         if(i == (n_lenght_for -1) ){
                             var  n_j = n_date_update;
                         }
                         else{
                             var  n_j = parseInt(j_start) + recode;
                         }

                         var databach = new Array();
                         for(var j  = j_start  ; j < n_j ;j++){
                            if(update_datas[j] != undefined){
                                 databach.push(update_datas[j]);
                             }

                         }
                         var update_func = $http({
                             url : url,
                             method : 'POST',
//#9748:Start
//                             data : {order_acceptance: databach,name_file:name_file,readfile:false,key:i,app_sel:false}
                             data : {order_acceptance: databach,name_file:name_file,readfile:false,key:i,app_sel:false, is_dp: true}
//#9748:End
                         })
                        .success(function(update_func, status, headers, config) {
                             status = update_func['status'];

                             tongkey = tongkey +1;
                             if(status){
                                 dem ++;
                             }

                             if(!status){
                                        successall = false;
                                        $(".loadgif").hide();
                                        var error ="";
                                        angular.forEach(update_func.messages, function(rows, key) {
                                            if (rows) {
                                                row_num = parseInt(key) + 1 + j_start;

                                                error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
                                                angular.forEach(rows, function(message, key) {
                                                    error += "<p class='import-error'>" + message + "</p>";
                                                });
                                            }
                                        });
                                        error += "</div>";
                                        $("#dialog-errors").html(error);
                                        $("#dialog-errors").dialog({
                                            title: '確認',
                                            height: 'auto',
                                            minWidth: 500,
                                            minHeight: 250,
                                            maxHeight: 250,
                                            resizable: true,
                                            modal: false,
                                            dialogClass: 'acceptance-dialog fixed-dialog',
                                            position: { my: "right top+30", at: "right top+30", of: window},
                                            dragStart: function(event, ui) {
                                                $('.acceptance-dialog').removeClass('fixed-dialog')
                                            },
                                            dragStop: function(event, ui) {
                                                // $('.acceptance-dialog').addClass('fixed-dialog')
                                            },
                                            resizeStart: function( event, ui ) {
                                                $(this).dialog("option", "maxHeight", false);
                                            }
                                        });

                                        return;
                             }
                             if(dem == n_lenght_for){
                                 var update_funcall = $http({
                                     url : url,
                                     method : 'POST',
//#9748:Start
//                                     data : {name_file:name_file,readfile:true,tongkey:tongkey,app_sel:false}
                                     data : {name_file:name_file,readfile:true,tongkey:tongkey,app_sel:false, is_dp: true}
//#9748:End
                                 })
                                 .success(function(update_funcall, status, headers, config) {
                                       $(".loadgif").hide();
                                        if(successall){
                                                    if ($("#dialog-errors").hasClass('ui-dialog-content')) {
                                                        $("#dialog-errors").dialog('destroy');
                                                    }

                                                    $scope.error_messages = "<div class='success-message'>"+update_funcall.messages+"</div>";
                                                    $scope.gridApi.rowEdit.setRowsClean(update_datas);

                                                    for (i = 0; i < update_datas.length; i++) {
                                                       if (update_datas[i]) {
                                                           update_datas[i].order_acceptance_status = is_approve_action ? order_acceptance_status_val.approved : order_acceptance_status_val.unapproved;
                                                           update_datas[i].is_editing  = false;
                                                           $scope.setErrorStatus(update_datas[i], false);
                                                       }

                                                       update_datas[i].jcode_branch = update_datas[i].jcode+update_datas[i].branch_cd;

                                                    }

                                                    if (delete_flag) {
                                                       for (i = 0; i < delete_datas.length; i++) {
                                                           if (delete_datas[i]) {
                                                               rowIndex = $scope.gridApi.grid.appScope.gridOptions.data.indexOf(delete_datas[i]);
                                                               $scope.gridOptions.data.splice(rowIndex, 1);
                                                               $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                                                           }
                                                       }
                                                    }
                                               }
                                 });
                             }
                         });


                         }
                         if(!status){
                              $(".loadgif").hide();
                         }


             return;
          }
         else{

                 var update_func = $http({
                          url : url,
                          method : 'POST',
//#9748:Start
//                          data : {order_acceptance: update_datas,app_sel:true}
                          data : {order_acceptance: update_datas,app_sel:true, is_dp: true}
//#9748:End
                      })
                      .success(function(update_result, status, headers, config) {
                          $(".loadgif").hide();
                          if(is_approve_action){
                                status = update_result['status'];
                               if(status){

                                     $scope.error_messages = "<div class='success-message'>"+update_result.messages+"</div>";
                                     $scope.gridApi.rowEdit.setRowsClean(update_datas);

                                     for (i = 0; i < update_datas.length; i++) {
                                        if (update_datas[i]) {
                                            update_datas[i].order_acceptance_status = is_approve_action ? order_acceptance_status_val.approved : order_acceptance_status_val.unapproved;
                                            update_datas[i].is_editing  = false;
                                            $scope.setErrorStatus(update_datas[i], false);
                                        }
                                     }

                                }
                               else{
                                        var error ="";
                                        if( typeof update_result.messages === 'string' ){
                                             error += "<div><p class='import-error validation-error'><span class='row-error-icon'>" + update_result.messages + "</span></p>";

                                        }
                                        else{
                                             angular.forEach(update_result['messages'], function(rows, key) {
                                                    if (rows) {
                                                        row_num = parseInt(key) + 1;

                                                        error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
                                                        angular.forEach(rows, function(message, key) {
                                                            error += "<p class='import-error'>" + message + "</p>";
                                                        });
                                                    }
                                                });
                                        }

                                           error += "</div>";
                                           $("#dialog-errors").html(error);
                                           $("#dialog-errors").dialog({
                                               title: '確認',
                                               height: 'auto',
                                               minWidth: 500,
                                               minHeight: 250,
                                               maxHeight: 250,
                                               resizable: true,
                                               modal: false,
                                               dialogClass: 'acceptance-dialog fixed-dialog',
                                               position: { my: "right top+30", at: "right top+30", of: window},
                                               dragStart: function(event, ui) {
                                                   $('.acceptance-dialog').removeClass('fixed-dialog')
                                               },
                                               dragStop: function(event, ui) {
                                                   // $('.acceptance-dialog').addClass('fixed-dialog')
                                               },
                                               resizeStart: function( event, ui ) {
                                                   $(this).dialog("option", "maxHeight", false);
                                               }
                                           });

                               }

                          }
                      });

         }
    };

    $scope.save = function(is_approve_action) {
        var datas        = null,
            insert_datas = null,
            update_datas = null,
            delete_datas = null,
            data_refesh  = [],
            insert_flag  = false,
            update_flag  = false,
            delete_flag  = false,flag,
            has_error    = null,
            array_errors = [],
            error        = null,
            row_num      = null,
            rowIndex     = null;
            $scope.error_messages = '';
            datas = $scope.gridApi.grid.appScope.gridOptions.data;

            insert_datas = [];
            update_datas = [];
            delete_datas = [];
            var k = 0;
        // Filter action and get data on row follow action (add, update, delete)
        for( var i = 0; i < datas.length; i++) {
            if((datas[i].jcode_branch != "" && datas[i].jcode_branch != null ) && (datas[i].is_difference_common == true)){
                datas[i].returnStatusReturn = 1;
            }
            else{

                if(datas[i].is_editing){
                    datas[i].returnStatusReturn = 1;
                }
                else{
                    datas[i].returnStatusReturn = 0;
                }

            }

            if((datas[i].is_change) &&(!datas[i].is_editing)){

                    data_refesh[i] =  datas[i];

            }
            if (is_approve_action) {
//                datas[i].is_change_acceptance_status = datas[i].is_approved ? true : false;

                if (datas[i].is_approved && datas[i].is_new) {
                    insert_datas[i] = datas[i];
                    insert_flag = true;
                }
                if (datas[i].is_approved && !datas[i].is_new) {
                    update_datas[k] = datas[i];
                    update_datas[k].updating_row = i;
                    k++;
                    update_flag = true;
                }
            } else {
                datas[i].is_change_acceptance_status = false;

                // Add new row
                if (datas[i].is_editing && datas[i].is_new && !datas[i].is_deleting) {
                    insert_datas[i] = datas[i];
                    insert_flag = true;
                }
                // Update
                if ((datas[i].is_editing == true) && (!datas[i].is_new)) {
                    update_datas[k] = datas[i];
                    update_datas[k].updating_row = i;
                    k++;
                    update_flag = true;
                }
                // Delete
                if (datas[i].is_deleting) {
                    delete_datas[i] = datas[i];
                    delete_flag = true;
                }
            }
        }

        backupdate(data_refesh);

        // If notthing change when save
        if ((!insert_flag && !update_flag) && !delete_flag) {
            $("#dialog-messages").html(label.nothing_change);
            if (is_approve_action) $("#dialog-messages").html(label.please_select_approve);
            $("#dialog-messages").dialog({
                title: '注意',
                width: 'auto',
                height: 147,
                modal: true,
                dialogClass: "acceptance-dialog",
                buttons: [{
                    text: "OK",
                    click: function() {
                    $(this).dialog("close");
                    }
                }],
            });
            return;
        }
        var update_func = {"data":{"status":true,"messages":label.update_success}};
        if (update_flag) {

                      just_save = true;
                      $(".loadgif").show();
                       var url = is_approve_action ? "/_admin/order_acceptance/approve" : "/_admin/order_acceptance/update";
                                var update_func = $http({
                                      url : url,
                                      method : 'POST',
//#9748:Start
//                                      data : {order_acceptance: update_datas,app_sel:true}
                                      data : {order_acceptance: update_datas,app_sel:true, is_dp: true}
//#9748:End
                                  })
                                  .success(function(update_result, status, headers, config) {
                                      if(update_result['status']){
                                            for (i = 0; i < update_datas.length; i++) {
                                                   if (update_datas[i]) {
                                                       if(update_datas[i].returnStatusReturn == 1){
                                                           update_datas[i].order_acceptance_status = order_acceptance_status_val.unapproved;
                                                              update_datas[i].is_editing  = false;
                                                           $scope.setErrorStatus(update_datas[i], false);
                                                       }
                                                       update_datas[i].jcode_branch = update_datas[i].j_code+update_datas[i].branch_cd;
                                                       update_datas[i].j_code_has_oldVal =  update_datas[i].j_code;
                                                       update_datas[i].branch_cd_has_oldVal =  update_datas[i].branch_cd;
                                                   }
                                                }
                                      }

                                      if(update_result['status']){
                                          for( var i = 0; i < update_datas.length; i++) {
                                                var  $colName2 =['j_code','branch_cd','account_id',
                                                             'source_subcontractor_id',
                                                             'delivery_date',
                                                             'amount',
                                                             'account_id',
                                                             'client_name_order',
                                                            'summary_month',
                                                            'amount',
                                                            'pro_name',
                                                            'content_pro',
                                                            'unit_price_in_order',
                                                            'quanity_in_order',
                                                            'sales_in_order',
                                                            'total_cost_in_order_excluding_tax'
                                                             ];

                                                  $colName2.forEach(function($rowcolName) {

                                                          if(update_datas[i][$rowcolName +'_has_oldVal'] != undefined){
                                                              $rowcolName = $rowcolName.trim();
                                                              update_datas[i][$rowcolName +'_has_oldVal'] = update_datas[i][$rowcolName];
                                                              update_datas[i]['is_error_'+$rowcolName] = false;
                                                          }


                                                  });
                                                  update_datas[i].is_error_j_code_branch = false;
                                                  update_datas[i].is_editing = false;
//Fixbug taskcheckbox:Start
                                                  update_datas[i].amount_frist_bd = update_datas[i].amount;
//Fixbug taskcheckbox:End
                                          }
                                      }


                                    $(".loadgif").hide();
                                  });

        }
        var insert_func = {"data":{"status":true,"messages":label.update_success}};

        if (insert_flag) {

            $('.both-blind').show();
            var insert_func = $http({
                                url : "/_admin/order_acceptance/insert",
                                method : 'POST',
//#9748:Start
//                                data : {order_acceptance: insert_datas}
                                data : {order_acceptance: insert_datas, is_dp: true}
//#9748:End
                            })
                            .success(function(insert_result, status, headers, config) {
                                if(insert_result['status']){
                                    $rootScope.search();
                                }
                                $('.both-blind').hide();

                            })
        }

        $q.all(
            [
               update_func,
               insert_func
            ]
        ).then(function(result) {
            update_result = result[0].data;
            insert_result = result[1].data;
            flag      = false;
            has_error = false;
            array_errors = [];
            if (update_result.status) {

                $scope.gridApi.rowEdit.setRowsClean(update_datas);
                list_row_update_error = [];
                for (i = 0; i < update_datas.length; i++) {
                    if (update_datas[i]) {

                        update_datas[i].is_editing  = false;
                        $scope.setErrorStatus(update_datas[i], false);

                    }
                }

            } else {
                has_error = true;

                angular.forEach(update_result.messages, function(rows, key) {
                    if (rows) {
                        error = '';
                        row_num = parseInt(key) + 1;

                        if (!flag) {
                            flag = true;
                            error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
                        } else {
                            error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
                        }

                        angular.forEach(rows, function(message, key) {
                            if (message != '') {
                                error += "<p class='import-error'>" + message + "</p>";
                            }
                        });

                        array_errors[row_num] = error;
                        // if save error set whatever column error status to true, then row turns red
                        if (list_row_update_error.indexOf(parseInt(key)) == -1) {
                            list_row_update_error.push(parseInt(key));
                            datas[key].is_error_j_code_branch = true;
                        }
                    }
                });

                angular.forEach(list_row_update_error, function(val, index) {
                    if (update_result.messages.hasOwnProperty(parseInt(val)) == false) {
                        datas[val].is_error_j_code_branch = false;
                        list_row_update_error.splice(index, 1);
                    }
                });
            }

            // Result insert action
            if (insert_result.status) {
                $scope.gridApi.rowEdit.setRowsClean(insert_datas);
                list_row_insert_error = [];

                for (i = 0; i < insert_datas.length; i++) {
                    if (insert_datas[i]) {

                        insert_datas[i].order_acceptance_status = is_approve_action ? order_acceptance_status_val.approved : order_acceptance_status_val.unapproved;
                        insert_datas[i].id = insert_result.data_id[i];
                        insert_datas[i].is_editing  = false;
                        insert_datas[i].is_separate = false;
                        $scope.setErrorStatus(insert_datas[i], false);

                    }
                }
            } else {

                has_error = true;
                var total_will_be_deleted;
                angular.forEach(insert_result.messages, function(rows, key) {
                    if (rows) {

                        row_num = parseInt(key) + 1;
                        error = '';
                        total_will_be_deleted = 0;
                        if (delete_flag) {
                            angular.forEach(delete_datas, function(val, index) {
                                if (val.is_new && index < key) {
                                    total_will_be_deleted++;
                                }
                            });
                        } else if (delete_flag && !update_result.status) {
                            angular.forEach(delete_datas, function(val, index) {
                                if (!val.is_new && index < key) {
                                    total_will_be_deleted++;
                                }
                            });
                        }

                        row_num -= total_will_be_deleted;

                        if (!flag) {
                            flag = true;
                            error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
                        } else {
                            error += "<span class='row-error-icon'>" + row_num + label.row + "</span>";
                        }

                        angular.forEach(rows, function(message, key) {
                            if (message != '') {
                                error += "<p class='import-error'>" + message + "</p>";
                            }
                        });
                        array_errors[row_num] = error;
                        // if save error set whatever column error status to true, then row turns red
                        if (list_row_insert_error.indexOf(parseInt(key)) == -1) {
                            list_row_insert_error.push(parseInt(key));
                            datas[key].is_error_j_code_branch = true;
                        }
                    }
                });
                angular.forEach(list_row_insert_error, function(val, index) {
                    if (insert_result.messages.hasOwnProperty(parseInt(val)) == false) {
                        datas[val].is_error_j_code_branch = false;
                        list_row_insert_error.splice(index, 1);
                    }
                });
            }
            // set all error row to be red once more time
            angular.forEach(list_row_insert_error, function(val, index) {
                datas[val].is_error_j_code_branch = true;
            });
            angular.forEach(list_row_update_error, function(val, index) {
                if(datas[val].is_editing == true){
                    datas[val].is_error_j_code_branch = true;
                }

            });
            if (update_result.status) {
                if (delete_flag) {
                    for (i = 0; i < delete_datas.length; i++) {
                        if (delete_datas[i]) {
                            rowIndex = $scope.gridApi.grid.appScope.gridOptions.data.indexOf(delete_datas[i]);
                            $scope.gridOptions.data.splice(rowIndex, 1);
                            $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                        }
                    }
                }
            }

            if (has_error) {
                error = "<div class='error-message'>" + array_errors.join("") + "</div>";
                $("#dialog-errors").html(error);
                $("#dialog-errors").dialog({
                    title: '確認',
                    height: 'auto',
                    minWidth: 500,
                    minHeight: 250,
                    maxHeight: 250,
                    resizable: true,
                    modal: false,
                    dialogClass: 'acceptance-dialog fixed-dialog',
                    position: { my: "right top+30", at: "right top+30", of: window},
                    dragStart: function(event, ui) {
                        $('.acceptance-dialog').removeClass('fixed-dialog')
                    },
                    dragStop: function(event, ui) {
                        // $('.acceptance-dialog').addClass('fixed-dialog')
                    },
                    resizeStart: function( event, ui ) {
                        $(this).dialog("option", "maxHeight", false);
                    }
                })
            } else {
                if ($("#dialog-errors").hasClass('ui-dialog-content')) {
                    $("#dialog-errors").dialog('destroy');
                }

                $scope.error_messages = "<div class='success-message'>"+update_result.messages+"</div>";
            }
        });

    };

    $scope.setErrorStatus = function(data, status) {
        if (!status) status = false;

        data.is_error_j_code              = status;
        data.is_error_branch_cd           = status;
        data.is_error_unit_price          = status;
        data.is_error_quantity            = status;
        data.is_error_delivery_date       = status;
        data.is_error_summary_month       = status;
        data.is_error_j_code_branch       = status;
        data.is_new                       = status;
    }

    $scope.insertRow = function(row) {
        var rowIndex = $scope.gridApi.grid.appScope.gridOptions.data.indexOf(row) + 1;
        var newData = null;
        newData = {
            order_acceptance_status           : order_acceptance_status_val.unapproved,
            id                                : null,
            order_id                          : null,
            j_code                            : null,
            branch_cd                         : null,
            account_id                        : null,
            client_name_order                 : null,
            source_subcontractor_id           : null,
            delivery_date                     : null,
            summary_month                     : null,
            pro_name                          : null,
            content_pro                       : null,
            unit_price_in_order               : null,
            quanity_in_order                  : null,
            sales_in_order                    : null,
            total_cost_in_order_excluding_tax : null,
            amount                            : null,
            can_add                           : angular.copy(row.can_add),
            can_edit                          : angular.copy(row.can_edit),
            can_approve                       : angular.copy(row.can_approve),
            can_separate                      : angular.copy(row.can_separate),
            can_delete                        : angular.copy(row.can_delete),
            is_new                            : true,
            is_editing                        : true,
            is_deleting                       : false,
            is_change					      : false,
        };


        $scope.gridOptions.data.splice(rowIndex, 0, newData);
        if($scope.gridOptions.data.length == (rowIndex+1)){
            $(".ui-grid-viewport").animate({ scrollTop: $(".ui-grid-viewport")[0].scrollHeight}, 'slow');
        }
        else{
            $scope.gridApi.core.scrollTo($scope.gridOptions.data[rowIndex+1],$scope.gridOptions.columnDefs[0]);
        }

        $("html, body").animate({
             scrollTop:postion_return
             },"");
        return true;
    };
    $(function(){
          $(window).scroll(function(){
            var aTop = $('.ui-grid-contents-wrapper').height();
            postion_return = $(this).scrollTop();

          });
    });
    $scope.separateRow = function(row) {

        var rowIndex = $scope.gridApi.grid.appScope.gridOptions.data.indexOf(row) + 1;
        var newData = null;
        newData                         = angular.copy(row);
        newData.order_acceptance_status = order_acceptance_status_val.unapproved;
        newData.id                      = null;
        newData.is_new                  = true;
        newData.is_editing              = true;
        newData.is_separate             = true;
        newData.is_deleting             = false;
        newData.shipping_charge_tax     = null;
        newData.tax_free                = null;
        newData.stamp_tax               = null;
        newData.etc_tax                 = null;
        newData.shipping_charge         = null;
        newData.etc                     = null;
        newData.new_is_separate			= true;
        newData.is_change				= false;
        newData.delivery_date_for_save  = newData.delivery_date;

        if (newData.j_code && newData.branch_cd && newData.order_id) {
            newData.separate_has_j_code_branch_cd = true;
        } else {
            newData.separate_has_j_code_branch_cd = false;
        }
        $scope.gridOptions.data.splice(rowIndex, 0, newData);
    };

    $scope.show_client = function(row) {
        if (!row.entity.client_id) return;
        $.post(
                '/_admin/client/detail',
                {id :row.entity.client_id, dialog_selector: '#clientDialog'},
                function(data) {
                    clientDialog.html(data);
                    clientDialog.dialog("option", "position", {my: "center", at: "center", of: window});
                    clientDialog.dialog('open');
         });
    }

    $scope.get_order_detail = function(j_code, branch_cd, callback) {
        $http({
            url : "/_admin/order_acceptance/ajax_get_order_detail",
            method : 'POST',
//#9748:Start
//            data : {j_code: j_code, branch_cd: branch_cd}
            data : {j_code: j_code, branch_cd: branch_cd, is_dp: true}
//#9748:Start
        })
        .success(function(data, status, headers, config) {
            callback(data);
        });
    }

    $scope.roundNumber = function(number, length) {
        number = parseFloat(number);
        return Math.round(number * Math.pow(10, length)) / Math.pow(10, length);
    };

    var fetchData = function() {

        if ($("#search_result").text()) {
            var count_list_order_acceptance = 0;

                var postFormOrder = $("#postFormOrder").val();
                var data = null;
                data = $.parseJSON($("#search_result").text());
                if (typeof data.is_over_record != 'undefined') {
                     $scope.is_over_record = true;
                     $scope.showWatermark = false;
                     $scope.gridOptions.data = [];
                     return;
                }


                var lengdata = data.length;
                for(var i = 0; i < data.length;i ++) {
                    data[i].is_new							= false;
                    data[i].is_editing					    = false;
                     /* #6784 - Add -E*/

                data[i].is_change						= false;
                count_list_order_acceptance++;
            }

                $scope.gridOptions.data = data;
                console.log($scope.gridOptions.data);
                if(lengdata == 0 && postFormOrder == 1 && post_is_acceptance_input == 0){
                    var jcodeidPOst = $('#jcodeidPOst').val();

                    var brandcode = $('#brandcode').val();
                    var rowIndex = 0;
                    var newData = null;
                    newData = {
                        order_acceptance_status        : order_acceptance_status_val.unapproved,
                        id                             : null,
                        order_id                       : null,
                        j_code                         : jcodeidPOst,
                        branch_cd                      : brandcode,
                        shipping_type_cd               : null,
                        account_id                     : null,
                        client_name_order              : null,
                        client_name                    : null,
                        source_subcontractor_id        : null,
                        delivery_date                  : null,
                        summary_month                  : null,
                        amount                         : null,
                        can_add                        : true,
                        can_edit                       : true,
                        can_approve                    : true,
                        can_separate                   : true,
                        can_delete                     : true,
                        is_new                         : true,
                        is_change					   : false,
                        is_post_is_acceptance_input    : true,
                    };

                    var set_jcode_branch_cd_data = function(data) {
                        newData.is_error_j_code_branch = true;
                        if (data) {

                            var is_change_report_apply = data['is_change_report_apply'];
                            if(is_change_report_apply == 0){
                                var summary_month = null;
                                summary_month = data.delivery_date;
                                summary_month = summary_month.replace(/-/g, '/');
                                newData.account_id               = data.j_account_id;
                                newData.client_name_order        = data.client_name;
                                newData.client_name				 = data.client_name;
                                newData.client_id                = data.client_id;
                                newData.order_id                 = data.id;
                                newData.summary_month            = summary_month.substring(0, 7);
                                newData.pro_name                 =  data.pro_name;
                                newData.content_pro              =  data.content_pro;
                                newData.unit_price_in_order      =  data.unit_price_in_order;
                                newData.quanity_in_order         =  data.quanity_in_order;
                                newData.sales_in_order           =  data.sales_in_order;
                                newData.total_cost_in_order_excluding_tax =  data.total_cost_in_order_excluding_tax;
                                newData.is_error_j_code_branch = false;
                                newData.is_change			   = false;
                                var  $colName2 =['j_code','branch_cd','account_id',
                                                 'source_subcontractor_id',
                                                 'delivery_date',
                                                 'amount',
                                                 'account_id',
                                                 'client_name_order',
                                                'summary_month',
                                                'amount',
                                                'pro_name',
                                                'content_pro',
                                                'unit_price_in_order',
                                                'quanity_in_order',
                                                'sales_in_order',
                                                'total_cost_in_order_excluding_tax'
                                                 ];

                                      $colName2.forEach(function($rowcolName) {
                                                  $rowcolName = $rowcolName.trim();
                                                  newData[$rowcolName +'_has_oldVal'] = newData[$rowcolName];
                                                  newData['is_error_'+$rowcolName] = false;
                                      });

                            }
                        }
                    }

                    $scope.get_order_detail(jcodeidPOst, brandcode, set_jcode_branch_cd_data);

                    $scope.gridOptions.data.splice(rowIndex, 0, newData);
                    count_list_order_acceptance++;
            }

                $scope.showWatermark = false;
                $scope.is_over_record = false;
            if(count_list_order_acceptance == 0){
                $scope.showWatermark = true;
                $scope.is_over_record = false;
            }

        } else {

                $scope.showWatermark = true;
                $scope.is_over_record = false;
            columnDefs = null;
        }
    }

    fetchData();
    // trigger for CTRL + S to do save action
    $(document).bind('keydown', function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            $scope.save();
            return false;
        }
    });

    // warning if there are some data have changed before close tab
    $(window).bind('beforeunload', function() {
        if ($scope.gridApi.rowEdit.getDirtyRows().length){
            return label.confirm_close_windows;
        }
    });


}])
    .filter('mapJCode', function() {
        return function(input, row) {
            if (typeof row.entity == 'undefined') return;
            if (input && input.toString().length > 10) {
                row.entity.is_error_j_code = true;
            } else {
                row.entity.is_error_j_code = false;
            }

            return input;
        }
    })

    .filter('mapBranchCd', function() {
        return function(input, row) {
            if (typeof row.entity == 'undefined') return;
            if (input && input.toString().length > 2) {
                row.entity.is_error_branch_cd = true;
            } else {
                row.entity.is_error_branch_cd = false;
            }

            if (input && input.toString().length == 1) return '0'+input;
            return input;
        }
    })
    .filter('mapAccountId', function() {

        return function(input) {

            if (!input) return '';

            for (i = 0; i < glb_account_options.length; i++) {
                if (glb_account_options[i].id == input) {

                    return glb_account_options[i].name;
                }
            }
        };
    })
    .filter('mapClientName', function() {
        return function(input, row) {
           return input;
        }
    })
    .filter('mapSubcontractorId', function() {
        return function(input,row) {

            var return_null = false;
            if (!input) return '';
            for (i = 0; i < glb_direct_subcontractor_options.length; i++) {
//                console.log(glb_direct_subcontractor_options[i]);
                if (glb_direct_subcontractor_options[i].id == input) {
                    return_null = true;
                    return glb_direct_subcontractor_options[i].abbr_name;

                }
            }
            if(!return_null){
                row.entity.source_subcontractor_id=null;

            }
        }
    })
    .filter('mapUnitPrice', function() {
        return function(input, row) {
            if (typeof row.entity == 'undefined') return;
            if (!input) return '';
            if (isNaN(input)) {
                row.entity.is_error_unit_price = true;
                return input;
            }

            if (input % 1 == 0) {
                var input_return = parseFloat(input);
                row.entity.is_error_unit_price = !IsNumberLessThanTen(input_return);
                input_return =  parseFloat(input)+'.00';
                input_return = formatMoneyAll(input_return,2);
                return input_return;
            }
            var input_return = Math.round(input * Math.pow(10, 2)) / Math.pow(10, 2);
            row.entity.is_error_unit_price = !IsNumberLessThanTen(input_return);
            input_return = formatMoneyAll(input_return,2);
            return input_return;

        }
    })
    .filter('mapQuantity', function() {
        return function(input, row) {
            if (typeof row.entity == 'undefined') return;
            if (!input) return '';
            if (isNaN(input) || input < 0) {
                row.entity.is_error_quantity = true;
                return input;
            }

            row.entity.is_error_quantity = false;

            var input_return = parseFloat(input);
            row.entity.is_error_quantity = !IsNumberLessThanEight(input_return);
            input_return = formatMoneyAll(input_return,0);

            return input_return;
        }
    })
    .filter('mapTotalPriceFormat', function() {
        return function(input, row) {
            if (typeof row.entity == 'undefined') return;
            if (!input) return '';
            if(isNaN(input)){
                console.log("SDsdsdsd");
                return input;
            }
            else{

                var input_return = parseFloat(input);
                input_return = formatMoneyAll(input_return,0);
                return input_return;
            }

        }
    })
//#9960:Start
     .filter('mapTaxType', function() {
        return function(input) {
            if (!input) {
                return '';
            } else {
                for (i = 0; i < tax_division.length; i++) {
                    if (tax_division[i].code == input) {
                        return tax_division[i].name;
                    }
                }
            }
        };
    })
//#9960:End
    .filter('mapDeliveryDate', function() {
        return function(input, row) {
            if (typeof row.entity == 'undefined') return;
            if (!input) {
                row.entity.delivery_date_for_save = '';
                return input;
            }
            if (input == '0000-00-00') {
                row.entity.delivery_date = '';
                row.entity.delivery_date_for_save = '';
                return '';
            }
            result = parse_date(input);
            if (!result) {
                row.entity.delivery_date_for_save = input;
                row.entity.is_error_delivery_date = true;
                return input;
            }
            row.entity.delivery_date_for_save = result;
            row.entity.is_error_delivery_date = false;
            return result;
        }
    })
    .filter('mapSummaryMonth', function() {
        return function(input, row) {
            if (typeof row.entity == 'undefined') return;
            if (!input) return input;
            var result = null;
            result = parse_summary_month(input);
            if (!result) {
                row.entity.summary_month_for_save = input;
                row.entity.is_error_summary_month = true;
                return input;
            }
            row.entity.summary_month_for_save = result;
            row.entity.is_error_summary_month = false;
            return result;
        };
    })
    .filter('mapTaxFree', function() {
        return function(input, row) {
            if (typeof row.entity == 'undefined') return;
            if(input ==""){return '';}
            if(input == 0){ return '0';}
            if (!input) return '';
            if (isNaN(input)) {
                row.entity.is_error_tax_free = true;
                return input;
            }

            row.entity.is_error_tax_free = false;
            row.entity.tax_free = parseFloat(input);
            var input_return = parseFloat(input);
            row.entity.is_error_tax_free = !IsNumberLessThanTen(input_return);
            input_return = formatMoneyAll(input_return,0);
            return input_return;
        };
    })
    .filter('mapOrderAcceptanceStatus', function() {
        return function(input) {

            var status = label.unapproved;
            if (input == order_acceptance_status_val.approved) status = label.approved;
            if (input == order_acceptance_status_val.account_tighten) status = label.account_tighten;
            return status;
        }
    })

    .filter("nl2br", function() {
        return function(data) {
        if (!data) return data;
            return data.replace(/\n\r?/g, '<br />');
        };
    })
    .filter('mapYesNo', function() {
        var mapHash = {
                0 : '未',
                1 : '済'
            };

        return function(input) {
            if (!input){
                return '-';
            } else {
                return mapHash[input];
            }
        };
    })
var parse_summary_month = function(str) {
//#7158:Start
    if (typeof str == 'undefined' || str === null) return '';
//#7158:End
    str = str.trim();

//#7846:Start
    var year, month, delimiter, parts, i = null;
//#7846:End
    year = month = '';

    delimiter = '-';
    if (str.indexOf('/') != -1) delimiter = '/';

    parts = str.split(delimiter);
    if (parts.length == 2) {
        // 2015/12
        year = parts[0].trim();
        if (year.length < 4) {
            year = '2';
            for (i = 0; i < 3 - parts[0].trim().length; i++) {
                year += '0';
            }
            year += parts[0].trim();
        }

        month = parts[1].trim();
        if (month.length > 2 || month < 1 || month > 12) {
            return false;
        }
        month = month.replace(/^\d$/,'0$&');
        return year + '/' + month;
    }
//#7846:Start
//    date_obj = new Date();
//#7846:End
    switch(str.length) {
        case 6:
            year = str.substr(0, 4);
            month = str.substr(4,2);
            if (month < 1 || month > 12) {
                return false
            }
            month = month.replace(/^\d$/,'0$&');
            return year + '/' + month;

            break;

    }
}
function show_error_message(texterror){
     var error = "<div class='error-message'>" +texterror + "</div>";
      $("#dialog-errors").html(error);
      $("#dialog-errors").dialog({
          title: '確認',
          height: 'auto',
          minWidth: 500,
          minHeight: 250,
          maxHeight: 250,
          resizable: true,
          modal: false,
          dialogClass: 'acceptance-dialog fixed-dialog',
          position: { my: "right top+30", at: "right top+30", of: window},
          dragStart: function(event, ui) {
              $('.acceptance-dialog').removeClass('fixed-dialog')
          },
          dragStop: function(event, ui) {
          },
          resizeStart: function( event, ui ) {
              $(this).dialog("option", "maxHeight", false);
          }
      });
}
function IsNumberLessThanEight(input_number){

       if(isNaN(input_number)){
           return true;
       }
       else{
           if (input_number <= -10000000 || input_number >= 10000000) {
               return false;
           }
           else{
                 return true;
           }
       }

}
function IsNumberLessThanTen(input_number){

       if(isNaN(input_number)){
           return true;
       }
       else{
           if (input_number <= -1000000000 || input_number >= 1000000000) {
               return false;
           }
           else{
                 return true;
           }
       }

}
function setAllValidateNumber(rowdata,field){

    var message_error = "";
    var valuefield = rowdata[field];

    if(field == "quantity"){
        var checkNumberField  = IsNumberLessThanEight(valuefield);
        if(!checkNumberField){
            message_error = message_error + lang['error_'+field+'_max_length'] +"<br/>";
        }

   }
   else{
       if((field == "unit_price")||( field == "tax_free")||( field == "shipping_charge_tax")||( field == "shipping_charge")||( field == "stamp_tax")||(field == "etc_tax")||(field =="etc")){

           var checkNumberField  = IsNumberLessThanTen(valuefield);
            if(!checkNumberField){
                message_error = message_error + lang['error_'+field+'_max_length'] +"<br/>";
            }

       }
       else{
           message_error ="";
       }
  }


   if(message_error != ""){
        show_error_message(message_error);
    }
   else{
      try{
          $("#dialog-errors").dialog( "close" );
      }
      catch(err){
          $("#dialog-errors").html("");
      }

    }
}
function backupdate($databk,$rowone){

    $colName =['j_code',
               'branch_cd',
               'account_id',
               'source_subcontractor_id',
               'delivery_date',
               'amount',
               'account_id',
               'client_name_order',
               'summary_month',
               'order_id',
               'client_id',
               'pro_name',
               'content_pro',
               'unit_price_in_order',
               'quanity_in_order',
               'sales_in_order',
               'total_cost_in_order_excluding_tax'
               ];

    if($rowone){

            var new_is_separate = false;
            if($databk.new_is_separate != undefined){
                new_is_separate = $databk.new_is_separate;
            }
            $is_post_is_acceptance_input = false;
            if($databk.is_post_is_acceptance_input){
                $is_post_is_acceptance_input = $databk.is_post_is_acceptance_input;
            }
            if(!new_is_separate && $databk.is_new  && !$is_post_is_acceptance_input){

                $colName.forEach(function($rowcolName) {
                        $rowcolName = $rowcolName.trim();
                        $databk[$rowcolName] = "";
                        $databk['is_error_'+$rowcolName] = false;
                        $databk['is_error_j_code_branch'] = false;
                });

            }
            else{
                $colName.forEach(function($rowcolName) {
                    if($databk[$rowcolName +'_has_oldVal'] != undefined){
                        $rowcolName = $rowcolName.trim();
                        $databk[$rowcolName] = $databk[$rowcolName +'_has_oldVal'];
                        $databk['is_error_'+$rowcolName] = false;
                    }

                    $databk['is_error_j_code_branch'] = false;
                });
                if(($databk['j_code'] != "") && ($databk['branch_cd'] !=  "")){
                    $databk.is_difference_common= false;
                }
                else{
                    $databk.is_difference_common= true;
                }
                $databk.is_editing = false;
            }
    }
    else{
             $databk.forEach(function($rowdatabk) {
                    var new_is_separate = false;
                    if($rowdatabk.new_is_separate != undefined){
                        new_is_separate = $rowdatabk.new_is_separate;
                    }

                    if(!new_is_separate && $rowdatabk.is_new){

                        $colName.forEach(function($rowcolName) {
                                $rowcolName = $rowcolName.trim();
                                $rowdatabk[$rowcolName] = "";
                                $rowdatabk['is_error_'+$rowcolName] = false;
                                $rowdatabk['is_error_j_code_branch'] = false;
                        });

                    }
                    else{

                        $colName.forEach(function($rowcolName) {

                                if($rowdatabk[$rowcolName +'_has_oldVal'] != undefined){
                                    $rowcolName = $rowcolName.trim();
                                    $rowdatabk[$rowcolName] = $rowdatabk[$rowcolName +'_has_oldVal'];
                                    $rowdatabk['is_error_'+$rowcolName] = false;
                                }
                                $rowdatabk['is_error_j_code_branch'] = false;

                        });
                         if(($rowdatabk['j_code'] != "") && ($rowdatabk['branch_cd'] !=  "")){
                             $rowdatabk.is_difference_common= false;
                         }
                         else{
                             $rowdatabk.is_difference_common= true;
                         }

                         $rowdatabk.is_error_j_code_branch = false;
                         $rowdatabk.is_editing = false;

                    }
            });

    }

}
