var app = angular.module('app', [ 'ngTouch', 'ngSanitize', 'ui.grid',
        'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.pagination',
        'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.cellNav',
        'ui.grid.selection' ]);

app
        .controller(
                'MainCtrl',
                [
                        '$scope',
                        '$http',
                        '$q',
                        '$interval',
                        'uiGridConstants',
                        function($scope, $http, $q, $interval, uiGridConstants) {
                            $scope.gridOptions = {
                                enableFiltering : true,
                                enableRowSelection : true,
                                enableSelectAll : true,
                                selectionRowHeaderWidth : 35,
                                paginationPageSizes : [ 5, 10, 20, 30, 50 ],
                                paginationPageSize : 10,
                                useExternalSorting : true,
                                useExternalFiltering : true,
                                totalItems : 100,
                                rowEditWaitInterval : -1
                            };
                            $scope.gridOptions.enableCellEditOnFocus = true;
                            $scope.gridOptions.multiSelect = true;
                            $http.get("/_admin/business_unit/get_all").success(
                                    function(data) {
                                        var arr = [];

                                        for(var x in data){
                                          arr.push({'value': data[x].id, 'label': data[x].name});
                                        }
                                        $scope.gridOptions.columnDefs[2].filter.selectOptions = arr;
                                    });
                            $scope.gridOptions.columnDefs = [
                                    {
                                        name : 'insert',
                                        enableFiltering : false,
                                        enableSorting : false,
                                        enableCellEdit : false,
                                        width : 100,
                                        pinnedLeft : true,
                                        cellTemplate:'<button class="btn primary" ng-click="grid.appScope.insertRow(row.entity)">Insert Row</button>'
                                    },
                                    {
                                        name : 'code',
                                        displayName : jsGlobals[0],
                                        width : 200,
                                        pinnedLeft : true,
                                        cellEditableCondition : function($scope) {
                                            return $scope.row.isSelected
                                        },
                                        sort : {
                                            direction : uiGridConstants.DESC
                                        }
                                    },
                                    {
                                        name : 'business_unit',
                                        displayName : jsGlobals[1],
                                        width : 200,
                                        editableCellTemplate : 'ui-grid/dropdownEditor',
                                        cellFilter : 'mapBusinessUnit:row.entity.business_options',
                                        cellEditableCondition : function($scope) {
                                            return $scope.row.isSelected
                                        },
                                        editDropdownValueLabel : 'name',
                                        editDropdownRowEntityOptionsArrayPath : 'business_options',
                                        filter : {
                                            type : uiGridConstants.filter.SELECT,
                                            selectOptions : []
                                        }
                                    },
                                    {
                                        name : 'name_product',
                                        displayName : jsGlobals[2],
                                        width : 200,
                                        cellEditableCondition : function($scope) {
                                            return $scope.row.isSelected
                                        },
                                        cellClass : function(grid, row, col,
                                                rowRenderIndex, colRenderIndex) {
                                            if (grid.getCellValue(row, col) === 'Velity') {
                                                return 'blue';
                                            }
                                        }
                                    },

                                    {
                                        name : 'is_no_add',
                                        displayName : jsGlobals[3],
                                        width : 1200,
                                        cellEditableCondition : function($scope) {
                                            return $scope.row.isSelected
                                        },
                                        editableCellTemplate : 'ui-grid/dropdownEditor',
                                        cellFilter : 'mapTax',
                                        editDropdownValueLabel : 'value',
                                        editDropdownOptionsArray : [ {
                                            id : 0,
                                            value : 'No'
                                        }, {
                                            id : 1,
                                            value : 'Yes'
                                        } ],
                                        filter : {
                                            type : uiGridConstants.filter.SELECT,
                                            selectOptions : [ {
                                                value : 0,
                                                label : 'No'
                                            }, {
                                                value : 1,
                                                label : 'Yes'
                                            } ]
                                        }
                                    } ];

                            var paginationOptions = {
                                currentPage : 1,
                                pageSize : 10,
                                condition : [ [ "desc", null ], [ null, null ],
                                        [ null, null ], [ null, null ] ],
                                load : true
                            };

                            $scope.insertRow = function(row) {
                                $scope.numPages = Math.ceil($scope.numProducts++ / paginationOptions.pageSize);
                                var indexRow = $scope.gridApi.grid.appScope.gridOptions.data.indexOf(row);
                                $scope.gridOptions.data.splice(
                                                indexRow,
                                                0,
                                                {
                                                    "code" : "",
                                                    "name_product" : "",
                                                    "business_unit" : row.business_unit,
                                                    "business_options" : row.business_options,
                                                    "is_no_add" : row.is_no_add,
                                                });
                                setTimeout(
                                        function() {
                                            $scope.gridApi.rowEdit.setRowsDirty([ $scope.gridOptions.data[indexRow] ]);
                                            $scope.gridApi.selection.selectRow($scope.gridOptions.data[indexRow]);
                                        }, 100);
                            };

                            $(document).bind('keydown', function(e) {
                                if (e.ctrlKey && (e.which == 83)) {
                                    e.preventDefault();
                                    $scope.save();
                                    return false;
                                }
                            });
                            $(document).bind('keypress', function(e) {
                                if (e.ctrlKey && (e.which == 115)) {
                                    e.preventDefault();
                                    return false;
                                }
                            });
                            $(window).bind('beforeunload', function(){
                                if ($scope.gridApi.selection.getSelectedRows().length)
                                    return 'You may lose recent changes by navigating away.';
                            });

                            var getPage = function(paginationOptions) {
                                // console.log(uiGridConstants);
                                $http({
                                    url : '/_admin/product/get_products_by_conditions',
                                    method : 'POST',
                                    data : {
                                        paginationOptions : paginationOptions
                                    }
                                })
                                        .success(
                                                function(data) {
                                                    //console.log(data);
                                                    if (paginationOptions.load) {
                                                        $scope.gridOptions.data = data.products;
                                                        $scope.numProducts = data.numProducts;
                                                        $scope.numPages = Math.ceil($scope.numProducts / paginationOptions.pageSize);
                                                        paginationOptions.load = false;
                                                    } else {
                                                        for (var i = 0; i < data.products.length; i++) {
                                                            if (searchProduct(
                                                                    $scope.gridOptions.data,
                                                                    data.products[i]) == false) {
                                                                $scope.gridOptions.data.push(data.products[i]);
                                                            }
                                                        }
                                                    }
                                                });
                            };

                            getPage(paginationOptions);

                            $scope.currentFocused = "";
                            $scope.save = function(callback, para) {
                                if (!$scope.gridApi.selection.getSelectedRows().length) return;
                                $scope.gridApi.cellNav.scrollToFocus($scope.gridOptions.data[0], $scope.gridOptions.columnDefs[0]);
                                var products = $scope.gridApi.selection.getSelectedRows();
                                for (i = 0; i < products.length; i++) {
                                    products[i].index = $scope.gridOptions.data
                                            .indexOf(products[i]);
                                }

                                $http(
                                        {
                                            url : "/_admin/product/update_list",
                                            method : 'POST',
                                            data : {
                                                products : $scope.gridApi.selection.getSelectedRows()
                                            }
                                        })
                                        .success(
                                                function(data, status, headers, config) {
                                                    // call a fake save
                                                    if (data.error_list) {
                                                        $('.red').show();
                                                        $scope.errors = '';
                                                        $scope.error_list = data.error_list;
                                                        /*
                                                         * for (index in
                                                         * data.error_list) {
                                                         * $scope.errors += '<p class="title">Row ' +
                                                         * (index + 1) + ' has
                                                         * errors: </p>' +
                                                         * data.error_list[index].errors; }
                                                         */

                                                        if ($scope.gridApi.rowEdit.getDirtyRows().length) $scope.gridApi.rowEdit.flushDirtyRows();
                                                        $scope.gridApi.grid.columns[1].filters[0].term = paginationOptions.condition[0][1];
                                                        $scope.gridApi.grid.columns[2].filters[0].term = paginationOptions.condition[1][1];
                                                        $scope.gridApi.grid.columns[3].filters[0].term = paginationOptions.condition[2][1];
                                                        $scope.gridApi.grid.columns[4].filters[0].term = paginationOptions.condition[3][1];

                                                        $scope.gridApi.grid.getColumn('code').sort.direction = paginationOptions.condition[0][0];
                                                        $scope.gridApi.grid.getColumn('business_unit').sort.direction = paginationOptions.condition[1][0];
                                                        $scope.gridApi.grid.getColumn('name_product').sort.direction = paginationOptions.condition[2][0];
                                                        $scope.gridApi.grid.getColumn('is_no_add').sort.direction = paginationOptions.condition[3][0];
                                                    } else {
                                                        $('.red').hide();
                                                        if (typeof callback !== 'undefined')
                                                            callback(para);

                                                        $scope.errors = '';
                                                        var gridRows = $scope.gridApi.rowEdit.getDirtyRows();
                                                        var dataRows = gridRows.map( function( gridRow ) { return gridRow.entity; });
                                                        $scope.gridApi.rowEdit.setRowsClean( dataRows );
                                                        $scope.gridApi.selection.clearSelectedRows();
                                                        alert("Saved!");
                                                    }
                                                }).error(
                                                function(data, status, headers,
                                                        config) {
                                                    console.log(data);
                                                });
                            }

                            $scope.addData = function() {
                                $scope.numPages = Math.ceil($scope.numProducts++ / paginationOptions.pageSize);
                                var gridData = $scope.gridOptions.data;
                                gridData.unshift({
                                            "code" : "",
                                            "name_product" : "",
                                            "business_unit" : $scope.gridOptions.data[0].business_unit,
                                            "business_options" : $scope.gridOptions.data[0].business_options,
                                            "is_no_add" : $scope.gridOptions.data[0].is_no_add,
                                        });

                                setTimeout(
                                        function() {
                                            $scope.gridOptions.data = gridData;

                                            console
                                                    .log($scope.gridOptions.data);
                                            $scope.gridApi.rowEdit.setRowsDirty([ $scope.gridOptions.data[0] ]);
                                            $scope.gridApi.cellNav.scrollToFocus(
                                                            $scope.gridOptions.data[0],
                                                            $scope.gridOptions.columnDefs[1]);
                                            $scope.gridApi.selection.selectRow($scope.gridOptions.data[0]);
                                        }, 100);

                            };

                            $scope.saveRow = function(rowEntity) {
                                // console.log(rowEntity);
                                // create a fake promise - normally you'd use
                                // the promise returned by $http or $resource
                                var promise = $q.defer();
                                $scope.gridApi.rowEdit.setSavePromise(
                                        rowEntity, promise.promise);
                                if (rowEntity.code == ""
                                        || rowEntity.name_product == "") {
                                    promise.reject();
                                }

                                var index = $scope.gridOptions.data.indexOf(rowEntity) + 1;

                                if ($scope.hasOwnProperty('error_list')
                                        && $scope.error_list[index - 1]) {
                                    promise.reject();
                                    $scope.errors += '<p class="title">Row '
                                            + index + ' has errors: </p>'
                                            + $scope.error_list[index - 1];
                                }
                                promise.resolve();;
                            };

                            $scope.removeRows = function() {
                                var txt;
                                var r = confirm(message_sure_to_delete);
                                if (r == true) {
                                    $scope.gridApi.selection
                                            .getSelectedRows()
                                            .forEach(
                                                    function(entry) {
                                                        for (var i = 0; i < $scope.gridOptions.data.length; i++) {
                                                            if ($scope.gridOptions.data[i].id === entry.id) {
                                                                $scope.gridOptions.data.splice(i, 1);
                                                                break;
                                                            }
                                                        }
                                                    });
                                    $http(
                                            {
                                                url : "/_admin/product/remove_list",
                                                method : 'POST',
                                                data : {
                                                    products : $scope.gridApi.selection
                                                            .getSelectedRows()
                                                }
                                            }).success(
                                            function(data, status, headers, config) {
                                                //console.log(data);
                                            }).error(
                                            function(data, status, headers, config) {
                                                console.log(data);
                                            });
                                }

                            };

                            $scope.filter = function(grid) {
                                paginationOptions.currentPage = 1;
                                $scope.gridApi.pagination.seek(1);
                                paginationOptions.condition[0][1] = grid.columns[2].filters[0].term;
                                paginationOptions.condition[1][1] = grid.columns[3].filters[0].term;
                                paginationOptions.condition[2][1] = grid.columns[4].filters[0].term;
                                paginationOptions.condition[3][1] = grid.columns[5].filters[0].term;
                                paginationOptions.load = true;
                                getPage(paginationOptions);
                            }

                            $scope.orderBy = function(sortColumns) {
                                if (sortColumns[0]) {
                                    if (sortColumns[0].name == 'code') {
                                        paginationOptions.condition[0][0] = sortColumns[0].sort.direction;
                                        paginationOptions.condition[1][0] = null;
                                        paginationOptions.condition[2][0] = null;
                                        paginationOptions.condition[3][0] = null;
                                    } else if (sortColumns[0].name == 'business_unit') {
                                        paginationOptions.condition[1][0] = sortColumns[0].sort.direction;
                                        paginationOptions.condition[0][0] = null;
                                        paginationOptions.condition[2][0] = null;
                                        paginationOptions.condition[3][0] = null;
                                    } else if (sortColumns[0].name == 'name_product') {
                                        paginationOptions.condition[2][0] = sortColumns[0].sort.direction;
                                        paginationOptions.condition[0][0] = null;
                                        paginationOptions.condition[1][0] = null;
                                        paginationOptions.condition[3][0] = null;
                                    } else {
                                        paginationOptions.condition[3][0] = sortColumns[0].sort.direction;
                                        paginationOptions.condition[0][0] = null;
                                        paginationOptions.condition[1][0] = null;
                                        paginationOptions.condition[2][0] = null;
                                    }
                                } else {
                                    paginationOptions.condition[3][0] = null;
                                    paginationOptions.condition[0][0] = null;
                                    paginationOptions.condition[1][0] = null;
                                    paginationOptions.condition[2][0] = null;
                                }
                                paginationOptions.load = true;
                                getPage(paginationOptions);
                            }

                            $scope.gridOptions.onRegisterApi = function(gridApi) {
                                $scope.gridApi = gridApi;

                                gridApi.rowEdit.on.saveRow($scope,
                                        $scope.saveRow);

                                gridApi.selection.on
                                        .rowSelectionChanged(
                                                $scope,
                                                function(row) {
                                                    if (row.isSelected) {
                                                        setTimeout(
                                                                function() {
                                                                    $scope.gridApi.cellNav.scrollToFocus(row.entity, $scope.gridOptions.columnDefs[1]);
                                                                }, 100);
                                                    }
                                                });

                                gridApi.pagination.on
                                        .paginationChanged(
                                                $scope,
                                                function(currentPage, pageSize) {
                                                    var numRows = pageSize * currentPage;
                                                    $scope.numPages = Math.ceil($scope.numProducts / pageSize);
                                                    if ($scope.gridOptions.data.length < numRows + pageSize) {
                                                        paginationOptions.currentPage = currentPage;
                                                        paginationOptions.pageSize = pageSize;
                                                        paginationOptions.load = false;
                                                        getPage(paginationOptions);
                                                    }
                                                });
                                gridApi.core.on
                                        .sortChanged(
                                                $scope,
                                                function(grid, sortColumns) {
                                                    if ($scope.gridApi.selection.getSelectedRows().length) {
                                                        $("#save-confirm")
                                                                .dialog(
                                                                        {
                                                                            resizable : false,
                                                                            modal : true,
                                                                            buttons : {
                                                                                "Yes" : function() {
                                                                                    $(this).dialog("close");
                                                                                    $scope.save($scope.orderBy, sortColumns);
                                                                                },
                                                                                "No" : function() {
                                                                                    $(this).dialog("close");
                                                                                    $scope.orderBy(sortColumns);
                                                                                },
                                                                                Cancel : function() {
                                                                                    $(this).dialog("close");
                                                                                    $scope.$apply(function() {
                                                                                                grid.getColumn('code').sort.direction = paginationOptions.condition[0][0];
                                                                                                grid.getColumn('business_unit').sort.direction = paginationOptions.condition[1][0];
                                                                                                grid.getColumn('name_product').sort.direction = paginationOptions.condition[2][0];
                                                                                                grid.getColumn('is_no_add').sort.direction = paginationOptions.condition[3][0];
                                                                                            });
                                                                                }
                                                                            }
                                                                        });
                                                    } else {
                                                        $scope.orderBy(sortColumns);
                                                    }
                                                });
                                gridApi.core.on
                                        .filterChanged(
                                                $scope,
                                                function() {
                                                    var grid = this.grid;
                                                    if ($scope.gridApi.selection.getSelectedRows().length) {
                                                        if ($scope.gridApi.grid.columns[1].filters[0].term != paginationOptions.condition[0][1]
                                                            || $scope.gridApi.grid.columns[2].filters[0].term != paginationOptions.condition[1][1]
                                                            || $scope.gridApi.grid.columns[3].filters[0].term != paginationOptions.condition[2][1]
                                                            || $scope.gridApi.grid.columns[4].filters[0].term != paginationOptions.condition[3][1]) {
                                                            $("#save-confirm").dialog(
                                                                            {
                                                                                resizable : false,
                                                                                modal : true,
                                                                                buttons : {
                                                                                    "Yes" : function() {
                                                                                        $(this).dialog("close");
                                                                                        $scope.save($scope.filter, grid);
                                                                                    },
                                                                                    "No" : function() {
                                                                                        $(this).dialog("close");
                                                                                        $scope.filter(grid);
                                                                                    },
                                                                                    Cancel : function() {
                                                                                        $(this).dialog("close");
                                                                                        $scope.$apply(function() {
                                                                                                    $scope.gridApi.grid.columns[1].filters[0].term = paginationOptions.condition[0][1];
                                                                                                    $scope.gridApi.grid.columns[2].filters[0].term = paginationOptions.condition[1][1];
                                                                                                    $scope.gridApi.grid.columns[3].filters[0].term = paginationOptions.condition[2][1];
                                                                                                    $scope.gridApi.grid.columns[4].filters[0].term = paginationOptions.condition[3][1];
                                                                                                });
                                                                                    }
                                                                                }
                                                                            });
                                                        }
                                                    } else {
                                                        $scope.filter(grid);
                                                    }
                                                });
                            };

                        } ]).filter('mapBusinessUnit', function() {
            return function(input, params) {
                if (!input) {
                    return '';
                } else {
                    for (i = 0; i < params.length; i++) {
                        if (params[i].id == input) {
                            return params[i].name;
                        }
                    }
                }
            };
        }).filter('mapTax', function() {
            var mapHash = {
                0 : 'No',
                1 : 'Yes'
            };

            return function(input) {
                return mapHash[input];
            };
        });

function searchProduct(products, product) {
    for (var i = 0; i < products.length; i++) {
        if (products[i].id == product.id) {
            return true;
        }
    }
    return false;
}