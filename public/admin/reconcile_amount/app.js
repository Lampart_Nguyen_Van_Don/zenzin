$(document).ready(function() {
    var tableInstance,
        config,
        can_save                     = false,
        is_show_etc                  = false,
        is_show_miscellaneous_loss   = false,
        is_show_miscellaneous_income = false,
        prev_month_const             = label.month - 1 < 1 ? 12 : label.month - 1,
        next_month_const             = label.month + 1 > 12 ? 1 : label.month + 1;
    var container = $("#reconcile_table");
    var header_month_data = {
        sales_slip_number: label.sales_slip_number,
        is_checked: label.sales_slip_amount,
        total_amount_tax: label.sales_slip_amount,
    };

    var is_edited = false;
    //tmp#6892: Start 2015/09/28
    var is_applying_filter = false;
    var first_filter       = 1; //当月入金＝当月入金予定分 消し込み
    var sencond_filter     = 2; //前受金＋当月入金＝当月入金予定分＋前月以前売掛金 消し込み
    var third_filter       = 3; //当月入金＝当月入金予定分＋前月以前売掛金 消し込み
    //tmp#6892: End

    // first header
    var grouping_header = {
            s_code : '',
            client_name: '',
            prev_month: {sales_slip_number: prev_month_const+label.prev_month},
            cur_month: {sales_slip_number: (label.month)+label.cur_month},
            next_month: {sales_slip_number: next_month_const+label.next_month},
            prev_ad_receive_payment_amount: '',
            payment_amount: '',
            payment_entry: {
                etc: '',
                fee: '',
                repayment: '',
                miscellaneous_loss: '',
                miscellaneous_income: '',
                offset_next_month_receive: '',
                offset_next_month_ad_receive: '',
                bugyo_next_month_receive: '',
                bugyo_next_month_ad_receive: '',
            },
        };
    // second header
    var header = {
            s_code : label.s_code,
            client_name: label.client_name,
            prev_month: header_month_data,
            cur_month: header_month_data,
            next_month: header_month_data,
            prev_ad_receive_payment_amount: label.prev_receive_payment,
            payment_amount: label.month+label.payment_amount,
            payment_entry: {
                etc: label.etc,
                fee: label.fee,
                repayment: label.repayment,
                miscellaneous_loss: label.miscellaneous_loss,
                miscellaneous_income: label.miscellaneous_income,
                offset_next_month_receive: label.offset_next_month_receive,
                offset_next_month_ad_receive: label.offset_next_month_ad_receive,
                bugyo_next_month_receive: label.bugyo_next_month_receive,
                bugyo_next_month_ad_receive: label.bugyo_next_month_ad_receive,
            },
        };
    // footer title
    var footer_title = {
        prev_ad_receive_payment_amount: prev_month_const+label.total_previous_receive_payment,
        payment_amount: label.month+label.total_current_receive_payment,
        payment_entry: {
//#9696:Start
            etc: label.month + label.total_etc,
            fee: label.month + label.total_fee,
            repayment: label.month + label.total_repayment,
            miscellaneous_loss: label.month + label.total_miscellaneous_loss,
            miscellaneous_income: label.month + label.total_miscellaneous_income,
//#9696:End
            offset_next_month_receive: label.month+label.total_offset_next_month_receive,
            offset_next_month_ad_receive: label.month+label.total_offset_next_month_ad_receive,
            bugyo_next_month_receive: label.month+label.total_bugyo_next_month_receive,
            bugyo_next_month_ad_receive: label.month+label.total_bugyo_next_month_ad_receive,
        }
    };
    var newData = [];
    var mergeCells = [];

    var etc_col_def = {
        data: 'payment_entry.etc',
        editor: 'text',
        type: 'numeric',
//#9696:Start
//            format: '0,0.[00]',
        renderer: formartAmountRenderer,
//#9696:End
        validator: numberLength9Validator,
        width: 100,
    };

    var miscellaneous_loss_col_def = {
        data: 'payment_entry.miscellaneous_loss',
        editor: 'text',
        type: 'numeric',
//#9696:Start
//            format: '0,0.[00]',
        renderer: formartAmountRenderer,
//#9696:End
        validator: numberLength9Validator,
        width: 100,
    };

    var miscellaneous_income_col_def = {
        data: 'payment_entry.miscellaneous_income',
        editor: 'text',
        type: 'numeric',
//#9696:Start
//            format: '0,0.[00]',
        renderer: formartAmountRenderer,
//#9696:End
        validator: numberLength9Validator,
        width: 100,
    };
    var colsDef = [
        {
            data: 's_code',
            editor: false,
            renderer: sCoderenderer,
            width : 70,
//#9218:Start
            readOnly: true,
//#9218:End
        },
        {
            data: 'client_name',
            editor: false,
//#9218:Start
            readOnly: true,
//#9218:End
        },
        {
            data: 'prev_month.sales_slip_number',
            editor: false,
            renderer: saleSlipNumberRenderer,
            width: 120,
//#9218:Start
            readOnly: true,
//#9218:End
        },
        {
            data: 'prev_month.is_checked',
            editor: false,
            renderer: checkbox,
            width: 30,
        },
        {
            data: 'prev_month.total_amount_tax',
            editor: false,
            type: 'numeric',
//#9696:Start
//            format: '0,0.[00]',
            format: '0,0',
//#9696:End
            className: 'ss-amount',
            width: 120,
//#9218:Start
            readOnly: true,
//#9218:End
        },
        {
            data: 'cur_month.sales_slip_number',
            editor: false,
            renderer: saleSlipNumberRenderer,
            width: 120,
//#9218:Start
            readOnly: true,
//#9218:End
        },
        {
            data: 'cur_month.is_checked',
            editor: false,
            renderer: checkbox,
            width: 30,
        },
        {
            data: 'cur_month.total_amount_tax',
            editor: false,
            type: 'numeric',
//#9696:Start
//            format: '0,0.[00]',
            format: '0,0',
//#9696:End
            className: 'ss-amount',
            width: 120,
//#9218:Start
            readOnly: true,
//#9218:End
        },
        {
            data: 'next_month.sales_slip_number',
            editor: false,
            renderer: saleSlipNumberRenderer,
            width: 120,
//#9218:Start
            readOnly: true,
//#9218:End
        },
        {
            data: 'next_month.is_checked',
            editor: false,
            renderer: checkbox,
            width: 30,
        },
        {
            data: 'next_month.total_amount_tax',
            editor: false,
            type: 'numeric',
//#9696:Start
//            format: '0,0.[00]',
            format: '0,0',
//#9696:End
            className: 'ss-amount',
            width: 120,
//#9218:Start
            readOnly: true,
//#9218:End
        },
        {
            data: 'prev_ad_receive_payment_amount',
            editor: false,
            type: 'numeric',
//#9696:Start
//            format: '0,0.[00]',
            format: '0,0',
//#9696:End
            className: 'prev-ad-receive-amount',
            width: 100,
//#9218:Start
            readOnly: true,
//#9218:End
        },
        {
            data: 'payment_amount',
            editor: false,
            renderer: paymentAmountRenderer,
            width: 100,
//#9218:Start
            readOnly: true,
//#9218:End
        },
        {
            data: 'payment_entry.fee',
            editor: 'text',
            type: 'numeric',
//#9696:Start
//            format: '0,0.[00]',
            renderer: formartAmountRenderer,
//#9696:End
            validator: numberLength9Validator,
            width: 100,
        },
        {
            data: 'payment_entry.repayment',
            editor: 'text',
            type: 'numeric',
//#9696:Start
//            format: '0,0.[00]',
            renderer: formartAmountRenderer,
//#9696:End
            validator: numberLength9Validator,
            width: 100,
        },
        {
            data: 'payment_entry.offset_next_month_receive',
            editor: false,
            type: 'numeric',
//#9696:Start
//            format: '0,0.[00]',
            format: '0,0',
//#9696:End
//#9547:Start
//        validator: numberLength9Validator,
//#9547:End
            width: 120,
//#9218:Start
            readOnly: true,
//#9218:End
        },
        {
            data: 'payment_entry.offset_next_month_ad_receive',
            editor: false,
            type: 'numeric',
//#9696:Start
//            format: '0,0.[00]',
            format: '0,0',
//#9696:End
//#9547:Start
//        validator: numberLength9Validator,
//#9547:End
            width: 120,
//#9218:Start
            readOnly: true,
//#9218:End
        },
        {
            data: 'payment_entry.bugyo_next_month_receive',
            editor: false,
            type: 'numeric',
//#9696:Start
//            format: '0,0.[00]',
            format: '0,0',
//#9696:End
//#9547:Start
//        validator: numberLength9Validator,
//#9547:End
            width: 120,
//#9218:Start
            readOnly: true,
//#9218:End
        },
        {
            data: 'payment_entry.bugyo_next_month_ad_receive',
            editor: false,
            type: 'numeric',
//#9696:Start
//            format: '0,0.[00]',
            format: '0,0',
//#9696:End
//#9547:Start
//        validator: numberLength9Validator,
//#9547:End
            width: 120,
//#9218:Start
            readOnly: true,
//#9218:End
        },
    ];

    var config = {
        data: newData,
        rowHeaders: false,
        contextmenu: false,
        fillHandle: false,
        multiSelect: false,
        disableVisualSelection: true,
        copyPaste: false,
        manualColumnResize: true,
        height: 400,
        defaultRowHeight: 100,
        stretchH: 'all',
        fixedRowsTop: 2,
        // autoColumnSize: true,
        // wordWrap: false,
        // colHeaders: [label.s_code, label.client_name, label.sales_slip_number, label.sales_slip_amount, label.sales_slip_number, label.sales_slip_amount, label.sales_slip_number, label.sales_slip_amount, label.prev_receive_payment, label.payment_amount, label.etc, label.fee, label.repayment, label.miscellaneous_loss, label.miscellaneous_income, label.offset_next_month_receive, label.offset_next_month_ad_receive, label.bugyo_next_month_receive, label.bugyo_next_month_ad_receive],
        // colWidths: function(index) {
        // },
        columns: colsDef,
        mergeCells: mergeCells,
        afterGetColHeader: function(col, TH) {
        },
        afterRender: function () {
        },
        beforeRender: function() {
        },
        afterValidate: function (isValid, value, row, prop, source) {
        },
        afterChange: function (changes, source) {
            if (!changes) return;
            is_edited    = true;
            row          = changes[0][0];
            prop         = changes[0][1];
            before_value = changes[0][2];
            value        = changes[0][3];
            // console.log('before change', newData);

            if (['prev_month.is_checked', 'cur_month.is_checked', 'next_month.is_checked'].indexOf(prop) !== -1) {

                var sales_slip_amount = autoCheckReference(row, prop, value);
                // set prev_month_ad_receive_payment
                // returnPrevAdReceive(row, prop, sales_slip_amount);
                // set offset_next_month_receive
                calOffsetNextMonthReceive(row, prop);
            }

            if (['prev_month.is_checked', 'cur_month.is_checked', 'next_month.is_checked', 'payment_entry.etc', 'payment_entry.fee', 'payment_entry.repayment', 'payment_entry.miscellaneous_loss', 'payment_entry.miscellaneous_income'].indexOf(prop) !== -1) {

                // set offset_next_month_ad_receive
                calOffsetNextMonthAdReceive(row, prop, before_value, value, sales_slip_amount);
            }

            if (['payment_entry.etc', 'payment_entry.fee', 'payment_entry.repayment', 'payment_entry.miscellaneous_loss', 'payment_entry.miscellaneous_income'].indexOf(prop) !== -1) {
                // set bugyo_next_month_receive
                calBugyoNextMonthReceive(row);

                // set bugyo_next_month_ad_receive
                calBugyoNextMonthAdReceive(row);

            }

            calFooterSummaryAmount();
            tableInstance.render();
            // console.log('after edit', newData);
        },
        afterRenderer: function(td, row, col, prop, value, cellProperties) {
        },
        cells: function (row, col, prop) {
            last_row_index = newData.length - 1;

            if (row == 0 || row == 1) {
                this.renderer = headerRenderer;
                this.readOnly = true;
            } else if (row == last_row_index || row == last_row_index - 1) {
                this.renderer = footerRenderer;
                this.readOnly = true;
            }

            if (!can_save) {
                this.readOnly = true;
            }

            if (row != 0 && row != 1 && row != last_row_index && row != last_row_index - 1) {
                if (!$.isEmptyObject(newData) && newData[row] && newData[row].color == 0) {
                    return {className : this.className + ' gray-row'}
                }
            }

        },
    }

    container.handsontable(config);
    tableInstance = $('#reconcile_table').handsontable('getInstance');

//#9474:Start
//    var isChecked = function(sales_slip, summary_month, prop, is_uncheck) {
    var isChecked = function(sales_slip, summary_month, prop, is_uncheck, row) {
//#9474:End
//#6892:Start
//      return  sales_slip.default_is_checked == 1 ? true : false;
        if (is_uncheck == true) {
//#9474:Start
            // when applying filter, if amount < 0
            // then all sales slip need to revert the checkbox status
            // if the the sales slip default is checked, but applying filter (auto check all sales slip if passed condition)
            // make amount < 0, then the default checked sales slip still keeping the default checkbox status
            if (is_applying_filter && sales_slip.have_child) autoCheckReference(row, prop, sales_slip.default_is_checked);
//#9474:End
            return sales_slip.default_is_checked == 1 ? true : false;
        }

        if (is_applying_filter == first_filter && prop == 'cur_month') {
//#8353:Start
            // if invoice only has AR part, then dont checked
            if (sales_slip.is_ad_receive_payment == 1 && sales_slip.have_child == 0) return false;
//#8353:End
//#9474:Start
            if (sales_slip.have_child) autoCheckReference(row, prop, true);
//#9474:End
            return true;
        } else if (is_applying_filter == sencond_filter && (prop == 'cur_month' || prop == 'prev_month')) {
//#8353:Start
            if (sales_slip.is_ad_receive_payment == 1 && sales_slip.have_child == 0) return false;
//#8353:End
//#9474:Start
            if (sales_slip.have_child) autoCheckReference(row, prop, true);
//#9474:End
            return true;
        } else if (is_applying_filter == third_filter && (prop == 'cur_month' || prop == 'prev_month')) {
//#8353:Start
            if (sales_slip.is_ad_receive_payment == 1 && sales_slip.have_child == 0) return false;
//#8353:End
//#9474:Start
            if (sales_slip.have_child) autoCheckReference(row, prop, true);
//#9474:End
            return true;
//#9474:Start
        // when apply filter, auto check reference sales slip
        // but maybe the reference part is in different payment date
        // so if the reference sales slip is checked then don't set it back to uncheck
        } else if (is_applying_filter && sales_slip.is_checked) {
            if (sales_slip.is_ad_receive_payment == 1 && sales_slip.have_child == 0) return false;

            return true;
//#9474:End
        } else {
            return sales_slip.default_is_checked == 1 ? true : false;
        }
//#6892:End
    }

    var toFloat = function(data) {
        return data ? parseFloat(data) : 0;
    }

    var toBoolean = function(data) {
        return data == 1 ? true : false;
    }

//#9696:Start
    var setHeaderFooterTitle = function() {
        var summary_month = $('#cbx-summary-month').val();
        var month         = parseInt(summary_month.substr(5,2));
        var year          = parseInt(summary_month.substr(0,4));

        label.month = month;
        label.year  = year;

        prev_month_const = month - 1 < 1 ? 12 : month - 1;
        next_month_const = month + 1 > 12 ? 1 : month + 1;

        grouping_header.prev_month.sales_slip_number = prev_month_const+label.prev_month;
        grouping_header.cur_month.sales_slip_number = (month)+label.cur_month;
        grouping_header.next_month.sales_slip_number = next_month_const+label.next_month;

        header.payment_amount = (month) + label.payment_amount;

        footer_title.prev_ad_receive_payment_amount             = prev_month_const+label.total_previous_receive_payment;
        footer_title.payment_amount                             = (month)+label.total_current_receive_payment;
        footer_title.payment_entry.etc                          = (month)+label.total_etc;
        footer_title.payment_entry.fee                          = (month)+label.total_fee;
        footer_title.payment_entry.repayment                    = (month)+label.total_repayment;
        footer_title.payment_entry.miscellaneous_loss           = (month)+label.total_miscellaneous_loss;
        footer_title.payment_entry.miscellaneous_income         = (month)+label.total_miscellaneous_income;
        footer_title.payment_entry.offset_next_month_receive    = (month)+label.total_offset_next_month_receive;
        footer_title.payment_entry.offset_next_month_ad_receive = (month)+label.total_offset_next_month_ad_receive;
        footer_title.payment_entry.bugyo_next_month_receive     = (month)+label.total_bugyo_next_month_receive;
        footer_title.payment_entry.bugyo_next_month_ad_receive  = (month)+label.total_bugyo_next_month_ad_receive;
    }
//#9696:End

    var fetchData = function(data) {
//#9696:Start
        setHeaderFooterTitle();
        // if there is no data, then set the show the message
        if ($.isEmptyObject(data)) {
            config.data = [grouping_header, header, {s_code: label.no_data, no_data: true}];

            config.mergeCells = [
                {row: 0, col: 2, colspan: 3, rowspan: 1},
                {row: 0, col: 5, colspan: 3, rowspan: 1},
                {row: 0, col: 8, colspan: 3, rowspan: 1},
                {row: 1, col: 3, colspan: 2, rowspan: 1},
                {row: 1, col: 6, colspan: 2, rowspan: 1},
                {row: 1, col: 9, colspan: 2, rowspan: 1},
                {row: 2, col: 0, colspan: colsDef.length, rowspan: 1},
            ];

            tableInstance.mergeCells = new Handsontable.MergeCells(config.mergeCells);
            tableInstance.updateSettings(config);
            return;
        }
//#9696:End
        newData = [grouping_header, header];

        mergeCells = [
            {row: 0, col: 2, colspan: 3, rowspan: 1},
            {row: 0, col: 5, colspan: 3, rowspan: 1},
            {row: 0, col: 8, colspan: 3, rowspan: 1},
            {row: 1, col: 3, colspan: 2, rowspan: 1},
            {row: 1, col: 6, colspan: 2, rowspan: 1},
            {row: 1, col: 9, colspan: 2, rowspan: 1},
        ];

        $.each(data, function(row, client) {
            first_clone = false;
            client_clones = []

            if (client.is_no_sales_slip) {
                clone = $.extend({} ,client);
                clone.prev_month    = [];
                clone.next_month    = [];
                clone.cur_month     = [];
                clone.next_checkbox = false;
                clone.prev_checkbox = false;
                clone.cur_checkbox  = false;
                client_clones.push(clone);
            } else {
                $.each(client.prev_month, function(index, sales_slip) {
                    clone = $.extend({} ,client);
                    clone.prev_month = sales_slip;
                    clone.prev_checkbox = true;
                    if (client.current_month.length > 0) {
                        clone.cur_month = client.current_month[index];
                        if (client.current_month[index]) clone.cur_checkbox = true;
                        if (client.current_month[index]) client.current_month[index].is_cloned = true;
                    }

                    if (client.next_month.length > 0) {
                        clone.next_month = client.next_month[index];
                        if (client.next_month[index]) clone.next_checkbox = true;
                        if (client.next_month[index]) client.next_month[index].is_cloned = true;
                    }


                    client_clones.push(clone);
                });

                $.each(client.current_month, function(index, sales_slip) {
                    if (sales_slip.is_cloned != true) {
                        clone = $.extend({} ,client);
                        clone.cur_month = sales_slip;
                        clone.cur_checkbox = true;
                        client_clones.push(clone);

                        if (client.next_month.length > 0) {
                            clone.next_month = client.next_month[index];
                            if (client.next_month[index]) clone.next_checkbox = true;
                            if (client.next_month[index]) client.next_month[index].is_cloned = true;
                        }
                    }
                });

                $.each(client.next_month, function(index, sales_slip) {
                    if (sales_slip.is_cloned != true) {
                        clone = $.extend({} ,client);
                        clone.next_month = sales_slip;
                        clone.next_checkbox = true;
                        client_clones.push(clone);
                    }
                });
            }
            
            if (!$.isEmptyObject(client_clones)) {
                if (client_clones.length > 1) {
                    mergeCells.push({row: newData.length, col: 0, colspan: 1, rowspan: client_clones.length});
                    mergeCells.push({row: newData.length, col: 1, colspan: 1, rowspan: client_clones.length});
                    mergeCells.push({row: newData.length, col: 11, colspan: 1, rowspan: client_clones.length});
                    mergeCells.push({row: newData.length, col: 12, colspan: 1, rowspan: client_clones.length});
                    mergeCells.push({row: newData.length, col: 13, colspan: 1, rowspan: client_clones.length});
                    mergeCells.push({row: newData.length, col: 14, colspan: 1, rowspan: client_clones.length});
                    mergeCells.push({row: newData.length, col: 15, colspan: 1, rowspan: client_clones.length});
                    mergeCells.push({row: newData.length, col: 16, colspan: 1, rowspan: client_clones.length});
                    mergeCells.push({row: newData.length, col: 17, colspan: 1, rowspan: client_clones.length});
                    mergeCells.push({row: newData.length, col: 18, colspan: 1, rowspan: client_clones.length});
                    mergeCells.push({row: newData.length, col: 19, colspan: 1, rowspan: client_clones.length});
                    mergeCells.push({row: newData.length, col: 20, colspan: 1, rowspan: client_clones.length});
                    mergeCells.push({row: newData.length, col: 21, colspan: 1, rowspan: client_clones.length});
                }

                // add child row index
                children_row = [];
                if (client_clones.length > 1) {
                    for (i = 1; i < client_clones.length; i++) {
                        children_row.push(newData.length + i);
                    }
                }

                // add parent row index
                $.each(client_clones, function(index, obj) {
                    obj.parent_row = newData.length;
                    obj.children_row = children_row;
                });
                $.each(client_clones, function(index, obj) {
                    if (!obj.prev_month) obj.prev_month = {};
                    if (!obj.cur_month) obj.cur_month = {};
                    if (!obj.next_month) obj.next_month = {};
                    newData.push(obj);
                });
            }            
        })

        $.each(newData, function(row, obj) {
            if (row != 0 && row != 1) {
                if (obj.prev_month.length >= 1) obj.prev_month = {};
                if (obj.next_month.length >= 1) obj.next_month = {};
                delete obj.current_month;

                // set checked for checkbox
                if (!$.isEmptyObject(obj.prev_month))  {
                    obj.prev_month.default_is_checked = toBoolean(obj.prev_month.default_is_checked);
//#6892:Start
//                    obj.prev_month.is_checked         = isChecked(obj.prev_month, summary_month, 'prev_month');
//#9474:Start
//                    obj.prev_month.is_checked         = isChecked(obj.prev_month, summary_month, 'prev_month', false);
                    obj.prev_month.is_checked         = isChecked(obj.prev_month, summary_month, 'prev_month', false, row);
//#9474:End
//#6892:End
                    obj.prev_month.total_amount_tax   = toFloat(obj.prev_month.total_amount_tax);
                }

                if (!$.isEmptyObject(obj.cur_month)) {
                    obj.cur_month.default_is_checked = toBoolean(obj.cur_month.default_is_checked);
//#6892:Start
//                    obj.cur_month.is_checked         = isChecked(obj.cur_month, summary_month, 'cur_month');
//#9474:Start
//                    obj.cur_month.is_checked         = isChecked(obj.cur_month, summary_month, 'cur_month', false);
                    obj.cur_month.is_checked         = isChecked(obj.cur_month, summary_month, 'cur_month', false, row);
//#9474:End
//#6892:End
                    obj.cur_month.total_amount_tax   = toFloat(obj.cur_month.total_amount_tax);
                }

                if (!$.isEmptyObject(obj.next_month)) {
                    obj.next_month.default_is_checked = toBoolean(obj.next_month.default_is_checked);
//#6892:Start
//                    obj.next_month.is_checked         = isChecked(obj.next_month, summary_month, 'next_month');
//#9474:Start
//                    obj.next_month.is_checked         = isChecked(obj.next_month, summary_month, 'next_month', false);
                    obj.next_month.is_checked         = isChecked(obj.next_month, summary_month, 'next_month', false, row);
//#9474:End
//#6892:End
                    obj.next_month.total_amount_tax   = toFloat(obj.next_month.total_amount_tax);
                }

                if (obj.parent_row == row) {
                    obj.payment_entry.etc                               = toFloat(obj.payment_entry.etc);
                    obj.payment_entry.fee                               = toFloat(obj.payment_entry.fee);
                    obj.payment_entry.repayment                         = toFloat(obj.payment_entry.repayment);
                    obj.payment_entry.miscellaneous_loss                = toFloat(obj.payment_entry.miscellaneous_loss);
                    obj.payment_entry.miscellaneous_income              = toFloat(obj.payment_entry.miscellaneous_income);
                    obj.payment_entry.offset_next_month_receive         = toFloat(obj.payment_entry.offset_next_month_receive);
                    obj.payment_entry.offset_next_month_ad_receive      = toFloat(obj.payment_entry.offset_next_month_ad_receive);
                    obj.payment_entry.bugyo_next_month_receive          = toFloat(obj.payment_entry.bugyo_next_month_receive);
                    obj.payment_entry.bugyo_next_month_ad_receive       = toFloat(obj.payment_entry.bugyo_next_month_ad_receive);
                    
                    obj.prev_payment_entry.offset_next_month_receive    = toFloat(obj.prev_payment_entry.offset_next_month_receive);
                    obj.prev_payment_entry.offset_next_month_ad_receive = toFloat(obj.prev_payment_entry.offset_next_month_ad_receive);
                    obj.prev_payment_entry.bugyo_next_month_receive     = toFloat(obj.prev_payment_entry.bugyo_next_month_receive);
                    obj.prev_payment_entry.bugyo_next_month_ad_receive  = toFloat(obj.prev_payment_entry.bugyo_next_month_ad_receive);

                    obj.prev_ad_receive_payment_amount = toFloat(obj.prev_ad_receive_payment_amount);
                    obj.payment_amount = toFloat(obj.payment_amount);

//#6892:Start
//                    if (obj.parent_row == row) {
//                        calOffsetNextMonthReceive(row);
//                        calOffsetNextMonthAdReceive(row);
//                        calBugyoNextMonthReceive(row);
//                        calBugyoNextMonthAdReceive(row);
//                    }
//tmp#6892:End
                }
            }
        });
        
//#6892:Start
        $.each(newData, function(row, obj) {
            if (obj.parent_row == row) {
                calOffsetNextMonthReceive(row);
                calOffsetNextMonthAdReceive(row);
                calBugyoNextMonthReceive(row);
                calBugyoNextMonthAdReceive(row);
            }
        });
//#6892:End

        //set row color
        if (!$.isEmptyObject(newData)) {
            newData[2].color = 1;
            $.each(newData, function(index, obj) {
                if (index > 2) {
                    if (obj.s_code == newData[index - 1].s_code) obj.color = newData[index - 1].color;
                    else obj.color = 1 - newData[index - 1].color;
                }
            });
        }


        newData.push(footer_title);
        newData.push({
            prev_ad_receive_payment_amount: 0,
            payment_amount: 0,
            payment_entry: {
                offset_next_month_receive: 0,
                offset_next_month_ad_receive: 0,
                bugyo_next_month_receive: 0,
                bugyo_next_month_ad_receive: 0,
            }
        });

        calFooterSummaryAmount();

        config.data = newData;
        config.mergeCells = mergeCells;
        tableInstance.mergeCells = new Handsontable.MergeCells(config.mergeCells);
        tableInstance.updateSettings(config);
        tableInstance.render();
//#6892:Start
        is_applying_filter = false;
//#6892:End

        // console.log('after fetch data',newData);
    }

    // fetchData(data);

    var returnPrevAdReceive = function(row, prop, sales_slip_amount) {
        parent_row = newData[row].parent_row;
        sales_slip = newData[row][prop.split('.')[0]];
        check_date = sales_slip.default_check_date.substr(0, 7).replace(/-/g, '/');

        if (toBoolean(sales_slip.default_is_checked) && check_date != summary_month) {
            if (sales_slip.is_checked == false) {
                newData[newData[row].parent_row].prev_ad_receive_payment_amount = toFloat(newData[parent_row].prev_ad_receive_payment_amount) + toFloat(sales_slip_amount);
            } else {
                newData[newData[row].parent_row].prev_ad_receive_payment_amount = toFloat(newData[parent_row].prev_ad_receive_payment_amount) - toFloat(sales_slip_amount);
            }
            newData[newData[row].parent_row].is_return_ad_receive = 1;

        }
    }

    var autoCheckReference = function(row, prop, value) {
        if (!prop) return;
        parent_row = newData[row].parent_row;
        month_part = prop.split('.')[0];
        amount = 0;

        newData[row][month_part].is_checked = value;
        sales_slip_number = newData[row][month_part].sales_slip_number;

        if (newData[parent_row].prev_month.sales_slip_number != 0 && newData[parent_row].prev_month.sales_slip_number == sales_slip_number) {
            newData[parent_row].prev_month.is_checked = value;
            amount += toFloat(newData[parent_row].prev_month.total_amount_tax);
        }

        if (newData[parent_row].cur_month.sales_slip_number != 0 && newData[parent_row].cur_month.sales_slip_number == sales_slip_number) {
            newData[parent_row].cur_month.is_checked = value;
            amount += toFloat(newData[parent_row].cur_month.total_amount_tax);
        }

        if (newData[parent_row].next_month.sales_slip_number != 0 && newData[parent_row].next_month.sales_slip_number == sales_slip_number) {
            newData[parent_row].next_month.is_checked = value;
            amount += toFloat(newData[parent_row].next_month.total_amount_tax);
        }

        $.each(newData[parent_row].children_row, function(index, child_row) {
            if (newData[child_row].prev_month.sales_slip_number != 0 && newData[child_row].prev_month.sales_slip_number == sales_slip_number) {
                newData[child_row].prev_month.is_checked = value;
                amount += toFloat(newData[child_row].prev_month.total_amount_tax);
            }

            if (newData[child_row].cur_month.sales_slip_number != 0 && newData[child_row].cur_month.sales_slip_number == sales_slip_number) {
                newData[child_row].cur_month.is_checked = value;
                amount += toFloat(newData[child_row].cur_month.total_amount_tax);
            }

            if (newData[child_row].next_month.sales_slip_number != 0 && newData[child_row].next_month.sales_slip_number == sales_slip_number) {
                newData[child_row].next_month.is_checked = value;
                amount += toFloat(newData[child_row].next_month.total_amount_tax);
            }
        });

        if (amount == 0) {
            amount = newData[row][month_part].total_amount_tax;
        }

        return amount;
    }

    var calOffsetNextMonthReceive = function(row, prop) {
        parent_row = newData[row].parent_row;
        amount = 0;
//#8353:Start
//#7371:Start
        if (!newData[parent_row].prev_month.is_checked) {
//        if (!newData[parent_row].prev_month.is_checked && newData[parent_row].prev_month.is_ad_receive_payment == 0) {
            amount += toFloat(newData[parent_row].prev_month.total_amount_tax);
//#7074:Start
        // }
        } else if (newData[parent_row].prev_month.is_checked && newData[parent_row].prev_month.default_is_checked && toYearMonth(newData[parent_row].prev_month.default_check_date) != summary_month) {
//        } else if (newData[parent_row].prev_month.is_checked && newData[parent_row].prev_month.default_is_checked && toYearMonth(newData[parent_row].prev_month.default_check_date) != summary_month && newData[parent_row].prev_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
            amount += toFloat(newData[parent_row].prev_month.total_amount_tax);
        }
//#7074:End

//#8353:Start
//#7371:Start
        if (!newData[parent_row].cur_month.is_checked) {
//        if (!newData[parent_row].cur_month.is_checked && newData[parent_row].cur_month.is_ad_receive_payment == 0) {
            amount += toFloat(newData[parent_row].cur_month.total_amount_tax);
//#7074:Start
        // }
        } else if (newData[parent_row].cur_month.is_checked && newData[parent_row].cur_month.default_is_checked && toYearMonth(newData[parent_row].cur_month.default_check_date) != summary_month) {
//        } else if (newData[parent_row].cur_month.is_checked && newData[parent_row].cur_month.default_is_checked && toYearMonth(newData[parent_row].cur_month.default_check_date) != summary_month && newData[parent_row].cur_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
            amount += toFloat(newData[parent_row].cur_month.total_amount_tax);
        }
//#7074:End

//#8353:Start
//#7371:Start
        if (!newData[parent_row].next_month.is_checked) {
//        if (!newData[parent_row].next_month.is_checked && newData[parent_row].next_month.is_ad_receive_payment == 0) {
            amount += toFloat(newData[parent_row].next_month.total_amount_tax);
//#7074:Start
        // }
        } else if (newData[parent_row].next_month.is_checked && newData[parent_row].next_month.default_is_checked && toYearMonth(newData[parent_row].next_month.default_check_date) != summary_month) {
//        } else if (newData[parent_row].next_month.is_checked && newData[parent_row].next_month.default_is_checked && toYearMonth(newData[parent_row].next_month.default_check_date) != summary_month && newData[parent_row].next_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
            amount += toFloat(newData[parent_row].next_month.total_amount_tax);
        }
//#7074:End
        
        $.each(newData[parent_row].children_row, function(index, child_row) {
//#8353:Start
//#7371:Start
            if (!newData[child_row].prev_month.is_checked) {
//            if (!newData[child_row].prev_month.is_checked && newData[child_row].prev_month.is_ad_receive_payment == 0) {
                amount += toFloat(newData[child_row].prev_month.total_amount_tax);
//#7074:Start
            // }
            } else if (newData[child_row].prev_month.is_checked && newData[child_row].prev_month.default_is_checked && toYearMonth(newData[child_row].prev_month.default_check_date) != summary_month) {
//            } else if (newData[child_row].prev_month.is_checked && newData[child_row].prev_month.default_is_checked && toYearMonth(newData[child_row].prev_month.default_check_date) != summary_month && newData[child_row].prev_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
                amount += toFloat(newData[child_row].prev_month.total_amount_tax);
            }
//#7074:End

//#8353:Start
//#7371:Start
            if (!newData[child_row].cur_month.is_checked) {
//            if (!newData[child_row].cur_month.is_checked && newData[child_row].cur_month.is_ad_receive_payment == 0) {
                amount += toFloat(newData[child_row].cur_month.total_amount_tax);
//#7074:Start
            // }
            } else if (newData[child_row].cur_month.is_checked && newData[child_row].cur_month.default_is_checked && toYearMonth(newData[child_row].cur_month.default_check_date) != summary_month) {
//            } else if (newData[child_row].cur_month.is_checked && newData[child_row].cur_month.default_is_checked && toYearMonth(newData[child_row].cur_month.default_check_date) != summary_month && newData[child_row].cur_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
                amount += toFloat(newData[child_row].cur_month.total_amount_tax);
            }
//#7074:End

//#8353:Start
//#7371:Start
            if (!newData[child_row].next_month.is_checked) {
//            if (!newData[child_row].next_month.is_checked && newData[child_row].next_month.is_ad_receive_payment == 0) {
                amount += toFloat(newData[child_row].next_month.total_amount_tax);
//#7074:Start
            // }
            } else if (newData[child_row].next_month.is_checked && newData[child_row].next_month.default_is_checked && toYearMonth(newData[child_row].next_month.default_check_date) != summary_month) {
//            } else if (newData[child_row].next_month.is_checked && newData[child_row].next_month.default_is_checked && toYearMonth(newData[child_row].next_month.default_check_date) != summary_month && newData[child_row].next_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
                amount += toFloat(newData[child_row].next_month.total_amount_tax);
            }
//#7074:End
        });
        // console.log('offset next month receive', amount);
        // console.log('====================================');
        newData[parent_row].payment_entry.offset_next_month_receive = amount > 0 ? amount : 0;
    }

    var toYearMonth = function(date) {
        if ($.isEmptyObject(date)) return false;
        return date.substr(0, 7).replace(/-/g, '/');
    }
    
    var checkDefaultChecked = function(row, prop) {
        if (newData[row][prop].default_is_checked == 1 && toYearMonth(newData[row][prop].default_check_date) != summary_month) return true;
        return false;
    }
    
    var calOffsetNextMonthAdReceive = function(row, prop, before_value, value, ss_amount) {
        var parent_row = newData[row].parent_row;

        amount = toFloat(newData[parent_row].prev_ad_receive_payment_amount);
        amount += toFloat(newData[parent_row].payment_amount);
        amount += toFloat(newData[parent_row].payment_entry.etc);
        amount += toFloat(newData[parent_row].payment_entry.fee);
        amount -= toFloat(newData[parent_row].payment_entry.repayment);
        amount += toFloat(newData[parent_row].payment_entry.miscellaneous_loss);
        amount -= toFloat(newData[parent_row].payment_entry.miscellaneous_income);

        if (newData[parent_row].prev_month.is_checked) {
//#7074:Start
            // amount -= toFloat(newData[parent_row].prev_month.total_amount_tax);
//#8353:Start
//#7371:Start
            if (newData[parent_row].prev_month.default_is_checked && toYearMonth(newData[parent_row].prev_month.default_check_date) == summary_month) {
//            if (newData[parent_row].prev_month.default_is_checked && toYearMonth(newData[parent_row].prev_month.default_check_date) == summary_month && newData[parent_row].prev_month.is_ad_receive_payment == 0) {
                amount -= toFloat(newData[parent_row].prev_month.total_amount_tax);
            } else if (!newData[parent_row].prev_month.default_is_checked) {
//            } else if (!newData[parent_row].prev_month.default_is_checked && newData[parent_row].prev_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
                amount -= toFloat(newData[parent_row].prev_month.total_amount_tax);
            }
//#7074:End
        }

        if (newData[parent_row].cur_month.is_checked) {
//#7074:Start
            // amount -= toFloat(newData[parent_row].cur_month.total_amount_tax);
//#8353:Start
//#7371:Start            
            if (newData[parent_row].cur_month.default_is_checked && toYearMonth(newData[parent_row].cur_month.default_check_date) == summary_month) {
//            if (newData[parent_row].cur_month.default_is_checked && toYearMonth(newData[parent_row].cur_month.default_check_date) == summary_month && newData[parent_row].cur_month.is_ad_receive_payment == 0) {
                amount -= toFloat(newData[parent_row].cur_month.total_amount_tax);
            } else if (!newData[parent_row].cur_month.default_is_checked) {
//            } else if (!newData[parent_row].cur_month.default_is_checked && newData[parent_row].cur_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
                amount -= toFloat(newData[parent_row].cur_month.total_amount_tax);
            }
//#7074:End
        }

        if (newData[parent_row].next_month.is_checked) {
//#7074:Start
            // amount -= toFloat(newData[parent_row].next_month.total_amount_tax);
//#8353:Start
//#7371:Start
            if (newData[parent_row].next_month.default_is_checked && toYearMonth(newData[parent_row].next_month.default_check_date) == summary_month) {
//            if (newData[parent_row].next_month.default_is_checked && toYearMonth(newData[parent_row].next_month.default_check_date) == summary_month && newData[parent_row].next_month.is_ad_receive_payment == 0) {
                amount -= toFloat(newData[parent_row].next_month.total_amount_tax);
            } else if (!newData[parent_row].next_month.default_is_checked) {
//            } else if (!newData[parent_row].next_month.default_is_checked && newData[parent_row].next_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
                amount -= toFloat(newData[parent_row].next_month.total_amount_tax);
            }
//#7074:End
        }

        $.each(newData[parent_row].children_row, function(index, child_row) {
            if (newData[child_row].prev_month.is_checked) {
//#7074:Start
                // amount -= toFloat(newData[child_row].prev_month.total_amount_tax);
//#8353:Start
//#7371:Start
                if (newData[child_row].prev_month.default_is_checked && toYearMonth(newData[child_row].prev_month.default_check_date) == summary_month) {
//                if (newData[child_row].prev_month.default_is_checked && toYearMonth(newData[child_row].prev_month.default_check_date) == summary_month && newData[child_row].prev_month.is_ad_receive_payment == 0) {
                    amount -= toFloat(newData[child_row].prev_month.total_amount_tax);
                } else if (!newData[child_row].prev_month.default_is_checked) {
//                } else if (!newData[child_row].prev_month.default_is_checked && newData[child_row].prev_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
                    amount -= toFloat(newData[child_row].prev_month.total_amount_tax);
                }
//#7074:End
            }

            if (newData[child_row].cur_month.is_checked) {
//#7074:Start
                // amount -= toFloat(newData[child_row].cur_month.total_amount_tax);
//#8353:Start
//#7371:Start
                if (newData[child_row].cur_month.default_is_checked && toYearMonth(newData[child_row].cur_month.default_check_date) == summary_month) {
//                if (newData[child_row].cur_month.default_is_checked && toYearMonth(newData[child_row].cur_month.default_check_date) == summary_month && newData[child_row].cur_month.is_ad_receive_payment == 0) {
                    amount -= toFloat(newData[child_row].cur_month.total_amount_tax);
                } else if (!newData[child_row].cur_month.default_is_checked) {
//                } else if (!newData[child_row].cur_month.default_is_checked && newData[child_row].cur_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
                    amount -= toFloat(newData[child_row].cur_month.total_amount_tax);
                }
//#7074:End
            }
            
            if (newData[child_row].next_month.is_checked) {
//#7074:Start
                // amount -= toFloat(newData[child_row].next_month.total_amount_tax);
//#8353:Start
//#7371:Start
                if (newData[child_row].next_month.default_is_checked && toYearMonth(newData[child_row].next_month.default_check_date) == summary_month) {
//                if (newData[child_row].next_month.default_is_checked && toYearMonth(newData[child_row].next_month.default_check_date) == summary_month && newData[child_row].next_month.is_ad_receive_payment == 0) {
                    amount -= toFloat(newData[child_row].next_month.total_amount_tax);
                } else if (!newData[child_row].next_month.default_is_checked) {
//                } else if (!newData[child_row].next_month.default_is_checked && newData[child_row].next_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
                    amount -= toFloat(newData[child_row].next_month.total_amount_tax);
                }
            }
//#7074:End
        });
        
        if (amount < 0) {
//#6892:Start
            if (is_applying_filter) {

                if (!$.isEmptyObject(newData[parent_row].prev_month)) {
//#9474:Start
//                    newData[parent_row].prev_month.is_checked = isChecked(newData[parent_row].prev_month, summary_month, 'prev_month', true);
                    newData[parent_row].prev_month.is_checked = isChecked(newData[parent_row].prev_month, summary_month, 'prev_month', true, parent_row);
//#9474:End
                }

                if (!$.isEmptyObject(newData[parent_row].cur_month)) {
//#9474:Start
//                    newData[parent_row].cur_month.is_checked = isChecked(newData[parent_row].cur_month, summary_month, 'cur_month', true);
                    newData[parent_row].cur_month.is_checked = isChecked(newData[parent_row].cur_month, summary_month, 'cur_month', true, parent_row);
//#9474:End
                }

                $.each(newData[parent_row].children_row, function(index, child_row) {
                    if (!$.isEmptyObject(newData[child_row].prev_month)) {
//#9474:Start
//                        newData[child_row].prev_month.is_checked = isChecked(newData[child_row].prev_month, summary_month, 'prev_month', true);
                        newData[child_row].prev_month.is_checked = isChecked(newData[child_row].prev_month, summary_month, 'prev_month', true, child_row);
//#9474:End
                    }
                    
                    if (!$.isEmptyObject(newData[child_row].cur_month)) {
//#9474:Start
//                        newData[child_row].cur_month.is_checked = isChecked(newData[child_row].cur_month, summary_month, 'cur_month', true);
                        newData[child_row].cur_month.is_checked = isChecked(newData[child_row].cur_month, summary_month, 'cur_month', true, child_row);
//#9474:End
                    }
                });

                calOffsetNextMonthReceive(parent_row, prop);
                calOffsetNextMonthAdReceive(parent_row, prop);
                
            } else {
//#6892 End
                $("#dialog-error").html(label.can_not_blance);
                $("#dialog-error").dialog({
                    title: '確認',
                    modal: true,
                    dialogClass: "reconcile-dialog",
                    width: 410,
                    buttons: [{
                        text: "OK",
                        click: function() {
                        $(this).dialog("close");
                        }
                    }],
                    close: function( event, ui ) {
                        $("#dialog-error").html('');
                    },
                });

                if (['payment_entry.etc', 'payment_entry.fee', 'payment_entry.repayment', 'payment_entry.miscellaneous_loss', 'payment_entry.miscellaneous_income'].indexOf(prop) !== -1) {
                    newData[parent_row]['payment_entry'][prop.split('.')[1]] = before_value;
                }
                autoCheckReference(row, prop, false);
                calOffsetNextMonthReceive(row, prop);
                return;
            }
        }
        // console.log('offset next month ad receive', amount);
        // console.log('====================================');
        newData[parent_row].payment_entry.offset_next_month_ad_receive = amount > 0 ? amount : 0;
    }

    var calBugyoNextMonthReceive = function(row) {
        parent_row = newData[row].parent_row;

        prev_month_bugyo_receive    = toFloat(newData[parent_row].prev_payment_entry.bugyo_next_month_receive);
        prev_month_bugyo_ad_receive = toFloat(newData[parent_row].prev_payment_entry.bugyo_next_month_ad_receive);
        payment_amount              = toFloat(newData[parent_row].payment_amount);
        etc                         = toFloat(newData[parent_row].payment_entry.etc);
        fee                         = toFloat(newData[parent_row].payment_entry.fee);
        repayment                   = toFloat(newData[parent_row].payment_entry.repayment);
        miscellaneous_loss          = toFloat(newData[parent_row].payment_entry.miscellaneous_loss);
        miscellaneous_income        = toFloat(newData[parent_row].payment_entry.miscellaneous_income);
//#9218:Start
//        total_cur_sales_slip_amount = 0;
       total_cur_sales_slip_amount = toFloat(newData[parent_row].total_delivery_date_amount);

//#8353:Start
//#7371:Start
//        if (toYearMonth(newData[parent_row].prev_month.delivery_date) == summary_month) {
//        if (toYearMonth(newData[parent_row].prev_month.delivery_date) == summary_month && newData[parent_row].prev_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
//            total_cur_sales_slip_amount += toFloat(newData[parent_row].prev_month.total_amount_tax);
//        }

//#8353:Start
//#7371:Start
//        if (toYearMonth(newData[parent_row].cur_month.delivery_date) == summary_month) {
//        if (toYearMonth(newData[parent_row].cur_month.delivery_date) == summary_month && newData[parent_row].cur_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
//            total_cur_sales_slip_amount += toFloat(newData[parent_row].cur_month.total_amount_tax);
//        }

//#8353:Start
//#7371:Start
//        if (toYearMonth(newData[parent_row].next_month.delivery_date) == summary_month) {
//        if (toYearMonth(newData[parent_row].next_month.delivery_date) == summary_month && newData[parent_row].next_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
//            total_cur_sales_slip_amount += toFloat(newData[parent_row].next_month.total_amount_tax);
//        }

//        $.each(newData[parent_row].children_row, function(index, child_row) {
//#8353:Start
//#7371:Start
//            if (toYearMonth(newData[child_row].prev_month.delivery_date) == summary_month) {
//            if (toYearMonth(newData[child_row].prev_month.delivery_date) == summary_month && newData[child_row].prev_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
//                total_cur_sales_slip_amount += toFloat(newData[child_row].prev_month.total_amount_tax);
//            }

//#8353:Start
//#7371:Start
//            if (toYearMonth(newData[child_row].cur_month.delivery_date) == summary_month) {
//            if (toYearMonth(newData[child_row].cur_month.delivery_date) == summary_month && newData[child_row].cur_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
//                total_cur_sales_slip_amount += toFloat(newData[child_row].cur_month.total_amount_tax);
//            }

//#8353:Start
//#7371:Start
//            if (toYearMonth(newData[child_row].next_month.delivery_date) == summary_month) {
//            if (toYearMonth(newData[child_row].next_month.delivery_date) == summary_month && newData[child_row].next_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
//                total_cur_sales_slip_amount += toFloat(newData[child_row].next_month.total_amount_tax);
//            }
//        });
//#9218:End
        amount = (prev_month_bugyo_receive + total_cur_sales_slip_amount) - (payment_amount + etc + fee - repayment + miscellaneous_loss - miscellaneous_income + prev_month_bugyo_ad_receive);
        // console.log('prev_month_bugyo_rezceive', prev_month_bugyo_receive);
        // console.log('total_cur_sales_slip_amount', total_cur_sales_slip_amount);
        // console.log('payment_amount', payment_amount);
        // console.log('prev_month_bugyo_ad_receive', prev_month_bugyo_ad_receive);
        // console.log('bugyo next month receive', amount);
        // console.log('====================================');
        newData[parent_row].payment_entry.bugyo_next_month_receive = amount > 0 ? amount : 0;
    }

    var calBugyoNextMonthAdReceive = function(row) {
        parent_row = newData[row].parent_row;

        amount = 0;
        amount += toFloat(newData[parent_row].prev_payment_entry.bugyo_next_month_ad_receive);
        amount += toFloat(newData[parent_row].payment_amount);
        amount += toFloat(newData[parent_row].payment_entry.etc);
        amount += toFloat(newData[parent_row].payment_entry.fee);
        amount -= toFloat(newData[parent_row].payment_entry.repayment);
        amount += toFloat(newData[parent_row].payment_entry.miscellaneous_loss);
        amount -= toFloat(newData[parent_row].payment_entry.miscellaneous_income);
//#9218:Start
//        total_cur_sales_slip_amount = 0;
        total_cur_sales_slip_amount = toFloat(newData[parent_row].total_delivery_date_amount);
//#8353:Start
//#7371:Start
//        if (toYearMonth(newData[parent_row].prev_month.delivery_date) == summary_month) {
//        if (toYearMonth(newData[parent_row].prev_month.delivery_date) == summary_month && newData[parent_row].prev_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
//            total_cur_sales_slip_amount += toFloat(newData[parent_row].prev_month.total_amount_tax);
//        }

//#8353:Start
//#7371:Start
//        if (toYearMonth(newData[parent_row].cur_month.delivery_date) == summary_month) {
//        if (toYearMonth(newData[parent_row].cur_month.delivery_date) == summary_month && newData[parent_row].cur_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
//            total_cur_sales_slip_amount += toFloat(newData[parent_row].cur_month.total_amount_tax);
//        }

//#8353:Start
//#7371:Start
//        if (toYearMonth(newData[parent_row].next_month.delivery_date) == summary_month) {
//        if (toYearMonth(newData[parent_row].next_month.delivery_date) == summary_month && newData[parent_row].next_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
//            total_cur_sales_slip_amount += toFloat(newData[parent_row].next_month.total_amount_tax);
//        }

//        $.each(newData[parent_row].children_row, function(index, child_row) {
//#8353:Start
//#7371:Start
//            if (toYearMonth(newData[child_row].prev_month.delivery_date) == summary_month) {
//            if (toYearMonth(newData[child_row].prev_month.delivery_date) == summary_month && newData[child_row].prev_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
//                total_cur_sales_slip_amount += toFloat(newData[child_row].prev_month.total_amount_tax);
//            }

//#8353:Start
//#7371:Start
//            if (toYearMonth(newData[child_row].cur_month.delivery_date) == summary_month) {
//            if (toYearMonth(newData[child_row].cur_month.delivery_date) == summary_month && newData[child_row].cur_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
//                total_cur_sales_slip_amount += toFloat(newData[child_row].cur_month.total_amount_tax);
//            }

//#8353:Start
//#7371:Start
//            if (toYearMonth(newData[child_row].next_month.delivery_date) == summary_month) {
//            if (toYearMonth(newData[child_row].next_month.delivery_date) == summary_month && newData[child_row].next_month.is_ad_receive_payment == 0) {
//#7371:End
//#8353:End
//                total_cur_sales_slip_amount += toFloat(newData[child_row].next_month.total_amount_tax);
//            }
//        });
//#9218:End
        amount -= total_cur_sales_slip_amount;
        amount -= toFloat(newData[parent_row].prev_payment_entry.bugyo_next_month_receive);
        // console.log('bugyo_next_month_ad_receive',newData[parent_row].prev_payment_entry.bugyo_next_month_ad_receive);
        // console.log('payment_amount',newData[parent_row].payment_amount);
        // console.log('total_cur_sales_slip_amount',total_cur_sales_slip_amount);
        // console.log('bugyo_next_month_receive',newData[parent_row].prev_payment_entry.bugyo_next_month_receive);
        // console.log('bugyo next month ad receive', amount);
        // console.log('====================================');
        newData[parent_row].payment_entry.bugyo_next_month_ad_receive = amount > 0 ? amount : 0;
    }

    var calFooterSummaryAmount = function() {
        var total_previous_ad_receive_payment  = 0;
        var total_payment                      = 0;
        var total_offset_next_month_receive    = 0;
        var total_offset_next_month_ad_receive = 0;
        var total_bugyo_next_month_receive     = 0;
        var total_bugyo_next_month_ad_receive  = 0;
//#9696:Start
        var total_etc                          = 0;
        var total_fee                          = 0;
        var total_repayment                    = 0;
        var total_miscellaneous_loss           = 0;
        var total_miscellaneous_income         = 0;
//#9696:End

        $.each(newData, function(row, obj) {
            if (obj.parent_row == row) {
                total_previous_ad_receive_payment  += toFloat(obj.prev_ad_receive_payment_amount);
                total_payment                      += toFloat(obj.payment_amount);
                total_offset_next_month_receive    += toFloat(obj.payment_entry.offset_next_month_receive);
                total_offset_next_month_ad_receive += toFloat(obj.payment_entry.offset_next_month_ad_receive);
                total_bugyo_next_month_receive     += toFloat(obj.payment_entry.bugyo_next_month_receive);
                total_bugyo_next_month_ad_receive  += toFloat(obj.payment_entry.bugyo_next_month_ad_receive);
//#9696:Start
                total_etc                          += toFloat(obj.payment_entry.etc);
                total_fee                          += toFloat(obj.payment_entry.fee);
                total_repayment                    += toFloat(obj.payment_entry.repayment);
                total_miscellaneous_loss           += toFloat(obj.payment_entry.miscellaneous_loss);
                total_miscellaneous_income         += toFloat(obj.payment_entry.miscellaneous_income);
//#9696:End
            }
        });

        newData[newData.length - 1].prev_ad_receive_payment_amount             = total_previous_ad_receive_payment;
        newData[newData.length - 1].payment_amount                             = total_payment;
        newData[newData.length - 1].payment_entry.offset_next_month_receive    = total_offset_next_month_receive;
        newData[newData.length - 1].payment_entry.offset_next_month_ad_receive = total_offset_next_month_ad_receive;
        newData[newData.length - 1].payment_entry.bugyo_next_month_receive     = total_bugyo_next_month_receive;
        newData[newData.length - 1].payment_entry.bugyo_next_month_ad_receive  = total_bugyo_next_month_ad_receive;
//#9696:Start
        newData[newData.length - 1].payment_entry.etc                          = toFloat(total_etc);
        newData[newData.length - 1].payment_entry.fee                          = toFloat(total_fee);
        newData[newData.length - 1].payment_entry.repayment                    = toFloat(total_repayment);
        newData[newData.length - 1].payment_entry.miscellaneous_loss           = toFloat(total_miscellaneous_loss);
        newData[newData.length - 1].payment_entry.miscellaneous_income         = toFloat(total_miscellaneous_income);
//#9696:Enc
    }

    function displayPositive(instance, td, row, col, prop, value, cellProperties) {
        td.className += ' htNumeric';
        if (newData[row].color == 0) {
            td.className += ' gray-row';
        }
        td.innerHTML = toFloat(value) > 0 ? numeral(value).format('0,0.[00]') : 0;
    }

    function sCoderenderer(instance, td, row, col, prop, value, cellProperties) {
        if (config.data[row].no_data == true) {
            td.className = 'htCenter htMiddle';
        }
        Handsontable.TextCell.renderer.apply(this, arguments);

        // $('#reconcile_table').handsontable('getInstance').render();
    }

    function checkbox(instance, td, row, col, prop, value, cellProperties) {
        if ($.isEmptyObject(newData)) return;

        if (can_save == false) {
            value = '';
            Handsontable.TextCell.renderer.apply(this, arguments);
            return;
        }

        show = false;
        switch(prop) {
            case 'prev_month.is_checked':
                if (newData[row].prev_checkbox == true) {
                    show = true;
                    if (newData[row].prev_month.is_ad_receive_payment == 1 && newData[row].prev_month.have_child == 0) {
                        show = false;
                    }
                }
                break;
            case 'cur_month.is_checked':
                if (newData[row].cur_checkbox == true) {
                    show = true;
                    if (newData[row].cur_month.is_ad_receive_payment == 1 && newData[row].cur_month.have_child == 0) {
                        show = false;
                    }
                }
                break;
            case 'next_month.is_checked':
                if (newData[row].next_checkbox == true) {
                    show = true;
                    if (newData[row].next_month.is_ad_receive_payment == 1 && newData[row].next_month.have_child == 0) {
                        show = false;
                    }
                }
                break;
        }
        if (show == true) {
            Handsontable.cellTypes['checkbox'].renderer.apply(this, arguments);
            td.className = 'htCenter htMiddle ss-checkbox';
        } else {
            value = '';
            td.className = 'htCenter htMiddle ss-checkbox';
            Handsontable.TextCell.renderer.apply(this, arguments);
        }

        if (newData[row].color == 0) {
            td.className += ' gray-row ss-checkbox';
        }

        return td;
        // $('#reconcile_table').handsontable('getInstance').render();
    }

    function headerRenderer(instance, td, row, col, prop, value, cellProperties) {
        if (row == 1) {
            Handsontable.renderers.HtmlRenderer.apply(this, arguments);
            td.className = 'htCenter htMiddle header-color';
        } else {
            Handsontable.renderers.HtmlRenderer.apply(this, arguments);
            if (col == 2 || col == 5 || col == 8) {
                td.className = 'htCenter htMiddle header-color';
            } else {
                if (col == 1) {
                    td.className = 'htCenter htMiddle grouping-header border-right';
                } else {
                    td.className = 'htCenter htMiddle grouping-header';
                }
            }
        }
    }

    function footerRenderer(instance, td, row, col, prop, value, cellProperties) {
        last_row_index = newData.length - 1;

        if (row == last_row_index) {
//#9696:Start
//            if (value) value = numeral(value).format('0,0.[00]');
            if (value) value = addCommas(truncateDecimal(value, 2));
//#9696:End
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.className = 'htCenter htMiddle grouping-footer';
            if (prop == 'payment_entry.offset_next_month_ad_receive' || prop == 'payment_entry.bugyo_next_month_receive' || prop == 'payment_entry.bugyo_next_month_ad_receive' || prop == 'payment_amount') {
                td.className = 'htNumeric htMiddle border-top border-right';
            }
//#9696:Start
//            if (prop == 'prev_ad_receive_payment_amount' || prop == 'payment_entry.offset_next_month_receive') {
            if (prop == 'prev_ad_receive_payment_amount') {
//#9696:End
                td.className = 'htNumeric htMiddle border-top border-left';
            }

//#9696:Start
            if (prop == 'payment_entry.offset_next_month_receive' || prop == 'payment_entry.etc' || prop == 'payment_entry.repayment' || prop == 'payment_entry.miscellaneous_loss' || prop == 'payment_entry.miscellaneous_income') {
                td.className = 'htNumeric htMiddle border-top';
            }

            if (prop == 'payment_entry.fee') {
                td.className = 'htNumeric htMiddle border-top border-right';
            }
//#9696:End
        } else if (row == last_row_index - 1) {
            if (value == null) value = '';
            td.className = 'htCenter htMiddle grouping-footer';
            Handsontable.renderers.HtmlRenderer.apply(this, arguments);
        } else {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
        }
    }

    function saleSlipNumberRenderer(instance, td, row, col, prop, value, cellProperties) {
        if ($.isEmptyObject(newData)) return;
        
        switch(prop) {
            case 'prev_month.sales_slip_number':
                if (!$.isEmptyObject(newData[row].prev_month) && value != null) {
                    if (newData[row].prev_month.is_past_data == 1) {
                        prefix = '';
                    } else {
                        prefix = newData[row].prev_month.is_ad_receive_payment == 1 ? 'AR' : 'BN';
                    }
                    value = prefix + value;
                } else {
                    value = '';
                }
                break;
            case 'cur_month.sales_slip_number':
                if (!$.isEmptyObject(newData[row].cur_month) && value != null) {
                    prefix = newData[row].cur_month.is_ad_receive_payment == 1 ? 'AR' : 'BN';
                    value = prefix + value;
                } else {
                    value = '';
                }
                break;
            case 'next_month.sales_slip_number':
                if (!$.isEmptyObject(newData[row].next_month) && value != null) {
                    prefix = newData[row].next_month.is_ad_receive_payment == 1 ? 'AR' : 'BN';
                    value = prefix + value;
                } else {
                    value = '';
                }
                break;
        }

        if (newData[row].color == 0) {
            td.className += ' gray-row';
        }
        td.innerHTML = value;
    }

    function paymentAmountRenderer(instance, td, row, col, prop, value, cellProperties) {
        if ($.isEmptyObject(newData)) return;

//#9696:Start
//        td.innerHTML = '<a class="client_date" data-row="'+row+'" data-id="'+config.data[row].client_id+'-'+label.month+'-'+label.year+'">'+numeral(value).format('0,0.[00]')+'</a>'
        td.innerHTML = '<a class="client_date" data-row="'+row+'" data-id="'+config.data[row].client_id+'-'+label.month+'-'+label.year+'">'+addCommas(truncateDecimal(value, 2))+'</a>'
//#9696:End
        td.className += ' htNumeric';
        if (newData[row].color == 0) {
            td.className += ' gray-row';
        }
        return td;
    }

//#9696:Start
    function formartAmountRenderer(instance, td, row, col, prop, value, cellProperties) {
        if (!value || value == "") value = 0;
        
        td.innerHTML = addCommas(truncateDecimal(value, 2));
        td.className += ' htNumeric ';
        if (!$.isNumeric(value)) {
            td.className += ' htInvalid ';
        }

        if (newData[row].color == 0) {
            td.className += ' gray-row';
        }
        return td;
    }
//#9696:End

    function numberLength9Validator(value, callback) {
        if (value >= 0 && value < 1000000000) {
            callback(true);
        } else {
            callback(false);
        }
    }

    // <------------------------------------------------------------ CLICK ENTER TO SEARCH ----------------------------------------------------------->
    $(".search-reconcile-table").keypress(function (e) {
         var key = e.which;
         if(key == 13)  // the enter key code
          {
             e.preventDefault();

             var summary_month = $("select[name='summary_month']").is(':focus');

             var remaining_scrub = $("input[name='remaining_scrub']").is(':focus');

             var s_code = $("input[name='s_code']").is(':focus');

             if((summary_month | remaining_scrub | s_code)) {
                 $(".btn-search").click();
             }
          }
    });

    $('#slide-left-btn').click(function () {
        setTimeout(function () {
            container.handsontable('render');
        }, 500);
    });

    $(".show-etc").on('click', function(event) {
        if (is_show_etc == true) return;
        is_show_etc = true;

        $.each(colsDef, function(index, col) {
            if (col.data == 'payment_amount') col_index = index + 1;
        });
        colsDef.splice(col_index, 0, etc_col_def);
        config.colsDef = colsDef;
        tableInstance.updateSettings(config);
        tableInstance.render();
    });

    $(".show-miscellaneous-loss").on('click', function(event) {
        if (is_show_miscellaneous_loss == true) return;
        is_show_miscellaneous_loss = true;

        $.each(colsDef, function(index, col) {
            if (col.data == 'payment_entry.repayment') col_index = index + 1;
        });

        colsDef.splice(col_index, 0, miscellaneous_loss_col_def);
        config.colsDef = colsDef;
        tableInstance.updateSettings(config);
        tableInstance.render();
    });

    $(".show-miscellaneous-income").on('click', function(event) {
        if (is_show_miscellaneous_income == true) return;
        is_show_miscellaneous_income = true;

        $.each(colsDef, function(index, col) {
            if (col.data == 'payment_entry.repayment') col_index = index + 1;
        });

        if (is_show_miscellaneous_loss) col_index += 1;

        colsDef.splice(col_index, 0, miscellaneous_income_col_def);
        config.colsDef = colsDef;
        tableInstance.updateSettings(config);
        tableInstance.render();
    });

    $(".btn-1").on('click', function(event) {
        $(".messages").html('');
        /*tmp#6892: Start 2015/09/28
        search(1);
        tmp#6892: End*/
        search(first_filter);
        is_applying_filter = first_filter;
    });

    $(".btn-2").on('click', function(event) {
        $(".messages").html('');
        /*tmp#6892: Start 2015/09/28
        search(2);
        tmp#6892: End*/
        search(sencond_filter);
        is_applying_filter = sencond_filter;
    });

    $(".btn-3").on('click', function(event) {
        $(".messages").html('');
        /*tmp#6892: Start 2015/09/28
        search(3);
        tmp#6892: End*/
        search(third_filter);
        is_applying_filter = third_filter;
    });

    $(".btn-save").on('click', function(event) {
        save();
    });

    $(".btn-download-csv").on('click', function(event) {
        s_code         = $("#search-form input[type=text]").val();
        remaining_srub = $("#search-form input[type=checkbox]").is(':checked') ? 1 : 0;

        dom =   "<form method='post' action='/_admin/reconcile_amount/export_csv' style='display:none'>"+
                    "<input type='hidden' name='summary_month' value='"+summary_month+"'>" +
                    "<input type='hidden' name='s_code' value='"+s_code+"'>" +
                    "<input type='hidden' name='remaining_srub' value='"+remaining_srub+"'>" +
                "<form>";
        $(dom).appendTo('body').submit();

    });

    $(".btn-search").on('click', function(event) {
        $(".messages").html('');
        search();
        is_applying_filter = false;
    });

    var search = function(action) {
        $('.btn-search').addClass('loading icon-spin');
        summary_month   = $('#cbx-summary-month').val();
        s_code          = $('#ip-s-code').val();
        remaining_scrub = $('#ip-remaining-scrub').is(":checked") ? 1 : 0;

        $.ajax({
            url: '/_admin/reconcile_amount/get_data',
            type: 'POST',
            data: {summary_month: summary_month, s_code: s_code, remaining_scrub: remaining_scrub, action: action},
        })
        .done(function(data) {
            $('.btn-search').removeClass('loading icon-spin');
            can_save = data.can_save;
            $(".btn-save, .btn-download-csv").hide();
            if (can_save) $(".btn-save, .btn-download-csv").show();
//#9696:Start
//            if ($.isEmptyObject(data.reconcile_datas)) {
//                config.data = [grouping_header, header, {s_code: label.no_data, no_data: true}];
//
//                config.mergeCells = [
//                    {row: 0, col: 2, colspan: 3, rowspan: 1},
//                    {row: 0, col: 5, colspan: 3, rowspan: 1},
//                    {row: 0, col: 8, colspan: 3, rowspan: 1},
//                    {row: 1, col: 3, colspan: 2, rowspan: 1},
//                    {row: 1, col: 6, colspan: 2, rowspan: 1},
//                    {row: 1, col: 9, colspan: 2, rowspan: 1},
//                    {row: 2, col: 0, colspan: colsDef.length, rowspan: 1},
//                ];

//                tableInstance.mergeCells = new Handsontable.MergeCells(config.mergeCells);
//                tableInstance.updateSettings(config);
//
//                return;
//            }

//            month = parseInt(summary_month.substr(5,2));
//            year  = parseInt(summary_month.substr(0,4));
//            label.month = month;
//            label.year = year;

//            prev_month_const = month - 1 < 1 ? 12 : month - 1;
//            next_month_const = month + 1 > 12 ? 1 : month + 1;

//            grouping_header.prev_month.sales_slip_number = prev_month_const+label.prev_month;
//            grouping_header.cur_month.sales_slip_number = (month)+label.cur_month;
//            grouping_header.next_month.sales_slip_number = next_month_const+label.next_month;

//            header.payment_amount = (month) + label.payment_amount;

//            footer_title.prev_ad_receive_payment_amount = prev_month_const+label.total_previous_receive_payment;
//            footer_title.payment_amount = (month)+label.total_current_receive_payment;
//            footer_title.payment_entry.offset_next_month_receive =  (month)+label.total_offset_next_month_receive,
//            footer_title.payment_entry.offset_next_month_ad_receive =  (month)+label.total_offset_next_month_ad_receive,
//            footer_title.payment_entry.bugyo_next_month_receive =  (month)+label.total_bugyo_next_month_receive,
//            footer_title.payment_entry.bugyo_next_month_ad_receive =  (month)+label.total_bugyo_next_month_ad_receive,
//#9696:End
            fetchData(data.reconcile_datas);
        });
    }
    search();


    var save = function() {
//#9218:Start
        if (!can_save) return;
//#9218:End

        $(".messages").html('');
        $.ajax({
            url: '/_admin/reconcile_amount/update_clearance',
            type: 'POST',
            data: {data: JSON.stringify(newData)},
        })
        .done(function(data) {
            if (!data.status) {
                if (typeof data.messages != 'object') {
                    $(".messages").html('<span class="error-message">'+data.messages+'</span>');
                    return;
                }

                
                group_mess = '';
                $.each(data.messages, function(row, error) {
                    flag = true;
                    messages = '';
                    $.each(error, function(index, mess) {
                        if (flag) {
                            group_mess += '<div class="row-error-icon">'+row;
                            flag = false;
                        }

                        messages += '<p class="import-error"><span class="validation-error">'+mess+'</span></p>'
                    });
                    group_mess += messages +'</div>';

                });
                $("#dialog-error").html('<div class="error-message">'+group_mess+'</div>');
                $("#dialog-error").dialog({
                    title: '確認',
                    height: 'auto',
                    minWidth: 500,
                    minHeight: 250,
                    maxHeight: 250,
                    resizable: true,
                    modal: false,
                    dialogClass: 'reconcile-dialog fixed-dialog',
                    position: { my: "right top+30", at: "right top+30", of: window},
                    dragStart: function(event, ui) {
                        $('.reconcile-dialog').removeClass('fixed-dialog')
                    },
                    resizeStart: function( event, ui ) {
                        $(this).dialog("option", "maxHeight", false);
                    },
                    close: function( event, ui ) {
                        $("#dialog-error").html('');
                    },
                });

                return;
            }

            $(".messages").html('<span class="success-message">'+data.messages+'</span>');
            $.each(data.payment_entry_ids, function(index, entry) {
                 newData[entry.row].payment_entry.id = entry.id;
            });
        });
        tableInstance.render();
    }


    var $dialog = $('#dialog-detail');

    $dialog.dialog({
        title: '入金情報参照画面兼入金調整画面',
        autoOpen : false,
        modal : true,
        open: function(){
        },
        beforeClose: function( event, ui ) {
            var close = close_confirm();
            return close;
        },
        close : function() {
            search();
            form_changed = false;
        }
    });


    $(document).on('click', ".client_date", function(event) {
        var id              = $(this).data('id');
        var temp            = id.split('-');
        var view_detail_row = $(this).data('row');

        $("#dialog-detail").load('/_admin/reconcile_amount/detail', {
            id: temp[0],
            month:temp[1],
            year: temp[2],

            redirect_form_post: redirect_data()
            }, function() {
                $dialog.dialog('option', 'width', 'auto');     //1000
                $dialog.dialog('option', 'height', 'auto');    //600
                $dialog.dialog("open");
            }
        );
    });

    $("#search-form").submit(function(event) {
        event.preventDefault();
    });

    $(document).bind('keydown', function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            save();
            return false;
        }
    });

    // warning if there are some data have changed before close tab
    $(window).bind('beforeunload', function() {
        if (is_edited) {
            return 'このページから移動しますか？ 入力したデータは保存されません。';
        }
    });
});
