$(function () {
    var container = $('#sales_form_table');

    // Merge old_sales_slip_detail into sales_slip_detail for viewing purpose
    // row that originated from old_sales_slip_detail will have the property 'exclude' = true
    var add_edit_data = sales_slip_detail;
    for (var i = 0; i < old_sales_slip_detail.length; i++) {
        for (var j = 0; j < add_edit_data.length; j++) {
            if (add_edit_data[j].ad_receive_sales_slip_detail_id == old_sales_slip_detail[i].ad_receive_sales_slip_detail_id) {
                old_sales_slip_detail[i].order_id = add_edit_data[j].order_id;
                add_edit_data.splice(j, 0, cloneObj(old_sales_slip_detail[i]));
                add_edit_data[j].exclude = true;
                break;
            }
        }
    }

//#8149:Start
    if ($('.btn-slEditSalesSlip').length) {
        add_edit_data.sort(function (a, b) {
            return a.sequence - b.sequence;
        });
    }
//#8149:End

    var sales_slip_add_edit_config = {
        data: add_edit_data,
        rowHeaders: false,
        contextmenu: false,
        fillHandle: false,
        multiSelect: false,
        manualColumnResize: true,
        copyPaste: false,
        disableVisualSelection: true,
        height: 160,
        //width: 1000,
        stretchH: 'all',
        colHeaders: [
            label.lbl_j_code,
            label.lbl_branch,
            label.lbl_delivery_date,
            label.lbl_product,
            label.lbl_contents,
            label.lbl_tax,
            label.lbl_quantity,
            label.lbl_unit_price,
            label.lbl_money_amount,
            label.lbl_action,
            label.lbl_move
        ],
        columns: [
            {
                data: 'j_code',
                editor: false,
                readOnly: true,
                width: 70,
                renderer: function(instance, td, row, col, prop, value, cellProperties) {
                    //td.innerHTML = value;
//#7152:Start
                    //td.innerHTML = '<a href="javascript:void(0)" ng-click="loadDetail(' + sales_slip_add_edit_config.data[row].order_id + ')" style="color:#03C">' + value + '</a>';
                    td.innerHTML = '<a ng-click="loadDetail(' + sales_slip_add_edit_config.data[row].order_id + ')">' + value + '</a>';
//#7152:End
                    return td;
                }
            },
            {
                data: 'branch_cd',
                editor: false,
                readOnly: true,
                width: 25
            },
            {
                data: 'delivery_date',
                editor: false,
                readOnly: true,
                width: 80,
                renderer: function (instance, td, row, col, prop, value, cellProperties) {
                    value = value.replace(/-/g, '/');
                    Handsontable.TextCell.renderer.apply(this, arguments);
                }
            },
            {
                data: 'product_name',
                width: 110
            },
            {
                data: 'contents',
                width: 130
            },
            {
                data: 'tax_type',
                renderer: function(instance, td, row, col, prop, value, cellProperties) {
                    value = tax_options_obj[value];
                    Handsontable.TextCell.renderer.apply(this, arguments);
                },
                readOnly: true,
                width: 20,
                editor: false,
            },
            {
                data: 'quantity',
                editor: false,
                readOnly: true,
                type: 'numeric',
                format: '0,0',
                width: 65,
            },
            {
                data: 'unit_price',
                editor: false,
                readOnly: true,
                type: 'numeric',
                format: '0,0.00',
                width: 105,
            },
            {
                data: 'amount',
                editor: false,
                readOnly: true,
                type: 'numeric',
                format: '0,0',
                width: 125,
            },
            {
                data: '',
                editor: false,
                renderer: function(instance, td, row, col, prop, value, cellProperties) {
                    td.innerHTML = '';
                    var count = 0;
                    $.each(sales_slip_add_edit_config.data, function (k, v) {
                        if (!v.hasOwnProperty('exclude')) {
                            count++;
                        }
                    });
                    if (count > 1 && !sales_slip_add_edit_config.data[row].hasOwnProperty('exclude')) {
                        td.innerHTML = '<div style="margin-top: 5px"><a href="javascript:void(0)" class="btn-small btn-green btn-slRemove">' + label.btn_exclude + '</a></div>';
                    }
                    return td;
                },
                width: 60
            },
            {
                data: '',
                editor: false,
                renderer: function(instance, td, row, col, prop, value, cellProperties) {
                    td.innerHTML = '<div style="margin-top: 5px" >';

                    if (row < sales_slip_add_edit_config.data.length - 1) {
                        td.innerHTML += '<a href="javascript:void(0)" class="btn-small btn-green btn-slMoveDown"> ↓ </a>';
                    }

                    if (row > 0) {
                        td.innerHTML += '<a href="javascript:void(0)" class="btn-small btn-green btn-slMoveUp"> ↑ </a>';
                    }

                    td.innerHTML += '</div>';
                    return td;
                },
                width: 80
            }
        ],
        afterRender: function (isForced) {
            angular.element(document).injector().invoke(function($compile) {
                var scope = angular.element(container).scope();
                $compile(container)(scope);
                scope.$apply();
            });
            $('#sales_form_table .ht_master .wtHolder .wtHider .wtSpreader').width('100%');
            $('#sales_form_table .ht_master .wtHolder .wtHider .wtSpreader .htCore').width('100%');
            $('#sales_form_table .ht_clone_top').hide();
        },
//#7791:Start
        afterChange: function (changes, source) {
            if (source == 'edit') {
                for (var i = 0; i < changes.length; i++) {
                    var row = changes[i][0];
                    if (changes[i][1] == 'product_name' || changes[i][1] == 'contents') {
                        sales_slip_add_edit_config.data[row][changes[i][1]] = changes[i][3].replace(/\n/g, '');
                    }
                }
                tableInstance.render();
            }
        },
//#7791:End
//#8035:Start
        afterSelectionEnd: function (r, c, r2, c2) {
            var editor = this.getActiveEditor();
            if (typeof editor !== 'undefined' && editor.hasOwnProperty('TEXTAREA') && editor.cellProperties.readOnly == false) {
                editor.beginEditing();
                editor.TEXTAREA.select();
            }
        },
//#8035:End
        cells: function (row, col, prop) {
            switch (prop) {
                case 'product_name':
                case 'contents':
                    return {readOnly: sales_slip_add_edit_config.data[row].hasOwnProperty('exclude')};
                case 'quantity':
                case 'amount':
                    if (sales_slip_add_edit_config.data[row].hasOwnProperty('exclude')
                        && (parseInt(sales_slip_add_edit_config.data[row][prop]) > 0)
                    ) {
                        sales_slip_add_edit_config.data[row][prop] = '-' + sales_slip_add_edit_config.data[row][prop];
                    }
                case 'unit_price':
                case 'tax_type':
                    if ((old_sales_slip_detail.length == 0 && sales_slip_detail[0].is_ad_receive_payment == 1)
                        || (sales_slip_add_edit_config.data[row].hasOwnProperty('exclude') || sales_slip_add_edit_config.data[row][prop] < 0)
                    ) {
                        return {className: 'red-txt'};
                    } else {
                        return {className: ''};
                    }
            }
        }
    };

    container.handsontable(sales_slip_add_edit_config);
    tableInstance = container.handsontable('getInstance');
    tableInstance.render();

    $('#sales_slip_form').on('click', '.btn-slMoveDown', function() {
        var cur_index = tableInstance.getSelected()[0];

        if (typeof sales_slip_add_edit_config.data[cur_index + 1] == 'undefined') {
            return;
        }

        var temp_element = sales_slip_add_edit_config.data[cur_index];
        sales_slip_add_edit_config.data[cur_index] = sales_slip_add_edit_config.data[cur_index + 1];
        sales_slip_add_edit_config.data[cur_index + 1] = temp_element;
        tableInstance.render();
    });

    $('#sales_slip_form').on('click', '.btn-slMoveUp', function() {
        var cur_index = tableInstance.getSelected()[0];

        if (typeof sales_slip_add_edit_config.data[cur_index - 1] == 'undefined') {
            return;
        }

        var temp_element = sales_slip_add_edit_config.data[cur_index];
        sales_slip_add_edit_config.data[cur_index] = sales_slip_add_edit_config.data[cur_index - 1];
        sales_slip_add_edit_config.data[cur_index - 1] = temp_element;
        tableInstance.render();
    });

    $('#sales_slip_form').on('click', '.btn-slRemove', function() {
        if (sales_slip_add_edit_config.data.length == 1) {
            return;
        }
        var cur_index = tableInstance.getSelected()[0];
        var ad_receive_sales_slip_detail_id = sales_slip_add_edit_config.data[cur_index].ad_receive_sales_slip_detail_id;

        sales_slip_add_edit_config.data.splice(cur_index, 1);

        if (ad_receive_sales_slip_detail_id != 0) {
            $.each(sales_slip_add_edit_config.data, function (k, v) {
                if (v.hasOwnProperty('exclude') && v.ad_receive_sales_slip_detail_id == ad_receive_sales_slip_detail_id) {
                    sales_slip_add_edit_config.data.splice(k, 1);
                    return false;
                }
            });
        }

        tableInstance.render();

        update_sales_slip_amount();
    });

    $('.btn-slCreateSalesSlip').click(function() {
        $('.error').html('');
        errorContainer.dialog('close');
        var sales_slip = {};
        $.each($('#sales-slip-form').serializeArray(), function(k, input) {
            sales_slip[input.name] = input.value;
        });

        var sales_slip_detail_params = [];
        var sales_slip_detail_id = [];
        var sales_slip_detail_sequence = [];
        var sequence = 1;
        $.each(sales_slip_add_edit_config.data, function(k, row){
//#8149:Start
            sales_slip_detail_sequence.push({
                id: row.id,
                sequence: sequence
            });
            sequence++;
//#8149:End

            if (row.hasOwnProperty('exclude')) {
                return;
            }
            sales_slip_detail_params.push(row);
            sales_slip_detail_id.push(row.id);
        }) ;

        $.post('/_admin/sales/add', {sales_slip_detail_id: sales_slip_detail_id, sales_slip: sales_slip, sales_slip_detail: sales_slip_detail_params, sequence: sales_slip_detail_sequence, action: 'add'},
            function(rs) {
                if (rs.status == 1) {
                    $('.btn-slCreateSalesSlip').prop('disabled', true).addClass('btn-gray');
                    $('#sales_slip_form .message').addClass('success-message').html(rs.message);
                    is_submitted = true;
                    form_changed = false;
                    fetchData(config.data, $('#ss_search').serialize());
                } else if (rs.status == 0) {
                    display_error(rs);
                }
            }, 'json');
    });

    $('.btn-slEditSalesSlip').click(function() {
        $('.error').html('');
        errorContainer.dialog('close');
        var sales_slip = {};
        $.each($('#sales-slip-form').serializeArray(), function(k, input) {
            sales_slip[input.name] = input.value;
        });

        var sales_slip_detail_params = [];
        var sales_slip_detail_id = [];
        var sales_slip_detail_sequence = [];
        var sequence = 1;
        $.each(sales_slip_add_edit_config.data, function(k, row){
//#8149:Start
            sales_slip_detail_sequence.push({
                id: row.id,
                sequence: sequence
            });
            sequence++;
//#8149:End

            if (row.hasOwnProperty('exclude')) {
                return;
            }
            sales_slip_detail_params.push(row);
            sales_slip_detail_id.push(row.id);
        }) ;

        $.post('/_admin/sales/edit', {sales_slip_id: $(this).data('salesSlipId'), sales_slip: sales_slip, sales_slip_detail: sales_slip_detail_params, sequence: sales_slip_detail_sequence, action: 'edit'},
            function(rs) {
                if (rs.status == 1) {
                    $('.btn-slEditSalesSlip').prop('disabled', true).addClass('btn-gray');
                    $('.btn-slDeleteSalesSlip').prop('disabled', true).addClass('btn-gray');
                    $('#sales_slip_form .message').addClass('success-message').html(rs.message);
                    is_submitted = true;
                    form_changed = false;
                    fetchData(config.data, $('#ss_search').serialize());
                } else if (rs.status == 0) {
                    display_error(rs);
                }
            }, 'json');
    });

    $('.btn-slDeleteSalesSlip').click(function() {
        var sales_slip_id = $(this).data('salesSlipId');
        confirm_dialog(' 削除します。よろしいですか？', {
            'delete': function() {
                $(this).dialog('close');
                $.post('/_admin/sales/edit', {sales_slip_id: sales_slip_id, action: 'delete'},
                    function (rs) {
                        if (rs.status == 1) {
                            $('.btn-slEditSalesSlip').prop('disabled', true).addClass('btn-gray');
                            $('.btn-slDeleteSalesSlip').prop('disabled', true).addClass('btn-gray');
                            $('#sales_slip_form .message').addClass('success-message').html(rs.message);
                            is_submitted = true;
                            form_changed = false;
                            fetchData(config.data, $('#ss_search').serialize());
                        }
                    }, 'json');
            }
        });
    });

    $('#billing_closing_date ,#payment_expire_date').change(function () {
        var date = parse_date($(this).val());
        if (date !== false && date != $(this).val()) {
            $(this).val(date);
        }
    });

    // Calculate sales slip total amount
    function update_sales_slip_amount() {
        var sales_slip_amount = calc_sales_slip_amount(sales_slip_detail, old_sales_slip_detail);
        $('.ss_total_amount').text(sales_slip_amount.total_amount);
        $('.ss_total_tax').text(sales_slip_amount.total_tax);
        $('.ss_total_amount_tax').text(sales_slip_amount.total_amount_tax);
    }
    update_sales_slip_amount();

    function display_error(rs) {
        if (typeof rs.message == 'string') {
            errorContainer.find('.popup-error').html('<span class="row-error-icon">' + rs.message + '</span>');
            errorContainer.dialog('open');
        } else {
            if (rs.message.hasOwnProperty('sales_slip')) {
                $.each(rs.message.sales_slip, function (k, v) {
                    $('[name=' + k + ']').siblings('.error').html(
                        '<div class="validation-error">' + v + '</div>');
                });
            }
            if (rs.message.hasOwnProperty('sales_slip_detail')) {
                errorContainer.find('.popup-error').html('');
                $.each(rs.message.sales_slip_detail, function (index, error) {
                    errorContainer.find('.popup-error').append('<span class="row-error-icon">' + (parseInt(index) + 1) + '行目</span>');
                    $.each(error, function (field, message) {
                        errorContainer.find('.popup-error').append('<p style="padding-left: 20px"><span class="validation-error">' + message + '</span></p>');
                    });
                });
                errorContainer.dialog('open');
            }
        }
    }
});