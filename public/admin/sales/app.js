var fetchData;
var loadingData = false;
var config;
$(function () {
    var tableInstance;
    var container = $('#sales_table');
    var colsDef = [];
    var colsMap = {};
    var select2EditorCols = [];
    var tableValid = true;
    var colHeaders = [];
//#9158:Start
    //var chkApplySSNumber = [];
    //var chkApproveSSNumber = [];
    var SSNumber = [];
//#9158:End
    var searchArray = [];
    var initColsDef = function() {
        if (ss_can_approve) {
            colsDef.push({
                data: 'to_be_approved',
                renderer: checkboxRenderer,
                editor: Handsontable.editors.CheckboxEditor,
                width: 50
            });
            colHeaders.push(label.lbl_bulk_approval);
        }
        if (can_create != 'none') {
            colsDef.push(
                {
                    data: 'to_be_applied',
                    renderer: checkboxRenderer,
                    editor: Handsontable.editors.CheckboxEditor,
                    width: 50
                },
                {
                    data: 'to_be_created',
                    renderer: checkboxRenderer,
                    editor: Handsontable.editors.CheckboxEditor,
                    width: 50
                },
                {
                    data: '',
                    renderer: actionRenderer,
                    editor: false,
                    width: 100
                },
                {
                    data: 'to_be_updated',
                    renderer: checkboxRenderer,
                    editor: Handsontable.editors.CheckboxEditor,
                    width: 50
                }
            );
            colHeaders.push(label.lbl_batch_apply, label.lbl_slip_create, label.lbl_action, label.lbl_edit);
        }
        colsDef.push(
            {
                data: 'sales_slip_number',
                editor: false,
                renderer: slipNumberRenderer,
                readOnly: true,
                width: 90
            },
//#7622:Start
            {
                data: 'status',
                renderer: statusRenderer,
                editor: false,
                readOnly: true,
                width: 130
            },
//#7622:End
            {
                data: 'client',
                editor: false,
                renderer: 'html',
                readOnly: true,
                width: 200
            },
            {
                data: 'j_code_disp',
                renderer: 'html',
                editor: false,
                readOnly: true,
                width: 80
            },
            {
                data: 'branch_cd',
                editor: false,
                readOnly: true,
                width: 25
            },
            {
                data: 'delivery_date',
                validator: deliveryDateValidator,
                allowInvalid: false,
                readOnly: true,
                width: 80
            },
            {
                data: 'product_name',
                //renderer: productSelectRenderer,
                validator: contentsValidator,
                allowInvalid: false,
                //editor: 'select2',
                //select2Options: {
                //    data: products,
                //    width: 'element',
                //    minimumResultsForSearch: Infinity
                //},
                readOnly: true,
                width: 100
            },
            {
                data: 'contents',
                readOnly: true,
                validator: contentsValidator,
                allowInvalid: false,
                width: 150
            },
//#7622:Start
            //{
            //    data: 'status',
            //    renderer: statusRenderer,
            //    editor: false,
            //    width: 130
            //},
//#7622:End
            {
                data: 'quantity',
                type: 'numeric',
                renderer: numericRenderer,
                format: '0,0',
                //validator: numberLength6Validator,
                validator: numberLength7Validator,
                readOnly: true,
                allowInvalid: false,
                width: 65
            },
            {
                data: 'unit_price',
                type: 'numeric',
                renderer: numericRenderer,
                format: '0,0.00',
                validator: numberLength9Validator,
                readOnly: true,
                allowInvalid: false,
                width: 105
            },
            {
                data: 'tax_type',
                renderer: taxRenderer,
                editor: 'select2',
                select2Options: {
                    data: tax_options,
                    width: 'element',
                    minimumResultsForSearch: Infinity
                },
                readOnly: true,
                width: 30
            },
            {
                data: 'amount',
                type: 'numeric',
                renderer: numericRenderer,
                format: '0,0',
                editor: false,
                validator: numberLength9Validator,
                readOnly: true,
                width: 125
            },
            {
                data: 'date',
                editor: false,
                renderer: 'html',
                readOnly: true,
                width: 100
            },
            {
                data: 'order',
                editor: false,
                renderer: 'html',
                className: 'htRight',
                readOnly: true,
                width: 100
            },
            {
                data: 'gross',
                editor: false,
                renderer: 'html',
                className: 'htRight',
                readOnly: true,
                width: 100
            },
            {
                data: 'acceptance_amount',
                editor: false,
                renderer: orderAcceptancRenderer,
                readOnly: true,
                width: 100
            },
            {
                data: 'j_account_id',
                renderer: accountRenderer,
                editor: false,
                readOnly: true,
                width: 100
            }
        );
        colHeaders.push(label.lbl_slip_number, label.lbl_status, label.lbl_apply_client, 'Ｊ' + label.lbl_code, label.lbl_branch,
            label.lbl_delivery_date, label.lbl_product, label.lbl_contents,
            label.lbl_quantity, label.lbl_unit_price, label.lbl_tax, label.lbl_total,
            label.lbl_delivery_date + '/' + label.lbl_billing_date + '/' + label.lbl_payment_date,
            label.lbl_order_quantity + '/' + label.lbl_tax + '/' + label.lbl_unit_price + '/' + label.lbl_total,
            label.lbl_gross_unit_price + '/' + label.lbl_total, label.lbl_total_amount, 'Ｊ' + label.lbl_sales_representative);
        for (var i = 0; i < colsDef.length; i++) {
            colsMap[colsDef[i].data] = i;
            if (colsDef[i].editor == 'select2') {
                select2EditorCols.push(colsDef[i].data);
            }
        }
    };
    initColsDef();
    var emptyTableColsDef = colsDef.slice(0);
    emptyTableColsDef[0] = {
        data: 'message',
        editor: false,
        className: 'htCenter',
        width: 50
    };
    config = {
        data: [],
        rowHeaders: false,
        contextmenu: false,
        fillHandle: false,
        multiSelect: false,
        manualColumnResize: true,
        copyPaste: false,
        //disableVisualSelection: true,
//#7589:Start
//      height: 800
        height: get_handsontable_viewport_height(),
//#7589:End
        //width: 700,
        stretchH: 'all',
        //colWidths: [50, 50, 50, 120, 50, 100, 200, 100, 50, 100, 100, 100, 125, 50, 30, 50, 100, 100, 100, 100, 100, 100],
        colHeaders: colHeaders,
        columns: colsDef,
        mergeCells: [],
        cells: function(row, col, prop) {
            // If table is empty -> exit
            if (config.data.length == 0 || config.columns[0].data == 'message') {
                return;
            }
            // Init variables
            var cellProp = {};
            cellProp.className = '';
            var rowProp = config.data[row].cellProp;

            // Alternative row background
            if (row % 2 == config.data[row].alterBackground) {
                cellProp.className = 'cell-dark';
            }

            // Workaround to fix alignment
            if (prop == 'gross' || prop == 'order') {
                cellProp.className += ' htRight';
            }

            // Color tax type red when it is adv payment row
            if (config.data[row].is_ad_receive_payment == 1 && prop == 'tax_type') {
                cellProp.className += ' red-txt';
            }

            // Custom cell's attribute
            for (var i = 0; i < rowProp.length; i++) {
                if (rowProp[i].target.indexOf(prop) !== -1) {
                    if (rowProp[i].prop == 'className') {
                        cellProp.className += ' ' + rowProp[i].value;
                    } else {
                        cellProp[rowProp[i].prop] = rowProp[i].value;
                    }
                }
            }

            // Enable editing for row that have 'edit' column checked
            if (['delivery_date', 'product_name', 'contents', 'quantity', 'tax_type', 'unit_price'].indexOf(prop) !== -1) {
                if (config.data[row].status < SALES_STATUS_INVOICE_ISSUED) {
                    cellProp.readOnly = config.data[row].to_be_deleted || !config.data[row].to_be_updated;
                } else {
                    if (row > 0 && config.data[row - 1].is_ad_receive_payment == 1) {
                        cellProp.readOnly = config.data[row - 1].to_be_deleted || !config.data[row - 1].to_be_updated;
                    }
                    if (config.data[row].is_ad_receive_payment == 1 && prop == 'delivery_date') {
                        cellProp.readOnly = !(findMerge(row, colsMap.delivery_date) && config.data[row + 1].to_be_updated);
                    }
                }
            }

            // Mark row that is going to be deleted dark background
            if (config.data[row].to_be_deleted || (row > 0 && config.data[row - 1].to_be_deleted && config.data[row - 1].is_ad_receive_payment == 1)) {
                if (cellProp.hasOwnProperty('className')) {
                    cellProp.className += ' gray-row';
                } else {
                    cellProp.className = 'gray-row';
                }

                if (['to_be_approved', 'to_be_applied', 'to_be_created', 'to_be_updated'].indexOf(prop) !== -1) {
                    cellProp.readOnly = true;
                }
            } else {
                if (['to_be_approved', 'to_be_applied', 'to_be_created', 'to_be_updated'].indexOf(prop) !== -1) {
                    cellProp.readOnly = false;
                }
            }

            return cellProp;
        },
        afterValidate: function (isValid, value, row, prop, source) {
            tableValid = isValid;
            if (source == 'edit' && prop == 'delivery_date') {
                if (isValid) {
                    //!(config.data[row].is_ad_receive_payment == 0 && (config.data[row].ad_receive_sales_slip_detail_id != 0 || config.data[row].ad_receive_sales_slip_detail_row_id != -1))) {
                    var newDate = parse_date(value);
                    var oldDate = parse_date(config.data[row].delivery_date);
                    var orderDate = parse_date(config.data[row].order_delivery_date);
                    if ((newDate && oldDate) && orderDate.substr(0, 7) != newDate.substr(0, 7)) {
                        errorContainer.find('.popup-error').text(label.msg_delivery_date_within_same_month);
                        errorContainer.dialog('open');
                        tableValid = false;
                        return false;
                    } else {
                        errorContainer.dialog('close');
                    }
                } else {
                    if (value.trim() == '') {
                        errorContainer.find('.popup-error').text(label.msg_delivery_date_empty);
                    } else {
                        errorContainer.find('.popup-error').text(label.msg_delivery_date_invalid);
                    }
                    errorContainer.dialog('open');
                }
            }
            if (source == 'edit' && ['contents', 'product_name', 'quantity', 'unit_price', 'amount'].indexOf(prop) !== false) {
                if (!isValid) {
                    if (prop == 'contents') {
                        errorContainer.find('.popup-error').text('内容入力可能文字数は30文字です。');
                    } else if (prop == 'product_name') {
                        errorContainer.find('.popup-error').text('商品名入力可能文字数は30文字です。');
                    } else if (prop == 'quantity') {
                        //errorContainer.find('.popup-error').text('数量は７桁以上の入力ができません。管理部に問い合わせてください。');
                        errorContainer.find('.popup-error').text('数量は８桁以上の入力ができません。管理部に問い合わせてください。');
                    } else if (prop == 'unit_price') {
                        errorContainer.find('.popup-error').text('単価は１０桁以上の入力ができません。管理部に問い合わせてください。');
                    } else if (prop == 'amount') {
                        errorContainer.find('.popup-error').text('合計は１０桁以上の入力ができません。管理部に問い合わせてください。');
                    }
                    errorContainer.dialog('open');
                } else {
                    var amount = false;
                    if (prop == 'unit_price') {
                        amount = value * config.data[row].quantity;
                    } else if (prop == 'quantity') {
                        amount = config.data[row].unit_price * value;
                    }
                    if (amount !== false) {
                        if (amount <= -1000000000 || amount >= 1000000000) {
                            //tableValid = false;
                            errorContainer.find('.popup-error').text('合計は１０桁以上の入力ができません。管理部に問い合わせてください。');
                            errorContainer.dialog('open');
                            return true;
                        }
                    }
                    errorContainer.dialog('close');
                }
            }
        },
        beforeChange: function(changes, source) {
            $("#sales_table").handsontable("destroyEditor",true);
            if (loadingData) {
                return false;
            }
//#6900:Start
            for (var i = 0; i < changes.length; i++) {
                var row = changes[i][0];
                if (changes[i][1] == 'to_be_updated' && changes[i][3] && config.data[row].is_change_report_apply == 1) {
                    errorContainer.find('.popup-error').text('変レポ中なので入力できません');
                    errorContainer.dialog('open');
                    changes[i] = null;
                }
            }
//#6900:End
        },
        afterChange: function (changes, source) {
            if (source == 'edit') {
                form_changed = true;
                needMergeRefresh = false;
                for (var i = 0; i < changes.length; i++) {
                    var row = changes[i][0];
                    if (changes[i][1] == 'quantity' || changes[i][1] == 'unit_price') {
//#9199:Start
                        var unit_price = new BigNumber(config.data[row].unit_price);
                        var quantity = new BigNumber(config.data[row].quantity);
                        if (!unit_price.isNaN()) {
                            config.data[row].unit_price = unit_price.round(2, BigNumber.ROUND_DOWN).toNumber();
                        }
                        if (!quantity.isNaN()) {
                            config.data[row].quantity = quantity.round(0, BigNumber.ROUND_DOWN).toNumber();
                        }
//#9199:End
                        calcAmount(row);
                    }
                    if (changes[i][1] == 'delivery_date') {
                        config.data[row].delivery_date = parse_date(config.data[row].delivery_date);
                    }

                    if (config.data[row].is_ad_receive_payment == 1 && config.data[row].status >= SALES_STATUS_INVOICE_ISSUED) {
                        if (changes[i][1] == 'delivery_date') {
                            config.data[row].delivery_date = changes[i][2];
                            config.data[row + 1].delivery_date = parse_date(changes[i][3]);
                            if (config.data[row].delivery_date != config.data[row + 1].delivery_date) {
                                removeMerge(row, colsMap.delivery_date);
                                needMergeRefresh = true;
                            }
                        }
                        if (['to_be_applied', 'to_be_approved', 'to_be_created', 'to_be_updated'].indexOf(changes[i][1]) !== -1) {
                            if (changes[i][3] == true) {
                                config.data[row][changes[i][1]] = changes[i][2];
                                config.data[row + 1][changes[i][1]] = changes[i][3];
                            } else {
                                config.data[row][changes[i][1]] = false;
                                config.data[row + 1][changes[i][1]] = false;
                            }
                        }
                        if (['product_name', 'contents'].indexOf(changes[i][1]) !== -1) {
                            config.data[row + 1][changes[i][1]] = changes[i][3];
                        }
//#7791:Start
                        if (changes[i][1] == 'product_name' || changes[i][1] == 'contents') {
                            config.data[row + 1][changes[i][1]] = changes[i][3].replace(/\n/g, '');
                        }
//#7791:End
                    }

                    if (['to_be_applied', 'to_be_approved', 'to_be_created', 'to_be_updated'].indexOf(changes[i][1]) !== -1) {
                        //$("#sales_table").handsontable("destroyEditor",true);
                        errorContainer.dialog('close');
                    }

//#7791:Start
                    if (changes[i][1] == 'product_name' || changes[i][1] == 'contents') {
                        config.data[row][changes[i][1]] = changes[i][3].replace(/\n/g, '');
                    }
//#7791:End
                }

                if (needMergeRefresh) {
                    mergeRefresh();
                }
                tableInstance.render();
            }
        },
//#8035:Start
        afterSelectionEnd: function (r, c, r2, c2) {
            var editor = this.getActiveEditor();
            if (typeof editor !== 'undefined') {
                if (editor.hasOwnProperty('TEXTAREA') && editor.cellProperties.readOnly == false && tableValid) {
                    editor.beginEditing();
                    $(editor.TEXTAREA).blur(function () {
                        $(this).select();
                    });
                    editor.TEXTAREA.select();
                }

//#8124:Start
                if ($(editor.TD).find('input[type=checkbox]').length == 1) {
                    if (config.data[r].is_change_report_apply == 1 && colsMap.to_be_updated == c) {
                        errorContainer.find('.popup-error').text('変レポ中なので入力できません');
                        errorContainer.dialog('open');
                        return;
                    }
                    var checkbox = $(editor.TD).find('input[type=checkbox]').first();
                    checkbox.prop('checked', !checkbox.prop('checked'));
                    if (r > 0 && config.data[r - 1].is_ad_receive_payment == 1 && config.data[r - 1].status != SALES_STATUS_INVOICE_ISSUED) {
                        config.data[r - 1][editor.prop] = !config.data[r - 1][editor.prop];
                    } else {
                        config.data[r][editor.prop] = !config.data[r][editor.prop];
                    }
                    //tableInstance.render();
                }
//#8124:End
            }
        },
//#8035:End
//#8020:Start
        beforeKeyDown: function (e) {
            if (e.keyCode == 27) {
                tableValid = true;
                $('.htInvalid').removeClass('htInvalid');
                errorContainer.dialog('close');
            }
//#10119:Start
            if (e.keyCode == 46 || e.keyCode == 8) {
                var editor = this.getActiveEditor();
                if (select2EditorCols.indexOf(editor.prop) != -1) {
                    Handsontable.Dom.enableImmediatePropagation(e);
                    e.stopImmediatePropagation();
                }
            }
//#10119:End
        },
//#8020:End
//#9158:Start
//        beforeRender: function (isForced) {
//            chkApplySSNumber = [];
//            chkApproveSSNumber = [];
//        },
//#9158:End
        afterRender: function (isForced) {
            angular.element(document).injector().invoke(function($compile) {
                var scope = angular.element(container).scope();
                $compile(container)(scope);
                scope.$apply();
            });
        }
    };

    container.handsontable(config);
    tableInstance = $('#sales_table').handsontable('getInstance');

//#8035:Start
    container.find('.wtHolder').scroll(function () {
        clearTimeout($.data(this, 'scrollTimer'));
        $.data(this, 'scrollTimer', setTimeout(function() {
            var editor = tableInstance.getActiveEditor();
            if (typeof editor !== 'undefined' && editor.hasOwnProperty('TEXTAREA') && editor.cellProperties.readOnly == false) {
                editor.TEXTAREA.select();
            }
        }, 250));
    });
//#8035:End

    // ENTER => SEARCH
    $("#ss_search").keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
         {
            e.preventDefault();
            // alert(111);

            var search_status = $("input[name='search_status[]']").is(':focus');
            var search_is_ad_receive_payment = $("input[name='search_is_ad_receive_payment']").is(':focus');
            var search_j_business_unit = $("select[name='search_j_business_unit']").is(':focus');

            var search_j_account = $("select[name='search_j_account']").is(':focus');
            var search_sales_slip_number = $("input[name='search_sales_slip_number']").is(':focus');
            var search_client_name = $("input[name='search_client_name']").is(':focus');

            var search_j_code_branch_cd_from = $("input[name='search_j_code_branch_cd_from']").is(':focus');
            var search_j_code_branch_cd_to = $("input[name='search_j_code_branch_cd_to']").is(':focus');
            var search_delivery_date_from = $("input[name='search_delivery_date_from']").is(':focus');

            var search_delivery_date_to = $("input[name='search_delivery_date_to']").is(':focus');
            var search_order_regist_date_from = $("input[name='search_order_regist_date_from']").is(':focus');
            var search_order_regist_date_to = $("input[name='search_order_regist_date_to']").is(':focus');

            var search_approval_date_from = $("input[name='search_approval_date_from']").is(':focus');
            var search_approval_date_to = $("input[name='search_approval_date_to']").is(':focus');
            var search_billing_date_from = $("input[name='search_billing_date_from']").is(':focus');

            var search_billing_date_to = $("input[name='search_billing_date_to']").is(':focus');
            var search_payment_date_from = $("input[name='search_payment_date_from']").is(':focus');
            var search_payment_date_to = $("input[name='search_payment_date_to']").is(':focus');

            var search_s_code = $("input[name='search_s_code']").is(':focus');
            var search_client_name_furigana = $("input[name='search_client_name_furigana']").is(':focus');
            var search_client_department_name = $("input[name='search_client_department_name']").is(':focus');

            var search_s_business_unit = $("select[name='search_s_business_unit']").is(':focus');
            var search_s_account = $("select[name='search_s_account']").is(':focus');


            if(search_status | search_is_ad_receive_payment | search_j_business_unit
            | search_j_account | search_sales_slip_number | search_client_name
            | search_j_code_branch_cd_from | search_j_code_branch_cd_to | search_delivery_date_from
            | search_delivery_date_to | search_order_regist_date_from | search_order_regist_date_to
            | search_approval_date_from | search_approval_date_to | search_billing_date_from
            | search_billing_date_to | search_payment_date_from | search_payment_date_to
            | search_s_code | search_client_name_furigana | search_client_department_name
            | search_s_business_unit | search_s_account
            ) {
                $('.btn-search').click();
            }
         }
    });


    $('.btn-search').click(function (e) {
        e.preventDefault();
        if (loadingData) {
            return;
        }
        $(this).addClass('loading icon-spin');
        $('#message').text('').removeClass('caution').removeClass('success-message');
        searchArray = $("#ss_search").serializeArray();
        fetchData(config.data, $(this).parents('form').serialize());
    });

    fetchData = function(data, search_params) {

        loadingData = true;

        $.post('/_admin/sales/get_data',
            search_params,
            function (result) {
                loadingData = false;
                $('.loading').removeClass('loading icon-spin');
                if (result.hasOwnProperty('error')) {
                    show_search_error(result.error);
//#7067:Start
                    show_data_error(data, label.common_lbl_no_data, false);
//#7067:End
                    return;
//#7087:Start
                } else if (result.hasOwnProperty('is_over_record')) {
                    show_data_error(data, label.common_msg_over_record);
                    return;
//#7087:End
                } else if (result.length == 0) {
                    show_data_error(data, label.common_lbl_no_data);
                    return;
                }

                data.length = 0;
                config.mergeCells.length = 0;
//#9158:Start
                SSNumber = [];
//#9158:End

                var processed_discard = [];

                //Fix wrong order of adv payment set
                for (var i=0; i<result.length; i++) {
                    if (result[i].ad_receive_sales_slip_detail_id == result[i].sales_slip_detail_id) {
//#6949:Start
                        if (result[i].status != SALES_STATUS_INVOICED_DISCARDED) {
                            if ((i == result.length - 1) || (result[i + 1].ad_receive_sales_slip_detail_id != result[i].sales_slip_detail_id || parseInt(result[i + 1].status) == SALES_STATUS_INVOICED_DISCARDED)) {
                                for (var j = 0; j < result.length; j++) {
                                    if (result[j].ad_receive_sales_slip_detail_id == result[i].ad_receive_sales_slip_detail_id && i != j && parseInt(result[j].status) != SALES_STATUS_INVOICED_DISCARDED) {
//#6949:End
                                        if (j < i) {
                                            array_move(result, j, i);
                                        } else {
                                            array_move(result, j, i + 1);
                                        }
                                        break;
                                    }
                                }
                            }
//#6949:Start
                        } else {
                            if ((i == result.length - 1) || (result[i + 1].ad_receive_sales_slip_detail_id != result[i].sales_slip_detail_id || parseInt(result[i + 1].status) != SALES_STATUS_INVOICED_DISCARDED)) {
                                for (var j = 0; j < result.length; j++) {
                                    if (result[j].ad_receive_sales_slip_detail_id == result[i].ad_receive_sales_slip_detail_id && i != j && parseInt(result[j].status) == SALES_STATUS_INVOICED_DISCARDED) {
                                        if (j < i) {
                                            array_move(result, j, i);
                                        } else {
                                            array_move(result, j, i + 1);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
//#6949:End
                    }

//#7988:Start
                    if (result[i].status == SALES_STATUS_INVOICED_DISCARDED && processed_discard.indexOf(result[i].j_code + result[i].branch_cd) == -1) {
                        var create = true;
                        for (var j = 0; j < result.length; j++) {
                            if ((result[j].j_code + result[j].branch_cd) == (result[i].j_code + result[i].branch_cd) && result[j].status != SALES_STATUS_INVOICED_DISCARDED) {
                                create = false;
                            }
                        }
                        if (create && is_match_search_condition(result[i], searchArray)) {
                            var newSsd = cloneObj(result[i], true);
                            newSsd.status = SALES_STATUS_NO_INPUT;
                            newSsd.sales_slip_detail_id = null;
                            newSsd.sales_slip_number = '';
                            newSsd.sales_slip_id = null;
                            newSsd.ad_receive_sales_slip_detail_id = null;
                            newSsd.to_be_updated = true;
                            newSsd.product_name = products[newSsd.order_product_id];
                            newSsd.contents = newSsd.order_contents;
                            newSsd.tax_type = newSsd.order_tax_type;
                            newSsd.unit_price = newSsd.order_unit_price;
                            newSsd.quantity = newSsd.order_quantity;
                            newSsd.amount = newSsd.order_amount;
                            newSsd.ssd_delivery_date = newSsd.default_delivery_date;
                            newSsd.ssd_payment_date = newSsd.order_payment_date;
                            newSsd.ssd_billing_date = newSsd.order_billing_date;

                            result.splice(i, 0, newSsd);
                        }
                        processed_discard.push(result[i].j_code + result[i].branch_cd);
                    }
//#7988:End
                }

                clear_search_error();
                config.columns = colsDef;
                var rowId = 0;
                var rowToMerge = [];
                for (var i=0; i<result.length; i++) {
                    var row = {
                        alterBackground: 1,
                        sales_slip_id: result[i].sales_slip_id,
                        sales_slip_detail_id: result[i].sales_slip_detail_id,
                        to_be_approved: false,
                        to_be_applied: false,
                        to_be_created: false,
                        to_be_updated: false,
                        to_be_deleted: false,
                        jbu_id: result[i].jbu_id,
                        sales_slip_number: result[i].sales_slip_number,
                        s_code: result[i].s_code,
                        client:
//#7152:Start
                        //'<div><a href="javascript:void(0)" class="view-client" data-client-id="' + result[i].client_id + '">' + result[i].s_code + '</a></div>' +
                        //'<div><a href="javascript:void(0)" class="view-client" data-client-id="' + result[i].client_id + '">' + result[i].name + '</a></div>' +
                            '<div><a class="view-client" data-client-id="' + result[i].client_id + '">' + result[i].s_code + '</a></div>' +
                            '<div><a class="view-client" data-client-id="' + result[i].client_id + '">' + result[i].name + '</a></div>' +
//#7152:End
                            '<div>' + result[i].order_charge_name + '</div>',
                        j_code: result[i].j_code,
//#7152:Start
                        //j_code_disp: '<a href="javascript:void(0)" ng-click="loadDetail(' + result[i].order_id + ')">' + result[i].j_code + '</a>',
                        j_code_disp: '<a ng-click="loadDetail(' + result[i].order_id + ')">' + result[i].j_code + '</a>',
//#7152:End
                        branch_cd: result[i].branch_cd,
                        delivery_date: result[i].ssd_delivery_date.replace(/-/g, '/'),
                        product_name: result[i].product_name,
                        contents: result[i].contents,
                        status: result[i].status,
                        quantity: result[i].quantity,
                        tax_type: result[i].tax_type,
                        unit_price: result[i].unit_price,
                        amount: result[i].amount,
                        //amount: Math.floor(strip(result[i].unit_price * result[i].quantity)),
                        order_billing_date: result[i].order_billing_date,
                        order_payment_date: result[i].order_payment_date,
//#7769:Start
                        ssd_billing_date: result[i].ssd_billing_date,
                        ssd_payment_date: result[i].ssd_payment_date,
//#7769:End
                        date:
                            '<div>' + ((result[i].acceptance_delivery_date && result[i].acceptance_delivery_date != '0000-00-00') ? result[i].acceptance_delivery_date.replace(/-/g, '/') : result[i].order_delivery_date.replace(/-/g, '/')) + '</div>' +
//#7769:Start
//                        '<div>' + result[i].order_billing_date.replace(/-/g, '/') + '</div>' +
//                        '<div>' + result[i].order_payment_date.replace(/-/g, '/') + '</div>',
                            '<div>' + ((result[i].order_is_ad_receive_payment == 1) ? result[i].order_billing_date.replace(/-/g, '/') : result[i].ssd_billing_date.replace(/-/g, '/')) + '</div>' +
                            '<div>' + ((result[i].order_is_ad_receive_payment == 1) ? result[i].order_payment_date.replace(/-/g, '/') : result[i].ssd_payment_date.replace(/-/g, '/')) + '</div>',
//#7769:End
                        order:
                            '<div>' + formatMoneyAll(result[i].order_quantity,2) + '</div>' +
                            '<div>' + tax_options_obj[result[i].order_tax_type] + '</div>' +
                            '<div>' + formatMoneyAll(result[i].order_unit_price,2) + '</div>' +
                            '<div>' + formatMoneyAll( result[i].order_amount,2) + '</div>',
                        gross:
                            '<div>' + formatMoneyAll(result[i].cost_unit_price,2) + '</div>' +
                            '<div>' + formatMoneyAll(result[i].cost_total_price,2) + '</div>',
                        acceptance_amount:
                        	formatMoneyAll(result[i].sum_acceptance_amount,2),
                        j_account_id: result[i].j_account_id,
                        is_ad_receive_payment: result[i].is_ad_receive_payment,
                        ad_receive_sales_slip_detail_row_id: -1,
                        ad_receive_sales_slip_detail_id: result[i].ad_receive_sales_slip_detail_id,
                        cellProp: [],
                        order_delivery_date: result[i].order_delivery_date,
                        order_product_id: result[i].order_product_id,
//#7988:Start
                        order_product_name: products[result[i].order_product_id],
//#7988:End
                        order_contents: result[i].order_contents,
                        order_tax_type: result[i].order_tax_type,
                        order_unit_price: result[i].order_unit_price,
                        order_quantity: result[i].order_quantity,
                        order_amount: result[i].order_amount,
                        default_delivery_date: result[i].default_delivery_date.replace(/-/g, '/'),
                        readonly: false,
//#6900:Start
                        order_detail_id: result[i].order_detail_id,
                        is_change_report_apply: result[i].is_change_report_apply,
//#6900:End
                        // bu_id: result[i].business_unit_id
//#7769:Start
                        order_is_ad_receive_payment: result[i].order_is_ad_receive_payment,
                        client_id: result[i].client_id,
//#7769:End
                        firstSSN: false
                    };

//#9158:Start
                    if (typeof row.sales_slip_number == 'string' && row.sales_slip_number.length > 0 && SSNumber.indexOf(row.sales_slip_number) === -1) {
                        row.firstSSN = true;
                        SSNumber.push(row.sales_slip_number);
                    }
//#9158:End

                    // Check sales slip detail is accountant closed
                    if (row.delivery_date.substr(0, 7).replace(/\//g, '-') <= closing_accountant_date.substr(0, 7)) {
                        row.readonly = true;
                    }

                    data.push(row);
                    rowId++;
                    if (row.sales_slip_detail_id == null) {
                        if (row.is_ad_receive_payment == 1) {
                            var subRow = cloneObj(row, true);
                            subRow.is_ad_receive_payment = 0;
                            subRow.ad_receive_sales_slip_detail_row_id = rowId - 1;
                            subRow.delivery_date = '';
                            subRow.quantity = '';
                            subRow.unit_price = '';
                            subRow.amount = '';
                            data.push(subRow);
                            rowToMerge.push(rowId - 1);
                            rowId++;
                        }
                    } else {
                        if (row.ad_receive_sales_slip_detail_id == row.sales_slip_detail_id) {
//#7988:Start
                            //if ((i == result.length - 1) || (result[i + 1].ad_receive_sales_slip_detail_id != row.sales_slip_detail_id)) {
                            if ((i == result.length - 1) || (result[i + 1].ad_receive_sales_slip_detail_id != row.sales_slip_detail_id) || (result[i].status != SALES_STATUS_INVOICED_DISCARDED && result[i + 1].status == SALES_STATUS_INVOICED_DISCARDED)) {
//#7988:End
                                var subRow = cloneObj(row, true);
                                subRow.sales_slip_detail_id = null;
                                subRow.sales_slip_id = null;
                                subRow.is_ad_receive_payment = 0;
                                subRow.ad_receive_sales_slip_detail_row_id = rowId - 1;
                                subRow.status = SALES_STATUS_NO_INPUT;
                                //subRow.delivery_date = result[i].acceptance_delivery_date ? result[i].acceptance_delivery_date : result[i].order_delivery_date;
                                if (result[i].acceptance_delivery_date && (result[i].acceptance_delivery_date.substr(0, 7) == result[i].order_delivery_date.substr(0, 7))) {
                                    subRow.delivery_date = result[i].acceptance_delivery_date;
                                } else {
                                    subRow.delivery_date = result[i].order_delivery_date;
                                }
                                subRow.delivery_date = subRow.delivery_date.replace(/-/g, '/');
                                subRow.quantity = '';
                                subRow.unit_price = '';
                                subRow.amount = '';
                                data.push(subRow);
                                rowToMerge.push(rowId - 1);
                                rowId++;
                            } else {
                                rowToMerge.push(rowId - 1);
                            }
                        }
                    }
                }

                for (var i = 0; i < rowToMerge.length; i++) {
                    mergeRow(rowToMerge[i]);
                }

                alternativeBackground(0);
                mergeRefresh();
                tableInstance.render();
                $("#sales_table").handsontable("destroyEditor",true);
                tableValid = true;
                errorContainer.dialog('close');
            }
        );
    };
    fetchData(config.data, $('#ss_search').serialize());

//#7988:Start
    function is_match_search_condition(data, searchArray) {
        var search = {};
        var is_status_search = false;
        var is_status_no_input = false;
        $.each(searchArray, function (k, v) {
            switch (v.name) {
                case 'search_status[]':
                    is_status_search = true;
                    if (v.value == SALES_STATUS_NO_INPUT) {
                        is_status_no_input = true;
                    }
                    break;
                case 'search_delivery_date_from':
                case 'search_delivery_date_to':
                case 'search_billing_date_from':
                case 'search_billing_date_to':
                case 'search_payment_date_from':
                case 'search_payment_date_to':
                case 'search_sales_slip_number':
                    search[v.name] = v.value.trim();
            }
        });

        if (is_status_search && !is_status_no_input) {
            return false;
        }

        if (search.search_sales_slip_number) {
            return false;
        }

        if (!is_inside_search_date_range(search.search_delivery_date_from, search.search_delivery_date_to, data.default_delivery_date)) {
            return false;
        }

        if (!is_inside_search_date_range(search.search_billing_date_from, search.search_billing_date_to, data.order_billing_date)) {
            return false;
        }

        if (!is_inside_search_date_range(search.search_payment_date_from, search.search_payment_date_to, data.order_payment_date)) {
            return false;
        }

        return true;
    }

    function is_inside_search_date_range(from, to, value) {
        if (from) {
            var date_value = parse_date(value);
            if (!date_value) {
                return false;
            }

            var date_from = parse_date(from);
            if (!date_from) {
                return false;
            }
            if (to) {
                var date_to = parse_date(to);
                if (!date_to) {
                    return false;
                }
                return (date_from <= date_value && date_value <= date_to);
            } else {
                return date_from == date_value;
            }
        }

        return true;
    }
//#7988:End

//#7087:Start
    function show_data_error(data, msg, is_clear_search_error) {
//#7067:Start
        is_clear_search_error = typeof is_clear_search_error !== 'undefined' ? is_clear_search_error : true;
//#7067:End
        data.length = 0;
        config.mergeCells.length = 0;
//#7067:Start
        if (is_clear_search_error) {
            clear_search_error();
        }
//#7067:End
        config.columns = emptyTableColsDef;
        data.push({
            message: msg
        });
        config.mergeCells.push({row: 0, col: 0, rowspan: 1, colspan: colsDef.length});
        mergeRefresh();
        tableInstance.render();
    }
//#7087:End

    function calcAmount(rowIndex) {
        var unit_price = parseFloat(config.data[rowIndex].unit_price);
        var quantity = parseInt(config.data[rowIndex].quantity);

        if (isNaN(unit_price) || isNaN(quantity)) {
            config.data[rowIndex].amount = '';
        } else {
            //config.data[rowIndex].amount = Math.floor(strip(unit_price * quantity));
            var amount = (new BigNumber(unit_price)).mul(quantity).floor();
            //amount = amount.mul(quantity).floor();
            config.data[rowIndex].amount = amount.toNumber();
        }
    }

    function advSplit(index) {
        if (config.data[index].to_be_deleted) {
            return;
        }

        if (config.data[index].is_ad_receive_payment == 1) {
            var newAdvRow = cloneObj(config.data[index], true);
            var newConfirmRow = cloneObj(config.data[index + 1], true);

            newAdvRow.status = SALES_STATUS_NO_INPUT;
            newAdvRow.sales_slip_detail_id = null;
            newAdvRow.sales_slip_number = '';
            newAdvRow.sales_slip_id = null;
            newAdvRow.ad_receive_sales_slip_detail_id = null;
            newAdvRow.to_be_updated = true;
            newAdvRow.product_name = products[newAdvRow.order_product_id];
            newAdvRow.contents = newAdvRow.order_contents;
            newAdvRow.tax_type = newAdvRow.order_tax_type;
            newAdvRow.unit_price = newAdvRow.order_unit_price;
            newAdvRow.quantity = newAdvRow.order_quantity;
            newAdvRow.amount = newAdvRow.order_amount;
            newAdvRow.delivery_date = newAdvRow.default_delivery_date;

            newConfirmRow.sales_slip_detail_id = null;
            newConfirmRow.status = SALES_STATUS_NO_INPUT;
            newConfirmRow.tax_type = newConfirmRow.order_tax_type;
            newConfirmRow.quantity = '';
            newConfirmRow.unit_price = '';
            newConfirmRow.amount = '';
            newConfirmRow.delivery_date = newConfirmRow.default_delivery_date;

            config.data.splice(index + 2, 0, newAdvRow);
            config.data.splice(index + 3, 0, newConfirmRow);
            for (var i = 0; i < config.mergeCells.length; i++) {
                if (config.mergeCells[i].row >= index + 2) {
                    config.mergeCells[i].row += 2;
                }
            }
            alternativeBackground(index + 2);
            mergeRow(index + 2);
            tableInstance.render();
            mergeRefresh();
        }
    }

    function confirmSplit(index) {
        if (config.data[index].to_be_deleted) {
            return;
        }

        if (config.data[index].is_ad_receive_payment == 1) {
            var newRow = cloneObj(config.data[index + 1], true);
            newRow.status = SALES_STATUS_NO_INPUT;
            newRow.quantity = '';
            newRow.unit_price = '';
            newRow.amount = '';
            newRow.sales_slip_detail_id = null;
            newRow.sales_slip_id = null;
            newRow.ad_receive_sales_slip_detail_id = null;
            newRow.sales_slip_number = '';
            newRow.to_be_updated = true;
            newRow.product_name = products[newRow.order_product_id];
            newRow.contents = newRow.order_contents;
            newRow.tax_type = newRow.order_tax_type;
            config.data.splice(index + 2, 0, newRow);
            for (var i = 0; i < config.mergeCells.length; i++) {
                if (config.mergeCells[i].row >= index + 2) {
                    config.mergeCells[i].row++;
                }
            }
            //mergeRow(index + 2);
            alternativeBackground(index + 2);
            tableInstance.render();
            mergeRefresh();

        } else if (config.data[index].ad_receive_sales_slip_detail_row_id == -1) {
            var newRow = cloneObj(config.data[index], true);
            newRow.status = SALES_STATUS_NO_INPUT;
            newRow.quantity = '';
            newRow.unit_price = '';
            newRow.amount = '';
            newRow.sales_slip_detail_id = null;
            newRow.sales_slip_id = null;
            newRow.sales_slip_number = '';
            newRow.to_be_updated = true;
            newRow.product_name = products[newRow.order_product_id];
            newRow.contents = newRow.order_contents;
            newRow.tax_type = newRow.order_tax_type;
            config.data.splice(index + 1, 0, newRow);
            for (var i = 0; i < config.mergeCells.length; i++) {
                if (config.mergeCells[i].row >= index + 1) {
                    config.mergeCells[i].row++;
                }
            }
            alternativeBackground(index + 1);
            tableInstance.render();
            mergeRefresh();
        }
    }

    function markDelete(index) {
        // console.log(index, config.data[index]);
        if (config.data[index].status == SALES_STATUS_UNCREATED) {
            config.data[index].to_be_deleted = true;
            config.data[index].to_be_updated = true;
            config.data[index].to_be_applied = false;
            config.data[index].to_be_approved = false;
            config.data[index].to_be_created = false;
        }
        if (config.data[index].is_ad_receive_payment == 1 && config.data[index + 1].status == SALES_STATUS_UNCREATED) {
            config.data[index + 1].to_be_deleted = true;
            config.data[index + 1].to_be_updated = true;
            config.data[index + 1].to_be_applied = false;
            config.data[index + 1].to_be_approved = false;
            config.data[index + 1].to_be_created = false;
        }
        tableInstance.render();
    }
    function unmarkDelete(index) {
        // console.log(index, config.data[index]);
        if (config.data[index].to_be_deleted) {
            config.data[index].to_be_deleted = false;
            config.data[index].to_be_updated = false;
        }
        if (config.data[index].is_ad_receive_payment == 1 && config.data[index + 1].to_be_deleted) {
            config.data[index + 1].to_be_deleted = false;
            config.data[index + 1].to_be_updated = false;
        }
        tableInstance.render();
    }

    var splitCols = ['delivery_date', 'status', 'quantity', 'tax_type', 'unit_price', 'amount'];
    function mergeRow(rowId) {
        for (var i=0; i<colsDef.length; i++) {
            if ((config.data[rowId].is_ad_receive_payment == 1) &&
                (colsDef[i].data == 'delivery_date') &&
                (rowId < config.data.length - 1) &&
                (config.data[rowId].delivery_date == config.data[rowId + 1].delivery_date)) {
                config.mergeCells.push({row: rowId, col: i, rowspan: 2, colspan: 1});
            } else if (splitCols.indexOf(colsDef[i].data) === -1) {
                config.mergeCells.push({row: rowId, col: i, rowspan: 2, colspan: 1});
            }
        }
    }

    function removeMerge(row, col) {
        removeRowIndex = [];
        for (var i = 0; i < config.mergeCells.length; i++) {
            if (config.mergeCells[i].row == row) {
                if (typeof col == 'undefined' || config.mergeCells[i].col == col) {
                    removeRowIndex.push(i);
                }
            }
        }
        if (removeRowIndex.length > 0) {
            for (var i = 0; i < removeRowIndex.length; i++) {
                config.mergeCells.splice(removeRowIndex[i], 1);
            }
        }
    }

    function findMerge(row, col) {
        for (var i = 0; i < config.mergeCells.length; i++) {
            if (config.mergeCells[i].row == row && config.mergeCells[i].col == col) {
                return true;
            }
        }
        return false;
    }

    function mergeRefresh() {
        tableInstance.mergeCells = new Handsontable.MergeCells(config.mergeCells);
        tableInstance.updateSettings(config);
    }

    function alternativeBackground(startIndex) {
        startIndex = (startIndex < 1) ? 1 : startIndex;
        for (var i = startIndex; i < config.data.length; i++) {
            if (config.data[i - 1].is_ad_receive_payment == 1) {
                config.data[i].alterBackground = 1 - config.data[i - 1].alterBackground;
            } else {
                config.data[i].alterBackground = config.data[i - 1].alterBackground;
            }
        }
    }

    function checkboxRenderer(instance, td, row, col, prop, value, cellProperties) {
        var hide = false;
        var status = (config.data[row].is_ad_receive_payment == 1 && config.data[row].status >= SALES_STATUS_INVOICE_ISSUED) ? config.data[row + 1].status : config.data[row].status;
        if (config.data[row].is_ad_receive_payment == 1 && row < config.data.length - 1) {
            value = config.data[row][prop] || config.data[row + 1][prop];
        }
        if (config.data[row].readonly) {
            hide = true;
        } else {
            switch (prop) {
                case 'to_be_approved':
                    if (status == SALES_STATUS_APPROVAL_PENDING) {
//#9158:Start
                        //if (config.data[row].sales_slip_number && chkApproveSSNumber.indexOf(config.data[row].sales_slip_number) !== -1) {
                        if (config.data[row].sales_slip_number && !config.data[row].firstSSN) {
//#9158:End
                            hide = true;
                        } else if (config.data[row].sales_slip_number) {
                            if (bu_id_user_logged != config.data[row].jbu_id) {
                                hide = true;
                            }
//#9158:Start
                            //} else {
                            //    chkApproveSSNumber.push(config.data[row].sales_slip_number);
                            //}
//#9158:End
                        }
                    } else {
                        hide = true;
                    }
                    break;
                case 'to_be_applied':
                    if (check_can_create(config.data[row].j_account_id) && status == SALES_STATUS_CREATED) {
//#9158:Start
                        //if (config.data[row].sales_slip_number && chkApplySSNumber.indexOf(config.data[row].sales_slip_number) !== -1) {
                        if (config.data[row].sales_slip_number && !config.data[row].firstSSN) {
//#9158:End
                            hide = true;
                        }
//#9158:Start
                        //} else if (config.data[row].sales_slip_number) {
                        //    chkApplySSNumber.push(config.data[row].sales_slip_number);
                        //}
//#9158:End
                    } else {
                        hide = true;
                    }
                    break;
                case 'to_be_created':
                    if (!(check_can_create(config.data[row].j_account_id) && status == SALES_STATUS_UNCREATED)) {
                        hide = true;
                    }
                    break;
                case 'to_be_updated':
//#6949:Start
                    if (!(check_can_create(config.data[row].j_account_id) && [SALES_STATUS_NO_INPUT, SALES_STATUS_UNCREATED, SALES_STATUS_CREATED, SALES_STATUS_APPROVAL_PENDING].indexOf(parseInt(status)) !== -1) || config.data[row].status == SALES_STATUS_INVOICED_DISCARDED) {
//#6949:End
                        hide = true;
                    }
                    break;
            }
        }

        if (!hide) {
            Handsontable.cellTypes['checkbox'].renderer.apply(this, arguments);
            td.className = 'htCenter htMiddle';
        } else {
            value = '';
            Handsontable.TextCell.renderer.apply(this, arguments);

        }
        if (config.data[row].to_be_deleted) {
            td.className += ' gray-row';
        }
        if (row % 2 == config.data[row].alterBackground) {
            td.className += ' cell-dark';
        }

        return td;
    }

    //function productSelectRenderer(instance, td, row, col, prop, value, cellProperties) {
    //    for (var i=0; i<all_products.length; i++) {
    //        if (all_products[i].id == parseInt(value)) {
    //            value = all_products[i].name;
    //            break;
    //        }
    //    }
    //    Handsontable.TextCell.renderer.apply(this, arguments);
    //}

    function actionRenderer(instance, td, row, col, prop, value, cellProperties) {
        td.innerHTML = '';

        var btnColor = config.data[row].to_be_deleted ? 'btn-gray' : 'btn-green';

        if (check_can_create(config.data[row].j_account_id)) {
//#6949:Start
            if (config.data[row].is_ad_receive_payment == 1 && row < config.data.length - 1 && config.data[row + 1].status != SALES_STATUS_INVOICE_ISSUED  && !config.data[row].readonly) {
//#6949:End
                td.innerHTML += '<div style="margin-top: 5px"><a href="javascript:void(0)" class="btn-small ' + btnColor + ' btn-slAdvSplit">' + label.btn_unearned_split + '</a></div>';
            }

//#6949:Start
            if (!(config.data[row].is_ad_receive_payment == 1 && config.data[row].status < SALES_STATUS_INVOICE_ISSUED) && !config.data[row].readonly && config.data[row].status != SALES_STATUS_INVOICED_DISCARDED) {
//#6949:End
                td.innerHTML += '<div style="margin-top: 5px"><a href="javascript:void(0)" class="btn-small ' + btnColor + ' btn-slConfSplit">' + label.btn_confirm_split + '</a></div>';
            }

            if (config.data[row].sales_slip_number) {
                td.innerHTML += '<div style="margin-top: 5px"><a href="javascript:void(0)" class="btn-small ' + btnColor + ' btn-slView">' + label.btn_view_slip + '</a></div>';
            }
            if (!config.data[row].readonly && (config.data[row].status == SALES_STATUS_UNCREATED || (config.data[row].is_ad_receive_payment == 1 && row < config.data.length - 1 && config.data[row + 1].status == SALES_STATUS_UNCREATED))) {
                if ((config.data[row].status == SALES_STATUS_UNCREATED && !config.data[row].to_be_deleted) || (config.data[row].is_ad_receive_payment == 1 && row < config.data.length - 1 && config.data[row + 1].status == SALES_STATUS_UNCREATED && !config.data[row + 1].to_be_deleted)) {
                    td.innerHTML += '<div style="margin-top: 5px; margin-bottom: 5px"><a href="javascript:void(0)" class="btn-small btn-green btn-slDelete">' + label.btn_spec_delete + '</a></div>';
                } else {
                    td.innerHTML += '<div style="margin-top: 5px; margin-bottom: 5px"><a href="javascript:void(0)" class="btn-small btn-green btn-slUnDelete">' + label.btn_undo + '</a></div>';
                }
            }
        }

        td.className = 'htCenter';
        if (config.data[row].to_be_deleted) {
            td.className += ' gray-row';
        }
        if (row % 2 == config.data[row].alterBackground) {
            td.className += ' cell-dark';
        }

        return td;
    }

    function slipNumberRenderer(instance, td, row, col, prop, value, cellProperties) {
        var text = Handsontable.helper.stringify(config.data[row].sales_slip_number);

        var prefix = 'SC';
        if (row < config.data.length - 1 && config.data[row].is_ad_receive_payment == 1 && config.data[row + 1].status < SALES_STATUS_CREATED) {
            prefix = 'SA';
        }

        if (text) {
//#7152:Start
            //text = '<a href="javascript:void(0)" class="btn-slView">' + prefix + text + '</a>';
            text = '<a class="btn-slView">' + prefix + text + '</a>';
//#7152:End
            td.innerHTML = text;
        } else {
            td.innerHTML = '';
        }
        td.className = '';
        if (config.data[row].to_be_deleted) {
            td.className += ' gray-row';
        }
        if (row % 2 == config.data[row].alterBackground) {
            td.className += ' cell-dark';
        }

        return td;
    }

    function numericRenderer(instance, td, row, col, prop, value, cellProperties) {
        if (config.data[row].is_ad_receive_payment == 1 || value < 0) {
            td.className += ' red-txt';
            //if (prop == 'quantity' || prop == 'amount') {
            //    value = '-' + value;
            //}
        }

        Handsontable.cellTypes['numeric'].renderer.apply(this, arguments);
    }
    
// #6946:Start
//    function formatMoneyAll(valueinput,c, d, t){
//    	if((valueinput != "")  && (valueinput!= null)){
//    	var n = valueinput,
//    	    c = isNaN(c = Math.abs(c)) ? 2 : c,
//    	    d = d == undefined ? "." : d,
//    	    t = t == undefined ? "," : t,
//    	    s = n < 0 ? "-" : "",
//    	    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
//    	    j = (j = i.length) > 3 ? j % 3 : 0;
//
//    	   var valreturn =  s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
//    	   var n = valueinput.indexOf("."); // Check  input have decimal
//    	   if(n > 0){    // True
//    		   return valreturn;
//    	   }
//    	   else{ // False
//    		   var end_cut = parseInt(c)+1;
//    		   valreturn = valreturn.slice(0,-end_cut);
//    		   return valreturn;
//    	   }
//    	}
//    	else{
//    		return valueinput;
//    	}
//    };
//#6946:End
    
    function deliveryDateValidator(value, callback) {
        if (parse_date(value) === false) {
            callback(false);
        } else {
            callback(true);
        }
    }

    function contentsValidator(value, callback) {
        if (value.length > 30) {
            callback(false);
        } else {
            callback(true);
        }
    }

    function numberLength6Validator(value, callback) {
        if (value > -1000000 && value < 1000000) {
            callback(true);
        } else {
            callback(false);
        }
    }

    function numberLength7Validator(value, callback) {
        if (value > -10000000 && value < 10000000) {
            callback(true);
        } else {
            callback(false);
        }
    }

    function numberLength9Validator(value, callback) {
        if (value > -1000000000 && value < 1000000000) {
            callback(true);
        } else {
            callback(false);
        }
    }

    function check_can_create(row_account_id) {
        row_account_id = parseInt(row_account_id);
        if (isNaN(row_account_id)) {
            return false;
        }

        return ((can_create == 'self' && current_login_id == row_account_id) || can_create == 'all');
    }

    function check_integrity(checkboxCol, status) {
        var is_adv_pay, s_code, payment_date, billing_date, delivery_date, sales_slip_number;
        for (var i = 0; i < config.data.length; i++) {
            if (config.data[i][checkboxCol]) {
                if (typeof s_code == 'undefined') {
                    s_code = config.data[i].s_code;
                }
                if (typeof payment_date == 'undefined') {
//#7769:Start
//                    payment_date = config.data[i].order_payment_date;
                    payment_date = config.data[i].ssd_payment_date;
//#7769:End
                }
                if (typeof billing_date == 'undefined') {
//#7769:Start
//                    billing_date = config.data[i].order_billing_date;
                    billing_date = config.data[i].ssd_billing_date;
//#7769:End
                }
                if (typeof delivery_date == 'undefined') {
                    delivery_date = config.data[i].order_delivery_date.substr(0, 7);
                }
                if (typeof is_adv_pay == 'undefined') {
                    is_adv_pay = config.data[i].is_ad_receive_payment;
                }
                if (typeof sales_slip_number == 'undefined') {
                    if (config.data[i].ad_receive_sales_slip_detail_id != 0
                    && config.data[i].is_ad_receive_payment == 0
                    && config.data[i - 1].sales_slip_number.length == 8) {
                        sales_slip_number = config.data[i - 1].sales_slip_number;
                    }
                }

                if (s_code != config.data[i].s_code) {
                    return {
                        result: false,
                        message: label.msg_same_s_code
                    };
                }
//#7769:Start
//                if (payment_date != config.data[i].order_payment_date || billing_date != config.data[i].order_billing_date) {
                if (payment_date != config.data[i].ssd_payment_date || billing_date != config.data[i].ssd_billing_date) {
//#7769:End
                    return {
                        result: false,
                        message: label.msg_same_payment_billing_date
                    };
                }
                if (delivery_date != config.data[i].order_delivery_date.substr(0, 7)) {
                    return {
                        result: false,
                        message: label.msg_same_delivery_date
                    };
                }
                if (is_adv_pay != config.data[i].is_ad_receive_payment) {
                    return {
                        result: false,
                        message: label.msg_mix_adv_payment_type
                    }
                }
                if (config.data[i].ad_receive_sales_slip_detail_id != 0
                && config.data[i].is_ad_receive_payment == 0
                && config.data[i - 1].sales_slip_number.length == 8
                && sales_slip_number != config.data[i - 1].sales_slip_number) {
                    return {
                        result: false,
                        message: label.msg_not_same_parent_slip_number
                    }
                }
                if (typeof status == 'object' && status.length > 0) {
                    if (status.indexOf(parseInt(config.data[i].status)) === -1) {
                        return {
                            result: false,
                            message: 'Wrong sales slip status'
                        };
                    }
                }
            }
        }

        return {result: true};
    }

    function orderAcceptancRenderer(instance, td, row, col, prop, value, cellProperties) {
        var text = Handsontable.helper.stringify(config.data[row].acceptance_amount);

        if (text) {
//#7152:Start
            //text = '<a href="javascript:void(0)" class="view-order-acceptance">' + text + '</a>';
            text = '<a class="view-order-acceptance">' + text + '</a>';
//#7152:End
            td.innerHTML = text;
        } else {
            td.innerHTML = '';
        }

        td.className = '';

        if (config.data[row].to_be_deleted) {
            td.className += ' gray-row';
        }
        if (row % 2 == config.data[row].alterBackground) {
            td.className += ' cell-dark';
        }

        return td;
    }

    container.on('click', '.btn-slAdvSplit', function () {
        form_changed = true;
        var row = tableInstance.getSelected()[0];
        if (row > 0) {
            if (config.data[row - 1].is_ad_receive_payment == 1) {
                advSplit(row - 1);
                return;
            }
        }
        // console.log('row=' + row);
        advSplit(row);
    });
    container.on('click', '.btn-slConfSplit', function () {
        form_changed = true;
        var row = tableInstance.getSelected()[0];
        if (row > 0) {
            if (config.data[row - 1].is_ad_receive_payment == 1) {
                confirmSplit(row - 1);
                return;
            }
        }
        // console.log('row=' + row);
        confirmSplit(row);
    });
    container.on('click', '.btn-slDelete', function () {
        form_changed = true;
        var row = tableInstance.getSelected()[0];
        if (row > 0) {
            if (config.data[row - 1].is_ad_receive_payment == 1) {
                markDelete(row - 1);
                return;
            }
        }
        // console.log('row=' + row);
        markDelete(row);
    });
    container.on('click', '.btn-slUnDelete', function () {
        var row = tableInstance.getSelected()[0];
        if (row > 0) {
            if (config.data[row - 1].is_ad_receive_payment == 1) {
                unmarkDelete(row - 1);
                return;
            }
        }
        // console.log('row=' + row);
        unmarkDelete(row);
    });

    container.on('click', '.btn-slView', function (e) {
        e.preventDefault();

        var tableInstance = $('#sales_table').handsontable('getInstance');

        var row = tableInstance.getSelected()[0];
        if (row > 0 && config.data[row - 1].is_ad_receive_payment == 1 && config.data[row].status < SALES_STATUS_CREATED) {
            row--;
        }

        dialogContainer.load("/_admin/sales/detail", {
            id : config.data[row].sales_slip_id
        }, function() {
            dialogContainer.dialog('option', 'title', label.btn_view_slip);
            dialogContainer.dialog( "option", "height", 'auto');
            dialogContainer.dialog( "option", "width", '1040');
            dialogContainer.dialog("open");

            form_changed = false;

            $('#sales_detail').handsontable('render');
        });
    });

    $('body').on('click', '.ui-dialog .btn-close', function() {
        dialogContainer.dialog('close');
    });

    container.on('click', '.view-client', function () {
        var client_id = $(this).data('clientId');
        dialogContainer.load("/_admin/client/detail", {
            id : client_id,
            dialog_selector: '#dialogContainer'
        }, function() {
            dialogContainer.dialog('option', 'title', lang['text_title_show_client']); //#6849 :  DElete - Modify
            dialogContainer.dialog( "option", "height", 'auto');
            dialogContainer.dialog( "option", "width", 'auto');
            dialogContainer.dialog("open");

            form_changed = false;
        });
    });

    // popup view order_acceptance
    container.on('click', '.view-order-acceptance', function () {
        var tableInstance = $('#sales_table').handsontable('getInstance');

        var row = tableInstance.getSelected()[0];

        var j_code    = config.data[row].j_code;
        var branch_cd = config.data[row].branch_cd;

        dialogContainer.load("/_admin/sales/order_acceptance", {
            j_code : j_code,
            branch_cd : branch_cd
        }, function() {
            dialogContainer.dialog("option", "title", label.lbl_order_acceptance_view);
            dialogContainer.dialog("option", "height", "auto");
            dialogContainer.dialog("option", "width", "auto");
            dialogContainer.dialog("open");

            form_changed = false;

            dialogContainer.dialog("option", "position", {my: "center", at: "center", of: window});

        });
    });

    function statusRenderer(instance, td, row, col, prop, value, cellProperties) {

        var prefix = '';
        if (config.data[row].is_ad_receive_payment == 1) {
            prefix = label.lbl_adv_pay + '：';
        } else if (config.data[row].ad_receive_sales_slip_detail_row_id != -1 || config.data[row].ad_receive_sales_slip_detail_id > 0) {
            prefix = label.lbl_confirmed_pay + '：';
        }
        value = prefix + status_options[value];
        Handsontable.TextCell.renderer.apply(this, arguments);
    }

    function taxRenderer(instance, td, row, col, prop, value, cellProperties) {
        value = tax_options_obj[value];
        Handsontable.TextCell.renderer.apply(this, arguments);
    }

    function accountRenderer(instance, td, row, col, prop, value, cellProperties) {
        value = accounts[value];
        Handsontable.TextCell.renderer.apply(this, arguments);
    }

    $('#slide-left-btn').click(function () {
        setTimeout(function () {
            tableInstance.render();
        }, 500);
    });

    $('.btn-apply').click(function () {
        var sendData = [];
        for (var i = 0; i < config.data.length; i++) {
            if (config.data[i].to_be_applied) {
                sendData.push(cloneObj(config.data[i], true), true);
            }
        }
        // console.log(sendData);

        if (sendData.length < 1) {
            $('#message').addClass('caution').text(label.msg_empty_bulk_apply);
        } else if (tableValid && !loadingData) {
            loadingData = true;
            $(this).addClass('loading icon-spin');
            sendRequest('/_admin/sales/apply_approval', {data: JSON.stringify(sendData)});
        }
    });

    $('.btn-approve').click(function () {
        var sendData = [];
        for (var i = 0; i < config.data.length; i++) {
            if (config.data[i].to_be_approved) {
                sendData.push(cloneObj(config.data[i], true), true);
            }
        }
        // console.log(sendData);

        if (sendData.length < 1) {
            $('#message').addClass('caution').text(label.msg_empty_bulk_approve);
        } else if (tableValid && !loadingData) {
            loadingData = true;
            $(this).addClass('loading icon-spin');
            sendRequest('/_admin/sales/approve', {data: JSON.stringify(sendData)});
        }
    });

    $('.btn-select-all').click(function () {
        for (var i = 0; i < config.data.length; i++) {
            if (config.data[i].status == SALES_STATUS_APPROVAL_PENDING && !config.data[i].readonly) {
                config.data[i].to_be_approved = true;
            }
        }
        tableInstance.render();
    });

    $('.btn-create-sales').click(function () {
        $('#message').text('').removeClass('caution').removeClass('success-message');
        var result = check_integrity('to_be_created');
        if (!result.result) {
            $('#message').addClass('caution').html(result.message);
            return;
        }

        var sendData = [];
        for (var i = 0; i < config.data.length; i++) {
            if (config.data[i].to_be_created) {
                sendData.push(config.data[i].sales_slip_detail_id);
            }
        }

        if (sendData.length < 1) {
            $('#message').addClass('caution').text(label.msg_empty_create_sales_slip);
        } else if (tableValid && !loadingData) {
            loadingData = true;
            dialogContainer.load('/_admin/sales/add', {
                sales_slip_detail_id: sendData
            }, function () {
                loadingData = false;
                dialogContainer.dialog('option', 'title', label.btn_create_sales);
                dialogContainer.dialog('option', 'width', 'auto');     //1000
                dialogContainer.dialog('option', 'height', 'auto');    //600
                dialogContainer.dialog("open");

                form_changed = false;

                $('#sales_form_table').handsontable('render');
            });
        }
    });

    $('.btn-add-slip-number').click(function () {
        $('#message').text('').removeClass('caution').removeClass('success-message');
        var result = check_integrity('to_be_created');
        if (!result.result) {
            $('#message').addClass('caution').html(result.message);
            return;
        }

        var sendData = [];
        for (var i = 0; i < config.data.length; i++) {
            if (config.data[i].to_be_created) {
                sendData.push(config.data[i].sales_slip_detail_id);
            }
        }
        // console.log(sendData);

        var slip_number = $(this).prev('.slip_number').val().trim();

        if (sendData.length < 1) {
            $('#message').addClass('caution').text(label.msg_empty_add_to_sales_slip);
        } else if (!slip_number){
            $('#message').addClass('caution').text(label.msg_no_sales_slip_number);
        } else if (tableValid && !loadingData) {
            loadingData = true;
            $(this).addClass('loading icon-spin');
            sendRequest('/_admin/sales/add_to_sales_slip', {
                slip_number: slip_number,
                sales_slip_detail_id: sendData
            });
        }
    });

    $('.btn-save').click(function () {
        var sendData = [];
        for (var i = 0; i < config.data.length; i++) {
            if (config.data[i].to_be_updated) {
                sendData.push(cloneObj(config.data[i], true));
            }
        }
        // console.log(sendData);

        if (sendData.length < 1) {
            $('#message').addClass('caution').text(label.msg_empty_edit);
        } else if (tableValid && !loadingData) {
            loadingData = true;
            $(this).addClass('loading icon-spin');
            sendRequest('/_admin/sales/change', {data: JSON.stringify(sendData)});
        }
    });

});


