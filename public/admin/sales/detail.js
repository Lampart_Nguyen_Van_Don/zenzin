$(function () {
    var tableInstance;
    var config_detail;
    var container_detail = $('#sales_detail');
    var colsDef = [];
    var colsMap = {};
    var columns_width = [75, 30, 85, 240, 250, 30, 70, 105, 100];
    //var columns_width = ['7%', '3%', '9%', '25%', '25%', '3%', '8%', '10%', '10%'];
    var initColsDef = function() {
        colsDef.push(
        {
            data: 'j_code',
            editor: false,
            readOnly: true,
            renderer: 'html'
        },
        {
            data: 'branch_cd',
            editor: false,
            readOnly: true,
            renderer: 'html'
        },
        {
            data: 'delivery_date',
            editor: false,
            readOnly: true,
            renderer: 'html'
        },
        {
            data: 'product_name',
            editor: false,
            readOnly: true,
            renderer: 'html'
        },
        {
            data: 'contents',
            editor: false,
            readOnly: true,
            renderer: 'html'
        },
        {
            data: 'tax_type',
            editor: false,
            readOnly: true,
            className: 'htCenter',
            renderer: taxRenderer
        },
        {
            data: 'quantity',
            editor: false,
            readOnly: true,
            type: 'numeric',
            format: '0,0',
            renderer: showQuantity
        },
        {
            data: 'unit_price',
            type: 'numeric',
            editor: false,
            readOnly: true,
            format: '0,0.00',
            renderer: numericRenderer,
        },
        {
            data: 'amount',
            editor: false,
            readOnly: true,
            type: 'numeric',
            format: '0,0',
            renderer: showAmount
        }
        );
    };
    initColsDef();
     config_detail = {
        data: sales_slip_detail,
        rowHeaders: false,
        contextmenu: false,
        fillHandle: false,
        multiSelect: false,
        manualColumnResize: true,
        copyPaste: false,
        height: 300,
        stretchH: 'all',
        colWidths: [75, 30, 85, 240, 250, 30, 70, 100, 100],
        // colWidths: ['7%', '3%', '9%', '25%', '25%', '3%', '8%', '10%', '10%'],
        colHeaders: ['Ｊコード','枝', '納品日', '商品名', '内容', '税', '数量', '単価', '金額'],
        columns: colsDef,
        zIndex: 1,
        afterRender: function() {
            dialogContainer.dialog("option", "position", {my: "center", at: "center", of: window});
            dialogContainer.dialog( "option", "minWidth", 1050 );
            if( $('#sales_detail .ht_master .wtHolder .wtHider .wtSpreader').width()< $('#sales_detail .ht_master .wtHolder .wtHider .wtSpreader').parent().width()){
                $('#sales_detail .ht_master .wtHolder .wtHider .wtSpreader').width('100%');
                $('#sales_detail .ht_master .wtHolder .wtHider .wtSpreader .htCore').width('100%');
                $('#sales_detail .ht_clone_top').hide();
                $('#sales_detail .ht_master .wtHolder .wtHider').width('100%');
                ($('#dialogContainer').width());
                for (var i = 0; i < columns_width.length; i++) {
                    $('#sales_detail .ht_master .wtHolder .wtHider .wtSpreader .htCore').children("colgroup").children("col").eq(i).width(columns_width[i]);
                };
            }
           
        }
    };

    tableInstance = container_detail.handsontable(config_detail);
    tableInstance = $('#sales_detail').handsontable('getInstance');
    // tableInstance.render();
    // console.log(tableInstance);


    function numericRenderer(instance, td, row, col, prop, value, cellProperties) {
        if (config_detail.data[row].is_show_minus || value < 0 || config_detail.data[row].is_ad_receive_payment_sales == 1) {
            td.className += ' red-txt';
        }
        Handsontable.cellTypes['numeric'].renderer.apply(this, arguments);
    }

    function setTextColor(instance, td, row, col, prop, value, cellProperties) {
        if (config_detail.data[row].is_show_minus) {
            td.className += ' red-txt';
        }
        Handsontable.TextCell.renderer.apply(this, arguments);
    }

    function showQuantity(instance, td, row, col, prop, value, cellProperties) {
        td.className += ' htNumeric';
        if (config_detail.data[row].is_show_minus || value < 0 || config_detail.data[row].is_ad_receive_payment_sales == 1) {
            td.className += ' red-txt';
        }

        if (config_detail.data[row].is_show_minus) {
            td.innerHTML = '-' + addCommas(config_detail.data[row].quantity);
        } else {
            td.innerHTML = addCommas(config_detail.data[row].quantity);
        }
        return td;
    }

    function showAmount(instance, td, row, col, prop, value, cellProperties) {
        td.className += ' htNumeric';
        if (config_detail.data[row].is_show_minus || value < 0 || config_detail.data[row].is_ad_receive_payment_sales == 1) {
            td.className += ' red-txt';
        }

        if (config_detail.data[row].is_show_minus) {
            td.innerHTML = '-' + addCommas(config_detail.data[row].amount);
        } else {
            td.innerHTML = addCommas(config_detail.data[row].amount);
        }
        return td;
    }

    function taxRenderer(instance, td, row, col, prop, value, cellProperties) {
        td.className += ' htCenter';
        if (config_detail.data[row].is_show_minus || config_detail.data[row].is_ad_receive_payment_sales == 1) {
            td.className += ' red-txt';
        }
        td.innerHTML = tax_type[config_detail.data[row].tax_type];
        return td;
    }

});

$(document).ready(function() {

    $('body').on('click', '#approval_reject_sale_slip', function () {
        var $this = $(this);

        $this.addClass('btn-close');
        $this.parents('.buttons').find('.btn-small:not(".btn-close")').remove();

        var ui_dialog = $('.ui-dialog:visible');

        if (!ui_dialog.find('.comment').length) {
            $('.ui-dialog:visible .table-simple').after('\
                <div class="comment" style="margin: 20px 0 0;">\n\
                    <label for="comment">' + sales_slip_lbl_comment + '</label>\
                    <textarea name="comment" id="comment" style="box-sizing: border-box; width: 100%; height: 100px; padding: 5px;"></textarea>\n\
                </div>'
                );
        }

        if (!ui_dialog.find('#sales_slip_rejection').length) {
            $this.after('<button type="button" class="btn-small btn-green" id="sales_slip_rejection">' + sales_slip_bt_rejection + '</button>');
        }

        if (!ui_dialog.find('#sales_slip_approval').length) {
            $this.after('<button type="button" class="btn-small btn-green" id="sales_slip_approval">' + sales_slip_bt_approval + '</button>');
        }

        $this.remove();
    });

    // 6. Approval sales slip
    if (!$('body').hasHandlers('click', '#sales_approval')) {
        $('body').on('click', '#sales_approval', function(e) {
            e.preventDefault();

            $(this).attr('disabled', 'disabled').addClass('loading icon-spin').blur();

            $('#message').text('').removeClass('caution').removeClass('success-message');

            $.post('/_admin/sales/approval_to_pending', {
                sales_slip_id: sales_slip_id
            }, function (result) {
                dialogContainer.dialog('close');

                if (result.status) {
                    form_changed = false;
                    $('#message').addClass('success-message').text(result.message);
                    fetchData(config.data, $('#ss_search').serialize());
                    errorContainer.dialog('close');
                } else {
                    if (result.hasOwnProperty('errors')) {
                        errorContainer.find('.popup-error').html('');

                        $.each(result.errors, function (j_code, error) {
                            errorContainer.find('.popup-error').append('<span class="row-error-icon">' + 'Ｊコード＋枝' + j_code + '</span>');
                            $.each(error, function (name, message) {
                                errorContainer.find('.popup-error').append('<p style="padding-left: 20px"><span class="validation-error">' + message + '</span></p>');
                            });
                        });

                        errorContainer.dialog('open');
                    } else {
                        $('#message').addClass('caution').text(result.message);
                    }
                }
            });
        });
    }

    // Approval sales slip
    if (!$('body').hasHandlers('click', '#sales_slip_approval')) {
        $('body').on('click', '#sales_slip_approval', function(e) {
            e.preventDefault();

            $(this).attr('disabled', 'disabled').addClass('loading icon-spin').blur();

            var sales_slip_comment = $('.ui-dialog:visible #comment').length ? $('.ui-dialog:visible #comment').val() : '';

            $('#message').text('').removeClass('caution').removeClass('success-message');

            $.post('/_admin/sales/approval', {
                sales_slip_id: sales_slip_id,
                sales_slip_comment: sales_slip_comment
            }, function (result) {
                dialogContainer.dialog('close');

                if (result.status) {
                    form_changed = false;
                    $('#message').addClass('success-message').text(result.message);
                    fetchData(config.data, $('#ss_search').serialize());
                    errorContainer.dialog('close');
                } else {
                    if (result.hasOwnProperty('errors')) {
                        errorContainer.find('.popup-error').html('');

                        $.each(result.errors, function (j_code, error) {
                            errorContainer.find('.popup-error').append('<span class="row-error-icon">' + 'Ｊコード＋枝' + j_code + '</span>');
                            $.each(error, function (name, message) {
                                errorContainer.find('.popup-error').append('<p style="padding-left: 20px"><span class="validation-error">' + message + '</span></p>');
                            });
                        });

                        errorContainer.dialog('open');
                    } else {
                        $('#message').addClass('caution').text(result.message);
                    }
                }
            });
        });
    }

    // Rejection sales slip
    if (!$('body').hasHandlers('click', '#sales_slip_rejection')) {
        $('body').on('click', '#sales_slip_rejection', function(e) {
            e.preventDefault();

            $(this).attr('disabled', 'disabled').addClass('loading icon-spin').blur();

            var sales_slip_comment = $('.ui-dialog:visible #comment').length ? $('.ui-dialog:visible #comment').val() : '';

            $('#message').text('').removeClass('caution').removeClass('success-message');

            $.post('/_admin/sales/reject', {
                sales_slip_id: sales_slip_id,
                sales_slip_comment: sales_slip_comment
            }, function (result) {
                dialogContainer.dialog('close');

                if (result.status) {
                    form_changed = false;
                    $('#message').addClass('success-message').text(result.message);
                    fetchData(config.data, $('#ss_search').serialize());
                    errorContainer.dialog('close');
                } else {
                    if (result.hasOwnProperty('errors')) {
                        errorContainer.find('.popup-error').html('');

                        $.each(result.errors, function (j_code, error) {
                            errorContainer.find('.popup-error').append('<span class="row-error-icon">' + 'Ｊコード＋枝' + j_code + '</span>');
                            $.each(error, function (name, message) {
                                errorContainer.find('.popup-error').append('<p style="padding-left: 20px"><span class="validation-error">' + message + '</span></p>');
                            });
                        });

                        errorContainer.dialog('open');
                    } else {
                        $('#message').addClass('caution').text(result.message);
                    }
                }
            });
        });
    }

    $('#sales_slip_pdf').click(function () {
        var form = '<form id="frm_sales_slip_pdf" action="/_admin/sales/pdf_export" method="POST" target="_blank">'
            + '<input type="hidden" name="sales_slip_id" value="' + $(this).data('sales_slip_id') + '"/>'
            + '</form>';
        $('#dialogContainer').append(form);
        $('#frm_sales_slip_pdf').submit();
    });

    $('#edit_sales_slip').click(function() {
        dialogContainer.load('/_admin/sales/edit', {
            sales_slip_id: $('#edit_sales_slip').data('sales_slip_id')
        }, function () {
            dialogContainer.dialog('option', 'title', label.common_lbl_edit);
            dialogContainer.dialog('option', 'width', 'auto');     //1000
            dialogContainer.dialog('option', 'height', 'auto');    //600
            dialogContainer.dialog("open");

            form_changed = false;

            $('#sales_form_table').handsontable('render');
        });
    });
});