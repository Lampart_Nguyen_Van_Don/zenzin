/**
 * Common functions use in sales slip feature
 */

//var saving = false;
var storageSupport = (typeof(Storage) !== "undefined") ? true : false;

var SALES_STATUS_NO_INPUT = 1;                    // 売上明細未入力
var SALES_STATUS_UNCREATED = 2;                   // 売上伝票未作成
var SALES_STATUS_CREATED = 3;                     // 売上伝票作成中
var SALES_STATUS_APPROVAL_PENDING = 11;           // 売上伝票承認申請中
var SALES_STATUS_INVOICE_APPROVAL_PENDING = 21;   // 請求書発行承認申請中
var SALES_STATUS_INVOICE_APPROVED = 22;           // 請求書発行承認済み
var SALES_STATUS_INVOICE_ISSUED = 23;             // 請求書発行済み
var SALES_STATUS_INVOICED_DISCARDED = 29;         // 請求書発行破棄

var TAX_EXCLUSIVE = 1; // 外
var TAX_INCLUDED  = 2; // 内
var TAX_NON       = 3; // 非

form_changed = false;

if (typeof ss_functions_loaded === 'undefined' || !ss_functions_loaded) {

    function saveData(key, value) {
        if (storageSupport) {
            localStorage.setItem(key, value);
        } else {
            cookies[key] = value;
        }
    }

    function loadData(key) {
        if (storageSupport) {
            return localStorage.getItem(key);
        } else {
            return cookies[key];
        }
    }

    function hideSearch(animate) {
        $('#search_toggle').css('width', $('#search_body').width());
        $('#search_toggle td').css('border-bottom', '1px solid #ddd');
        $('#search_toggle a').text(lbl_open_search);
        if (animate) {
            $('#search_body').slideUp();
        } else {
            $('#search_body').hide();
        }

        saveData('ss_showSearch', 0);
    }

    function showSearch(animate) {
        $('#search_toggle').css('width', '100%');
        $('#search_toggle td').css('border-bottom', 'none');
        $('#search_toggle a').text(lbl_hide_search);
        if (animate) {
            $('#search_body').slideDown();
        } else {
            $('#search_body').show();
        }

        saveData('ss_showSearch', 1);
    }

    function sendRequest(url, data) {
        $('#message').text('').removeClass('caution').removeClass('success-message');
        $.post(url, data, function (result) {
            if (result.status) {
                form_changed = false;
                $('.loading').removeClass('loading icon-spin');
                $('#message').addClass('success-message').text(result.message);
                fetchData(config.data, $('#ss_search').serialize());
                errorContainer.dialog('close');
            } else {
                loadingData = false;
                $('.loading').removeClass('loading icon-spin');
                if (result.hasOwnProperty('errors')) {
                    errorContainer.find('.popup-error').html('');
                    //errorContainer.find('.popup-error').append('<p class="import-error">');
                    $.each(result.errors, function (j_code, error) {
                        errorContainer.find('.popup-error').append('<span class="row-error-icon">' + 'Ｊコード＋枝' + j_code + '</span>');
                        $.each(error, function (name, message) {
                            errorContainer.find('.popup-error').append('<p style="padding-left: 20px"><span class="validation-error">' + message + '</span></p>');
                        });
                    });
                    //errorContainer.find('.popup-error').append('</p>');
                    errorContainer.dialog('open');
                } else {
                    $('#message').addClass('caution').text(result.message);
                }
            }
        });
    }

    function show_search_error(error) {
        clear_search_error();
        $.each(error, function (k, v) {
            $('#search_body #' + k).html('<span class="validation-error">' + v + '</span>');
        });
    }

    function clear_search_error() {
        $('#search_body .validation-error').remove();
    }

    function calc_sales_slip_amount(sales_slip_detail, adv_sales_slip_detail) {
        var amount = [];
        var is_confirm_payment = false;
        var conf_amount = 0;
        var adv_amount = 0;
        var total_conf_amount = 0;
        var total_adv_amount = 0;
        var temp_amount;
        var max_i = 0;
        var tax_rate = sales_slip_detail[0].tax_rate;
        for (var i = 0; i < sales_slip_detail.length; i++) {
            if (sales_slip_detail[i].hasOwnProperty('exclude')) {
                continue;
            }
            if (sales_slip_detail[i].delivery_date > sales_slip_detail[max_i].delivery_date) {
                max_i = i;
                tax_rate = sales_slip_detail[i].tax_rate;
            }

            temp_amount = undefined;
            conf_amount = sales_slip_detail[i].amount;
            if (parseInt(sales_slip_detail[i].tax_type) == TAX_INCLUDED) {
                //conf_amount = Math.ceil(strip(sales_slip_detail[i].amount / (1 + parseInt(sales_slip_detail[i].tax_rate) / 100)));
                conf_amount = (new BigNumber(sales_slip_detail[i].amount)).div((new BigNumber(sales_slip_detail[i].tax_rate)).div(100).plus(1)).ceil().toNumber();
            }
            total_conf_amount += parseInt(conf_amount);
            if (sales_slip_detail[i].is_ad_receive_payment == 0 && sales_slip_detail[i].ad_receive_sales_slip_detail_id != 0) {
                for (var j = 0; j < adv_sales_slip_detail.length; j++) {
                    adv_amount = adv_sales_slip_detail[j].amount;
                    if (parseInt(adv_sales_slip_detail[j].tax_type) == TAX_INCLUDED) {
                        //adv_amount = Math.ceil(strip(adv_sales_slip_detail[j].amount / (1 + parseInt(adv_sales_slip_detail[j].tax_rate) / 100)));
                        adv_amount = (new BigNumber(adv_sales_slip_detail[j].amount)).div((new BigNumber(adv_sales_slip_detail[j].tax_rate)).div(100).plus(1)).ceil().toNumber();
                    }
                    if (adv_sales_slip_detail[j].id == sales_slip_detail[i].ad_receive_sales_slip_detail_id) {
                        temp_amount = parseInt(conf_amount) - parseInt(adv_amount);
                        is_confirm_payment = true;
                        break;
                    }
                }
            }

            if (typeof temp_amount === 'undefined') {
                amount.push({
                    tax_type: sales_slip_detail[i].tax_type,
                    amount: parseInt(conf_amount)
                });
            } else {
                total_adv_amount += parseInt(adv_amount);
                amount.push({
                    tax_type: sales_slip_detail[i].tax_type,
                    amount: temp_amount
                });
            }
        }

        var total_amount = 0;
        var total_tax  = 0;
        var total_amount_tax = 0;
        var amount_tax_non = 0;
        for (var i = 0; i < amount.length; i++) {
            //if (is_confirm_payment && sales_slip_detail[i].is_ad_receive_payment == 1) {
            //    continue;
            //}
            switch (parseInt(amount[i].tax_type)) {
                case TAX_EXCLUSIVE:
                case TAX_INCLUDED:
                    total_amount += amount[i].amount;
                    break;
                case TAX_NON:
                    total_amount += amount[i].amount;
                    amount_tax_non += amount[i].amount;
                    break;
            }
        }

        if (is_confirm_payment) {
            total_amount = total_conf_amount - total_adv_amount;
            //total_tax = Math.floor(strip( total_conf_amount * (tax_rate / 100) )) - Math.floor(strip( total_adv_amount * (tax_rate / 100) ));
            total_tax = (new BigNumber(total_conf_amount)).mul(tax_rate).div(100).floor().toNumber() - (new BigNumber(total_adv_amount)).mul(tax_rate).div(100).floor().toNumber();
        } else {
            //total_tax = Math.floor(strip((total_amount - amount_tax_non) * (tax_rate / 100)));
            total_tax = (new BigNumber(total_amount - amount_tax_non)).mul(tax_rate).div(100).floor().toNumber();
        }

        total_amount_tax = total_amount + total_tax;

        return {
            total_amount: total_amount.toLocaleString(),
            total_tax: total_tax.toLocaleString(),
            total_amount_tax: total_amount_tax.toLocaleString()
        };
    }

    /*var save_key_handler = function (e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            if (!saving) {
                e.data.btn.trigger('click');
            }
            return false;
        }
    };

    function bind_save_key(submit_btn) {
        $(document).bind('keydown', {btn: submit_btn}, save_key_handler);
    }

    function unbind_save_key() {
        $(document).unbind('keydown', save_key_handler);
    }*/

    ss_functions_loaded = true;
}
