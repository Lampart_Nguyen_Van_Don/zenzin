var app = angular.module('app', [ 'ngTouch', 'ngSanitize', 'ui.grid',
    'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.pagination',
    'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.cellNav',
    'ui.grid.selection' ], function($compileProvider) {
        $compileProvider.directive('compile', function($compile) {
            return function(scope, element, attrs) {
                scope.$watch(
                    function(scope) {
                        return scope.$eval(attrs.compile);
                    },
                    function(value) {
                        element.html(value);
                        $compile(element.contents())(scope);
                    } // End function(value)
                ); // End scope.$watch(
            }; // End return function(scope, element, attrs)
        }); // End $compileProvider.directive
    } // function($compileProvider)
);

// Declaration and construction
app.controller('saleTarget', [ '$scope', '$http','uiGridConstants', function($scope, $http, uiGridConstants) {
    // Only display table when click "search"

    $scope.lang = 'ja';
    $scope.displayTable = false;

    $scope.rows = [];

    // For search
    $scope.search = {
        'datepicker_from' : '',
        'datepicker_to': '',
        'business_unit_id': '',
    };

    $scope.is_saving = false;
    $scope.search.business_unit_id = $('#business_unit_id').find(':selected').val();

    // Get data of order acceptance table for showing in grids
    $scope.allGrids = null;
    $scope.changedGrids = {};
    $scope.changedGrids[0] = {};
    $scope.changedGrids[0].sales_target = [];
    var getPageData = function(params) {
        // If first time = OK => The page has loaded and user clicks "Search"
        if(!isNaN($scope.firstTime)) {
            // Add circle loading to button search
            $('.btn-search').addClass('loading icon-spin');
            $http({
                url : '/_admin/sales_target/get_data',
                method : 'POST',
                data :  params
            })
            .success(function(data) {
                $scope.changedGrids[0].sales_target = [];
                if(data['has_error']) {
                    $('[name=' + "datepicker_from" + ']').siblings('.error').html(
                                    '<div class="validation-error">' + data['error_messages'].datepicker_from + '</div>');
                    $scope.displayNoData = true;

                     data.sales_target = [];
                }
                else {
                    $scope.search.datepicker_from = data['general_format'];
                    $scope.datepicker_from = data['general_format'];
                    $('[name=' + "datepicker_from" + ']').siblings('.error').html('');
                }
                data_list = data;
                var init_data = {};
                var informaton = {};
                informaton.$$hashKey = 'sales_target';
                informaton.id = 'id';
                init_data.information = informaton;
                init_data.sales_target = data_list.sales_target;
                data_list.targets = {};
                data_list.targets[0] = init_data;


                $scope.displayNoData = false;

                $scope.displayTable = true;
                $scope.allGrids = data_list.targets;

                if(data_list.sales_target.length == 0) {
                    $scope.displayNoData = true;
                }
                // Check authority to display or hide button SAVE
                if(data_list.has_authority) {
                    $scope.displayBtnSave = true;
                } else {
                    $scope.displayBtnSave = false;
                }
                $scope.rows = [];
                refreshGrid($scope, data_list);
                // Remove circle loading from button search
                $('.btn-search').removeClass('loading icon-spin');
            });
        }
        $scope.firstTime = true;

        var init_data = {};
        var informaton = {};
        informaton.$$hashKey = 'sales_target';
        informaton.id = 'id';
        init_data.information = informaton;
        init_data.sales_target = data_list.sales_target;
        data_list.targets = {};
        data_list.targets[0] = init_data;

        $scope.displayNoData = false;
        $scope.displayTable = true;
        $scope.allGrids = data_list.targets;

        if(data_list.sales_target.length == 0) {
            $scope.displayNoData = true;
        }

        // Check authority to display or hide button SAVE
        if(data_list.has_authority) {
            $scope.displayBtnSave = true;
        } else {
            $scope.displayBtnSave = false;
        }
        $scope.rows = [];
        refreshGrid($scope, data_list);
    }; // End  var getPageData

    var mySortFn = function(a,b){
        if(!a && !b){
            if(a == 0) {
                return 1;
            }
            return -1;
        }

        if(a != '' || !(Math.floor(a) == a && $.isNumeric(a)) )  {
            a = parseInt(a);
            if(!b) {
                return 1;
            }
        }

        if(b != '' || !(Math.floor(b) == b && $.isNumeric(b)) )  {
            b = parseInt(b);
            if(!a) {
                return -1;
            }
        }

        if (a == b) return 0;
        if (a < b) return -1;
        return 1;
    };

    var refreshGrid = function($scope, data_list) {
         angular.forEach(data_list.targets, function(target, id) {
            var gridData = target.information;
            gridData.gridOption = {
                enablePagination: false,
                enableFiltering : false,
                enableRowSelection : true,
                enableSelectAll : true,
                enableSorting: true,
                selectionRowHeaderWidth : 35,
                rowEditWaitInterval : -1,
                // width: 600,
                enablePaginationControls: false,
                rowData: target.sales_target,
                data : target.sales_target,
                rowTemplate:
                '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ui-grid-cell ng-class="{\'green-row\' : (row.invalid)}" ></div>',
                onRegisterApi: function(gridApi){
                    $scope.gridApi = gridApi;
                    //<---------------------CHANGE CELL (Target Amount)----------------------->
                    gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue){

                         if(newValue != oldValue) {
                            if(!(Math.floor(rowEntity.target_amount) == rowEntity.target_amount && $.isNumeric(rowEntity.target_amount)) || rowEntity.target_amount < 0) {
                                rowEntity.target_amount = 0;
                            }

                            var isExist = false;
                            for(var kk = 0; kk < $scope.changedGrids[0].sales_target.length; kk ++) {
                                if($scope.changedGrids[0].sales_target[kk].account_id == rowEntity.account_id) {
                                     $scope.changedGrids[0].sales_target[kk] = rowEntity;
                                     isExist = true;
                                 }
                            }

                            if(!isExist) {
                                $scope.changedGrids[0].sales_target.push(rowEntity);
                            }


                        }
                    }); // End gridApi.edit.on.afterCellEdit
                }, // End onRegisterApi: function(
                // <-----------------------------------------------COLUMN DECLARATION------------------------------------------>
                columnDefs : [
                    {
                        name : 'department_name',
                        enableCellEdit : false,
                        displayName : label.department,
                        // width: 200
                    },
                    {
                        name : 'name',
                        enableCellEdit : false,
                        displayName : label.name,
                        // width: 200
                    },
                    {
                        name : 'target_amount',
                        cellEditableCondition: function($scope) {
                       		$scope.row.entity.target_amount = parseInt($scope.row.entity.target_amount);
                            return true;
                        },
                        // width: 200,
                        displayName : label.target,
                        //cellFilter: 'number:2',
                        cellFilter: 'fractionFilter',
                        cellClass: 'a-right',
                        sortingAlgorithm: mySortFn
                    },
                ]
            };
            //
            $scope.rows.push(gridData);
            $scope.gridOptions = gridData.gridOption;
        });
    }

    // <!-------------------------------------------------- SET DEFAULT VALUE TO SEARCH TIME ---------------------------------------------->
    var d = new Date();
    var month = (d.getMonth()+1) <10 ? "0"+(d.getMonth()+1) :(d.getMonth()+1)
    var default_value = d.getFullYear()+"/" + month;
    $scope.datepicker_from = default_value;
    $scope.datepicker_to = default_value;
    $scope.search.datepicker_from = default_value;
    $scope.search.datepicker_to = default_value;

    // Default search when load page
    getPageData($scope.search);
    // Click "Search"
    $scope.filter = function(search) {
        if(search.datepicker_from.trim() == '') {
            $('[name=' + "datepicker_from" + ']').siblings('.error').html(
                                    '<div class="validation-error">' + error_msg.datepicker_from + '</div>');
            return false;
        }
        if(typeof(search.business_unit_id) !== 'undefined' ) {
            getPageData(search);
        }
    };

    // <-------------------------------------------- ENTER TO SEARCH (TARGET SETTING) ----------------------------------------->
    // $(document).keypress(function (e) {
    $(".table-simple").bind('keydown', function(e) {
        var key = e.which;
        if(key == 13)  // the enter key code
         {
            if($("input[name='datepicker_from']").is(':focus') | $("select[name='business_unit_id']").is(':focus')) {
                $('button[name=search]').click();
            }
         }
    });

    // <!------------------------------------------- USER CONFIRM SAVE NEW DATA BEFORE PRINT------------------------------------------->
    $scope.print = function (rows) {
        $scope.printPDFData = JSON.stringify(rows);
        if(rows.length==0) {
            alert(lang['nothing_to_print']);
        } else {
            setTimeout(function(){
                $("#printPDFForm").submit();
           }, 100);
        }
    };

    // <------------------------------------------ CLICK SAVE --------------------------------------------->
    $scope.saveGrid = function(allGrids, grid, subcontractor) {
        $scope.save();
    };

    // <-------------------------------------------------------- CTR+S ----------------------------------------------------------->
    $(document).bind('keydown', function(e) {
        if (e.ctrlKey && (e.which == 83)) {
            e.preventDefault();
            $scope.save();
            return false;
        }
    });

    // <------------------------------------------- SAVE ALL ------------------------------------>
    $scope.save = function() {
        if(!$scope.is_saving) {
            $scope.is_saving = true;
            $('.btn-save').addClass('loading icon-spin');
            // Simple POST request example (passing data) :
            var account_ids = [];
            for(var jj = 0; jj < $scope.allGrids[0].sales_target.length; jj ++) {
                $scope.gridApi.grid.rows[jj].invalid = false;
            }
            for(var k = 0 ; k < $scope.changedGrids[0].sales_target.length; k ++) {
               var value = $scope.changedGrids[0].sales_target[k].target_amount;
               // if(value == 0 || !(Math.floor(value) == value && $.isNumeric(value)) ) {
               //      account_ids.push($scope.changedGrids[0].sales_target[k].account_id);
               // }
            }

            if(account_ids.length > 0 ) {
               $("#dialog-status").html(lang['save_fail_try_again']).dialog({
                    title: '確認',
                    width: 'auto',
                    height: 110,
                    modal: true,
                    buttons: [],
              });
              for(var jj = 0; jj < $scope.allGrids[0].sales_target.length; jj ++) {
                if( $.inArray($scope.allGrids[0].sales_target[jj].account_id, account_ids) != -1) {
                    $scope.gridApi.grid.rows[jj].invalid = true;
                }
              }
              return false;
            }

             $http.post('/_admin/sales_target/save_all_grids/', {allGrids: $scope.changedGrids, searchDate: $scope.datepicker_from}).
              success(function(data, status, headers, config) {
                  $scope.is_saving = false;
                  $('.btn-save').removeClass('loading icon-spin');
                  // Save all grids OK
                  if (data["error"] == 0) {
                      getPageData($scope.search);
                      $("#dialog-status").html(lang['save_all_ok']).dialog({
                            title: '確認',
                            width: 'auto',
                            height: 110,
                            modal: true,
                            buttons: [],
                      });
                      return true;
                  }
                  // No data changed to save
                  else if(data['error']==2){
                      $("#dialog-status").html(lang['no_new_data_to_save']).dialog({
                            title: '確認',
                            width: 'auto',
                            height: 110,
                            modal: true,
                            buttons: [],
                      });
                      return false;
                  }
                  // Save processing error
                  else {
                      $("#dialog-status").html(lang['save_fail_try_again']).dialog({
                            title: '確認',
                            width: 'auto',
                            height: 110,
                            modal: true,
                            buttons: [],
                      });
                      return false;
                  }
              }).error(function(data, status, headers, config) {
                  $scope.is_saving = false;
                  $('.btn-save').removeClass('loading icon-spin');
                  return false;
              });
          }
    };
} ])
.filter('fractionFilter', function() {
		return function (value) {
			if (value == 0 ||isNaN(value) || value == null)
				return "";
			else {
				var value_ = (parseFloat(Math.round(value * 100) / 100).toFixed(2)).toString();
		        value_ = value_.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		        //return value_.replace(".",",");
		        return value_;
			}
		}
	}
)
;
$('button[name=search-report-btn]').click(function(){
    // alert("88888");
    // Add circle loading to button search
    $('button[name=search-report-btn]').addClass('loading icon-spin');
    var time = $('input[name="time"]').val();
    var business_unit = $('select[name="business_unit"]').val();
    $.ajax({
        url: "/_admin/sales_target/report",
        type: 'POST',
        data: {
            time: time,
            business_unit:business_unit
        },
        success: function(data, time) {

            if(data == 'error_empty_date'){
                $('.time-error').each(function(){
                    $(this).hide();
                });
                $('#error_empty_date').show();
            }
            else if(data == 'error_date_format'){
                $('.time-error').each(function(){
                    $(this).hide();
                });
                $('#error_date_format').show();
            }
            else{
                var time = $('input[name="time"]').val().replace('-','/').split('/');
                time[0] = (parseInt(time[0]) > 999)?time[0]:(parseInt(time[0])+2000);
                time[1] = (time[1].length > 1)?time[1]:('0'+time[1]);
                $('input[name="time"]').val(time[0].toString()+'/'+time[1]);
                $('.time-error').each(function(){
                    $(this).hide();
                });
                $('#report-content').html(data);
            }
            // Remove circle loading from button search
            $('button[name=search-report-btn]').removeClass('loading icon-spin');
        }
    });

});
$('input[name="time"]').change(function(){
    if($(this).val().trim()!=''){
        $('#error_empty_date').hide();
    }
});

//<-------------------------------------------- ENTER TO SEARCH (REPORT) ----------------------------------------->
// $(document).keypress(function (e) {
$(".table-input-sales-report").bind('keydown', function(e) {
    var key = e.which;
    if(key == 13)  // the enter key code
     {
        if($('input[name="time"]').is(':focus') | $('select[name="business_unit"]').is(':focus')) {
            $('button[name=search-report-btn]').click();
        }
     }
});