var app = angular.module('app', [ 'ngTouch', 'ngSanitize', 'ui.grid',
        'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.pagination',
        'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.cellNav',
        'ui.grid.selection' ]);

app
        .controller(
                'MainCtrl',
                [
                        '$scope',
                        '$http',
                        '$q',
                        '$interval',
                        'uiGridConstants',
                        function($scope, $http, $q, $interval, uiGridConstants) {
                            $scope.gridOptions = {
                                enableFiltering : true,
                                enableRowSelection : true,
                                enableSelectAll : true,
                                selectionRowHeaderWidth : 35,
                                paginationPageSizes : [ 5, 10, 20, 30, 50 ],
                                paginationPageSize : 10,
                                useExternalSorting : true,
                                useExternalFiltering : true,
                                totalItems : 100
                            };
                            $scope.gridOptions.enableCellEditOnFocus = true;
                            $scope.gridOptions.multiSelect = true;
                            $http.get("/_admin/sample/get_all_department").success(
                                    function(data) {
                                        var arr = [];

                                        for(var x in data){
                                            arr.push({'value': data[x].id, 'label': data[x].name});
                                        }
                                        $scope.gridOptions.columnDefs[2].filter.selectOptions = arr;
                                    });

                            $http.get("/_admin/sample/get_all_authority").success(
                                    function(data) {
                                        var arr = [];

                                        for(var x in data){
                                            arr.push({'value': data[x].id, 'label': data[x].name});
                                        }
                                        $scope.gridOptions.columnDefs[4].filter.selectOptions = arr;
                                    });

                            $scope.gridOptions.columnDefs = [
                                    {
                                        name : 'insert',
                                        enableFiltering : false,
                                        enableSorting : false,
                                        enableCellEdit : false,
                                        width : 100,
                                        pinnedLeft : true,
                                        cellTemplate:'<button class="btn primary" ng-click="grid.appScope.insertRow(row.entity)">Insert Row</button>'
                                    },
                                    {
                                        name : 'name',
                                        pinnedLeft : true,
                                        enableCellEdit : true,
                                        sort : {
                                            direction : uiGridConstants.DESC
                                        }
                                    },
                                    {
                                        name : 'department_id',
                                        enableCellEdit : true,
                                        editableCellTemplate : 'ui-grid/dropdownEditor',
                                        cellFilter : 'mapDepartment:row.entity.department_options',
                                        editDropdownValueLabel : 'name',
                                        editDropdownRowEntityOptionsArrayPath : 'department_options',
                                        filter : {
                                            type : uiGridConstants.filter.SELECT,
                                            selectOptions : []
                                        }
                                    },
                                    {
                                        name : 'login_id',
                                        enableCellEdit : true,
                                    },

                                    {
                                        name : 'authority_id',
                                        enableCellEdit : true,
                                        editableCellTemplate : 'ui-grid/dropdownEditor',
                                        cellFilter : 'mapAuthority:row.entity.authority_options',
                                        editDropdownValueLabel : 'name',
                                        editDropdownRowEntityOptionsArrayPath : 'authority_options',
                                        filter : {
                                            type : uiGridConstants.filter.SELECT,
                                            selectOptions : []
                                        }
                                    },

                                    {
                                        name : 'mail_address',
                                        enableCellEdit : true,
                                    }];

                            var paginationOptions = {
                                currentPage : 1,
                                pageSize : 10,
                                condition : [ [ "desc", null ], [ null, null ],
                                        [ null, null ], [ null, null ] ],
                                load : true
                            };

                            $scope.insertRow = function(row) {
                                $scope.numPages = Math.ceil($scope.numAccounts++ / paginationOptions.pageSize);
                                var indexRow = $scope.gridApi.grid.appScope.gridOptions.data.indexOf(row);
                                $scope.gridOptions.data.splice(
                                                indexRow,
                                                0,
                                                {
                                                    "name" : "",
                                                    "department_options" : row.department_options,
                                                    "login_id" : "",
                                                    "authority_options" : row.authority_options,
                                                    "mail_address" : "",
                                                });
                                setTimeout(
                                        function() {
                                            $scope.gridApi.rowEdit.setRowsDirty([ $scope.gridOptions.data[indexRow] ]);
                                            $scope.gridApi.selection.selectRow($scope.gridOptions.data[indexRow]);
                                        }, 100);
                            };

                            $(document).bind('keydown', function(e) {
                                if (e.ctrlKey && (e.which == 83)) {
                                    e.preventDefault();
                                    $scope.save();
                                    return false;
                                }
                            });
                            $(document).bind('keypress', function(e) {
                                if (e.ctrlKey && (e.which == 115)) {
                                    e.preventDefault();
                                    return false;
                                }
                            });
                            $(window).bind('beforeunload', function(){
                                if ($scope.gridApi.selection.getSelectedRows().length)
                                    return 'You may lose recent changes by navigating away.';
                            });

                            var getPage = function(paginationOptions) {
                                // console.log(uiGridConstants);
                                $http({
                                    url : '/_admin/sample/get_accounts_by_conditions',
                                    method : 'POST',
                                    data : {
                                        paginationOptions : paginationOptions
                                    }
                                })
                                        .success(
                                                function(data) {
                                                    //console.log(data);
                                                    if (paginationOptions.load) {
                                                        $scope.gridOptions.data = data.accounts;
                                                        $scope.numAccounts = data.numAccounts;
                                                        $scope.numPages = Math.ceil($scope.numAccounts / paginationOptions.pageSize);
                                                        paginationOptions.load = false;
                                                    } else {
                                                        for (var i = 0; i < data.products.length; i++) {
                                                            if (searchProduct(
                                                                    $scope.gridOptions.data,
                                                                    data.products[i]) == false) {
                                                                $scope.gridOptions.data.push(data.products[i]);
                                                            }
                                                        }
                                                    }
                                                });
                            };

                            getPage(paginationOptions);

                            $scope.addData = function() {
                                $scope.numPages = Math.ceil($scope.numAccounts++ / paginationOptions.pageSize);
                                var gridData = $scope.gridOptions.data;
                                gridData.unshift({
                                            "name" : "",
                                            "department_options" : $scope.gridOptions.data[0].department_options,
                                            "login_id" : "",
                                            "authority_options" : $scope.gridOptions.data[0].authority_options,
                                            "mail_address" : "",
                                        });

                                setTimeout(
                                        function() {
                                            $scope.gridOptions.data = gridData;
                                            $scope.gridApi.rowEdit.setRowsDirty([ $scope.gridOptions.data[0] ]);
                                            $scope.gridApi.cellNav.scrollToFocus(
                                                            $scope.gridOptions.data[0],
                                                            $scope.gridOptions.columnDefs[1]);
                                        }, 100);

                            };

                            $scope.saveRow = function(rowEntity) {
                                var promise = $q.defer();
                                $scope.gridApi.rowEdit.setSavePromise( rowEntity, promise.promise );

                                $http(
                                        {
                                            url : "/_admin/sample/save_account",
                                            method : 'POST',
                                            data : {rowEntity}
                                        }).success(
                                        function(data, status, headers, config) {
                                            $('.red').hide();
                                            $scope.errors = '';
                                            if (data.error_list) {
                                                promise.reject();
                                                $('.red').show();
                                                $scope.errors = data.error_list;
                                            } else {
                                                promise.resolve();
                                            }
                                        }).error(
                                        function(data, status, headers, config) {
                                            promise.reject();
                                        });
                            };

                            $scope.removeRows = function() {
                                var txt;
                                var r = confirm(message_sure_to_delete);
                                if (r == true) {
                                    $scope.gridApi.selection
                                            .getSelectedRows()
                                            .forEach(
                                                    function(entry) {
                                                        for (var i = 0; i < $scope.gridOptions.data.length; i++) {
                                                            if ($scope.gridOptions.data[i].id === entry.id) {
                                                                $scope.gridOptions.data.splice(i, 1);
                                                                break;
                                                            }
                                                        }
                                                    });
                                    $http(
                                            {
                                                url : "/_admin/sample/remove_list_product",
                                                method : 'POST',
                                                data : {
                                                    products : $scope.gridApi.selection
                                                            .getSelectedRows()
                                                }
                                            }).success(
                                            function(data, status, headers, config) {
                                                //console.log(data);
                                            }).error(
                                            function(data, status, headers, config) {
                                                console.log(data);
                                            });
                                }

                            };

                            $scope.filter = function(grid) {
                                paginationOptions.currentPage = 1;
                                $scope.gridApi.pagination.seek(1);
                                paginationOptions.condition[0][1] = grid.columns[2].filters[0].term;
                                paginationOptions.condition[1][1] = grid.columns[3].filters[0].term;
                                paginationOptions.condition[2][1] = grid.columns[4].filters[0].term;
                                paginationOptions.condition[3][1] = grid.columns[5].filters[0].term;
                                paginationOptions.load = true;
                                getPage(paginationOptions);
                            }

                            $scope.orderBy = function(sortColumns) {
                                if (sortColumns[0]) {
                                    if (sortColumns[0].name == 'code') {
                                        paginationOptions.condition[0][0] = sortColumns[0].sort.direction;
                                        paginationOptions.condition[1][0] = null;
                                        paginationOptions.condition[2][0] = null;
                                        paginationOptions.condition[3][0] = null;
                                    } else if (sortColumns[0].name == 'business_unit') {
                                        paginationOptions.condition[1][0] = sortColumns[0].sort.direction;
                                        paginationOptions.condition[0][0] = null;
                                        paginationOptions.condition[2][0] = null;
                                        paginationOptions.condition[3][0] = null;
                                    } else if (sortColumns[0].name == 'name_product') {
                                        paginationOptions.condition[2][0] = sortColumns[0].sort.direction;
                                        paginationOptions.condition[0][0] = null;
                                        paginationOptions.condition[1][0] = null;
                                        paginationOptions.condition[3][0] = null;
                                    } else {
                                        paginationOptions.condition[3][0] = sortColumns[0].sort.direction;
                                        paginationOptions.condition[0][0] = null;
                                        paginationOptions.condition[1][0] = null;
                                        paginationOptions.condition[2][0] = null;
                                    }
                                } else {
                                    paginationOptions.condition[3][0] = null;
                                    paginationOptions.condition[0][0] = null;
                                    paginationOptions.condition[1][0] = null;
                                    paginationOptions.condition[2][0] = null;
                                }
                                paginationOptions.load = true;
                                getPage(paginationOptions);
                            }

                            $scope.gridOptions.onRegisterApi = function(gridApi) {
                                $scope.gridApi = gridApi;

                                gridApi.rowEdit.on.saveRow($scope,
                                        $scope.saveRow);

                                gridApi.pagination.on
                                        .paginationChanged(
                                                $scope,
                                                function(currentPage, pageSize) {
                                                    var numRows = pageSize * currentPage;
                                                    $scope.numPages = Math.ceil($scope.numAccounts / pageSize);
                                                    if ($scope.gridOptions.data.length < numRows + pageSize) {
                                                        paginationOptions.currentPage = currentPage;
                                                        paginationOptions.pageSize = pageSize;
                                                        paginationOptions.load = false;
                                                        getPage(paginationOptions);
                                                    }
                                                });
                                gridApi.core.on
                                        .sortChanged(
                                                $scope,
                                                function(grid, sortColumns) {
                                                    if ($scope.gridApi.selection.getSelectedRows().length) {
                                                        $("#save-confirm")
                                                                .dialog(
                                                                        {
                                                                            resizable : false,
                                                                            modal : true,
                                                                            buttons : {
                                                                                "Yes" : function() {
                                                                                    $(this).dialog("close");
                                                                                    $scope.save($scope.orderBy, sortColumns);
                                                                                },
                                                                                "No" : function() {
                                                                                    $(this).dialog("close");
                                                                                    $scope.orderBy(sortColumns);
                                                                                },
                                                                                Cancel : function() {
                                                                                    $(this).dialog("close");
                                                                                    $scope.$apply(function() {
                                                                                                grid.getColumn('code').sort.direction = paginationOptions.condition[0][0];
                                                                                                grid.getColumn('business_unit').sort.direction = paginationOptions.condition[1][0];
                                                                                                grid.getColumn('name_product').sort.direction = paginationOptions.condition[2][0];
                                                                                                grid.getColumn('is_no_add').sort.direction = paginationOptions.condition[3][0];
                                                                                            });
                                                                                }
                                                                            }
                                                                        });
                                                    } else {
                                                        $scope.orderBy(sortColumns);
                                                    }
                                                });
                                gridApi.core.on
                                        .filterChanged(
                                                $scope,
                                                function() {
                                                    var grid = this.grid;
                                                    if ($scope.gridApi.selection.getSelectedRows().length) {
                                                        if ($scope.gridApi.grid.columns[1].filters[0].term != paginationOptions.condition[0][1]
                                                            || $scope.gridApi.grid.columns[2].filters[0].term != paginationOptions.condition[1][1]
                                                            || $scope.gridApi.grid.columns[3].filters[0].term != paginationOptions.condition[2][1]
                                                            || $scope.gridApi.grid.columns[4].filters[0].term != paginationOptions.condition[3][1]) {
                                                            $("#save-confirm").dialog(
                                                                            {
                                                                                resizable : false,
                                                                                modal : true,
                                                                                buttons : {
                                                                                    "Yes" : function() {
                                                                                        $(this).dialog("close");
                                                                                        $scope.save($scope.filter, grid);
                                                                                    },
                                                                                    "No" : function() {
                                                                                        $(this).dialog("close");
                                                                                        $scope.filter(grid);
                                                                                    },
                                                                                    Cancel : function() {
                                                                                        $(this).dialog("close");
                                                                                        $scope.$apply(function() {
                                                                                                    $scope.gridApi.grid.columns[1].filters[0].term = paginationOptions.condition[0][1];
                                                                                                    $scope.gridApi.grid.columns[2].filters[0].term = paginationOptions.condition[1][1];
                                                                                                    $scope.gridApi.grid.columns[3].filters[0].term = paginationOptions.condition[2][1];
                                                                                                    $scope.gridApi.grid.columns[4].filters[0].term = paginationOptions.condition[3][1];
                                                                                                });
                                                                                    }
                                                                                }
                                                                            });
                                                        }
                                                    } else {
                                                        $scope.filter(grid);
                                                    }
                                                });
                            };

                        } ]).filter('mapDepartment', function() {
                                return function(input, params) {
                                    if (!input) {
                                        return '';
                                    } else {
                                        for (i = 0; i < params.length; i++) {
                                            if (params[i].id == input) {
                                                return params[i].name;
                                            }
                                        }
                                    }
                                };
                            }).filter('mapAuthority', function() {
                                return function(input, params) {
                                    if (!input) {
                                        return '';
                                    } else {
                                        for (i = 0; i < params.length; i++) {
                                            if (params[i].id == input) {
                                                return params[i].name;
                                            }
                                        }
                                    }
                                };
                            })

function searchProduct(products, product) {
    for (var i = 0; i < products.length; i++) {
        if (products[i].id == product.id) {
            return true;
        }
    }
    return false;
}