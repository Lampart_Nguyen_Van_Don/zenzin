$(document).ready(function() {

    //check valid input
    $('#validate_input').on('click', function() {
        var delivery_date_from = $('#delivery_date_from').val();
        var delivery_date_to = $('#delivery_date_to').val();

        $.post(
                '/_admin/stock_check_list/ajax_check_valid_input',
                {
                    'delivery_date_from' : delivery_date_from,
                    'delivery_date_to' : delivery_date_to,
                },
                function(data) {
                    $('.validation-error').hide();
                    if (data.result !== false) {
                        var url = $('a#print_report').attr('href') + '?' + 'dlf=' + delivery_date_from + '&dlt=' + delivery_date_to;
                        window.open(url, '_blank');

                    } else {
                        $.each(data.error_msg, function(key, val) {
                            $('span#' + key + '_error_msg').html(val).show();
                        });
                    }
                },
             "json"
         );

    });

});