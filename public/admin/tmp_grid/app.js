var app = angular.module('app', ['ngTouch', 'ngSanitize', 'ui.grid', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.pagination', 'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.cellNav', 'ui.grid.selection']);

app.controller('MainCtrl', ['$scope', '$http', '$q', '$interval', 'uiGridConstants', function ($scope, $http, $q, $interval, uiGridConstants) {
    $scope.gridOptions = {
        enableFiltering: true,
        enableRowSelection: true,
        enableSelectAll: true,
        selectionRowHeaderWidth: 35,
        paginationPageSizes: [20, 30, 50],
        paginationPageSize: 20,
        rowEditWaitInterval: -1,
        showColumnFooter: true,
    };
    $scope.gridOptions.enableCellEditOnFocus = true;
    $scope.gridOptions.multiSelect = true;

    $scope.gridOptions.columnDefs = [
                                     { name: 'item', enableCellEdit: true, pinnedLeft:true,},
                                     { name: 'quantity', enableCellEdit: true,},
                                     { name: 'price', enableCellEdit: true,},
                                     { name: 'Total()', aggregationType: uiGridConstants.aggregationTypes.sum, displayName: 'Quantity*Price'},
                                   ];

    $http.get("/_admin/tmp/get_all")
    .success(function(data) {
        $scope.gridOptions.data = data;

        angular.forEach($scope.gridOptions.data, function (row) {
            row.Total = function () {
              return row.quantity * row.price;
            };
          });
    });
}])