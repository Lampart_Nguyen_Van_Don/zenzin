var lang = {
    message_confirm_update : "Do you want update?",
    message_confirm_apply : "Do you want to apply?",
    message_confirm_sequence_update: "Are you sure to change sequences?",
    message_confirm_delete_title : "Do you want delete?",
    message_confirm_delete: "Are you sure to delete?",
    title_confirm:"Confirm",
    lbl_yes:"YES",
    lbl_no:"NO",

    lbl_confirm: "Confirm",
    lbl_total_amount: "Total",
    lbl_adjusted_amount: "Adjusted Amount",
    lbl_accumulative: "Accumulative",
    lbl_action: "Action",
    lbl_change: "Change",
    lbl_j_code: "JCode",
    lbl_branch_cd: "Branch",
    lbl_client_name: "Client Name",
    lbl_unit_price: "Unit Price",
    lbl_quantity: "Quantity",
    lbl_delivery_date: "Delivery Date",



    button_yes:"YES",
    button_no:"NO",
    button_cancel: "Cancel",
    button_close: "Close",
    button_delete: "Delete",
    button_add: "Add",
    // button_delete is different in JPN so add one item on 26/June/2015
    button_delete_ad_pay: "Delete",

    //label
    please_choose : 'Please choose',
    all : 'All',

    //validation's messages
    name_required : 'Name is required',
    name_must_greater_than_6_characters : 'Name is minimum 6 characters',
    department_id_invalid : 'Department is invalid',
    login_id_required : 'Login id is required',
    login_must_greater_than_8_characters : 'Login id is minimum 8 characters',
    login_id_unique : 'Login id is uniqued',
    authority_id_invalid : 'Authority is invalid',
    mail_address_required : 'Mail address is required',
    mail_address_invalid : 'Mail address is invalid',
    mail_address_uniqued : 'Mail address is uniqued',

    // Added 24/June/2015
    date_between_row_above_and_below: "Date must be between row above and row below",
    time_must_be_in_month: "Time must be in month less or equal",
    time_must_be_in_year: "Time must be in year ",
    date_is_not_valid: "Date is not valid",
    new_value_save_and_print: "There is new values. Do you want to print (lost new values)?",
    nothing_to_print: "Nothing to print!",
    save_ok_for_subcontractor: "Saved successfully for subcontractor: ",
    save_fail_try_again: "Save fail. Please try again later!",
    no_new_data_to_save: "No data to save!",
    save_all_ok: "Save all successfully",
    other_grids_changed_want_to_save: "Other grid has been changed. Do you want to save?",

    fill_client_name_before_save: "Please fill all client name!",
    input_search_from: "Please input search from!",
    data_changed_search_again_or_not: "Data has been changed. Do you want to search again?",
    choose_file_before_upload: "Please choose file before upload!",
    areyousure: "Are you sure delete ？",
    discardsusscesinvoice:'This invoice have already delete',
     //#6849 - Add -S
    text_title_show_order : "Detail",
    text_title_show_client : "Client Detail",
     //#6849 - Add -E

    /* #6784 - Add -S */
    col_last_name_order_acceptance : "total amount",
    col_frist_name_order_acceptance: "approve all acceptance",
    /* #6784 - Add -E */

//* #6946:Start
    error_quantity_max_length : "Quantity isn't more than 7 number.Please you contact admin",
    error_unit_price_max_length : "Unit price isn't more than 10 number.Please you contact admin",
    error_tax_free_max_length : "Tax Free isn't more than 10 number.Please you contact admin",
    error_shipping_charge_tax_max_length : "Shipping Charge tax isn't more than 10 number.Please you contact admin",
    error_stamp_tax_max_length : "Stamp tax max isn't more than 10 number.Please you contact admin",
    error_etc_tax_max_length:"Ect tax max isn't more than 10 number.Please you contact admin",
    error_etc_max_length:"Ect  max isn't more than 10 number.Please you contact admin",
    error_shipping_charge_max_length:"Shipping Charge isn't more than 10 number.Please you contact admin",
//* #6946:End
};
