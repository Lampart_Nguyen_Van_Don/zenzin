var lang = {
    //Dropdown label
    all : '＝＝すべて＝＝',
    message_confirm_sequence_update: "表示順を変更してもよろしいですか？？",
    message_confirm_update : "更新してもよろしいですか？",
    message_confirm_apply : "申請してもよろしいですか？",
    message_confirm_delete: "削除してもよろしいですか？",
    title_confirm : "確認",
    lbl_yes:"YES",
    lbl_no:"NO",

    lbl_confirm: "確定",
    lbl_total_amount: "合計額",
    lbl_adjusted_amount: "入金",
    lbl_accumulative: "累計額",
    lbl_action: "処理",
    lbl_change: "変更",
    lbl_j_code: "Ｊコード",
    lbl_branch_cd: "枝",
    lbl_client_name: "会社名 ※",
    lbl_unit_price: "単価",
    lbl_quantity: "件数",
    lbl_delivery_date: "発送日 ※",

    button_delete : "はい",
    button_cancel : "いいえ",
    button_confirm : "確認",
    button_ok : "OK",
    button_yes : "はい",
    button_no : "いいえ",
    button_close : "閉じる",
    button_add: "追加",
    button_delete_ad_pay: "削除",

    // Added 24/June/2015
    date_between_row_above_and_below: "Date must be between row above and row below",
    time_must_be_in_month: "Time must be in month less or equal than:  ",
    time_must_be_in_year: "Time must be in year: ",
    date_is_not_valid: "発送日が無効です。",
    new_value_save_and_print: "このページから移動しますか？ 入力したデータは保存されません。",
    nothing_to_print: "データがありません。",
    save_ok_for_subcontractor: "保存しました。（ブレーンコード: <<code>>、ブレーン名: <<name>>）",
    save_fail_try_again: "保存に失敗しました。",
    no_new_data_to_save: "変更がありません。",
    save_all_ok: "全て保存しました。",
    other_grids_changed_want_to_save: "他のブレーンを編集中ですので、保存してもよろしいですか？",

    fill_client_name_before_save: "新規追加した行には会社名を入力してください。",
    input_search_from: "発送年月Fromを入力してください。",
    data_changed_search_again_or_not: "データが編集されました。検索を続けてもよろしいですか。",

    choose_file_before_upload: "アップロードファイルを選択してください。",
    areyousure: "破棄してよろしいですか？",
    discardsusscesinvoice:'破棄が完了しました。',
     //#6849 - Add -S
    text_title_show_order : "詳細",
    text_title_show_client : "クライアント詳細",
    col_last_name_order_acceptance : "税別その他",
     //#6849 - Add -E

     /* #6784 - Add -S */
    col_last_name_order_acceptance : "合計(税別、非課税)",
    col_frist_name_order_acceptance: "一括検収承認",
     /* #6784 - Add -E */

//* #6946:Start
//#8258:Start
    error_quantity_max_length : "件数は８桁以上の入力ができません。管理部に問い合わせてください。",
//#8258:End
    error_unit_price_max_length : "単価は１０桁以上の入力ができません。管理部に問い合わせてください。",
    error_tax_free_max_length : "非課税は１０桁以上の入力ができません。管理部に問い合わせてください。",
    error_shipping_charge_tax_max_length : "税込発送料は１０桁以上の入力ができません。管理部に問い合わせてください。",
    error_stamp_tax_max_length : "税込切手代は１０桁以上の入力ができません。管理部に問い合わせてください。",
    error_etc_tax_max_length:"税込その他は１０桁以上の入力ができません。管理部に問い合わせてください。",
    error_etc_max_length:"税別その他は１０桁以上の入力ができません。管理部に問い合わせてください。",
    error_shipping_charge_max_length:"税別発送料は１０桁以上の入力ができません。管理部に問い合わせてください。",
    
//* #6946:End

};
