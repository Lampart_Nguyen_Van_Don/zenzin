<?php die();

/**
 * Add you custom models here that you are loading in your controllers
 *
 * <code>
 * $this->site_model->get_records()
 * </code>
 * Where site_model is the model Class
 *
 * ---------------------- Models to Load ----------------------
 * <examples>
 *
 * @property Client_model                   $client_model
 * @property Account_model                  $account_model
 * @property Action_group_model             $action_group_model
 * @property Action_model                   $action_model
 * @property Advance_payment_model          $advance_payment_model
 * @property Authority_model                $authority_model
 * @property Bank_account_model             $bank_account_model
 * @property Business_unit_model            $business_unit_model
 * @property Consumption_tax_model          $consumption_tax_model
 * @property Department_model               $department_model
 * @property Gui_parts_element_model        $gui_parts_element_model
 * @property Gui_parts_model                $gui_parts_model
 * @property Holiday_model                  $holiday_model
 * @property Order_model                    $order_model
 * @property Order_acceptance_model         $order_acceptance_model
 * @property Order_detail_model             $order_detail_model
 * @property Product_model                  $product_model
 * @property Sales_slip_model               $sales_slip_model
 * @property Sample_model                   $sample_model
 * @property Shipping_type_model            $shipping_type_model
 * @property Subcontractor_model            $subcontractor_model
 * @property Subcontractor_relation_model   $subcontractor_relation_model
 * @property Tmp_grid_model                 $tmp_grid_model
 *
 */
class my_models
{
}

// End my_models.php
